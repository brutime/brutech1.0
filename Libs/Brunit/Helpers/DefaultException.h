//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Пользовательское исключение, не унаследованное от std::exception
//

#pragma once
#ifndef BRUNIT_DEFAULT_EXCEPTION_H
#define BRUNIT_DEFAULT_EXCEPTION_H

#include <string>
#include <ostream>

namespace Brunit
{

//===========================================================================//

class DefaultException
{
public:
	DefaultException();
	~DefaultException();
	
	DefaultException(const DefaultException & other);
	DefaultException & operator =(const DefaultException & other);
	
	const char * GetMessage() const;
	
private:
	std::string _message;
};

//===========================================================================//

extern std::ostream & operator <<(std::ostream & stream, const DefaultException & defaultException);

//===========================================================================//

} // namespace Brunit

#endif // BRUNIT_DEFAULT_EXCEPTION_H

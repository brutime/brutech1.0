//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Таймер
//

#pragma once
#ifndef BRUNIT_TIMER_H
#define BRUNIT_TIMER_H

namespace Brunit
{

//===========================================================================//

class Timer
{
public:
	Timer();
	virtual ~Timer();

	void Reset();
	double GetSeconds() const;

private:
	long long
	GetTicks() const;

	long long _startTime;
	long long _frequency;

#ifdef _WIN32
	void * _threadId;
	unsigned long _processAffinityMask;
#endif

	Timer(const Timer &) = delete;
	Timer & operator =(const Timer &) = delete;
};

//===========================================================================//

} // namespace Brunit

#endif // TIMER_H

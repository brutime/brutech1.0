//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Brunit
{

//===========================================================================//

template<typename Exception>
int TestRunner::RunAllTests(IOutputStream * outputStream)
{
	Timer timer;

	_outputStream = outputStream;

	int totalTestsCount = 0;
	totalTestsCount += _globalTestFixture.GetTestsCount();
	for (TestFixture & testFixture : _testFixtures)
	{
		totalTestsCount += testFixture.GetTestsCount();
	}

	int failedTestsCount = 0;
	failedTestsCount += RunTestFixture<Exception>(_globalTestFixture);
	for (TestFixture & testFixture : _testFixtures)
	{
		testFixture.Setup();
		failedTestsCount += RunTestFixture<Exception>(testFixture);
		testFixture.TearDown();
	}

	_outputStream->WriteSummary(totalTestsCount, failedTestsCount, timer.GetSeconds());

	return failedTestsCount;
}

//===========================================================================//

template<typename Exception>
int TestRunner::RunTestFixture(TestFixture & testFixture)
{
	int failedTestCount = 0;
	for (Test * test : testFixture)
	{
		test->Run<Exception>();
		if (test->IsPassed())
		{
			_outputStream->WriteTestPassed(test->GetTestInfo());
		}
		else
		{
			_outputStream->WriteTestFailure(test->GetTestInfo(), test->GetFailureText());
			failedTestCount++;
		}
	}
	return failedTestCount;	
}

//===========================================================================//

} // namespace Brunit

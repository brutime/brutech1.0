//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: "Запускатель" тестов
//

#pragma once
#ifndef BRUNIT_TEST_RUNNER_H
#define BRUNIT_TEST_RUNNER_H

#include <vector>
#include "OutputStreams/IOutputStream.h"
#include "OutputStreams/OSConsoleOutputStream.h"
#include "TestFixture/TestFixture.h"
#include "Helpers/DefaultException.h"
#include "Helpers/Timer.h"

namespace Brunit
{

//===========================================================================//

class TestRunner
{
public:
	TestRunner();
	virtual ~TestRunner();
	
	template<typename Exception = DefaultException>
	int RunAllTests(IOutputStream * outputStream);

	static void AddTestFixture();
	static void RestoreGlobalTestFixture();

	static void SetOutputStream(IOutputStream * outputStream);
	static IOutputStream * GetOutputStream();

	static TestFixture * GetCurrentTestFixture();
	
private:	
	template<typename Exception>
	int RunTestFixture(TestFixture & testFixture);

	static TestFixture _globalTestFixture;
	static std::vector<TestFixture> _testFixtures;
	static TestFixture * _currentTestFixture;
	static IOutputStream * _outputStream;

	TestRunner(const TestRunner &) = delete;
	TestRunner & operator =(const TestRunner &) = delete;
};

//===========================================================================//

} // namespace Brunit

#include "TestRunner.inl"

#endif // BRUNIT_TEST_RUNNER_H

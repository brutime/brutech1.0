//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тестовая конфигурация
//

#pragma once
#ifndef BRUNIT_TEST_FIXTURE_H
#define BRUNIT_TEST_FIXTURE_H

#include <list>
#include "../Test/Test.h"
#include "TestFixtureSetup.h"
#include "TestFixtureTearDown.h"

namespace Brunit
{

//===========================================================================//

class TestFixture
{
public:
	TestFixture();
	TestFixture(const TestFixture & other);
	virtual ~TestFixture();

	void AddTest(Test * test);
	int GetTestsCount() const;

	void Setup();
	void TearDown();

	void SetTestFixtureSetup(TestFixtureSetup * testFixtureSetup);
	void SetTestFixtureTearDown(TestFixtureTearDown * testFixtureTearDown);

	std::list<Test *>::iterator begin();
	std::list<Test *>::iterator end();

private:
	std::list<Test *> _list;
	TestFixtureSetup * _testFixtureSetup;
	TestFixtureTearDown * _testFixtureTearDown;

	TestFixture & operator =(const TestFixture &) = delete;
};

//===========================================================================//

} // namespace Brunit

#endif // BRUNIT_TEST_FIXTURE_H

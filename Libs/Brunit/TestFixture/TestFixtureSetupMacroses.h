//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Макросы для вызова найстройки и сворачивания тестовой конфигурации
//

#ifndef BRUNIT_TEST_FIXTURE_SETUP_MACROS_H
#define BRUNIT_TEST_FIXTURE_SETUP_MACROS_H

#include "TestFixtureSetup.h"
#include "TestFixtureTearDown.h"

//===========================================================================//

#define TEST_FIXTURE_SETUP(name) \
    class TestFixtureSetup##name : public Brunit::TestFixtureSetup \
    { \
    public: \
    	TestFixtureSetup##name() : TestFixtureSetup() {} \
		virtual ~TestFixtureSetup##name() {} \
		 \
		virtual void DoSetup(); \
    }; \
    \
    TestFixtureSetup##name testFixtureSetup##name; \
    \
    void TestFixtureSetup##name::DoSetup()

//===========================================================================//

#define TEST_FIXTURE_TEAR_DOWN(name) \
    class TestFixtureTearDown##name : public Brunit::TestFixtureTearDown \
    { \
    public: \
    TestFixtureTearDown##name() : TestFixtureTearDown() {} \
		virtual ~TestFixtureTearDown##name() {} \
		 \
		virtual void DoTearDown(); \
    }; \
    \
    TestFixtureTearDown##name testFixtureTearDown##name; \
    \
    void TestFixtureTearDown##name::DoTearDown()

//===========================================================================//

#endif // BRUNIT_TEST_FIXTURE_SETUP_MACROS_H

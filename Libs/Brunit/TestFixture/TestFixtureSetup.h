//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс для настройки тестовой конфигурации
//

#pragma once
#ifndef BRUNIT_TEST_FIXTURE_SETUP_H
#define BRUNIT_TEST_FIXTURE_SETUP_H

namespace Brunit
{

//===========================================================================//

class TestFixtureSetup
{
public:
	TestFixtureSetup();
	virtual ~TestFixtureSetup();

	virtual void DoSetup() = 0;

private:
	TestFixtureSetup(const TestFixtureSetup &) = delete;
	TestFixtureSetup & operator =(const TestFixtureSetup &) = delete;
};

//===========================================================================//

} // namespace Brunit

#endif // BRUNIT_TEST_FIXTURE_SETUP_H

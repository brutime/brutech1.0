//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс для сворачивания тестовой конфигурации
//

#pragma once
#ifndef BRUNIT_TEST_FIXTURE_TEAR_DOWN_H
#define BRUNIT_TEST_FIXTURE_TEAR_DOWN_H

namespace Brunit
{

//===========================================================================//

class TestFixtureTearDown
{
public:
	TestFixtureTearDown();
	virtual ~TestFixtureTearDown();

	virtual void DoTearDown() = 0;

private:
	TestFixtureTearDown(const TestFixtureTearDown &) = delete;
	TestFixtureTearDown & operator =(const TestFixtureTearDown &) = delete;
};

//===========================================================================//

} // namespace Brunit

#endif // BRUNIT_TEST_FIXTURE_TEAR_DOWN_H

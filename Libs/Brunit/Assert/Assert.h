//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Основной функционал для проверок
//

#pragma once
#ifndef BRUNIT_ASSERT_H
#define BRUNIT_ASSERT_H

#include <sstream>
#include <vector>
#include "../TestRunner.h"
#include "../Test/TestInfo.h"

namespace Brunit
{

//===========================================================================//

class Assert
{
public:
	static bool IsTrue(bool condition, const char * conditionText, const TestInfo & testInfo);
	static bool IsFalse(bool condition, const char * conditionText, const TestInfo & testInfo);
	
	template<typename Actual>
	static bool IsNull(const Actual & actual, const TestInfo & testInfo);

	template<typename Expected, typename Actual>
	inline static bool AreEqual(const Expected & expected, const Actual & actual, const TestInfo & testInfo);
	
	template<typename Actual>
	inline static bool AreEqual(std::nullptr_t, const Actual & actual, const TestInfo & testInfo);	

	static bool AreEqual(const char * expected, const char * actual, const TestInfo & testInfo);

	template<typename Expected, typename Actual, typename Accuracy>
	inline static bool AreClose(const Expected & expected, const Actual & actual, const Accuracy & accuracy, const TestInfo & testInfo);

	template<typename Expected, typename Actual>
	inline static bool AreArraysEqual(const Expected & expected, const Actual & actual, int count, const TestInfo & testInfo);

	template<typename Expected, typename Actual, typename Accuracy>
	inline static bool AreArraysClose(const Expected & expected, const Actual & actual, int count, const Accuracy & accuracy,
	                                  const TestInfo & testInfo);

	template<typename Expected, typename Actual>
	inline static bool AreArrays2DEqual(const Expected & expected, const Actual & actual, int rowsCount, int columnsCount,
	                                    const TestInfo & testInfo);

	template<typename Expected, typename Actual, typename Accuracy>
	inline static bool AreArrays2DClose(const Expected & expected, const Actual & actual, int rowsCount, int columnsCount,
	                                    Accuracy accuracy, const TestInfo & testInfo);
										
	template<typename Expected, typename Actual, typename Comparer>
	inline static bool Compare(const Expected & expected, const Actual & actual, const Comparer & comparer, const TestInfo & testInfo);										

private:
	template<typename Expected, typename Actual, typename Accuracy>
	static bool AreClose(const Expected & expected, const Actual & actual, const Accuracy & accuracy);

	template<typename Expected, typename Actual, typename Accuracy>
	static bool ArraysAreClose(const Expected & expected, const Actual & actual, int count, const Accuracy & accuracy);

	template<typename Array>
	static void WriteArray(std::stringstream & stream, const Array & array, std::vector<bool> & notEqualIndices, int count);

	template<typename Array>
	static void WriteArray2D(std::stringstream & stream, const Array & array, std::vector<std::vector<bool>> & notEqualIndices,
	                         int rowsCount, int columnsCount);

	Assert() = delete;
	Assert(const Assert &) = delete;
	~Assert() = delete;
	Assert & operator =(const Assert &) = delete;
};

//===========================================================================//

} // namespace Brunit

#include "Assert.inl"

#endif // BRUNIT_ASSERT_H

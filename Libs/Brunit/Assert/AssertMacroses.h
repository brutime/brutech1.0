//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Макросы для вызова проверок
//

#ifndef BRUNIT_ASSERT_MACORSES_H
#define BRUNIT_ASSERT_MACORSES_H

#include "Assert.h"

//===========================================================================//

#define IS_TRUE(condition) \
	if (!Brunit::Assert::IsTrue(condition, #condition, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define IS_FALSE(condition) \
	if (!Brunit::Assert::IsFalse(condition, #condition, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}
		
//===========================================================================//

#define IS_NULL(actual) \
	if (!Brunit::Assert::IsNull(actual, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define ARE_EQUAL(expected, actual) \
	if (!Brunit::Assert::AreEqual(expected, actual, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define ARE_CLOSE(expected, actual, accuracy) \
	if (!Brunit::Assert::AreClose(expected, actual, accuracy, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define ARE_ARRAYS_EQUAL(expected, actual, count) \
	if (!Brunit::Assert::AreArraysEqual(expected, actual, count, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define ARE_ARRAYS_CLOSE(expected, actual, count, accuracy) \
	if (!Brunit::Assert::AreArraysClose(expected, actual, count, accuracy, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define ARE_ARRAYS_2D_EQUAL(expected, actual, rowsCount, columnsCount) \
	if (!Brunit::Assert::AreArrays2DEqual(expected, actual, rowsCount, columnsCount, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define ARE_ARRAYS_2D_CLOSE(expected, actual, rowsCount, columnsCount, accuracy) \
	if (!Brunit::Assert::AreArrays2DClose(expected, actual, rowsCount, columnsCount, accuracy, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define COMPARE(expected, actual, comparer) \
	if (!Brunit::Assert::Compare(expected, actual, comparer, Brunit::TestInfo(_testInfo, __LINE__))) \
	{ \
		_isPassed = false; \
	}

//===========================================================================//

#define IS_THROW(expression, expectedExceptionType) \
	{ \
		bool thrown = false; \
		try \
		{ \
			expression; \
		} \
		catch (expectedExceptionType const &) \
		{ \
			thrown = true; \
		} \
		catch (...) \
		{ \
		} \
		 \
		if (!thrown) \
		{ \
			std::stringstream stream; \
			stream << "Expected exception: \"" << #expectedExceptionType << "\" not thrown"; \
			Brunit::TestRunner::GetOutputStream()->WriteTestFailure(_testInfo, stream.str().c_str()); \
		} \
		_isPassed = thrown; \
	}

//===========================================================================//

#endif // BRUNIT_ASSERT_MACORSES_H

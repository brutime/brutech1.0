//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Brunit
{

//===========================================================================//

template<typename Actual>
bool Assert::IsNull(const Actual & actual, const TestInfo & testInfo)
{
	if (actual == nullptr)
	{
		return true;
	}

	std::stringstream stream;
	stream << "Expected: \"Null\"" << std::endl;
	stream << "Actual:   \"" << actual << "\"" << std::endl;

	TestRunner::GetOutputStream()->WriteTestFailure(testInfo, stream.str().c_str());

	return false;
}

//===========================================================================//

template<typename Expected, typename Actual>
bool Assert::AreEqual(const Expected & expected, const Actual & actual, const TestInfo & testInfo)
{
	if (actual == expected)
	{
		return true;
	}

	std::stringstream stream;
	stream << "Expected: \"" << expected << "\"" << std::endl;
	stream << "Actual:   \"" << actual << "\"" << std::endl;

	TestRunner::GetOutputStream()->WriteTestFailure(testInfo, stream.str().c_str());

	return false;
}

//===========================================================================//

template<typename Actual>
bool Assert::AreEqual(std::nullptr_t, const Actual & actual, const TestInfo & testInfo)
{
	return IsNull(actual, testInfo);
}

//===========================================================================//

template<typename Expected, typename Actual, typename Accuracy>
bool Assert::AreClose(const Expected & expected, const Actual & actual, const Accuracy & accuracy, const TestInfo & testInfo)
{
	if (AreClose(expected, actual, accuracy))
	{
		return true;
	}

	std::stringstream stream;
	stream << "Expected: \"" << expected - accuracy << "-" << expected + accuracy << "\"" << std::endl;
	stream << "Actual:   \"" << actual << "\"" << std::endl;
	stream << "Accuracy: \"" << accuracy << "\"" << std::endl;

	TestRunner::GetOutputStream()->WriteTestFailure(testInfo, stream.str().c_str());

	return false;
}

//===========================================================================//

template<typename Expected, typename Actual>
bool Assert::AreArraysEqual(const Expected & expected, const Actual & actual, int count, const TestInfo & testInfo)
{
	bool equal = true;
	std::vector<bool> notEqualIndices(count);
	for (int i = 0; i < count; i++)
	{
		notEqualIndices[i] = expected[i] != actual[i];
		if (notEqualIndices[i])
		{
			equal = false;
		}
	}

	if (equal)
	{
		return true;
	}

	std::stringstream stream;

	stream << "Expected: ";
	WriteArray(stream, expected, notEqualIndices, count);
	stream << std::endl << "Actual:   ";
	WriteArray(stream, actual, notEqualIndices, count);
	stream << std::endl;

	TestRunner::GetOutputStream()->WriteTestFailure(testInfo, stream.str().c_str());

	return false;
}

//===========================================================================//

template<typename Expected, typename Actual, typename Accuracy>
bool Assert::AreArraysClose(const Expected & expected, const Actual & actual, int count, const Accuracy & accuracy,
                            const TestInfo & testInfo)
{
	bool equal = true;
	std::vector<bool> notEqualIndices(count);
	for (int i = 0; i < count; i++)
	{
		notEqualIndices[i] = !AreClose(expected[i], actual[i], accuracy);
		if (notEqualIndices[i])
		{
			equal = false;
		}
	}

	if (equal)
	{
		return true;
	}

	std::stringstream stream;

	stream << "Expected: ";
	WriteArray(stream, expected, notEqualIndices, count);
	stream << std::endl << "Actual:   ";
	WriteArray(stream, actual, notEqualIndices, count);
	stream << std::endl << "Accuracy: \"" << accuracy << "\"";
	stream << std::endl;

	TestRunner::GetOutputStream()->WriteTestFailure(testInfo, stream.str().c_str());

	return false;
}

//===========================================================================//

template<typename Expected, typename Actual>
bool Assert::AreArrays2DEqual(const Expected & expected, const Actual & actual, int rowsCount, int columnsCount,
                              const TestInfo & testInfo)
{
	bool equal = true;
	std::vector<std::vector<bool>> notEqualIndices(rowsCount);
	for (int i = 0; i < rowsCount; i++)
	{
		notEqualIndices[i].resize(columnsCount);
		for (int j = 0; j < columnsCount; j++)
		{
			notEqualIndices[i][j] = expected[i][j] != actual[i][j];
			if (notEqualIndices[i][j])
			{
				equal = false;
			}
		}
	}

	if (equal)
	{
		return true;
	}

	std::stringstream stream;

	stream << "Expected:" << std::endl;
	WriteArray2D(stream, expected, notEqualIndices, rowsCount, columnsCount);
	stream << std::endl << "Actual:" << std::endl;
	WriteArray2D(stream, actual, notEqualIndices, rowsCount, columnsCount);
	stream << std::endl;

	TestRunner::GetOutputStream()->WriteTestFailure(testInfo, stream.str().c_str());

	return false;
}

//===========================================================================//

template<typename Expected, typename Actual, typename Accuracy>
bool Assert::AreArrays2DClose(const Expected & expected, const Actual & actual, int rowsCount, int columnsCount,
                              Accuracy accuracy, const TestInfo & testInfo)
{
	bool equal = true;
	std::vector<std::vector<bool>> notEqualIndices(rowsCount);
	for (int i = 0; i < rowsCount; i++)
	{
		notEqualIndices[i].resize(columnsCount);
		for (int j = 0; j < columnsCount; j++)
		{
			notEqualIndices[i][j] = !AreClose(expected[i][j], actual[i][j], accuracy);
			if (notEqualIndices[i][j])
			{
				equal = false;
			}
		}
	}

	if (equal)
	{
		return true;
	}

	std::stringstream stream;

	stream << "Expected:" << std::endl;
	WriteArray2D(stream, expected, notEqualIndices, rowsCount, columnsCount);
	stream << std::endl << "Actual:" << std::endl;
	WriteArray2D(stream, actual, notEqualIndices, rowsCount, columnsCount);
	stream << std::endl << "Accuracy: \"" << accuracy << "\"";
	stream << std::endl;

	TestRunner::GetOutputStream()->WriteTestFailure(testInfo, stream.str().c_str());

	return false;
}

//===========================================================================//

template<typename Expected, typename Actual, typename Comparer>
bool Assert::Compare(const Expected & expected, const Actual & actual, const Comparer & comparer, const TestInfo & testInfo)
{
	return comparer.Compare(expected, actual, testInfo);
}

//===========================================================================//

template<typename Expected, typename Actual, typename Accuracy>
bool Assert::AreClose(const Expected & expected, const Actual & actual, const Accuracy & accuracy)
{
	return (actual >= (expected - accuracy)) && (actual <= (expected + accuracy));
}

//===========================================================================//

template<typename Array>
void Assert::WriteArray(std::stringstream & stream, const Array & array, std::vector<bool> & notEqualIndices, int count)
{
	stream << "[ ";
	for (int i = 0; i < count; i++)
	{
		if (notEqualIndices[i])
		{
			stream << "|" << array[i] << "|";
		}
		else
		{
			stream << array[i];
		}

		if (i < count - 1)
		{
			stream << ",  ";
		}
	}
	stream << " ]";
}

//===========================================================================//

template<typename Array>
void Assert::WriteArray2D(std::stringstream & stream, const Array & array, std::vector<std::vector<bool>> & notEqualIndices,
                          int rowsCount, int columnsCount)
{
	for (int i = 0; i < rowsCount; i++)
	{
		stream << "[ ";
		for (int j = 0; j < columnsCount; j++)
		{
			if (notEqualIndices[i][j])
			{
				stream << "|" << array[i][j] << "|";
			}
			else
			{
				stream << array[i][j];
			}

			if (j < columnsCount - 1)
			{
				stream << ",  ";
			}
		}
		stream << " ]";
		if (i < rowsCount - 1)
		{
			stream << std::endl;
		}
	}
}

//===========================================================================//

} // namespace Brunit

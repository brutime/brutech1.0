//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс потока вывода
//

#pragma once
#ifndef BRUNIT_I_OUTPUT_STREAM_H
#define BRUNIT_I_OUTPUT_STREAM_H

#include "../Test/TestInfo.h"

namespace Brunit
{

//===========================================================================//

class IOutputStream
{
public:
	virtual ~IOutputStream();

	virtual void WriteTestPassed(const TestInfo & testInfo) = 0;
	virtual void WriteTestFailure(const TestInfo & testInfo, const char * failureText) = 0;
	virtual void WriteSummary(int totalTestCount, int failedTestCount, float elapsedTime) = 0;

protected:
	IOutputStream();

private:
	IOutputStream(const IOutputStream &) = delete;
	IOutputStream & operator =(const IOutputStream &) = delete;
};

//===========================================================================//

} // namespace Brunit

#endif // BRUNIT_I_OUTPUT_STREAM_H

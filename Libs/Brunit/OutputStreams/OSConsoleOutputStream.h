//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Поток вывода в консоль операционной системы
//

#pragma once
#ifndef BRUNIT_OS_CONSOLE_OUTPUT_STREAM_H
#define BRUNIT_OS_CONSOLE_OUTPUT_STREAM_H

#include "IOutputStream.h"

namespace Brunit
{

//===========================================================================//

class OSConsoleOutputStream : public IOutputStream
{
public:
	OSConsoleOutputStream();
	virtual ~OSConsoleOutputStream();

	virtual void WriteTestPassed(const TestInfo & testInfo);
	virtual void WriteTestFailure(const TestInfo & testInfo, const char * failureText);
	virtual void WriteSummary(int totalTestCount, int failedTestCount, float elapsedTime);

private:
	const char * GetTestsString(int testsCount) const;

	OSConsoleOutputStream(const OSConsoleOutputStream &) = delete;
	OSConsoleOutputStream & operator =(const OSConsoleOutputStream &) = delete;
};

//===========================================================================//

} // namespace Brunit

#endif // BRUNIT_OS_CONSOLE_OUTPUT_STREAM_H

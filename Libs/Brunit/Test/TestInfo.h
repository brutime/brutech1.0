//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Информация о тесте
//

#pragma once
#ifndef BRUNIT_TEST_INFO_H
#define BRUNIT_TEST_INFO_H

#include <string>

namespace Brunit
{

//===========================================================================//

class TestInfo
{
public:
	TestInfo(const char * testName, const char * fileName, int lineNumber);
	TestInfo(const TestInfo & other, int lineNumber);
	virtual ~TestInfo();

	const char * GetTestName() const;
	const char * GetFileName() const;
	int GetLineNumber() const;

private:
	std::string _testName;
	std::string _fileName;
	int _lineNumber;

	TestInfo(const TestInfo &) = delete;
	TestInfo & operator =(const TestInfo &) = delete;
	
	friend class TestTestInfoCopyConstructor;
};

//===========================================================================//

} // namespace Brunit

#endif // BRUNIT_TEST_INFO_H

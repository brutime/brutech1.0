//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Макросы для объявления тестов
//

#ifndef BRUNIT_TEST_MACROS_H
#define BRUNIT_TEST_MACROS_H

#include "Test.h"
#include "../TestRunner.h"

//===========================================================================//

#define TEST(name) \
    class Test##name : public Brunit::Test \
    { \
    public: \
		Test##name() : Test(#name, __FILE__, __LINE__) {} \
		virtual ~Test##name() {} \
    protected: \
        virtual void DoTest(); \
    }; \
    \
    Test##name test##name; \
    \
    void Test##name::DoTest()

//===========================================================================//

#define SETUP_TEAR_DOWN_TEST(name, setupMethod, tearDownMethod) \
    class Test##name : public Brunit::Test \
    { \
    public: \
		Test##name() : Test(#name, __FILE__, __LINE__) {} \
		virtual ~Test##name() {} \
    protected: \
    	virtual void DoTestWrapper() \
    	{ \
    		setupMethod();\
    		DoTest();\
    		tearDownMethod();\
    	} \
        virtual void DoTest(); \
    }; \
    \
    Test##name test##name; \
    \
    void Test##name::DoTest()

//===========================================================================//

#endif // BRUNIT_TEST_MACROS_H

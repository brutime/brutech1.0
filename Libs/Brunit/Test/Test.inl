//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Brunit
{

//===========================================================================//

template<typename Exception>
void Test::Run()
{
	try
	{
		DoTestWrapper();
	}
	catch (const std::exception & exception)
	{
		_isPassed = false;
		_failureText = "Unhandled exception: " + std::string(exception.what());
	}
	catch (const Exception & exception)
	{
		_isPassed = false;		
		std::stringstream stream;
		stream << exception;
		_failureText = "Unhandled exception: " + stream.str();
	}
	catch (...)
	{
		_isPassed = false;
		_failureText = "Unhandled exception: Crash!";
	}
}

//===========================================================================//

} // namespace Brunit

//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест
//

#pragma once
#ifndef BRUNIT_TEST_H
#define BRUNIT_TEST_H

#include <string>
#include <sstream>
#include <exception>
#include "TestInfo.h"

namespace Brunit
{

//===========================================================================//

class Test
{
public:
	Test(const char * testName, const char * fileName, int lineNumber);
	virtual ~Test();
	
	template<typename Exception>
	void Run();

	const TestInfo & GetTestInfo() const;
	bool IsPassed() const;
	const char * GetFailureText() const;

protected:
	virtual void DoTestWrapper();
	virtual void DoTest() = 0;

	TestInfo _testInfo;
	bool _isPassed;
	std::string _failureText;

	Test(const Test &) = delete;
	Test & operator =(const Test &) = delete;
};

//===========================================================================//

} // namespace Brunit

#include "Test.inl"

#endif // BRUNIT_TEST_H

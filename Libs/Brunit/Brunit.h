//
//    Brunit Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of Brunit Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Главный хедер BruTest
//

#pragma once
#ifndef BRUNIT_H
#define BRUNIT_H

#include "Assert/AssertMacroses.h"
#include "OutputStreams/IOutputStream.h"
#include "OutputStreams/OSConsoleOutputStream.h"
#include "Test/TestMacros.h"
#include "TestFixture/TestFixtureSetupMacroses.h"
#include "TestRunner.h"

#endif // BRUNIT_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс цвета
//

#pragma once
#ifndef COLOR_H
#define COLOR_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class Color
{
public:
	static const Color Empty;
	static const Color Default;

	Color();
	Color(float red, float green, float blue, float alpha = 1.0f);
	Color(integer redByte, integer greenByte, integer blueByte, integer alphaByte = 255);
	Color(const string & colorString);
	Color(const Color & other);		
	Color(const Color & other, float alpha);
	Color(Color && other);
	~Color();

	Color & operator =(const Color & other);
	Color & operator =(Color && other);

	Color & operator +=(const Color & someColor);
	Color & operator -=(const Color & someColor);
	Color & operator *=(const Color & someColor);

	Color operator +(const Color & someColor) const;
	Color operator -(const Color & someColor) const;
	Color operator *(const Color & someColor) const;

	bool operator ==(const Color & someColor) const;
	bool operator !=(const Color & someColor) const;
	bool operator <(const Color & someColor) const;
	bool operator <=(const Color & someColor) const;
	bool operator >(const Color & someColor) const;
	bool operator >=(const Color & someColor) const;
	
	Color GetInverted() const;
	Color GetInvertedExceptAlpha() const;

	void MakeRandomRGB();
	void MakeRandomRGBA();

	static Color RandomRGB();
	static Color RandomRGBA();

	void SetRed(float red);
	float GetRed() const;
	integer GetRedByte() const;

	void SetGreen(float green);
	float GetGreen() const;
	integer GetGreenByte() const;

	void SetBlue(float blue);
	float GetBlue() const;
	integer GetBlueByte() const;

	void SetAlpha(float alpha);
	float GetAlpha() const;
	integer GetAlphaByte() const;

	void Clear();

	string ToString() const;

private:
	void MakeValid();

	static const integer ColorNoAlphaStringLength;
	static const integer ColorWithAlphaStringLength;

	float _red;
	float _green;
	float _blue;
	float _alpha;
};

//===========================================================================//

}//namespace Bru

#endif // COLOR_H

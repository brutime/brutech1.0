//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Набор цветов
//

#pragma once
#ifndef COLORS_H
#define COLORS_H

#include "Color.h"

namespace Bru
{

//===========================================================================//

namespace Colors
{

///Оттенки серого
const Color Black(0.0f, 0.0f, 0.0f, 1.0f);
const Color Gray05(0.05f, 0.05f, 0.05f, 1.0f);
const Color Gray10(0.10f, 0.10f, 0.10f, 1.0f);
const Color Gray15(0.15f, 0.15f, 0.15f, 1.0f);
const Color Gray20(0.20f, 0.20f, 0.20f, 1.0f);
const Color Gray25(0.25f, 0.25f, 0.25f, 1.0f);
const Color Gray30(0.30f, 0.30f, 0.30f, 1.0f);
const Color Gray35(0.35f, 0.35f, 0.35f, 1.0f);
const Color Gray40(0.40f, 0.40f, 0.40f, 1.0f);
const Color Gray45(0.45f, 0.45f, 0.45f, 1.0f);
const Color Gray50(0.50f, 0.50f, 0.50f, 1.0f);
const Color Gray55(0.55f, 0.55f, 0.55f, 1.0f);
const Color Gray60(0.60f, 0.60f, 0.60f, 1.0f);
const Color Gray65(0.65f, 0.65f, 0.65f, 1.0f);
const Color Gray70(0.70f, 0.70f, 0.70f, 1.0f);
const Color Gray75(0.75f, 0.75f, 0.75f, 1.0f);
const Color Gray80(0.80f, 0.80f, 0.80f, 1.0f);
const Color Gray85(0.85f, 0.85f, 0.85f, 1.0f);
const Color Gray90(0.90f, 0.90f, 0.90f, 1.0f);
const Color Gray95(0.95f, 0.95f, 0.95f, 1.0f);
const Color White(1.0f, 1.0f, 1.0f, 1.0f);

//Другие серые цвета
const Color DimGram(0.329412f, 0.329412f, 0.329412f, 1.0f);
const Color Gray(0.752941f, 0.752941f, 0.752941f, 1.0f);
const Color LightGray(0.658824f, 0.658824f, 0.658824f, 1.0f);

//Различные цвета
const Color Aquamarine(0.439216f, 0.858824f, 0.576471f, 1.0f);
const Color BakersChoc(0.36f, 0.20f, 0.09f, 1.0f);
const Color BlueViolet(0.623520f, 0.372549f, 0.623529f, 1.0f);
const Color Brass(0.71f, 0.65f, 0.26f, 1.0f);
const Color BrightGold(0.85f, 0.85f, 0.10f, 1.0f);
const Color Bronze(0.55f, 0.47f, 0.14f, 1.0f);
const Color Bronze2(0.65f, 0.49f, 0.24f, 1.0f);
const Color Brown(0.647059f, 0.164706f, 0.164706f, 1.0f);
const Color CadetBlue(0.372549f, 0.623529f, 0.623529f, 1.0f);
const Color CoolCopper(0.85f, 0.53f, 0.10f, 1.0f);
const Color Copper(0.72f, 0.45f, 0.20f, 1.0f);
const Color Coral(1.0f, 0.498039f, 0.0f, 1.0f);
const Color CornFlowerBlue(0.258824f, 0.258824f, 0.435294f, 1.0f);
const Color CpicyPink(1.00f, 0.11f, 0.68f, 1.0f);
const Color DarkBrown(0.36f, 0.25f, 0.20f, 1.0f);
const Color DarkGreen(0.184314f, 0.309804f, 0.184314f, 1.0f);
const Color DarkGreenCopper(0.29f, 0.46f, 0.43f, 1.0f);
const Color DarkOliveGreen(0.309804f, 0.309804f, 0.184314f, 1.0f);
const Color DarkOrchid(0.6f, 0.196078f, 0.8f, 1.0f);
const Color DarkPurple(0.53f, 0.12f, 0.47f, 1.0f);
const Color DarkSlateBlue(0.419608f, 0.137255f, 0.556863f, 1.0f);
const Color DarkSlateGray(0.184314f, 0.309804f, 0.309804f, 1.0f);
const Color DarkTan(0.59f, 0.41f, 0.31f, 1.0f);
const Color DarkTurqoise(0.439216f, 0.576471f, 0.858824f, 1.0f);
const Color DarkWood(0.52f, 0.37f, 0.26f, 1.0f);
const Color DustyRose(0.52f, 0.39f, 0.39f, 1.0f);
const Color Feldrfar(0.82f, 0.57f, 0.46f, 1.0f);
const Color FireBrick(0.556863f, 0.137255f, 0.137255f, 1.0f);
const Color Flesh(0.96f, 0.80f, 0.69f, 1.0f);
const Color ForestGreen(0.137255f, 0.556863f, 0.137255f, 1.0f);
const Color Gold(0.8f, 0.498039f, 0.196078f, 1.0f);
const Color GoldenRod(0.858824f, 0.858824f, 0.439216f, 1.0f);
const Color GreenCopper(0.32f, 0.49f, 0.46f, 1.0f);
const Color GreenYellow(0.576471f, 0.858824f, 0.439216f, 1.0f);
const Color HuntersGreen(0.13f, 0.37f, 0.31f, 1.0f);
const Color Indian(0.309804f, 0.184314f, 0.184314f, 1.0f);
const Color Khaki(0.623529f, 0.623529f, 0.372549f, 1.0f);
const Color LightBlue(0.749020f, 0.847059f, 0.847059f, 1.0f);
const Color LightGreen(0.694118f, 0.988235f, 0.584314f, 1.0f);
const Color LightPurple(0.87f, 0.58f, 0.98f, 1.0f);
const Color LightRed(1.0f, 0.2f, 0.2f, 1.0f);
const Color LightSteelBlue(0.560784f, 0.560784f, 0.737255f, 1.0f);
const Color LightWood(0.91f, 0.76f, 0.65f, 1.0f);
const Color LimeGreen(0.196078f, 0.8f, 0.196078f, 1.0f);
const Color MandarinOrange(0.89f, 0.47f, 0.20f, 1.0f);
const Color Maroon(0.556863f, 0.137255f, 0.419608f, 1.0f);
const Color MediumAquamarine(0.196078f, 0.8f, 0.6f, 1.0f);
const Color MediumBlue(0.196078f, 0.196078f, 0.8f, 1.0f);
const Color MediumForestGreen(0.419608f, 0.556863f, 0.137255f, 1.0f);
const Color MediumGoldenRod(0.917647f, 0.917647f, 0.678431f, 1.0f);
const Color MediumOrchid(0.576471f, 0.439216f, 0.858824f, 1.0f);
const Color MediumPurple(0.73f, 0.16f, 0.96f, 1.0f);
const Color MediumSeaGreen(0.258824f, 0.435294f, 0.258824f, 1.0f);
const Color MediumSlateBlue(0.498039f, 0.0f, 1.0f, 1.0f);
const Color MediumSpringGreen(0.498039f, 1.0f, 0.0f, 1.0f);
const Color MediumTurquoise(0.439216f, 0.858824f, 0.858824f, 1.0f);
const Color MediumViolet(0.858824f, 0.439216f, 0.576471f, 1.0f);
const Color MediumWood(0.65f, 0.50f, 0.39f, 1.0f);
const Color MidnightBlue(0.184314f, 0.184314f, 0.309804f, 1.0f);
const Color Navy(0.137255f, 0.137255f, 0.556863f, 1.0f);
const Color NavyBlue(0.137255f, 0.137255f, 0.556863f, 1.0f);
const Color NeonBlue(0.30f, 0.30f, 1.00f, 1.0f);
const Color NeonPink(1.00f, 0.43f, 0.78f, 1.0f);
const Color NewMidnightBlue(0.00f, 0.00f, 0.61f, 1.0f);
const Color NewTan(0.92f, 0.78f, 0.62f, 1.0f);
const Color OldGold(0.81f, 0.71f, 0.23f, 1.0f);
const Color Orange(1.0f, 0.5f, 0.0f, 1.0f);
const Color OrangeRed(1.0f, 0.25f, 0.0f, 1.0f);
const Color Orchid(0.858824f, 0.439216f, 0.858824f, 1.0f);
const Color PaleGreen(0.560784f, 0.737255f, 0.560784f, 1.0f);
const Color Pink(0.737255f, 0.560784f, 0.560784f, 1.0f);
const Color Plum(0.917647f, 0.678431f, 0.917647f, 1.0f);
const Color Quartz(0.85f, 0.85f, 0.95f, 1.0f);
const Color RichBlue(0.35f, 0.35f, 0.67f, 1.0f);
const Color RoyalBlue(0.254902f, 0.411765f, 1.00f, 1.0f);
const Color Salmon(0.435294f, 0.258824f, 0.258824f, 1.0f);
const Color Scarlet(0.55f, 0.09f, 0.09f, 1.0f);
const Color SeaGreen(0.137255f, 0.556863f, 0.419608f, 1.0f);
const Color SemiSweetChoc(0.42f, 0.26f, 0.15f, 1.0f);
const Color Sienna(0.556863f, 0.419608f, 0.137255f, 1.0f);
const Color Silver(0.90f, 0.91f, 0.98f, 1.0f);
const Color SkyBlue(0.196078f, 0.6f, 0.8f, 1.0f);
const Color SlateBlue(0.0f, 0.498039f, 1.0f, 1.0f);
const Color SpringGreen(0.0f, 1.0f, 0.498039f, 1.0f);
const Color SteelBlue(0.137255f, 0.419608f, 0.556863f, 1.0f);
const Color SummerSky(0.22f, 0.69f, 0.87f, 1.0f);
const Color Tan(0.858824f, 0.576471f, 0.439216f, 1.0f);
const Color Thistle(0.847059f, 0.749020f, 0.847059f, 1.0f);
const Color Turquise(0.678431f, 0.917647f, 0.917647f, 1.0f);
const Color VeryDarkBrown(0.35f, 0.16f, 0.14f, 1.0f);
const Color VeryLightPurple(0.94f, 0.81f, 0.99f, 1.0f);
const Color Violet(0.309804f, 0.184314f, 0.309804f, 1.0f);
const Color VioletRed(0.8f, 0.196078f, 0.6f, 1.0f);
const Color Wheat(0.847059f, 0.847059f, 0.749020f, 1.0f);
const Color YellowGreen(0.6f, 0.8f, 0.196078f, 1.0f);

//Базовые цвета
const Color Aqua(0.0f, 1.0f, 1.0f, 1.0f);
const Color Blue(0.0f, 0.0f, 1.0f, 1.0f);
const Color Fuchsia(1.0f, 0.0f, 1.0f, 1.0f);
const Color Green(0.0f, 1.0f, 0.0f, 1.0f);
const Color Lime(0.0f, 1.0f, 0.0f, 1.0f);
const Color Olive(0.5f, 0.5f, 1.0f, 1.0f);
const Color Purple(1.0f, 0.0f, 1.0f, 1.0f);
const Color Red(1.0f, 0.0f, 0.0f, 1.0f);
const Color Teal(0.0f, 0.5f, 0.5f, 1.0f);
const Color Yellow(1.0f, 1.0f, 0.0f, 1.0f);

} // Colors

//===========================================================================//

}// Namespace

//===========================================================================//

#endif // COLORS_H

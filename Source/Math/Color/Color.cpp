//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Color.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

const Color Color::Empty(0.0f, 0.0f, 0.0f, 0.0f);
const Color Color::Default = Colors::White;
const integer Color::ColorNoAlphaStringLength = 6;
const integer Color::ColorWithAlphaStringLength = 8;

//===========================================================================//

Color::Color() :
	_red(Color::Default._red),
	_green(Color::Default._green),
	_blue(Color::Default._blue),
	_alpha(Color::Default._alpha)
{
}

//===========================================================================//

Color::Color(float red, float green, float blue, float alpha) :
	_red(red),
	_green(green),
	_blue(blue),
	_alpha(alpha)
{
	MakeValid();
}

//===========================================================================//

Color::Color(integer redByte, integer greenByte, integer blueByte, integer alphaByte) :
	_red(redByte / 255.0f),
	_green(greenByte / 255.0f),
	_blue(blueByte / 255.0f),
	_alpha(alphaByte / 255.0f)
{
	MakeValid();
}

//===========================================================================//

Color::Color(const string & colorString) :
	_red(Color::Default._red),
	_green(Color::Default._green),
	_blue(Color::Default._blue),
	_alpha(Color::Default._alpha)
{
	TokenParser parser(colorString);
	if (parser.GetTokensCount() != 4)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 4, but it is {0}", parser.GetTokensCount()));
	}

	_red = Converter::ToFloat(parser.GetToken(0)) / 255;
	_green = Converter::ToFloat(parser.GetToken(1)) / 255;
	_blue = Converter::ToFloat(parser.GetToken(2)) / 255;
	_alpha = Converter::ToFloat(parser.GetToken(3)) / 255;
	MakeValid();
}

//===========================================================================//

Color::Color(const Color & other) :
	_red(other._red),
	_green(other._green),
	_blue(other._blue),
	_alpha(other._alpha)
{
}

//===========================================================================//

Color::Color(const Color & other, float alpha) :
	_red(other._red),
	_green(other._green),
	_blue(other._blue),
	_alpha(alpha)
{
}

//===========================================================================//

Color::Color(Color && other) :
	_red(other._red),
	_green(other._green),
	_blue(other._blue),
	_alpha(other._alpha)
{
	other._red = 0.0f;
	other._green = 0.0f;
	other._blue = 0.0f;
	other._alpha = 0.0f;
}

//===========================================================================//

Color::~Color()
{
}

//===========================================================================//

Color & Color::operator =(const Color & other)
{
	if (this == &other)
	{
		return *this;
	}

	_red = other._red;
	_green = other._green;
	_blue = other._blue;
	_alpha = other._alpha;

	return *this;
}

//===========================================================================//

Color & Color::operator =(Color && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_red, other._red);
	std::swap(_green, other._green);
	std::swap(_blue, other._blue);
	std::swap(_alpha, other._alpha);

	return *this;
}

//===========================================================================//

Color & Color::operator +=(const Color & someColor)
{
	_red += someColor._red;
	_green += someColor._green;
	_blue += someColor._blue;
	_alpha += someColor._alpha;

	MakeValid();

	return *this;
}

//===========================================================================//

Color & Color::operator *=(const Color & someColor)
{
	_red *= someColor._red;
	_green *= someColor._green;
	_blue *= someColor._blue;
	_alpha *= someColor._alpha;

	MakeValid();

	return *this;
}

//===========================================================================//

Color & Color::operator -=(const Color & someColor)
{
	_red -= someColor._red;
	_green -= someColor._green;
	_blue -= someColor._blue;
	_alpha -= someColor._alpha;

	MakeValid();

	return *this;
}

//===========================================================================//

Color Color::operator +(const Color & someColor) const
{
	Color resultColor = *this;
	resultColor += someColor;
	return resultColor;
}

//===========================================================================//

Color Color::operator -(const Color & someColor) const
{
	Color resultColor = *this;
	resultColor -= someColor;
	return resultColor;
}

//===========================================================================//

Color Color::operator *(const Color & someColor) const
{
	Color resultColor = *this;
	resultColor *= someColor;
	return resultColor;
}

//===========================================================================//

bool Color::operator ==(const Color & someColor) const
{
	return (Math::Equals(_red, someColor._red) && Math::Equals(_green, someColor._green)
	        && Math::Equals(_blue, someColor._blue) && Math::Equals(_alpha, someColor._alpha));
}

//===========================================================================//

bool Color::operator !=(const Color & someColor) const
{
	return !(*this == someColor);
}

//===========================================================================//

bool Color::operator <(const Color & someColor) const
{
	return !(*this >= someColor);
}

//===========================================================================//

bool Color::operator <=(const Color & someColor) const
{
	if (_red > someColor._red)
	{
		return false;
	}

	if (_green > someColor._green)
	{
		return false;
	}

	if (_blue > someColor._blue)
	{
		return false;
	}

	if (_alpha > someColor._alpha)
	{
		return false;
	}

	return true;
}

//===========================================================================//

bool Color::operator >(const Color & someColor) const
{
	return !(*this <= someColor);
}

//===========================================================================//

bool Color::operator >=(const Color & someColor) const
{
	if (_red < someColor._red)
	{
		return false;
	}

	if (_green < someColor._green)
	{
		return false;
	}

	if (_blue < someColor._blue)
	{
		return false;
	}

	if (_alpha < someColor._alpha)
	{
		return false;
	}

	return true;
}

//===========================================================================//

Color Color::GetInverted() const
{
	return Color(1.0f - _red, 1.0f - _green, 1.0f - _blue, 1.0f - _alpha);
}

//===========================================================================//

Color Color::GetInvertedExceptAlpha() const
{
	return Color(1.0f - _red, 1.0f - _green, 1.0f - _blue, _alpha);
}

//===========================================================================//

void Color::MakeRandomRGB()
{
	_red = Math::Random(1.0f);
	_green = Math::Random(1.0f);
	_blue = Math::Random(1.0f);
}

//===========================================================================//

void Color::MakeRandomRGBA()
{
	MakeRandomRGB();
	_alpha = Math::Random(1.0f);
}

//===========================================================================//

Color Color::RandomRGB()
{
	Color color;
	color.MakeRandomRGB();
	return color;
}

//===========================================================================//

Color Color::RandomRGBA()
{
	Color color;
	color.MakeRandomRGBA();
	return color;
}

//===========================================================================//

void Color::SetRed(float red)
{
	_red = Math::FitInRange(red, 0.0f, 1.0f);
}

//===========================================================================//

float Color::GetRed() const
{
	return _red;
}

//===========================================================================//

integer Color::GetRedByte() const
{
	return _red * 255;
}

//===========================================================================//

void Color::SetGreen(float green)
{
	_green = Math::FitInRange(green, 0.0f, 1.0f);
}

//===========================================================================//

float Color::GetGreen() const
{
	return _green;
}

//===========================================================================//

integer Color::GetGreenByte() const
{
	return _green * 255;
}

//===========================================================================//

void Color::SetBlue(float blue)
{
	_blue = Math::FitInRange(blue, 0.0f, 1.0f);
}

//===========================================================================//

float Color::GetBlue() const
{
	return _blue;
}

//===========================================================================//

integer Color::GetBlueByte() const
{
	return _blue * 255;
}

//===========================================================================//

void Color::SetAlpha(float alpha)
{
	_alpha = Math::FitInRange(alpha, 0.0f, 1.0f);
}

//===========================================================================//

float Color::GetAlpha() const
{
	return _alpha;
}

//===========================================================================//

integer Color::GetAlphaByte() const
{
	return _alpha * 255;
}

//===========================================================================//

void Color::Clear()
{
	_red = 0.0f;
	_green = 0.0f;
	_blue = 0.0f;
	_alpha = 0.0f;
}

//===========================================================================//

void Color::MakeValid()
{
	_red = Math::FitInRange(_red, 0.0f, 1.0f);
	_green = Math::FitInRange(_green, 0.0f, 1.0f);
	_blue = Math::FitInRange(_blue, 0.0f, 1.0f);
	_alpha = Math::FitInRange(_alpha, 0.0f, 1.0f);
}

//===========================================================================//

string Color::ToString() const
{
	return string::Format("{0}, {1}, {2}, {3}", GetRedByte(), GetGreenByte(), GetBlueByte(), GetAlphaByte());
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Rectangle.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

Rectangle::Rectangle() :
	X(0.0f),
	Y(0.0f),
	Width(0.0f),
	Height(0.0f)
{
}

//===========================================================================//

Rectangle::Rectangle(float x, float y, float width, float height) :
	X(x),
	Y(y),
	Width(width),
	Height(height)
{
}

//===========================================================================//

Rectangle::Rectangle(const Rectangle & other) :
	X(other.X),
	Y(other.Y),
	Width(other.Width),
	Height(other.Height)
{
}

//===========================================================================//

Rectangle & Rectangle::operator =(const Rectangle & other)
{
	if (this == &other)
	{
		return *this;
	}

	X = other.X;
	Y = other.Y;
	Width = other.Width;
	Height = other.Height;

	return *this;
}

//===========================================================================//

bool Rectangle::operator ==(const Rectangle & someRectangle) const
{
	return Math::Equals(X, someRectangle.X) && Math::Equals(Y, someRectangle.Y)
	       && Math::Equals(Width, someRectangle.Width) && Math::Equals(Height, someRectangle.Height);
}

//===========================================================================//

bool Rectangle::Contains(const Vector3 & vector) const
{
	return vector.X >= X && vector.X <= Width && vector.Y >= Y && vector.Y <= Height;
}

//===========================================================================//

bool Rectangle::operator !=(const Rectangle & someRectangle) const
{
	return !(*this == someRectangle);
}

//===========================================================================//

Array<Rectangle> Rectangle::SubtractRectangle(const Rectangle & other) const
{
	const float dX = other.X - X;
	const float dY = other.Y - Y;
	const float dWidth = other.Width - Width;
	const float dHeight = other.Height - Height;

	Array<Rectangle> result;
	if (dX > 0)
	{
		result.Add(Rectangle(X, Y, dX, Height));
	}
	
	const bool isChunkingPossible = other.X + other.Width > X && other.X < X + Width;
	const float chunkWidth = Math::Min(X + Width, other.X + other.Width) - Math::Max(X, other.X);
	if (isChunkingPossible && dY > 0)
	{
		result.Add(Rectangle(X + Math::Max(dX, 0.0f), Y, chunkWidth, dY));
	}
	if (isChunkingPossible && other.Y + other.Height < Y + Height)
	{
		result.Add(Rectangle(X + Math::Max(dX, 0.0f), other.Y + other.Height, chunkWidth, -dY - dHeight));
	}
	
	if (X + Width > other.X + other.Width)
	{
		result.Add(Rectangle(Math::Max(other.X + other.Width, X), Y, Math::Min(-dX - dWidth, Width), Height));
	}
	
	return result;
}

//===========================================================================//

}

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс геометрии
//

#pragma once
#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "../Vectors/Vector2.h"

namespace Bru
{

//===========================================================================//

class Geometry
{
public:
	static bool PointInPolygon(const Array<Vector2> & polygonPoints, const Vector2 & point);

private:
	Geometry() = delete;
	Geometry(const Geometry &) = delete;
	~Geometry() = delete;
	Geometry & operator =(const Geometry &) = delete;
};

//===========================================================================//

}// namespace

#endif // GEOMETRY_H

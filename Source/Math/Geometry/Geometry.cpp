//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Geometry.h"

namespace Bru
{

//===========================================================================//

bool Geometry::PointInPolygon(const Array<Vector2> & polygonPoints, const Vector2 & point)
{
	bool result = 0;

	for (integer i = 0, j = polygonPoints.GetCount() - 1; i < polygonPoints.GetCount(); j = i++)
	{
		if ((((polygonPoints[i].Y <= point.Y) && (point.Y < polygonPoints[j].Y)) || ((polygonPoints[j].Y <= point.Y) && (point.Y < polygonPoints[i].Y))) &&
		        (point.X < (polygonPoints[j].X - polygonPoints[i].X) * (point.Y - polygonPoints[i].Y) / (polygonPoints[j].Y - polygonPoints[i].Y) + polygonPoints[i].X))
		{
			result = !result;
		}
	}

	return result;
}

//===========================================================================//

} // namespace Bru

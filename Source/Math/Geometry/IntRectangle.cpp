//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "IntRectangle.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

IntRectangle::IntRectangle() :
	X(0),
	Y(0),
	Width(0),
	Height(0)
{
}

//===========================================================================//

IntRectangle::IntRectangle(integer x, integer y, integer width, integer height) :
	X(x),
	Y(y),
	Width(width),
	Height(height)
{
}

//===========================================================================//

IntRectangle::IntRectangle(const IntRectangle & other) :
	X(other.X),
	Y(other.Y),
	Width(other.Width),
	Height(other.Height)
{
}

//===========================================================================//

IntRectangle & IntRectangle::operator =(const IntRectangle & other)
{
	if (this == &other)
	{
		return *this;
	}

	X = other.X;
	Y = other.Y;
	Width = other.Width;
	Height = other.Height;

	return *this;
}

//===========================================================================//

bool IntRectangle::operator ==(const IntRectangle & someRectangle) const
{
	return X == someRectangle.X && Y == someRectangle.Y &&
	       Width == someRectangle.Width && Height == someRectangle.Height;
}

//===========================================================================//

bool IntRectangle::Contains(const IntVector3 & vector) const
{
	return vector.X >= X && vector.X <= Width && vector.Y >= Y && vector.Y <= Height;
}

//===========================================================================//

bool IntRectangle::operator !=(const IntRectangle & someRectangle) const
{
	return !(*this == someRectangle);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс прямоугольника
//

#pragma once
#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "../../Common/Common.h"
#include "../Vectors/Vector3.h"

namespace Bru
{

//===========================================================================//

struct Rectangle
{
	Rectangle();
	Rectangle(float x, float y, float width, float height);
	Rectangle(const Rectangle & other);

	Rectangle & operator =(const Rectangle & other);

	bool operator ==(const Rectangle & someRectangle) const;
	bool operator !=(const Rectangle & someRectangle) const;

	bool Contains(const Vector3 & vector) const;
	
	Array<Rectangle> SubtractRectangle(const Rectangle & other) const;

	float X;
	float Y;
	float Width;
	float Height;
};

//===========================================================================//

}// namespace

#endif // RECTANGLE_H

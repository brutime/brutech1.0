//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс прямоугольника
//

#pragma once
#ifndef INT_RECTANGLE_H
#define INT_RECTANGLE_H

#include "../../Common/Common.h"
#include "../Vectors/IntVector3.h"

namespace Bru
{

//===========================================================================//

struct IntRectangle
{
	IntRectangle();
	IntRectangle(integer x, integer y, integer width, integer height);
	IntRectangle(const IntRectangle & other);

	IntRectangle & operator =(const IntRectangle & other);

	bool operator ==(const IntRectangle & someRectangle) const;
	bool operator !=(const IntRectangle & someRectangle) const;

	bool Contains(const IntVector3 & vector) const;

	integer X;
	integer Y;
	integer Width;
	integer Height;
};

//===========================================================================//

} // namespace Bru

#endif // RECTANGLE_H

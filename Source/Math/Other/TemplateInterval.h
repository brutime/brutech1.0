//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс интервала
//

#pragma once
#ifndef TEMPLATE_INTERVAL_H
#define TEMPLATE_INTERVAL_H

#include "../../Common/Common.h"
#include "../Math.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class TemplateInterval
{
public:
	TemplateInterval();
	TemplateInterval(T from, T to);
	TemplateInterval(const string & intervalString);
	TemplateInterval(const TemplateInterval<T> & other);
	~TemplateInterval();

	TemplateInterval<T> & operator =(const TemplateInterval<T> & other);

	TemplateInterval<T> & operator +=(const TemplateInterval<T> & interval);
	TemplateInterval<T> & operator -=(const TemplateInterval<T> & interval);

	TemplateInterval<T> operator +(const TemplateInterval<T> & interval) const;
	TemplateInterval<T> operator -(const TemplateInterval<T> & interval) const;

	bool operator ==(const TemplateInterval<T> & interval) const;
	bool operator !=(const TemplateInterval<T> & interval) const;

	bool operator <(const TemplateInterval<T> & interval) const;
	bool operator >(const TemplateInterval<T> & interval) const;
	
	TemplateInterval<T> GetIntersection(const TemplateInterval<T> & interval);
	Array<TemplateInterval<T>> Cut(const TemplateInterval<T> & interval);

	bool Contains(T value) const;
	T GetLength() const;
	T GetRandom() const;

	void Clear();

	string ToString() const;

	//Открытые поля
	T From;
	T To;
};

//===========================================================================//

typedef TemplateInterval<float>	Interval;
typedef TemplateInterval<integer> IntInterval;

} // namespace Bru

#include "TemplateInterval.inl"

#endif // TEMPLATE_INTERVAL_H

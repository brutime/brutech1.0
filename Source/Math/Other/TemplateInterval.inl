//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
TemplateInterval<T>::TemplateInterval() :
	From(0),
	To(0)
{
}

//===========================================================================//

template<typename T>
TemplateInterval<T>::TemplateInterval(T from, T to) :
	From(from),
	To(to)
{
}

//===========================================================================//

template<typename T>
TemplateInterval<T>::TemplateInterval(const string & intervalString) :
	From(0),
	To(0)
{
	TokenParser parser(intervalString, ":");
	if (parser.GetTokensCount() != 2)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 2, but it is {0}", parser.GetTokensCount()));
	}

	From = Convert<string, T>(parser.GetToken(0));
	To = Convert<string, T>(parser.GetToken(1));
}

//===========================================================================//

template<typename T>
TemplateInterval<T>::TemplateInterval(const TemplateInterval<T> & other) :
	From(other.From),
	To(other.To)
{
}

//===========================================================================//

template<typename T>
TemplateInterval<T>::~TemplateInterval()
{
}

//===========================================================================//

template<typename T>
TemplateInterval<T> & TemplateInterval<T>::operator =(const TemplateInterval<T> & other)
{
	if (this == &other)
	{
		return *this;
	}

	From = other.From;
	To = other.To;

	return *this;
}

//===========================================================================//

template<typename T>
TemplateInterval<T> & TemplateInterval<T>::operator +=(const TemplateInterval<T> & interval)
{
	From += interval.From;
	To += interval.To;
	return *this;
}

//===========================================================================//

template<typename T>
TemplateInterval<T> & TemplateInterval<T>::operator -=(const TemplateInterval<T> & interval)
{
	From -= interval.From;
	To -= interval.To;
	return *this;
}

//===========================================================================//

template<typename T>
TemplateInterval<T> TemplateInterval<T>::operator +(const TemplateInterval<T> & interval) const
{
	TemplateInterval resultVector = *this;
	resultVector += interval;
	return resultVector;
}

//===========================================================================//

template<typename T>
TemplateInterval<T> TemplateInterval<T>::operator -(const TemplateInterval<T> & interval) const
{
	TemplateInterval resultVector = *this;
	resultVector -= interval;
	return resultVector;
}

//===========================================================================//

template<typename T>
bool TemplateInterval<T>::operator ==(const TemplateInterval<T> & interval) const
{
	return Math::Equals(From, interval.From) && Math::Equals(To, interval.To);
}

//===========================================================================//

template<typename T>
bool TemplateInterval<T>::operator !=(const TemplateInterval<T> & interval) const
{
	return !(*this == interval);
}

//===========================================================================//

template<typename T>
bool TemplateInterval<T>::operator <(const TemplateInterval<T> & interval) const
{
	return From < interval.From && To < interval.To;
}

//===========================================================================//

template<typename T>
bool TemplateInterval<T>::operator >(const TemplateInterval<T> & interval) const
{
	return From > interval.From && To > interval.To;
}

//===========================================================================//

template<typename T>
TemplateInterval<T> TemplateInterval<T>::GetIntersection(const TemplateInterval<T> & interval)
{
	return TemplateInterval(Math::Max(From, interval.From), Math::Min(To, interval.To));
}

//===========================================================================//

template<typename T>
Array<TemplateInterval<T>> TemplateInterval<T>::Cut(const TemplateInterval<T> & interval)
{
	Array<TemplateInterval<T>> resultIntervals;

	TemplateInterval<T> intersection = GetIntersection(interval);

	if (intersection.To <= intersection.From)
	{
		return resultIntervals;
	}

	TemplateInterval<T> firstInterval = TemplateInterval<T>(From, intersection.From);
	if (!Math::EqualsZero(firstInterval.GetLength()))
	{
		resultIntervals.Add(firstInterval);
	}

	TemplateInterval<T> secondInterval = TemplateInterval<T>(intersection.To, To);
	if (!Math::EqualsZero(secondInterval.GetLength()))
	{
		resultIntervals.Add(secondInterval);
	}

	return resultIntervals;
}

//===========================================================================//

template<typename T>
bool TemplateInterval<T>::Contains(T value) const
{
	return From <= value && value <= To;
}

//===========================================================================//

template<typename T>
T TemplateInterval<T>::GetLength() const
{
	return To - From;
}

//===========================================================================//

template<typename T>
T TemplateInterval<T>::GetRandom() const
{
	return Math::RandomInRange(From, To);
}

//===========================================================================//

template<typename T>
void TemplateInterval<T>::Clear()
{
	From = 0;
	To = 0;
}

//===========================================================================//

template<typename T>
string TemplateInterval<T>::ToString() const
{
	return string::Format("{0}:{1}", From, To);
}

//===========================================================================//

}// namespace Bru

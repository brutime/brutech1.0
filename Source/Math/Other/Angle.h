//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс угла
//

#pragma once
#ifndef ANGLE_H
#define ANGLE_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class Angle
{
public:
	Angle();
	Angle(float angle);
	Angle(const string & angleString);
	Angle(const Angle & other);
	Angle(Angle && other);
	~Angle();

	Angle & operator =(const Angle & other);
	Angle & operator =(Angle && other);

	Angle & operator +=(const Angle & someAngle);
	Angle & operator -=(const Angle & someAngle);
	Angle & operator *=(const Angle & someAngle);
	Angle & operator /=(const Angle & someAngle);

	Angle operator +(const Angle & someAngle) const;
	Angle operator -(const Angle & someAngle) const;
	Angle operator *(const Angle & someAngle) const;
	Angle operator /(const Angle & someAngle) const;

	bool operator ==(const Angle & someAngle) const;
	bool operator !=(const Angle & someAngle) const;

	operator float() const;

	string ToString() const;

private:
	void Normalize();

	float _angle;
};

//===========================================================================//

} // namespace Bru

#endif // ANGLE_H

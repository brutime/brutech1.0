//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
TemplateSize<T>::TemplateSize() :
	Width(0),
	Height(0)
{
}

//===========================================================================//

template<typename T>
TemplateSize<T>::TemplateSize(T width, T height) :
	Width(width),
	Height(height)
{
}

//===========================================================================//

template<typename T>
TemplateSize<T>::TemplateSize(const string & intervalString) :
	Width(0),
	Height(0)
{
	TokenParser parser(intervalString, ",");
	if (parser.GetTokensCount() != 2)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 2, but it is {0}", parser.GetTokensCount()));
	}

	Width = Convert<string, T>(parser.GetToken(0));
	Height = Convert<string, T>(parser.GetToken(1));
}

//===========================================================================//

template<typename T>
TemplateSize<T>::TemplateSize(const TemplateSize<T> & other) :
	Width(other.Width),
	Height(other.Height)
{
}

//===========================================================================//

template<typename T>
TemplateSize<T>::~TemplateSize()
{
}

//===========================================================================//

template<typename T>
TemplateSize<T> & TemplateSize<T>::operator =(const TemplateSize<T> & other)
{
	if (this == &other)
	{
		return *this;
	}

	Width = other.Width;
	Height = other.Height;

	return *this;
}

//===========================================================================//

template<typename T>
bool TemplateSize<T>::operator ==(const TemplateSize<T> & size) const
{
	return Math::Equals(Width, size.Width) && Math::Equals(Height, size.Height);
}

//===========================================================================//

template<typename T>
bool TemplateSize<T>::operator !=(const TemplateSize<T> & someSize) const
{
	return !(*this == someSize);
}

//===========================================================================//

template<typename T>
void TemplateSize<T>::Clear()
{
	Width = 0;
	Height = 0;
}

//===========================================================================//

template<typename T>
string TemplateSize<T>::ToString() const
{
	return string::Format("{0}, {1}", Width, Height);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Angle.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

Angle::Angle() :
	_angle(0.0f)
{
}

//===========================================================================//

Angle::Angle(float angle) :
	_angle(angle)
{
	Normalize();
}

//===========================================================================//

Angle::Angle(const string & angleString) :
	_angle(Converter::ToFloat(angleString))
{

}

//===========================================================================//

Angle::Angle(const Angle & other) :
	_angle(other._angle)
{
}

//===========================================================================//

Angle::Angle(Angle && other) :
	_angle(other._angle)
{
	other._angle = 0.0f;
}

//===========================================================================//

Angle & Angle::operator =(const Angle & other)
{
	if (this == &other)
	{
		return *this;
	}

	_angle = other._angle;

	return *this;
}

//===========================================================================//

Angle & Angle::operator =(Angle && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_angle, other._angle);

	return *this;
}

//===========================================================================//

Angle::~Angle()
{
}

//===========================================================================//

Angle & Angle::operator +=(const Angle & someAngle)
{
	_angle += someAngle;
	Normalize();
	return *this;
}

//===========================================================================//

Angle & Angle::operator -=(const Angle & someAngle)
{
	_angle -= someAngle;
	Normalize();
	return *this;
}

//===========================================================================//

Angle & Angle::operator *=(const Angle & someAngle)
{
	_angle *= someAngle;
	Normalize();
	return *this;
}

//===========================================================================//

Angle & Angle::operator /=(const Angle & someAngle)
{
	_angle /= someAngle;
	Normalize();
	return *this;
}

//===========================================================================//

Angle Angle::operator +(const Angle & someAngle) const
{
	Angle resultAngle = *this;
	resultAngle += someAngle;
	return resultAngle;
}

//===========================================================================//

Angle Angle::operator -(const Angle & someAngle) const
{
	Angle resultAngle = *this;
	resultAngle -= someAngle;
	return resultAngle;
}

//===========================================================================//

Angle Angle::operator *(const Angle & someAngle) const
{
	Angle resultAngle = *this;
	resultAngle *= someAngle;
	return resultAngle;
}

//===========================================================================//

Angle Angle::operator /(const Angle & someAngle) const
{
	Angle resultAngle = *this;
	resultAngle /= someAngle;
	return resultAngle;
}

//===========================================================================//

bool Angle::operator ==(const Angle & someAngle) const
{
	return Math::Equals(_angle, someAngle);
}

//===========================================================================//

bool Angle::operator !=(const Angle & someAngle) const
{
	return !(*this == someAngle);
}

//===========================================================================//

void Angle::Normalize()
{
	if (_angle > 360.f)
	{
		_angle = Math::Mod(_angle, 360.f);
	}

	if (Math::Equals(_angle, 360.f))
	{
		_angle = 0.f;
	}

	if (_angle < 0.f)
	{
		_angle = 360.f + Math::Mod(_angle, 360.f);
	}
}

//===========================================================================//

Angle::operator float() const
{
	return _angle;
}

//===========================================================================//

string Angle::ToString() const
{
	return _angle;
}

//===========================================================================//

} // namespace Bru

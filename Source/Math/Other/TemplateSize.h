//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс интервала
//

#pragma once
#ifndef TEMPLATE_SIZE_H
#define TEMPLATE_SIZE_H

#include "../../Common/Common.h"
#include "../Math.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class TemplateSize
{
public:
	TemplateSize();
	TemplateSize(T width, T height);
	TemplateSize(const string & sizeString);
	TemplateSize(const TemplateSize<T> & other);
	~TemplateSize();

	TemplateSize<T> & operator =(const TemplateSize<T> & other);

	bool operator ==(const TemplateSize<T> & size) const;
	bool operator !=(const TemplateSize<T> & size) const;

	void Clear();

	string ToString() const;

	//Открытые поля
	T Width;
	T Height;
};

//===========================================================================//

} // namespace Bru

#include "TemplateSize.inl"

#endif // TEMPLATE_SIZE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ���������� - ������� �� 0
//

#pragma once
#ifndef DIVISION_BY_ZERO_EXCEPTION_H
#define DIVISION_BY_ZERO_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class DivisionByZeroException : public Exception
{
public:
	DivisionByZeroException(const string & defails, const string & fileName, const string & methodName,
	                        integer fileLine);
	virtual ~DivisionByZeroException();
};

//===========================================================================//

#define DIVISION_BY_ZERO_EXCEPTION(details) \
    throw DivisionByZeroException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // DIVISION_BY_ZERO_EXCEPTION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Vector3.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

Vector3::Vector3() :
	X(0.0f),
	Y(0.0f),
	Z(0.0f)
{
	Clear();
}

//===========================================================================//

Vector3::Vector3(float x, float y, float z) :
	X(x),
	Y(y),
	Z(z)
{
}

//===========================================================================//

Vector3::Vector3(const string & vectorString) :
	X(0.0f),
	Y(0.0f),
	Z(0.0f)
{
	TokenParser parser(vectorString);
	if (parser.GetTokensCount() != 2 && parser.GetTokensCount() != 3)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 2 or 3, but it is {0}", parser.GetTokensCount()));
	}

	X = Converter::ToFloat(parser.GetToken(0));
	Y = Converter::ToFloat(parser.GetToken(1));
	Z = parser.GetTokensCount() == 3 ? Converter::ToFloat(parser.GetToken(2)) : 0.0f;
}

//===========================================================================//

Vector3::Vector3(const Vector2 & other) :
	X(other.X),
	Y(other.Y),
	Z(0.0f)
{
}

//===========================================================================//

Vector3::Vector3(const Vector3 & other) :
	X(other.X),
	Y(other.Y),
	Z(other.Z)
{
}

//===========================================================================//

Vector3::Vector3(Vector2 && other) :
	X(other.X),
	Y(other.Y),
	Z(0.0f)
{
	other.X = 0.0f;
	other.Y = 0.0f;
}

//===========================================================================//

Vector3::Vector3(Vector3 && other) :
	X(other.X),
	Y(other.Y),
	Z(other.Z)
{
	other.X = 0.0f;
	other.Y = 0.0f;
	other.Z = 0.0f;
}

//===========================================================================//

Vector3::~Vector3()
{
}

//===========================================================================//

Vector3 & Vector3::operator =(const Vector2 & other)
{
	X = other.X;
	Y = other.Y;
	Z = 0.0f;

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator =(const Vector3 & other)
{
	if (this == &other)
	{
		return *this;
	}

	X = other.X;
	Y = other.Y;
	Z = other.Z;

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator =(Vector2 && other)
{
	std::swap(X, other.X);
	std::swap(Y, other.Y);
	Z = 0.0f;

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator =(Vector3 && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(X, other.X);
	std::swap(Y, other.Y);
	std::swap(Z, other.Z);

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator +=(const Vector3 & vector)
{
	X += vector.X;
	Y += vector.Y;
	Z += vector.Z;

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator -=(const Vector3 & vector)
{
	X -= vector.X;
	Y -= vector.Y;
	Z -= vector.Z;

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator *=(const Vector3 & vector)
{
	X *= vector.X;
	Y *= vector.Y;
	Z *= vector.Z;

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator *=(float factor)
{
	X *= factor;
	Y *= factor;
	Z *= factor;

	return *this;
}

//===========================================================================//

Vector3 & Vector3::operator /=(const Vector3 & vector)
{
	X /= vector.X;
	Y /= vector.Y;
	Z /= vector.Z;

	return *this;
}

//===========================================================================//

Vector3 Vector3::operator +(const Vector3 & vector) const
{
	Vector3 resultVector = *this;
	resultVector += vector;
	return resultVector;
}

//===========================================================================//

Vector3 Vector3::operator -(const Vector3 & vector) const
{
	Vector3 resultVector = *this;
	resultVector -= vector;
	return resultVector;
}

//===========================================================================//

Vector3 Vector3::operator *(const Vector3 & vector) const
{
	Vector3 resultVector = *this;
	resultVector *= vector;
	return resultVector;
}

//===========================================================================//

Vector3 Vector3::operator *(float factor) const
{
	Vector3 resultVector = *this;
	resultVector *= factor;
	return resultVector;
}

//===========================================================================//

Vector3 Vector3::operator /(const Vector3 & vector) const
{
	Vector3 resultVector = *this;
	resultVector /= vector;
	return resultVector;
}

//===========================================================================//

bool Vector3::operator ==(const Vector3 & vector) const
{
	return Math::Equals(X, vector.X) && Math::Equals(Y, vector.Y) && Math::Equals(Z, vector.Z);
}

//===========================================================================//

bool Vector3::operator !=(const Vector3 & vector) const
{
	return !(*this == vector);
}

//===========================================================================//

Vector3 Vector3::operator -() const
{
	return Vector3(-X, -Y, -Z);
}

//===========================================================================//

bool Vector3::Equals(const Vector3 & vector, float accuracy) const
{
	return Math::Equals(X, vector.X, accuracy) && Math::Equals(Y, vector.Y, accuracy)
	       && Math::Equals(Z, vector.Z, accuracy);
}

//===========================================================================//

Vector3 Vector3::MultiplyByMatrix4x4AsPoint(const Matrix4x4 & matrix) const
{
	const float * m = matrix.GetElements().GetData();

	return Vector3(X * m[0] + Y * m[4] + Z * m[8] + m[12],
	               X * m[1] + Y * m[5] + Z * m[9] + m[13],
	               X * m[2] + Y * m[6] + Z * m[10] + m[14]);
}

//===========================================================================//

Vector3 Vector3::MultiplyByMatrix4x4AsVector(const Matrix4x4 & matrix) const
{
	const float * m = matrix.GetElements().GetData();

	return Vector3(X * m[0] + Y * m[4] + Z * m[8],
	               X * m[1] + Y * m[5] + Z * m[9],
	               X * m[2] + Y * m[6] + Z * m[10]);
}

//===========================================================================//

void Vector3::Clear()
{
	X = 0.0f;
	Y = 0.0f;
	Z = 0.0f;
}

//===========================================================================//

float Vector3::Length() const
{
	return Math::Sqrt(X * X + Y * Y + Z * Z);
}

//===========================================================================//

void Vector3::Normalize()
{
	float vectorLength = Length();

	if (vectorLength > Math::ZeroTolerance)
	{
		float invertLength = 1.0f / vectorLength;

		X *= invertLength;
		Y *= invertLength;
		Z *= invertLength;
	}
}

//===========================================================================//

float Vector3::DistanceTo(const Vector3 & vector) const
{
	return Math::Sqrt(Math::Sqr(X - vector.X) + Math::Sqr(Y - vector.Y) + Math::Sqr(Z - vector.Z));
}

//===========================================================================//

float Vector3::Dot(const Vector3 & vector) const
{
	return X * vector.X + Y * vector.Y + Z * vector.Z;
}

//===========================================================================//

Vector3 Vector3::Cross(const Vector3 & vector) const
{
	Vector3 resultVector(Y * vector.Z - Z * vector.Y, Z * vector.X - X * vector.Z, X * vector.Y - Y * vector.X);
	return resultVector;
}

//===========================================================================//

void Vector3::Negative()
{
	X = -X;
	Y = -Y;
	Z = -Z;
}

//===========================================================================//

void Vector3::NegativeX()
{
	X = -X;
}

//===========================================================================//

void Vector3::NegativeY()
{
	Y = -Y;
}

//===========================================================================//

void Vector3::NegativeZ()
{
	Z = -Z;
}

//===========================================================================//

string Vector3::ToString() const
{
	return string::Format("{0:.4}, {1:.4}, {2:.4}", X, Y, Z);
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс вектора, состоящего из 4-ех компонентов
//

#pragma once
#ifndef VECTOR4_H
#define VECTOR4_H

#include "../../Common/Common.h"
#include "../Matrices/Matrix4x4.h"

namespace Bru
{

//===========================================================================//

class Vector4
{
public:
	Vector4();
	Vector4(float x, float y, float z, float w);
	Vector4(const string & vectorString);
	Vector4(const Vector4 & other);
	Vector4(Vector4 && other);
	~Vector4();

	Vector4 & operator =(const Vector4 & other);
	Vector4 & operator =(Vector4 && other);

	Vector4 & operator +=(const Vector4 & vector);
	Vector4 & operator -=(const Vector4 & vector);
	Vector4 & operator *=(const Vector4 & vector);
	Vector4 & operator *=(float factor);
	Vector4 & operator *=(const Matrix4x4 & matrix);
	Vector4 & operator /=(const Vector4 & vector);

	Vector4 operator +(const Vector4 & vector) const;
	Vector4 operator -(const Vector4 & vector) const;
	Vector4 operator *(const Vector4 & vector) const;
	Vector4 operator *(float factor) const;
	Vector4 operator *(const Matrix4x4 & matrix) const;
	Vector4 operator /(const Vector4 & vector) const;

	///Склярное произведение
	float Dot(const Vector4 & vector) const;

	bool operator ==(const Vector4 & vector) const;
	bool operator !=(const Vector4 & vector) const;

	Vector4 operator -() const;

	void Clear();

	float Length() const;
	void Normalize();
	float DistanceTo(const Vector4 & vector) const;

	void Negative();
	void NegativeX();
	void NegativeY();
	void NegativeZ();
	void NegativeW();

	string ToString() const;

	//Открытые поля
	float X;
	float Y;
	float Z;
	float W;
};

//===========================================================================//

}//namespace Bru

#endif // VECTOR4_H

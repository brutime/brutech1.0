//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Vector2.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

Vector2::Vector2() :
	X(0.0f),
	Y(0.0f)
{
}

//===========================================================================//

Vector2::Vector2(float x, float y) :
	X(x),
	Y(y)
{
}

//===========================================================================//

Vector2::Vector2(const string & vectorString) :
	X(0.0f),
	Y(0.0f)
{
	TokenParser parser(vectorString);
	if (parser.GetTokensCount() != 2)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 2, but it is {0}", parser.GetTokensCount()));
	}

	X = Converter::ToFloat(parser.GetToken(0));
	Y = Converter::ToFloat(parser.GetToken(1));
}

//===========================================================================//

Vector2::Vector2(const Vector2 & other) :
	X(other.X),
	Y(other.Y)
{
}

//===========================================================================//

Vector2::Vector2(Vector2 && other) :
	X(other.X),
	Y(other.Y)
{
	other.X = 0.0f;
	other.Y = 0.0f;
}

//===========================================================================//

Vector2::~Vector2()
{
}

//===========================================================================//

Vector2 & Vector2::operator =(const Vector2 & other)
{
	if (this == &other)
	{
		return *this;
	}

	X = other.X;
	Y = other.Y;

	return *this;
}

//===========================================================================//

Vector2 & Vector2::operator =(Vector2 && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(X, other.X);
	std::swap(Y, other.Y);

	return *this;
}

//===========================================================================//

Vector2 & Vector2::operator +=(const Vector2 & vector)
{
	X += vector.X;
	Y += vector.Y;

	return *this;
}

//===========================================================================//

Vector2 & Vector2::operator -=(const Vector2 & vector)
{
	X -= vector.X;
	Y -= vector.Y;

	return *this;
}

//===========================================================================//

Vector2 & Vector2::operator *=(const Vector2 & vector)
{
	X *= vector.X;
	Y *= vector.Y;

	return *this;
}

//===========================================================================//

Vector2 & Vector2::operator *=(float factor)
{
	X *= factor;
	Y *= factor;

	return *this;
}

//===========================================================================//

Vector2 & Vector2::operator /=(const Vector2 & vector)
{
	X /= vector.X;
	Y /= vector.Y;

	return *this;
}

//===========================================================================//

Vector2 Vector2::operator +(const Vector2 & vector) const
{
	Vector2 resultVector = *this;
	resultVector += vector;
	return resultVector;
}

//===========================================================================//

Vector2 Vector2::operator -(const Vector2 & vector) const
{
	Vector2 resultVector = *this;
	resultVector -= vector;
	return resultVector;
}

//===========================================================================//

Vector2 Vector2::operator *(const Vector2 & vector) const
{
	Vector2 resultVector = *this;
	resultVector *= vector;
	return resultVector;
}

//===========================================================================//

Vector2 Vector2::operator *(float factor) const
{
	Vector2 resultVector = *this;
	resultVector *= factor;
	return resultVector;
}

//===========================================================================//

Vector2 Vector2::operator /(const Vector2 & vector) const
{
	Vector2 resultVector = *this;
	resultVector /= vector;
	return resultVector;
}

//===========================================================================//

bool Vector2::operator ==(const Vector2 & vector) const
{
	return Math::Equals(X, vector.X) && Math::Equals(Y, vector.Y);
}

//===========================================================================//

bool Vector2::operator !=(const Vector2 & vector) const
{
	return !(*this == vector);
}

//===========================================================================//

Vector2 Vector2::operator -() const
{
	return Vector2(-X, -Y);
}

//===========================================================================//

void Vector2::Clear()
{
	X = 0.0f;
	Y = 0.0f;
}

//===========================================================================//

float Vector2::Length() const
{
	return Math::Sqrt(X * X + Y * Y);
}

//===========================================================================//

void Vector2::Normalize()
{
	float vectorLength = Length();

	if (vectorLength > Math::ZeroTolerance)
	{
		float invertLength = 1.0f / vectorLength;

		X *= invertLength;
		Y *= invertLength;
	}
}

//===========================================================================//

float Vector2::DistanceTo(const Vector2 & vector) const
{
	return Math::Sqrt(Math::Sqr(X - vector.X) + Math::Sqr(Y - vector.Y));
}

//===========================================================================//

float Vector2::Dot(const Vector2 & vector) const
{
	return X * vector.X + Y * vector.Y;
}

//===========================================================================//

void Vector2::Negative()
{
	X = -X;
	Y = -Y;
}

//===========================================================================//

void Vector2::NegativeX()
{
	X = -X;
}

//===========================================================================//

void Vector2::NegativeY()
{
	Y = -Y;
}

//===========================================================================//

string Vector2::ToString() const
{
	return string::Format("{0:.4}, {1:.4}", X, Y);
}

//===========================================================================//

}//namespace

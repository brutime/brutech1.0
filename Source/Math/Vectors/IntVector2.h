//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс вектора, состоящего из 2-ух компонентов целого типа
//

#pragma once
#ifndef INT_VECTOR_2_H
#define INT_VECTOR_2_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class IntVector2
{
public:
	IntVector2();
	IntVector2(integer x, integer y);
	IntVector2(const string & vectorString);
	IntVector2(const IntVector2 & other);
	IntVector2(IntVector2 && other);

	~IntVector2();

	IntVector2 & operator =(const IntVector2 & other);
	IntVector2 & operator =(IntVector2 && other);

	IntVector2 & operator +=(const IntVector2 & vector);
	IntVector2 & operator -=(const IntVector2 & vector);
	IntVector2 & operator *=(const IntVector2 & vector);
	IntVector2 & operator *=(integer factor);
	IntVector2 & operator /=(const IntVector2 & vector);

	IntVector2 operator +(const IntVector2 & vector) const;
	IntVector2 operator -(const IntVector2 & vector) const;
	IntVector2 operator *(const IntVector2 & vector) const;
	IntVector2 operator *(integer factor) const;
	IntVector2 operator /(const IntVector2 & vector) const;

	bool operator ==(const IntVector2 & vector) const;
	bool operator !=(const IntVector2 & vector) const;

	IntVector2 operator -() const;

	void Clear();
	
	float DistanceTo(const IntVector2 & vector) const;

	void Negative();
	void NegativeX();
	void NegativeY();

	string ToString() const;

	//Открытые поля
	integer X;
	integer Y;
};

//===========================================================================//

}//namespace Bru

#endif // INT_VECTOR_2_H

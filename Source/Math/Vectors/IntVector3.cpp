//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "IntVector3.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

IntVector3::IntVector3() :
	X(0),
	Y(0),
	Z(0)
{
	Clear();
}

//===========================================================================//

IntVector3::IntVector3(integer x, integer y, integer z) :
	X(x),
	Y(y),
	Z(z)
{
}

//===========================================================================//

IntVector3::IntVector3(const string & vectorString) :
	X(0),
	Y(0),
	Z(0)
{
	TokenParser parser(vectorString);
	if (parser.GetTokensCount() != 3)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 3, but it is {0}", parser.GetTokensCount()));
	}

	X = Converter::ToInteger(parser.GetToken(0));
	Y = Converter::ToInteger(parser.GetToken(1));
	Z = Converter::ToInteger(parser.GetToken(2));
}

//===========================================================================//

IntVector3::IntVector3(const IntVector3 & other) :
	X(other.X),
	Y(other.Y),
	Z(other.Z)
{
}

//===========================================================================//

IntVector3::IntVector3(IntVector3 && other) :
	X(other.X),
	Y(other.Y),
	Z(other.Z)
{
	other.X = 0;
	other.Y = 0;
	other.Z = 0;
}

//===========================================================================//

IntVector3::~IntVector3()
{
}

//===========================================================================//

IntVector3 & IntVector3::operator =(const IntVector3 & other)
{
	if (this == &other)
	{
		return *this;
	}

	X = other.X;
	Y = other.Y;
	Z = other.Z;

	return *this;
}

//===========================================================================//

IntVector3 & IntVector3::operator =(IntVector3 && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(X, other.X);
	std::swap(Y, other.Y);
	std::swap(Z, other.Z);

	return *this;
}

//===========================================================================//

IntVector3 & IntVector3::operator +=(const IntVector3 & vector)
{
	X += vector.X;
	Y += vector.Y;
	Z += vector.Z;

	return *this;
}

//===========================================================================//

IntVector3 & IntVector3::operator -=(const IntVector3 & vector)
{
	X -= vector.X;
	Y -= vector.Y;
	Z -= vector.Z;

	return *this;
}

//===========================================================================//

IntVector3 & IntVector3::operator *=(const IntVector3 & vector)
{
	X *= vector.X;
	Y *= vector.Y;
	Z *= vector.Z;

	return *this;
}

//===========================================================================//

IntVector3 & IntVector3::operator *=(integer factor)
{
	X *= factor;
	Y *= factor;
	Z *= factor;

	return *this;
}

//===========================================================================//

IntVector3 & IntVector3::operator /=(const IntVector3 & vector)
{
	X /= vector.X;
	Y /= vector.Y;
	Z /= vector.Z;

	return *this;
}

//===========================================================================//

IntVector3 IntVector3::operator +(const IntVector3 & vector) const
{
	IntVector3 resultVector = *this;
	resultVector += vector;
	return resultVector;
}

//===========================================================================//

IntVector3 IntVector3::operator -(const IntVector3 & vector) const
{
	IntVector3 resultVector = *this;
	resultVector -= vector;
	return resultVector;
}

//===========================================================================//

IntVector3 IntVector3::operator *(const IntVector3 & vector) const
{
	IntVector3 resultVector = *this;
	resultVector *= vector;
	return resultVector;
}

//===========================================================================//

IntVector3 IntVector3::operator *(integer factor) const
{
	IntVector3 resultVector = *this;
	resultVector *= factor;
	return resultVector;
}

//===========================================================================//

IntVector3 IntVector3::operator /(const IntVector3 & vector) const
{
	IntVector3 resultVector = *this;
	resultVector /= vector;
	return resultVector;
}

//===========================================================================//

bool IntVector3::operator ==(const IntVector3 & vector) const
{
	return X == vector.X && Y == vector.Y && Z == vector.Z;
}

//===========================================================================//

bool IntVector3::operator !=(const IntVector3 & vector) const
{
	return !(*this == vector);
}

//===========================================================================//

IntVector3 IntVector3::operator -() const
{
	return IntVector3(-X, -Y, -Z);
}

//===========================================================================//

void IntVector3::Clear()
{
	X = 0;
	Y = 0;
	Z = 0;
}

//===========================================================================//

float IntVector3::DistanceTo(const IntVector3 & vector) const
{
	return Math::Sqrt(Math::Sqr(X - vector.X) + Math::Sqr(Y - vector.Y) + Math::Sqr(Z - vector.Z));
}

//===========================================================================//

void IntVector3::Negative()
{
	X = -X;
	Y = -Y;
	Z = -Z;
}

//===========================================================================//

void IntVector3::NegativeX()
{
	X = -X;
}

//===========================================================================//

void IntVector3::NegativeY()
{
	Y = -Y;
}

//===========================================================================//

void IntVector3::NegativeZ()
{
	Z = -Z;
}

//===========================================================================//

string IntVector3::ToString() const
{
	return string::Format("{0}, {1}, {2}", X, Y, Z);
}

//===========================================================================//

}//namespace

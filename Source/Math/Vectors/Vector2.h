//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс вектора, состоящего из 2-ух компонентов
//

#pragma once
#ifndef VECTOR_2_H
#define VECTOR_2_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class Vector2
{
public:
	Vector2();
	Vector2(float x, float y);
	Vector2(const string & vectorString);
	Vector2(const Vector2 & other);
	Vector2(Vector2 && other);
	~Vector2();

	Vector2 & operator =(const Vector2 & other);
	Vector2 & operator =(Vector2 && other);

	Vector2 & operator +=(const Vector2 & vector);
	Vector2 & operator -=(const Vector2 & vector);
	Vector2 & operator *=(const Vector2 & vector);
	Vector2 & operator *=(float factor);
	Vector2 & operator /=(const Vector2 & vector);

	Vector2 operator +(const Vector2 & vector) const;
	Vector2 operator -(const Vector2 & vector) const;
	Vector2 operator *(const Vector2 & vector) const;
	Vector2 operator *(float factor) const;
	Vector2 operator /(const Vector2 & vector) const;

	bool operator ==(const Vector2 & vector) const;
	bool operator !=(const Vector2 & vector) const;

	Vector2 operator -() const;

	void Clear();

	float Length() const;
	void Normalize();
	float DistanceTo(const Vector2 & vector) const;

	///Склярное произведение
	float Dot(const Vector2 & vector) const;

	void Negative();
	void NegativeX();
	void NegativeY();

	string ToString() const;

	//Открытые поля
	float X;
	float Y;
};

//===========================================================================//

}//namespace Bru

#endif // VECTOR_2_H

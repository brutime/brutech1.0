//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс вектора, состоящего из 3-ех компонентов
//

#pragma once
#ifndef VECTOR3_H
#define VECTOR3_H

#include "../../Common/Common.h"
#include "../Matrices/Matrix4x4.h"
#include "Vector2.h"

namespace Bru
{

//===========================================================================//

class Vector3
{
public:
	Vector3();
	Vector3(float x, float y, float z = 0.0f);
	Vector3(const string & vectorString);
	Vector3(const Vector2 & other);
	Vector3(const Vector3 & other);
	Vector3(Vector2 && other);
	Vector3(Vector3 && other);
	~Vector3();

	Vector3 & operator =(const Vector2 & other);
	Vector3 & operator =(const Vector3 & other);

	Vector3 & operator =(Vector2 && other);
	Vector3 & operator =(Vector3 && other);

	Vector3 & operator +=(const Vector3 & vector);
	Vector3 & operator -=(const Vector3 & vector);
	Vector3 & operator *=(const Vector3 & vector);
	Vector3 & operator *=(float factor);
	Vector3 & operator /=(const Vector3 & vector);

	Vector3 operator +(const Vector3 & vector) const;
	Vector3 operator -(const Vector3 & vector) const;
	Vector3 operator *(const Vector3 & vector) const;
	Vector3 operator *(float factor) const;
	Vector3 operator /(const Vector3 & vector) const;

	bool operator ==(const Vector3 & vector) const;
	bool operator !=(const Vector3 & vector) const;

	Vector3 operator -() const;

	bool Equals(const Vector3 & vector, float accuracy) const;

	///Разница в трактовке - в 4й координате, которая приписывается к 3м координатам вектора
	///для точки w = 1, для вектора w = 0
	Vector3 MultiplyByMatrix4x4AsPoint(const Matrix4x4 & matrix) const;
	Vector3 MultiplyByMatrix4x4AsVector(const Matrix4x4 & matrix) const;

	void Clear();

	float Length() const;
	void Normalize();
	///Скалярное расстояние
	float DistanceTo(const Vector3 & vector) const;
	///Склярное произведение
	float Dot(const Vector3 & vector) const;
	///Векторное произведение
	Vector3 Cross(const Vector3 & vector) const;

	void Negative();
	void NegativeX();
	void NegativeY();
	void NegativeZ();

	string ToString() const;

	//Открытые поля
	float X;
	float Y;
	float Z;
};

//===========================================================================//

}//namespace Bru

#endif // VECTOR3_H

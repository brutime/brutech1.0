//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "IntVector2.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

IntVector2::IntVector2() :
	X(0),
	Y(0)
{
	Clear();
}

//===========================================================================//

IntVector2::IntVector2(integer x, integer y) :
	X(x),
	Y(y)
{
}

//===========================================================================//

IntVector2::IntVector2(const string & vectorString) :
	X(0),
	Y(0)
{
	TokenParser parser(vectorString);
	if (parser.GetTokensCount() != 2)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 2, but it is {0}", parser.GetTokensCount()));
	}

	X = Converter::ToInteger(parser.GetToken(0));
	Y = Converter::ToInteger(parser.GetToken(1));
}

//===========================================================================//

IntVector2::IntVector2(const IntVector2 & other) :
	X(other.X),
	Y(other.Y)
{
}

//===========================================================================//

IntVector2::IntVector2(IntVector2 && other) :
	X(other.X),
	Y(other.Y)
{
	other.X = 0;
	other.Y = 0;
}

//===========================================================================//

IntVector2::~IntVector2()
{
}

//===========================================================================//

IntVector2 & IntVector2::operator =(const IntVector2 & other)
{
	if (this == &other)
	{
		return *this;
	}

	X = other.X;
	Y = other.Y;

	return *this;
}

//===========================================================================//

IntVector2 & IntVector2::operator =(IntVector2 && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(X, other.X);
	std::swap(Y, other.Y);

	return *this;
}

//===========================================================================//

IntVector2 & IntVector2::operator +=(const IntVector2 & vector)
{
	X += vector.X;
	Y += vector.Y;

	return *this;
}

//===========================================================================//

IntVector2 & IntVector2::operator -=(const IntVector2 & vector)
{
	X -= vector.X;
	Y -= vector.Y;

	return *this;
}

//===========================================================================//

IntVector2 & IntVector2::operator *=(const IntVector2 & vector)
{
	X *= vector.X;
	Y *= vector.Y;

	return *this;
}

//===========================================================================//

IntVector2 & IntVector2::operator *=(integer factor)
{
	X *= factor;
	Y *= factor;

	return *this;
}

//===========================================================================//

IntVector2 & IntVector2::operator /=(const IntVector2 & vector)
{
	X /= vector.X;
	Y /= vector.Y;

	return *this;
}

//===========================================================================//

IntVector2 IntVector2::operator +(const IntVector2 & vector) const
{
	IntVector2 resultVector = *this;
	resultVector += vector;
	return resultVector;
}

//===========================================================================//

IntVector2 IntVector2::operator -(const IntVector2 & vector) const
{
	IntVector2 resultVector = *this;
	resultVector -= vector;
	return resultVector;
}

//===========================================================================//

IntVector2 IntVector2::operator *(const IntVector2 & vector) const
{
	IntVector2 resultVector = *this;
	resultVector *= vector;
	return resultVector;
}

//===========================================================================//

IntVector2 IntVector2::operator *(integer factor) const
{
	IntVector2 resultVector = *this;
	resultVector *= factor;
	return resultVector;
}

//===========================================================================//

IntVector2 IntVector2::operator /(const IntVector2 & vector) const
{
	IntVector2 resultVector = *this;
	resultVector /= vector;
	return resultVector;
}

//===========================================================================//

bool IntVector2::operator ==(const IntVector2 & vector) const
{
	return X == vector.X && Y == vector.Y;
}

//===========================================================================//

bool IntVector2::operator !=(const IntVector2 & vector) const
{
	return !(*this == vector);
}

//===========================================================================//

IntVector2 IntVector2::operator -() const
{
	return IntVector2(-X, -Y);
}

//===========================================================================//

void IntVector2::Clear()
{
	X = 0;
	Y = 0;
}

//===========================================================================//

float IntVector2::DistanceTo(const IntVector2 & vector) const
{
	return Math::Sqrt(Math::Sqr(X - vector.X) + Math::Sqr(Y - vector.Y));
}

//===========================================================================//

void IntVector2::Negative()
{
	X = -X;
	Y = -Y;
}

//===========================================================================//

void IntVector2::NegativeX()
{
	X = -X;
}

//===========================================================================//

void IntVector2::NegativeY()
{
	Y = -Y;
}

//===========================================================================//

string IntVector2::ToString() const
{
	return string::Format("{0}, {1}", X, Y);
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Vector4.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

Vector4::Vector4() :
	X(0.0f),
	Y(0.0f),
	Z(0.0f),
	W(0.0f)
{
}

//===========================================================================//

Vector4::Vector4(float x, float y, float z, float w) :
	X(x),
	Y(y),
	Z(z),
	W(w)
{
}

//===========================================================================//

Vector4::Vector4(const string & vectorString) :
	X(0.0f),
	Y(0.0f),
	Z(0.0f),
	W(0.0f)
{
	TokenParser parser(vectorString);
	if (parser.GetTokensCount() != 4)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Tokens count must be 4, but it is {0}", parser.GetTokensCount()));
	}

	X = Converter::ToFloat(parser.GetToken(0));
	Y = Converter::ToFloat(parser.GetToken(1));
	Z = Converter::ToFloat(parser.GetToken(2));
	W = Converter::ToFloat(parser.GetToken(3));
}

//===========================================================================//

Vector4::Vector4(const Vector4 & other) :
	X(other.X),
	Y(other.Y),
	Z(other.Z),
	W(other.W)
{
}

//===========================================================================//

Vector4::Vector4(Vector4 && other) :
	X(other.X),
	Y(other.Y),
	Z(other.Z),
	W(other.W)
{
	other.X = 0.0f;
	other.Y = 0.0f;
	other.Z = 0.0f;
	other.W = 0.0f;
}

//===========================================================================//

Vector4::~Vector4()
{
}

//===========================================================================//

Vector4 & Vector4::operator =(const Vector4 & other)
{
	if (this == &other)
	{
		return *this;
	}

	X = other.X;
	Y = other.Y;
	Z = other.Z;
	W = other.W;

	return *this;
}

//===========================================================================//

Vector4 & Vector4::operator =(Vector4 && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(X, other.X);
	std::swap(Y, other.Y);
	std::swap(Z, other.Z);
	std::swap(W, other.W);

	return *this;
}

//===========================================================================//

Vector4 & Vector4::operator +=(const Vector4 & vector)
{
	X += vector.X;
	Y += vector.Y;
	Z += vector.Z;
	W += vector.W;

	return *this;
}

//===========================================================================//

Vector4 & Vector4::operator -=(const Vector4 & vector)
{
	X -= vector.X;
	Y -= vector.Y;
	Z -= vector.Z;
	W -= vector.W;

	return *this;
}

//===========================================================================//

Vector4 & Vector4::operator *=(const Vector4 & vector)
{
	X *= vector.X;
	Y *= vector.Y;
	Z *= vector.Z;
	W *= vector.W;

	return *this;
}

//===========================================================================//

Vector4 & Vector4::operator *=(float factor)
{
	X *= factor;
	Y *= factor;
	Z *= factor;
	W *= factor;

	return *this;
}

//===========================================================================//

Vector4 & Vector4::operator *=(const Matrix4x4 & matrix)
{
	*this = Vector4(X * matrix[0] + Y * matrix[4] + Z * matrix[8] + W * matrix[12],
	                X * matrix[1] + Y * matrix[5] + Z * matrix[9] + W * matrix[13],
	                X * matrix[2] + Y * matrix[6] + Z * matrix[10] + W * matrix[14],
	                X * matrix[3] + Y * matrix[7] + Z * matrix[11] + W * matrix[15]);

	return *this;
}

//===========================================================================//

Vector4 & Vector4::operator /=(const Vector4 & vector)
{
	X /= vector.X;
	Y /= vector.Y;
	Z /= vector.Z;
	W /= vector.W;

	return *this;
}

//===========================================================================//

Vector4 Vector4::operator +(const Vector4 & vector) const
{
	Vector4 resultVector = *this;
	resultVector += vector;
	return resultVector;
}

//===========================================================================//

Vector4 Vector4::operator -(const Vector4 & vector) const
{
	Vector4 resultVector = *this;
	resultVector -= vector;
	return resultVector;
}

//===========================================================================//

Vector4 Vector4::operator *(const Vector4 & vector) const
{
	Vector4 resultVector = *this;
	resultVector *= vector;
	return resultVector;
}

//===========================================================================//

Vector4 Vector4::operator *(float factor) const
{
	Vector4 resultVector = *this;
	resultVector *= factor;
	return resultVector;
}

//===========================================================================//

Vector4 Vector4::operator *(const Matrix4x4 & matrix) const
{
	Vector4 resultVector = *this;
	resultVector *= matrix;
	return resultVector;
}

//===========================================================================//

Vector4 Vector4::operator /(const Vector4 & vector) const
{
	Vector4 resultVector = *this;
	resultVector /= vector;
	return resultVector;
}

//===========================================================================//

float Vector4::Dot(const Vector4 & vector) const
{
	return X * vector.X + Y * vector.Y + Z * vector.Z + W * vector.W;
}

//===========================================================================//

bool Vector4::operator ==(const Vector4 & vector) const
{
	return Math::Equals(X, vector.X) && Math::Equals(Y, vector.Y) && Math::Equals(Z, vector.Z)
	       && Math::Equals(W, vector.W);
}

//===========================================================================//

bool Vector4::operator !=(const Vector4 & vector) const
{
	return !(*this == vector);
}

//===========================================================================//

Vector4 Vector4::operator -() const
{
	return Vector4(-X, -Y, -Z, -W);
}

//===========================================================================//

void Vector4::Clear()
{
	X = 0.0f;
	Y = 0.0f;
	Z = 0.0f;
	W = 0.0f;
}

//===========================================================================//

float Vector4::Length() const
{
	return Math::Sqrt(X * X + Y * Y + Z * Z + W * W);
}

//===========================================================================//

void Vector4::Normalize()
{
	float vectorLength = Length();

	if (vectorLength > Math::ZeroTolerance)
	{
		float invertLength = 1.0f / vectorLength;

		X *= invertLength;
		Y *= invertLength;
		Z *= invertLength;
		W *= invertLength;
	}
}

//===========================================================================//

float Vector4::DistanceTo(const Vector4 & vector) const
{
	return Math::Sqrt(
	           Math::Sqr(X - vector.X) + Math::Sqr(Y - vector.Y) + Math::Sqr(Z - vector.Z) + Math::Sqr(W - vector.W));
}

//===========================================================================//

void Vector4::Negative()
{
	X = -X;
	Y = -Y;
	Z = -Z;
	W = -W;
}

//===========================================================================//

void Vector4::NegativeX()
{
	X = -X;
}

//===========================================================================//

void Vector4::NegativeY()
{
	Y = -Y;
}

//===========================================================================//

void Vector4::NegativeZ()
{
	Z = -Z;
}

//===========================================================================//

void Vector4::NegativeW()
{
	W = -W;
}

//===========================================================================//

string Vector4::ToString() const
{
	return string::Format("{0:.4}, {1:.4}, {2:.4}, {3:.4}", X, Y, Z, W);
}

//===========================================================================//

}//namespace

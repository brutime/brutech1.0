//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс вектора, состоящего из 3-ех компонентов целого типа
//

#pragma once
#ifndef INT_VECTOR3_H
#define INT_VECTOR3_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class IntVector3
{
public:
	IntVector3();
	IntVector3(integer x, integer y, integer z);
	IntVector3(const string & vectorString);
	IntVector3(const IntVector3 & other);
	IntVector3(IntVector3 && other);
	~IntVector3();

	IntVector3 & operator =(const IntVector3 & other);
	IntVector3 & operator =(IntVector3 && other);

	IntVector3 & operator +=(const IntVector3 & vector);
	IntVector3 & operator -=(const IntVector3 & vector);
	IntVector3 & operator *=(const IntVector3 & vector);
	IntVector3 & operator *=(integer factor);
	IntVector3 & operator /=(const IntVector3 & vector);

	IntVector3 operator +(const IntVector3 & vector) const;
	IntVector3 operator -(const IntVector3 & vector) const;
	IntVector3 operator *(const IntVector3 & vector) const;
	IntVector3 operator *(integer factor) const;
	IntVector3 operator /(const IntVector3 & vector) const;

	bool operator ==(const IntVector3 & vector) const;
	bool operator !=(const IntVector3 & vector) const;

	IntVector3 operator -() const;

	void Clear();
	
	float DistanceTo(const IntVector3 & vector) const;

	void Negative();
	void NegativeX();
	void NegativeY();
	void NegativeZ();

	string ToString() const;

	//Открытые поля
	integer X;
	integer Y;
	integer Z;
};

//===========================================================================//

}//namespace Bru

#endif // INT_VECTOR3_H

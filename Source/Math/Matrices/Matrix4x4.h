//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс матрицы 4x4
//

#pragma once
#ifndef MATRIX_4X4_H
#define MATRIX_4X4_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class Vector4;

//===========================================================================//

class Matrix4x4
{
public:
	static const Matrix4x4 Zero;
	static const Matrix4x4 Identity;

	Matrix4x4();
	Matrix4x4(float e11, float e12, float e13, float e14, float e21, float e22, float e23, float e24, float e31,
	          float e32, float e33, float e34, float e41, float e42, float e43, float e44);
	Matrix4x4(const Array<float>::SimpleType & elements);
	Matrix4x4(const Vector4 & firstLine, const Vector4 & secondLine, const Vector4 & thirdLine,
	          const Vector4 & fourthLine);
	Matrix4x4(const Matrix4x4 & other);
	Matrix4x4(Matrix4x4 && other);

	virtual ~Matrix4x4();

	Matrix4x4 & operator =(const Matrix4x4 & other);
	Matrix4x4 & operator =(Matrix4x4 && other);

	Matrix4x4 & operator +=(const Matrix4x4 & someMatrix);
	Matrix4x4 & operator -=(const Matrix4x4 & someMatrix);
	Matrix4x4 & operator *=(const Matrix4x4 & someMatrix);
	Matrix4x4 & operator *=(float scalarValue);
	Matrix4x4 & operator /=(float scalarValue);

	Matrix4x4 operator +(const Matrix4x4 & someMatrix) const;
	Matrix4x4 operator -(const Matrix4x4 & someMatrix) const;
	Matrix4x4 operator *(const Matrix4x4 & someMatrix) const;
	Matrix4x4 operator *(float scalarValue) const;
	Matrix4x4 operator /(float scalarValue) const;

	bool operator ==(const Matrix4x4 & someMatrix) const;
	bool operator !=(const Matrix4x4 & someMatrix) const;

	float & operator[](integer index)
	{
		return _elements[index];
	}
	float operator[](integer index) const
	{
		return _elements[index];
	}

	void SetElement(integer indexX, integer indexY, float value);
	float GetElement(integer indexX, integer indexY) const;

	void MakeZero();
	void MakeIdentity();

	///Транспонируем матрицу
	void Transpose();
	Matrix4x4 GetTransposed() const;

	///Возвращает обратную матрицу
	Matrix4x4 GetInversed() const;

	///Определитель матрицы
	float GetDeterminant() const;

	const float * GetData() const;
	const Array<float>::SimpleType & GetElements() const;

	operator float * () const;

private:
	Array<float>::SimpleType _elements;
};

//===========================================================================//

}//namespace Bru

#endif // MATRIX_4X4_H

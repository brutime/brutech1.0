//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Matrix4x4.h"

#include <stdio.h>

#include "../Math.h"
#include "../Exceptions/DivisionByZeroException.h"

namespace Bru
{

//===========================================================================//

const Matrix4x4 Matrix4x4::Zero(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
                                0.0f, 0.0f);

//===========================================================================//

const Matrix4x4 Matrix4x4::Identity(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
                                    0.0f, 1.0f);

//===========================================================================//

Matrix4x4::Matrix4x4() :
	_elements(16)
{
	MakeZero();
}

//===========================================================================//

Matrix4x4::Matrix4x4(float e11, float e12, float e13, float e14, float e21, float e22, float e23, float e24, float e31,
                     float e32, float e33, float e34, float e41, float e42, float e43, float e44) :
	_elements(16)
{
	_elements[0] = e11;
	_elements[1] = e12;
	_elements[2] = e13;
	_elements[3] = e14;
	_elements[4] = e21;
	_elements[5] = e22;
	_elements[6] = e23;
	_elements[7] = e24;
	_elements[8] = e31;
	_elements[9] = e32;
	_elements[10] = e33;
	_elements[11] = e34;
	_elements[12] = e41;
	_elements[13] = e42;
	_elements[14] = e43;
	_elements[15] = e44;
}

//===========================================================================//

Matrix4x4::Matrix4x4(const Array<float>::SimpleType & elements) :
	_elements(elements)
{
}

//===========================================================================//

Matrix4x4::Matrix4x4(const Vector4 & firstLine, const Vector4 & secondLine, const Vector4 & thirdLine,
                     const Vector4 & fourthLine) :
	_elements(16)
{
	_elements[0] = firstLine.X;
	_elements[1] = firstLine.Y;
	_elements[2] = firstLine.Z;
	_elements[3] = firstLine.W;
	_elements[4] = secondLine.X;
	_elements[5] = secondLine.Y;
	_elements[6] = secondLine.Z;
	_elements[7] = secondLine.W;
	_elements[8] = thirdLine.X;
	_elements[9] = thirdLine.Y;
	_elements[10] = thirdLine.Z;
	_elements[11] = thirdLine.W;
	_elements[12] = fourthLine.X;
	_elements[13] = fourthLine.Y;
	_elements[15] = fourthLine.W;
	_elements[14] = fourthLine.Z;
}

//===========================================================================//

Matrix4x4::Matrix4x4(const Matrix4x4 & other) :
	_elements(other._elements)
{
}

//===========================================================================//

Matrix4x4::Matrix4x4(Matrix4x4 && other) :
	_elements(std::move(other._elements))
{
	other._elements.Clear();
}

//===========================================================================//

Matrix4x4::~Matrix4x4()
{
	_elements.Clear();
}

//===========================================================================//

Matrix4x4 & Matrix4x4::operator =(const Matrix4x4 & other)
{
	if (this == &other)
	{
		return *this;
	}

	_elements = other._elements;

	return *this;
}

//===========================================================================//

Matrix4x4 & Matrix4x4::operator =(Matrix4x4 && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_elements, other._elements);

	return *this;
}

//===========================================================================//

Matrix4x4 & Matrix4x4::operator +=(const Matrix4x4 & someMatrix)
{
	for (integer i = 0; i < 16; i++)
	{
		_elements[i] += someMatrix._elements[i];
	}
	return *this;
}

//===========================================================================//

Matrix4x4 & Matrix4x4::operator -=(const Matrix4x4 & someMatrix)
{
	for (integer i = 0; i < 16; i++)
	{
		_elements[i] -= someMatrix._elements[i];
	}
	return *this;
}

//===========================================================================//

Matrix4x4 & Matrix4x4::operator *=(const Matrix4x4 & someMatrix)
{
	Matrix4x4 resultMatrix;

	float * r = resultMatrix._elements.GetData();
	float * m = _elements.GetData();
	float * s = someMatrix._elements.GetData();

	r[0] = m[0] * s[0] + m[1] * s[4] + m[2] * s[8] + m[3] * s[12];
	r[1] = m[0] * s[1] + m[1] * s[5] + m[2] * s[9] + m[3] * s[13];
	r[2] = m[0] * s[2] + m[1] * s[6] + m[2] * s[10] + m[3] * s[14];
	r[3] = m[0] * s[3] + m[1] * s[7] + m[2] * s[11] + m[3] * s[15];

	r[4] = m[4] * s[0] + m[5] * s[4] + m[6] * s[8] + m[7] * s[12];
	r[5] = m[4] * s[1] + m[5] * s[5] + m[6] * s[9] + m[7] * s[13];
	r[6] = m[4] * s[2] + m[5] * s[6] + m[6] * s[10] + m[7] * s[14];
	r[7] = m[4] * s[3] + m[5] * s[7] + m[6] * s[11] + m[7] * s[15];

	r[8] = m[8] * s[0] + m[9] * s[4] + m[10] * s[8] + m[11] * s[12];
	r[9] = m[8] * s[1] + m[9] * s[5] + m[10] * s[9] + m[11] * s[13];
	r[10] = m[8] * s[2] + m[9] * s[6] + m[10] * s[10] + m[11] * s[14];
	r[11] = m[8] * s[3] + m[9] * s[7] + m[10] * s[11] + m[11] * s[15];

	r[12] = m[12] * s[0] + m[13] * s[4] + m[14] * s[8] + m[15] * s[12];
	r[13] = m[12] * s[1] + m[13] * s[5] + m[14] * s[9] + m[15] * s[13];
	r[14] = m[12] * s[2] + m[13] * s[6] + m[14] * s[10] + m[15] * s[14];
	r[15] = m[12] * s[3] + m[13] * s[7] + m[14] * s[11] + m[15] * s[15];

//Типа с кэшированием
//    float m0 = m[0], m1 = m[1], m2 = m[2], m3 = m[3];
//    float m4 = m[4], m5 = m[5], m6 = m[6], m7 = m[7];
//    float m8 = m[8], m9 = m[9], m10 = m[10], m11 = m[11];
//    float m12 = m[12], m13 = m[13], m14 = m[14], m15 = m[15];
//
//    float s0 = s[0], s1 = s[1], s2 = s[2], s3 = s[3];
//    float s4 = s[4], s5 = s[5], s6 = s[6], s7 = s[7];
//    float s8 = s[8], s9 = s[9], s10 = s[10], s11 = s[11];
//    float s12 = s[12], s13 = s[13], s14 = s[14], s15 = s[15];
//
//    r[0] = m0 * s0 + m1 * s4 + m2 * s8 + m3 * s12;
//    r[1] = m0 * s1 + m1 * s5 + m2 * s9 + m3 * s13;
//    r[2] = m0 * s2 + m1 * s6 + m2 * s10 + m3 * s14;
//    r[3] = m0 * s3 + m1 * s7 + m2 * s11 + m3 * s15;
//
//    r[4] = m4 * s0 + m5 * s4 + m6 * s8 + m7 * s12;
//    r[5] = m4 * s1 + m5 * s5 + m6 * s9 + m7 * s13;
//    r[6] = m4 * s2 + m5 * s6 + m6 * s10 + m7 * s14;
//    r[7] = m4 * s3 + m5 * s7 + m6 * s11 + m7 * s15;
//
//    r[8] = m8 * s0 + m9 * s4 + m10 * s8 + m11 * s12;
//    r[9] = m8 * s1 + m9 * s5 + m10 * s9 + m11 * s13;
//    r[10] = m8 * s2 + m9 * s6 + m10 * s10 + m11 * s14;
//    r[11] = m8 * s3 + m9 * s7 + m10 * s11 + m11 * s15;
//
//    r[12] = m12 * s0 + m13 * s4 + m14 * s8 + m15 * s12;
//    r[13] = m12 * s1 + m13 * s5 + m14 * s9 + m15 * s13;
//    r[14] = m12 * s2 + m13 * s6 + m14 * s10 + m15 * s14;
//    r[15] = m12 * s3 + m13 * s7 + m14 * s11 + m15 * s15;

//    for (integer i = 0; i < 4; i++)
//    {
//        r[i * 4 + 0] = m[i * 4 + 0] * s[0 * 4 + 0] + m[i * 4 + 1] * s[1 * 4 + 0] +
//                           m[i * 4 + 2] * s[2 * 4 + 0] + m[i * 4 + 3] * s[3 * 4 + 0];
//
//        r[i * 4 + 1] = m[i * 4 + 0] * s[0 * 4 + 1] + m[i * 4 + 1] * s[1 * 4 + 1] +
//                           m[i * 4 + 2] * s[2 * 4 + 1] + m[i * 4 + 3] * s[3 * 4 + 1];
//
//        r[i * 4 + 2] = m[i * 4 + 0] * s[0 * 4 + 2] + m[i * 4 + 1] * s[1 * 4 + 2] +
//                           m[i * 4 + 2] * s[2 * 4 + 2] + m[i * 4 + 3] * s[3 * 4 + 2];
//
//        r[i * 4 + 3] = m[i * 4 + 0] * s[0 * 4 + 3] + m[i * 4 + 1] * s[1 * 4 + 3] +
//                           m[i * 4 + 2] * s[2 * 4 + 3] + m[i * 4 + 3] * s[3 * 4 + 3];
//    }

	*this = resultMatrix;
	return *this;
}

//===========================================================================//

Matrix4x4 & Matrix4x4::operator *=(float scalarValue)
{
	for (integer i = 0; i < 16; i++)
	{
		_elements[i] *= scalarValue;
	}
	return *this;
}

//===========================================================================//

Matrix4x4 & Matrix4x4::operator /=(float scalarValue)
{
	if (Math::EqualsZero(scalarValue))
	{
		DIVISION_BY_ZERO_EXCEPTION("scalarValue == 0");
	}

	for (integer i = 0; i < 16; i++)
	{
		_elements[i] /= scalarValue;
	}
	return *this;
}

//===========================================================================//

Matrix4x4 Matrix4x4::operator +(const Matrix4x4 & someMatrix) const
{
	Matrix4x4 resultMatrix = *this;
	resultMatrix += someMatrix;
	return resultMatrix;
}

//===========================================================================//

Matrix4x4 Matrix4x4::operator -(const Matrix4x4 & someMatrix) const
{
	Matrix4x4 resultMatrix = *this;
	resultMatrix -= someMatrix;
	return resultMatrix;
}

//===========================================================================//

Matrix4x4 Matrix4x4::operator *(const Matrix4x4 & someMatrix) const
{
	Matrix4x4 resultMatrix = *this;
	resultMatrix *= someMatrix;
	return resultMatrix;
}

//===========================================================================//

Matrix4x4 Matrix4x4::operator *(float scalarValue) const
{
	Matrix4x4 resultMatrix = *this;
	resultMatrix *= scalarValue;
	return resultMatrix;
}

//===========================================================================//

Matrix4x4 Matrix4x4::operator /(float scalarValue) const
{
	if (Math::EqualsZero(scalarValue))
	{
		DIVISION_BY_ZERO_EXCEPTION("scalarValue == 0");
	}

	Matrix4x4 resultMatrix = *this;
	resultMatrix /= scalarValue;
	return resultMatrix;
}

//===========================================================================//

bool Matrix4x4::operator ==(const Matrix4x4 & someMatrix) const
{
	for (integer i = 0; i < 16; i++)
	{
		if (!Math::Equals(_elements[i], someMatrix._elements[i]))
		{
			return false;
		}
	}
	return true;
}

//===========================================================================//

bool Matrix4x4::operator !=(const Matrix4x4 & someMatrix) const
{
	return !(*this == someMatrix);
}

//===========================================================================//

void Matrix4x4::MakeZero()
{
	Memory::FillWithNull(_elements.GetData(), 16 * sizeof(float));
}

//===========================================================================//

void Matrix4x4::MakeIdentity()
{
	MakeZero();
	_elements[0] = 1;
	_elements[5] = 1;
	_elements[10] = 1;
	_elements[15] = 1;
}

//===========================================================================//

const float * Matrix4x4::GetData() const
{
	return _elements.GetData();
}

//===========================================================================//

const Array<float>::SimpleType & Matrix4x4::GetElements() const
{
	return _elements;
}

//===========================================================================//

Matrix4x4::operator float * () const
{
	return _elements.GetData();
}

//===========================================================================//

void Matrix4x4::SetElement(integer indexX, integer indexY, float value)
{
	if (indexX < 0 || indexX > 3)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("indexX out of bounds: {0} ([0..3])", indexX));
	}

	if (indexY < 0 || indexY > 3)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("indexY out of bounds: {0} ([0..3])", indexX));
	}

	_elements[indexX * 4 + indexY] = value;
}

//===========================================================================//

float Matrix4x4::GetElement(integer indexX, integer indexY) const
{
	if (indexX < 0 || indexX > 3)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("indexX out of bounds: {0} ([0..3])", indexX));
	}

	if (indexY < 0 || indexY > 3)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("indexY out of bounds: {0} ([0..3])", indexX));
	}

	return _elements[indexX * 4 + indexY];
}

//===========================================================================//

void Matrix4x4::Transpose()
{
	Matrix4x4 clone = *this;

	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			_elements[i * 4 + j] = clone._elements[j * 4 + i];
		}
	}
}

//===========================================================================//

Matrix4x4 Matrix4x4::GetTransposed() const
{
	Matrix4x4 result;

	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			result._elements[i * 4 + j] = _elements[j * 4 + i];
		}
	}

	return result;
}

//===========================================================================//

Matrix4x4 Matrix4x4::GetInversed() const
{
	Matrix4x4 transposed = GetTransposed();
	const float * t = transposed.GetData();
	Matrix4x4 a //матрица дополнений
	(
	    t[5] * t[10] * t[15] + t[6] * t[11] * t[13] + t[7] * t[9] * t[14] - t[7] * t[10] * t[13] - t[6] * t[9] * t[15]
	    - t[5] * t[11] * t[14],
	    t[4] * t[10] * t[15] + t[6] * t[11] * t[12] + t[7] * t[8] * t[14] - t[7] * t[10] * t[12] - t[6] * t[8] * t[15]
	    - t[4] * t[11] * t[14],
	    t[4] * t[9] * t[15] + t[5] * t[11] * t[12] + t[7] * t[8] * t[13] - t[7] * t[9] * t[12] - t[5] * t[8] * t[15]
	    - t[4] * t[11] * t[13],
	    t[4] * t[9] * t[14] + t[5] * t[10] * t[12] + t[6] * t[8] * t[13] - t[6] * t[9] * t[12] - t[5] * t[8] * t[14]
	    - t[4] * t[10] * t[13],

	    t[1] * t[10] * t[15] + t[2] * t[11] * t[13] + t[3] * t[9] * t[14] - t[3] * t[10] * t[13] - t[2] * t[9] * t[15]
	    - t[1] * t[11] * t[14],
	    t[0] * t[10] * t[15] + t[2] * t[11] * t[12] + t[3] * t[8] * t[14] - t[3] * t[10] * t[12] - t[2] * t[8] * t[15]
	    - t[0] * t[11] * t[14],
	    t[0] * t[9] * t[15] + t[1] * t[11] * t[12] + t[3] * t[8] * t[13] - t[3] * t[9] * t[12] - t[1] * t[8] * t[15]
	    - t[0] * t[11] * t[13],
	    t[0] * t[9] * t[14] + t[1] * t[10] * t[12] + t[2] * t[8] * t[13] - t[2] * t[9] * t[12] - t[1] * t[8] * t[14]
	    - t[0] * t[10] * t[13],

	    t[1] * t[6] * t[15] + t[2] * t[7] * t[13] + t[3] * t[5] * t[14] - t[3] * t[6] * t[13] - t[2] * t[5] * t[15]
	    - t[1] * t[7] * t[14],
	    t[0] * t[6] * t[15] + t[2] * t[7] * t[12] + t[3] * t[4] * t[14] - t[3] * t[6] * t[12] - t[2] * t[4] * t[15]
	    - t[0] * t[7] * t[14],
	    t[0] * t[5] * t[15] + t[1] * t[7] * t[12] + t[3] * t[4] * t[13] - t[3] * t[5] * t[12] - t[1] * t[4] * t[15]
	    - t[0] * t[7] * t[13],
	    t[0] * t[5] * t[14] + t[1] * t[6] * t[12] + t[2] * t[4] * t[13] - t[2] * t[5] * t[12] - t[1] * t[4] * t[14]
	    - t[0] * t[6] * t[13],

	    t[1] * t[6] * t[11] + t[2] * t[7] * t[9] + t[3] * t[5] * t[10] - t[3] * t[6] * t[9] - t[2] * t[5] * t[11]
	    - t[1] * t[7] * t[10],
	    t[0] * t[6] * t[11] + t[2] * t[7] * t[8] + t[3] * t[4] * t[10] - t[3] * t[6] * t[8] - t[2] * t[4] * t[11]
	    - t[0] * t[7] * t[10],
	    t[0] * t[5] * t[11] + t[1] * t[7] * t[8] + t[3] * t[4] * t[9] - t[3] * t[5] * t[8] - t[1] * t[4] * t[11]
	    - t[0] * t[7] * t[9],
	    t[0] * t[5] * t[10] + t[1] * t[6] * t[8] + t[2] * t[4] * t[9] - t[2] * t[5] * t[8] - t[1] * t[4] * t[10]
	    - t[0] * t[6] * t[9]);

	const float invDet = 1.0f / GetDeterminant();

	Matrix4x4 result;

	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			if ((i + j) & 1)
			{
				result._elements[i * 4 + j] = -1 * invDet * (a[i * 4 + j]);
			}
			else
			{
				result._elements[i * 4 + j] = invDet * (a[i * 4 + j]);
			}
		}
	}

	return result;
}

//===========================================================================//

float Matrix4x4::GetDeterminant() const
{
	const float * m = _elements.GetData();
	const float d1 = m[10] * m[15] - m[11] * m[14];
	const float d2 = m[9] * m[15] - m[11] * m[13];
	const float d3 = m[9] * m[14] - m[10] * m[13];
	const float d4 = m[8] * m[15] - m[11] * m[12];
	const float d5 = m[8] * m[14] - m[10] * m[12];
	const float d6 = m[8] * m[13] - m[9] * m[12];
	return +m[0] * (m[5] * d1 - m[6] * d2 + m[7] * d3) - m[1] * (m[4] * d1 - m[6] * d4 + m[7] * d5)
	       + m[2] * (m[4] * d2 - m[5] * d4 + m[7] * d6) - m[3] * (m[4] * d3 - m[5] * d5 + m[6] * d6);
}

//===========================================================================//

}//namespace

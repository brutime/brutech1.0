//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    cosf почему-то конфликтует с Direct3D shapes. (Мне не нравить Direct3D)
//    поэтому используется обычные фукнции, а не *f
//

#include "Math.h"

#include <math.h>
#include <stdlib.h>

#include "Exceptions/DivisionByZeroException.h"
#include "Other/TemplateInterval.h"
#include "../Common/Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

const float Math::E = 2.7182818284590452354f;
const float Math::Pi = 3.1415926535897932385f;
const float Math::ZeroTolerance = 0.000001f;
const float Math::DigreeToRadianFactor = 0.01745329251994329577f;
const float Math::RadianToDegreeFactor = 57.2957795130823208768f;
const float Math::Accuracy = 0.0001f;

const float Math::RandomMax = static_cast<float>(RAND_MAX);

//===========================================================================//

float Math::DigreesToRadians(float value)
{
	return value * DigreeToRadianFactor;
}

//===========================================================================//

float Math::RadiansToDigrees(float value)
{
	return value * RadianToDegreeFactor;
}

//===========================================================================//

float Math::Min(float firstValue, float secondValue)
{
	if (firstValue < secondValue)
	{
		return firstValue;
	}

	return secondValue;
}

//===========================================================================//

float Math::Min(integer firstValue, integer secondValue)
{
	if (firstValue < secondValue)
	{
		return firstValue;
	}

	return secondValue;
}

//===========================================================================//

float Math::Max(float firstValue, float secondValue)
{
	if (firstValue > secondValue)
	{
		return firstValue;
	}

	return secondValue;
}

//===========================================================================//

float Math::Max(integer firstValue, integer secondValue)
{
	if (firstValue > secondValue)
	{
		return firstValue;
	}

	return secondValue;
}

//===========================================================================//

void Math::Swap(float & firstValue, float & secondValue)
{
	float thirdValue = firstValue;
	firstValue = secondValue;
	secondValue = thirdValue;
}

//===========================================================================//

bool Math::Even(integer value)
{
	return (value % 2 == 0) ? true : false;
}

//===========================================================================//

bool Math::Odd(integer value)
{
	return (value % 2 != 0) ? true : false;
}

//===========================================================================//

float Math::Abs(float value)
{
	return fabsf(value);
}

//===========================================================================//

float Math::Sign(float value)
{
	if (value > 0.0f)
	{
		return 1.0f;
	}

	if (value < 0.0f)
	{
		return -1.0f;
	}

	return 0.0f;
}

//===========================================================================//

float Math::Ceiling(float value)
{
	return ceilf(value);
}

//===========================================================================//

float Math::Ceiling(float value, float fractionRoundStep)
{
	return fractionRoundStep * Ceiling(value / fractionRoundStep);	
}

//===========================================================================//

float Math::Floor(float value)
{
	return floorf(value);
}

//===========================================================================//

float Math::Floor(float value, float fractionRoundStep)
{
	return fractionRoundStep * Floor(value / fractionRoundStep);
}

//===========================================================================//

float Math::Round(float value)
{
	return roundf(value);
}

//===========================================================================//

float Math::Round(float value, float fractionRoundStep)
{	
	return fractionRoundStep * Round(value / fractionRoundStep);	
}

//===========================================================================//

float Math::RoundWithPrecision(float value, integer precision)
{
	float roundedValue = value * Pow(10, precision);
	roundedValue = Round(roundedValue);
	roundedValue /= Pow(10, precision);

	return roundedValue;
}

//===========================================================================//

float Math::Sqr(float value)
{
	return value * value;
}

//===========================================================================//

float Math::Pow(float value, float exponent)
{
	return powf(value, exponent);
}

//===========================================================================//

float Math::Sqrt(float value)
{
	if (value < 0.0f)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value must be >= 0, but it is {0}", value));
	}

	return sqrtf(value);
}

//===========================================================================//

float Math::Exp(float value)
{
	return expf(value);
}

//===========================================================================//

float Math::Log(float value)
{
	if (Equals(value, 0.0f) || (value < 0.0f))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value must be > 0, but it is {0}", value));
	}

	return logf(value);
}

//===========================================================================//

integer Math::BitsNeededToStoreN(integer value)
{
	if (value < 0.0f)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value must be >= 0, but it is {0}", value));
	}

	integer result = 0;

	value--;
	while (value >>= 1)
	{
		result++;
	}
	return result + 1;
}

//===========================================================================//

float Math::Log10(float value)
{
	if (Equals(value, 0.0f) || (value < 0))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value must be > 0, but it is {0}", value));
	}

	return log10f(value);
}

//===========================================================================//

float Math::Mod(float dividend, float divisor)
{
	if (EqualsZero(divisor))
	{
		DIVISION_BY_ZERO_EXCEPTION("divisor == 0");
	}

	return fmodf(dividend, divisor);
}

//===========================================================================//

float Math::Sin(float angle)
{
	return sinf(DigreesToRadians(angle));
}

//===========================================================================//

float Math::Cos(float angle)
{
	//cosf почему-то конфликтует с Direct3D shapes. Мне не нравить DirectX :)
	return static_cast<float>(cos(static_cast<float>(DigreesToRadians(angle))));
}

//===========================================================================//

float Math::Tan(float angle)
{
	if (Equals(angle, -90.0f))
	{
		INVALID_ARGUMENT_EXCEPTION("angle == -90 (sin(-90) == 0)");
	}

	if (Equals(angle, -270.0f))
	{
		INVALID_ARGUMENT_EXCEPTION("angle == -270 (sin(-270) == 0)");
	}

	if (Equals(angle, 90.0f))
	{
		INVALID_ARGUMENT_EXCEPTION("angle == 90 (sin(90) == 0)");
	}

	if (Equals(angle, 270.0f))
	{
		INVALID_ARGUMENT_EXCEPTION("angle == 270 (sin(270) == 0)");
	}

	return tanf(DigreesToRadians(angle));
}

//===========================================================================//

float Math::HyperSin(float angle)
{
	return sinhf(DigreesToRadians(angle));
}

//===========================================================================//

float Math::HyperCos(float angle)
{
	return coshf(DigreesToRadians(angle));
}

//===========================================================================//

float Math::HyperTan(float angle)
{
	return tanhf(DigreesToRadians(angle));
}

//===========================================================================//

float Math::ArcSin(float value)
{
	if (value < -1.0f || value > 1.0f)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value out of range: {0} ([0..180])", value));
	}

	return RadiansToDigrees(asin(value));
}

//===========================================================================//

float Math::ArcCos(float value)
{
	if (value < -1.0f || value > 1.0f)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value out of range: {0} ([0..180])", value));
	}

	return RadiansToDigrees(acos(value));
}

//===========================================================================//

float Math::ArcTan(float value)
{
	return RadiansToDigrees(atan(value));
}

//===========================================================================//

float Math::ArcTan(float y, float x)
{
	return RadiansToDigrees(atan2(y, x));
}

//===========================================================================//

void Math::Randomize(integer seed)
{
	srand(seed);
}

//===========================================================================//

float Math::Random()
{
	return static_cast<float>(rand()) / RandomMax;
}

//===========================================================================//

float Math::Random(float value)
{
	return value * static_cast<float>(rand()) / RandomMax;
}

//===========================================================================//

integer Math::Random(integer value)
{
	return static_cast<integer>(value * (static_cast<float>(rand()) / RandomMax));
}

//===========================================================================//

bool Math::BoolRandom()
{
	return static_cast<bool>(Random(integer(2)));
}

//===========================================================================//

float Math::RandomInRange(float minValue, float maxValue)
{
	return minValue + Random() * (maxValue - minValue);
}

//===========================================================================//

integer Math::RandomInRange(integer minValue, integer maxValue)
{
	return Math::Round(RandomInRange(static_cast<float>(minValue), static_cast<float>(maxValue)));
}

//===========================================================================//

bool Math::Equals(float firstValue, float secondValue)
{
	return (Abs(firstValue - secondValue) < Accuracy);
}

//===========================================================================//

bool Math::Equals(float firstValue, float secondValue, float accuracy)
{
	return (Abs(firstValue - secondValue) < accuracy);
}

//===========================================================================//

bool Math::EqualsZero(float value)
{
	return (Abs(value) < Accuracy);
}

//===========================================================================//

bool Math::EqualsZero(float value, float accuracy)
{
	return (Abs(value) < accuracy);
}

//===========================================================================//

bool Math::IsPowerOfTwo(integer digital)
{
	return (digital & (digital - 1)) == 0;
}

//===========================================================================//

integer Math::GetNearestPowerOfTwo(integer value)
{
	integer result = static_cast<integer>(1);
	while (result < value)
	{
		result <<= 1;
	}
	return result;
}

//===========================================================================//

bool Math::IsDivisibleBy(integer value, integer divisor)
{
	if (EqualsZero(divisor))
	{
		DIVISION_BY_ZERO_EXCEPTION("divisor == 0");
	}

	return value % divisor == 0;
}

//===========================================================================//

integer Math::ToPercent(integer current, integer total)
{
	return Math::Round(current / static_cast<float>(total) * 100.0f);
}

//===========================================================================//

float Math::ToPercent(float current, float total)
{
	return current / total * 100.0f;
}

//===========================================================================//

integer Math::FromPercent(integer percent, integer total)
{
	return Math::Round(total / 100.0f * percent);
}

//===========================================================================//

float Math::FromPercent(float percent, float total)
{
	return total / 100.0f * percent;
}

//===========================================================================//

float Math::FitInRange(float value, float rangeFrom, float rangeTo)
{	
	if (value < rangeFrom)
	{
		return rangeFrom;
	}
	
	if (value > rangeTo)
	{
		return rangeTo;
	}
	
	return value;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс для работы с математикой. Внимание, тригонометрические
//    функии работают с градусами, а не с радианами, как в стандартной библиотеке
//

#pragma once
#ifndef MATH_H
#define MATH_H

#include "Color/Colors.h"
#include "Exceptions/DivisionByZeroException.h"
#include "Geometry/Geometry.h"
#include "Geometry/IntRectangle.h"
#include "Geometry/Rectangle.h"
#include "Matrices/Matrix4x4.h"
#include "Other/Angle.h"
#include "SpaceMaths/BoundingSphere.h"
#include "SpaceMaths/Plane.h"
#include "SpaceMaths/Transformation.h"
#include "Vectors/IntVector2.h"
#include "Vectors/IntVector3.h"
#include "Vectors/Vector4.h"
#include "Vectors/Vector2.h"
#include "Vectors/Vector3.h"
#include "Vectors/Vector4.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

class Math
{
public:
	static const float E;
	static const float Pi;
	static const float ZeroTolerance;
	static const float DigreeToRadianFactor;
	static const float RadianToDegreeFactor;
	static const float Accuracy;

	static float DigreesToRadians(float value);
	static float RadiansToDigrees(float value);

	static float Min(float firstValue, float secondValue);
	static float Max(float firstValue, float secondValue);

	static float Min(integer firstValue, integer secondValue);
	static float Max(integer firstValue, integer secondValue);

	static void Swap(float & firstValue, float & secondValue);

	///Четное число
	static bool Even(integer value);
	///Нечетное число
	static bool Odd(integer value);
	static float Abs(float value);
	static float Sign(float value);

	static float Ceiling(float value);
	static float Ceiling(float value, float fractionRoundStep);
	static float Floor(float value);
	static float Floor(float value, float fractionRoundStep);
	static float Round(float value);
	static float Round(float value, float fractionRoundStep);
	static float RoundWithPrecision(float value, integer precision);	

	static float Sqr(float value);
	static float Pow(float value, float exponent);
	static float Sqrt(float value);
	static float Exp(float value);
	static float Log(float value);
	static integer BitsNeededToStoreN(integer value);
	static float Log10(float value);
	static float Mod(float dividend, float divisor);

	static float Sin(float degrees);
	static float Cos(float degrees);
	static float Tan(float degrees);

	static float HyperSin(float degrees);
	static float HyperCos(float degrees);
	static float HyperTan(float degrees);

	///Возвращает угол, синус которого равен указанному числу
	static float ArcSin(float value);
	///Возвращает угол, косинус которого равен указанному числу
	static float ArcCos(float value);
	///Возвращает угол, тангенс которого равен указанному числу
	static float ArcTan(float value);
	///Возвращает угол, тангенс которого равен отношению двух указанных чисел
	static float ArcTan(float y, float x);

	static void Randomize(integer seed);
	static float Random();
	static float Random(float value);
	static integer Random(integer value);
	static bool BoolRandom();
	static float RandomInRange(float minValue, float maxValue);
	static integer RandomInRange(integer minValue, integer maxValue);

	static bool Equals(float firstValue, float secondValue);
	static bool Equals(float firstValue, float secondValue, float accuracy);
	static bool EqualsZero(float value);
	static bool EqualsZero(float value, float accuracy);

	static bool IsPowerOfTwo(integer digital);
	static integer GetNearestPowerOfTwo(integer value);

	static bool IsDivisibleBy(integer value, integer divisor);

	static integer ToPercent(integer current, integer total);
	static float ToPercent(float current, float total);

	static integer FromPercent(integer percent, integer total);
	static float FromPercent(float percent, float total);
	
	static float FitInRange(float value, float rangeFrom, float rangeTo);

private:
	static const float RandomMax;

	Math() = delete;
	Math(const Math &) = delete;
	~Math() = delete;
	Math & operator =(const Math &) = delete;
};

//===========================================================================//

} // namespace Bru

#include "Other/TemplateInterval.h"
#include "Other/TemplateSize.h"

//===========================================================================//

namespace Bru
{

typedef TemplateInterval<float>	Interval;
typedef TemplateInterval<integer> IntInterval;

typedef TemplateSize<float> Size;
typedef TemplateSize<integer> IntSize;

} // namespace Bru

//===========================================================================//

#endif // MATH_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BoundingSphere.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

BoundingSphere::BoundingSphere() :
	Position(),
	Radius(0),
	IsEmpty(true)
{
}

//===========================================================================//

BoundingSphere::BoundingSphere(const BoundingSphere & other) :
	Position(other.Position),
	Radius(other.Radius),
	IsEmpty(other.IsEmpty)
{
}

//===========================================================================//

BoundingSphere::~BoundingSphere()
{
}

//===========================================================================//

BoundingSphere & BoundingSphere::operator =(const BoundingSphere & other)
{
	return Assign(other);
}

//===========================================================================//

BoundingSphere & BoundingSphere::Assign(const BoundingSphere & other)
{
	if (this == &other)
	{
		return *this;
	}

	Position = other.Position;
	Radius = other.Radius;
	IsEmpty = other.IsEmpty;

	return *this;
}

//===========================================================================//

void BoundingSphere::Clear()
{
	Position = Vector3(0.0f, 0.0f, 0.0f);
	Radius = 0;
	IsEmpty = true;
}

//===========================================================================//

BoundingSphere & BoundingSphere::Consume(const BoundingSphere & other)
{
	if (IsEmpty)
	{
		Position = other.Position;
		Radius = other.Radius;
		IsEmpty = false;
		return *this;
	}

	//нужно найти точку на старой сфере, максимально отдаленную от добавляемой, и аналогичную точку на добавляемой сфере
	//затем найти середину соединяющего их отрезка
	const float dx = other.Position.X - Position.X, dy = other.Position.Y - Position.Y, dz = other.Position.Z
	                 - Position.Z;
	if (Math::EqualsZero(dx) && Math::EqualsZero(dy) && Math::EqualsZero(dz))
	{
		Radius = Math::Max(Radius, other.Radius);
		return *this;
	}

	Vector3 oldToNew(dx, dy, dz);
	oldToNew.Normalize();
	Vector3 p1(Position.X - oldToNew.X * Radius, Position.Y - oldToNew.Y * Radius, Position.Z - oldToNew.Z * Radius);
	Vector3 p2(other.Position.X + oldToNew.X * other.Radius, other.Position.Y + oldToNew.Y * other.Radius,
	           other.Position.Z + oldToNew.Z * other.Radius);

	Radius = (p2 - p1).Length() / 2;
	Position.X = (p2.X + p1.X) / 2;
	Position.Y = (p2.Y + p1.Y) / 2;
	Position.Z = (p2.Z + p1.Z) / 2;

	IsEmpty = false;
	return *this;
}

//===========================================================================//

BoundingSphere BoundingSphere::Transform(const Transformation & transformation) const
{
	BoundingSphere resultBoundingSphere(*this);
	if (resultBoundingSphere.IsEmpty)
	{
		return resultBoundingSphere;
	}
	resultBoundingSphere.Position = Position.MultiplyByMatrix4x4AsPoint(transformation.GetMatrix());
	resultBoundingSphere.Radius = Radius;
	return resultBoundingSphere;
}

//===========================================================================//

}

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс, содержащий информацию о трансформации объекта в некоторой системе координат
//

#pragma once
#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "../Vectors/Vector3.h"
#include "../Other/Angle.h"

namespace Bru
{
//===========================================================================//

class Transformation
{
public:
	Transformation();
	Transformation(const Transformation & other);
	Transformation(Transformation && other);
	~Transformation();

	Transformation & operator =(const Transformation & other);
	Transformation & operator =(Transformation && other);

	Transformation & operator +=(const Transformation & someTransformation);
	Transformation & operator -=(const Transformation & someTransformation);
	Transformation & operator *=(const Transformation & someTransformation);

	Transformation operator +(const Transformation & someTransformation) const;
	Transformation operator -(const Transformation & someTransformation) const;
	Transformation operator *(const Transformation & someTransformation) const;

	bool operator ==(const Transformation & someTransformation) const;
	bool operator !=(const Transformation & someTransformation) const;

	void MakeZero();
	void MakeIdentity();

	///Задание положения в пространстве
	void SetPosition(float newX, float newY, float newZ = 0.f);
	void SetPosition(const Vector3 & newPosition);

	void SetPositionX(float newX);
	void SetPositionY(float newY);
	void SetPositionZ(float newZ);

	///Смещение относительно текущего положения
	void Translate(float dx, float dy, float dz = 0.f);
	void Translate(const Vector3 & deltaPosition);

	void TranslateX(float dx);
	void TranslateY(float dy);
	void TranslateZ(float dz);

	//	///Неоднородное масштабирование по осям
	//	void Scale(float xScale, float yScale, float zScale);

	///Вращение вокруг оси(x,y,z) на angle градусов
	void Rotate(float x, float y, float z, float angle);

	///Вращение в плоскости экрана на angle градусов
	void Rotate2D(float angle);
 
	///Поворот относительно текущей позиции, после которого объект "смотрит" на точку (x,y,z)
	void LookAt(float x, float y, float z, const Vector3 & up = Vector3(0.f, 1.f, 0.f));

	void Clear();

	const Matrix4x4 & GetMatrix() const;

	///Возвращает вектор с текущей позицией
	const Vector3 & GetPosition() const;

	float GetPositionX() const;
	float GetPositionY() const;
	float GetPositionZ() const;

	float GetAngleX() const;
	float GetAngleY() const;
	float GetAngleZ() const;

	bool IsIdentityRotation() const;

	string ToString() const;

private:
	void UpdatePosition();

	Matrix4x4 _matrix;
	//Закэшированный данные, возможно расчитывать их будет быстрее из-за более компактного использования кэш памяти
	Vector3 _position;
	Angle _angleX;
	Angle _angleY;
	Angle _angleZ;

	Transformation & operator *=(float) = delete;
	Transformation & operator /=(float) = delete;

	Transformation operator *(float) const = delete;
	Transformation operator /(float) const = delete;
};

//===========================================================================//

} // namespace Bru

#endif // TRANSFORMATION_H

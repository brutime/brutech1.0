//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: математика связанная с матрицами
//

#pragma once
#ifndef MATRIX_MATH_H
#define MATRIX_MATH_H

#include "../Math.h"

namespace Bru
{

//===========================================================================//

class MatrixMath
{
public:
	static Matrix4x4 Ortho(float left, float right, float bottom, float top, float nearZ, float farZ);
	static Matrix4x4 Ortho2D(float left, float right, float bottom, float top);
	static Matrix4x4 Frustum(float left, float right, float bottom, float top, float nearZ, float farZ);
	static Matrix4x4 Perspective(float fov, float aspect, float nearZ, float farZ);

	static Matrix4x4 LookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX =
	                            0.0f, float upY = 1.0f, float upZ = 0.0f);
	static Matrix4x4 LookAt(const Vector3 & eye, const Vector3 & center, const Vector3 & up);

private:
	MatrixMath() = delete;
	MatrixMath(const MatrixMath &);
	~MatrixMath() = delete;
	MatrixMath & operator =(const MatrixMath &);
};

//===========================================================================//

}// namespace

#endif // MATRIX_MATH_H

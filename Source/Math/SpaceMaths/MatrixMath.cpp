//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: задается все по столбцам, так как этого требует Graphic API
//

#include "MatrixMath.h"

namespace Bru
{

//===========================================================================//

Matrix4x4 MatrixMath::Ortho(float left, float right, float bottom, float top, float nearZ, float farZ)
{
	const float tx = -(right + left) / (right - left);
	const float ty = -(top + bottom) / (top - bottom);
	const float tz = -(farZ + nearZ) / (farZ - nearZ);

	//транспанированная матрица
	return Matrix4x4(2.0f / (right - left), 0.0f, 0.0f, 0.0f, 0.0f, 2.0f / (top - bottom), 0.0f, 0.0f, 0.0f, 0.0f,
	                 -2.0f / (farZ - nearZ), 0.0f, tx, ty, tz, 1.0f);
}

//===========================================================================//

Matrix4x4 MatrixMath::Ortho2D(float left, float right, float bottom, float top)
{
	return Ortho(left, right, bottom, top, -1.0f, 1.0f);
}

//===========================================================================//

Matrix4x4 MatrixMath::Frustum(float left, float right, float bottom, float top, float nearZ, float farZ)
{
	if (nearZ < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("nearZ must be >= 0, but it is {0}", farZ));
	}

	if (farZ < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("farZ must be >= 0, but it is {0}", farZ));
	}

	const float A = (right + left) / (right - left);
	const float B = (top + bottom) / (top - bottom);
	const float C = -(farZ + nearZ) / (farZ - nearZ);
	const float D = -2.0f * farZ * nearZ / (farZ - nearZ);

	//транспанированная матрица
	return Matrix4x4(2.0f * nearZ / (right - left), 0.0f, 0.0f, 0.0f, 0.0f, 2.0f * nearZ / (top - bottom), 0.0f, 0.0f,
	                 A, B, C, -1.0f, 0.0f, 0.0f, D, 0.0f);
}

//===========================================================================//

Matrix4x4 MatrixMath::Perspective(float fov, float aspect, float nearZ, float farZ)
{
	if (fov < 0 || fov > 180)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("fov out of range: {0} ([0..180])", farZ));
	}

	if (nearZ < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("nearZ must be >= 0, but it is {0}", farZ));
	}

	if (farZ < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("farZ must be >= 0, but it is {0}", farZ));
	}

	float yMax = nearZ * Math::Tan(fov * 0.5f);
	float yMin = -yMax;
	float xMin = yMin * aspect;
	float xMax = yMax * aspect;

	return Frustum(xMin, xMax, yMin, yMax, nearZ, farZ);
}

//===========================================================================//

Matrix4x4 MatrixMath::LookAt(float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX,
                             float upY, float upZ)
{
	return LookAt(Vector3(eyeX, eyeY, eyeZ), Vector3(centerX, centerY, centerZ), Vector3(upX, upY, upZ));
}

//===========================================================================//

Matrix4x4 MatrixMath::LookAt(const Vector3 & eye, const Vector3 & center, const Vector3 & up)
{
	Vector3 x;
	Vector3 y;
	Vector3 z;

	z = eye - center;
	z.Normalize();

	y = up;

	x = y.Cross(z);
	y = z.Cross(x);

	x.Normalize();
	y.Normalize();

	//транспанированная матрица
	return Matrix4x4(x.X, y.X, z.X, 0.0f, x.Y, y.Y, z.Y, 0.0f, x.Z, y.Z, z.Z, 0.0f, -(x.Dot(eye)), -(y.Dot(eye)),
	                 -(z.Dot(eye)), 1.0f);
}

//===========================================================================//

}

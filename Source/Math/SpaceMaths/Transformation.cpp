//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Transformation.h"

#include "../Math.h"

namespace Bru
{

//===========================================================================//

Transformation::Transformation() :
	_matrix(),
	_position(),
	_angleX(),
	_angleY(),
	_angleZ()
{
	_matrix.MakeIdentity();
}

//===========================================================================//

Transformation::Transformation(const Transformation & other) :
	_matrix(other._matrix),
	_position(other._position),
	_angleX(other._angleX),
	_angleY(other._angleY),
	_angleZ(other._angleZ)
{
}

//===========================================================================//

Transformation::Transformation(Transformation && other) :
	_matrix(std::move(other._matrix)),
	_position(std::move(other._position)),
	_angleX(other._angleX),
	_angleY(other._angleY),
	_angleZ(other._angleZ)
{
	other._matrix.MakeZero();
	other._position.Clear();
	other._angleX = 0.0f;
	other._angleY = 0.0f;
	other._angleZ = 0.0f;
}

//===========================================================================//

Transformation::~Transformation()
{
}

//===========================================================================//

Transformation & Transformation::operator =(const Transformation & other)
{
	if (this == &other)
	{
		return *this;
	}

	_matrix = other._matrix;
	_position = other._position;
	_angleX = other._angleX;
	_angleY = other._angleY;
	_angleZ = other._angleZ;

	return *this;
}

//===========================================================================//

Transformation & Transformation::operator =(Transformation && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_matrix, other._matrix);
	std::swap(_position, other._position);
	std::swap(_angleX, other._angleX);
	std::swap(_angleY, other._angleY);
	std::swap(_angleZ, other._angleZ);

	return *this;
}

//===========================================================================//

Transformation & Transformation::operator +=(const Transformation & someTransformation)
{
	_matrix += someTransformation._matrix;
	_position += someTransformation._position;
	_angleX += someTransformation._angleX;
	_angleY += someTransformation._angleY;
	_angleZ += someTransformation._angleZ;

	return *this;
}

//===========================================================================//

Transformation & Transformation::operator -=(const Transformation & someTransformation)
{
	_matrix -= someTransformation._matrix;
	_position -= someTransformation._position;
	_angleX -= someTransformation._angleX;
	_angleY -= someTransformation._angleY;
	_angleZ -= someTransformation._angleZ;

	return *this;
}

//===========================================================================//

Transformation & Transformation::operator *=(const Transformation & someTransformation)
{
	_matrix *= someTransformation._matrix;

	UpdatePosition();

	//Вызывают сомнения эти строки
	_angleX += someTransformation._angleX;
	_angleY += someTransformation._angleY;
	_angleZ += someTransformation._angleZ;

	return *this;
}

//===========================================================================//

Transformation Transformation::operator +(const Transformation & someTransformation) const
{
	Transformation resultTransformation = *this;
	resultTransformation += someTransformation;
	return resultTransformation;
}

//===========================================================================//

Transformation Transformation::operator -(const Transformation & someTransformation) const
{
	Transformation resultTransformation = *this;
	resultTransformation -= someTransformation;
	return resultTransformation;
}

//===========================================================================//

Transformation Transformation::operator *(const Transformation & someTransformation) const
{
	Transformation resultTransformation = *this;
	resultTransformation *= someTransformation;
	return resultTransformation;
}

//===========================================================================//

bool Transformation::operator ==(const Transformation & someTransformation) const
{
	return _matrix == someTransformation._matrix;
}

//===========================================================================//

bool Transformation::operator !=(const Transformation & someTransformation) const
{
	return !(*this == someTransformation);
}

//===========================================================================//

void Transformation::MakeZero()
{
	Clear();
}

//===========================================================================//

void Transformation::MakeIdentity()
{
	Clear();
	_matrix.MakeIdentity();
}

//===========================================================================//

void Transformation::SetPosition(float newX, float newY, float newZ)
{
	_matrix[12] = newX;
	_matrix[13] = newY;
	_matrix[14] = newZ;
	UpdatePosition();
}

//===========================================================================//

void Transformation::SetPosition(const Vector3 & newPosition)
{
	SetPosition(newPosition.X, newPosition.Y, newPosition.Z);
}

//===========================================================================//

void Transformation::SetPositionX(float newX)
{
	_matrix[12] = newX;
	_position.X = newX;
}

//===========================================================================//

void Transformation::SetPositionY(float newY)
{
	_matrix[13] = newY;
	_position.Y = newY;
}

//===========================================================================//

void Transformation::SetPositionZ(float newZ)
{
	_matrix[14] = newZ;
	_position.Z = newZ;
}

//===========================================================================//

void Transformation::Translate(float dx, float dy, float dz)
{
	_matrix[12] += dx;
	_matrix[13] += dy;
	_matrix[14] += dz;
	UpdatePosition();
}

//===========================================================================//

void Transformation::Translate(const Vector3 & deltaPosition)
{
	Translate(deltaPosition.X, deltaPosition.Y, deltaPosition.Z);
}

//===========================================================================//

void Transformation::TranslateX(float dx)
{
	_matrix[12] += dx;
	_position.X = _matrix[12];
}

//===========================================================================//

void Transformation::TranslateY(float dy)
{
	_matrix[13] += dy;
	_position.Y = _matrix[13];
}

//===========================================================================//

void Transformation::TranslateZ(float dz)
{
	_matrix[14] += dz;
	_position.Z = _matrix[14];
}

//===========================================================================//

//void Transformation::Scale(float xScale, float yScale, float zScale)
//{
//    elements[0] *= xScale;
//    elements[1] *= xScale;
//    elements[2] *= xScale;
//
//    elements[4] *= yScale;
//    elements[5] *= yScale;
//    elements[6] *= yScale;
//
//    elements[8] *= zScale;
//    elements[9] *= zScale;
//    elements[10] *= zScale;
//}

//===========================================================================//

//угол в градусах
void Transformation::Rotate(float x, float y, float z, float angle)
{
	//расчет элементов матрицы есть здесь:
	//http://www.opengl.org/sdk/docs/man/xhtml/glRotate.xml
	//или здесь
	//http://opengl.gamedev.ru/doc/?func=glRotate

	Matrix4x4 rotation;

	const float cosA = Math::Cos(angle);
	const float sinA = Math::Sin(angle);

	const float oneMinusCosA = 1.0f - cosA;

	rotation[0] = oneMinusCosA * x * x + cosA;
	rotation[1] = oneMinusCosA * x * y - sinA * z;
	rotation[2] = oneMinusCosA * x * z + sinA * y;
	rotation[3] = 0.0f;

	rotation[4] = oneMinusCosA * y * x + sinA * z;
	rotation[5] = oneMinusCosA * y * y + cosA;
	rotation[6] = oneMinusCosA * y * z - sinA * x;
	rotation[7] = 0.0f;

	rotation[8] = oneMinusCosA * z * x - sinA * y;
	rotation[9] = oneMinusCosA * z * y + sinA * x;
	rotation[10] = oneMinusCosA * z * z + cosA;
	rotation[11] = 0.0f;

	rotation[12] = 0.0f;
	rotation[13] = 0.0f;
	rotation[14] = 0.0f;
	rotation[15] = 1.0f;

	_matrix *= rotation;

	UpdatePosition();
	_angleX += angle * x;
	_angleY += angle * y;
	_angleZ += angle * z;
}

//===========================================================================//

void Transformation::Rotate2D(float angle)
{
	Rotate(0.0f, 0.0f, 1.0f, angle);
}

//===========================================================================//

void Transformation::LookAt(float x, float y, float z, const Vector3 & up)
{
	Vector3 zv(x - _matrix[12], y - _matrix[13], _matrix[14] - z); // этот врод правильный, но работает не так, как ожидалось
	//Vector3 zv(elements[12] - x, elements[13] - y, elements[14] - z);
	zv.Normalize();
	Vector3 xv = up.Cross(zv);
	xv.Normalize();
	Vector3 yv = zv.Cross(xv);
	yv.Normalize();
	_matrix[0] = xv.X;
	_matrix[1] = xv.Y;
	_matrix[2] = xv.Z;

	_matrix[4] = yv.X;
	_matrix[5] = yv.Y;
	_matrix[6] = yv.Z;

	_matrix[8] = zv.X;
	_matrix[9] = zv.Y;
	_matrix[10] = zv.Z;

	UpdatePosition();
}

//===========================================================================//

void Transformation::Clear()
{
	_matrix.MakeZero();
	_position.Clear();
	_angleX = 0.0f;
	_angleY = 0.0f;
	_angleZ = 0.0f;
}

//===========================================================================//

const Matrix4x4 & Transformation::GetMatrix() const
{
	return _matrix;
}

//===========================================================================//

const Vector3 & Transformation::GetPosition() const
{
	return _position;
}

//===========================================================================//

float Transformation::GetPositionX() const
{
	return _position.X;
}

//===========================================================================//

float Transformation::GetPositionY() const
{
	return _position.Y;
}

//===========================================================================//

float Transformation::GetPositionZ() const
{
	return _position.Z;
}

//===========================================================================//

float Transformation::GetAngleX() const
{
	return _angleX;
}

//===========================================================================//

float Transformation::GetAngleY() const
{
	return _angleY;
}

//===========================================================================//

float Transformation::GetAngleZ() const
{
	return _angleZ;
}

//===========================================================================//

bool Transformation::IsIdentityRotation() const
{
	return Math::EqualsZero(_angleX) && Math::EqualsZero(_angleY) && Math::EqualsZero(_angleZ);
}

//===========================================================================//

string Transformation::ToString() const
{
	return string::Format("Position: {0}\nAngles: {1:.2}, {2:.2}, {3:.2}",
	                      _position.ToString(), _angleX.ToString(), _angleY.ToString(), _angleZ.ToString());
}

//===========================================================================//

void Transformation::UpdatePosition()
{
	_position.X = _matrix[12];
	_position.Y = _matrix[13];
	_position.Z = _matrix[14];
}

//===========================================================================//

} // namespace Bru

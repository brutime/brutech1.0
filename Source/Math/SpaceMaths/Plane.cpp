//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Plane.h"

namespace Bru
{

//===========================================================================//

Plane::Plane() :
	_normal(),
	_planeD(0),
	_isNormalized(false)
{
}

//===========================================================================//

Plane::Plane(float a, float b, float c, float d) :
	_normal(a, b, c),
	_planeD(d),
	_isNormalized(false)
{
}

//===========================================================================//

void Plane::Normalize()
{
	if (!_isNormalized)
	{
		float invL = 1.0f / _normal.Length();
		_planeD *= invL;
		_normal *= invL;
		_isNormalized = true;
	}
}

//===========================================================================//

float Plane::GetDistance(const Vector3 & point)
{
	//вектор нормали должен быть нормализован
	return _normal.Dot(point) + _planeD;
}

//===========================================================================//

}

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, представляющий плоскость в 3-мерном пространстве
//

#ifndef PLANE_H
#define PLANE_H

#include "../Vectors/Vector3.h"

namespace Bru
{

//===========================================================================//

class Plane
{
public:
	Plane();
	Plane(float a, float b, float c, float d);

	void Normalize();
	float GetDistance(const Vector3 & point);

private:
	Vector3 _normal;
	float _planeD;
	bool _isNormalized;
};

//===========================================================================//

}

#endif // PLANE_H

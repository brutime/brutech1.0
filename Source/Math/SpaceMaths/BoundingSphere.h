//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Сфера, ограничивающая 2д-объект
//

#ifndef BOUNDING_SPHERE_H
#define BOUNDING_SPHERE_H

#include "../Vectors/Vector3.h"
#include "Transformation.h"

namespace Bru
{

//===========================================================================//

class BoundingSphere
{
public:
	BoundingSphere();
	BoundingSphere(const BoundingSphere & other);
	virtual ~BoundingSphere();

	BoundingSphere & operator =(const BoundingSphere & other);

	BoundingSphere & Assign(const BoundingSphere & other);
	void Clear();

	BoundingSphere & Consume(const BoundingSphere & other);
	BoundingSphere Transform(const Transformation & transformation) const;

	Vector3 Position;
	float Radius;
	bool IsEmpty;
};

//===========================================================================//

}

#endif // BOUNDING_SPHERE_H

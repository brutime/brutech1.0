//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: демка поворотов
//

#pragma once
#ifndef ROTATION_DEMO_H
#define ROTATION_DEMO_H

#include "../../Helpers/Engine/Engine.h"
#include "../Common/QuitInputReceiver.h"

namespace Bru
{

//===========================================================================//

class RotationDemo : public BaseApplication
{
public:
	static const string DomainName;
	static const string LayerName;

	RotationDemo();
	virtual ~RotationDemo();

	void Initialize();
	void MainLoop();
	void FreeResources();

private:
	static const integer SurfaceCoordsWidth;
	static const integer SurfaceCoordsHeight;

	SharedPtr<PositioningNode> _sun;
	SharedPtr<PositioningNode> _sunCenter;
	SharedPtr<Sprite> _sunSprite;

	SharedPtr<PositioningNode> _earth;
	SharedPtr<PositioningNode> _earthCenter;
	SharedPtr<Sprite> _earthSprite;

	SharedPtr<PositioningNode> _moon;
	SharedPtr<PositioningNode> _moonCenter;
	SharedPtr<Sprite> _moonSprite;

	SharedPtr<RotationController> _sunSelfRotationController;
	SharedPtr<RotationController> _earthRotationController;
	SharedPtr<RotationController> _earthSelfRotationController;
	SharedPtr<RotationController> _moonRotationController;

	SharedPtr<QuitInputReceiver> _inputReceiver;

	SharedPtr<Camera> _camera;
};

//===========================================================================//

}//namespace

#endif // ROTATION_DEMO_H

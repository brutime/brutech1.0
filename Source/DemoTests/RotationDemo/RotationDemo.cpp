//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RotationDemo.h"

namespace Bru
{

//===========================================================================//

const string RotationDemo::DomainName = "RotationDemo";
const string RotationDemo::LayerName = "RotationDemo";

//===========================================================================//

const integer RotationDemo::SurfaceCoordsWidth = 16;
const integer RotationDemo::SurfaceCoordsHeight = 12;

//===========================================================================//

RotationDemo::RotationDemo() :
	BaseApplication(),
	_sun(),
	_sunCenter(),
	_sunSprite(),
	_earth(),
	_earthCenter(),
	_earthSprite(),
	_moon(),
	_moonCenter(),
	_moonSprite(),
	_sunSelfRotationController(),
	_earthRotationController(),
	_earthSelfRotationController(),
	_moonRotationController(),
	_inputReceiver(),
	_camera()
{
}

//===========================================================================//

RotationDemo::~RotationDemo()
{
}

//===========================================================================//

void RotationDemo::Initialize()
{
	Engine = new EngineSingleton(SurfaceCoordsWidth, SurfaceCoordsHeight);

	TextureManager->Load("Earth");
	TextureManager->Load("Moon");
	TextureManager->Load("Sun");

	ParametersFileManager->Load("Sprites/Earth");
	ParametersFileManager->Load("Sprites/Moon");
	ParametersFileManager->Load("Sprites/Sun");

	DomainManager->Add(DomainName);

	Renderer->SetClearColor(Colors::Black);
	Renderer->AddLayerToBack(DomainName, new RenderableLayer(LayerName));

	//Солнце
	_sun = new PositioningNode(DomainName);

	_sunCenter = new PositioningNode(DomainName);
	_sunCenter->AddChild(_sun);
	_sunCenter->SetPosition(8.0f, 6.0f, 0.0f);

	_sunSprite = new Sprite(DomainName, LayerName, "Sun");
	_sunSprite->SetPositioningNode(_sun);

	//Земля
	_earth = new PositioningNode(DomainName);

	_earthCenter = new PositioningNode(DomainName);
	_earthCenter->AddChild(_earth);
	_earthCenter->Translate(-1.0f, 2.0f, 0.0f);

	_earthSprite = new Sprite(DomainName, LayerName, "Earth");
	_earthSprite->SetPositioningNode(_earth);

	_sun->AddChild(_earthCenter);

	//Луна
	_moon = new PositioningNode(DomainName);

	_moonCenter = new PositioningNode(DomainName);
	_moonCenter->AddChild(_moon);
	_moonCenter->Translate(1.0f, 0.0f, 0.0f);

	_moonSprite = new Sprite(DomainName, LayerName, "Moon");
	_moonSprite->SetPositioningNode(_moon);

	_earth->AddChild(_moonCenter);

	//Солнце вращается вокруг собственной оси
	_sunSelfRotationController = new RotationController(_sun, -25.0f);

	//Земля вращается вокруг Солнца
	_earthRotationController = new RotationController(_earthCenter, 50.0f);

	//Земля вращается вокруг собственной оси
	_earthSelfRotationController = new RotationController(_earth, 300.0f);

	//Луна - вокруг Земли, при этом вокруг своей оси не вращается
	_moonRotationController = new RotationController(_moonCenter, -150.0f);

	WidgetsManager->HideCursor();

	_inputReceiver = new QuitInputReceiver(DomainName);

	_camera = new Camera();
	_camera->Set2DPosition(SurfaceCoordsWidth / 2.0f, SurfaceCoordsHeight / 2.0f, SurfaceCoordsHeight / 2.0f);

	DomainManager->Select(DomainName);
}

//===========================================================================//

void RotationDemo::MainLoop()
{
	Engine->Update();
}

//===========================================================================//

void RotationDemo::FreeResources()
{
	_camera = NullPtr;
	_inputReceiver = NullPtr;
	_moonRotationController = NullPtr;
	_earthSelfRotationController = NullPtr;
	_earthRotationController = NullPtr;
	_sunSelfRotationController = NullPtr;
	_moonSprite = NullPtr;
	_moonCenter = NullPtr;
	_moon = NullPtr;
	_earthSprite = NullPtr;
	_earthCenter = NullPtr;
	_earth = NullPtr;
	_sunSprite = NullPtr;
	_sunCenter = NullPtr;
	_sun = NullPtr;

	Renderer->RemoveLayer(DomainName, LayerName);
	DomainManager->Remove(DomainName);

	Engine = NullPtr;
}

//===========================================================================//

}// namespace Bru

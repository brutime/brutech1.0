//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MinimalApplicationDemo.h"

namespace Bru
{

//===========================================================================//

const string MinimalApplicationDemo::DomainName = "MinimalApplicationDemo";
const string MinimalApplicationDemo::LayerName = "MinimalApplicationDemo";

//===========================================================================//

const integer MinimalApplicationDemo::SurfaceCoordsWidth = 32;
const integer MinimalApplicationDemo::SurfaceCoordsHeight = 18;

//===========================================================================//

MinimalApplicationDemo::MinimalApplicationDemo() :
	_camera(),
	_inputReceiver()
{
}

//===========================================================================//

MinimalApplicationDemo::~MinimalApplicationDemo()
{
}

//===========================================================================//

void MinimalApplicationDemo::Initialize()
{
	Engine = new EngineSingleton(SurfaceCoordsWidth, SurfaceCoordsHeight);

	DomainManager->Add(DomainName);

	Renderer->AddLayerToBack(DomainName, new RenderableLayer(LayerName));

	WidgetsManager->HideCursor();

	_camera = new Camera();
	_camera->Set2DPosition(SurfaceCoordsWidth / 2.0f, SurfaceCoordsHeight / 2.0f, SurfaceCoordsHeight / 2.0f);

	_inputReceiver = new QuitInputReceiver(DomainName);

	DomainManager->Select(DomainName);
}

//===========================================================================//

void MinimalApplicationDemo::MainLoop()
{
	Engine->Update();
}

//===========================================================================//

void MinimalApplicationDemo::FreeResources()
{
	_camera = NullPtr;
	_inputReceiver = NullPtr;

	Renderer->RemoveLayer(DomainName, LayerName);
	DomainManager->Remove(DomainName);

	Engine = NullPtr;
}

//===========================================================================//

}//namespace

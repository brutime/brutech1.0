//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: минимальное приложение
//

#pragma once
#ifndef MINIMAL_APPLICATION_DEMO_H
#define MINIMAL_APPLICATION_DEMO_H

#include "../../Helpers/Engine/Engine.h"
#include "../Common/QuitInputReceiver.h"

namespace Bru
{

//===========================================================================//

class MinimalApplicationDemo : public BaseApplication
{
public:
	static const string DomainName;
	static const string LayerName;

	MinimalApplicationDemo();
	virtual ~MinimalApplicationDemo();

	void Initialize();
	void MainLoop();
	void FreeResources();

private:
	SharedPtr<Camera> _camera;
	SharedPtr<QuitInputReceiver> _inputReceiver;

	static const integer SurfaceCoordsWidth;
	static const integer SurfaceCoordsHeight;
};

//===========================================================================//

}//namespace

#endif // MINIMAL_APPLICATION_DEMO_H

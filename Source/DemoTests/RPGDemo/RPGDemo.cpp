//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RPGDemo.h"

#include "HeroLogic.h"
#include "HeroInputReceiver.h"

namespace Bru
{

//===========================================================================//

const string RPGDemo::GameplayDomainName = "Gameplay";
const string RPGDemo::GameplayGroupName = "Gameplay";
const string RPGDemo::GameplayLayerName = "Gameplay";
const string RPGDemo::HUDLayerName = "HUD";

const integer RPGDemo::SurfaceCoordsWidth = 32;
const integer RPGDemo::SurfaceCoordsHeight = 18;

//===========================================================================//

const integer RPGDemo::TilesCountByWidth = 16;
const integer RPGDemo::TilesCountByHeight = 9;

//===========================================================================//

RPGDemo::RPGDemo() :
	BaseApplication(),
	_floorSceneNodes(),
	_floorSprites(),
	_levelNameSceneNode(),
	_levelNameText(),
	_hero(),
	_inputReceiver(),
	_camera(),
	_mainTheme()
{
}

//===========================================================================//

RPGDemo::~RPGDemo()
{
}

//===========================================================================//

void RPGDemo::Initialize()
{
	Engine = new EngineSingleton(SurfaceCoordsWidth, SurfaceCoordsHeight);

	TextureManager->Load("RPG/Floor");
	TextureManager->Load("RPG/HeroAss");
	TextureManager->Load("RPG/HeroFace");
	TextureManager->Load("RPG/HeroRight");

	ParametersFileManager->Load("AnimatedSprites/RPG/Hero");
	ParametersFileManager->Load("Sprites/RPG/Floor");
	ParametersFileManager->Load("RenderableTexts/RPG/DungeonName");

	WidgetsManager->HideCursor();
	DomainManager->Add(GameplayDomainName);

	WeakPtr<ParametersFile> componentsFile = ParametersFileManager->Get(PathHelper::PathToComponents);
	Updater->SetUpdatingGroups(componentsFile->GetArray<string>("UpdatingGroupsNames"));

	Renderer->AddLayerToBack(GameplayDomainName, new RenderableLayer(HUDLayerName, ProjectionMode::PercentsOrtho));
	Renderer->AddLayerToBack(GameplayDomainName, new RenderableLayer(GameplayLayerName));

	EntityManager = new EntityManagerSingleton(CreateCreatingComponentDelegates(), {GameplayGroupName}, {}, true);
	EntityManager->AddGroup(GameplayGroupName);

	_mainTheme = SoundSystem->CreateSound("MainTheme");
	_mainTheme->Play();

	CreateLocation();

	Vector3 heroPosition = {SurfaceCoordsWidth / 2.0f, SurfaceCoordsHeight / 2.0f, 0.0f};
	_hero = EntityManager->CreateEntity("RPG/Hero", GameplayDomainName, GameplayLayerName, heroPosition);

	_inputReceiver = new QuitInputReceiver(GameplayDomainName);

	_camera = new Camera();
	_camera->Set2DPosition(SurfaceCoordsWidth / 2.0f, SurfaceCoordsHeight / 2.0f, SurfaceCoordsHeight / 2.0f);

	DomainManager->Select(GameplayDomainName);
}

//===========================================================================//

void RPGDemo::MainLoop()
{
	Engine->Update();
}

//===========================================================================//

void RPGDemo::FreeResources()
{
	_camera = NullPtr;

	_inputReceiver = NullPtr;

	_hero = NullPtr;

	_levelNameText = NullPtr;
	_levelNameSceneNode = NullPtr;

	_floorSprites.Clear();
	_floorSceneNodes.Clear();

	_mainTheme = NullPtr;

	EntityManager->ClearGroup(GameplayGroupName);
	EntityManager->RemoveGroup(GameplayGroupName);
	EntityManager = NullPtr;

	Renderer->RemoveLayer(GameplayDomainName, HUDLayerName);
	Renderer->RemoveLayer(GameplayDomainName, GameplayLayerName);

	Updater->SetUpdatingGroups({});

	DomainManager->Remove(GameplayDomainName);

	Engine = NullPtr;
}

//===========================================================================//

void RPGDemo::CreateLocation()
{
	SharedPtr<Sprite> tileForSize = new Sprite(GameplayDomainName, GameplayLayerName, "RPG/Floor");
	const float tileWidth = tileForSize->GetWidth();
	const float tileHeight = tileForSize->GetWidth();

	float y = 0.0f;
	for (integer j = 0; j < TilesCountByHeight; j++)
	{
		float x = 0.0f;
		for (integer i = 0; i < TilesCountByWidth; i++)
		{
			string tileName = string::Format("Floor{0}", i);

			SharedPtr<PositioningNode> tileSceneNode = new PositioningNode(GameplayDomainName);
			tileSceneNode->SetPosition(x, y, 0.0f);
			_floorSceneNodes.Add(tileSceneNode);

			SharedPtr<Sprite> tile = new Sprite(GameplayDomainName, GameplayLayerName, "RPG/Floor");
			tile->SetPositioningNode(tileSceneNode);
			tile->SetRenderingOrder(2);
			_floorSprites.Add(tile);

			x += tileWidth;
		}
		y += tileHeight;
	}

	_levelNameSceneNode = new PositioningNode(GameplayDomainName);

	_levelNameText = new RenderableText(GameplayDomainName, HUDLayerName, "RPG/DungeonName");
	_levelNameText->SetPositioningNode(_levelNameSceneNode);
	_levelNameText->SetRenderingOrder(0);

	_levelNameSceneNode->SetPosition(50.0f, 90.0f, 0.0f);
}

//===========================================================================//

TreeMap<string, EntityManagerSingleton::CreatingComponentDelegate> RPGDemo::CreateCreatingComponentDelegates() const
{
	return
		{
			//Standard components
			EntityManagerSingleton::CreateComponentDelegate<PositioningNode>(),
			EntityManagerSingleton::CreateComponentDelegate<RotationNode>(),
			EntityManagerSingleton::CreateComponentDelegate<LinearMovementController>(),
			EntityManagerSingleton::CreateComponentDelegate<MovementToPointController>(),
			EntityManagerSingleton::CreateComponentDelegate<FloatingMovementToPointController>(),
			EntityManagerSingleton::CreateComponentDelegate<TrajectoryMovementController>(),
			EntityManagerSingleton::CreateComponentDelegate<RotationController>(),
			EntityManagerSingleton::CreateComponentDelegate<RotationToAngleController>(),
			EntityManagerSingleton::CreateComponentDelegate<Sprite>(),
			EntityManagerSingleton::CreateComponentDelegate<AnimatedSprite>(),
			EntityManagerSingleton::CreateComponentDelegate<RenderableText>(),
			EntityManagerSingleton::CreateComponentDelegate<AnimationController>(),
			EntityManagerSingleton::CreateComponentDelegate<SoundEmitter>(),

			//Demo components
			EntityManagerSingleton::CreateComponentDelegate<HeroLogic>(),
			EntityManagerSingleton::CreateComponentDelegate<HeroInputReceiver>()
		};
}

//===========================================================================//

} // namespace Bru

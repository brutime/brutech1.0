//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef HERO_LOGIC_H
#define HERO_LOGIC_H

#include "../../Helpers/Engine/Engine.h"

#include "Enums/HorizontalMovementState.h"
#include "Enums/VerticalMovementState.h"

namespace Bru
{

//===========================================================================//

class HeroLogic : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	HeroLogic(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~HeroLogic();

	virtual void Update(float deltaTime);

	void MoveLeft();
	void MoveRight();
	void MoveDown();
	void MoveUp();

	void StopHorizontal();
	void StopVectical();

	void SetSpeed(float speed);
	float GetSpeed() const;

private:
	static const string PathToSpeed;
	static const string OnMoveKey;

	virtual void OnAddedToOwner();

	void UpdateHorizontalMovement(float deltaTime);
	void UpdateVerticalMovement(float deltaTime);

	void OnStartMoving();
	void OnStop();

	HorizontalMovementState _horizontalMovementState;
	VerticalMovementState _verticalMovementState;
	float _speed;
	bool _isMoving;
	WeakPtr<PositioningNode> _node;
	WeakPtr<BaseSprite> _sprite;
	WeakPtr<AnimationController> _animationController;
	WeakPtr<SoundEmitter> _soundEmitter;

	HeroLogic(const HeroLogic &) = delete;
	HeroLogic & operator =(const HeroLogic &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // HERO_LOGIC_H

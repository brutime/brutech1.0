//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "HeroLogic.h"

#include "RPGDemo.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(HeroLogic, Updatable)

//===========================================================================//

const string HeroLogic::PathToSpeed = "Speed";
const string HeroLogic::OnMoveKey = "OnMove";

//===========================================================================//

HeroLogic::HeroLogic(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	Updatable(domainName, provider),
	_horizontalMovementState(HorizontalMovementState::Stand),
	_verticalMovementState(VerticalMovementState::Stand),
	_speed(provider->Get<float>(PathToSpeed)),
	_isMoving(false),
	_node(),
	_sprite(),
	_animationController(),
	_soundEmitter()
{
}

//===========================================================================//

HeroLogic::~HeroLogic()
{
}

//===========================================================================//

void HeroLogic::Update(float deltaTime)
{
	UpdateHorizontalMovement(deltaTime);
	UpdateVerticalMovement(deltaTime);
}

//===========================================================================//

void HeroLogic::MoveLeft()
{
	_sprite->FlipHorizontal();
	_animationController->SelectAnimation("Side");

	if (_verticalMovementState == VerticalMovementState::Stand &&
	    _horizontalMovementState != HorizontalMovementState::MoveRight)
	{
		OnStartMoving();
	}

	_horizontalMovementState = HorizontalMovementState::MoveLeft;
}

//===========================================================================//

void HeroLogic::MoveRight()
{
	_sprite->UnflipHorizontal();
	_animationController->SelectAnimation("Side");

	if (_verticalMovementState == VerticalMovementState::Stand &&
	    _horizontalMovementState != HorizontalMovementState::MoveLeft)
	{
		OnStartMoving();
	}

	_horizontalMovementState = HorizontalMovementState::MoveRight;
}

//===========================================================================//

void HeroLogic::MoveDown()
{
	_animationController->SelectAnimation("Down");

	if (_horizontalMovementState == HorizontalMovementState::Stand &&
	    _verticalMovementState != VerticalMovementState::MoveUp)
	{
		OnStartMoving();
	}

	_verticalMovementState = VerticalMovementState::MoveDown;
}

//===========================================================================//

void HeroLogic::MoveUp()
{
	_animationController->SelectAnimation("Up");

	if (_horizontalMovementState == HorizontalMovementState::Stand &&
	    _verticalMovementState != VerticalMovementState::MoveDown)
	{
		OnStartMoving();
	}

	_verticalMovementState = VerticalMovementState::MoveUp;
}

//===========================================================================//

void HeroLogic::StopHorizontal()
{
	if (_horizontalMovementState == HorizontalMovementState::Stand)
	{
		return;
	}

	_horizontalMovementState = HorizontalMovementState::Stand;
	if (_verticalMovementState == VerticalMovementState::Stand)
	{
		OnStop();
	}
}

//===========================================================================//

void HeroLogic::StopVectical()
{
	if (_verticalMovementState == VerticalMovementState::Stand)
	{
		return;
	}

	_verticalMovementState = VerticalMovementState::Stand;
	if (_horizontalMovementState == HorizontalMovementState::Stand)
	{
		OnStop();
	}
}

//===========================================================================//

void HeroLogic::SetSpeed(float speed)
{
	_speed = speed;
}

//===========================================================================//

float HeroLogic::GetSpeed() const
{
	return _speed;
}

//===========================================================================//

void HeroLogic::OnAddedToOwner()
{
	_node = GetOwnerComponent<PositioningNode>();
	_sprite = GetOwnerComponent<BaseSprite>();
	
	_animationController = GetOwnerComponent<AnimationController>();
	_animationController->SelectAnimation("Down");
	_animationController->Stop();
	
	_soundEmitter = GetOwnerComponent<SoundEmitter>();
}

//===========================================================================//

void HeroLogic::UpdateHorizontalMovement(float deltaTime)
{
	if (_horizontalMovementState == HorizontalMovementState::MoveLeft)
	{
		_node->Translate(-_speed * deltaTime, 0.0f);
	}
	else if (_horizontalMovementState == HorizontalMovementState::MoveRight)
	{
		_node->Translate(_speed * deltaTime, 0.0f);
	}

	float boundsLeft = 0.0f;
	float boundsRight = RPGDemo::SurfaceCoordsWidth;

	if (_node->GetPosition().X - _sprite->GetOrigin().X < boundsLeft)
	{
		_node->SetPositionX(boundsLeft + _sprite->GetOrigin().X);
	}
	else if (_node->GetPosition().X + _sprite->GetWidth() - _sprite->GetOrigin().X > boundsRight)
	{
		_node->SetPositionX(boundsRight - _sprite->GetWidth() + _sprite->GetOrigin().X);
	}
}

//===========================================================================//

void HeroLogic::UpdateVerticalMovement(float deltaTime)
{
	if (_verticalMovementState == VerticalMovementState::MoveUp)
	{
		_node->Translate(0.0f, _speed * deltaTime);
	}
	else if (_verticalMovementState == VerticalMovementState::MoveDown)
	{
		_node->Translate(0.0f, -_speed * deltaTime);
	}

	float boundsDown = 0.0f;
	float boundsUp = RPGDemo::SurfaceCoordsHeight;

	if (_node->GetPosition().Y - _sprite->GetOrigin().Y < boundsDown)
	{
		_node->SetPositionY(boundsDown + _sprite->GetOrigin().Y);
	}
	else if (_node->GetPosition().Y + _sprite->GetHeight() - _sprite->GetOrigin().Y > boundsUp)
	{
		_node->SetPositionY(boundsUp - _sprite->GetHeight() + _sprite->GetOrigin().Y);
	}
}

//===========================================================================//

void HeroLogic::OnStartMoving()
{
	_animationController->Play();
	_soundEmitter->Play(OnMoveKey);
}

//===========================================================================//

void HeroLogic::OnStop()
{
	_animationController->SelectAnimation("Down");
	_animationController->Stop();
	_soundEmitter->Stop(OnMoveKey);
}

//===========================================================================//

} // namespace Bru

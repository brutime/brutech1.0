//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef HERO_INPUT_RECEIVER_H
#define HERO_INPUT_RECEIVER_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class HeroInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	HeroInputReceiver(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~HeroInputReceiver();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs &);
	virtual void DispatchJoystickEvent(const JoystickEventArgs &);

	virtual void LostFocus();

private:
	HeroInputReceiver(const HeroInputReceiver &) = delete;
	HeroInputReceiver & operator =(const HeroInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // HERO_INPUT_RECEIVER_H

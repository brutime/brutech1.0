//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: Демо спрайтов
//

#pragma once
#ifndef RPG_DEMO_H
#define RPG_DEMO_H

#include "../../Helpers/Engine/Engine.h"
#include "../Common/QuitInputReceiver.h"

namespace Bru
{

//===========================================================================//

class RPGDemo : public BaseApplication
{
	friend class RPGDemoInputReceiver;

public:
	static const string GameplayDomainName;
	static const string GameplayGroupName;
	static const string GameplayLayerName;
	static const string HUDLayerName;

	static const integer SurfaceCoordsWidth;
	static const integer SurfaceCoordsHeight;

	RPGDemo();
	virtual ~RPGDemo();

	void Initialize();
	void MainLoop();
	void FreeResources();

private:
	static const integer TilesCountByWidth;
	static const integer TilesCountByHeight;

	void CreateLocation();
	
	TreeMap<string, EntityManagerSingleton::CreatingComponentDelegate> CreateCreatingComponentDelegates() const;
	
	List<SharedPtr<PositioningNode>> _floorSceneNodes;
	List<SharedPtr<Sprite>> _floorSprites;

	SharedPtr<PositioningNode> _levelNameSceneNode;
	SharedPtr<RenderableText> _levelNameText;

	SharedPtr<Entity> _hero;

	SharedPtr<QuitInputReceiver> _inputReceiver;

	SharedPtr<Camera> _camera;
	SharedPtr<Sound> _mainTheme;
	
	RPGDemo(const RPGDemo &) = delete;
	RPGDemo & operator =(const RPGDemo &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // RPG_DEMO_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "HeroInputReceiver.h"

#include "HeroLogic.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(HeroInputReceiver, InputReceiver)

//===========================================================================//

HeroInputReceiver::HeroInputReceiver(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	InputReceiver(domainName, provider)
{
}

//===========================================================================//

HeroInputReceiver::~HeroInputReceiver()
{
}

//===========================================================================//

void HeroInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	const auto & heroLogic = GetOwnerComponent<HeroLogic>();

	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::Left:
			heroLogic->MoveLeft();
			break;

		case KeyboardKey::Right:
			heroLogic->MoveRight();
			break;

		case KeyboardKey::Down:
			heroLogic->MoveDown();
			break;

		case KeyboardKey::Up:
			heroLogic->MoveUp();
			break;

		default:
			break;
		}
	}
	else if (args.State == KeyState::Up)
	{
		switch (args.Key)
		{
		case KeyboardKey::Left:
			if (!Keyboard->IsKeyDown(KeyboardKey::Right))
			{
				heroLogic->StopHorizontal();
			}
			else
			{
				heroLogic->MoveRight();
			}
			break;

		case KeyboardKey::Right:
			if (!Keyboard->IsKeyDown(KeyboardKey::Left))
			{
				heroLogic->StopHorizontal();
			}
			else
			{
				heroLogic->MoveLeft();
			}
			break;

		case KeyboardKey::Down:
			if (!Keyboard->IsKeyDown(KeyboardKey::Up))
			{
				heroLogic->StopVectical();
			}
			else
			{
				heroLogic->MoveUp();
			}
			break;

		case KeyboardKey::Up:
			if (!Keyboard->IsKeyDown(KeyboardKey::Down))
			{
				heroLogic->StopVectical();
			}
			else
			{
				heroLogic->MoveDown();
			}
			break;

		default:
			break;
		}
	}
}

//===========================================================================//

void HeroInputReceiver::DispatchMouseEvent(const MouseEventArgs &)
{
}

//===========================================================================//

void HeroInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

void HeroInputReceiver::LostFocus()
{
	const auto & heroLogic = GetOwnerComponent<HeroLogic>();
	heroLogic->StopHorizontal();
	heroLogic->StopVectical();
	
	InputReceiver::LostFocus();
}

//===========================================================================//

} // namespace Bru

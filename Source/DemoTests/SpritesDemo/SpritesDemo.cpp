//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SpritesDemo.h"

namespace Bru
{

//===========================================================================//

const string SpritesDemo::DomainName = "RotationDemo";
const string SpritesDemo::LayerName = "RotationDemo";

//===========================================================================//

const integer SpritesDemo::SurfaceCoordsWidth = 16;
const integer SpritesDemo::SurfaceCoordsHeight = 9;

//===========================================================================//

SpritesDemo::SpritesDemo() :
	BaseApplication(),
	_notAnimatedUnitSceneNode(),
	_notAnimatedUnitRenderable(),
	_animatedUnitSceneNode(),
	_animatedUnitRenderable(),
	_animatedUnitController(),
	_normalSceneNode(),
	_normalAnimatedSprite(),
	_normalAnimatedController(),
	_reverseSceneNode(),
	_reverseAnimatedSprite(),
	_reverseAnimatedController(),
	_pingPongSceneNode(),
	_pingPongAnimatedSprite(),
	_pingPongAnimatedController(),
	_inputReceiver(),
	_camera()
{
}

//===========================================================================//

SpritesDemo::~SpritesDemo()
{
}

//===========================================================================//

void SpritesDemo::Initialize()
{
	Engine = new EngineSingleton(SurfaceCoordsWidth, SurfaceCoordsHeight);

	TextureManager->Load("AnimatedUnit");
	TextureManager->Load("NotAnimatedUnit");
	TextureManager->Load("Shield");

	ParametersFileManager->Load("AnimatedSprites/AnimatedUnit");
	ParametersFileManager->Load("Sprites/NotAnimatedUnit");
	ParametersFileManager->Load("AnimatedSprites/Shield");

	DomainManager->Add(DomainName);

	Renderer->AddLayerToBack(DomainName, new RenderableLayer(LayerName));

	_notAnimatedUnitSceneNode = new PositioningNode(DomainName);
	_notAnimatedUnitSceneNode->SetPosition(5.5f, 5.5f, 0.0f);

	_notAnimatedUnitRenderable = new Sprite(DomainName, LayerName, "NotAnimatedUnit");
	_notAnimatedUnitRenderable->SetPositioningNode(_notAnimatedUnitSceneNode);

	_animatedUnitSceneNode = new PositioningNode(DomainName);
	_animatedUnitSceneNode->SetPosition(10.0f, 5.5f, 0.0f);

	_animatedUnitRenderable = new AnimatedSprite(DomainName, LayerName, "AnimatedUnit");
	_animatedUnitRenderable->SetPositioningNode(_animatedUnitSceneNode);

	_animatedUnitController = new AnimationController(_animatedUnitRenderable);
	_animatedUnitController->SelectAnimation("Run");

	AddAnimatedSprite("Normal", _normalSceneNode, _normalAnimatedSprite, _normalAnimatedController, 4.8671875f, 2.3671875f);
	AddAnimatedSprite("Reverse", _reverseSceneNode, _reverseAnimatedSprite, _reverseAnimatedController, 7.8671875f, 2.3671875f);
	AddAnimatedSprite("PingPong", _pingPongSceneNode, _pingPongAnimatedSprite, _pingPongAnimatedController,10.8671875f, 2.3671875f);
	
	WidgetsManager->HideCursor();	

	_inputReceiver = new SpritesDemoInputReceiver(DomainName);

	_camera = new Camera();
	_camera->Set2DPosition(SurfaceCoordsWidth / 2.0f, SurfaceCoordsHeight / 2.0f, SurfaceCoordsHeight / 2.0f);
	
	DomainManager->Select(DomainName);
}

//===========================================================================//

void SpritesDemo::MainLoop()
{
	Engine->Update();
}

//===========================================================================//

void SpritesDemo::FreeResources()
{
	_camera = NullPtr;
	_inputReceiver = NullPtr;

	_pingPongAnimatedController = NullPtr;
	_pingPongAnimatedSprite = NullPtr;
	_pingPongSceneNode = NullPtr;

	_reverseAnimatedController = NullPtr;
	_reverseAnimatedSprite = NullPtr;
	_reverseSceneNode = NullPtr;

	_normalAnimatedController = NullPtr;
	_normalAnimatedSprite = NullPtr;
	_normalSceneNode = NullPtr;

	_animatedUnitController = NullPtr;
	_animatedUnitRenderable = NullPtr;
	_animatedUnitSceneNode = NullPtr;

	_notAnimatedUnitRenderable = NullPtr;
	_notAnimatedUnitSceneNode = NullPtr;

	Renderer->RemoveLayer(DomainName, LayerName);
	DomainManager->Remove(DomainName);

	Engine = NullPtr;
}

//===========================================================================//

void SpritesDemo::FlipHorizontal()
{
	_animatedUnitRenderable->FlipHorizontal();
	_notAnimatedUnitRenderable->FlipHorizontal();
}

//===========================================================================//

void SpritesDemo::UnflipHorizontal()
{
	_animatedUnitRenderable->UnflipHorizontal();
	_notAnimatedUnitRenderable->UnflipHorizontal();
}

//===========================================================================//

void SpritesDemo::FlipVertical()
{
	_animatedUnitRenderable->FlipVertical();
	_notAnimatedUnitRenderable->FlipVertical();
}

//===========================================================================//

void SpritesDemo::UnflipVertical()
{
	_animatedUnitRenderable->UnflipVertical();
	_notAnimatedUnitRenderable->UnflipVertical();
}

//===========================================================================//

void SpritesDemo::AddAnimatedSprite(const string & animationName, SharedPtr<PositioningNode> & sceneObject,
                                    SharedPtr<AnimatedSprite> & animatedSprite, 
									SharedPtr<AnimationController> & animationController, float x, float y)
{
	sceneObject = new PositioningNode(DomainName);
	sceneObject->SetPosition(x, y, 0.0f);

	animatedSprite = new AnimatedSprite(DomainName, LayerName, "Shield");
	animatedSprite->SelectAnimation(animationName);
	animatedSprite->SetPositioningNode(sceneObject);

	animationController = new AnimationController(animatedSprite);
}

//===========================================================================//

}// namespace Bru

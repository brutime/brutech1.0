//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SPRITES_DEMO_INPUT_RECEIVER_H
#define SPRITES_DEMO_INPUT_RECEIVER_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class SpritesDemoInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	SpritesDemoInputReceiver(const string & domainName);
	virtual ~SpritesDemoInputReceiver();
	
	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs &);
	virtual void DispatchJoystickEvent(const JoystickEventArgs &);

private:
	SpritesDemoInputReceiver(const SpritesDemoInputReceiver &) = delete;
	SpritesDemoInputReceiver & operator =(const SpritesDemoInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // SPRITES_DEMO_INPUT_RECEIVER_H

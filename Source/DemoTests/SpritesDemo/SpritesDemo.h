//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: спрайты
//

#pragma once
#ifndef SPRITES_DEMO_H
#define SPRITES_DEMO_H

#include "../../Helpers/Engine/Engine.h"
#include "SpritesDemoInputReceiver.h"

namespace Bru
{

//===========================================================================//

class SpritesDemo : public BaseApplication
{
public:
	static const string DomainName;
	static const string LayerName;

	SpritesDemo();
	virtual ~SpritesDemo();

	void Initialize();
	void MainLoop();
	void FreeResources();

	void FlipHorizontal();
	void UnflipHorizontal();

	void FlipVertical();
	void UnflipVertical();

private:
	static const integer SurfaceCoordsWidth;
	static const integer SurfaceCoordsHeight;

	void AddAnimatedSprite(const string & animationName, SharedPtr<PositioningNode> & sceneObject,
	                       SharedPtr<AnimatedSprite> & animatedSprite,
	                       SharedPtr<AnimationController> & animationController, float x, float y);

	SharedPtr<PositioningNode> _notAnimatedUnitSceneNode;
	SharedPtr<Sprite> _notAnimatedUnitRenderable;

	SharedPtr<PositioningNode> _animatedUnitSceneNode;
	SharedPtr<AnimatedSprite> _animatedUnitRenderable;
	SharedPtr<AnimationController> _animatedUnitController;

	SharedPtr<PositioningNode> _normalSceneNode;
	SharedPtr<AnimatedSprite> _normalAnimatedSprite;
	SharedPtr<AnimationController> _normalAnimatedController;

	SharedPtr<PositioningNode> _reverseSceneNode;
	SharedPtr<AnimatedSprite> _reverseAnimatedSprite;
	SharedPtr<AnimationController> _reverseAnimatedController;

	SharedPtr<PositioningNode> _pingPongSceneNode;
	SharedPtr<AnimatedSprite> _pingPongAnimatedSprite;
	SharedPtr<AnimationController> _pingPongAnimatedController;

	SharedPtr<SpritesDemoInputReceiver> _inputReceiver;

	SharedPtr<Camera> _camera;

	SpritesDemo(const SpritesDemo &) = delete;
	SpritesDemo & operator =(const SpritesDemo &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // SPRITES_DEMO_H

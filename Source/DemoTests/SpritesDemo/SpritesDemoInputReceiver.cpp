//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SpritesDemoInputReceiver.h"

#include "SpritesDemo.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(SpritesDemoInputReceiver, InputReceiver)

//===========================================================================//

SpritesDemoInputReceiver::SpritesDemoInputReceiver(const string & domainName) :
	InputReceiver(domainName)
{
}

//===========================================================================//

SpritesDemoInputReceiver::~SpritesDemoInputReceiver()
{
}

//===========================================================================//

void SpritesDemoInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	SharedPtr<SpritesDemo> demo = static_pointer_cast<SpritesDemo>(Application);

	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::Left:
			demo->FlipHorizontal();
			break;

		case KeyboardKey::Right:
			demo->UnflipHorizontal();
			break;

		case KeyboardKey::Up:
			demo->FlipVertical();
			break;

		case KeyboardKey::Down:
			demo->UnflipVertical();
			break;

		case KeyboardKey::Escape:
			demo->Quit();
			break;

		default:
			break;
		}
	}
}

//===========================================================================//

void SpritesDemoInputReceiver::DispatchMouseEvent(const MouseEventArgs &)
{
}

//===========================================================================//

void SpritesDemoInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

} // namespace Bru

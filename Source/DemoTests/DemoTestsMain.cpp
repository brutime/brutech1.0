//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: Входная точка демок
//

#include "../Helpers/Engine/Engine.h"

#include "WidgetsDemo/WidgetsDemo.h"
#include "MinimalApplicationDemo/MinimalApplicationDemo.h"
#include "PrimitivesDemo/PrimitivesDemo.h"
#include "RotationDemo/RotationDemo.h"
#include "RPGDemo/RPGDemo.h"
#include "SpritesDemo/SpritesDemo.h"
#include "StressTestDemo/StressTestDemo.h"

using namespace Bru;

//===========================================================================//

int main(int, char const *[])
{
	//	Application = new MinimalApplicationDemo();
	//	Application = new PrimitivesDemo();
	//	Application = new RotationDemo();
	//	Application = new SpritesDemo();
	//	Application = new StressTestDemo();
	//	Application = new WidgetsDemo();
	Application = new RPGDemo();

	try
	{
		Application->ApplicationMain();
	}
	catch (Exception & exception)
	{
		OSConsoleStream->WriteLine("Exception occurred:");
		OSConsoleStream->WriteLine(string::Format("type:    {0}", exception.GetType()));
		OSConsoleStream->WriteLine(string::Format("details: {0}", exception.GetDetails()));
		OSConsoleStream->WriteLine(string::Format("file:    {0}", PathHelper::GetFileName(exception.GetFileName(), 2)));
		OSConsoleStream->WriteLine(string::Format("method:  {0}", exception.GetMethodName()));		
		OSConsoleStream->WriteLine(string::Format("line:    {0}", exception.GetFileLine()));	
		Application->FreeResources();
	}

	return 0;
}

//===========================================================================//

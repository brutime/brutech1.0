//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "QuitInputReceiver.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(QuitInputReceiver, InputReceiver)

//===========================================================================//

QuitInputReceiver::QuitInputReceiver(const string & domainName) :
	InputReceiver(domainName)
{
}

//===========================================================================//

QuitInputReceiver::~QuitInputReceiver()
{
}

//===========================================================================//

void QuitInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::Escape:
			Application->Quit();
			break;

		default:
			break;
		}
	}
}

//===========================================================================//

void QuitInputReceiver::DispatchMouseEvent(const MouseEventArgs &)
{
}

//===========================================================================//

void QuitInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

} // namespace Bru

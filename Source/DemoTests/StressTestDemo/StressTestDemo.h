//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef STRESS_TEST_DEMO_H
#define STRESS_TEST_DEMO_H

#include "../../Helpers/Engine/Engine.h"
#include "StressTestDemoInputReceiver.h"

namespace Bru
{

//===========================================================================//

class StressTestDemo : public BaseApplication
{
public:
	static const integer SurfaceCoordsWidth;
	static const integer SurfaceCoordsHeight;

	static const string DomainName;
	static const string LayerName;

	StressTestDemo();
	virtual ~StressTestDemo();

	void Initialize();
	void MainLoop();
	void FreeResources();

	void AddUnits(integer count);
	void RemoveUnits(integer count);

private:
	BlendFunction GetRandomBlendFunction() const;

	Array<SharedPtr<Entity>> _units;
	SharedPtr<StressTestDemoInputReceiver> _inputReceiver;
	SharedPtr<Camera> _camera;
};

//===========================================================================//

} // namespace Bru

#endif // STRESS_TEST_DEMO_H

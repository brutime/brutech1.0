//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StressTestDemo.h"

#include "StressTestUnitLogic.h"

namespace Bru
{

//===========================================================================//

const integer StressTestDemo::SurfaceCoordsWidth = 16;
const integer StressTestDemo::SurfaceCoordsHeight = 9;

const string StressTestDemo::DomainName = "StressTestDemo";
const string StressTestDemo::LayerName = "StressTestDemo";

//===========================================================================//

StressTestDemo::StressTestDemo() :
	BaseApplication(),
	_units(),
	_inputReceiver(),
	_camera()
{
}

//===========================================================================//

StressTestDemo::~StressTestDemo()
{
}

//===========================================================================//

void StressTestDemo::Initialize()
{
	Engine = new EngineSingleton(SurfaceCoordsWidth, SurfaceCoordsHeight);

	float startTime = GlobalTimer->GetSeconds();

	TextureManager->Load("Grass/Grass1");
	TextureManager->Load("Grass/Grass2");
	TextureManager->Load("Grass/Grass3");
	TextureManager->Load("Grass/Grass4");
	TextureManager->Load("Grass/Grass5");
	TextureManager->Load("Grass/Grass6");
	TextureManager->Load("Grass/Grass7");

	ParametersFileManager->Load("Sprites/Grass/Grass1");
	ParametersFileManager->Load("Sprites/Grass/Grass2");
	ParametersFileManager->Load("Sprites/Grass/Grass3");
	ParametersFileManager->Load("Sprites/Grass/Grass4");
	ParametersFileManager->Load("Sprites/Grass/Grass5");
	ParametersFileManager->Load("Sprites/Grass/Grass6");
	ParametersFileManager->Load("Sprites/Grass/Grass7");

	float endTime = GlobalTimer->GetSeconds();
	Console->Notice(string::Format("Loading resources time: {0:0.3} s", endTime - startTime));

	DomainManager->Add(DomainName);

	Renderer->AddLayerToBack(DomainName, new RenderableLayer(LayerName));

	WidgetsManager->HideCursor();

	_inputReceiver = new StressTestDemoInputReceiver(DomainName);

	_camera = new Camera();
	_camera->Set2DPosition(SurfaceCoordsWidth / 2.0f, SurfaceCoordsHeight / 2.0f, SurfaceCoordsHeight / 2.0f);

	DomainManager->Select(DomainName);

	startTime = GlobalTimer->GetSeconds();

	AddUnits(100);

	endTime = GlobalTimer->GetSeconds();
	Console->Notice(string::Format("Creating time: {0:0.3} s", endTime - startTime));
}

//===========================================================================//

void StressTestDemo::MainLoop()
{
	Engine->Update();
}

//===========================================================================//

void StressTestDemo::FreeResources()
{
	_camera = NullPtr;

	_inputReceiver = NullPtr;

	_units.Clear();

	Renderer->RemoveLayer(DomainName, LayerName);
	DomainManager->Remove(DomainName);

	Engine = NullPtr;
}

//===========================================================================//

void StressTestDemo::AddUnits(integer count)
{
	integer startIndex = _units.GetCount();
	_units.Resize(_units.GetCount() + count);

	for (integer i = 0; i < count; ++i)
	{		
		SharedPtr<PositioningNode> node = new PositioningNode(DomainName);
		node->SetPosition(Math::RandomInRange(0.0f, static_cast<float>(SurfaceCoordsWidth)),
		                  Math::RandomInRange(0.0f, static_cast<float>(SurfaceCoordsHeight)));

		SharedPtr<RotationNode> rotationNode = new RotationNode(DomainName);
		rotationNode->Rotate2D(Math::Random(360.0f));

		//string pathToFile = string::Format("Grass/Grass{0}", static_cast<integer>(Math::RandomInRange(1.0f, 7.0f)));
		string pathToFile = "Zazaka";
		SharedPtr<Sprite> sprite = new Sprite(DomainName, LayerName, pathToFile);
		sprite->SetOriginFactor({0.5f, 0.5f, 0.0f});
		float randomFactor = Math::RandomInRange(0.5f, 2.0f);
		sprite->SetWidth(sprite->GetWidth() * randomFactor);
		sprite->SetHeight(sprite->GetHeight() * randomFactor);

		SharedPtr<StressTestUnitLogic> unitLogic = new StressTestUnitLogic(DomainName);

		auto & unit = _units[startIndex + i];
		unit = new Entity("Unit", "Units");
		unit->AddComponent(node);
		unit->AddComponent(rotationNode);
		unit->AddComponent(sprite);
		unit->AddComponent(unitLogic);
	}
}

//===========================================================================//

void StressTestDemo::RemoveUnits(integer count)
{
	if (_units.GetCount() - count < 0)
	{
		return;
	}

	_units.Resize(_units.GetCount() - count);
}

//===========================================================================//

BlendFunction StressTestDemo::GetRandomBlendFunction() const
{
	integer randomIndex = Math::Random(14);
	Console->Hint(randomIndex);
	return static_cast<BlendFunction>(randomIndex);
}

//===========================================================================//

} // namespace Bru

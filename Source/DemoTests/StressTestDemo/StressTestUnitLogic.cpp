//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StressTestUnitLogic.h"

#include "StressTestDemo.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(StressTestUnitLogic, Updatable)

//===========================================================================//

StressTestUnitLogic::StressTestUnitLogic(const string & domainName) :
	Updatable(domainName),
	_direction(Math::RandomInRange(-1.0f, 1.0f), Math::RandomInRange(-1.0f, 1.0f)),
	_speed(Math::RandomInRange(1.0f, 3.0f)),
	_rotationSpeed(Math::RandomInRange(50.0f, 100.0f)),
	_sizeFactor(Math::RandomInRange(0.5f, 1.0f))
{
}

//===========================================================================//

StressTestUnitLogic::~StressTestUnitLogic()
{
}

//===========================================================================//

void StressTestUnitLogic::Update(float deltaTime)
{
	const auto & node = GetOwnerComponent<PositioningNode>();

	node->Translate(_direction * _speed * deltaTime);
	if (node->GetPosition().X < -1.0f || node->GetPosition().X > StressTestDemo::SurfaceCoordsWidth + 1.0f)
	{
		_direction.NegativeX();
	}
	else if (node->GetPosition().Y < -1.0f || node->GetPosition().Y > StressTestDemo::SurfaceCoordsHeight + 1.0f)
	{
		_direction.NegativeY();
	}

	const auto & rotationNode = GetOwnerComponent<RotationNode>();

	rotationNode->Rotate2D(_rotationSpeed * deltaTime);

	const auto & baseSprite = GetOwnerComponent<BaseSprite>();

	baseSprite->SetSize (baseSprite->GetWidth() * (1.0f + _sizeFactor * deltaTime),
	                     baseSprite->GetHeight() * (1.0f + _sizeFactor * deltaTime));
	if (baseSprite->GetWidth() < 0.5f || baseSprite->GetWidth() > 2.0f ||
	    baseSprite->GetHeight() < 0.5f || baseSprite->GetHeight() > 2.0f)
	{
		_sizeFactor = -_sizeFactor;
	}
}

//===========================================================================//

} // namespace Bru

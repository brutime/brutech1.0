//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StressTestDemoInputReceiver.h"

#include "StressTestDemo.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(StressTestDemoInputReceiver, InputReceiver)

//===========================================================================//

StressTestDemoInputReceiver::StressTestDemoInputReceiver(const string & domainName) :
	InputReceiver(domainName)
{
}

//===========================================================================//

StressTestDemoInputReceiver::~StressTestDemoInputReceiver()
{
}

//===========================================================================//

void StressTestDemoInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	SharedPtr<StressTestDemo> demo = static_pointer_cast<StressTestDemo>(Application);

	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::Up:
			demo->AddUnits(100);
			break;

		case KeyboardKey::Down:
			demo->RemoveUnits(100);
			break;

		case KeyboardKey::Escape:
			demo->Quit();
			break;

		default:
			break;
		}
	}
}

//===========================================================================//

void StressTestDemoInputReceiver::DispatchMouseEvent(const MouseEventArgs &)
{
}

//===========================================================================//

void StressTestDemoInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

}         // namespace Bru

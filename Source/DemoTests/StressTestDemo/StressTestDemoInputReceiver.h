//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef STRESS_TEST_DEMO_INPUT_RECEIVER_H
#define STRESS_TEST_DEMO_INPUT_RECEIVER_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class StressTestDemoInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	StressTestDemoInputReceiver(const string & domainName);
	virtual ~StressTestDemoInputReceiver();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs &);
	virtual void DispatchJoystickEvent(const JoystickEventArgs &);

private:
	StressTestDemoInputReceiver(const StressTestDemoInputReceiver &) = delete;
	StressTestDemoInputReceiver & operator =(const StressTestDemoInputReceiver &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // STRESS_TEST_DEMO_INPUT_RECEIVER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#pragma once
#ifndef STRESS_TEST_UNIT_LOGIC_H
#define STRESS_TEST_UNIT_LOGIC_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class StressTestUnitLogic : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	StressTestUnitLogic(const string & domainName);
	virtual ~StressTestUnitLogic();
	
	virtual void Update(float deltaTime);

private:
	Vector3 _direction;
	float _speed;
	float _rotationSpeed;
	float _sizeFactor;

	StressTestUnitLogic(const StressTestUnitLogic &);
	StressTestUnitLogic & operator =(const StressTestUnitLogic &);
};

//===========================================================================//

} // namespace Bru

#endif // STRESS_TEST_UNIT_LOGIC_H

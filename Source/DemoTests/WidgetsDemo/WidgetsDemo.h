//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: Демо с элементами интерфейса
//

#pragma once
#ifndef WIDGETS_DEMO_H
#define WIDGETS_DEMO_H

#include "../../Helpers/Engine/Engine.h"
#include "../Common/QuitInputReceiver.h"

namespace Bru
{

//===========================================================================//

class WidgetsDemo : public BaseApplication
{
public:
	static const string DomainName;
	static const string LayerName;

	WidgetsDemo();
	virtual ~WidgetsDemo();

	void Initialize();
	void MainLoop();
	void FreeResources();

private:
	static const integer SurfaceCoordsWidth;
	static const integer SurfaceCoordsHeight;

	void OnInputSubmittedClick(const string &);
	void OnTestButtonClick(const Widget *, const MouseEventArgs & args);

	SharedPtr<Frame> _frame;
	SharedPtr<Label> _frameLabel;
	SharedPtr<Label> _nameLabel;
	SharedPtr<Label> _storyLabel;
	SharedPtr<InputBox> _inputBox;
	SharedPtr<Button> _button;

	SharedPtr<QuitInputReceiver> _inputReceiver;

	SharedPtr<Camera> _camera;

	SharedPtr<EventHandler<const string &>> _inputSubmittedHandler;
	SharedPtr<EventHandler<const Widget*, const MouseEventArgs &>> _buttonClickHandler;
};

//===========================================================================//

}// namespace Bru

#endif // WIDGETS_DEMO_H

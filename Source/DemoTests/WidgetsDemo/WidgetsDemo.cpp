//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WidgetsDemo.h"

namespace Bru
{

//===========================================================================//

const string WidgetsDemo::DomainName = "WidgetsDemo";
const string WidgetsDemo::LayerName = "WidgetsDemo";

//===========================================================================//

const integer WidgetsDemo::SurfaceCoordsWidth = 800;
const integer WidgetsDemo::SurfaceCoordsHeight = 600;

//===========================================================================//

WidgetsDemo::WidgetsDemo() :
	BaseApplication(),
	_frame(),
	_frameLabel(),
	_nameLabel(),
	_storyLabel(),
	_inputBox(),
	_button(),
	_inputReceiver(),
	_camera(),
	_inputSubmittedHandler(new EventHandler<const string &>(this, &WidgetsDemo::OnInputSubmittedClick)),
	_buttonClickHandler(new EventHandler<const Widget*, const MouseEventArgs &>(this, &WidgetsDemo::OnTestButtonClick))
{
}

//===========================================================================//

WidgetsDemo::~WidgetsDemo()
{
}

//===========================================================================//

void WidgetsDemo::Initialize()
{
	Engine = new EngineSingleton(SurfaceCoordsWidth, SurfaceCoordsHeight);

	TextureManager->Load("WidgetsDemo/Wood");
	TextureManager->Load("WidgetsDemo/ButtonNormal");
	TextureManager->Load("WidgetsDemo/ButtonPressed");
	TextureManager->Load("WidgetsDemo/ButtonHovered");

	WeakPtr<Font> largeFont = FontManager->Load("Arial", 20);
	WeakPtr<Font> smallFont = FontManager->Load("Arial", 14);

	DomainManager->Add(DomainName);

	Renderer->AddLayerToFront(DomainName, new RenderableLayer(LayerName, ProjectionMode::PercentsOrtho));

	WidgetsManager->HideCursor();

	_frame = new Frame(DomainName, LayerName, 80.0f, 80.0f, Colors::DarkBrown);
	_frame->Move(10.0f, 10.0f);

	_frameLabel = new Label(DomainName, LayerName, "Настройки персонажа", largeFont, Colors::Gray90);
	_frameLabel->Move(40.0f - _frameLabel->GetSize().X / 2.0f, 70.0f);
	_frame->AddChild(_frameLabel);

	_nameLabel = new Label(DomainName, LayerName, "Имя", smallFont, Colors::Gray90);
	_nameLabel->Move(_frameLabel->GetPosition().X, 60.0);
	_frame->AddChild(_nameLabel);

	_storyLabel = new Label(DomainName, LayerName, string::Empty, smallFont, Colors::Gray90);
	_frame->AddChild(_storyLabel);

	_inputBox = new InputBox(DomainName, LayerName, 0.0f, 0.0f, InputCursorOrientation::Vertical, smallFont, Colors::Gray90);
	_inputBox->Move(_nameLabel->GetPosition().X + _nameLabel->GetSize().X + 1.0f, 60.0f);
	_inputBox->InputSubmitted.AddHandler(_inputSubmittedHandler);
	_frame->AddChild(_inputBox);

	_button = new Button(DomainName, LayerName, 20.0f, 16.5f, "WidgetsDemo/ButtonNormal", "WidgetsDemo/ButtonPressed", "WidgetsDemo/ButtonHovered");
	_button->Move(40.0f - _button->GetSize().X / 2.0f, 20.0f);
	_button->Click.AddHandler(_buttonClickHandler);
	_frame->AddChild(_button);

	_inputReceiver = new QuitInputReceiver(DomainName);

	_camera = new Camera();
	_camera->Set2DPosition(0.0f, 0.0f, 0.0f);

	DomainManager->Select(DomainName);
}

//===========================================================================//

void WidgetsDemo::MainLoop()
{
	Engine->Update();
}

//===========================================================================//

void WidgetsDemo::FreeResources()
{
	_camera = NullPtr;
	_inputReceiver = NullPtr;
	_button->Click.RemoveHandler(_buttonClickHandler);
	_button = NullPtr;
	_inputBox->InputSubmitted.RemoveHandler(_inputSubmittedHandler);
	_inputBox = NullPtr;
	_storyLabel = NullPtr;
	_nameLabel = NullPtr;
	_frameLabel = NullPtr;
	_frame = NullPtr;

	Renderer->RemoveLayer(DomainName, LayerName);
	DomainManager->Remove(DomainName);

	Engine = NullPtr;
}

//===========================================================================//

void WidgetsDemo::OnTestButtonClick(const Widget *, const MouseEventArgs &)
{
	Quit();
}

//===========================================================================//

void WidgetsDemo::OnInputSubmittedClick(const string &)
{
	string story = string::Format("Новое имя: \"{0}\"", _inputBox->GetText());
	_storyLabel->SetText(story);
	_storyLabel->MoveTo(_frameLabel->GetPosition().X, 55.0f);
}

//===========================================================================//

} // namespace Bru

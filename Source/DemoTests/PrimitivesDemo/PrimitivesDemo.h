//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: Демо с использовангие геометрических примитивов
//

#pragma once
#ifndef PRIMITIVES_DEMO_H
#define PRIMITIVES_DEMO_H

#include "../../Helpers/Engine/Engine.h"
#include "../Common/QuitInputReceiver.h"

namespace Bru
{

//===========================================================================//

class PrimitivesDemo : public BaseApplication
{
public:
	static const string DomainName;
	static const string LayerName;

	PrimitivesDemo();
	virtual ~PrimitivesDemo();

	void Initialize();
	void MainLoop();
	void FreeResources();

private:
	static const integer SurfaceCoordsWidth;
	static const integer SurfaceCoordsHeight;

	SharedPtr<PrimitivesContainer> _primitiveContainer;
	
	SharedPtr<QuitInputReceiver> _inputReceiver;

	SharedPtr<Camera> _camera;
};

//===========================================================================//

} // namespace Bru

#endif // PRIMITIVES_DEMO_H

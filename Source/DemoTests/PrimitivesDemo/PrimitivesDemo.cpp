//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PrimitivesDemo.h"

namespace Bru
{

//===========================================================================//

const string PrimitivesDemo::DomainName = "PrimitivesDemo";
const string PrimitivesDemo::LayerName = "PrimitivesDemo";

//===========================================================================//

const integer PrimitivesDemo::SurfaceCoordsWidth = 40;
const integer PrimitivesDemo::SurfaceCoordsHeight = 30;

//===========================================================================//

PrimitivesDemo::PrimitivesDemo() :
	BaseApplication(),
	_primitiveContainer(),
	_inputReceiver(),
	_camera()
{
}

//===========================================================================//

PrimitivesDemo::~PrimitivesDemo()
{
}

//===========================================================================//

void PrimitivesDemo::Initialize()
{
	Engine = new EngineSingleton(SurfaceCoordsWidth, SurfaceCoordsHeight);
	Renderer->SetClearColor(Colors::Black);

	DomainManager->Add(DomainName);

	Renderer->AddLayerToFront(DomainName, new RenderableLayer(LayerName));

	WidgetsManager->HideCursor();

	_primitiveContainer = new PrimitivesContainer(DomainName, LayerName);
		
	_primitiveContainer->AddLine( {{0.0f, 15.0f}, {40.0f, 15.0f}}, Colors::LightGreen, 3.0f);
	_primitiveContainer->AddLine( {{20.0f, 0.0f}, {20.0f, 30.0f}}, Colors::LightGreen, 3.0f);
	
	_primitiveContainer->AddPoints( {{2.0f, 2.0f}, {2.0f, 13.0f}, {18.0f, 13.0f}, {18.0f, 2.0f}}, Colors::White, 3.0f);
	_primitiveContainer->AddPoints( {{10.0f, 7.5f}}, Colors::Gray50, 5.0f);

	_primitiveContainer->AddRectangle( {22.0f, 2.0f}, 10.0f, 8.0f, true, Colors::White, 3.0f);
	_primitiveContainer->AddRectangle( {28.0f, 5.0f}, 10.0f, 8.0f, false, Colors::Gray50, 3.0f);

	_primitiveContainer->AddCircle( {7.0f, 22.5f}, 5.0f, true, Colors::White);
	_primitiveContainer->AddCircle( {13.0f, 22.5f}, 5.0f, false, Colors::Gray50, 3.0f);

	Array<Vector3> filledVertices = {{22.0f, 22.5f}, {26.0f, 19.0f}, {28.0f, 19.0f}, {32.0f, 22.5f}, {28.0f, 26.0f}, {26.0f, 26.0f}};
	_primitiveContainer->AddPolygon(filledVertices, true, Colors::White);
	
	Array<Vector3> notFilledVertices = {{28.0f, 22.5f}, {32.0f, 19.0f}, {34.0f, 19.0f}, {38.0f, 22.5f}, {34.0f, 26.0f}, {32.0f, 26.0f}};
	_primitiveContainer->AddPolygon(notFilledVertices, false, Colors::Gray50, 3.0f);

	_inputReceiver = new QuitInputReceiver(DomainName);

	_camera = new Camera();
	_camera->Set2DPosition(SurfaceCoordsWidth / 2.0f, SurfaceCoordsHeight / 2.0f, SurfaceCoordsHeight / 2.0f);

	DomainManager->Select(DomainName);
}

//===========================================================================//

void PrimitivesDemo::MainLoop()
{
	Engine->Update();
}

//===========================================================================//

void PrimitivesDemo::FreeResources()
{
	_camera = NullPtr;
	_inputReceiver = NullPtr;
	_primitiveContainer = NullPtr;

	Renderer->RemoveLayer(DomainName, LayerName);
	DomainManager->Remove(DomainName);

	Engine = NullPtr;
}

//===========================================================================//

} // namespace Bru

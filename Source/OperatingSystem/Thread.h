//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: интерфейс потока
//

#pragma once
#ifndef ITHREAD_H
#define ITHREAD_H

#define _PTHREADS

#if PLATFORM == WINDOWS
#include "Windows/Thread.h"
#elif PLATFORM == MAC
//no implementation
#elif PLATFORM == LINUX
#include "Linux/Thread.h"
#else
#error Platform not defined (WINDOWS,MAC,LINUX)
#endif

#endif // ITHREAD_H

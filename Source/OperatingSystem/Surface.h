//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс окна
//

#pragma once
#ifndef SURFACE_H
#define SURFACE_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "OperatingSystemTypes.h"

namespace Bru
{

//===========================================================================//

class SurfaceSingleton
{
public:
	SurfaceSingleton(integer width, integer height, const Color & color);
	virtual ~SurfaceSingleton();

	void Minimize();
	void Normalize();

	surface_handle GetHandle() const;

	void SetPosition(const IntVector2 & position);
	void SetPosition(integer positionX, integer positionY);
	const IntVector2 & GetPosition() const;
	
	void PutToDesktopCenter();

	void SetSize(integer width, integer height);

	void SetWidth(integer width);
	integer GetWidth() const;

	void SetHeight(integer height);
	integer GetHeight() const;

private:
	static const string Name;

	void Register(const window_proc eventHandler, const Color & color);

	void Initialize();
	void ShutDown();

	IntVector2 GetDesktopCenterPosition() const;

	void Update();

	surface_handle _handle;
	
	IntVector2 _position;

	integer _width;
	integer _height;

private:
	InnerEvent<const IntSize &> _sizeChanged;

public:
	Event<const IntSize &> SizeChanged;

	SurfaceSingleton(const SurfaceSingleton &) = delete;
	SurfaceSingleton & operator =(const SurfaceSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<SurfaceSingleton> Surface;

//===========================================================================//

}//namespace Bru

#endif // SURFACE_H

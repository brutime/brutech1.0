//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Глобальный таймер
//

#include "../Common/Common.h"

#ifndef DATE_TIME_HELPER_H
#define DATE_TIME_HELPER_H

namespace Bru
{

//===========================================================================//

class DateTimeHelper
{
public:
	static string GetCurrentLocalDateAndTime();
	static string GetCurrentLocalDate();
	static string GetCurrentLocalTime();

private:
	DateTimeHelper() = delete;
	DateTimeHelper(const DateTimeHelper &) = delete;
	~DateTimeHelper() = delete;
	DateTimeHelper & operator =(const DateTimeHelper &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // DATE_TIME_HELPER_H

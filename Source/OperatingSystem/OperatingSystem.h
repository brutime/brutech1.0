//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс для взаимодействия с операционной системой
//

#pragma once
#ifndef OPERATING_SYSTEM_H
#define OPERATING_SYSTEM_H

#include "Dialogs/OpenFileDialog.h"
#include "Dialogs/SaveFileDialog.h"
#include "OperatingSystemTypes.h"
#include "BaseApplication.h"
#include "DateTimeHelper.h"
#include "Display.h"
#include "GlobalTimer.h"
#include "Timer.h"
#include "Surface.h"
#include "OSCapabilities.h"
#include "InputSystem/InputSystem.h"

namespace Bru
{

//===========================================================================//

class OperatingSystemSingleton
{
public:
	OperatingSystemSingleton();
	~OperatingSystemSingleton();

	void Sleep(integer milliseconds) const;
		
	IntVector2 DisplayToSurface(integer mouseX, integer mouseY) const;
	bool IsSurfaceFocused(surface_handle surfaceHandle) const;

	void SetPixelFormat(device_context deviceContext, integer colorBufferDepth, integer depthBufferDepth) const;

	FunctionPtr GetFuctionAddress(string functionName) const;

	device_context CreateDeviceContext(surface_handle windowHandle) const;
	void DeleteDeviceContext(surface_handle windowHandle, device_context deviceContext) const;

	rendering_context CreateRenderingContext(device_context deviceContext) const;
	void DestroyRenderingContext(rendering_context renderingContext) const;

	void SetRenderingContextAsCurrent(device_context deviceContext, rendering_context renderingContext) const;
	void ClearCurrentRenderingContext() const;

	void SwapBuffers(device_context deviceContext) const;
	
	string GetTextFromClipboard() const;
	void PutTextToClipboard(const string & text) const;

private:
	static const string Name;

	OperatingSystemSingleton(const OperatingSystemSingleton &);
	OperatingSystemSingleton & operator =(const OperatingSystemSingleton &);
};

//===========================================================================//

extern SharedPtr<OperatingSystemSingleton> OperatingSystem;

//===========================================================================//

}//namespace

#endif // OPERATING_SYSTEM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Событие
//

#pragma once
#ifndef THREAD_EVENT_H
#define THREAD_EVENT_H

#include "../Common/Common.h"
#include "OperatingSystemTypes.h"

namespace Bru
{

//===========================================================================//

class ThreadEvent
{
public:
	ThreadEvent(bool autoReset = true);
	~ThreadEvent();

	void Set();
	bool Wait(ulong milliseconds = 0);
	void Reset();

private:
	handle _handle;
	bool _isCreated;

	ThreadEvent(const ThreadEvent &) = delete;
	ThreadEvent & operator =(const ThreadEvent &) = delete;
};

//===========================================================================//

} //namespace Bru

#endif // I_THREAD_EVENT_H

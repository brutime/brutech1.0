//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Display.h"

#include "../../Common/Helpers/Memory.h"
#include "../../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

SharedPtr<DisplaySingleton> Display;

//===========================================================================//

DisplaySingleton::DisplaySingleton() :
	_modes(),
	_xResolution(0),
	_yResolution(0),
	_reversedXResolution(0.0f),
	_reversedYResolution(0.0f)
{
	BuildModesList();
	UpdateResolution();
}

//===========================================================================//

DisplaySingleton::~DisplaySingleton()
{
	RestoreMode();
}

//===========================================================================//

void DisplaySingleton::SelectMode(integer modeIndex)
{
	if (modeIndex < 0 || modeIndex >= _modes.GetCount())
	{
		Console->Notice(
		    string::Format("Display mode index out of bounds: {0} ([0..{1}])", modeIndex, _modes.GetCount()));
		return;
	}

	SelectMode(_modes[modeIndex]);
}

//===========================================================================//

void DisplaySingleton::SelectMode(integer width, integer height, integer bitsPerPixel)
{
	try
	{
		SharedPtr<DisplayMode> mode = FindSuitableDisplayMode(width, height, bitsPerPixel);
		SelectMode(mode);
	}
	catch (InvalidArgumentException &)
	{
		//Какой именно mode - сообщит FindSuitableDisplayMode
	}
}

//===========================================================================//

void DisplaySingleton::RestoreMode()
{
	integer result = ChangeDisplaySettings(NULL, 0);
	if (result == DISP_CHANGE_SUCCESSFUL)
	{
		UpdateResolution();
		Console->Notice("Display mode restored");
	}
	else
	{
		Console->Error("Can't restore display mode");
	}
}

//===========================================================================//

void DisplaySingleton::RemoveUnsuitableMode(const SharedPtr<DisplayMode> & mode)
{
	_modes.Remove(mode);
}

//===========================================================================//

void DisplaySingleton::SelectMode(const SharedPtr<DisplayMode> & mode)
{
	DEVMODE newMode = CreateEmptyDevMode();

	newMode.dmBitsPerPel = mode->GetBitsPerPixel();
	newMode.dmPelsWidth = mode->GetWidth();
	newMode.dmPelsHeight = mode->GetHeight();
	newMode.dmDisplayFlags = mode->GetFlags();
	newMode.dmDisplayFrequency = mode->GetFrequency();
	newMode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFLAGS | DM_DISPLAYFREQUENCY;

	integer result = ChangeDisplaySettings(&newMode, CDS_FULLSCREEN);

	if (result != DISP_CHANGE_SUCCESSFUL)
	{
		Console->Error(string::Format("Can't select display mode {0}", mode->ToString()));
	}

	UpdateResolution();

	Console->Notice(string::Format("Display mode {0} selected", mode->ToString()));
}

//===========================================================================//

bool DisplaySingleton::ContainsMode(const SharedPtr<DisplayMode> & mode)
{
	//Поскольку указатели - нельзя проверить Contains напрямую
	for (const SharedPtr<DisplayMode> & currentMode : _modes)
	{
		if (*currentMode == *mode)
		{
			return true;
		}
	}

	return false;
}

//===========================================================================//

void DisplaySingleton::BuildModesList()
{
	_modes.Clear();

	integer modeNum = 0;
	DEVMODE mode = CreateEmptyDevMode();
	//Резутат вызова EnumDisplaySettings - выполнилась (не 0), не выполнилась (0)
	//Поэтому здесь вместо bool integer
	integer hasNextSettings = 1;
	while (hasNextSettings)
	{
		hasNextSettings = EnumDisplaySettings(NULL, modeNum, &mode);

		SharedPtr<DisplayMode> currentMode = new DisplayMode(mode.dmPelsWidth, mode.dmPelsHeight, mode.dmBitsPerPel,
		        mode.dmDisplayFrequency, mode.dmDisplayFlags);

		if (!ContainsMode(currentMode))
		{
			_modes.Add(currentMode);
		}

		modeNum++;
	}
}

//===========================================================================//

void DisplaySingleton::UpdateResolution()
{
	surface_handle desktopHandle = GetDesktopWindow();
	device_context desktopContext = GetDC(desktopHandle);
	_xResolution = GetDeviceCaps(desktopContext, HORZRES);
	_yResolution = GetDeviceCaps(desktopContext, VERTRES);
	_reversedXResolution = 1.0f / _xResolution;
	_reversedYResolution = 1.0f / _yResolution;
	ReleaseDC(desktopHandle, desktopContext);
}

//===========================================================================//

SharedPtr<DisplayMode> DisplaySingleton::FindSuitableDisplayMode(integer width, integer height, integer bitsPerPixel)
{
	integer maxFrequency = 0;
	SharedPtr<DisplayMode> suitableDisplayMode;
	//Ищем в 2 прохода
	//Сначала полное соответсвие
	for (const SharedPtr<DisplayMode> & mode : _modes)
	{
		if (mode->GetWidth() == width && mode->GetHeight() == height && mode->GetBitsPerPixel() == bitsPerPixel
		        && mode->GetFrequency() > maxFrequency)
		{
			maxFrequency = mode->GetFrequency();
			suitableDisplayMode = mode;
		}
	}

	if (suitableDisplayMode != NullPtr)
	{
		return suitableDisplayMode;
	}

	maxFrequency = 0;
	//Затем ищем разрешения равные или меньше заданных
	for (const SharedPtr<DisplayMode> & mode : _modes)
	{
		if (mode->GetWidth() <= width && mode->GetHeight() <= height && mode->GetBitsPerPixel() <= bitsPerPixel
		        && mode->GetFrequency() > maxFrequency)
		{
			maxFrequency = mode->GetFrequency();
			suitableDisplayMode = mode;
		}
	}

	//Если не находим за 2 прохода - бросаем исключение - скорее всего "плохие" входные данные
	if (suitableDisplayMode == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("Can't find a suitable mode for {0}x{1}x{2}", width, height, bitsPerPixel));
	}

	return suitableDisplayMode;
}

//===========================================================================//

DEVMODE DisplaySingleton::CreateEmptyDevMode()
{
	DEVMODE mode;

	Memory::FillWithNull(&mode, sizeof(mode));
	mode.dmSize = sizeof(DEVMODE);
	mode.dmDriverExtra = 0;

	return mode;
}

//===========================================================================//

}// namespace

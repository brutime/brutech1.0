//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../BaseApplication.h"

#include "../../ScriptSystem/ScriptSystem.h"

namespace Bru
{

//===========================================================================//

SharedPtr<BaseApplication> Application;

//===========================================================================//

BaseApplication::BaseApplication() :
	_quit(false)
{
}

//===========================================================================//

BaseApplication::~BaseApplication()
{
}

//===========================================================================//

void BaseApplication::ApplicationMain()
{
	Initialize();
	RegisterCommands();

	MSG msg;
	while (!_quit)
	{		
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			//OSConsoleStream->WriteLine("PeekMessage");
			//printf("PeekMessage()\n");
			if (msg.message == WM_QUIT)
			{
#ifdef DEBUG_WINDOW
				printf("WM_QUIT received - exiting loop...\n");
#endif
				_quit = true;
			}
			else
			{
				TranslateMessage(&msg);
				//OSConsoleStream->WriteLine("DispatchMessage");
				DispatchMessage(&msg);
			}
		}
		else
		{
			MainLoop();
		}
	}

	UnregisterCommands();
	FreeResources();
}

//===========================================================================//

void BaseApplication::Quit()
{
	_quit = true;
}

//===========================================================================//

void BaseApplication::RegisterCommands()
{
	ScriptSystem->RegisterCommand(new ClassCommand<BaseApplication, void>("Quit", this, &BaseApplication::Quit));
}

//===========================================================================//

void BaseApplication::UnregisterCommands()
{
	ScriptSystem->UnregisterCommand("Quit");
}

//===========================================================================//

}// namespace

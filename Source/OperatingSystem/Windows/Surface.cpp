//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Surface.h"

#include "../../Utils/Console/Console.h"
#include "../BaseApplication.h"
#include "../Display.h"
#include "../OperatingSystem.h"

namespace Bru
{

//===========================================================================//

SharedPtr<SurfaceSingleton> Surface;

//===========================================================================//

const string SurfaceSingleton::Name = "Surface";

//===========================================================================//

//Обработчик для главного окна. Позже можно сделать другие обработчики, а в конструктор окна передавать Enum
LRESULT CALLBACK SurfaceEventHandler(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam);

//===========================================================================//

SurfaceSingleton::SurfaceSingleton(integer width, integer height, const Color & color) :
	_handle(NullPtr),
	_position(),
	_width(width),
	_height(height),
	_sizeChanged(),
	SizeChanged(_sizeChanged)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	Register(SurfaceEventHandler, color);
	Initialize();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

SurfaceSingleton::~SurfaceSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	ShutDown();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void SurfaceSingleton::Minimize()
{
	ShowWindow(_handle, SW_MINIMIZE);
}

//===========================================================================//

void SurfaceSingleton::Normalize()
{
	ShowWindow(_handle, SW_NORMAL);
}

//===========================================================================//

surface_handle SurfaceSingleton::GetHandle() const
{
	return _handle;
}

//===========================================================================//

void SurfaceSingleton::SetPosition(const IntVector2 & position)
{
	_position = position;
	Update();
}

//===========================================================================//

void SurfaceSingleton::SetPosition(integer positionX, integer positionY)
{
	_position.X = positionX;
	_position.Y = positionY;
	Update();
}

//===========================================================================//

const IntVector2 & SurfaceSingleton::GetPosition() const
{
	return _position;
}

//===========================================================================//

void SurfaceSingleton::PutToDesktopCenter()
{
	_position = GetDesktopCenterPosition();
	Update();
}

//===========================================================================//

void SurfaceSingleton::SetSize(integer width, integer height)
{
	_width = width;
	_height = height;
	Update();
	_sizeChanged.Fire(IntSize(_width, _height));
}

//===========================================================================//

void SurfaceSingleton::SetWidth(integer width)
{
	_width = width;
	Update();
	_sizeChanged.Fire(IntSize(_width, _height));
}

//===========================================================================//

integer SurfaceSingleton::GetWidth() const
{
	return _width;
}

//===========================================================================//

void SurfaceSingleton::SetHeight(integer height)
{
	_height = height;
	Update();
	_sizeChanged.Fire(IntSize(_width, _height));
}

//===========================================================================//

integer SurfaceSingleton::GetHeight() const
{
	return _height;
}

//===========================================================================//

void SurfaceSingleton::Register(const window_proc mainSurfaceEventHandler, const Color & color)
{
	WNDCLASS winClass;
	winClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	winClass.lpfnWndProc = mainSurfaceEventHandler;
	winClass.cbClsExtra = 0;
	winClass.cbWndExtra = 0;
	winClass.hInstance = 0; //appInstance
	winClass.hIcon = NULL;
	winClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winClass.hbrBackground = CreateSolidBrush(RGB(color.GetRedByte(),color.GetGreenByte(), color.GetBlueByte()) );
	winClass.lpszMenuName = NULL;
	winClass.lpszClassName = Name.ToChars();
	if (RegisterClass(&winClass) == 0)
	{
		INITIALIZATION_EXCEPTION("Couldn't register window class");
	}
}

//===========================================================================//

void SurfaceSingleton::Initialize()
{
	DWORD style = WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	_position = GetDesktopCenterPosition();

	_handle = CreateWindow(Name, Name, style, _position.X, Display->GetYResolution() - _height - _position.Y, _width, _height, NULL, NULL, 0, NULL);

	if (_handle == NULL)
	{
		INITIALIZATION_EXCEPTION("Couldn't create window");
	}

	ShowWindow(_handle, SW_SHOW);
	UpdateWindow(_handle);
}

//===========================================================================//

void SurfaceSingleton::ShutDown()
{
	BOOL result = DestroyWindow(_handle);
	if (result == 0)
	{
		INITIALIZATION_EXCEPTION("Couldn't destroy window");
	}
}

//===========================================================================//

IntVector2 SurfaceSingleton::GetDesktopCenterPosition() const
{
	RECT desktop;
	SystemParametersInfo(SPI_GETWORKAREA, 0, &desktop, 0);
	
	return IntVector2((desktop.right - desktop.left - _width) / 2,
	                  (desktop.bottom - desktop.top - _height) / 2);
}

//===========================================================================//

void SurfaceSingleton::Update()
{
	MoveWindow(_handle, _position.X, Display->GetYResolution() - _height - _position.Y, _width, _height, false);
}

//===========================================================================//

LRESULT CALLBACK SurfaceEventHandler(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		break;

	case WM_DESTROY:
		return 0;
		
	case WM_SETFOCUS:
		SetCapture(windowHandle);
		SetCursor(LoadCursor(NULL, IDC_ARROW));
		break;				
		
	case WM_KILLFOCUS:
		ReleaseCapture();
		break;

	case WM_SYSKEYDOWN:
		Keyboard->DiscardMessage(OSKeyInfo(wParam, lParam), KeyState::Down);
		return DefWindowProc(windowHandle, message, wParam, lParam);

	case WM_SYSKEYUP:
		Keyboard->DiscardMessage(OSKeyInfo(wParam, lParam), KeyState::Up);
		return DefWindowProc(windowHandle, message, wParam, lParam);

	case WM_KEYDOWN:
		Keyboard->DiscardMessage(OSKeyInfo(wParam, lParam), KeyState::Down);
		break;

	case WM_KEYUP:
		Keyboard->DiscardMessage(OSKeyInfo(wParam, lParam), KeyState::Up);
		break;
			
		//Слежением за передвижением курсора занимается InputSystem,
		//так как нам нужно знать его положение даже при выходе за пределы окна
	case WM_MOUSEMOVE:		
		break;

	case WM_LBUTTONDOWN:
		Mouse->DiscardMessage(OSKeyInfo(wParam, lParam), MouseKey::Left, KeyState::Down);
		break;

	case WM_LBUTTONUP:
		Mouse->DiscardMessage(OSKeyInfo(wParam, lParam), MouseKey::Left, KeyState::Up);
		break;

	case WM_RBUTTONDOWN:
		Mouse->DiscardMessage(OSKeyInfo(wParam, lParam), MouseKey::Right, KeyState::Down);
		break;

	case WM_RBUTTONUP:
		Mouse->DiscardMessage(OSKeyInfo(wParam, lParam), MouseKey::Right, KeyState::Up);
		break;

	case WM_MBUTTONDOWN:
		Mouse->DiscardMessage(OSKeyInfo(wParam, lParam), MouseKey::Middle, KeyState::Down);
		break;

	case WM_MBUTTONUP:
		Mouse->DiscardMessage(OSKeyInfo(wParam, lParam), MouseKey::Middle, KeyState::Up);
		break;

	case WM_MOUSEWHEEL:
		Mouse->DiscardMessage(OSKeyInfo(wParam, lParam), MouseKey::Scroll, KeyState::None);
		break;

	case WM_SYSCOMMAND:
		if (!(wParam == SC_KEYMENU && (lParam >> 16) <= 0))
		{
			return DefWindowProc(windowHandle, message, wParam, lParam);
		}
		break;

	default:
		return DefWindowProc(windowHandle, message, wParam, lParam);
	}

	return 0L;
}

//===========================================================================//

}// namespace Bru

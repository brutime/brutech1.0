//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: windows-реализация класса потока
//

#pragma once
#ifndef THREAD_IMPL_H
#define THREAD_IMPL_H

#include <windows.h>
#include "../IThreadEvent.h"
#include "../Task.h"
#include "../../Common/Containers.h"
#include "../Exceptions/ThreadException.h"

//#define WIN_THREAD_DEBUG

namespace Bru
{

//===========================================================================//

/*enum THREAD_STATE
{
    THREAD_STATE_BUSY,               	// thread is currently handling a task
    THREAD_STATE_WAITING,            	// thread is waiting for something to do
    THREAD_STATE_DOWN,               	// thread is not running
    THREAD_STATE_SHUTTING_DOWN,       	// thread is in the process of shutting down
    THREAD_STATE_FAULT	            	// an error has occured and the thread could not
    // be launched
};*/

//===========================================================================//

enum THREAD_TYPE
{
    THREAD_TYPE_EVENT_DRIVEN,
    THREAD_TYPE_INTERVAL_DRIVEN
};

//===========================================================================//

enum THREAD_ERRORS
{
    NO_ERRORS = 			0x00,
    MUTEX_CREATION = 		0x01,
    EVENT_CREATION = 		0x02,
    THREAD_CREATION = 		0x04,
    UNKNOWN = 				0x08,
    ILLEGAL_USE_OF_EVENT = 	0x10,
    MEMORY_FAULT = 			0x20
};

//===========================================================================//

class Thread
{
public:
	Thread(THREAD_TYPE type, bool terminateAfterExecution);
	virtual ~Thread();

	bool        	AddTask(Task * pvTask);
	ThreadType	GetId()
	{
		return threadId;    // ID процесса
	}
	void			SetPriority(DWORD dwPriority = THREAD_PRIORITY_NORMAL);
	DWORD			GetErrorFlags()
	{
		return objectCondition;
	}
	//менять на ходу - вроде как ненужно и плохо
	//void			SetThreadType(THREAD_TYPE type = THREAD_TYPE_EVENT_DRIVEN, DWORD dwIdle = 100);
	void			SetIdle(DWORD dwIdle = 100);
	integer 		GetPendingTasksCount();				// кол-во задач, ожидающих в очереди
	bool 			Join(DWORD milliseconds = INFINITE);// дождаться выполнения всех запланированных задач
	bool			IsAlive();
	void			Terminate();						// негрубое завершение потока
	void			Suspend();
	void			Resume();

	static bool ThreadIdsEqual(ThreadType * p1, ThreadType * p2)
	{
		return ((*p1 == *p2) ? true : false);
	}

	static ThreadType GetCurrentThreadId()
	{
		ThreadType thisThreadsId ;
		thisThreadsId = ::GetCurrentThreadId();
		return thisThreadsId;
	}

	static void Sleep(integer milli)
	{
		::Sleep(milli);
	}

protected:
	Mutex	  		mutex;         		// mutex that protects threads internal data

	bool			KernelProcess(); 	// обрабатывает отдельную задачу из очереди

	bool			Start(bool suspended);		// создает внутренний поток, руками вызывать не нужно
	virtual bool 	OnTask(void * taskToRun); 	// вызывается при появлении новой задачи в очереди

	friend DWORD WINAPI _THKERNEL(void * lpvData);	// точка входа внутреннего потока-обработчика

private:
	static const integer DEFAULT_STACK_SIZE = 0;

	bool			Push(void * lpv);	// добавление задачи в очередь
	bool			Pop();				// извлечение задачи из очереди
	bool			IsEmpty();			// пуста ли очередь

	ThreadEvent		jobEvent;			// event controller
	ThreadEvent 	finishedEvent;		// показывает, что запланированная работа выполнена

	bool			terminateWhenDone;	// удалять ли поток после завершения
	bool			isTerminating;		// поток завершается
	bool			isSuspended;		// поток приостановлен
	HANDLE  		threadHandle;		// thread handle
	ThreadType 	threadId;			// id of this thread
	List<void *> 	taskQue;			// очередь задач для обработки
	DWORD 			idlePeriod;			// интервалы между вызовами обработки задач по таймеру
	void 	*		dataToProcess;		// обрабатываемая задача
	THREAD_TYPE 	threadType;			// тип потока - просыпающийся по таймеру или при наступлении события
	DWORD 			stackSize;			// thread stack size
	DWORD			objectCondition;	// состояние объекта потока

	Thread(const Thread &);
	Thread & operator =(const Thread &);
};

//===========================================================================//

} //namespace

#endif // THREAD_IMPL_H

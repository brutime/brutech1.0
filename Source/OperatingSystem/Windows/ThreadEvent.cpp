//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../ThreadEvent.h"

namespace Bru
{

//===========================================================================//

//событие автоматически переводится в несигнальное состояние
//после того, как его кто-то дождался, поэтому Reset() не используется
ThreadEvent::ThreadEvent(bool autoReset) :
	_handle(NullPtr),
	_isCreated(true)
{
	_handle = CreateEvent(NullPtr, !autoReset, false, NullPtr);
	if (!_handle)
	{
		_isCreated = false;
	}
}

//===========================================================================//

ThreadEvent::~ThreadEvent(void)
{
	CloseHandle(_handle);
}

//===========================================================================//

void ThreadEvent::Set()
{
	if (!_handle)
	{
		INVALID_STATE_EXCEPTION("Invalid handle");
	}
	SetEvent(_handle);
}

//===========================================================================//

bool ThreadEvent::Wait(ulong milliseconds)
{
	if (!_handle)
	{
		INVALID_STATE_EXCEPTION("Invalid handle");
	}

	if (milliseconds == 0)
	{
		milliseconds = INFINITE;
	}

	return WaitForSingleObject(_handle, milliseconds) == WAIT_OBJECT_0;
}

//===========================================================================//

void ThreadEvent::Reset()
{
	if (!_handle)
	{
		INVALID_STATE_EXCEPTION("Invalid handle");
	}
	ResetEvent(_handle);
}

//===========================================================================//

} //namespace

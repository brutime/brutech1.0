//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../OperatingSystem.h"

#include "../../Utils/Console/Console.h"
#include "../Exceptions/OperatingSystemException.h"

namespace Bru
{

//===========================================================================//

SharedPtr<OperatingSystemSingleton> OperatingSystem;

//===========================================================================//

const string OperatingSystemSingleton::Name = "Operating system";

//===========================================================================//

OperatingSystemSingleton::OperatingSystemSingleton()
{
	Console->Notice(string::Format("{0} initializing...", Name));

	Display = new DisplaySingleton();

	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

OperatingSystemSingleton::~OperatingSystemSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));

	Display = NullPtr;

	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void OperatingSystemSingleton::Sleep(integer milliseconds) const
{
	::Sleep(milliseconds);
}

//===========================================================================//

IntVector2 OperatingSystemSingleton::DisplayToSurface(integer mouseX, integer mouseY) const
{
	IntVector2 position(mouseX - Surface->GetPosition().X, mouseY - Surface->GetPosition().Y);

	if (position.X < 0)
	{
		position.X = 0;
	}
	else if (position.X > Surface->GetWidth() - 1)
	{
		position.X = Surface->GetWidth() - 1;
	}

	if (position.Y < 0)
	{
		position.Y = 0;
	}
	else if (position.Y > Surface->GetHeight() - 1)
	{
		position.Y = Surface->GetHeight() - 1;
	}

	return position;
}

//===========================================================================//

bool OperatingSystemSingleton::IsSurfaceFocused(surface_handle surfaceHandle) const
{
	return GetActiveWindow() == surfaceHandle;
}

//===========================================================================//

void OperatingSystemSingleton::SetPixelFormat(device_context deviceContext, integer colorBufferDepth, integer depthBufferDepth) const
{
	PIXELFORMATDESCRIPTOR pixelFormatDescriptor;

	pixelFormatDescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixelFormatDescriptor.nVersion = 1;
	pixelFormatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits = static_cast<byte>(colorBufferDepth);
	pixelFormatDescriptor.cRedBits = 0;
	pixelFormatDescriptor.cRedShift = 0;
	pixelFormatDescriptor.cGreenBits = 0;
	pixelFormatDescriptor.cGreenShift = 0;
	pixelFormatDescriptor.cBlueBits = 0;
	pixelFormatDescriptor.cBlueShift = 0;
	pixelFormatDescriptor.cAlphaBits = 0;
	pixelFormatDescriptor.cAlphaShift = 0;
	pixelFormatDescriptor.cAccumBits = 0;
	pixelFormatDescriptor.cAccumRedBits = 0;
	pixelFormatDescriptor.cAccumGreenBits = 0;
	pixelFormatDescriptor.cAccumBlueBits = 0;
	pixelFormatDescriptor.cAccumAlphaBits = 0;
	pixelFormatDescriptor.cDepthBits = static_cast<byte>(depthBufferDepth);
	pixelFormatDescriptor.cStencilBits = 0;
	pixelFormatDescriptor.cAuxBuffers = 0;
	pixelFormatDescriptor.iLayerType = PFD_MAIN_PLANE;
	pixelFormatDescriptor.bReserved = 0;
	pixelFormatDescriptor.dwLayerMask = 0;
	pixelFormatDescriptor.dwVisibleMask = 0;
	pixelFormatDescriptor.dwDamageMask = 0;

	integer pixelFormat = ChoosePixelFormat(deviceContext, &pixelFormatDescriptor);

	if (pixelFormat == 0)
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't find a appropriate pixel format");
	}

	if (!::SetPixelFormat(deviceContext, pixelFormat, &pixelFormatDescriptor))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't  set the choosed pixel format");
	}
}

//===========================================================================//

FunctionPtr OperatingSystemSingleton::GetFuctionAddress(string functionName) const
{
	return reinterpret_cast<FunctionPtr>(wglGetProcAddress(functionName.ToChars()));
}

//===========================================================================//

device_context OperatingSystemSingleton::CreateDeviceContext(surface_handle windowHandle) const
{
	device_context deviceContext = GetDC(windowHandle);
	if (deviceContext == 0)
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't create a device context");
	}

	return deviceContext;
}

//===========================================================================//

void OperatingSystemSingleton::DeleteDeviceContext(surface_handle windowHandle, device_context deviceContext) const
{
	if (!ReleaseDC(windowHandle, deviceContext))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't delete a device context");
	}
}

//===========================================================================//

rendering_context OperatingSystemSingleton::CreateRenderingContext(device_context deviceContext) const
{
	rendering_context renderingContext;
	renderingContext = wglCreateContext(deviceContext);

	if (renderingContext == 0)
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't create a rendering context");
	}

	return renderingContext;
}

//===========================================================================//

void OperatingSystemSingleton::DestroyRenderingContext(rendering_context renderingContext) const
{
	if (!wglDeleteContext(renderingContext))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't delete a rendering context");
	}
}

//===========================================================================//

void OperatingSystemSingleton::SetRenderingContextAsCurrent(device_context deviceContext, rendering_context renderingContext) const
{
	if (!wglMakeCurrent(deviceContext, renderingContext))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't make the rendering context pararmeter as the current rendering context");
	}
}

//===========================================================================//

void OperatingSystemSingleton::ClearCurrentRenderingContext() const
{
	if (!wglMakeCurrent(NullPtr, NullPtr))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't clear current rendering context");
	}
}

//===========================================================================//

void OperatingSystemSingleton::SwapBuffers(device_context deviceContext) const
{
	::SwapBuffers(deviceContext);
}

//===========================================================================//

string OperatingSystemSingleton::GetTextFromClipboard() const
{
	if (!OpenClipboard(NULL))
	{
		Console->Warning("Can't open clipboard");
		return string::Empty;
	}

	if (!IsClipboardFormatAvailable(CF_UNICODETEXT))
	{
		return string::Empty;
	}

	handle textHandle = GetClipboardData(CF_UNICODETEXT);
	string clipboardText(utf16_string(static_cast<ushort *>(textHandle)));
	CloseClipboard();
  	return clipboardText;
}

//===========================================================================//

void OperatingSystemSingleton::PutTextToClipboard(const string & text) const
{
	utf16_string utf16text = text.ToUTF16();
	
	integer lenghtInBytes = (utf16text.GetLength() + 1) * sizeof(ushort);
	
	handle memoryHandle =  GlobalAlloc(GMEM_MOVEABLE, lenghtInBytes);
	Memory::Copy(GlobalLock(memoryHandle), utf16text.ToChars(), lenghtInBytes);
	GlobalUnlock(memoryHandle);
	OpenClipboard(NULL);
	EmptyClipboard();
	SetClipboardData(CF_UNICODETEXT, memoryHandle);
	CloseClipboard();
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Timer.h"

#include <windows.h>

namespace Bru
{

//===========================================================================//

Timer::Timer() :
	_startTime(0),
	_frequency(0),
	_threadId(::GetCurrentThread()),
	_processAffinityMask(0)
{
	DWORD_PTR systemMask;
	::GetProcessAffinityMask(GetCurrentProcess(), &_processAffinityMask, &systemMask);

	::SetThreadAffinityMask(_threadId, 1);
	::QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER *>(&_frequency));
	::SetThreadAffinityMask(_threadId, _processAffinityMask);

	_startTime = GetTicks();
}

//===========================================================================//

Timer::~Timer()
{
}

//===========================================================================//

void Timer::Reset()
{
	_startTime = GetTicks();
}

//===========================================================================//

double Timer::GetSeconds() const
{
	const int64 elapsedTime = GetTicks() - _startTime;
	return static_cast<double>(elapsedTime) / _frequency;
}

//===========================================================================//

int64 Timer::GetTicks() const
{
	LARGE_INTEGER curTime;
	::SetThreadAffinityMask(_threadId, 1);
	::QueryPerformanceCounter(&curTime);
	::SetThreadAffinityMask(_threadId, _processAffinityMask);
	return curTime.QuadPart;
}

//===========================================================================//

}

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../DateTimeHelper.h"

#include <windows.h>

namespace Bru
{

//===========================================================================//

string DateTimeHelper::GetCurrentLocalDateAndTime()
{
	return string::Format("{0} {1}", GetCurrentLocalDate(), GetCurrentLocalTime());
}

//===========================================================================//

string DateTimeHelper::GetCurrentLocalDate()
{
	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	return string::Format("{0:00}.{1:00}.{2:2000}", localTime.wDay, localTime.wMonth, localTime.wYear);
}

//===========================================================================//

string DateTimeHelper::GetCurrentLocalTime()
{
	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	return string::Format("{0:00}:{1:00}:{2:00}:{3:0000}", localTime.wHour, localTime.wMinute, localTime.wSecond,
	                      localTime.wMilliseconds);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: windows-реализация класса мьютекса
//

#include "../Mutex.h"

#include "../Thread.h"

namespace Bru
{

//===========================================================================//

Mutex::Mutex():
	_handle(NullPtr),
	_ownerThread(0),
	_isCreated(false)
{
//	ownerThread = Thread::GetCurrentThreadId();
	_handle = CreateMutex(NullPtr, false, NullPtr);
	//mutexHandle = CreateMutex(NullPtr, true, NullPtr);
	_isCreated = (_handle != NullPtr);
}

//===========================================================================//

Mutex::~Mutex()
{
	WaitForSingleObject(_handle, INFINITE);
	CloseHandle(_handle);
}

//===========================================================================//

void Mutex::Lock()
{
#ifdef MUTEX_DEBUG
	printf("Mutex::Lock() called (ownerId=%x)\n", _ownerThread);
#endif
	ThreadType id = Thread::GetCurrentThreadId();
	if (Thread::ThreadIdsEqual(&_ownerThread, &id))
	{
		return;
	}
	WaitForSingleObject(_handle, INFINITE);
	_ownerThread = Thread::GetCurrentThreadId();
}

//===========================================================================//

void Mutex::Unlock()
{
#ifdef MUTEX_DEBUG
	printf("Mutex::Unlock() (ownerId=%x)\n", _ownerThread);
#endif
	ThreadType id = Thread::GetCurrentThreadId();
	if (! Thread::ThreadIdsEqual(&id, &_ownerThread))
	{
		return;
	}
	memset(&_ownerThread, 0, sizeof(ThreadType));
	ReleaseMutex(_handle);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: windows-реализация класса потока
//

#include "Thread.h"
#include <stdio.h>

namespace Bru
{

//===========================================================================//

DWORD WINAPI _THKERNEL(void * lpvData)
{
	Thread * targetThread = static_cast<Thread *>(lpvData);

	while (targetThread->IsAlive())
	{
#ifdef WIN_THREAD_DEBUG
		printf("Calling KernelProcess() for thread %x\n", targetThread->threadId);
#endif
		if (targetThread->jobEvent.Wait())
			if (! targetThread->KernelProcess())
				break;
	}

#ifdef WIN_THREAD_DEBUG
	printf("Thread[%x] main() terminating...\n", targetThread->threadId);
#endif
	return 0;
}

//===========================================================================//

Thread::Thread(THREAD_TYPE type, bool terminateAfterExecution) :
	mutex(),
	jobEvent(),
	finishedEvent(),
	terminateWhenDone(terminateAfterExecution),
	isTerminating(false),
	isSuspended(true),
	threadHandle(NullPtr),
	threadId(0L),
	taskQue(),
	idlePeriod(100),
	dataToProcess(NullPtr),
	threadType(type),
	stackSize(DEFAULT_STACK_SIZE),
	objectCondition()
{
	objectCondition = NO_ERRORS;

	if (!mutex.IsCreated())
	{
		perror("mutex creation failed");
		objectCondition |= MUTEX_CREATION;
		return;
	}

	if (!jobEvent.isCreated)
	{
		perror("event creation failed");
		objectCondition |= EVENT_CREATION;
		return;
	}

	Start(isSuspended);
}

//===========================================================================//

bool Thread::OnTask(void * lpvData)
{
	Task * pTask = static_cast<Task *>(lpvData);

	pTask->SetTaskStatus(TaskStatusBeingProcessed);
#ifdef WIN_THREAD_DEBUG
	printf("Thread[%x]::OnTask: calling pTask->Task()...\n", threadId);
#endif
	bool bReturn = pTask->TheTask();

	pTask->SetTaskStatus(TaskStatusCompleted);
#ifdef WIN_THREAD_DEBUG
	printf("Thread[%x] finishedEvent set to signaled...\n", threadId);
#endif

	finishedEvent.Set();

	return bReturn;
}

//===========================================================================//

bool Thread::AddTask(Task * task)
{
//    ThreadType id;
	task->SetId(GetId());
	if (! Push((void *)task))
		return false;
	task->SetTaskStatus(TaskStatusWaitingOnQueue);
	jobEvent.Set();
	return true;
}

//===========================================================================//

void Thread::SetPriority(DWORD dwPriority)
{
	SetThreadPriority(threadHandle, dwPriority);
}

//===========================================================================//

bool Thread::KernelProcess()
{
	mutex.Lock();

	if (isTerminating)
	{
#ifdef WIN_THREAD_DEBUG
		printf("Thread[%x]::KernelProcess: terminating \n", threadId);
#endif
		mutex.Unlock();
		return false;
	}
	mutex.Unlock();

	if (!IsEmpty())
	{
		if (isTerminating)
		{
			return false;
		}

#ifdef WIN_THREAD_DEBUG
		printf("Thread[%x]::KernelProcess: waiting for job\n", threadId);
#endif

		Pop();
#ifdef WIN_THREAD_DEBUG
		printf("Thread[%x]::KernelProcess: processing task\n", threadId);
#endif
		if (!OnTask(dataToProcess))
		{
			mutex.Lock();
			dataToProcess = NullPtr;
			mutex.Unlock();
			return false;
		}
	}
	mutex.Lock();
	dataToProcess = NullPtr;
	mutex.Unlock();

#ifdef WIN_THREAD_DEBUG
	printf("Thread[%x]::KernelProcess: job done, checking if to terminate\n", threadId);
#endif

	if (threadType == THREAD_TYPE_INTERVAL_DRIVEN)
	{
		Sleep(idlePeriod);
		jobEvent.Set();
	}
	else if (terminateWhenDone)
		Terminate();

	return true;
}

//===========================================================================//

integer Thread::GetPendingTasksCount()
{
	integer chEventsWaiting;
	mutex.Lock();
	chEventsWaiting = taskQue.GetCount();
	mutex.Unlock();

	return chEventsWaiting;
}

//===========================================================================//

bool Thread::Join(DWORD milliseconds)
{
#ifdef WIN_THREAD_DEBUG
	printf("Thread[%x]::Join() called\n", threadId);
#endif
	if (!isTerminating)
	{
#ifdef WIN_THREAD_DEBUG
		printf("Thread %x is not terminating, so Join() can wait for job to complete\n", threadId);
#endif
		if (finishedEvent.Wait(milliseconds))
		{
			finishedEvent.Reset();
			return true;
		}
		finishedEvent.Reset();
	}
	/*if (!isTerminating)
	{
	#ifdef WIN_THREAD_DEBUG
	    printf("Thread %x is not terminating, so Join() can wait for job to complete\n", threadId);
	#endif
	    finishedEvent.Wait(milliseconds);
	    finishedEvent.Reset();
	}*/
	return false;
}

//===========================================================================//

bool Thread::IsEmpty()
{
	integer taskCount;
	mutex.Lock();
	taskCount = taskQue.GetCount();
	mutex.Unlock();

	return (taskCount == 0);
}

//===========================================================================//

bool Thread::Push(void * taskPointer)
{
	if (!taskPointer) return false;

	mutex.Lock();
	if (threadType == THREAD_TYPE_INTERVAL_DRIVEN)
		taskQue.Clear();

	taskQue.Add(static_cast<Task *>(taskPointer));

	mutex.Unlock();

	return true;
}

//===========================================================================//

bool Thread::Pop()
{

	mutex.Lock();

	if (taskQue.GetCount() <= 0)
	{
		mutex.Unlock();
		return false;
	}

	dataToProcess = taskQue.Get(0);

	if (threadType == THREAD_TYPE_EVENT_DRIVEN)
		taskQue.RemoveAt(0);

	mutex.Unlock();
	return true;
}

//===========================================================================//

void Thread::Suspend()
{
#ifdef WIN_THREAD_DEBUG
	printf("Suspending thread %x...\n", threadId);
#endif

	mutex.Lock();
	isSuspended = true;
	SuspendThread(threadHandle);
	mutex.Unlock();

#ifdef WIN_THREAD_DEBUG
	printf("Thread %x suspended...\n", threadId);
#endif
}

//===========================================================================//

void Thread::Resume()
{
#ifdef WIN_THREAD_DEBUG
	printf("Resuming thread %x...\n", threadId);
#endif

	mutex.Lock();
	isSuspended = false;
	ResumeThread(threadHandle);
	mutex.Unlock();

#ifdef WIN_THREAD_DEBUG
	printf("Thread %x resumed...\n", threadId);
#endif
}

//===========================================================================//

void Thread::SetIdle(DWORD dwIdle)
{
	mutex.Lock();
	idlePeriod = dwIdle;
	mutex.Unlock();
}

//===========================================================================//

bool Thread::Start(bool suspended)
{
	mutex.Lock();
	if (threadHandle)
	{
		mutex.Unlock();
		return true;
	}

	mutex.Unlock();

	if (objectCondition & THREAD_CREATION)
		objectCondition = objectCondition ^ THREAD_CREATION;

	if (threadHandle) CloseHandle(threadHandle);

	threadHandle = CreateThread(NullPtr, stackSize , _THKERNEL, (void *)this, suspended ? CREATE_SUSPENDED : 0, &threadId);
	if (!threadHandle)
	{
		THREAD_EXCEPTION("Thread creation failed");
	}

	return true;
}

//===========================================================================//

Thread::~Thread(void)
{
#ifdef WIN_THREAD_DEBUG
	printf("Destroying thread %x...\n", threadId);
#endif

	Terminate();

#ifdef WIN_THREAD_DEBUG
	printf("Thread %x destroyed...\n", threadId);
#endif
}

//===========================================================================//

bool Thread::IsAlive()
{
#ifdef WIN_THREAD_DEBUG
	printf("Checking %x to be alive: ", threadId);
#endif
	bool alive = !isTerminating;
#ifdef WIN_THREAD_DEBUG
	printf("%s", alive ? "true" : "false");
#endif
	return alive;
}

//===========================================================================//

void Thread::Terminate()
{
	if (isTerminating)
		return;
#ifdef WIN_THREAD_DEBUG
	printf("Trying to terminate thread %x...\n", threadId);
#endif

	isTerminating = true;
	if (isSuspended)
	{
		Resume();

		WaitForSingleObject(threadHandle, INFINITE);
	}
	CloseHandle(threadHandle);
}

//===========================================================================//

} //namespace

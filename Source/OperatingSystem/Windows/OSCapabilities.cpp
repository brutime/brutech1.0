//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../OSCapabilities.h"

#include <windows.h>

namespace Bru
{

//===========================================================================//

integer OSCapabilities::GetXDPI()
{
	integer result;
	HWND desktopHandle = GetDesktopWindow();
	HDC context = GetDC(desktopHandle);
	result = GetDeviceCaps(context, LOGPIXELSX);
	ReleaseDC(desktopHandle, context);
	return result;
}

//===========================================================================//

integer OSCapabilities::GetYDPI()
{
	integer result;
	HWND desktopHandle = GetDesktopWindow();
	HDC context = GetDC(desktopHandle);
	result = GetDeviceCaps(context, LOGPIXELSY);
	ReleaseDC(desktopHandle, context);
	return result;
}

//===========================================================================//

}//namespace

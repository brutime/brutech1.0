//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: интерфейс мьютекса
//

#pragma once
#ifndef MUTEX_H
#define MUTEX_H

#include "../Common/Common.h"
#include "OperatingSystemTypes.h"
#include "ThreadTypes.h"

namespace Bru
{

//===========================================================================//

class Mutex
{
public:
	Mutex();
	virtual ~Mutex();

	void Lock();
	void Unlock();

	bool IsCreated() const
	{
		return _isCreated;
	}

private:
	handle _handle;
	ThreadType _ownerThread;
	bool _isCreated;

	Mutex(const Mutex &) = delete;
	Mutex & operator =(const Mutex &) = delete;
};

//===========================================================================//

} //namespace Bru

#endif // IMUTEX_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ������������ ����, ������������ ��� ������������� ���������������
//				�� ����� �������� ����
//

#pragma once
#ifndef THREAD_TYPES_H
#define THREAD_TYPES_H

#include "../Common/Defines/Defines.h"

namespace Bru
{

//===========================================================================//

#if PLATFORM == WINDOWS
#include <windows.h>

typedef DWORD ThreadType;
typedef DWORD TimeInMsType;
typedef DWORD StackSizeType;
typedef DWORD ObjectConditionType;

#elif PLATFORM == MAC

#error no MAC implementation

#elif PLATFORM == LINUX
#include <pthread.h>

typedef pthread_t ThreadType;
typedef unsigned long TimeInMsType;
typedef unsigned int StackSizeType;
typedef unsigned int ObjectConditionType;
typedef unsigned int ThreadPriorityType;
const TimeInMsType INFINITE = -1L;

#else
#error Platform not defined (WINDOWS,MAC,LINUX)
#endif

//===========================================================================//

} //namespace Bru

#endif // THREAD_TYPES_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс экранного режима
//

#pragma once
#ifndef DISPLAY_MODE_H
#define DISPLAY_MODE_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

class DisplayMode
{
public:
	DisplayMode();
	DisplayMode(integer width, integer height, byte bitsPerPixel, byte frequency, integer flags);
	DisplayMode(const DisplayMode & other);
	~DisplayMode();

	DisplayMode & operator =(const DisplayMode & other);

	bool operator ==(const DisplayMode & someDisplayMode) const;
	bool operator !=(const DisplayMode & someDisplayMode) const;

	integer GetWidth() const
	{
		return _width;
	}
	integer GetHeight() const
	{
		return _height;
	}
	byte GetBitsPerPixel() const
	{
		return _bitsPerPixel;
	}
	byte GetFrequency() const
	{
		return _frequency;
	}
	byte GetFlags() const
	{
		return _flags;
	}

	string ToString() const;

private:
	integer _width;
	integer _height;
	byte _bitsPerPixel;
	byte _frequency;
	integer _flags;
};

//===========================================================================//

}//namespace

#endif // DISPLAY_MODE_H

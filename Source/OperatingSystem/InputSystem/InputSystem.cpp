//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "InputSystem.h"

#include "../../Utils/Console/Console.h"
#include "../../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"

namespace Bru
{

//===========================================================================//

SharedPtr<InputSystemSingleton> InputSystem;

//===========================================================================//

const string InputSystemSingleton::Name = "Input system";

//===========================================================================//

InputSystemSingleton::InputSystemSingleton() :
	_components(),
	_currentDomainName(),
	_isServiceOnlyModeEnabled(false),
	_keyboardKeysUsingInGlobalDomainOnly(),
	_mouseKeysUsingInGlobalDomainOnly(),
	_keyboardSignalHandler(new EventHandler<const KeyboardEventArgs &>(this, &InputSystemSingleton::OnKeyboardSingnal)),
	_mouseSignalHandler(new EventHandler<const MouseEventArgs &>(this, &InputSystemSingleton::OnMouseSingnal))
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	Keyboard = new KeyboardSingleton();
	Keyboard->_signal.AddHandler(_keyboardSignalHandler);

	Mouse = new MouseSingleton();
	Mouse->_signal.AddHandler(_mouseSignalHandler);

	AddDomain(EntitySystem::GlobalDomainName);

	PerfomanceDataCollector->AddMeasurement(Name);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

InputSystemSingleton::~InputSystemSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	PerfomanceDataCollector->RemoveMeasurement(Name);

	Mouse->_signal.RemoveHandler(_mouseSignalHandler);
	Mouse = NullPtr;

	Keyboard->_signal.RemoveHandler(_keyboardSignalHandler);
	Keyboard = NullPtr;

	RemoveAllDomains();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void InputSystemSingleton::AddDomain(const string & domainName)
{
#ifdef DEBUGGING
	if (!EntitySystem::IsDomainNameValid(domainName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} trying to add domain with invalid name: \"{0}\"", Name, domainName));
	}
#endif

	_components.Add(domainName, List<InputReceiver * const>());

	Console->Debug(string::Format("{0} added domain: \"{1}\"", Name, domainName));
}

//===========================================================================//

void InputSystemSingleton::RemoveDomain(const string & domainName)
{
#ifdef DEBUGGING
	if (!_components[domainName].IsEmpty())
	{
		PrintInputReceivers(domainName);
		INVALID_STATE_EXCEPTION(string::Format("{0} trying to delete not empty domain: \"{1}\"", Name, domainName));
	}
#endif

	_components.Remove(domainName);

	Console->Debug(string::Format("{0} removed domain: \"{1}\"", Name, domainName));
}

//===========================================================================//

void InputSystemSingleton::SelectDomain(const string & domainName)
{
	_currentDomainName = domainName;
}

//===========================================================================//

void InputSystemSingleton::RegisterComponent(InputReceiver * const component)
{
#ifdef DEBUGGING
	if (_components[component->GetDomainName()].Contains(component))
	{
		INVALID_STATE_EXCEPTION(string::Format("{0} already contains component: \"{1}\"", Name, component->GetName()));
	}
#endif

	_components[component->GetDomainName()].Add(component);
}

//===========================================================================//

void InputSystemSingleton::UnregisterComponent(InputReceiver * const component)
{
	_components[component->GetDomainName()].Remove(component);
}

//===========================================================================//

void InputSystemSingleton::Update()
{
	PerfomanceDataCollector->StartMeasurement(Name);
	Mouse->Update();
	PerfomanceDataCollector->EndMeasurement(Name);
}

//===========================================================================//

void InputSystemSingleton::EnableGlobalDomainOnlyMode()
{
	_isServiceOnlyModeEnabled = true;

	for (auto & componentPair : _components)
	{
		if (componentPair.GetKey() == EntitySystem::GlobalDomainName)
		{
			continue;
		}

		for (InputReceiver * const inputReceiver : componentPair.GetValue())
		{
			inputReceiver->LostFocus();
		}
	}
}

//===========================================================================//

void InputSystemSingleton::DisableGlobalDomainOnlyMode()
{
	_isServiceOnlyModeEnabled = false;
}

//===========================================================================//

bool InputSystemSingleton::IsServiceOnlyModeEnabled() const
{
	return _isServiceOnlyModeEnabled;
}

//===========================================================================//

void InputSystemSingleton::AddKeyboardKeyUsingInGlobalDomainOnly(KeyboardKey key)
{
	_keyboardKeysUsingInGlobalDomainOnly.Add(key);
}

//===========================================================================//

void InputSystemSingleton::RemoveKeyboardKeyUsingInGlobalDomainOnly(KeyboardKey key)
{
	_keyboardKeysUsingInGlobalDomainOnly.Remove(key);
}

//===========================================================================//

void InputSystemSingleton::AddMouseKeyUsingInGlobalDomainOnly(MouseKey key)
{
	_mouseKeysUsingInGlobalDomainOnly.Add(key);
}

//===========================================================================//

void InputSystemSingleton::RemoveMouseKeyUsingInGlobalDomainOnly(MouseKey key)
{
	_mouseKeysUsingInGlobalDomainOnly.Remove(key);
}

//===========================================================================//

const string & InputSystemSingleton::GetCurrentDomainName() const
{
	return _currentDomainName;
}

//===========================================================================//

integer InputSystemSingleton::GetComponentCount(const string & domainName) const
{
	return _components[domainName].GetCount();
}

//===========================================================================//

integer InputSystemSingleton::GetComponentCount() const
{
	integer count = 0;
	for (auto & componentPair : _components)
	{
		count += componentPair.GetValue().GetCount();
	}
	return count;
}

//===========================================================================//

integer InputSystemSingleton::GetEnabledComponentCount() const
{
	integer count = 0;
	for (auto & componentPair : _components)
	{
		for (InputReceiver * const inputReceiver : componentPair.GetValue())
		{
			if (inputReceiver->IsEnabled())
			{
				count++;
			}
		}
	}
	return count;
}

//===========================================================================//

void InputSystemSingleton::PrintInputReceivers(const string & domainName) const
{
	Console->Hint("InputReceivers (\"{0}\"):", domainName);
	Console->AddOffset(2);
	for (const auto & component : _components[domainName])
	{
		Console->Hint(component->GetName());
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void InputSystemSingleton::PrintInputReceivers() const
{
	Console->Hint("InputReceivers:");
	Console->AddOffset(2);
	for (const auto & componentPair : _components)
	{
		Console->Hint(string::Format("{0}:", componentPair.GetKey()));
		Console->AddOffset(2);
		for (const auto & component : componentPair.GetValue())
		{
			Console->Hint(component->GetName());
		}
		Console->ReduceOffset(2);
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void InputSystemSingleton::RemoveAllDomains()
{
	//Нельзя просто вызвать Clear - нужно проверить, пусты ли слои в доменах
	//А такая проверка проходит в RemoveDomain
	TreeMapIterator<string, List<InputReceiver * const >> it = _components.CreateIterator();
	while (it.HasNext())
	{
		string domainName = it.GetCurrent().GetKey();
		it.Next();

		RemoveDomain(domainName);
	}
}

//===========================================================================//

void InputSystemSingleton::OnKeyboardSingnal(const KeyboardEventArgs & args)
{
	PerfomanceDataCollector->StartMeasurement(Name);
	DispatchKeyboardEvent(EntitySystem::GlobalDomainName, args);
	if (!_isServiceOnlyModeEnabled)
	{
		DispatchKeyboardEvent(_currentDomainName, args);
	}
	PerfomanceDataCollector->EndMeasurement(Name);
}

//===========================================================================//

void InputSystemSingleton::OnMouseSingnal(const MouseEventArgs & args)
{
	DispatchMouseEvent(EntitySystem::GlobalDomainName, args);
	if (!_isServiceOnlyModeEnabled)
	{
		DispatchMouseEvent(_currentDomainName, args);
	}
}

//===========================================================================//

void InputSystemSingleton::DispatchKeyboardEvent(const string & domainName, const KeyboardEventArgs & args)
{
	if (domainName.IsEmpty())
	{
		return;
	}

	if (domainName != EntitySystem::GlobalDomainName)
	{
		if (_keyboardKeysUsingInGlobalDomainOnly.Contains(args.Key))
		{
			return;
		}
	}

	for (InputReceiver * const inputReceiver : _components[domainName])
	{
		if (inputReceiver->IsEnabled())
		{
			inputReceiver->DispatchKeyboardEvent(args);
		}
	}
}

//===========================================================================//

void InputSystemSingleton::DispatchMouseEvent(const string & domainName, const MouseEventArgs & args)
{
	if (domainName.IsEmpty())
	{
		return;
	}

	if (domainName != EntitySystem::GlobalDomainName)
	{
		if (_mouseKeysUsingInGlobalDomainOnly.Contains(args.Key))
		{
			return;
		}
	}

	for (InputReceiver * const inputReceiver : _components[domainName])
	{
		if (inputReceiver->IsEnabled())
		{
			inputReceiver->DispatchMouseEvent(args);
		}
	}
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef MOUSE_EVENT_ARGS_H
#define MOUSE_EVENT_ARGS_H

#include "../../../Common/Common.h"
#include "../Enums/KeyState.h"
#include "../Enums/MouseKey.h"

namespace Bru
{

//===========================================================================//

struct MouseEventArgs
{
	MouseEventArgs(MouseKey key, KeyState state, float x, float y, float displayX, float displayY,
	               float deltaX, float deltaY, integer scrollClickCount);
	MouseEventArgs(const MouseEventArgs & other);
	MouseEventArgs & operator =(const MouseEventArgs & other);

	MouseKey Key;
	KeyState State;
	float X;
	float Y;
	float DisplayX;
	float DisplayY;
	float DeltaX; //на сколько сдвинулась мышка относительно предыдущей позиции по оси X в экранных координатах
	float DeltaY; //на сколько сдвинулась мышка относительно предыдущей позиции по оси Y в экранных координатах
	integer ScrollClickCount; //количество щелчков колесика
};

//===========================================================================//

} // namespace Bru

#endif // MOUSE_EVENT_ARGS_H

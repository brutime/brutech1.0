//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef KEYBOARD_EVENT_ARGS_H
#define KEYBOARD_EVENT_ARGS_H

#include "../../../Common/Common.h"
#include "../Enums/KeyState.h"
#include "../Enums/KeyboardKey.h"

namespace Bru
{

//===========================================================================//

struct KeyboardEventArgs
{
	KeyboardEventArgs(KeyboardKey key, KeyState state);	
	KeyboardEventArgs(const KeyboardEventArgs & other);
	KeyboardEventArgs & operator =(const KeyboardEventArgs & other);	

	KeyboardKey Key;
	KeyState State;
};

//===========================================================================//

} // namespace Bru

#endif // KEYBOARD_EVENT_ARGS_H

//
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "OSKeyInfo.h"

namespace Bru
{

//===========================================================================//

OSKeyInfo::OSKeyInfo(WPARAM virtualCode, LPARAM keyData) :
	VirtualCode(virtualCode),
	KeyData(keyData)
{
}

//===========================================================================//

}// namespace

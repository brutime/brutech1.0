//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: структура хранит доп. информацию о нажатой клавише для облегчения
//              получения доп.информации(помимо кода клавиши и её состояния), где это
//              нужно. например, для получения символа, представляющего нажатые клавиши
//

#ifndef OS_KEY_INFO_H
#define OS_KEY_INFO_H

#include "../../../Common/Common.h"

#if PLATFORM == WINDOWS
#include <windows.h>

namespace Bru
{

//===========================================================================//

class OSKeyInfo
{
public:
	OSKeyInfo(WPARAM virtualCode, LPARAM keyData);

	WPARAM VirtualCode;
	LPARAM KeyData;
};

//===========================================================================//

}// namespace Bru

#elif PLATFORM == LINUX
#error No KeyboardEventArgs struct defined!
#elif PLATFORM == MAC
#error No realization for MAC
#else
#error Platform not defined
#endif

//===========================================================================//

#endif // OS_KEY_INFO_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "KeyboardEventArgs.h"

namespace Bru
{

//===========================================================================//

KeyboardEventArgs::KeyboardEventArgs(KeyboardKey key, KeyState state) :
	Key(key),
	State(state)
{
}

//===========================================================================//

KeyboardEventArgs::KeyboardEventArgs(const KeyboardEventArgs & other) :
	Key(other.Key),
	State(other.State)
{
}

//===========================================================================//

KeyboardEventArgs & KeyboardEventArgs::operator =(const KeyboardEventArgs & other)
{
	if (this == &other)
	{
		return *this;
	}

	Key = other.Key;
	State = other.State;

	return *this;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MouseEventArgs.h"

namespace Bru
{

//===========================================================================//

MouseEventArgs::MouseEventArgs(MouseKey key, KeyState state, float x, float y, float displayX, float displayY,
                               float deltaX, float deltaY, integer scrollClickCount) :
	Key(key),
	State(state),
	X(x),
	Y(y),
	DisplayX(displayX),
	DisplayY(displayY),
	DeltaX(deltaX),
	DeltaY(deltaY),
	ScrollClickCount(scrollClickCount)
{
}

//===========================================================================//

MouseEventArgs::MouseEventArgs(const MouseEventArgs & other) :
	Key(other.Key),
	State(other.State),
	X(other.X),
	Y(other.Y),
	DisplayX(other.DisplayX),
	DisplayY(other.DisplayY),
	DeltaX(other.DeltaX),
	DeltaY(other.DeltaY),
	ScrollClickCount(other.ScrollClickCount)
{
}

//===========================================================================//

MouseEventArgs & MouseEventArgs::operator =(const MouseEventArgs & other)
{
	if (this == &other)
	{
		return *this;
	}

	Key = other.Key;
	State = other.State;
	X = other.X;
	Y = other.Y;
	DisplayX = other.DisplayX;
	DisplayY = other.DisplayY;
	DeltaX = other.DeltaX;
	DeltaY = other.DeltaY;
	ScrollClickCount = other.ScrollClickCount;

	return *this;
}

//===========================================================================//

} // namespace Bru

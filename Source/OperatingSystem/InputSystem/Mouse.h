//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс мыши
//

#ifndef MOUSE_H
#define MOUSE_H

#include "../../Math/Math.h"
#include "Types/MouseEventArgs.h"
#include "Types/OSKeyInfo.h"

namespace Bru
{

//===========================================================================//

class MouseSingleton
{
	friend class InputSystemSingleton;

public:
	MouseSingleton();
	~MouseSingleton();

	void DiscardMessage(OSKeyInfo keyInfo, MouseKey key, KeyState state);
	void Update();
	
	integer GetX() const;
	integer GetY() const;	
	
	integer GetScreenX() const;
	integer GetScreenY() const;

private:
	static const string Name;
	
	integer _x;
	integer _y;	

	integer _displayX;
	integer _displayY;

	InnerEvent<const MouseEventArgs &> _signalEvent;
	Event<const MouseEventArgs &> _signal;

	MouseSingleton(const MouseSingleton &) = delete;
	MouseSingleton & operator =(const MouseSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<MouseSingleton> Mouse;

//===========================================================================//

}// namespace

#endif // MOUSE_H

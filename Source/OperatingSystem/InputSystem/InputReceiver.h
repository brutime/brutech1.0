//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef INPUT_RECEIVER_H
#define INPUT_RECEIVER_H

#include "../../EntitySystem/EntitySystem.h"
#include "Types/KeyboardEventArgs.h"
#include "Types/MouseEventArgs.h"
#include "Types/JoystickEventArgs.h"

namespace Bru
{

//===========================================================================//

class InputReceiver : public EntityComponent
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	virtual ~InputReceiver();

	virtual void Reset();	

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args) = 0;
	virtual void DispatchMouseEvent(const MouseEventArgs & args) = 0;
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args) = 0;

	virtual void Enable();
	virtual void Disable();
	bool IsEnabled() const;

	virtual void LostFocus();

protected:
	static const string PathToIsEnabled;

	InputReceiver(const string & domainName, bool isEnabled = true);
	InputReceiver(const string & domainName, const SharedPtr<BaseDataProvider> & provider);

	virtual void Register();
	virtual void Unregister();

	bool _isEnabled;
	bool _isEnabledOnInitialize;

private:
	InputReceiver(const InputReceiver &) = delete;
	InputReceiver & operator =(const InputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // INPUT_RECEIVER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "InputReceiver.h"

#include "InputSystem.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(InputReceiver, EntityComponent)

//===========================================================================//

const string InputReceiver::PathToIsEnabled = "IsEnabled";

//===========================================================================//

InputReceiver::InputReceiver(const string & domainName, bool isEnabled) :
	EntityComponent(domainName),
	_isEnabled(isEnabled),
	_isEnabledOnInitialize(isEnabled)
{
	Register();
}

//===========================================================================//

InputReceiver::InputReceiver(const string & domainName, const SharedPtr<BaseDataProvider> & provider) : 
	EntityComponent(domainName),
	_isEnabled(provider->Get<bool>(PathToIsEnabled)),
	_isEnabledOnInitialize(_isEnabled)
{
	Register();
}

//===========================================================================//

InputReceiver::~InputReceiver()
{
	if (IsRegistred())
	{
		Unregister();
	}
}

//===========================================================================//

void InputReceiver::Reset()
{
	EntityComponent::Reset();
	_isEnabledOnInitialize ? Enable() : Disable();
}

//===========================================================================//

void InputReceiver::Enable()
{
	_isEnabled = true;
}

//===========================================================================//

void InputReceiver::Disable()
{
	_isEnabled = false;
}

//===========================================================================//

bool InputReceiver::IsEnabled() const
{
	return _isEnabled;
}

//===========================================================================//

void InputReceiver::LostFocus()
{
}

//===========================================================================//

void InputReceiver::Register()
{
	InputSystem->RegisterComponent(this);
	EntityComponent::Register();
}

//===========================================================================//

void InputReceiver::Unregister()
{
	InputSystem->UnregisterComponent(this);
	EntityComponent::Unregister();
}

//===========================================================================//

} // namespace Bru

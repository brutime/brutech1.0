//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "Enums/KeyboardKey.h"
#include "Types/KeyboardEventArgs.h"
#include "Types/OSKeyInfo.h"

namespace Bru
{

//===========================================================================//

class KeyboardSingleton
{
	friend class InputSystemSingleton;

public:
	KeyboardSingleton();
	~KeyboardSingleton();

	bool IsKeyPrintableCharacter(KeyboardKey key);
	string GetCharForKey(KeyboardKey key);
	bool IsKeyDown(KeyboardKey key);

	void DiscardMessage(OSKeyInfo keyInfo, KeyState state);

private:
	static const string Name;

	TreeMap<integer, KeyboardKey> _virtualCodeToKeyMap;
	TreeMap<KeyboardKey, integer> _keyToVirtualCodeMap;

	InnerEvent<const KeyboardEventArgs &> _signalEvent;
	Event<const KeyboardEventArgs &> _signal;

	KeyboardSingleton(const KeyboardSingleton &) = delete;
	KeyboardSingleton & operator =(const KeyboardSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<KeyboardSingleton> Keyboard;

//===========================================================================//

}// namespace Bru

#endif // KEYBOARD_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: определение массива для задания соответствия между сис.кодом и кодом кнопки
//

#pragma once
#ifndef KEY_MAP_H
#define KEY_MAP_H
#define XK_MISCELLANY // выбираем общий(нормальный, человеческий) набор клавиш, который будет использоваться, т.к. в X11/keysymdef.h есть объявления для хреновой тучи наборов
#include <X11/keysymdef.h>
#include "../Types/OSToLogicalKeyPair.h"

//x64 может вызвать проблемы, и придётся делать static_cast<integer>
namespace Bru
{

namespace Keyboard
{

//===========================================================================//

static const OSToLogicalKeyPair KeyboardKeysMap[] = { { XK_Left, KeyboardKey::Left }, { XK_Right, KeyboardKey::Right },
	{ XK_KP_Space, KeyboardKey::Space }, { XK_Escape, KeyboardKey::Escape }
};

//===========================================================================//

const integer MappedOSKeysNumber = static_cast<integer>(4);

//===========================================================================//

}//namespace Keyboard

} //namespace Bru

#endif // KEY_MAP_H

////
////    Copyright (C) 2010-2012, BruTime
////    This file is part of BruTech source code.
////
////    All rights are reserved. Reproduction or transmission
////    in whole or in part, in any form or by any means,
////    electronic, mechanical or otherwise, is prohibited
////    without the prior written consent of the copyright owner.
////
//
//#include "../Keyboard.h"
//#include <linux/keyboard.h>
////#include "../../OperatingSystem/Timer.h"
//
////temp
//#include <stdio.h>
//
//namespace Bru
//{
//
////Bru::Timer timer;
////static const float MinUpdateDelay=0.0f;
//
//	void KeyboardSingleton::Update(ISignalsDispatcher *signalsReceiver)
//	{
//		/*static float lastAccessedTime=0.0f;
//		if (timer.GetSeconds() < lastAccessedTime + MinUpdateDelay)
//			return;
//		lastAccessedTime=timer.GetSeconds();
//		*/
//
////    const short BIT_LOWEST = (integer)1;
//		const short BIT_HIGHEST = (integer)0x8000;
//		static char keyBuffer[Keyboard::MappedOSKeysNumber * static_cast<integer>(2)];
//		static integer secondPartOffset = static_cast<integer>(Keyboard::MappedOSKeysNumber);
//		static integer currentPart = 0; // в которую часть буфера писать состояние
//
//		for (integer i = 0; i < Keyboard::MappedOSKeysNumber; i++)
//		{
//			//получаем состояние
////?????        keyBuffer[currentPart *secondPartOffset+i] = GetKeyState(Keyboard::KeyboardKeysMap[i].osCode);
//		}
//
//		//2 цикла сделаны для того, чтоб нажатие не потерялось, если обработка нажатия затягивается
//		for (integer i = 0; i < Keyboard::MappedOSKeysNumber; i++)
//		{
//			//сравниваем состояние с предыдущим
//			if ((keyBuffer[i] ^ keyBuffer[i+secondPartOffset]) & BIT_HIGHEST)// состояние изменилось
//			{
//				/*if (currentPart)
//					printf("Changed: from %x to %x\n", keyBuffer[i+secondPartOffset], keyBuffer[i]);
//				else
//					printf("Changed: from %x to %x\n", keyBuffer[i], keyBuffer[i+secondPartOffset]);*/
//				//printf("Keyboard::Update(): %10.3f\n", lastAccessedTime);
//				if (keyBuffer[currentPart*secondPartOffset+i] & BIT_HIGHEST) // нажата
//				{
//					printf("Down\n");
//					signalsReceiver->DispatchKeyboardMessage(KbMessage(0, Keyboard::KeyboardKeysMap[i].key, Keyboard::KeyState::Down));
//				}
//				else // отпущена
//				{
//					printf("Up\n");
//					signalsReceiver->DispatchKeyboardMessage(KbMessage(0, Keyboard::KeyboardKeysMap[i].key, Keyboard::KeyState::Up));
//				}
//			}
//		}
//
//		currentPart = currentPart ? (integer)0 : (integer)1;
//		return;
//	}
//}

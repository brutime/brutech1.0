//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENUM_KEY_STATE_H
#define ENUM_KEY_STATE_H

namespace Bru
{

//===========================================================================//

enum class KeyState
{
    None,

    Down,
    Up,

    RepeatedDown
};

//===========================================================================//

}// Bru

#endif // ENUM_KEY_STATE_H

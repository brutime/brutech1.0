//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef KEYBOARD_KEY_H
#define KEYBOARD_KEY_H

namespace Bru
{

//===========================================================================//

enum class KeyboardKey
{
    Unknown,

    Left,
    Right,
    Up,
    Down,

    Backspace,
    Tab,
    Enter,
    Shift,
    Control,
    Alt,
    Escape,
    Space,
	Home,
	End,
    PageUp,
    PageDown,
	Insert,
	Delete,

    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12,

    Digit0,
    Digit1,
    Digit2,
    Digit3,
    Digit4,
    Digit5,
    Digit6,
    Digit7,
    Digit8,
    Digit9,

    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,

    Semicolumn,
    Slash,
    Backquote,
    SquareBracketOpening,
    Backslash,
    SquareBracketClosing,
    Quote,
    Plus,
    Minus,
    Comma,
    Period
};

//===========================================================================//

}

#endif // KEYBOARD_KEY_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Mouse.h"

#include "../../../Utils/Console/Console.h"
#include "../../../OperatingSystem/OperatingSystem.h"

namespace Bru
{

//===========================================================================//

SharedPtr<MouseSingleton> Mouse;

//===========================================================================//

const string MouseSingleton::Name = "Mouse";

//===========================================================================//

MouseSingleton::MouseSingleton() :
	_x(0),
	_y(0),
	_displayX(0),
	_displayY(0),
	_signalEvent(),
	_signal(_signalEvent)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

MouseSingleton::~MouseSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void MouseSingleton::DiscardMessage(OSKeyInfo keyInfo, MouseKey key, KeyState state)
{
	integer scrollClickCount = 0;

	if (key == MouseKey::Scroll)
	{
		scrollClickCount = ((short) HIWORD(keyInfo.VirtualCode) / WHEEL_DELTA);
		if (scrollClickCount > 0)
		{
			state = KeyState::Up;
		}
		else if (scrollClickCount < 0)
		{
			state = KeyState::Down;
			scrollClickCount = -scrollClickCount;
		}
		else
		{
			state = KeyState::None;
		}
	}

	_signalEvent.Fire(MouseEventArgs(key, state, _x, _y, _displayX, _displayY, 0, 0, scrollClickCount));
}

//===========================================================================//

void MouseSingleton::Update()
{
	static POINT newDisplayPosition;
	GetCursorPos(&newDisplayPosition);
	newDisplayPosition.y = (Display->GetYResolution() - 1) - newDisplayPosition.y;
	IntVector2 newPosition = OperatingSystem->DisplayToSurface(newDisplayPosition.x, newDisplayPosition.y);

	if (newPosition.X != _x || newPosition.Y != _y)
	{
		if (OperatingSystem->IsSurfaceFocused(Surface->GetHandle()))
		{
			_signalEvent.Fire(MouseEventArgs(MouseKey::Move, KeyState::None, newPosition.X, newPosition.Y,
			                                 newDisplayPosition.x, newDisplayPosition.y,
			                                 newDisplayPosition.x - _displayX, newDisplayPosition.y - _displayY, 0));
		}
		
		_x = newPosition.X;
		_y = newPosition.Y;

		_displayX = newDisplayPosition.x;
		_displayY = newDisplayPosition.y;
	}
}

//===========================================================================//

integer MouseSingleton::GetX() const
{
	return _x;
}

//===========================================================================//

integer MouseSingleton::GetY() const
{
	return _y;
}

//===========================================================================//

integer MouseSingleton::GetScreenX() const
{
	return _displayX;
}

//===========================================================================//

integer MouseSingleton::GetScreenY() const
{
	return _displayY;
}

//===========================================================================//

} // namespace Bru

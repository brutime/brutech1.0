//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Keyboard.h"

#include "../../../Utils/Console/Console.h"
#include "KeyboardKeysMap.h"

namespace Bru
{

//===========================================================================//

SharedPtr<KeyboardSingleton> Keyboard;

//===========================================================================//

const string KeyboardSingleton::Name = "Keyboard";

//===========================================================================//

const integer KeyboardStateBufferLength = 256;
const integer UnicodeCharLength = 4;

//===========================================================================//

KeyboardSingleton::KeyboardSingleton() :
	_virtualCodeToKeyMap(),
	_keyToVirtualCodeMap(),
	_signalEvent(),
	_signal(_signalEvent)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	for (integer i = 0; i < MappedOSKeysNumber; i++)
	{
		_virtualCodeToKeyMap.Add(KeyboardKeysMap[i].OSCode, KeyboardKeysMap[i].Key);
		_keyToVirtualCodeMap.Add(KeyboardKeysMap[i].Key, KeyboardKeysMap[i].OSCode);
	}

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

KeyboardSingleton::~KeyboardSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

bool KeyboardSingleton::IsKeyPrintableCharacter(KeyboardKey key)
{
	return ((key >= KeyboardKey::Digit0 && key <= KeyboardKey::Digit9) ||
	        (key >= KeyboardKey::A && key <= KeyboardKey::Z) ||
	        (key >= KeyboardKey::Semicolumn && key <= KeyboardKey::Period));
}

//===========================================================================//

string KeyboardSingleton::GetCharForKey(KeyboardKey key)
{
	integer virtualKey = 0;
	if (_keyToVirtualCodeMap.Contains(key))
	{
		virtualKey = _keyToVirtualCodeMap[key];
	}

	integer scanCode = MapVirtualKey(virtualKey, 0);

	static byte keyboardState[KeyboardStateBufferLength];
	GetKeyboardState(keyboardState);

	static ushort buffer[UnicodeCharLength];
	Memory::FillWithNull(buffer, sizeof(buffer));

	ToUnicode(virtualKey, scanCode, keyboardState, reinterpret_cast<wide_char *>(buffer), sizeof(buffer), 0);

	return string(utf16_string(buffer));
}

//===========================================================================//

bool KeyboardSingleton::IsKeyDown(KeyboardKey key)
{
	if (_keyToVirtualCodeMap.Contains(key))
	{
		integer virtualKey = _keyToVirtualCodeMap[key];
		return GetKeyState(virtualKey) & 0x80;
	}

	return false;
}

//===========================================================================//

void KeyboardSingleton::DiscardMessage(OSKeyInfo keyInfo, KeyState state)
{
	if (!_virtualCodeToKeyMap.Contains(keyInfo.VirtualCode))
	{
		_signalEvent.Fire(KeyboardEventArgs(KeyboardKey::Unknown, state));
		return;
	}

	const integer bit30 = static_cast<integer>(1 << 30);
	if (state == KeyState::Down && (keyInfo.KeyData & bit30))
	{
		state = KeyState::RepeatedDown;
	}

	_signalEvent.Fire(KeyboardEventArgs(_virtualCodeToKeyMap[keyInfo.VirtualCode], state));
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: определение массива для задания соответствия между сис.кодом и кодом кнопки
//

#pragma once
#ifndef KEYBOARD_KEYS_MAP_H
#define KEYBOARD_KEYS_MAP_H

#include <windows.h>
#include "../Types/OSToLogicalKeyPair.h"

namespace Bru
{

//===========================================================================//

static const OSToLogicalKeyPair KeyboardKeysMap[] =
{
	{ VK_LEFT, KeyboardKey::Left },
	{ VK_RIGHT, KeyboardKey::Right },
	{ VK_UP, KeyboardKey::Up },
	{ VK_DOWN, KeyboardKey::Down },

	{ VK_BACK, KeyboardKey::Backspace },
	{ VK_TAB, KeyboardKey::Tab },
	{ VK_RETURN, KeyboardKey::Enter },
	{ VK_SHIFT, KeyboardKey::Shift },
	{ VK_CONTROL, KeyboardKey::Control },
	{ VK_MENU, KeyboardKey::Alt },
	{ VK_ESCAPE, KeyboardKey::Escape },
	{ VK_SPACE, KeyboardKey::Space },
	{ VK_HOME, KeyboardKey::Home },
	{ VK_END, KeyboardKey::End },	
	{ VK_PRIOR, KeyboardKey::PageUp },
	{ VK_NEXT, KeyboardKey::PageDown },
	{ VK_INSERT, KeyboardKey::Insert},
	{ VK_DELETE, KeyboardKey::Delete },	

	{ VK_F1, KeyboardKey::F1 },
	{ VK_F2, KeyboardKey::F2 },
	{ VK_F3, KeyboardKey::F3 },
	{ VK_F4, KeyboardKey::F4 },
	{ VK_F5, KeyboardKey::F5 },
	{ VK_F6, KeyboardKey::F6 },
	{ VK_F7, KeyboardKey::F7 },
	{ VK_F8, KeyboardKey::F8 },
	{ VK_F9, KeyboardKey::F9 },
	{ VK_F10, KeyboardKey::F10 },
	{ VK_F11, KeyboardKey::F11 },
	{ VK_F12, KeyboardKey::F12 },

	{ '0', KeyboardKey::Digit0 },
	{ '1', KeyboardKey::Digit1 },
	{ '2', KeyboardKey::Digit2 },
	{ '3', KeyboardKey::Digit3 },
	{ '4', KeyboardKey::Digit4 },
	{ '5', KeyboardKey::Digit5 },
	{ '6', KeyboardKey::Digit6 },
	{ '7', KeyboardKey::Digit7 },
	{ '8', KeyboardKey::Digit8 },
	{ '9', KeyboardKey::Digit9 },

	{ 'A', KeyboardKey::A },
	{ 'B', KeyboardKey::B },
	{ 'C', KeyboardKey::C },
	{ 'D', KeyboardKey::D },
	{ 'E', KeyboardKey::E},
	{ 'F', KeyboardKey::F },
	{ 'G', KeyboardKey::G },
	{ 'H', KeyboardKey::H },
	{ 'I', KeyboardKey::I },
	{ 'J', KeyboardKey::J },
	{ 'K', KeyboardKey::K },
	{ 'L', KeyboardKey::L },
	{ 'M', KeyboardKey::M },
	{ 'N', KeyboardKey::N },
	{ 'O', KeyboardKey::O },
	{ 'P', KeyboardKey::P },
	{ 'Q', KeyboardKey::Q },
	{ 'R', KeyboardKey::R },
	{ 'S', KeyboardKey::S },
	{ 'T', KeyboardKey::T },
	{ 'U', KeyboardKey::U },
	{ 'V', KeyboardKey::V },
	{ 'W', KeyboardKey::W },
	{ 'X', KeyboardKey::X },
	{ 'Y', KeyboardKey::Y },
	{ 'Z', KeyboardKey::Z },

	{ VK_OEM_1, KeyboardKey::Semicolumn },
	{ VK_OEM_2, KeyboardKey::Slash },
	{ VK_OEM_3, KeyboardKey::Backquote },
	{ VK_OEM_4, KeyboardKey::SquareBracketOpening },
	{ VK_OEM_5, KeyboardKey::Backslash },
	{ VK_OEM_6, KeyboardKey::SquareBracketClosing },
	{ VK_OEM_7, KeyboardKey::Quote },
	
	{ 0xBB, KeyboardKey::Plus },
	{ 0xBD, KeyboardKey::Minus },
	{ 0xBC, KeyboardKey::Comma },
	{ 0xBE, KeyboardKey::Period }
};

//===========================================================================//

const integer MappedOSKeysNumber = static_cast<integer>(sizeof(KeyboardKeysMap) / sizeof(OSToLogicalKeyPair));

//===========================================================================//

}// Bru

#endif // KEYBOARD_KEYS_MAP_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: определение массива для задания соответствия между сис.кодом и кодом кнопки
//
#pragma once
#ifndef MOUSE_KEYS_MAP_H
#define MOUSE_KEYS_MAP_H

#include <windows.h>
#include "../Types/OSToLogicalMouseKeyPair.h"

namespace Bru
{

//===========================================================================//

static const OSToLogicalMouseKeyPair MouseKeysMap[] =
{
	{ VK_LBUTTON, MouseKey::Left },
	{ VK_RBUTTON, MouseKey::Right },
	{ VK_MBUTTON, MouseKey::Middle }
};

//===========================================================================//

const integer MappedOSMouseKeysNumber = static_cast<integer>(sizeof(MouseKeysMap) / sizeof(OSToLogicalMouseKeyPair));

//===========================================================================//

}//namespace Bru

#endif // MOUSE_KEYS_MAP_H

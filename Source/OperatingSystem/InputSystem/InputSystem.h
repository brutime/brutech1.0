//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс, обрабатывающий сигналы от всех устройств ввода
//

#ifndef INPUT_SYSTEM_H
#define INPUT_SYSTEM_H

#include "InputReceiver.h"
#include "Keyboard.h"
#include "Mouse.h"
#include "Types/KeyboardEventArgs.h"
#include "Types/MouseEventArgs.h"
#include "Types/JoystickEventArgs.h"

namespace Bru
{

//===========================================================================//

class InputSystemSingleton
{
public:
	InputSystemSingleton();
	~InputSystemSingleton();

	void AddDomain(const string & domainName);
	void RemoveDomain(const string & domainName);
	void SelectDomain(const string & domainName);

	void RegisterComponent(InputReceiver * const component);
	void UnregisterComponent(InputReceiver * const component);

	void Update();

	void EnableGlobalDomainOnlyMode();
	void DisableGlobalDomainOnlyMode();
	bool IsServiceOnlyModeEnabled() const;
	
	void AddKeyboardKeyUsingInGlobalDomainOnly(KeyboardKey key);
	void RemoveKeyboardKeyUsingInGlobalDomainOnly(KeyboardKey key);
	
	void AddMouseKeyUsingInGlobalDomainOnly(MouseKey key);
	void RemoveMouseKeyUsingInGlobalDomainOnly(MouseKey key);	

	const string & GetCurrentDomainName() const;

	integer GetComponentCount(const string & domainName) const;
	integer GetComponentCount() const;
	integer GetEnabledComponentCount() const;
	
	void PrintInputReceivers(const string & domainName) const;
	void PrintInputReceivers() const;

private:
	static const string Name;

	void RemoveAllDomains();

	void OnKeyboardSingnal(const KeyboardEventArgs & args);
	void OnMouseSingnal(const MouseEventArgs & args);

	void DispatchKeyboardEvent(const string & domainName, const KeyboardEventArgs & args);
	void DispatchMouseEvent(const string & domainName, const MouseEventArgs & args);

	TreeMap<string, List<InputReceiver * const >> _components;
	string _currentDomainName;

	bool _isServiceOnlyModeEnabled;
	
	List<KeyboardKey> _keyboardKeysUsingInGlobalDomainOnly;
	List<MouseKey> _mouseKeysUsingInGlobalDomainOnly;

	SharedPtr<EventHandler<const KeyboardEventArgs &>> _keyboardSignalHandler;
	SharedPtr<EventHandler<const MouseEventArgs &>> _mouseSignalHandler;

	InputSystemSingleton(const InputSystemSingleton &) = delete;
	InputSystemSingleton & operator =(const InputSystemSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<InputSystemSingleton> InputSystem;

//===========================================================================//

}// namespace Bru

#endif // INPUT_SYSTEM_H

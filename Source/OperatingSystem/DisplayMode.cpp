//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DisplayMode.h"

namespace Bru
{

//===========================================================================//

DisplayMode::DisplayMode() :
	_width(0),
	_height(0),
	_bitsPerPixel(0),
	_frequency(0),
	_flags(0)
{
}

//===========================================================================//

DisplayMode::DisplayMode(integer width, integer height, byte bitsPerPixel, byte frequency, integer flags) :
	_width(width),
	_height(height),
	_bitsPerPixel(bitsPerPixel),
	_frequency(frequency),
	_flags(flags)
{
}

//===========================================================================//

DisplayMode::DisplayMode(const DisplayMode & other) :
	_width(other._width),
	_height(other._height),
	_bitsPerPixel(other._bitsPerPixel),
	_frequency(other._frequency),
	_flags(other._flags)
{
}

//===========================================================================//

DisplayMode::~DisplayMode()
{
}

//===========================================================================//

DisplayMode & DisplayMode::operator =(const DisplayMode & other)
{
	if (this == &other)
	{
		return *this;
	}

	_width = other._width;
	_height = other._height;
	_bitsPerPixel = other._bitsPerPixel;
	_frequency = other._frequency;
	_flags = other._flags;

	return *this;
}

//===========================================================================//

bool DisplayMode::operator ==(const DisplayMode & someDisplayMode) const
{
	return _width == someDisplayMode._width && _height == someDisplayMode._height
	       && _bitsPerPixel == someDisplayMode._bitsPerPixel && _frequency == someDisplayMode._frequency
	       && _flags == someDisplayMode._flags;
}

//===========================================================================//

bool DisplayMode::operator !=(const DisplayMode & someDisplayMode) const
{
	return !(*this == someDisplayMode);
}

//===========================================================================//

string DisplayMode::ToString() const
{
	string displayModeString = string::Format("{0}x{1}x{2}@{3}", _width, _height, _bitsPerPixel, _frequency);
	if (_flags != 0)
	{
		displayModeString += "some flags";
	}

	return displayModeString;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: характеристики компьютера, получаемые с помощью ОС
//

#ifndef OS_CAPABILITIES_H
#define OS_CAPABILITIES_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

class OSCapabilities
{
public:
	static integer GetXDPI();
	static integer GetYDPI();

private:
	OSCapabilities() = delete;
	OSCapabilities(const OSCapabilities &) = delete;
	~OSCapabilities() = delete;
	OSCapabilities & operator =(const OSCapabilities &) = delete;
};

//===========================================================================//

}// namespace

#endif // OS_CAPABILITIES_H

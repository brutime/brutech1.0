//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef OPERATING_SYSTEM_TYPES_H
#define OPERATING_SYSTEM_TYPES_H

#include "../Common/Defines/Defines.h"

#if PLATFORM == WINDOWS

#include <windows.h>

#elif PLATFORM == LINUX

#include <X11/Xlib.h>
#include <GL/glx.h>
#elif PLATFORM == MAC

#error No realization for MAC

#else
#error Platform not defined
#endif

namespace Bru
{

//===========================================================================//

#if PLATFORM == WINDOWS

typedef HANDLE handle;
typedef HWND surface_handle;
typedef HDC device_context;
typedef HGLRC rendering_context;
typedef LRESULT CALLBACK (*window_proc)(HWND, UINT, WPARAM, LPARAM);
typedef int (*FunctionPtr)();

#elif PLATFORM == LINUX

typedef ::Display * device_context; //GC
typedef GLXContext rendering_context;
typedef ::Window surface_handle;
typedef void * (*window_proc)(void *);
typedef void (*FunctionPtr)();

#elif PLATFORM == MAC

#error No realization for MAC

#else
#error Platform not defined
#endif

//===========================================================================//

}
//namespace

#endif // OPERATING_SYSTEM_TYPES_H

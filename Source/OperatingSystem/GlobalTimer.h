//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Глобальный таймер
//

#ifndef GLOBAL_TIMER_H
#define GLOBAL_TIMER_H

#include "Timer.h"

namespace Bru
{

//===========================================================================//

class GlobalTimerSingleton : public Timer
{
public:
	GlobalTimerSingleton();
	virtual ~GlobalTimerSingleton();

private:
	GlobalTimerSingleton(const GlobalTimerSingleton &);
	GlobalTimerSingleton & operator =(const GlobalTimerSingleton &);
};

//===========================================================================//

extern SharedPtr<GlobalTimerSingleton> GlobalTimer;

//===========================================================================//

}// namespace

#endif // GLOBAL_TIMER_H

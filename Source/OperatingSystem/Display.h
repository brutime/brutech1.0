//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс экрана
//

#pragma once
#ifndef DISPLAY_H
#define DISPLAY_H

#include "DisplayMode.h"
#include "OperatingSystemTypes.h"

namespace Bru
{

//===========================================================================//

class DisplaySingleton
{
public:
	DisplaySingleton();
	~DisplaySingleton();

	void SelectMode(integer modeIndex);
	void SelectMode(integer width, integer height, integer bitsPerPixel);
	void RestoreMode();

	const List<SharedPtr<DisplayMode> > & GetModes()
	{
		return _modes;
	}
	void RemoveUnsuitableMode(const SharedPtr<DisplayMode> & mode);

	integer GetXResolution()
	{
		return _xResolution;
	}
	integer GetYResolution()
	{
		return _yResolution;
	}

	float GetReversedXResolution()
	{
		return _reversedXResolution;
	}
	float GetReversedYResolution()
	{
		return _reversedYResolution;
	}

private:
	void SelectMode(const SharedPtr<DisplayMode> & mode);
	bool ContainsMode(const SharedPtr<DisplayMode> & mode);

	void BuildModesList();
	void UpdateResolution();
	SharedPtr<DisplayMode> FindSuitableDisplayMode(integer width, integer height, integer bitsPerPixel);

#if PLATFORM == WINDOWS

	DEVMODE CreateEmptyDevMode();

#endif

	List<SharedPtr<DisplayMode> > _modes;
	integer _xResolution;
	integer _yResolution;
	float _reversedXResolution;
	float _reversedYResolution;

	DisplaySingleton(const DisplaySingleton &);
	DisplaySingleton & operator =(const DisplaySingleton &);
};

//===========================================================================//

extern SharedPtr<DisplaySingleton> Display;

//===========================================================================//

}//namespace

#endif // DISPLAY_H

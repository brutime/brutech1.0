#include "Application.h"
#define XK_MISCELLANY // выбираем общий(нормальный, человеческий) набор клавиш, который будет использоваться, т.к. в X11/keysymdef.h есть объявления для хреновой тучи наборов
#include <X11/keysymdef.h>
#include "Application.h"
#include "Display.h"
#include "../../Input/Keyboard.h"

#ifdef DEBUG_WINDOW
#include <stdio.h>
#endif

namespace Bru
{

//===========================================================================//

//void* MainWindowEventHandler(::Window* windowHandle, UINT message, WPARAM wParam, LPARAM lParam);

//===========================================================================//

AbstractApplication::AbstractApplication(): ApplicationBase()
{
}

//===========================================================================//

AbstractApplication::~AbstractApplication()
{
}

//===========================================================================//
void AbstractApplication::ApplicationMain()
{
	bool quit = false;
	Display * display;

	while (!quit)
	{
		do
		{
			XEvent event;
			display = BruDisplay->GetHandle();
			XNextEvent(display, &event);
			switch (event.type)
			{
			case KeyPress:
			{
				KeySym     keysym;
				XKeyEvent * kevent;
				char       buffer[1];
				/* It is necessary to convert the keycode to a
				 * keysym before checking if it is an escape */
				kevent = (XKeyEvent *) &event;
				if ((XLookupString((XKeyEvent *)&event, buffer, 1, &keysym, NULL) == 1)
				        && (keysym == (KeySym)XK_Escape))
					quit=true;
				break;
			}
			/*case ConfigureNotify:
				glViewport(0, 0, event.xconfigure.width,
						   event.xconfigure.height);
				/* fall through... */
			/*case Expose:
				needRedraw = GL_TRUE;
				break;
				*/
			}
		}
		while(XPending(display));   /* loop to compress events */

		MainLoop();
	}
//win-specific
	/*    Initialize();

	    MSG msg;
	    bool quit = false;
	    while (!quit)
	    {
	        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	        {
	            if (msg.message == WM_QUIT)
	            {
	                printf("WM_QUIT received - exiting loop...\n");
	                quit = TRUE;
	            }
	            else
	            {
	                TranslateMessage(&msg);
	                DispatchMessage(&msg);
	            }
	        }
	        else
	        {
	            MainLoop();
	            //Sleep(1);//?
	        }
	    }

	    FreeResources();
	*/
}

//===========================================================================//

void AbstractApplication::DispatchKeyboardMessage(KeyboardMessage message)
{
}

//===========================================================================//

void AbstractApplication::DispatchMouseMessage(MouseMessage message)
{
}

//===========================================================================//

void AbstractApplication::DispatchJoystickMessage(const JoystickMessage & message)
{
}

//===========================================================================//

/*window_proc AbstractApplication::GetMainEventHandler()
{
    return NullPtr;
    //return MainWindowEventHandler;
}*/

//===========================================================================//
//это заготовка для оконной процедуры - обработчика сообщений
/*
LRESULT CALLBACK MainWindowEventHandler(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CLOSE:
        PostQuitMessage(0);
        break;

    case WM_DESTROY:
        return 0;

    case WM_KEYDOWN:
//        DispatchKeyboardMessage(wParam);
        break;

    default:
        return DefWindowProc(windowHandle, message, wParam, lParam);
    }

    return 0L;
}
*/
//===========================================================================//

} // namespace

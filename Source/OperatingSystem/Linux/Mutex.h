//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: linux-реализация класса мьютекса
//

#pragma once
#ifndef MUTEX_H
#define MUTEX_H

#include "../ThreadID.h"

//#define MUTEX_DEBUG

namespace Bru
{

//===========================================================================//

class Mutex
{
private:
	//HANDLE mutexHandle;
	pthread_mutex_t mutexHandle;

	ThreadType ownerThread;

public:
	bool isCreated;
	void Lock();
	void Unlock();
	Mutex();
	~Mutex();
};

//===========================================================================//

} //namespace

#endif // MUTEX_H

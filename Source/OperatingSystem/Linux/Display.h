//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: linux-реализация класса экрана
//

#pragma once
#ifndef DISPLAY_H
#define DISPLAY_H

#include "../Display.h"

#include <X11/Xlib.h>
#include "../../Common/Common.h"
#include "../../Common/Containers.h"
#include "../DisplayMode.h"

namespace Bru
{

//===========================================================================//

class SingletonDisplay: public IDisplay, public SingletonMixIn<SingletonDisplay>
{
	friend class SingletonMixIn<SingletonDisplay>;

public:

	virtual ~SingletonDisplay();

	virtual SharedPtr< Array<DisplayMode> > GetDisplayModes();
	virtual bool SetDisplayMode(const DisplayMode & mode);
	virtual bool SetDisplayMode(integer modeIndex);
	virtual bool SetDisplayMode(integer width, integer height, integer bitsPerPixel, integer frequency);
	//virtual bool ResetDisplayMode();
	virtual bool RestoreDisplayMode();

	virtual void ChoosePixelFormat(const device_context deviceContext, const integer colorBufferDepth,
	                               const integer depthBufferDepth);

	virtual integer GetXResolution();
	virtual integer GetYResolution();
	virtual ::Display * GetHandle();

protected:
	SingletonDisplay();

private:
	integer xresolution;
	integer yresolution;
	Display * displayHandle;
};

//===========================================================================//

#define BruDisplay SingletonDisplay::Instance()

//===========================================================================//

} //namespace

#endif // DISPLAY_H

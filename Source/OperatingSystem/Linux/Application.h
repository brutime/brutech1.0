//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс приложения. Должен заниматься инициализацией подсистем, загружать
//    необходимые ресурсы. Содержит указатель на точку входа в главный цикл обработки сообщений.
//

#pragma once
#ifndef APPLICATION_H
#define APPLICATION_H
#include "../Application.h"
#include "../../Common/Helpers/SingletonMixIn.h"

//#define DEBUG_WINDOW

namespace Bru
{

//===========================================================================//

enum WINDOW_CLASS
{
    WINDOW_CLASS_MAIN
};

//===========================================================================//

class AbstractApplication : public ApplicationBase
{
public:
	virtual ~AbstractApplication();

	void ApplicationMain();

	void DispatchKeyboardMessage(const KeyboardMessage & message);
	void DispatchMouseMessage(const MouseMessage & message);
	void DispatchJoystickMessage(const JoystickMessage & message);

	SharedPtr<Window> GetMainWindow()
	{
		return mainWindow;
	}
	virtual window_proc GetMainEventHandler() = 0;

protected:

	AbstractApplication();

	SharedPtr<Window> mainWindow;
};

//===========================================================================//

}
#endif // APPLICATION_H

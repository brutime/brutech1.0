//
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//
#include "Mutex.h"

#include "../Thread.h"
#include "../../Common/Common.h"
#include <pthread.h>

using namespace Bru;

Mutex::Mutex(): isCreated(true)
{
	pthread_mutexattr_t mattr;

	pthread_mutexattr_init(&mattr);
	pthread_mutex_init(&mutexHandle, &mattr);

//    mutexHandle = CreateMutex(NullPtr, false, NullPtr);
//    if (!mutexHandle) isCreated = false;
}

Mutex::~Mutex()
{
	pthread_mutex_lock(&mutexHandle);
	pthread_mutex_unlock(&mutexHandle);
	pthread_mutex_destroy(&mutexHandle);

//    WaitForSingleObject(mutexHandle, INFINITE);
//    CloseHandle(mutexHandle);
}

void Mutex::Lock()
{
#ifdef MUTEX_DEBUG
	printf("Mutex::Lock() called (ownerId=%x)\n", ownerThread);
#endif
	ThreadType id = CThread::GetCurrentThreadId();
	if (CThread::ThreadIdsEqual(&ownerThread, &id))
		return;
	pthread_mutex_lock(&mutexHandle);

	//WaitForSingleObject(mutexHandle, INFINITE);

	ownerThread = CThread::GetCurrentThreadId();
}

void Mutex::Unlock()
{
#ifdef MUTEX_DEBUG
	printf("Mutex::Unlock() (ownerId=%x)\n", ownerThread);
#endif
	ThreadType id = CThread::GetCurrentThreadId();
	if (! CThread::ThreadIdsEqual(&id, &ownerThread))
		return;
	memset(&ownerThread, 0, sizeof(ThreadType));
	pthread_mutex_unlock(&mutexHandle);	//ReleaseMutex(mutexHandle);
}


//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef TIMER_H
#define TIMER_H

#include "../Timer.h"

namespace Bru
{

//===========================================================================//

class Timer : public ITimer
{
public:
	Timer();
	virtual ~Timer();

	void Reset();
	float GetSeconds() const;
	int64 GetTicks() const;

private:
	long long GetTime() const;

	long long startTime;
	long long frequency;

	Timer(const Timer &);
	Timer & operator =(const Timer &);
};

//===========================================================================//

}

#endif // TIMER_H


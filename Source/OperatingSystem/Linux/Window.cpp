//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: windows-реализация класса окна
//

#include "Window.h"
#include "../Application.h"
#include "../Display.h"
#include "../../Common/Exceptions/InitializerException.h"

namespace Bru
{

Window::Window(const string & title, integer width, integer height, bool isFullScreened): AbstractWindow(title, width, height, isFullScreened), isCreated(false)
{
#ifdef DEBUG_LINUX_WINDOW
	printf("Creating window...");
#endif
	::Display * display = Display->GetHandle();
	printf("display=%p", display);
	int blackColor = BlackPixel(display, DefaultScreen(display));
	int whiteColor = WhitePixel(display, DefaultScreen(display));
	XSetWindowAttributes windowAttributes;
	windowAttributes.background_pixel = WhitePixel(display, DefaultScreen(display));

	if (isFullScreened)
	{
#ifdef DEBUG_LINUX_WINDOW
		printf(" in fullscreen\n");
#endif
		windowHandle = //XCreateSimpleWindow(display, DefaultRootWindow(display), 0, 0, Display->GetXResolution(), Display->GetYResolution(), 1, blackColor, whiteColor);
		    XCreateWindow(display, DefaultRootWindow(display), 0, 0, Display->GetXResolution(), Display->GetYResolution(), 0, DefaultDepth(display, 0), InputOutput, DefaultVisual(display, 0), CWBackPixel, &windowAttributes);
	}
	else
	{
#ifdef DEBUG_LINUX_WINDOW
		printf(" clipped\n");
#endif
		windowHandle = //XCreateSimpleWindow(display, DefaultRootWindow(display), (Display->GetXResolution() - width) / 2, (Display->GetYResolution() - height) / 2, Display->GetXResolution(), Display->GetYResolution(), 1, blackColor, whiteColor);
		    XCreateWindow(display, DefaultRootWindow(display), (Display->GetXResolution() - width) / 2, (Display->GetYResolution() - height) / 2, Display->GetXResolution(), Display->GetYResolution(), 0, DefaultDepth(display, 0), InputOutput, DefaultVisual(display, 0), CWBackPixel, &windowAttributes);
	}

	if (windowHandle == BadWindow)
	{
		INITIALIZER_EXCEPTION("Culdn't create window");
	}

	//задаем маску принимаемых сообщений
	XSelectInput(display, windowHandle, StructureNotifyMask);

	//отобразить окно
	XMapWindow(display, windowHandle);

	//дождаться отображения окна на экране
	for (;;)
	{
		XEvent e;
		XNextEvent(display, &e);
		if (e.type == MapNotify)
			break;
	}

	isCreated = true;
}

//===========================================================================//

Window::~Window()
{
	Close();
}

//===========================================================================//

void Window::Minimize()
{
	XUnmapWindow(Display->GetHandle(), windowHandle);
}

//===========================================================================//

void Window::Normalize()
{
	XMapWindow(Display->GetHandle(), windowHandle);
}

//===========================================================================//

device_context Window::GetDeviceContext()
{
	return 0;
	//return XCreateGC (Display->GetHandle(), windowHandle);
}

//===========================================================================//

void Window::Close()
{
	printf("Window::Close called...");
	if (windowHandle)
	{
		XDestroyWindow(Display->GetHandle(), windowHandle);
		windowHandle = NullPtr;
	}
}

//===========================================================================//

WindowHandleType Window::GetHandle()
{
	return windowHandle;
}

//===========================================================================//

} //namespace

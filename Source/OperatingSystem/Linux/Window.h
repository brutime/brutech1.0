//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: linux-реализация класса окна
//

#pragma once
#ifndef WINDOW_H
#define WINDOW_H
#include "../Window.h"
#include <X11/Xlib.h>

#define DEBUG_LINUX_WINDOW
namespace Bru
{

//===========================================================================//

class Window : public AbstractWindow
{
public:
	Window();
	Window(const window_proc mainEventHandler, const string & title, integer width, integer height, bool isFullScreened); //, AppInstanceType instance);
	virtual ~Window();

	void Minimize();
	void Normalize();
	void Close();

	surface_handle GetHandle();

	void SwitchFullScreen();

	integer GetWidth() const;
	integer GetHeight() const;

private:
	surface_handle   windowHandle;
	string          className;
	bool            isClassRegistered;

	void Register(const window_proc mainEventHandler);

	surface_handle CreateWindowedWindow();
	surface_handle CreateFullScreenWindow();
};

//===========================================================================//

}

#endif // WINDOW_H

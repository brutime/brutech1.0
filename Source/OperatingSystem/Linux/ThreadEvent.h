//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: linux-реализация класса события
//

#pragma once
#ifndef THREAD_EVENT_H
#define THREAD_EVENT_H

#include "../IThreadEvent.h"
#include "../ThreadID.h"

namespace Bru
{

//===========================================================================//

class ThreadEvent: public IThreadEvent
{
	friend class CThread;

private:
	//HANDLE eventHandle;
	pthread_cond_t  isReady;
	bool            isReallyReady;
	pthread_mutex_t lock;

public:
	bool isCreated;
	void Set();
	bool Wait(TimeInMsType milliseconds=INFINITE);
	void Reset();
	ThreadEvent(bool autoReset=true);
	~ThreadEvent();
};

//===========================================================================//

} //namespace

#endif // THREAD_EVENT_H

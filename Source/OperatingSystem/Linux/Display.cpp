//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: windows-реализация класса экрана
//
#include "Display.h"
#include <X11/Xlib.h>
#include "../../Common/Containers.h"
#include "../../Common/Exceptions/InitializerException.h"

namespace Bru
{

//===========================================================================//

SingletonDisplay::~SingletonDisplay()
{
	RestoreDisplayMode();
}

//===========================================================================//

SingletonDisplay::SingletonDisplay()
{
	displayHandle = XOpenDisplay(NullPtr);
	if (displayHandle == NullPtr)
	{
		INITIALIZER_EXCEPTION("Display not opened. Epic fail");
	}

	xresolution = XDisplayWidth(displayHandle, XDefaultScreen(displayHandle));
	yresolution = XDisplayHeight(displayHandle, XDefaultScreen(displayHandle));
}

//===========================================================================//

SharedPtr< Array< DisplayMode > >SingletonDisplay::GetDisplayModes()
{
	return NullPtr;
	/*List<DisplayMode> availableModes;
	integer modeNum = 0;
	DEVMODE mode;
	while (EnumDisplaySettings(NULL, modeNum, &mode))
	{
		availableModes.Add(DisplayMode(mode.dmPelsWidth, mode.dmPelsHeight, mode.dmBitsPerPel, mode.dmDisplayFrequency, mode.dmDisplayFlags, modeNum));
		modeNum++;
	}

	return availableModes.ToArray();
	*/
}

//===========================================================================//

bool SingletonDisplay::SetDisplayMode(const DisplayMode & mode)
{
	return false;
	/*
	DEVMODE newMode;
	newMode.dmBitsPerPel = mode.GetBitsPerPixel();
	newMode.dmPelsWidth = mode.GetWidthInPixels();
	newMode.dmPelsHeight = mode.GetHeightInPixels();
	newMode.dmDisplayFlags = mode.GetFlags();
	newMode.dmDisplayFrequency = mode.GetFrequency();
	newMode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFLAGS | DM_DISPLAYFREQUENCY;

	if ((ChangeDisplaySettings(&newMode, CDS_TEST) == DISP_CHANGE_SUCCESSFUL) && (ChangeDisplaySettings(&newMode, 0) == DISP_CHANGE_SUCCESSFUL))
		return true;
	else
		return false;
		*/
}

//===========================================================================//

bool SingletonDisplay::SetDisplayMode(integer modeIndex)
{
	return false;
}

//===========================================================================//

bool SingletonDisplay::SetDisplayMode(integer width, integer height, integer bitsPerPixel, integer frequency)
{
	return false;
}

//===========================================================================//

/*bool SingletonDisplay::ResetDisplayMode()
{
	return false;
}*/

//===========================================================================//

bool SingletonDisplay::RestoreDisplayMode()
{
	return false;
	//return (ChangeDisplaySettings(NULL, 0) == DISP_CHANGE_SUCCESSFUL) ;
}

//===========================================================================//

void SingletonDisplay::ChoosePixelFormat(const device_context deviceContext, const integer colorBufferDepth,
        const integer depthBufferDepth)
{

}

//===========================================================================//

integer SingletonDisplay::GetXResolution()
{
	return xresolution;
}

//===========================================================================//

integer SingletonDisplay::GetYResolution()
{
	return yresolution;
}

//===========================================================================//
//  UNIX-specific
//===========================================================================//

::Display * SingletonDisplay::GetHandle()
{
	return displayHandle;
}

} // namespace

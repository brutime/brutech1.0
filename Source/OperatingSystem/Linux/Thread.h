//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: linux-реализация класса потока
//

#pragma once
#ifndef THREAD_IMPL_H
#define THREAD_IMPL_H

#include "../ThreadID.h"

#include "../Event.h"
#include "../Task.h"
#include "../../Common/Containers.h"
//#include "../../Common/Exceptions/ThreadException.h"

//#define LINUX_THREAD_DEBUG

namespace Bru
{

//===========================================================================//

/*enum THREAD_STATE
{
    THREAD_STATE_BUSY,               	// thread is currently handling a task
    THREAD_STATE_WAITING,            	// thread is waiting for something to do
    THREAD_STATE_DOWN,               	// thread is not running
    THREAD_STATE_SHUTTING_DOWN,       	// thread is in the process of shutting down
    THREAD_STATE_FAULT	            	// an error has occured and the thread could not
    // be launched
};*/

enum THREAD_TYPE
{
    THREAD_TYPE_EVENT_DRIVEN,
    THREAD_TYPE_INTERVAL_DRIVEN
};

enum THREAD_ERRORS
{
    NO_ERRORS = 			0,
    MUTEX_CREATION = 		0x01,
    EVENT_CREATION = 		0x02,
    THREAD_CREATION = 		0x04,
    UNKNOWN = 				0x08,
    ILLEGAL_USE_OF_EVENT = 	0x10,
    MEMORY_FAULT = 			0x20
};

class CThread
{
private:
	static StackSizeType DEFAULT_STACK_SIZE;

	CEvent			jobEvent;			// event controller
	CEvent 			finishedEvent;		// показывает, что запланированная работа выполнена

	bool			terminateWhenDone;	// удалять ли поток после завершения
	bool			isTerminating;		// поток завершается
	bool			isSuspended;		// поток приостановлен
	bool            hasStarted;         // в юникс потоки  могут быть созданы приостановленными, поэтому мы просто не создаем до первого вызова Resume()
	ThreadType      tmpType;
	bool            tmpTerminateAfterExecution;

	ThreadType 		threadHandle;		// thread handle
	ThreadType 	threadId;			// id of this thread
	List<void *> 	taskQue;			// очередь задач для обработки
	TimeInMsType	idlePeriod;			// интервалы между вызовами обработки задач по таймеру
	void 	*		dataToProcess;		// обрабатываемая задача
	THREAD_TYPE 	threadType;			// тип потока - просыпающийся по таймеру или при наступлении события
	StackSizeType	stackSize;			// thread stack size
	ObjectConditionType	objectCondition;	// состояние объекта потока
	bool			Push(void * lpv);	// добавление задачи в очередь
	bool			Pop();				// извлечение задачи из очереди
	bool			IsEmpty();			// пуста ли очередь
	void            WaitForResume();    // unix-specific

protected:

	Mutex	  		mutex;         		// мьютекс для синхронизации доступа к данным-членам класса

//    pthread_mutex_t suspendMutex;       // мьютекс для реализации приостановки (Suspend/Resume) (хз, нужен ли отдельный мьютекс, или mutex тоже подошел бы)
//    pthread_cond_t  suspendCondition;
//    bool            isSuspended;
	CEvent          resumedEvent;

	bool			KernelProcess(); 	// обрабатывает отдельную задачу из очереди

	bool			Start(bool suspended);		// создает внутренний поток, руками вызывать не нужно
	virtual bool 	OnTask(void * taskToRun); 	// вызывается при появлении новой задачи в очереди

	friend void * _THKERNEL(void * taskPointer);	// точка входа внутреннего потока-обработчика

public:

	CThread(THREAD_TYPE type, bool terminateAfterExecution);
	virtual ~CThread(void);

	bool        	AddTask(Task * pvTask);
	ThreadType	GetId()
	{
		return threadId;    // ID процесса
	}
	void			SetPriority(ThreadPriorityType dwPriority = 0);
	ObjectConditionType	GetErrorFlags()
	{
		return objectCondition;
	}
	//менять на ходу - вроде как ненужно и плохо
	//void			SetThreadType(THREAD_TYPE type = THREAD_TYPE_EVENT_DRIVEN, DWORD dwIdle = 100);
	void			SetIdle(TimeInMsType dwIdle = 100);
	integer 		GetPendingTasksCount();				// кол-во задач, ожидающих в очереди
	bool 			Join(TimeInMsType milliseconds = INFINITE);// дождаться выполнения всех запланированных задач
	bool			IsAlive();
	void			Terminate();						// негрубое завершение потока
	void			Suspend();
	void			Resume();

	static bool ThreadIdsEqual(ThreadType * p1, ThreadType * p2)
	{
		return (pthread_equal(*p1, *p2) ? true : false);

		//return ((*p1 == *p2) ? true : false);
	}

	static ThreadType GetCurrentThreadId()
	{
		ThreadType thisThreadsId = pthread_self();

//        ThreadType thisThreadsId ;
//        thisThreadsId = ::GetCurrentThreadId();

		return thisThreadsId;
	}

	static void Sleep(integer milli);
};

//===========================================================================//

} //namespace

#endif // THREAD_IMPL_H

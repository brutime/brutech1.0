//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: windows-реализация класса события
//

#include "ThreadEvent.h"

#include <errno.h>
#include <sys/time.h>

#include "../../Common/Exceptions/Exception.h"
using namespace Bru;

//событие автоматически переводится в несигнальное состояние
//после того, как его кто-то дождался, поэтому Reset() не используется
ThreadEvent::ThreadEvent(bool autoReset): isCreated(true), isReallyReady(false)
{
	/*eventHandle = CreateEvent(NullPtr, !autoReset, false, NullPtr);
	if (!eventHandle)
	{
	    isCreated = false;
	}*/

	pthread_mutexattr_t mattr;

	pthread_mutexattr_init(&mattr);
	pthread_mutex_init(&lock,&mattr);
	pthread_cond_init(&isReady,NULL);
}

ThreadEvent::~ThreadEvent(void)
{
	//CloseHandle(eventHandle);

	pthread_cond_destroy(&isReady);
	pthread_mutex_destroy(&lock);
}


void ThreadEvent::Set()
{
	pthread_mutex_lock(&lock);
	isReallyReady = true;
	pthread_mutex_unlock(&lock);
	pthread_cond_signal(&isReady);

//	if (!eventHandle)
//		INVALID_STATE_EXCEPTION_FULL("ThreadEvent::Set: invalid handle",this);
//    SetEvent(eventHandle);
}


bool ThreadEvent::Wait(TimeInMsType milliseconds)
{
	if (isReallyReady)
		return true;

	pthread_mutex_lock(&lock);

	struct timeval now;
	struct timezone zone;
	struct timespec absTime;
	gettimeofday(&now, &zone);
	absTime.tv_sec = now.tv_sec + (milliseconds / 1000);
	absTime.tv_nsec = now.tv_usec * 1000 + (milliseconds % 1000) * 1000000L;

	while (true)
	{
		if (milliseconds == INFINITE)
		{
			pthread_cond_wait(&isReady,&lock);
		}
		else
		{
			if (pthread_cond_timedwait(&isReady, &lock, &absTime) == ETIMEDOUT)
				return false;
		}
		if (isReallyReady)
			break;
	}
	return true;

//	if (!eventHandle)
//		INVALID_STATE_EXCEPTION_FULL("ThreadEvent::Wait: invalid handle",this);
//    if (WaitForSingleObject(eventHandle, milliseconds) != WAIT_OBJECT_0)
//    {
//        return false;
//    }
//    return true;
}


void ThreadEvent::Reset()
{
	isReallyReady = false;
	pthread_mutex_unlock(&lock);

//	if (!eventHandle)
//		INVALID_STATE_EXCEPTION_FULL("ThreadEvent::Reset: invalid handle",this);
//	ResetEvent(eventHandle);
}

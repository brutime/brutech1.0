//
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: linux-реализация класса потока
//
#include "../Linux/Thread.h"
#include <pthread.h>
#include <unistd.h>

namespace Bru
{

//===========================================================================//
StackSizeType CThread::DEFAULT_STACK_SIZE = 0;
//===========================================================================//

extern "C"
{
	int	usleep(useconds_t useconds);
}

void CThread::Sleep(integer milli)
{
	//printf("%d...", milli);
	usleep(milli * 1000);
}

//===========================================================================//

void * _THKERNEL(void * lpvData)
{
	CThread * targetThread = (CThread *)lpvData;
	targetThread->threadId=CThread::GetCurrentThreadId();
//    THREAD_TYPE lastType;

	while (targetThread->IsAlive())
	{
#ifdef LINUX_THREAD_DEBUG
		printf("Waiting for thread [%x] job event...\n", targetThread->threadId);
#endif
		if (targetThread->jobEvent.Wait())
		{
#ifdef LINUX_THREAD_DEBUG
			printf("Calling KernelProcess() for thread %x\n", targetThread->threadId);
#endif
			if (! targetThread->KernelProcess())
				break;
		}
	}

#ifdef LINUX_THREAD_DEBUG
	printf("Thread[%x] main() terminating...\n", targetThread->threadId);
#endif

	return 0;
}

//===========================================================================//

bool CThread::OnTask(void * lpvData)
{
	CTask * pTask = (CTask *)lpvData;

	pTask->SetTaskStatus(TaskStatusBeingProcessed);
#ifdef LINUX_THREAD_DEBUG
	printf("Thread[%x]::OnTask: calling pTask->Task()...\n", threadId);
#endif
	bool bReturn = pTask->Task();

	pTask->SetTaskStatus(TaskStatusCompleted);
#ifdef LINUX_THREAD_DEBUG
	printf("Thread[%x] finishedEvent set to signaled...\n", threadId);
#endif

	finishedEvent.Set();

	return bReturn;
}

//===========================================================================//

bool CThread::AddTask(CTask * task)
{
//    ThreadType id;
	task->SetId(GetId());
	if (! Push((void *)task))
		return false;
	task->SetTaskStatus(TaskStatusWaitingOnQueue);
	jobEvent.Set();
	return true;
}

//===========================================================================//

void CThread::SetPriority(ThreadPriorityType dwPriority)
{
	//SetThreadPriority(threadHandle, dwPriority);
}

//===========================================================================//

bool CThread::KernelProcess()
{
	mutex.Lock();

	if (isTerminating)
	{
#ifdef LINUX_THREAD_DEBUG
		printf("Thread[%x]::KernelProcess: terminating \n", threadId);
#endif
		mutex.Unlock();
		return false;
	}
	mutex.Unlock();

	if (!IsEmpty())
	{
		if (isTerminating)
		{
			return false;
		}

		if (isSuspended)
		{
			WaitForResume();
		}


#ifdef LINUX_THREAD_DEBUG
		printf("Thread[%x]::KernelProcess: waiting for job\n", threadId);
#endif

		Pop();
#ifdef LINUX_THREAD_DEBUG
		printf("Thread[%x]::KernelProcess: processing task\n", threadId);
#endif
		if (!OnTask(dataToProcess))
		{
			mutex.Lock();
			dataToProcess = NullPtr;
			mutex.Unlock();
			return false;
		}
	}
	mutex.Lock();
	dataToProcess = NullPtr;
	mutex.Unlock();

#ifdef LINUX_THREAD_DEBUG
	printf("Thread[%x]::KernelProcess: job done, checking if to terminate\n", threadId);
#endif

	if (threadType == THREAD_TYPE_INTERVAL_DRIVEN)
	{
		printf("Since thread [%x] is INTERVAL_DRIVEN, sleep for %d and repeat job...\n", threadId, idlePeriod);
		Sleep(idlePeriod);
		jobEvent.Set();
	}
	else if (terminateWhenDone)
		Terminate();

	return true;
}

//===========================================================================//

void CThread::WaitForResume()
{
	while (true)
	{
		resumedEvent.Wait();
//        pthread_cond_wait(&suspendCondition, &suspendMutex);
		if (!isSuspended)
			return;
	}
}

//===========================================================================//

integer CThread::GetPendingTasksCount()
{
	integer chEventsWaiting;
	mutex.Lock();
	chEventsWaiting = taskQue.GetSize();
	mutex.Unlock();

	return chEventsWaiting;
}

//===========================================================================//

bool CThread::Join(TimeInMsType milliseconds)
{
#ifdef LINUX_THREAD_DEBUG
	printf("Thread[%x]::Join() called\n", threadId);
#endif
	if (!isTerminating)
	{
#ifdef LINUX_THREAD_DEBUG
		printf("Thread %x is not terminating, so Join() can wait for job to complete\n", threadId);
#endif
		if (finishedEvent.Wait(milliseconds))
		{
			finishedEvent.Reset();
			return true;
		}
		finishedEvent.Reset();
	}
	return false;
}

//===========================================================================//

CThread::CThread(THREAD_TYPE type, bool terminateAfterExecution)
	: terminateWhenDone(terminateAfterExecution)
	, isTerminating(false)
	, isSuspended(true)
	, threadHandle(NullPtr)
	, threadId(0L)
	, idlePeriod(100)
	, dataToProcess(NullPtr)
	, threadType(type)
	, stackSize(DEFAULT_STACK_SIZE)
	, hasStarted(false)
{

	objectCondition = NO_ERRORS;

	if (!mutex.isCreated)
	{
		perror("mutex creation failed");
		objectCondition |= MUTEX_CREATION;
		return;
	}

	if (!jobEvent.isCreated)
	{
		perror("event creation failed");
		objectCondition |= EVENT_CREATION;
		return;
	}

	//Start(isSuspended);
}

//===========================================================================//

bool CThread::IsEmpty()
{
	integer taskCount;
	mutex.Lock();
	taskCount = taskQue.GetSize();
	mutex.Unlock();

	return (taskCount == 0);
}

//===========================================================================//

bool CThread::Push(void * taskPointer)
{
	if (!taskPointer) return false;

	mutex.Lock();
	if (threadType == THREAD_TYPE_INTERVAL_DRIVEN)
		taskQue.Clear();

	taskQue.Add((CTask *)taskPointer);

	mutex.Unlock();

	return true;
}

//===========================================================================//

bool CThread::Pop()
{

	mutex.Lock();

	if (taskQue.GetSize() <= 0)
	{
		mutex.Unlock();
		return false;
	}

	dataToProcess = taskQue.Get(0);

	if (threadType == THREAD_TYPE_EVENT_DRIVEN)
		taskQue.Remove((integer)0);

	mutex.Unlock();
	return true;
}

//===========================================================================//

void CThread::Suspend()
{
#ifdef LINUX_THREAD_DEBUG
	printf("Suspending thread %x...\n", threadId);
#endif

	if (isSuspended)
		return;

	mutex.Lock();
	isSuspended = true;
	resumedEvent.Reset();
	mutex.Unlock();

	//pthread_mutex_unlock(&suspendMutex);

	/*    mutex.Lock();
	    isSuspended = true;
	    SuspendThread(threadHandle);
	    mutex.Unlock();
	*/
#ifdef LINUX_THREAD_DEBUG
	printf("Thread %x suspended...\n", threadId);
#endif
}

//===========================================================================//

void CThread::Resume()
{
#ifdef LINUX_THREAD_DEBUG
	printf("Resuming thread %x...\n", threadId);
#endif

	if (!hasStarted)
	{
		hasStarted = true;
		isSuspended=false;
		Start(true);
		return;
	}

	if (!isSuspended)
		return;

	//pthread_mutex_lock(&suspendMutex);
	mutex.Lock();
	isSuspended = false;
	resumedEvent.Set();
//    pthread_cond_signal(&suspendCondition);
	mutex.Unlock();
	//pthread_mutex_unlock(&suspendMutex);

	/*mutex.Lock();
	isSuspended = false;
	ResumeThread(threadHandle);
	mutex.Unlock();
	*/
#ifdef LINUX_THREAD_DEBUG
	printf("Thread %x resumed...\n", threadId);
#endif
}

//===========================================================================//

void CThread::SetIdle(TimeInMsType dwIdle)
{
	mutex.Lock();
	idlePeriod = dwIdle;
	mutex.Unlock();
}

//===========================================================================//

bool CThread::Start(bool suspended)
{
	mutex.Lock();
	if (threadHandle)
	{
		mutex.Unlock();
		return true;
	}

	mutex.Unlock();

	if (objectCondition & THREAD_CREATION)
		objectCondition = objectCondition ^ THREAD_CREATION;

	pthread_attr_t attr;
	pthread_attr_init(&attr);

	if (stackSize != 0)
		pthread_attr_setstacksize(&attr, stackSize);

	int error = pthread_create(&threadHandle, &attr, _THKERNEL, (void *)this);

	if (error != 0)
	{
		objectCondition |= THREAD_CREATION;
		//m_state = ThreadStateFault;

		switch (error)
		{

//        case EINVAL:
//            THREAD_EXCEPTION_FULL("CThread::Start: attr in an invalid thread attributes object\n", this);
//            break;
//        case EAGAIN:
//            THREAD_EXCEPTION_FULL("CThread::Start: the necessary resources to create a thread are not available.\n", this);
//            break;
//        case EPERM:
//            THREAD_EXCEPTION_FULL("CThread::Start: the caller does not have the privileges to create the thread with the specified attr object.\n", this);
//            break;
		default:
			THREAD_EXCEPTION_FULL("An unknown error was encountered attempting to create the requested thread.\n", this);
			break;
		}

		return false;
	}

	return true;
}

//===========================================================================//

CThread::~CThread(void)
{
#ifdef LINUX_THREAD_DEBUG
	printf("Destroying thread %x...\n", threadId);
#endif

	Terminate();

#ifdef LINUX_THREAD_DEBUG
	printf("Thread %x destroyed...\n", threadId);
#endif
}

//===========================================================================//

bool CThread::IsAlive()
{
#ifdef LINUX_THREAD_DEBUG
	printf("Checking %x to be alive: ", threadId);
#endif
	bool alive = !isTerminating;
#ifdef LINUX_THREAD_DEBUG
	printf("%s", alive ? "true" : "false");
#endif
	return alive;
}

//===========================================================================//

void CThread::Terminate()
{
	if (!isTerminating)
	{
#ifdef LINUX_THREAD_DEBUG
		printf("Trying to terminate thread %x...\n", threadId);
#endif

		isTerminating = true;

		if (isSuspended)
		{
			Resume();
			void * threadExitStatus;
#ifdef LINUX_THREAD_DEBUG
			printf("Trying to join thread %x before termination...\n", threadId);
#endif
			pthread_join(threadHandle, &threadExitStatus);
//          WaitForSingleObject(threadHandle, INFINITE);
		}

//        pthread_cond_destroy(&suspendCondition);
//        pthread_mutex_destroy(&suspendMutex);
	}
//    CloseHandle(threadHandle);
}

//===========================================================================//

} //namespace

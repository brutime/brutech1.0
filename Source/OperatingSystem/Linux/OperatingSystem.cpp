//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../OperatingSystem.h"

#include <X11/Xlib.h>
#include <GL/gl.h>
#include "Display.h"
#include "../Exceptions/OperatingSystemException.h"

namespace Bru
{

//===========================================================================//

OperatingSystem::OperatingSystem()
{
}

//===========================================================================//

OperatingSystem::~OperatingSystem()
{
}

//===========================================================================//

void OperatingSystem::Sleep(integer milliseconds)
{
	NOT_IMPLEMENTED_EXCEPTION("Not implemented");
}

//===========================================================================//

void OperatingSystem::SetPixelFormat(const device_context deviceContext, const integer colorBufferDepth,
                                     const integer depthBufferDepth)
{
	//static int snglBuf[] = {GLX_RGBA, GLX_DEPTH_SIZE, 16, None};
	static int dblBuf[]  = {GLX_RGBA, depthBufferDepth, colorBufferDepth, GLX_DOUBLEBUFFER, None};
	XVisualInfo * vi;

	/*PIXELFORMATDESCRIPTOR pixelFormatDescriptor;

	pixelFormatDescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixelFormatDescriptor.nVersion = 1;
	pixelFormatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits = static_cast<byte>(colorBufferDepth);
	pixelFormatDescriptor.cRedBits = 0;
	pixelFormatDescriptor.cRedShift = 0;
	pixelFormatDescriptor.cGreenBits = 0;
	pixelFormatDescriptor.cGreenShift = 0;
	pixelFormatDescriptor.cBlueBits = 0;
	pixelFormatDescriptor.cBlueShift = 0;
	pixelFormatDescriptor.cAlphaBits = 0;
	pixelFormatDescriptor.cAlphaShift = 0;
	pixelFormatDescriptor.cAccumBits = 0;
	pixelFormatDescriptor.cAccumRedBits = 0;
	pixelFormatDescriptor.cAccumGreenBits = 0;
	pixelFormatDescriptor.cAccumBlueBits = 0;
	pixelFormatDescriptor.cAccumAlphaBits = 0;
	pixelFormatDescriptor.cDepthBits = static_cast<byte>(depthBufferDepth);
	pixelFormatDescriptor.cStencilBits = 0;
	pixelFormatDescriptor.cAuxBuffers = 0;
	pixelFormatDescriptor.iLayerType = PFD_MAIN_PLANE;
	pixelFormatDescriptor.bReserved = 0;
	pixelFormatDescriptor.dwLayerMask = 0;
	pixelFormatDescriptor.dwVisibleMask = 0;
	pixelFormatDescriptor.dwDamageMask = 0;
	*/

	/* integer pixelFormat = ChoosePixelFormat(deviceContext, &pixelFormatDescriptor); */

	vi = glXChooseVisual(deviceContext, DefaultScreen(deviceContext), dblBuf);
	if (vi == NULL)
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't set the choosed pixel format");
		/*vi = glXChooseVisual(dpy, DefaultScreen(dpy), snglBuf);
		if (vi == NULL) fatalError("no RGB visual with depth buffer");
		doubleBuffer = GL_FALSE;
		*/
	}

	/*if (pixelFormat == 0)
	{

	}

	if (!::SetPixelFormat(deviceContext, pixelFormat, &pixelFormatDescriptor))
	{
	    OPERATING_SYSTEM_EXCEPTION("Couldn't set the choosed pixel format");
	}*/
}

//===========================================================================//

device_context OperatingSystem::CreateDeviceContext(const surface_handle windowHandle)
{
	//???вообще хз, что тут и в DeleteDeviceContext должно быть
	device_context deviceContext = BruDisplay->GetHandle();
	if (deviceContext == 0)
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't create a device context");
	}

	return deviceContext;
}

//===========================================================================//

void OperatingSystem::DeleteDeviceContext(const surface_handle windowHandle, const device_context deviceContext)
{
	//if (!ReleaseDC(windowHandle, deviceContext))
	if (!glXMakeCurrent(BruDisplay->GetHandle(), windowHandle, NullPtr))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't delete a device context");
	}
}

//===========================================================================//

rendering_context OperatingSystem::CreateRenderingContext(const device_context deviceContext)
{
	rendering_context renderingContext;
	renderingContext = wglCreateContext(deviceContext);

	if (renderingContext == 0)
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't create a rendering context");
	}

	return renderingContext;
}

//===========================================================================//

void OperatingSystem::DestroyRenderingContext(const rendering_context renderingContext)
{
	if (!wglDeleteContext(renderingContext))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't destroy a rendering context");
	}
}

//===========================================================================//

void OperatingSystem::SetRenderingContextAsCurrent(const device_context deviceContext,
        const rendering_context renderingContext)
{
	if (!wglMakeCurrent(deviceContext, renderingContext))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't make the rendering context pararmeter as the current rendering context");
	}
}

//===========================================================================//

void OperatingSystem::ClearCurrentRenderingContext()
{
	if (!wglMakeCurrent(NullPtr, NullPtr))
	{
		OPERATING_SYSTEM_EXCEPTION("Couldn't clear current rendering context");
	}
}

//===========================================================================//

void OperatingSystem::SwapBuffers(const device_context deviceContext)
{
	::SwapBuffers(deviceContext);
}

//===========================================================================//

string OperatingSystem::GetCurrentLocalDateAndTime()
{
	NOT_IMPLEMENTED_EXCEPTION("Not implemented");
}

//===========================================================================//

string OperatingSystem::GetCurrentLocalDate()
{
	NOT_IMPLEMENTED_EXCEPTION("Not implemented");
}

//===========================================================================//

string OperatingSystem::GetCurrentLocalTime()
{
	NOT_IMPLEMENTED_EXCEPTION("Not implemented");
}

//===========================================================================//

} //namespace


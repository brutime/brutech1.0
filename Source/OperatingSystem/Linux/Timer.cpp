//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Timer.h"
#include <time.h>

namespace Bru
{

//===========================================================================//

Timer::Timer() : ITimer(),
	startTime(0),
	frequency(0)
{
	startTime=clock();
}

//===========================================================================//

Timer::~Timer()
{
}

//===========================================================================//

void Timer::Reset()
{
	startTime = GetTime();
}

//===========================================================================//

float Timer::GetSeconds() const
{
	long long const elapsedTime = GetTime() - startTime;
	return (float(elapsedTime) / frequency);
}

//===========================================================================//

int64 Timer::GetTicks() const
{
	return GetTime();
}

//===========================================================================//

long long Timer::GetTime() const
{
	clock_t t=clock();
	return curTime.QuadPart;
}

//===========================================================================//

}


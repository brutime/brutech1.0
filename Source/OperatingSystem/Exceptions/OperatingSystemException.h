//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - неисправимые ошибки при вызове методов
//    операционной системы
//

#pragma once
#ifndef OPERATING_SYSTEM_EXCEPTION_H
#define OPERATING_SYSTEM_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class OperatingSystemException : public Exception
{
public:
	OperatingSystemException(const string & details, const string & fileName, const string & methodName,
	                         integer fileLine);
	virtual ~OperatingSystemException();
};

//===========================================================================//

#define OPERATING_SYSTEM_EXCEPTION(details) \
    throw OperatingSystemException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // OPERATING_SYSTEM_EXCEPTION_H

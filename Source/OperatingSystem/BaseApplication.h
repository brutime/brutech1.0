//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс приложения. Должен заниматься инициализацией подсистем, загружать
//    необходимые ресурсы. Содержит указатель на точку входа в главный цикл обработки сообщений.
//

#pragma once
#ifndef BASE_APPLICATION_H
#define BASE_APPLICATION_H

#include "../Common/Common.h"
#include "InputSystem/InputSystem.h"

//#define DEBUG_WINDOW

namespace Bru
{

//===========================================================================//

class BaseApplication
{
public:
	virtual ~BaseApplication();

	virtual void Initialize() = 0;
	virtual void MainLoop() = 0;
	virtual void FreeResources() = 0;

	void ApplicationMain();
	void Quit();

protected:
	BaseApplication();

private:
	void RegisterCommands();
	void UnregisterCommands();

	bool _quit;

	BaseApplication(const BaseApplication &) = delete;
	BaseApplication & operator =(const BaseApplication &) = delete;
};

//===========================================================================//

extern SharedPtr<BaseApplication> Application;

//===========================================================================//

}// namespace Bru

#endif // APPLICATION_H

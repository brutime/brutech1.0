//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ��� ���������� �������
//

#pragma once
#ifndef FILE_DIALOG_MODE_H
#define FILE_DIALOG_MODE_H

namespace Bru
{

//===========================================================================//

enum class FileDialogMode
{
    Open,
    Save
};

//===========================================================================//

}

#endif // FILE_DIALOG_MODE_H

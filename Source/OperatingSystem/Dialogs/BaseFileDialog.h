//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Обертка для диалога открытия файла
//

#pragma once
#ifndef BASE_FILE_DIALOG_H
#define BASE_FILE_DIALOG_H

#include "../../Common/Common.h"
#include "IDialog.h"
#include "Enums/FileDialogMode.h"

namespace Bru
{

//===========================================================================//

class BaseFileDialog : public IDialog
{
public:
	virtual ~BaseFileDialog();

	virtual DialogResult Show();

	void SetTitle(const string & title)
	{
		_title = title;
	}
	const string & GetTitle() const
	{
		return _title;
	}

	void SetInitialDirectory(const string & initialDirectory)
	{
		_initialDirectory = initialDirectory;
	}
	const string & GetInitialDirectory() const
	{
		return _initialDirectory;
	}

	void AddFilterItem(const string & itemTitle, const List<string> & itemExtensions);
	void ClearFilter();
	void SelectFilterItem(integer filterItemIndex);

	string GetPathToFile() const
	{
		return _pathToFile;
	}

protected:
	BaseFileDialog(const string & title, FileDialogMode dialogMode);

private:
	string _title;
	string _initialDirectory;
	List<KeyValuePair<string, List<string>>> _filterItems;
	integer _filterItemIndex;
	string _pathToFile;
	FileDialogMode _dialogMode;

	BaseFileDialog(const BaseFileDialog &) = delete;
	BaseFileDialog & operator =(const BaseFileDialog &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // BASE_FILE_DIALOG_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../BaseFileDialog.h"

#include <windows.h>

#include "../../../Utils/Console/Console.h"
#include "../../Surface.h"

namespace Bru
{

//===========================================================================//

BaseFileDialog::BaseFileDialog(const string & title, FileDialogMode dialogMode) :
	_title(title),
	_initialDirectory(),
	_filterItems(),
	_filterItemIndex(0),
	_pathToFile(),
	_dialogMode(dialogMode)
{
}

//===========================================================================//

BaseFileDialog::~BaseFileDialog()
{
}

//===========================================================================//

DialogResult BaseFileDialog::Show()
{
	//Позже заменим ОС диалоги на свои, так что можно лячкать :)
	ushort pathToFileChars[MAX_PATH];
	pathToFileChars[0] = NullTerminator;

	ushort * filterChars = NullPtr;

	OPENFILENAMEW fileNameData;
	Memory::FillWithNull(&fileNameData, sizeof(fileNameData));

	fileNameData.lStructSize = sizeof(fileNameData);
	fileNameData.hwndOwner = NULL;
	fileNameData.hInstance = NULL;

	utf16_string utf16Title = _title.ToUTF16();
	fileNameData.lpstrTitle = reinterpret_cast<const wchar_t *>(utf16Title.ToChars());

	utf16_string utf16InitialDirectory = _initialDirectory.ToUTF16();
	fileNameData.lpstrInitialDir = reinterpret_cast<const wchar_t *>(utf16InitialDirectory.ToChars());

	if (!_filterItems.IsEmpty())
	{
		string filterString;

		for (auto & filterItemPair : _filterItems)
		{
			string visibleExtensions;
			for (const string & extension : filterItemPair.GetValue())
			{
				visibleExtensions += string::Format("*.{0}, ", extension);
			}
			visibleExtensions = visibleExtensions.Remove(visibleExtensions.GetLength() - 2, 2);

			string usingExtensions;
			for (const string & extension : filterItemPair.GetValue())
			{
				usingExtensions += string::Format("*.{0};", extension);
			}
			usingExtensions = usingExtensions.Remove(usingExtensions.GetLength() - 1, 1);

			filterString += string::Format("{0} ({1})|{2}|", filterItemPair.GetKey(), visibleExtensions,
			                               usingExtensions);
		}

		utf16_string utf16FilterString = filterString.ToUTF16();

		filterChars = new ushort[utf16FilterString.GetLengthInWords() + 1];
		Memory::Copy(filterChars, utf16FilterString.ToChars(), utf16FilterString.GetLengthInWords() * sizeof(wchar_t));
		filterChars[utf16FilterString.GetLengthInWords()] = NullTerminator;

		for (integer i = 0; i < utf16FilterString.GetLengthInWords(); i++)
		{
			if (filterChars[i] == '|')
			{
				filterChars[i] = NullTerminator;
			}
		}

		fileNameData.lpstrFilter = reinterpret_cast<const wchar_t *>(filterChars);
		fileNameData.nFilterIndex = _filterItemIndex + 1;
	}

	fileNameData.lpstrFile = reinterpret_cast<wchar_t *>(pathToFileChars);
	fileNameData.nMaxFile = MAX_PATH;

	fileNameData.Flags = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY;

	if (_dialogMode == FileDialogMode::Save)
	{
		fileNameData.Flags |= OFN_OVERWRITEPROMPT;
	}

	BOOL result;
	if (_dialogMode == FileDialogMode::Open)
	{
		result = GetOpenFileNameW(&fileNameData);
	}
	else if (_dialogMode == FileDialogMode::Save)
	{
		result = GetSaveFileNameW(&fileNameData);
	}
	else
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("_dialogMode {0} not defined", static_cast<integer>(_dialogMode)));
	}

	DialogResult dialogResult;
	if (result == 0)
	{
		_pathToFile = string::Empty;
		dialogResult = DialogResult::Cancel;
	}
	else
	{
		_pathToFile = utf16_string(pathToFileChars);
		dialogResult = DialogResult::OK;
	}

	if (filterChars != NullPtr)
	{
		delete[] filterChars;
	}

	return dialogResult;
}

//===========================================================================//

void BaseFileDialog::AddFilterItem(const string & itemTitle, const List<string> & itemExtensions)
{
	if (itemExtensions.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("No extensions in item \"{0}\"", itemTitle));
	}

	for (const string & extension : itemExtensions)
	{
		if (extension.IsEmptyOrBlankCharsOnly())
		{
			INVALID_ARGUMENT_EXCEPTION(string::Format("Empty extension in item \"{0}\"", itemTitle));
		}
	}

	_filterItems.Add(KeyValuePair<string, List<string>>(itemTitle, itemExtensions));
}

//===========================================================================//

void BaseFileDialog::ClearFilter()
{
	_filterItems.Clear();
}

//===========================================================================//

void BaseFileDialog::SelectFilterItem(integer filterItemIndex)
{
#ifdef DEBUGGING
	if (filterItemIndex < 0 || filterItemIndex >= _filterItems.GetCount())
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(
		    string::Format("filterItemIndex out of bounds: {0} ([0..{1}])", filterItemIndex, _filterItems.GetCount() - 1));
	}
#endif // DEBUGGING
	_filterItemIndex = filterItemIndex;
}

//===========================================================================//

}// namespace Bru

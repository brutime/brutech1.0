//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс диалога
//

#pragma once
#ifndef I_DIALOG_H
#define I_DIALOG_H

#include "Enums/DialogResult.h"

namespace Bru
{

//===========================================================================//

class IDialog
{
public:
	virtual ~IDialog();

	virtual DialogResult Show() = 0;

protected:
	IDialog();

private:
	IDialog(const IDialog &) = delete;
	IDialog & operator =(const IDialog &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // OPEN_FILE_DIALOG_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Обертка для диалога открытия файла
//

#pragma once
#ifndef OPEN_FILE_DIALOG_H
#define OPEN_FILE_DIALOG_H

#include "BaseFileDialog.h"

namespace Bru
{

//===========================================================================//

class OpenFileDialog : public BaseFileDialog
{
public:
	static const string DefaultTitle;

	OpenFileDialog(const string & title = DefaultTitle);
	virtual ~OpenFileDialog();

private:
	OpenFileDialog(const OpenFileDialog &) = delete;
	OpenFileDialog & operator =(const OpenFileDialog &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // OPEN_FILE_DIALOG_H

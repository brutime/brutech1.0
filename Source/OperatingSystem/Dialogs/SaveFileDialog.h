//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Обертка для диалога сохранения файла
//

#pragma once
#ifndef SAVE_FILE_DIALOG_H
#define SAVE_FILE_DIALOG_H

#include "BaseFileDialog.h"

namespace Bru
{

//===========================================================================//

class SaveFileDialog : public BaseFileDialog
{
public:
	static const string DefaultTitle;

	SaveFileDialog(const string & title = DefaultTitle);
	virtual ~SaveFileDialog();

private:
	SaveFileDialog(const SaveFileDialog &) = delete;
	SaveFileDialog & operator =(const SaveFileDialog &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // SAVE_FILE_DIALOG_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Таймер
//

#pragma once
#ifndef TIMER_H
#define TIMER_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

class Timer
{
public:
	Timer();
	virtual ~Timer();

	void Reset();
	double GetSeconds() const;
	int64 GetTicks() const;
	int64 GetFrequency() const
	{
		return _frequency;
	}

private:
	int64 _startTime;
	int64 _frequency;

#if PLATFORM == WINDOWS

	void * _threadId;

#if ARCHITECTURE == X64
	unsigned int64 _processAffinityMask;
#else
	ulong _processAffinityMask;
#endif

#elif PLATFORM == MAC

#elif PLATFORM == LINUX

#else
#error Platform not defined (WINDOWS,MAC,LINUX)
#endif

	Timer(const Timer &) = delete;
	Timer & operator =(const Timer &) = delete;
};

//===========================================================================//

}//namespace Bru

#endif // I_TIMER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс задачи для обработки в параллельном потоке
//

#pragma once
#ifndef TASK_H
#define TASK_H

#include "ThreadTypes.h"
#include "Mutex.h"

#include <memory.h>

namespace Bru
{

//===========================================================================//

enum TASK_STATUS
{
    TaskStatusNotSubmitted = 0x1,
    TaskStatusWaitingOnQueue = 0x2,
    TaskStatusBeingProcessed = 0x4,
    TaskStatusCompleted = 0x8
};

//===========================================================================//

class Task
{
public:
	Task();
	virtual ~Task() {}

	void SetTaskStatus(TASK_STATUS newState);
	void SetId(ThreadType pid);
	bool Wait(int timeoutSeconds);
	void GetThreadId(ThreadType * pId) const;
	virtual bool TheTask() = 0;

	/*TASK_STATUS Status()
	{
	    TASK_STATUS state ;

	    m_mutex.Lock();
	    state = state;
	    m_mutex.Unlock();
	    return state;
	}*/

	Mutex mutex;

private:
	TASK_STATUS state;
	ThreadType threadID;
};

//===========================================================================//

} //namespace

#endif // TASK_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#include "ApplicationForPerformanceTests.h"

#include "../../ScriptSystem/ScriptSystem.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/Config/Config.h"
#include "../../Utils/PathHelper/PathHelper.h"
#include "../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"
#include "../../Utils/StringTable/StringTable.h"
#include "../../OperatingSystem/Display.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

const string ApplicationForPerformanceTests::WindowTitle = "Application for performance tests";

//===========================================================================//

ApplicationForPerformanceTests::ApplicationForPerformanceTests()
{
}

//===========================================================================//

ApplicationForPerformanceTests::~ApplicationForPerformanceTests()
{
}

//===========================================================================//

void ApplicationForPerformanceTests::Initialize()
{
	OSConsoleStream = new OSConsoleStreamSingleton();
	ExceptionRegistrator = new ExceptionRegistratorSingleton();
	Console = new ConsoleSingleton(MessagePriority::Debug);
	Config = new ConfigSingleton();
	ScriptSystem = new ScriptSystemSingleton();
	PerfomanceDataCollector = new PerfomanceDataCollectorSingleton(0.5f);
	ParametersFileManager = new ParametersFileManagerSingleton();
	StringTable = new StringTableSingleton(PathHelper::PathToData + "Text/StringTable");
	OperatingSystem = new OperatingSystemSingleton();
	Renderer = new RendererSingleton(400, 300, 400, 300, FullScreenMode::Off, VerticalSyncMode::Off, Colors::Black, ProjectionMode::Perspective, BatchingMode::On);
}

//===========================================================================//

void ApplicationForPerformanceTests::MainLoop()
{
	Sleep(0);
}

//===========================================================================//

void ApplicationForPerformanceTests::FreeResources()
{
	Renderer = NullPtr;
	OperatingSystem = NullPtr;
	StringTable = NullPtr;
	ParametersFileManager = NullPtr;
	PerfomanceDataCollector = NullPtr;
	ScriptSystem = NullPtr;
	Config = NullPtr;
	Console = NullPtr;
	ExceptionRegistrator = NullPtr;
	OSConsoleStream = NullPtr;
}

//===========================================================================//

}// namespace Bru

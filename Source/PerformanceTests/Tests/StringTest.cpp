//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StringTest.h"

#include <string>
#include <memory>

namespace Bru
{

//===========================================================================//

const integer StringTest::RepetitionCount = 100000;

//===========================================================================//

StringTest::StringTest() :
	IPerformanceTest(),
	accumulator(0)
{
}

//===========================================================================//

StringTest::~StringTest()
{
}

//===========================================================================//

void StringTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("String test\n");
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	ForEachTest(timer);
	FormatTest(timer);
	SubstringTest(timer);
	ReplaceTest(timer);
	InsertTest(timer);
	RemoveTest(timer);
	ContainsTest(timer);
	ContainsCountTest(timer);
	FirstIndexOfTest(timer);
	FirstIndexNumberOfTest(timer);
	LastIndexOfTest(timer);
	LastIndexNumberOfTest(timer);
	ToLowerTest(timer);
	ToUpperTest(timer);
	ReverseTest(timer);
	TrimLeftTest(timer);
	TrimRightTest(timer);
	TrimTest(timer);
	CleanUpTest(timer);
}

//===========================================================================//

void StringTest::ForEachTest(const SharedPtr<Timer> & timer)
{
	string resultString("B¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩ");

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0, count = resultString.GetLength(); j < count; j++)
		{
			accumulator += resultString[j].GetLength();
			accumulator += resultString[j].GetLength();
		}

		for (integer j = resultString.GetLength() - 1; j >= 0; j--)
		{
			accumulator += resultString[j].GetLength();
			accumulator += resultString[j].GetLength();
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  For each string test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::FormatTest(const SharedPtr<Timer> & timer)
{
	string s0 = "fucking";
	string s1 = "awesome";
	float f = 13.885939f;
	string sink;

	timer->Reset();
	float startTime = timer->GetSeconds();

	string s = "BruTime is {0} {1}, {2:.2} and {0} cool";

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += string::Format(s, s0, s1, f).GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Format string test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::SubstringTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "B¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩ";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.Substring(4, 15).GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Sub string test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::ReplaceTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫";
	string oldString = "♫♫";
	string newString = "♫||||♫";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.Replace(oldString, newString).GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Replace test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::InsertTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫♫";
	string insertString = "||||||";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.Insert(4, insertString).GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Insert test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::RemoveTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "CLUUUUUUUUUUUUUUUUUUUUUUUUUU!!!!!!!!!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.Remove(14, 15).GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Remove test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::ContainsTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "CLUUUUUUUUUUUUUUUUUUUUUUUUUU!!!!!!!!!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.Contains("U!!!!");
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Contains test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::ContainsCountTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "CLUUUUUUUUUUUUUUUUUUUUUUUUUU!!!!!!!!!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.ContainsCount("U");
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Contains count test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::FirstIndexOfTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "CLUUUUUUUUUUUUUUUUUUUUUUUUUU!!!!!!!!!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.FirstIndexOf("U!!!!!!!!!");
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  First index of test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::FirstIndexNumberOfTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "CLUUUUUUUUUUUUUUUUUUUUUUUUUU!!!!!!!!!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.FirstIndexNumberOf("UUU", 6);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  First index number of test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::LastIndexOfTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "CLUUUUUUUUUUUUUUUUUUUUUUUUUU!!!!!!!!!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.LastIndexOf("U!!!!!!!!!");
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Last index of test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::LastIndexNumberOfTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "CLUUUUUUUUUUUUUUUUUUUUUUUUUU!!!!!!!!!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.LastIndexNumberOf("UUU", 6);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Last index number of test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::ToLowerTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "ИВАН И JOHN, ИВАН И JOHN, ИВАН И JOHN!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.ToLower().GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  To lower test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::ToUpperTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "иван и john, иван и john, иван и john!";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.ToUpper().GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  To upper test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::ReverseTest(const SharedPtr<Timer> & timer)
{
	string sourceString = "B¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩ";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.Reverse().GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Reverse test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::TrimLeftTest(const SharedPtr<Timer> & timer)
{
	string sourceString = " \t\n\r \t\n\r B¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩ \t\n\r \t\n\r ";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.TrimLeft().GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Trim left test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::TrimRightTest(const SharedPtr<Timer> & timer)
{
	string sourceString = " \t\n\r \t\n\r B¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩ \t\n\r \t\n\r ";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.TrimRight().GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Trim right test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::TrimTest(const SharedPtr<Timer> & timer)
{
	string sourceString = " \t\n\r \t\n\r B¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩB¥𠀀௵rΩ \t\n\r \t\n\r ";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.Trim().GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Trim test time = %f\n", resultTime);
}

//===========================================================================//

void StringTest::CleanUpTest(const SharedPtr<Timer> & timer)
{
	string sourceString = " \t\n\r \t\n\r B¥𠀀௵ n\r rΩ   B¥𠀀௵  rΩB¥𠀀௵rΩ\t\n\r \t\n\rB¥𠀀௵rΩ \t\n\r \t\n\r ";

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		accumulator += sourceString.CleanUp().GetLength();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Clean up test time = %f\n", resultTime);
}

//===========================================================================//

}// namespace Bru


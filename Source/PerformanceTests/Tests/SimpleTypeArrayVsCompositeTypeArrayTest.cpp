//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SimpleTypeArrayVsCompositeTypeArrayTest.h"

#include "../../Common/Common.h"
#include "../../Math/Math.h"

//#include "../../UnitTest++/MockObjects/DummyCompoundObject.h"

namespace Bru
{

//===========================================================================//

const integer SimpleTypeArrayVsCompositeTypeArrayTest::RepetitionCount = 10000;

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("Simple type array vs composite type array test\n");
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	SimpleTypeArrayAddingTest(timer);
	CompositeTypeArrayAddingTest(timer);

	SimpleTypeArrayAddingOtherArrayTest(timer);
	CompositeTypeArrayAddingOtherArrayTest(timer);

	SimpleTypeArrayInsertingTest(timer);
	CompositeTypeArrayInsertingTest(timer);

	SimpleTypeArrayInsertingOtherArrayTest(timer);
	CompositeTypeArrayInsertingOtherArrayTest(timer);

	SimpleTypeArrayRemovingTest(timer);
	CompositeTypeArrayRemovingTest(timer);

	SimpleTypeArrayRemovingAtTest(timer);
	CompositeTypeArrayRemovingAtTest(timer);

	SimpleTypeArrayRemovingAtIndexCountTest(timer);
	CompositeTypeArrayRemovingAtIndexCountTest(timer);

	SimpleTypeArraySetTest(timer);
	CompositeTypeArraySetTest(timer);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArrayAddingTest(SharedPtr<Timer> timer)
{
	Array<integer>::SimpleType array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of adding elements to simple type array = %0.10f\n", resultTime);
	printf("%d\n", array[RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArrayAddingTest(SharedPtr<Timer> timer)
{
	Array<integer> array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of adding elements to composite type array = %0.10f\n", resultTime);
	printf("%d\n", array[RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArrayAddingOtherArrayTest(SharedPtr<Timer> timer)
{
	const integer ElementsCount = 100;
	Array<integer>::SimpleType sourceArray;
	for (integer i = 0; i < ElementsCount; i++)
	{
		sourceArray.Add(i);
	}

	Array<integer>::SimpleType array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(sourceArray);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of adding array to simple type array = %0.10f\n", resultTime);
	printf("%d\n", array[ElementsCount * RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArrayAddingOtherArrayTest(SharedPtr<Timer> timer)
{
	const integer ElementsCount = 100;
	Array<integer> sourceArray;
	for (integer i = 0; i < ElementsCount; i++)
	{
		sourceArray.Add(i);
	}

	Array<integer> array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(sourceArray);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of adding array to composite type array = %0.10f\n", resultTime);
	printf("%d\n", array[ElementsCount * RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArrayInsertingTest(SharedPtr<Timer> timer)
{
	Array<integer>::SimpleType array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Insert(0, i);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of inserting elemets to simple type array = %0.10f\n", resultTime);
	printf("%d\n", array[RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArrayInsertingTest(SharedPtr<Timer> timer)
{
	Array<integer> array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Insert(0, i);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of inserting elemets to composite type array = %0.10f\n", resultTime);
	printf("%d\n", array[RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArrayInsertingOtherArrayTest(SharedPtr<Timer> timer)
{
	const integer ElementsCount = 10;
	Array<integer>::SimpleType sourceArray;
	for (integer i = 0; i < ElementsCount; i++)
	{
		sourceArray.Add(i);
	}

	Array<integer>::SimpleType array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Insert(0, sourceArray);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of inserting array to simple type array = %0.10f\n", resultTime);
	printf("%d\n", array[ElementsCount * RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArrayInsertingOtherArrayTest(SharedPtr<Timer> timer)
{
	const integer ElementsCount = 10;
	Array<integer> sourceArray;
	for (integer i = 0; i < ElementsCount; i++)
	{
		sourceArray.Add(i);
	}

	Array<integer> array;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Insert(0, sourceArray);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of inserting array to composite type array = %0.10f\n", resultTime);
	printf("%d\n", array[ElementsCount * RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArrayRemovingTest(SharedPtr<Timer> timer)
{
	Array<integer>::SimpleType array;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Remove(i);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of removing elemets from simple type array = %0.10f\n", resultTime);

	array.Add(1);
	printf("%d\n", array[0]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArrayRemovingTest(SharedPtr<Timer> timer)
{
	Array<integer> array;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Remove(i);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of removing elemets from composite type array = %0.10f\n", resultTime);

	array.Add(1);
	printf("%d\n", array[0]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArrayRemovingAtTest(SharedPtr<Timer> timer)
{
	Array<integer>::SimpleType array;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.RemoveAt(0);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of removing at elemets from simple type array = %0.10f\n", resultTime);

	array.Add(1);
	printf("%d\n", array[0]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArrayRemovingAtTest(SharedPtr<Timer> timer)
{
	Array<integer> array;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.RemoveAt(0);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of removing at elemets from composite type array = %0.10f\n", resultTime);

	array.Add(1);
	printf("%d\n", array[0]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArrayRemovingAtIndexCountTest(SharedPtr<Timer> timer)
{
	Array<integer>::SimpleType array;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount / 10; i++)
	{
		array.RemoveAt(0, 10);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of batch removing at elemets from simple type array = %0.10f\n", resultTime);

	array.Add(1);
	printf("%d\n", array[0]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArrayRemovingAtIndexCountTest(SharedPtr<Timer> timer)
{
	Array<integer> array;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.Add(i);
	}

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount / 10; i++)
	{
		array.RemoveAt(0, 10);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of batch removing at elemets from composite type array = %0.10f\n", resultTime);

	array.Add(1);
	printf("%d\n", array[0]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::SimpleTypeArraySetTest(SharedPtr<Timer> timer)
{
	Array<integer>::SimpleType sourceArray;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		sourceArray.Add(i);
	}

	Array<integer>::SimpleType array(sourceArray);

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.SetArray(0, sourceArray);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of set elemets in simple type array = %0.10f\n", resultTime);

	printf("%d\n", array[RepetitionCount - 1]);
}

//===========================================================================//

void SimpleTypeArrayVsCompositeTypeArrayTest::CompositeTypeArraySetTest(SharedPtr<Timer> timer)
{
	Array<integer> sourceArray;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		sourceArray.Add(i);
	}

	Array<integer> array(sourceArray);

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		array.SetArray(0, sourceArray);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of set elemets in composite type array = %0.10f\n", resultTime);

	printf("%d\n", array[RepetitionCount - 1]);
}

//===========================================================================//

}// namespace Bru

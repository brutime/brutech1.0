//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест показывает разницу между нашим Array и классическим массивом
//

#pragma once
#ifndef POINTERS_TEST_H
#define POINTERS_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class ContainerObject
{
public:
	ContainerObject();
	virtual ~ContainerObject();

	string * GetSimplePtr()
	{
		return simplePtr;
	}
	const SharedPtr<string> & GetSharedPtr()
	{
		return sharedPtr;
	}
	const WeakPtr<string> & GetWeakPtr()
	{
		return weakPtr;
	}

private:
	string * simplePtr;
	SharedPtr<string> sharedPtr;
	WeakPtr<string> weakPtr;

	ContainerObject(const ContainerObject &);
	ContainerObject & operator =(const ContainerObject &);
};

//===========================================================================//

class PointersTest : public IPerformanceTest
{
public:
	PointersTest() :
		IPerformanceTest()
	{
	}
	virtual ~PointersTest()
	{
	}

	void Start();

private:
	static const integer RepetitionCount;

	void SimplePointersTest(SharedPtr<Timer> timer);
	void SharedPointersTest(SharedPtr<Timer> timer);
	void WeakPointersTest(SharedPtr<Timer> timer);
	void WeakLockPointersTest(SharedPtr<Timer> timer);

	PointersTest(const PointersTest &);
	void operator =(const PointersTest &);
};

//===========================================================================//

}// namespace Bru

#endif // POINTERS_TEST_H

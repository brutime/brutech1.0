//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс теста производительности
//

#pragma once
#ifndef I_PERFORMANCE_TEST_H
#define I_PERFORMANCE_TEST_H

#include "../../Common/Common.h"

#include <stdio.h>

namespace Bru
{

//===========================================================================//

class IPerformanceTest
{
public:
	virtual ~IPerformanceTest();

	virtual void Start() = 0;

protected:
	IPerformanceTest();

	IPerformanceTest(const IPerformanceTest &) = delete;
	IPerformanceTest & operator =(const IPerformanceTest &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // I_PERFORMANCE_TEST_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест для оптимизации форматирования строки
//

#pragma once
#ifndef CONVERTER_TEST_H
#define CONVERTER_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class ConverterTest : public IPerformanceTest
{
public:
	ConverterTest() :
		IPerformanceTest()
	{
	}
	virtual ~ConverterTest()
	{
	}

	void Start();

	void IntegerWithOffsetTest(const SharedPtr<Timer> & timer);
	void IntegerWithoutOffsetTest(const SharedPtr<Timer> & timer);
	void FloatWithOffsetTest(const SharedPtr<Timer> & timer);
	void FloatWithoutOffsetTest(const SharedPtr<Timer> & timer);

private:
	static const integer RepetitionCount;

	ConverterTest(const ConverterTest &);
	void operator =(const ConverterTest &);
};

//===========================================================================//

}// namespace Bru

#endif // CONVERTER_TEST_H

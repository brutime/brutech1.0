//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест показывает разницу между прямым вызовом и с помощью Get\Set
//              Если в Debug разница значимая, то в Release ее просто нет.
//              Следовательно, везде можно смело писать Get и Set
//

#pragma once
#ifndef SETTERS_GETTERS_TEST_H
#define SETTERS_GETTERS_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class SettersGettersTest : public IPerformanceTest
{
public:
	SettersGettersTest() :
		IPerformanceTest(),
		value(0)
	{
	}

	virtual ~SettersGettersTest()
	{
	}

	void Start();

	void SetValue(const integer someValue)
	{
		value = someValue;
	}
	integer GetValue()
	{
		return value;
	}

private:
	static const integer RepetitionCount;

	integer value;

	void StartSetWithoutSetterTest(const SharedPtr<Timer> & timer);
	void StartSetWithSetterTest(const SharedPtr<Timer> & timer);

	void StartGetWithoutSetterTest(const SharedPtr<Timer> & timer);
	void StartGetWithSetterTest(const SharedPtr<Timer> & timer);

	SettersGettersTest(const SettersGettersTest &);
	void operator =(const SettersGettersTest &);
};

//===========================================================================//

}// namespace Bru

#endif // SETTERS_GETTERS_TEST_H

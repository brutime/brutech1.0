//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ImageFormatsTest.h"

#include "../../Graphics/Graphics.h"

namespace Bru
{

//===========================================================================//

const integer ImageFormatsTest::RepetitionCount = 100;

//===========================================================================//

ImageFormatsTest::ImageFormatsTest() :
	IPerformanceTest()
{
}

//===========================================================================//

ImageFormatsTest::~ImageFormatsTest()
{
}

//===========================================================================//

void ImageFormatsTest::Start()
{
	SharedPtr<Timer> timer = new Timer();
	
	OSConsoleStream->WriteLine("Image formats test");
	OSConsoleStream->WriteLine(string::Format("  Repetition count: {0}", RepetitionCount));	
	
	OSConsoleStream->WriteLine("  Image without alpha channel:");
	OSConsoleStream->WriteEndOfLine();	

	LoadingJPG(timer, "Wood");
	LoadingPNG(timer, "WoodPNG");
	LoadingTGA(timer, "WoodTGA");
	
	OSConsoleStream->WriteLine("  Image with alpha channel:");
	OSConsoleStream->WriteEndOfLine();	
	
	LoadingPNG(timer, "AnimatedUnit");
	LoadingTGA(timer, "AnimatedUnitTGA");
}

//===========================================================================//

void ImageFormatsTest::LoadingJPG(SharedPtr<Timer> timer, const string & pathToFile)
{
	OSConsoleStream->WriteLine("    Loading JPG...");
	timer->Reset();
	float startTime = timer->GetSeconds();
	
	for (integer i = 0; i < RepetitionCount; ++i)
	{
		Image image(pathToFile);
	}

	float resultTime = timer->GetSeconds() - startTime;
	OSConsoleStream->WriteLine(string::Format("    Loading JPG summary time: {0:0.4}", resultTime));
	OSConsoleStream->WriteLine(string::Format("    Loading JPG average time: {0:0.4}", resultTime / RepetitionCount));
	//Чтобы Release не обрезал
	OSConsoleStream->WriteEndOfLine();	
}

//===========================================================================//

void ImageFormatsTest::LoadingPNG(SharedPtr<Timer> timer, const string & pathToFile)
{
	OSConsoleStream->WriteLine("    Loading PNG...");
	timer->Reset();
	float startTime = timer->GetSeconds();
	
	for (integer i = 0; i < RepetitionCount; ++i)
	{
		Image image(pathToFile);
	}

	float resultTime = timer->GetSeconds() - startTime;
	OSConsoleStream->WriteLine(string::Format("    Loading PNG summary time: {0:0.4}", resultTime));
	OSConsoleStream->WriteLine(string::Format("    Loading PNG average time: {0:0.4}", resultTime / RepetitionCount));
	//Чтобы Release не обрезал
	OSConsoleStream->WriteEndOfLine();
}

//===========================================================================//

void ImageFormatsTest::LoadingTGA(SharedPtr<Timer> timer, const string & pathToFile)
{
	OSConsoleStream->WriteLine("    Loading TGA...");
	timer->Reset();
	float startTime = timer->GetSeconds();
	
	for (integer i = 0; i < RepetitionCount; ++i)
	{
		Image image(pathToFile);
	}	

	float resultTime = timer->GetSeconds() - startTime;
	OSConsoleStream->WriteLine(string::Format("    Loading TGA summary time: {0:0.4}", resultTime));
	OSConsoleStream->WriteLine(string::Format("    Loading TGA average time: {0:0.4}", resultTime / RepetitionCount));
	//Чтобы Release не обрезал
	OSConsoleStream->WriteEndOfLine();
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест показывает разницу между массивами (классический, STD, Bru)
//

#pragma once
#ifndef ARRAY_TEST_H
#define ARRAY_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class ArrayTest : public IPerformanceTest
{
public:
	ArrayTest() :
		IPerformanceTest()
	{
	}
	virtual ~ArrayTest()
	{
	}

	void Start();

private:
	static const integer VerticesCount;
	static const integer RepetitionCount;

	void BruForceConvertingTest(SharedPtr<Timer> timer);
	void BruDirectConvertingTest(SharedPtr<Timer> timer);

	void ClassicOrderedReadIndexing(SharedPtr<Timer> timer);
	void STDOrderedReadIndexing(SharedPtr<Timer> timer);
	void BruOrderedReadIndexing(SharedPtr<Timer> timer);

	void ClassicOrderedWriteIndexing(SharedPtr<Timer> timer);
	void STDOrderedWriteIndexing(SharedPtr<Timer> timer);
	void BruOrderedWriteIndexing(SharedPtr<Timer> timer);

	void ClassicRandomReadIndexing(SharedPtr<Timer> timer);
	void STDRandomReadIndexing(SharedPtr<Timer> timer);
	void BruRandomReadIndexing(SharedPtr<Timer> timer);

	void ClassicRandomWriteIndexing(SharedPtr<Timer> timer);
	void STDRandomWriteIndexing(SharedPtr<Timer> timer);
	void BruRandomWriteIndexing(SharedPtr<Timer> timer);

	ArrayTest(const ArrayTest & otherArrayTest);
	void operator =(const ArrayTest & otherArrayTest);
};

//===========================================================================//

}// namespace Bru

#endif // ARRAY_TEST_H

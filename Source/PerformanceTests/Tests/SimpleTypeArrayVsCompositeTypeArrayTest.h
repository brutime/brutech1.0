//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест показывает разницу между массивом простых типов и составных типов
//

#pragma once
#ifndef SIMPLE_TYPE_ARRAY_VS_COMPOSITE_TYPE_ARRAY_TEST_H
#define SIMPLE_TYPE_ARRAY_VS_COMPOSITE_TYPE_ARRAY_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class SimpleTypeArrayVsCompositeTypeArrayTest : public IPerformanceTest
{
public:
	SimpleTypeArrayVsCompositeTypeArrayTest() :
		IPerformanceTest()
	{
	}
	virtual ~SimpleTypeArrayVsCompositeTypeArrayTest()
	{
	}

	void Start();

private:
	static const integer RepetitionCount;

	void SimpleTypeArrayAddingTest(SharedPtr<Timer> timer);
	void CompositeTypeArrayAddingTest(SharedPtr<Timer> timer);

	void SimpleTypeArrayAddingOtherArrayTest(SharedPtr<Timer> timer);
	void CompositeTypeArrayAddingOtherArrayTest(SharedPtr<Timer> timer);

	void SimpleTypeArrayInsertingTest(SharedPtr<Timer> timer);
	void CompositeTypeArrayInsertingTest(SharedPtr<Timer> timer);

	void SimpleTypeArrayInsertingOtherArrayTest(SharedPtr<Timer> timer);
	void CompositeTypeArrayInsertingOtherArrayTest(SharedPtr<Timer> timer);

	void SimpleTypeArrayRemovingTest(SharedPtr<Timer> timer);
	void CompositeTypeArrayRemovingTest(SharedPtr<Timer> timer);

	void SimpleTypeArrayRemovingAtTest(SharedPtr<Timer> timer);
	void CompositeTypeArrayRemovingAtTest(SharedPtr<Timer> timer);

	void SimpleTypeArrayRemovingAtIndexCountTest(SharedPtr<Timer> timer);
	void CompositeTypeArrayRemovingAtIndexCountTest(SharedPtr<Timer> timer);

	void SimpleTypeArraySetTest(SharedPtr<Timer> timer);
	void CompositeTypeArraySetTest(SharedPtr<Timer> timer);

	SimpleTypeArrayVsCompositeTypeArrayTest(const SimpleTypeArrayVsCompositeTypeArrayTest & other);
	void operator =(const SimpleTypeArrayVsCompositeTypeArrayTest & other);
};

//===========================================================================//

}// namespace Bru

#endif // SIMPLE_TYPE_ARRAY_VS_COMPOSITE_TYPE_ARRAY_TEST_H

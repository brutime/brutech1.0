//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ConverterTest.h"

namespace Bru
{

//===========================================================================//

const integer ConverterTest::RepetitionCount = 50000;

//===========================================================================//

void ConverterTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("Converter test\n");
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	IntegerWithOffsetTest(timer);
	IntegerWithoutOffsetTest(timer);
	FloatWithOffsetTest(timer);
	FloatWithoutOffsetTest(timer);
}

//===========================================================================//

void ConverterTest::IntegerWithOffsetTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string resultString;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		resultString = Converter::ToString(static_cast<integer>(i / 100));
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Integer with offset test time = %f\n", resultTime);

	printf("\n\n%s\n", resultString.ToChars());
}

//===========================================================================//

void ConverterTest::IntegerWithoutOffsetTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string resultString;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		resultString = Converter::ToString(static_cast<integer>(i / 100));
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Integer without offset test time = %f\n", resultTime);

	printf("\n\n%s\n", resultString.ToChars());
}

//===========================================================================//

void ConverterTest::FloatWithOffsetTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string resultString;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		resultString = Converter::ToString(i / 100.0f, 4);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Float with offset test time = %f\n", resultTime);

	printf("\n\n%s\n", resultString.ToChars());
}

//===========================================================================//

void ConverterTest::FloatWithoutOffsetTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string resultString;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		resultString = Converter::ToString(i / 100.0f, 0);
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Float without offset test time = %f\n", resultTime);

	printf("\n\n%s\n", resultString.ToChars());
}

//===========================================================================//

}// namespace Bru

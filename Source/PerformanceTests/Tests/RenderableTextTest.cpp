//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderableTextTest.h"

namespace Bru
{

//===========================================================================//

const integer RenderableTextTest::RepetitionCount = 1000;

//===========================================================================//

RenderableTextTest::RenderableTextTest() :
	IPerformanceTest()
{
}

//===========================================================================//

RenderableTextTest::~RenderableTextTest()
{
}

//===========================================================================//

void RenderableTextTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	OSConsoleStream->WriteLine("Renderable text test");
	OSConsoleStream->WriteLine(string::Format("  Repetition count: {0}", RepetitionCount));

	Renderer->AddDomain("RendererTest");
	Renderer->AddLayerToBack("RendererTest", new RenderableLayer("Layer"));

	FontManager->Load("Consolas", 12);

	OSConsoleStream->WriteLine("  Single line word wrap mode:");
	TestWithWordWaprMode(timer, WordWrapMode::SingleLine);
	
	//OSConsoleStream->WriteLine("  Simple word wrap mode:");
	//TestWithWordWaprMode(timer, WordWrapMode::Simple);	

	Renderer->RemoveLayer("RendererTest", "Layer");
	Renderer->RemoveDomain("RendererTest");

	FontManager->Remove("Consolas", 12);
}

//===========================================================================//

void RenderableTextTest::TestWithWordWaprMode(const SharedPtr<Timer> & timer, WordWrapMode wordWrapMode)
{
	Vector2 textBounds;
	SharedPtr<RenderableText> text = new RenderableText("RendererTest", "Layer", string::Empty, FontManager->Get("Consolas", 12),
	                                                    textBounds, Color::Default, wordWrapMode);

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		//Чтобы строки были не равны
		if (Math::Odd(i))
		{
			text->SetText("[Notice]:        Texture created \"Default16Antialiased\" (1, 512x256x16, 1.00x1.00, IsMipMap: false, IsSmooth: false, Border: Clamp to edge)");
		}
		else
		{
			text->SetText("_[Notice]:        Texture created \"Default16Antialiased\" (1, 512x256x16, 1.00x1.00, IsMipMap: false, IsSmooth: false, Border: Clamp to edge)");
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	OSConsoleStream->WriteLine(string::Format("    Summary time: {0:0.4}", resultTime));

	text = NullPtr;

}

//===========================================================================//

} // namespace Bru

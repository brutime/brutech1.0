//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест показывает разницу между массивами (классический, STD, Bru)
//

#pragma once
#ifndef RENDERABLE_TEXT_TEST_H
#define RENDERABLE_TEXT_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class RenderableTextTest : public IPerformanceTest
{
public:
	RenderableTextTest();
	virtual ~RenderableTextTest();

	void Start();

private:
	static const integer RepetitionCount;	
	
	void TestWithWordWaprMode(const SharedPtr<Timer> & timer, WordWrapMode wordWrapMode);

	RenderableTextTest(const RenderableTextTest &) = delete;
	RenderableTextTest & operator =(const RenderableTextTest &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERABLE_TEXT_TEST_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PointersTest.h"

namespace Bru
{

//===========================================================================//

const integer PointersTest::RepetitionCount = 1000000;

//===========================================================================//

ContainerObject::ContainerObject() :
	simplePtr(new string("ContainerObject")),
	sharedPtr(new string("ContainerObject")),
	weakPtr(sharedPtr)
{
}

//===========================================================================//

ContainerObject::~ContainerObject()
{
	delete simplePtr;
}

//===========================================================================//

void PointersTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("Pointers test\n");
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	SimplePointersTest(timer);
	SharedPointersTest(timer);
	WeakPointersTest(timer);
	WeakLockPointersTest(timer);
}

//===========================================================================//

void PointersTest::SimplePointersTest(SharedPtr<Timer> timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string * s;
	ContainerObject * object = new ContainerObject();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		s = object->GetSimplePtr();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of simple pointer = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("\nfor release \"%s\"\n", s->ToChars());

	s = NullPtr;
	delete object;
}

//===========================================================================//

void PointersTest::SharedPointersTest(SharedPtr<Timer> timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string * s;
	SharedPtr<ContainerObject> object = new ContainerObject();
	for (integer i = 0; i < RepetitionCount; i++)
	{
		s = object->GetSharedPtr();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of shared pointer = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("\nfor release \"%s\"\n", s->ToChars());
}

//===========================================================================//

void PointersTest::WeakPointersTest(SharedPtr<Timer> timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string * s;
	SharedPtr<ContainerObject> object = new ContainerObject();
	for (integer i = 0; i < RepetitionCount; i++)
	{
		s = object->GetWeakPtr();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of weak pointer = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("\nfor release \"%s\"\n", s->ToChars());
}

//===========================================================================//

void PointersTest::WeakLockPointersTest(SharedPtr<Timer> timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();

	string * s;
	SharedPtr<ContainerObject> object = new ContainerObject();
	for (integer i = 0; i < RepetitionCount; i++)
	{
		s = object->GetWeakPtr().Lock();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of weak lock pointer = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("\nfor release \"%s\"\n", s->ToChars());
}

//===========================================================================//

}// namespace Bru

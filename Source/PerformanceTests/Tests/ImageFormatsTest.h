//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест показывает разницу между массивами (классический, STD, Bru)
//

#pragma once
#ifndef IMAGE_FORMATS_TEST_H
#define IMAGE_FORMATS_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class ImageFormatsTest : public IPerformanceTest
{
public:
	ImageFormatsTest();
	virtual ~ImageFormatsTest();

	void Start();

private:
	static const integer RepetitionCount;

	void LoadingJPG(SharedPtr<Timer> timer, const string & pathToFile);
	void LoadingPNG(SharedPtr<Timer> timer, const string & pathToFile);
	void LoadingTGA(SharedPtr<Timer> timer, const string & pathToFile);	

	ImageFormatsTest(const ImageFormatsTest &) = delete;
	ImageFormatsTest & operator =(const ImageFormatsTest &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // IMAGE_FORMATS_TEST_H

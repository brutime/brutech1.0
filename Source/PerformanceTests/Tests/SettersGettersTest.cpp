//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SettersGettersTest.h"

namespace Bru
{

//===========================================================================//

const integer SettersGettersTest::RepetitionCount = 100000;

//===========================================================================//

void SettersGettersTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("Setters getters test\n");
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	StartSetWithoutSetterTest(timer);
	StartSetWithSetterTest(timer);
	StartGetWithoutSetterTest(timer);
	StartGetWithSetterTest(timer);
}

//===========================================================================//

void SettersGettersTest::StartSetWithoutSetterTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();
	for (integer i = 0; i < RepetitionCount; i++)
	{
		value = 666;
	}
	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time without set method = %f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%d\n", value);
}

//===========================================================================//

void SettersGettersTest::StartSetWithSetterTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();
	for (integer i = 0; i < RepetitionCount; i++)
	{
		SetValue(666);
	}
	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time with set method = %f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%d\n", value);
}

//===========================================================================//

void SettersGettersTest::StartGetWithoutSetterTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();
	integer tempValue;
	for (integer i = 0; i < RepetitionCount; i++)
	{
		tempValue = value;
	}
	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time without get method = %f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%d\n", tempValue);
}

//===========================================================================//

void SettersGettersTest::StartGetWithSetterTest(const SharedPtr<Timer> & timer)
{
	timer->Reset();
	float startTime = timer->GetSeconds();
	integer tempValue;
	for (integer i = 0; i < RepetitionCount; i++)
	{
		tempValue = GetValue();
	}
	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time with get method = %f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%d\n", tempValue);
}

//===========================================================================//

}// namespace Bru

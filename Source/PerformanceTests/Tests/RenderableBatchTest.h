//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест для оптимизации RenderableBatchTest
//

#pragma once
#ifndef RENDERABLE_BATCH_TEST_H
#define RENDERABLE_BATCH_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class RenderableBatchTest : public IPerformanceTest
{
public:
	RenderableBatchTest() :
		IPerformanceTest()
	{
	}
	virtual ~RenderableBatchTest()
	{
	}

	void Start();

	void AddObjectTest(SharedPtr<Timer> timer);

private:
	static const integer RenderableObjectsCount;
	static const integer RepetitionCount;

	RenderableBatchTest(const RenderableBatchTest &);
	RenderableBatchTest & operator =(const RenderableBatchTest &);
};

//===========================================================================//

}// namespace Bru

#endif // RENDERABLE_BATCH_H

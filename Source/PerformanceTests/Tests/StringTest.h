//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тест для оптимизации форматирования строки
//

#pragma once
#ifndef STRING_TEST_H
#define STRING_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class StringTest : public IPerformanceTest
{
public:
	StringTest();
	virtual ~StringTest();

	void Start();

	void CurrentStringAssignTest(const SharedPtr<Timer> & timer);
	void NewWaveStringTest(const SharedPtr<Timer> & timer);

	void ForEachTest(const SharedPtr<Timer> & timer);
	void FormatTest(const SharedPtr<Timer> & timer);
	void SubstringTest(const SharedPtr<Timer> & timer);
	void ReplaceTest(const SharedPtr<Timer> & timer);
	void InsertTest(const SharedPtr<Timer> & timer);
	void RemoveTest(const SharedPtr<Timer> & timer);
	void ContainsTest(const SharedPtr<Timer> & timer);
	void ContainsCountTest(const SharedPtr<Timer> & timer);
	void FirstIndexOfTest(const SharedPtr<Timer> & timer);
	void FirstIndexNumberOfTest(const SharedPtr<Timer> & timer);
	void LastIndexOfTest(const SharedPtr<Timer> & timer);
	void LastIndexNumberOfTest(const SharedPtr<Timer> & timer);
	void ToLowerTest(const SharedPtr<Timer> & timer);
	void ToUpperTest(const SharedPtr<Timer> & timer);
	void ReverseTest(const SharedPtr<Timer> & timer);
	void TrimLeftTest(const SharedPtr<Timer> & timer);
	void TrimRightTest(const SharedPtr<Timer> & timer);
	void TrimTest(const SharedPtr<Timer> & timer);
	void CleanUpTest(const SharedPtr<Timer> & timer);

private:
	static const integer RepetitionCount;
	integer accumulator;

	StringTest(const StringTest &);
	void operator =(const StringTest &);
};

//===========================================================================//

}// namespace Bru

#endif // STRING_TEST_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderableBatchTest.h"

#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

const integer RenderableBatchTest::RenderableObjectsCount = 1000;
const integer RenderableBatchTest::RepetitionCount = 1000;

//===========================================================================//

void RenderableBatchTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("Renderable batch test\n");
	printf("  Renderable objects count = %d\n", RenderableObjectsCount);
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	Renderer->AddDomain("RenderableBatchTest");
	Renderer->AddLayerToBack("RenderableBatchTest", new RenderableLayer("Layer"));

	AddObjectTest(timer);

	Renderer->RemoveLayer("RenderableBatchTest", "Layer");
	Renderer->RemoveDomain("RenderableBatchTest");
}

//===========================================================================//

void RenderableBatchTest::AddObjectTest(SharedPtr<Timer> timer)
{
	List<SharedPtr<Renderable> > sprites;

	for (integer i = 0; i < RenderableObjectsCount; i++)
	{
		sprites.Add(new Sprite("RenderableBatchTest", "Layer", Size(1.0f, 1.0f), Colors::Red));
	}

	RenderableBatch renderableBatch;

	float resultTime = .0f;

	for (integer i = 0; i < RepetitionCount; i++)
	{
		timer->Reset();

		for (integer j = 0; j < RenderableObjectsCount; j++)
		{
			renderableBatch.AddObject(sprites[j]);
		}

		resultTime += timer->GetSeconds();

		renderableBatch.Clear();
	}

	printf("  Time of adding objects = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%d\n", renderableBatch.GetVertices().GetCount());
}

//===========================================================================//

}// namespace Bru

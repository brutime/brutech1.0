//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MathTest.h"

#include "../../Math/Math.h"

namespace Bru
{

//===========================================================================//

const integer MathTest::RepetitionCount = 350000;

//===========================================================================//

void MathTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("Math tests\n");
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	MatrixMultiplyTest(timer);
	Vector3MatrixMultyplyTest(timer);
}

//===========================================================================//

void MathTest::MatrixMultiplyTest(SharedPtr<Timer> timer)
{
	Matrix4x4 matrixOne(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                    16.0f);

	Matrix4x4 matrixTwo(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                    16.0f);

	timer->Reset();
	float startTime = timer->GetSeconds();

	float someValue = 0.0f;
	Matrix4x4 resultMatrix;
	for (integer i = 0; i < RepetitionCount; i++)
	{
		resultMatrix = matrixOne * matrixTwo;
		someValue += resultMatrix.GetElement(2, 2);
	}

	printf("%0.4f\n", someValue);

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Matrix 4x4 Multiply time = %0.10f\n", resultTime);
}

//===========================================================================//

void MathTest::Vector3MatrixMultyplyTest(SharedPtr<Timer> timer)
{
	Matrix4x4 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                 16.0f);

	Vector3 vector(1.f, 2.f, 3.f);

	timer->Reset();
	float startTime = timer->GetSeconds();

	Vector3 resultVector;
	for (integer i = 0; i < RepetitionCount; i++)
	{
		resultVector = vector.MultiplyByMatrix4x4AsPoint(matrix);
	}

	printf("%0.4f\n", resultVector.X);

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Vector 3 multiply time = %0.10f\n", resultTime);
}

//===========================================================================//

}// namespace Bru

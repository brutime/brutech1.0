//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ArrayTest.h"

#include <vector>
#include "../../Common/Common.h"
#include "../../Math/Math.h"

namespace Bru
{

//===========================================================================//

const integer ArrayTest::VerticesCount = 29160;
const integer ArrayTest::RepetitionCount = 1000;

//===========================================================================//

void ArrayTest::Start()
{
	SharedPtr<Timer> timer = new Timer();

	printf("Array test\n");
	printf("  Vertices count = %d\n", VerticesCount);
	printf("  Repetition count = %d\n", RepetitionCount);
	printf("\n");

	BruForceConvertingTest(timer);
	BruDirectConvertingTest(timer);

	ClassicOrderedReadIndexing(timer);
	STDOrderedReadIndexing(timer);
	BruOrderedReadIndexing(timer);

	ClassicOrderedWriteIndexing(timer);
	STDOrderedWriteIndexing(timer);
	BruOrderedWriteIndexing(timer);

	ClassicRandomReadIndexing(timer);
	STDRandomReadIndexing(timer);
	BruRandomReadIndexing(timer);

	ClassicRandomWriteIndexing(timer);
	STDRandomWriteIndexing(timer);
	BruRandomWriteIndexing(timer);
}

//===========================================================================//

void ArrayTest::BruForceConvertingTest(SharedPtr<Timer> timer)
{
	Array<float> array;
	array.Resize(VerticesCount);

	for (integer i = 0; i < VerticesCount; i++)
	{
		array[i] = Math::Random(1000.0f);
	}

	float classicArray[VerticesCount];

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			classicArray[j] = array[j];
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of force converting by from Bru array to C array = %0.10f\n", resultTime);
	printf("  Average time of force converting by from Bru array to C array of 1 covertion = %f\n",
	       resultTime / RepetitionCount);
	//Чтобы Release не обрезал
	printf("%f\n", classicArray[VerticesCount - 1]);
}

//===========================================================================//

void ArrayTest::BruDirectConvertingTest(SharedPtr<Timer> timer)
{
	Array<float> array;
	array.Resize(VerticesCount);

	for (integer i = 0; i < VerticesCount; i++)
	{
		array[i] = Math::Random(1000.0f);
	}

	float * classicArray;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		classicArray = array.GetData();
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of direct converting by from Bru array to C array = %0.10f\n", resultTime);
	printf("  Average time of direct converting by from Bru array to C array of 1 covertion = %0.10f\n",
	       resultTime / RepetitionCount);
	//Чтобы Release не обрезал
	printf("%f\n", classicArray[VerticesCount - 1]);
}

//===========================================================================//

void ArrayTest::ClassicOrderedReadIndexing(SharedPtr<Timer> timer)
{
	float array[VerticesCount] = { 0 };
	float value = 0.0f;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			value = array[j];
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of ordered read indexing a C array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", value);
}

//===========================================================================//

void ArrayTest::STDOrderedReadIndexing(SharedPtr<Timer> timer)
{
	std::vector<float> array;
	array.resize(VerticesCount);
	float value = 0.0f;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			value = array[j];
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of ordered read indexing a STD array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", value);
}

//===========================================================================//

void ArrayTest::BruOrderedReadIndexing(SharedPtr<Timer> timer)
{
	Array<float> array;
	array.Resize(VerticesCount);
	float value = 0.0f;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			value = array[j];
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of ordered read indexing a Bru array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", value);
}

//===========================================================================//

void ArrayTest::ClassicOrderedWriteIndexing(SharedPtr<Timer> timer)
{
	float array[VerticesCount] = { 0 };

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			array[j] = Math::Random(1000.0f);
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of ordered write indexing a C array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", array[VerticesCount - 1]);
}

//===========================================================================//

void ArrayTest::STDOrderedWriteIndexing(SharedPtr<Timer> timer)
{
	std::vector<float> array;
	array.resize(VerticesCount);

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			array[j] = Math::Random(1000.0f);
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of ordered write indexing a STD array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", array[VerticesCount - 1]);
}

//===========================================================================//

void ArrayTest::BruOrderedWriteIndexing(SharedPtr<Timer> timer)
{
	Array<float> array;
	array.Resize(VerticesCount);

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			array[j] = Math::Random(1000.0f);
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of ordered write indexing a Bru array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", array[VerticesCount - 1]);
}

//===========================================================================//

void ArrayTest::ClassicRandomReadIndexing(SharedPtr<Timer> timer)
{
	float array[VerticesCount] = { 0 };
	float value = 0.0f;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			value = array[Math::Random(RepetitionCount - 1)];
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of random read indexing a C array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", value);
}

//===========================================================================//

void ArrayTest::STDRandomReadIndexing(SharedPtr<Timer> timer)
{
	std::vector<float> array;
	array.resize(VerticesCount);
	float value = 0.0f;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			value = array[Math::Random(RepetitionCount - 1)];
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of random read indexing a STD array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", value);
}

//===========================================================================//

void ArrayTest::BruRandomReadIndexing(SharedPtr<Timer> timer)
{
	Array<float> array;
	array.Resize(VerticesCount);
	float value = 0.0f;

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			value = array[Math::Random(RepetitionCount - 1)];
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of random read indexing a Bru array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", value);
}

//===========================================================================//

void ArrayTest::ClassicRandomWriteIndexing(SharedPtr<Timer> timer)
{
	float array[VerticesCount];

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			array[Math::Random(RepetitionCount - 1)] = Math::Random(1000.0f);
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of random write indexing a C array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", array[VerticesCount - 1]);
}

//===========================================================================//

void ArrayTest::STDRandomWriteIndexing(SharedPtr<Timer> timer)
{
	std::vector<float> array;
	array.resize(VerticesCount);

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			array[Math::Random(RepetitionCount - 1)] = Math::Random(1000.0f);
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of random write indexing a STD array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", array[VerticesCount - 1]);
}

//===========================================================================//

void ArrayTest::BruRandomWriteIndexing(SharedPtr<Timer> timer)
{
	Array<float> array;
	array.Resize(VerticesCount);

	timer->Reset();
	float startTime = timer->GetSeconds();

	for (integer i = 0; i < RepetitionCount; i++)
	{
		for (integer j = 0; j < VerticesCount; j++)
		{
			array[Math::Random(RepetitionCount - 1)] = Math::Random(1000.0f);
		}
	}

	float resultTime = timer->GetSeconds() - startTime;
	printf("  Time of random write indexing a Bru array = %0.10f\n", resultTime);
	//Чтобы Release не обрезал
	printf("%f\n", array[VerticesCount - 1]);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef MATH_TEST_H
#define MATH_TEST_H

#include "IPerformanceTest.h"

#include "../../OperatingSystem/Timer.h"

namespace Bru
{

//===========================================================================//

class MathTest : public IPerformanceTest
{
public:
	MathTest() :
		IPerformanceTest()
	{
	}
	virtual ~MathTest()
	{
	}

	void Start();

private:
	static const integer RepetitionCount;

	void MatrixMultiplyTest(SharedPtr<Timer> timer);
	void Vector3MatrixMultyplyTest(SharedPtr<Timer> timer);

	MathTest(const MathTest & otherMatrix4x4Test);
	void operator =(const MathTest & otherMatrix4x4Test);
};

//===========================================================================//

}// namespace Bru

#endif // MATH_TEST_H

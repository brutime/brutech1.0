//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Входная точка тестов производительности
//

#include "Tests/IPerformanceTest.h"
#include "Tests/ArrayTest.h"
#include "Tests/ConverterTest.h"
#include "Tests/ImageFormatsTest.h"
#include "Tests/MathTest.h"
#include "Tests/PointersTest.h"
#include "Tests/RenderableBatchTest.h"
#include "Tests/RenderableTextTest.h"
#include "Tests/SettersGettersTest.h"
#include "Tests/SimpleTypeArrayVsCompositeTypeArrayTest.h"
#include "Tests/StringTest.h"

#include "Helpers/ApplicationForPerformanceTests.h"

#include "../Utils/Log/Log.h"

using namespace Bru;

//===========================================================================//

//SharedPtr<IPerformanceTest> currentTest = new ArrayTest();
//SharedPtr<IPerformanceTest> currentTest = new ConverterTest();
//SharedPtr<IPerformanceTest> currentTest = new ImageFormatsTest();
//SharedPtr<IPerformanceTest> currentTest = new MathTest();
//SharedPtr<IPerformanceTest> currentTest = new PointersTest();
//SharedPtr<IPerformanceTest> currentTest = new RenderableBatchTest();
SharedPtr<IPerformanceTest> currentTest = new RenderableTextTest();
//SharedPtr<IPerformanceTest> currentTest = new SettersGettersTest();
//SharedPtr<IPerformanceTest> currentTest = new SimpleTypeArrayVsCompositeTypeArrayTest();
//SharedPtr<IPerformanceTest> currentTest = new StringTest();

//===========================================================================//

int main()
{
	ScopedPtr<ApplicationForPerformanceTests> applicationForPerformanceTests(new ApplicationForPerformanceTests());
	applicationForPerformanceTests->Initialize();

	currentTest->Start();

	applicationForPerformanceTests->FreeResources();

	return 0;
}

//===========================================================================//

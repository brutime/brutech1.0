//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Основной компонент позиционирования, хранит иерархию себе подобны
//

#pragma once
#ifndef BASE_POSITIONING_NODE_H
#define BASE_POSITIONING_NODE_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "../EntitySystem/EntitySystem.h"
#include "IPositioning.h"

namespace Bru
{

//===========================================================================//

class BasePositioningNode : public EntityComponent, public IPositioning, public PtrFromThisMixIn<BasePositioningNode>
{
public:	
	DECLARE_COMPONENT

	virtual ~BasePositioningNode();

	virtual void Reset();

	void AddChild(const SharedPtr<BasePositioningNode> & child);
	void RemoveChild(const SharedPtr<BasePositioningNode> & child);
	void RemoveAllChildren();

	void MakeIdentity();

	virtual void SetPosition(float x, float y, float z = 0.0f);
	virtual void SetPosition(const Vector3 & position);

	virtual void SetPositionX(float x);
	virtual void SetPositionY(float y);
	virtual void SetPositionZ(float z);

	virtual void Translate(float dx, float dy, float dz = 0.0f);
	virtual void Translate(const Vector3 & delta);

	virtual void TranslateX(float dx);
	virtual void TranslateY(float dy);
	virtual void TranslateZ(float dz);

	const Vector3 & GetLocalPosition() const;
	virtual const Vector3 & GetPosition() const;

	virtual float GetPositionX() const;
	virtual float GetPositionY() const;
	virtual float GetPositionZ() const;

	virtual void Rotate(float x, float y, float z, float angle);
	virtual void Rotate2D(float angle);
	virtual void SetAngleZ(float angle);

	virtual float GetAngleX() const;
	virtual float GetAngleY() const;
	virtual float GetAngleZ() const;

	const Transformation & GetLocalTransformation() const;
	const Transformation & GetWorldTransformation() const;

	virtual const string & GetDomainName() const;

	string ToString() const;

protected:
	BasePositioningNode(const string & domainName);

private:
	void UpdateTransformation();
	void PrepareForRemoveFromParent();

	Transformation _localTransformation;
	Transformation _worldTransformation;

	WeakPtr<BasePositioningNode> _parent;
	List<SharedPtr<BasePositioningNode> > _children;

private:
	BasePositioningNode(const BasePositioningNode & other) = delete;
	BasePositioningNode & operator =(const BasePositioningNode & other) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // BASE_POSITIONING_NODE_H

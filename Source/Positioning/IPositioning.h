//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс для объектов которые изменяют свою трансформацию
//

#pragma once
#ifndef I_POSITIONING_H
#define I_POSITIONING_H

#include "../Common/Common.h"
#include "../Math/Math.h"

namespace Bru
{

//===========================================================================//

class IPositioning
{
public:
	virtual ~IPositioning();	

	virtual void SetPosition(float x, float y, float z = 0.0f) = 0;
	virtual void SetPosition(const Vector3 & position) = 0;

	virtual void SetPositionX(float x) = 0;
	virtual void SetPositionY(float y) = 0;
	virtual void SetPositionZ(float z) = 0;

	virtual void Translate(float dx, float dy, float dz = 0.0f) = 0;
	virtual void Translate(const Vector3 & delta) = 0;

	virtual void TranslateX(float dx) = 0;
	virtual void TranslateY(float dy) = 0;
	virtual void TranslateZ(float dz) = 0;

	virtual const Vector3 & GetPosition() const = 0;

	virtual float GetPositionX() const = 0;
	virtual float GetPositionY() const = 0;
	virtual float GetPositionZ() const = 0;

	virtual void Rotate(float x, float y, float z, float angle) = 0;
	virtual void Rotate2D(float angle) = 0;
	virtual void SetAngleZ(float angle) = 0;

	virtual float GetAngleX() const = 0;
	virtual float GetAngleY() const = 0;
	virtual float GetAngleZ() const = 0;

	virtual const string & GetDomainName() const = 0;

protected:
	IPositioning();
	
protected:
	InnerEvent<const EmptyEventArgs &> _transformationChangedEvent;

public:
	Event<const EmptyEventArgs &> TransformationChanged;	

private:
	IPositioning(const IPositioning & other) = delete;
	IPositioning & operator =(const IPositioning & other) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // I_POSITIONING_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BasePositioningNode.h"

#include "../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(BasePositioningNode, EntityComponent)

//===========================================================================//

BasePositioningNode::BasePositioningNode(const string & domainName) :
	EntityComponent(domainName),
	IPositioning(),
	PtrFromThisMixIn<BasePositioningNode>(),
	_localTransformation(),
	_worldTransformation(),
	_parent(),
	_children()
{
	Register();
}

//===========================================================================//

BasePositioningNode::~BasePositioningNode()
{
	RemoveAllChildren();
	if (IsRegistred())
	{
		Unregister();
	}
}

//===========================================================================//

void BasePositioningNode::Reset()
{
	EntityComponent::Reset();
	MakeIdentity();
}

//===========================================================================//

void BasePositioningNode::AddChild(const SharedPtr<BasePositioningNode> & child)
{
#ifdef DEBUGGING
	if (child == this)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Self adding node \"{0}\"", GetName()));
	}

	if (child->_parent != NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Child \"{0}\" already has parent \"{1}\"", child->GetName(), child->_parent->GetName()));
	}
#endif

	child->_parent = GetWeakPtr();
	_children.Add(child);
	child->UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::RemoveChild(const SharedPtr<BasePositioningNode> & child)
{
	child->PrepareForRemoveFromParent();
	_children.Remove(child);
}

//===========================================================================//

void BasePositioningNode::RemoveAllChildren()
{
	for (const SharedPtr<BasePositioningNode> & child : _children)
	{
		child->PrepareForRemoveFromParent();
	}
	_children.Clear();
}

//===========================================================================//

const string & BasePositioningNode::GetDomainName() const
{
	return EntityComponent::GetDomainName();
}

//===========================================================================//

void BasePositioningNode::MakeIdentity()
{
	_localTransformation.MakeIdentity();
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::SetPosition(float newX, float newY, float newZ)
{
	_localTransformation.SetPosition(newX, newY, newZ);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::SetPosition(const Vector3 & position)
{
	_localTransformation.SetPosition(position.X, position.Y, position.Z);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::SetPositionX(float newX)
{
	_localTransformation.SetPositionX(newX);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::SetPositionY(float newY)
{
	_localTransformation.SetPositionY(newY);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::SetPositionZ(float newZ)
{
	_localTransformation.SetPositionZ(newZ);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::Translate(float dx, float dy, float dz)
{
	_localTransformation.Translate(dx, dy, dz);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::Translate(const Vector3 & delta)
{
	_localTransformation.Translate(delta);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::TranslateX(float dx)
{
	_localTransformation.TranslateX(dx);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::TranslateY(float dy)
{
	_localTransformation.TranslateY(dy);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::TranslateZ(float dz)
{
	_localTransformation.TranslateZ(dz);
	UpdateTransformation();
}

//===========================================================================//

const Vector3 & BasePositioningNode::GetLocalPosition() const
{
	return _localTransformation.GetPosition();
}

//===========================================================================//

const Vector3 & BasePositioningNode::GetPosition() const
{
	return _worldTransformation.GetPosition();
}

//===========================================================================//

float BasePositioningNode::GetPositionX() const
{
	return _worldTransformation.GetPosition().X;
}

//===========================================================================//

float BasePositioningNode::GetPositionY() const
{
	return _worldTransformation.GetPosition().Y;
}

//===========================================================================//

float BasePositioningNode::GetPositionZ() const
{
	return _worldTransformation.GetPosition().Z;
}

//===========================================================================//

void BasePositioningNode::Rotate(float x, float y, float z, float angle)
{
	_localTransformation.Rotate(x, y, z, angle);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::Rotate2D(float angle)
{
	_localTransformation.Rotate2D(angle);
	UpdateTransformation();
}

//===========================================================================//

void BasePositioningNode::SetAngleZ(float angle)
{
	Rotate2D(angle - GetAngleZ());
}

//===========================================================================//

float BasePositioningNode::GetAngleX() const
{
	return _localTransformation.GetAngleX();
}

//===========================================================================//

float BasePositioningNode::GetAngleY() const
{
	return _localTransformation.GetAngleY();
}

//===========================================================================//

float BasePositioningNode::GetAngleZ() const
{
	return _localTransformation.GetAngleZ();
}

//===========================================================================//

const Transformation & BasePositioningNode::GetLocalTransformation() const
{
	return _localTransformation;
}

//===========================================================================//

const Transformation & BasePositioningNode::GetWorldTransformation() const
{
	return _worldTransformation;
}

//===========================================================================//

string BasePositioningNode::ToString() const
{
	return string::Format("{0}:\nWorld transformation:\n{1}\nLocal transformation:\n{2}", GetName(),
	                      _worldTransformation.ToString(), _localTransformation.ToString());
}

//===========================================================================//

void BasePositioningNode::UpdateTransformation()
{
	if (_parent != NullPtr)
	{
		_worldTransformation = _localTransformation * _parent->_worldTransformation;
	}
	else
	{
		_worldTransformation = _localTransformation;
	}

	_transformationChangedEvent.Fire(EmptyEventArgs());

	for (const SharedPtr<BasePositioningNode> & child : _children)
	{
		child->UpdateTransformation();
	}
}

//===========================================================================//

void BasePositioningNode::PrepareForRemoveFromParent()
{
	_parent = NullPtr;
	UpdateTransformation();
}

//===========================================================================//

} // namespace Bru

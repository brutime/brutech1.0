//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый контроллер передвижения
//

#pragma once
#ifndef POSITION_CONTROLLER_H
#define POSITION_CONTROLLER_H

#include <functional>

#include "../../Common/Common.h"
#include "../../Updater/Updatable.h"
#include "../SceneNode.h"

namespace Bru
{

//===========================================================================//

class PositionController : public Updatable
{
protected:
	typedef std::function<Vector3(float)> PositionFunction;
	typedef std::function<bool(Vector3, float)> SignalCondition;

public:
	static const string Type;

	PositionController(const string & domainName, integer order, const WeakPtr<SceneNode> & target,
	                   PositionFunction function, SignalCondition condition, bool isEnabled = true);

	virtual ~PositionController();

	virtual void Reset();

	virtual void Update(float deltaTime);

	virtual void SetPositionFunction(const PositionFunction & function, bool resetTime = false);

protected:
	WeakPtr<SceneNode> _target;
	PositionFunction _function;
	SignalCondition _condition;
	float _elapsedTime;
	InnerEvent<const PositionController &> _signalConditionMet;

public:
	Event<const PositionController &> ConditionMet;

private:
	PositionController(const PositionController &) = delete;
	PositionController & operator =(const PositionController &) = delete;
};

//===========================================================================//

}//namespace

#endif // POSITION_CONTROLLER_H

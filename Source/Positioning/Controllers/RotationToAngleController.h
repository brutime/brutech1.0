//
//    BruTime Software (c) 2011. All rights reserved
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ROTATION_TO_ANGLE_CONTROLLER_H
#define ROTATION_TO_ANGLE_CONTROLLER_H

#include "BaseRotationController.h"

namespace Bru
{

//===========================================================================//

class RotationToAngleController : public BaseRotationController
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	RotationToAngleController(const WeakPtr<IPositioning> & transformational, float rotationSpeed,
	                          const Nullable<Angle> & targetAngle, float accuracy, bool isEnabled = true);
	RotationToAngleController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~RotationToAngleController();

	virtual void Update(float deltaTime);

	bool IsTargetAngleReached() const;

	void SetTargetAngle(const Nullable<Angle> & targetAngle);
	const Nullable<Angle> & GetTargetAngle() const;

protected:
	Nullable<Angle> _targetAngle;
	float _accuracy;

private:
	static const string PathToTargetAngle;
	static const string PathToAccuracy;

protected:
	InnerEvent<const EmptyEventArgs &> _targetAngleReached;

public:
	Event<const EmptyEventArgs &> TargetAngleReached;

private:
	RotationToAngleController(const RotationToAngleController &);
	RotationToAngleController & operator = (const RotationToAngleController &);
};

//===========================================================================//

} // namespace Bru

#endif // ROTATION_TO_ANGLE_CONTROLLER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PositionController.h"

namespace Bru
{

//===========================================================================//

const string PositionController::Type = "PositionController";

//===========================================================================//

PositionController::PositionController(const string & domainName, integer order, const WeakPtr<SceneNode> & target,
                                       PositionFunction function, SignalCondition condition, bool isEnabled) :
	Updatable(domainName, Type, order, isEnabled),
	_target(target),
	_function(function),
	_condition(condition),
	_elapsedTime(0),
	_signalConditionMet(),
	ConditionMet(_signalConditionMet)
{
}

//===========================================================================//

PositionController::~PositionController()
{
}

//===========================================================================//

void PositionController::Reset()
{
	_elapsedTime = 0;
}

//===========================================================================//

void PositionController::Update(float deltaTime)
{
	_elapsedTime += deltaTime;
	_target->SetPosition(_function(_elapsedTime));
	if (_condition(_target->GetPosition(), _elapsedTime))
	{
		_signalConditionMet.Fire(*this);
	}
}

//===========================================================================//

void PositionController::SetPositionFunction(const PositionFunction & function, bool reset)
{
	_function = function;
	if (reset)
	{
		Reset();
	}
}

//===========================================================================//

}

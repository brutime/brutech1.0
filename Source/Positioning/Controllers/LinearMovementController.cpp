//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LinearMovementController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(LinearMovementController, BaseMovementController)

//===========================================================================//

const string LinearMovementController::PathToNormalVector = "NormalVector";

//===========================================================================//

LinearMovementController::LinearMovementController(const WeakPtr<IPositioning> & transformational,
        const Vector3 & normalVector, float speed, float accuracy, bool isEnabled) :
	BaseMovementController(transformational, speed, accuracy, isEnabled),
	_normalVector(normalVector),
	_normalVectorResetValue(_normalVector)
{
}

//===========================================================================//

LinearMovementController::LinearMovementController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	BaseMovementController(domainName, provider),
	_normalVector(provider->Get<Vector3>(PathToNormalVector)),
	_normalVectorResetValue(_normalVector)
{

}

//===========================================================================//

LinearMovementController::~LinearMovementController()
{
}

//===========================================================================//

void LinearMovementController::Reset()
{
	BaseMovementController::Reset();
	_normalVector = _normalVectorResetValue;
}

//===========================================================================//

void LinearMovementController::Update(float deltaTime)
{
	if (_transformational != NullPtr)
	{
		_transformational->Translate(_normalVector * GetSpeed() * deltaTime);
	}
		
	UpdateOrientation();
}

//===========================================================================//

void LinearMovementController::SetNormalVector(const Vector3 & normalVector)
{
	_normalVector = normalVector;
}

//===========================================================================//

const Vector3 & LinearMovementController::GetNormalVector() const
{
	return _normalVector;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseMovementController.h"

#include "../PositioningNode.h"
#include "../RotationNode.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(BaseMovementController, Updatable)

//===========================================================================//

const string BaseMovementController::PathToTransformationalTypeName = "TransformationalType";
const string BaseMovementController::PathToAccuracy = "Accuracy";
const string BaseMovementController::PathToSpeed = "Speed";

//===========================================================================//

BaseMovementController::BaseMovementController(const WeakPtr<IPositioning> & transformational,
                                               float speed, float accuracy, bool isEnabled) :
	Updatable(transformational->GetDomainName(), isEnabled),
	_transformational(transformational),
	_accuracy(accuracy),
	_previousPosition(transformational->GetPosition()),
	_orientation(),
	_speed(speed),
	_speedResetValue(_speed)
{
}

//===========================================================================//

BaseMovementController::BaseMovementController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	Updatable(domainName, provider),
	_transformational(NullPtr),
	_transformationalTypeID(EntitySystem::GetComponentTypeID(provider->Get<string>(PathToTransformationalTypeName))),
	_accuracy(provider->Get<float>(PathToAccuracy)),
	_previousPosition(),
	_orientation(),
	_speed(provider->Get<float>(PathToSpeed)),
	_speedResetValue(_speed)
{
}

//===========================================================================//

BaseMovementController::~BaseMovementController()
{
}

//===========================================================================//

void BaseMovementController::Reset()
{
	Updatable::Reset();
	_previousPosition.Clear();
	_orientation.Clear();
	_speed = _speedResetValue;
	ResetSpeedModifier();
}

//===========================================================================//

void BaseMovementController::SetSpeed(float speed)
{
	_speed = speed;
}

//===========================================================================//

float BaseMovementController::GetSpeed() const
{
	return _speed * _speedModifier;
}

//===========================================================================//

const Vector3 & BaseMovementController::GetOrientation() const
{
	return _orientation;
}

//===========================================================================//

void BaseMovementController::OnAddedToOwner()
{
	if (_transformationalTypeID == PositioningNode::TypeID)
	{
		_transformational = GetOwnerComponent<PositioningNode>();
	}
	else if (_transformationalTypeID == RotationNode::TypeID)
	{
		_transformational = GetOwnerComponent<RotationNode>();
	}

	UpdatePreviousPosition();
}

//===========================================================================//

void BaseMovementController::OnRemovingFromOwner()
{
	if (_transformationalTypeID != -1)
	{
		_transformational = NullPtr;
		_previousPosition.Clear();
		_orientation.Clear();
	}
}

//===========================================================================//

void BaseMovementController::UpdateOrientation()
{
	if (_transformational == NullPtr)
	{
		return;
	}

	_orientation = _transformational->GetPosition() - _previousPosition;
	_orientation.Normalize();
	UpdatePreviousPosition();
}

//===========================================================================//

bool BaseMovementController::IsTargetCoordsReached(const Vector3 & estimatedCoords, const Vector3 & targetCoords) const
{
	if (IsTargetCoordsEquals(estimatedCoords, targetCoords))
	{
		return true;
	}

	Vector3 currentCoods = _transformational->GetPosition();

	//учитывается "перелет" позиции из-за больших скоростей, или большого deltaTime
	return (((currentCoods.X <= targetCoords.X && targetCoords.X <= estimatedCoords.X) ||
	         (estimatedCoords.X <= targetCoords.X && targetCoords.X <= currentCoods.X)) &&
	        ((currentCoods.Y <= targetCoords.Y && targetCoords.Y <= estimatedCoords.Y) ||
	         (estimatedCoords.Y <= targetCoords.Y && targetCoords.Y <= currentCoods.Y)) &&
	        ((currentCoods.Z <= targetCoords.Z && targetCoords.Z <= estimatedCoords.Z) ||
	         (estimatedCoords.Z <= targetCoords.Z && targetCoords.Z <= currentCoods.Z)));
}

//===========================================================================//

bool BaseMovementController::IsTargetCoordsEquals(const Vector3 & estimatedCoords, const Vector3 & targetCoords) const
{
	return estimatedCoords.Equals(targetCoords, _accuracy);
}

//===========================================================================//

void BaseMovementController::SetSpeedModifier(float value)
{
	_speedModifier = value;
}

//===========================================================================//

float BaseMovementController::GetSpeedModifier() const
{
	return _speedModifier;
}

//===========================================================================//

void BaseMovementController::ResetSpeedModifier()
{
	_speedModifier = 1.0f;
}

//===========================================================================//

void BaseMovementController::UpdatePreviousPosition()
{
	_previousPosition = _transformational->GetPosition();
}

//===========================================================================//

} // namespace Bru

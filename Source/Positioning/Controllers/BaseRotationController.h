//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BASE_ROTATION_CONTROLLER_H
#define BASE_ROTATION_CONTROLLER_H

#include "../../Updater/Updater.h"
#include "../IPositioning.h"

namespace Bru
{

//===========================================================================//

class BaseRotationController : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	BaseRotationController(const WeakPtr<IPositioning> & transformational, float rotationSpeed, bool isEnabled = true);
	BaseRotationController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~BaseRotationController();

	virtual void Reset();

	void SetRotationSpeed(float rotationSpeed);
	float GetRotationSpeed() const;

protected:
	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

	WeakPtr<IPositioning> _transformational;
	integer _transformationalTypeID = -1;
	float _rotationSpeed;
	float _rotationSpeedOnInitialize;

private:	
	static const string PathToTransformationalTypeName;
	static const string PathToRotationSpeed;

	BaseRotationController(const BaseRotationController &) = delete;
	BaseRotationController & operator =(const BaseRotationController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // BASE_ROTATION_CONTROLLER_H

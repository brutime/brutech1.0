//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseRotationController.h"

#include "../PositioningNode.h"
#include "../RotationNode.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(BaseRotationController, Updatable)

//===========================================================================//

const string BaseRotationController::PathToTransformationalTypeName = "TransformationalType";
const string BaseRotationController::PathToRotationSpeed = "RotationSpeed";

//===========================================================================//

BaseRotationController::BaseRotationController(const WeakPtr<IPositioning> & transformational, float rotationSpeed, bool isEnabled) :
	Updatable(transformational->GetDomainName(), isEnabled),
	_transformational(transformational),
	_rotationSpeed(rotationSpeed),
	_rotationSpeedOnInitialize(rotationSpeed)
{
}

//===========================================================================//

BaseRotationController::BaseRotationController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	Updatable(domainName, provider),
	_transformational(NullPtr),
	_transformationalTypeID(EntitySystem::GetComponentTypeID(provider->Get<string>(PathToTransformationalTypeName))),
	_rotationSpeed(provider->Get<float>(PathToRotationSpeed)),
	_rotationSpeedOnInitialize(_rotationSpeed)
{
}

//===========================================================================//

BaseRotationController::~BaseRotationController()
{
}

//===========================================================================//

void BaseRotationController::Reset()
{
	Updatable::Reset();
	_rotationSpeed = _rotationSpeedOnInitialize;
}

//===========================================================================//

void BaseRotationController::SetRotationSpeed(float rotationSpeed)
{
	_rotationSpeed = Math::Mod(rotationSpeed, 360.0f);
}

//===========================================================================//

float BaseRotationController::GetRotationSpeed() const
{
	return _rotationSpeed;
}

//===========================================================================//

void BaseRotationController::OnAddedToOwner()
{
	if (_transformationalTypeID == RotationNode::TypeID)
	{
		_transformational = GetOwnerComponent<RotationNode>();
	}
	else if (_transformationalTypeID == PositioningNode::TypeID)
	{
		_transformational = GetOwnerComponent<PositioningNode>();
	}	
}

//===========================================================================//

void BaseRotationController::OnRemovingFromOwner()
{
	if (_transformationalTypeID != -1)
	{
		_transformational = NullPtr;
	}	
}

//===========================================================================//

} // namespace Bru

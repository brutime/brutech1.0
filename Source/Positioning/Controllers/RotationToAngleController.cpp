//
//    BruTime Software (c) 2011. All rights reserved
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RotationToAngleController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(RotationToAngleController, BaseRotationController)

//===========================================================================//

const string RotationToAngleController::PathToTargetAngle = "TargetAngle";
const string RotationToAngleController::PathToAccuracy = "Accuracy";

//===========================================================================//

RotationToAngleController::RotationToAngleController(const WeakPtr<IPositioning> & transformational, float rotationSpeed,
                                                     const Nullable<Angle> & targetAngle, float accuracy, bool isEnabled) :
	BaseRotationController(transformational, rotationSpeed, isEnabled),
	_targetAngle(targetAngle),
	_accuracy(accuracy),
	_targetAngleReached(),
	TargetAngleReached(_targetAngleReached)
{
}

//===========================================================================//

RotationToAngleController::RotationToAngleController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	BaseRotationController(domainName, provider),
	_targetAngle(provider->Get<Angle>(PathToTargetAngle)),
	_accuracy(provider->Get<float>(PathToAccuracy)),
	_targetAngleReached(),
	TargetAngleReached(_targetAngleReached)
{

}

//===========================================================================//

RotationToAngleController::~RotationToAngleController()
{
}

//===========================================================================//

void RotationToAngleController::Update(float deltaTime)
{
	if (!_targetAngle.HasValue())
	{
		return;
	}

	if (IsTargetAngleReached())
	{
		return;
	}

	float currentRotationSpeed = _rotationSpeed  * deltaTime;
	float delta = _transformational->GetAngleZ() - _targetAngle.GetValue();
	if ((0.0f < delta && delta < 180.0f) || (-180.0f > delta && delta > -360.0f))
	{
		currentRotationSpeed = -currentRotationSpeed;
	}

	float newAngle = _transformational->GetAngleZ() + currentRotationSpeed;
	if ((_transformational->GetAngleZ() <= _targetAngle.GetValue() && _targetAngle.GetValue() <= newAngle) ||
	    (newAngle <= _targetAngle.GetValue() && _targetAngle.GetValue() <= _transformational->GetAngleZ()))
	{
		_targetAngleReached.Fire(EmptyEventArgs());
		_transformational->SetAngleZ(_targetAngle.GetValue());
	}
	else
	{
		_transformational->Rotate2D(currentRotationSpeed);
	}
}

//===========================================================================//

bool RotationToAngleController::IsTargetAngleReached() const
{
	if (!_targetAngle.HasValue())
	{
		return false;
	}

	return Math::Equals(_transformational->GetAngleZ(), _targetAngle.GetValue(), _accuracy);
}

//===========================================================================//

void RotationToAngleController::SetTargetAngle(const Nullable<Angle> & targetAngle)
{
	_targetAngle = targetAngle;
}

//===========================================================================//

const Nullable<Angle> & RotationToAngleController::GetTargetAngle() const
{
	return _targetAngle;
}

//===========================================================================//

} // namespace Bru

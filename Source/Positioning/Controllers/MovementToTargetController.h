//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Контроллер движения к точке
//

#pragma once
#ifndef MOVEMENT_TO_TARGET_CONTROLLER_H
#define MOVEMENT_TO_TARGET_CONTROLLER_H

#include "BaseMovementController.h"

namespace Bru
{

//===========================================================================//

class MovementToTargetController : public BaseMovementController
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	MovementToTargetController(const WeakPtr<IPositioning> & transformational, const WeakPtr<IPositioning> & target,
	                           float speed, float accuracy, bool isEnabled = true);
	MovementToTargetController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~MovementToTargetController();

	virtual void Reset();

	virtual void Update(float deltaTime);
	virtual bool IsTargetReached() const;

	void SetTarget(const WeakPtr<IPositioning> & target);
	const WeakPtr<IPositioning> & GetTarget() const;

protected:
	WeakPtr<IPositioning> _target;

protected:
	InnerEvent<const EmptyEventArgs &> _targetReached;

public:
	Event<const EmptyEventArgs &> TargetReached;

private:
	MovementToTargetController(const MovementToTargetController &) = delete;
	MovementToTargetController & operator =(const MovementToTargetController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // MOVEMENT_TO_NODE_CONTROLLER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ROTATION_CONTROLLER_H
#define ROTATION_CONTROLLER_H

#include "BaseRotationController.h"

namespace Bru
{

//===========================================================================//

class RotationController : public BaseRotationController
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	RotationController(const WeakPtr<IPositioning> & transformational, float rotationSpeed, bool isEnabled = true);
	RotationController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~RotationController();

	virtual void Update(float deltaTime);

private:
	RotationController(const RotationController &) = delete;
	RotationController & operator =(const RotationController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ROTATION_CONTROLLER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Контроллер движения к точке
//

#pragma once
#ifndef MOVEMENT_TO_POINT_CONTROLLER_H
#define MOVEMENT_TO_POINT_CONTROLLER_H

#include "BaseMovementController.h"

namespace Bru
{

//===========================================================================//

class MovementToPointController : public BaseMovementController
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	MovementToPointController(const WeakPtr<IPositioning> & transformational, const Nullable<Vector3> & point,
	                          float speed, float accuracy, bool isEnabled = true);
	MovementToPointController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~MovementToPointController();

	virtual void Reset();

	virtual void Update(float deltaTime);
	virtual bool IsPointReached() const;

	void SetPoint(const Nullable<Vector3> & point);
	const Nullable<Vector3> & GetPoint() const;

protected:
	Nullable<Vector3> _point;
	
private:
	static const string PathToPoint;

protected:
	InnerEvent<const EmptyEventArgs &> _pointReached;

public:
	Event<const EmptyEventArgs &> PointReached;

private:
	MovementToPointController(const MovementToPointController &) = delete;
	MovementToPointController & operator =(const MovementToPointController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // MOVEMENT_TO_POINT_CONTROLLER_H

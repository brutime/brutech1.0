//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MovementToPointController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(MovementToPointController, BaseMovementController)

//===========================================================================//

const string MovementToPointController::PathToPoint = "Point";

//===========================================================================//

MovementToPointController::MovementToPointController(const WeakPtr<IPositioning> & transformational,
                                                     const Nullable<Vector3> & point, float speed, float accuracy, bool isEnabled) :
	BaseMovementController(transformational, speed, accuracy, isEnabled),
	_point(point),
	_pointReached(),
	PointReached(_pointReached)
{
}

//===========================================================================//

MovementToPointController::MovementToPointController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	BaseMovementController(domainName, provider),
	_point(provider->Get<Nullable<Vector3>>(PathToPoint)),
	_pointReached(),
	PointReached(_pointReached)
{

}

//===========================================================================//

MovementToPointController::~MovementToPointController()
{
}

//===========================================================================//

void MovementToPointController::Reset()
{
	BaseMovementController::Reset();
	_point = NullPtr;
}

//===========================================================================//

void MovementToPointController::Update(float deltaTime)
{
	if (_point.HasValue() && !IsTargetCoordsEquals(_transformational->GetPosition(), _point.GetValue()))
	{
		Vector3 directionVector = _point.GetValue() - _transformational->GetPosition();
		directionVector.Normalize();
		Vector3 newPosition = _transformational->GetPosition() + directionVector * GetSpeed() * deltaTime;
		
		if (IsTargetCoordsReached(newPosition, _point.GetValue()))
		{
			_transformational->SetPosition(_point.GetValue());
			_pointReached.Fire(EmptyEventArgs());
		}
		else
		{
			_transformational->SetPosition(newPosition);
		}
	}
	
	UpdateOrientation();
}

//===========================================================================//

bool MovementToPointController::IsPointReached() const
{
	if (!_point.HasValue())
	{
		return false;
	}

	return IsTargetCoordsReached(_transformational->GetPosition(), _point.GetValue());
}

//===========================================================================//

void MovementToPointController::SetPoint(const Nullable<Vector3> & point)
{
	_point = point;
}

//===========================================================================//

const Nullable<Vector3> & MovementToPointController::GetPoint() const
{
	return _point;
}

//===========================================================================//

} // namespace Bru

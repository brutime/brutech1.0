//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Контроллер движения к точке с "отклонением" от курса
//

#pragma once
#ifndef FLOATING_MOVEMENT_TO_POINT_CONTROLLER_H
#define FLOATING_MOVEMENT_TO_POINT_CONTROLLER_H

#include "MovementToPointController.h"

namespace Bru
{

//===========================================================================//

class FloatingMovementToPointController : public MovementToPointController
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	FloatingMovementToPointController(const WeakPtr<IPositioning> & transformational, const Nullable<Vector3> & point,
	                                  float speed, float accuracy, float variation,
	                                  float variationResetTime, bool isEnabled = true);
	FloatingMovementToPointController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~FloatingMovementToPointController();

	virtual void Reset();

	virtual void Update(float deltaTime);

protected:
	float _variation;
	const float _variationResetTime;
	Vector3 _floatingTarget;
	float _timeUntilReset = -1.0f;

private:
	static const string PathToVariation;
	static const string PathToVariationResetTime;

	void ResetFloatingTarget();

	FloatingMovementToPointController(const FloatingMovementToPointController &) = delete;
	FloatingMovementToPointController & operator =(const FloatingMovementToPointController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // FLOATING_MOVEMENT_TO_POINT_CONTROLLER_H

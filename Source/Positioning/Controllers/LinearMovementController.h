//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Линейное движение
//

#pragma once
#ifndef LINEAR_TO_POINT_CONTROLLER_H
#define LINEAR_TO_POINT_CONTROLLER_H

#include "BaseMovementController.h"

namespace Bru
{

//===========================================================================//

class LinearMovementController : public BaseMovementController
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	LinearMovementController(const WeakPtr<IPositioning> & transformational, const Vector3 & normalVector,
	                         float speed, float accuracy, bool isEnabled = true);
	LinearMovementController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~LinearMovementController();

	virtual void Reset();

	virtual void Update(float deltaTime);

	void SetNormalVector(const Vector3 & normalVector);
	const Vector3 & GetNormalVector() const;

protected:
	Vector3 _normalVector;
	const Vector3 _normalVectorResetValue;

private:
	static const string PathToNormalVector;

	LinearMovementController(const LinearMovementController &) = delete;
	LinearMovementController & operator =(const LinearMovementController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // LINEAR_TO_POINT_CONTROLLER_H

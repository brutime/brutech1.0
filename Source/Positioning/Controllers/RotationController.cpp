//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RotationController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(RotationController, BaseRotationController)

//===========================================================================//

RotationController::RotationController(const WeakPtr<IPositioning> & transformational, float rotationSpeed, bool isEnabled) :
	BaseRotationController(transformational, rotationSpeed, isEnabled)
{
}

//===========================================================================//

RotationController::RotationController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	BaseRotationController(domainName, provider)
{
}

//===========================================================================//

RotationController::~RotationController()
{
}

//===========================================================================//

void RotationController::Update(float deltaTime)
{
	if (_transformational == NullPtr)
	{
		return;
	}

	_transformational->Rotate2D(_rotationSpeed * deltaTime);
}

//===========================================================================//

} // namespace Bru

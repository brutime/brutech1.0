//
//    BruTime Software (c) 2011. All rights reserved
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание:
//

#pragma once
#ifndef TRAJECTORY_MOVEMENT_CONTROLLER_H
#define TRAJECTORY_MOVEMENT_CONTROLLER_H

#include "BaseMovementController.h"

namespace Bru
{

//===========================================================================//

class TrajectoryMovementController : public BaseMovementController
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	TrajectoryMovementController(const WeakPtr<IPositioning> & transformational, float speed, float accuracy, bool isEnabled = true);
	TrajectoryMovementController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~TrajectoryMovementController();

	virtual void Reset();

	virtual void Update(float deltaTime);

	void AddPoint(const Vector3 & point);
	void Clear();

	bool IsEmpty() const;

	bool HasNextPoint() const;
	bool HasLastPoint() const;

	const Vector3 & GetNextPoint() const;
	const Vector3 & GetLastPoint() const;

	bool IsCurrentPointReached() const;
	bool IsLastPointReached() const;

private:
	List<Vector3> _points;
	Vector3 _currentPoint;

protected:
	InnerEvent<const EmptyEventArgs &> _currentPointReachedEvent;
	InnerEvent<const EmptyEventArgs &> _lastPointReachedEvent;

public:
	Event<const EmptyEventArgs &> CurrentPointReached;
	Event<const EmptyEventArgs &> LastPointReached;

private:
	TrajectoryMovementController(const TrajectoryMovementController &);
	TrajectoryMovementController & operator =(const TrajectoryMovementController &);
};

//===========================================================================//

} // namespace Bru

#endif // TRAJECTORY_MOVEMENT_CONTROLLER_H

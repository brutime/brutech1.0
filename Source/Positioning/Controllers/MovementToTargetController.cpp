//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MovementToTargetController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(MovementToTargetController, BaseMovementController)

//===========================================================================//

MovementToTargetController::MovementToTargetController(const WeakPtr<IPositioning> & transformational,
                                                       const WeakPtr<IPositioning> & target, float speed, float accuracy, bool isEnabled) :
	BaseMovementController(transformational, speed, accuracy, isEnabled),
	_target(target),
	_targetReached(),
	TargetReached(_targetReached)
{
}

//===========================================================================//

MovementToTargetController::MovementToTargetController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	BaseMovementController(domainName, provider),
	_target(NullPtr),
	_targetReached(),
	TargetReached(_targetReached)
{

}

//===========================================================================//

MovementToTargetController::~MovementToTargetController()
{
}

//===========================================================================//

void MovementToTargetController::Reset()
{
	BaseMovementController::Reset();
	_target == NullPtr;
}

//===========================================================================//

void MovementToTargetController::Update(float deltaTime)
{
	if (_target != NullPtr && !IsTargetCoordsEquals(_transformational->GetPosition(), _target->GetPosition()))
	{
		Vector3 directionVector = _target->GetPosition() - _transformational->GetPosition();
		directionVector.Normalize();
		Vector3 newPosition = _transformational->GetPosition() + directionVector * GetSpeed() * deltaTime;

		if (IsTargetCoordsReached(newPosition, _target->GetPosition()))
		{
			_transformational->SetPosition(_target->GetPosition());
			_targetReached.Fire(EmptyEventArgs());
		}
		else
		{
			_transformational->SetPosition(newPosition);
		}
	}

	UpdateOrientation();
}

//===========================================================================//

bool MovementToTargetController::IsTargetReached() const
{
	if (_target == NullPtr)
	{
		return false;
	}

	return IsTargetCoordsReached(_transformational->GetPosition(), _target->GetPosition());
}

//===========================================================================//

void MovementToTargetController::SetTarget(const WeakPtr<IPositioning> & target)
{
	_target = target;
}

//===========================================================================//

const WeakPtr<IPositioning> & MovementToTargetController::GetTarget() const
{
	return _target;
}

//===========================================================================//

} // namespace Bru

//
//    BruTime Software (c) 2011. All rights reserved
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TrajectoryMovementController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(TrajectoryMovementController, BaseMovementController)

//===========================================================================//

TrajectoryMovementController::TrajectoryMovementController(const WeakPtr<IPositioning> & transformational,
                                                           float speed, float accuracy, bool isEnabled) :
	BaseMovementController(transformational, speed, accuracy, isEnabled),
	_points(),
	_currentPoint(),
	_currentPointReachedEvent(),
	_lastPointReachedEvent(),
	CurrentPointReached(_currentPointReachedEvent),
	LastPointReached(_lastPointReachedEvent)
{
}

//===========================================================================//

TrajectoryMovementController::TrajectoryMovementController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	BaseMovementController(domainName, provider),
	_points(),
	_currentPoint(),
	_currentPointReachedEvent(),
	_lastPointReachedEvent(),
	CurrentPointReached(_currentPointReachedEvent),
	LastPointReached(_lastPointReachedEvent)
{

}

//===========================================================================//

TrajectoryMovementController::~TrajectoryMovementController()
{
}

//===========================================================================//

void TrajectoryMovementController::Reset()
{
	BaseMovementController::Reset();
	Clear();
}

//===========================================================================//

void TrajectoryMovementController::Update(float deltaTime)
{
	if (IsLastPointReached())
	{		
		return;
	}

	Vector3 directionVector = _points.GetFirst() - _transformational->GetPosition();
	directionVector.Normalize();
	Vector3 newPosition = _transformational->GetPosition() + directionVector * GetSpeed() * deltaTime;

	if (IsTargetCoordsReached(newPosition, _points.GetFirst()))
	{
		_transformational->SetPosition(_points.GetFirst());
		if (_points.GetCount() == 1)
		{
			_lastPointReachedEvent.Fire(EmptyEventArgs());
		}
		else
		{
			_currentPointReachedEvent.Fire(EmptyEventArgs());	
		}		
		_points.Remove(_points.GetFirst());
	}
	else
	{
		_transformational->SetPosition(newPosition);
	}

	UpdateOrientation();
}

//===========================================================================//

void TrajectoryMovementController::AddPoint(const Vector3 & point)
{
	_points.Add(point);
}

//===========================================================================//

void TrajectoryMovementController::Clear()
{
	_points.Clear();
}

//===========================================================================//

bool TrajectoryMovementController::IsEmpty() const
{
	return _points.GetCount() == 0;
}

//===========================================================================//

bool TrajectoryMovementController::HasNextPoint() const
{
	return _points.GetCount() > 1;
}

//===========================================================================//

bool TrajectoryMovementController::HasLastPoint() const
{
	return _points.GetCount() > 0;
}

//===========================================================================//

const Vector3 & TrajectoryMovementController::GetNextPoint() const
{
	return _points[1];
}

//===========================================================================//

const Vector3 & TrajectoryMovementController::GetLastPoint() const
{
	return _points.GetLast();
}

//===========================================================================//

bool TrajectoryMovementController::IsLastPointReached() const
{
	return _points.GetCount() == 0;
}

//===========================================================================//

bool TrajectoryMovementController::IsCurrentPointReached() const
{
	if (IsEmpty())
	{
		return false;
	}

	return IsTargetCoordsReached(_transformational->GetPosition(), _points.GetFirst());
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FloatingMovementToPointController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(FloatingMovementToPointController, MovementToPointController)

//===========================================================================//

const string FloatingMovementToPointController::PathToVariation = "Variation";
const string FloatingMovementToPointController::PathToVariationResetTime = "VariationResetTime";

//===========================================================================//

FloatingMovementToPointController::FloatingMovementToPointController(
    const WeakPtr<IPositioning> & transformational, const Nullable<Vector3> & point,
    float speed, float accuracy, float variation, float variationResetTime, bool isEnabled) :
	MovementToPointController(transformational, point, speed, accuracy, isEnabled),
	_variation(variation),
	_variationResetTime(variationResetTime),
	_floatingTarget()
{
}

//===========================================================================//

FloatingMovementToPointController::FloatingMovementToPointController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	MovementToPointController(domainName, provider),
	_variation(provider->Get<float>(PathToVariation)),
	_variationResetTime(provider->Get<float>(PathToVariationResetTime)),
	_floatingTarget()
{
}

//===========================================================================//

FloatingMovementToPointController::~FloatingMovementToPointController()
{
}

//===========================================================================//

void FloatingMovementToPointController::Reset()
{
	MovementToPointController::Reset();
	ResetFloatingTarget();
}

//===========================================================================//

void FloatingMovementToPointController::Update(float deltaTime)
{
	if (_timeUntilReset > 0)
	{
		_timeUntilReset -= deltaTime;
	}

	if (_timeUntilReset < 0)
	{
		ResetFloatingTarget();
	}

	if (_point.HasValue() && !IsTargetCoordsEquals(_transformational->GetPosition(), _point.GetValue()))
	{
		Vector3 directionVector = _point.GetValue() - _transformational->GetPosition();
		directionVector.Normalize();		
		Vector3 newPosition = _transformational->GetPosition() + (directionVector + _floatingTarget * _variation) * GetSpeed() * deltaTime;

		if (IsTargetCoordsReached(newPosition, _point.GetValue()))
		{
			_transformational->SetPosition(_point.GetValue());
			_pointReached.Fire(EmptyEventArgs());
		}
		else
		{
			_transformational->SetPosition(newPosition);
		}
	}

	UpdateOrientation();
}

//===========================================================================//

void FloatingMovementToPointController::ResetFloatingTarget()
{
	float angle = Math::RandomInRange(0, 359);
	_floatingTarget = {Math::Cos(angle), Math::Sin(angle), 0.0f};
	_timeUntilReset = _variationResetTime;
}

//===========================================================================//

} // namespace Bru

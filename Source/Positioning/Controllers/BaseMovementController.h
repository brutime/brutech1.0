//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый контроллер движения
//

#pragma once
#ifndef BASE_MOVEMENT_CONTROLLER_H
#define BASE_MOVEMENT_CONTROLLER_H

#include "../../Updater/Updater.h"
#include "../IPositioning.h"

namespace Bru
{

//===========================================================================//

class BaseMovementController : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	BaseMovementController(const WeakPtr<IPositioning> & transformational, float speed, float accuracy, bool isEnabled = true);
	BaseMovementController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~BaseMovementController();

	virtual void Reset();

	void SetSpeed(float speed);

	void SetSpeedModifier(float value);
	float GetSpeedModifier() const;
	void ResetSpeedModifier();

	const Vector3 & GetOrientation() const;

protected:
	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

	virtual void UpdateOrientation();
	
	bool IsTargetCoordsReached(const Vector3 & estimatedCoords, const Vector3 & targetCoords) const;
	bool IsTargetCoordsEquals(const Vector3 & estimatedCoords, const Vector3 & targetCoords) const;

	float GetSpeed() const;

	WeakPtr<IPositioning> _transformational;
	integer _transformationalTypeID = -1;
	float _accuracy;
	Vector3 _previousPosition;
	Vector3 _orientation;

private:
	static const string PathToTransformationalTypeName;
	static const string PathToAccuracy;
	static const string PathToSpeed;
	
	void UpdatePreviousPosition();

	float _speed;
	float _speedModifier = 1.0f;

	const float _speedResetValue;

	BaseMovementController(const BaseMovementController &) = delete;
	BaseMovementController & operator =(const BaseMovementController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // BASE_MOVEMENT_CONTROLLER_H

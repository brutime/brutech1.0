//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//


#include "RotationNode.h"
#include "PositioningNode.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(RotationNode, BasePositioningNode)

//===========================================================================//

RotationNode::RotationNode(const string & domainName) :
	BasePositioningNode(domainName)
{
}

//===========================================================================//

RotationNode::RotationNode(const string & domainName, const SharedPtr<BaseDataProvider> &) :
	RotationNode(domainName)
{
}

//===========================================================================//

RotationNode::~RotationNode()
{
}

//===========================================================================//

void RotationNode::OnAddedToOwner()
{
	const auto & positioningNode = GetOwnerComponent<PositioningNode>();
	if (positioningNode != NullPtr)
	{
		positioningNode->AddChild(GetWeakPtr());
	}
}

//===========================================================================//

void RotationNode::OnRemovingFromOwner()
{
	const auto & positioningNode = GetOwnerComponent<PositioningNode>();
	if (positioningNode != NullPtr)
	{
		positioningNode->RemoveChild(GetWeakPtr());
	}	
}

//===========================================================================//

} //namespace Bru

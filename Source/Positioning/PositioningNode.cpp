//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PositioningNode.h"

#include "../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(PositioningNode, BasePositioningNode)

//===========================================================================//

PositioningNode::PositioningNode(const string & domainName) :
	BasePositioningNode(domainName)
{
}

//===========================================================================//

PositioningNode::PositioningNode(const string & domainName, const SharedPtr<BaseDataProvider> &) :
	PositioningNode(domainName)
{
}

//===========================================================================//

PositioningNode::~PositioningNode()
{
}

//===========================================================================//

void PositioningNode::OnOwnerAddedChild(const SharedPtr<Entity> & child)
{
	const auto & childNode = child->GetComponent<PositioningNode>();
	AddChild(childNode);	
}

//===========================================================================//

void PositioningNode::OnOwnerRemovingChild(const SharedPtr<Entity> & child)
{
	const auto & childNode = child->GetComponent<PositioningNode>();
	RemoveChild(childNode);
}

//===========================================================================//

} // namespace Bru

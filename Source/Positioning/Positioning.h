//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Хедер подключает основные типы и классы системы позиционирования
//

#pragma once
#ifndef POSITIONING_H
#define POSITIONING_H

#include "IPositioning.h"
#include "PositioningNode.h"
#include "RotationNode.h"
#include "Controllers/FloatingMovementToPointController.h"
#include "Controllers/LinearMovementController.h"
#include "Controllers/MovementToPointController.h"
#include "Controllers/MovementToTargetController.h"
#include "Controllers/TrajectoryMovementController.h"
#include "Controllers/RotationController.h"
#include "Controllers/RotationToAngleController.h"

#endif // POSITIONING_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Основной компонент позиционирования, хранит иерархию себе подобны
//

#pragma once
#ifndef POSITIONING_NODE_H
#define POSITIONING_NODE_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "../EntitySystem/EntitySystem.h"
#include "BasePositioningNode.h"

namespace Bru
{

//===========================================================================//

class PositioningNode : public BasePositioningNode
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	PositioningNode(const string & domainName);
	PositioningNode(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~PositioningNode();

protected:
	virtual void OnOwnerAddedChild(const SharedPtr<Entity> & child);
	virtual void OnOwnerRemovingChild(const SharedPtr<Entity> & child);

private:
	PositioningNode(const PositioningNode & other) = delete;
	PositioningNode & operator =(const PositioningNode & other) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // POSITIONING_NODE_H

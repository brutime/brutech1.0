//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ROTATION_NODE_H
#define ROTATION_NODE_H

#include "BasePositioningNode.h"

namespace Bru
{

//===========================================================================//

class RotationNode : public BasePositioningNode
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	RotationNode(const string & domainName);
	RotationNode(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~RotationNode();

protected:
	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

private:
	RotationNode(const RotationNode &) = delete;
	RotationNode & operator = (const RotationNode &) = delete;
};

//===========================================================================//

} //namespace Bru

#endif //ROTATION_NODE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENTITY_MANAGER_H
#define ENTITY_MANAGER_H

#include "../../Utils/Console/Console.h"
#include "../../Utils/Watcher/Watcher.h"
#include "../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../Utils/PathHelper/PathHelper.h"
#include "../../Positioning/Positioning.h"
#include "../../Renderer/Renderer.h"
#include "../DomainManager/DomainManager.h"
#include "../Exceptions/EntityRecursiveDeclarationException.h"
#include "EntityHelper.h"
#include "EntityManagerEventArgs.h"
#include "EntityManagerEntityParametersItem.h"
#include "EntityManagerPoolItem.h"

namespace Bru
{

//===========================================================================//

class EntityManagerSingleton
{
public:
	typedef std::function<SharedPtr<EntityComponent>(const EntityManagerSingleton &, const string &, const WeakPtr<ParametersFile> &, integer)> CreatingComponentDelegate;

	static const string PoolDomainName;
	static const string PoolLayerName;

	static const string PurityLogicGroupName;

	EntityManagerSingleton(TreeMap<string, CreatingComponentDelegate> componentsDelegates, const Array<string> & groupsNames,
	                       const Array<string> & savingGroupsNames, bool isShowingPoolWarningMessages);
	~EntityManagerSingleton();

	void AddEntity(const SharedPtr<Entity> & entity, const string & domainName);

	SharedPtr<Entity> CreateEntity(const string & pathToEntity, const string & domainName,
	                               const string & layerName, const Vector3 & position,
	                               float angle = EntityHelper::DefaultAngle,
	                               const Size & size = EntityHelper::DefaultSize);

	void ReserveEntities(const string & pathToEntity, integer count);

	bool ContainsEntity(const SharedPtr<Entity> & entity);
	void RemoveEntity(const SharedPtr<Entity> & entity);
	void RemoveEntityWithoutPooling(const SharedPtr<Entity> & entity);

	string GetPathToEntity(const string & type);

	void AddGroup(const string & groupName);
	void AddGroups(const Array<string> & groupsNames);
	const List<SharedPtr<Entity>> & GetGroup(const string &groupName) const;
	bool ContainsGroup(const string & groupName) const;
	bool IsGroupEmpty(const string & groupName);
	void ClearGroup(const string & groupName);
	void ClearGroups(const Array<string> & groupsNames);
	void RemoveGroup(const string & groupName);
	void RemoveGroups(const Array<string> & groupsNames);

	void ResetGroupComponents(const string & groupName, const Array<integer> & componentsTypesIDs) const;

	template<typename T>
	void EnableGroupComponents(const string & groupName, const Array<integer> & componentsTypesIDs);

	template<typename T>
	void DisableGroupComponents(const string & groupName, const Array<integer> & componentsTypesIDs);

	template<typename T>
	void EnableGroupDerivedComponents(const string & groupName);

	template<typename T>
	void DisableGroupDerivedComponents(const string & groupName);

	void Clear();

	SharedPtr<BaseSprite> CreateEntityBaseSprite(const string & pathToEntity, const string & domainName,
	                                             const string & layerName) const;

	template<typename T>
	T GetComponentParameter(const string & pathToEntity, const string & componentType, const string & pathToParameter) const;

	template<typename T>
	static KeyValuePair<string, CreatingComponentDelegate> CreateComponentDelegate();

	integer GetCount(const string & groupName) const;
	integer GetCount() const;
	const TreeMap<string, List<SharedPtr<Entity>>> & GetEntities() const;

	const Array<string> & GetGroupsNames() const;
	const Array<string> & GetSavingGroupsNames() const;

	void PrintEntities() const;
	void PrintEntities(const string & groupName) const;
	void PrintEntity(const string & entityName) const;
	void PrintCachedEntities() const;
	void PrintCachedEntities(const string & groupName) const;

	integer GetEntitiesCount() const;
	integer GetEntitiesInPoolCount() const;
	integer GetTotalEntitiesCount() const;

private:
	static const string Name;

	static const string PathToType;
	static const string PathToGroupName;
	static const string PathToComponentType;
	static const string PathToPathToFile;
	static const string PathToPathToEntity;
	static const string PathToPosition;
	static const string PathToComponent;

	SharedPtr<Entity> InternalCreateEntity(const string & pathToEntity, const string & domainName,
	                                       const string & layerName, const Vector3 & position,
	                                       float angle, const Size & size, List<string> & marks);

	SharedPtr<Entity> CreateNewEntity(const string & pathToEntity, const string & entityType, const string & groupName,
	                                  const string & domainName, const string & layerName,
	                                  const Vector3 & position, float angle, const Size & size, List<string> & marks);

	SharedPtr<Entity> GetEntityFromPool(const string & entityType, const string & groupName, const string & domainName,
	                                    const string & layerName, const Vector3 & position, float angle, const Size & size,
	                                    List<string> & marks);

	bool IsPoolContains(const string & entityType, const string & groupName) const;

	void InternalAddEntity(const SharedPtr<Entity> & entity, const string & domainName);
	void IntervalRemoveEntityWithoutPooling(const SharedPtr<Entity> & entity);

	void AddComponents(const SharedPtr<Entity> & entity, const string & domainName, const WeakPtr<ParametersFile> & file);

	template<typename ComponentType>
	void AddComponent(const SharedPtr<Entity> & entity, const string & domainName,
	                  const WeakPtr<ParametersFile> & file, integer componentIndexInFile) const;

	template<typename ComponentType>
	SharedPtr<ComponentType> CreateComponent(const string & domainName, const WeakPtr<ParametersFile> & file,
	                                         integer componentIndexInFile) const;

	void PutToPool(const SharedPtr<Entity> & entity);
	SharedPtr<Entity> GetFromPool(const string & entityType, const string & groupName, const string & domainName);

	SharedPtr<EntityManagerEntityParametersItem> GetEntityParametersItem(const string & pathToEntity);

	void UpdateAllEntitiesCount();

	void RegisterCommands();
	void UnregisterCommands();

	TreeMap<string, CreatingComponentDelegate> _componentsDelegates;

	Array<string> _groupsNames;
	Array<string> _savingGroupsNames;

	//тип сущности, путь к файлу
	TreeMap<string, string> _pathsToEntities;
	//имя группы, список сущностей
	TreeMap<string, List<SharedPtr<Entity>>> _entities;
	//имя группы, имя типа, список сущностей
	TreeMap<string, TreeMap<string, List<SharedPtr<Entity>>>> _entitiesPool;
	//путь до Entity, EntityManagerEntityParametersItem
	TreeMap<string, SharedPtr<EntityManagerEntityParametersItem>> _entitiesParametersCache;
	//имя типа, список EntityManagerPoolItem
	TreeMap<string, List<SharedPtr<EntityManagerPoolItem>>> _entitiesChildrenCache;

	integer _entitiesCount;
	integer _entitiesPoolCount;
	integer _totalEntitiesCount;

	bool _isShowingPoolWarningMessages;

protected:
	InnerEvent<const EntityManagerEventArgs &> _entityCreatedEvent;
	InnerEvent<const EntityManagerEventArgs &> _entityRemovedEvent;

public:
	Event<const EntityManagerEventArgs &> EntityCreatedEvent;
	Event<const EntityManagerEventArgs &> EntityRemovedEvent;

private:
	EntityManagerSingleton(const EntityManagerSingleton &) = delete;
	EntityManagerSingleton & operator =(const EntityManagerSingleton &) = delete;

	friend class TestEntityManagerAfterInitialization;
	friend class TestEntityManagerCreateAndRemoveEntity;
	friend class TestEntityManagerAddEntityAndRemoveEntityWithoutPooling;
	friend class TestEntityManagerReserveEntities;
	friend class TestEntityManagerClear;
};

//===========================================================================//

extern SharedPtr<EntityManagerSingleton> EntityManager;

//===========================================================================//

template<typename T>
void EntityManagerSingleton::EnableGroupComponents(const string & groupName, const Array<integer> & componentsTypesIDs)
{
	const List<SharedPtr<Entity>> & group = GetGroup(groupName);
	for (const auto & entity : group)
	{
		EntityHelper::EnableEntityComponents<T>(entity, componentsTypesIDs);
	}
}

//===========================================================================//

template<typename T>
void EntityManagerSingleton::DisableGroupComponents(const string & groupName, const Array<integer> & componentsTypesIDs)
{
	const List<SharedPtr<Entity>> & group = GetGroup(groupName);
	for (const auto & entity : group)
	{
		EntityHelper::DisableEntityComponents<T>(entity, componentsTypesIDs);
	}
}

//===========================================================================//

template<typename T>
void EntityManagerSingleton::EnableGroupDerivedComponents(const string & groupName)
{
	const List<SharedPtr<Entity>> & group = GetGroup(groupName);
	for (const auto & entity : group)
	{
		EntityHelper::EnableEntityDerivedComponents<T>(entity);
	}
}

//===========================================================================//

template<typename T>
void EntityManagerSingleton::DisableGroupDerivedComponents(const string & groupName)
{
	const List<SharedPtr<Entity>> & group = GetGroup(groupName);
	for (const auto & entity : group)
	{
		EntityHelper::DisableEntityDerivedComponents<T>(entity);
	}
}

//===========================================================================//

template<typename T>
T EntityManagerSingleton::GetComponentParameter(const string & pathToEntity, const string & componentType,
                                                const string & pathToParameter) const
{
	WeakPtr<ParametersFile> file = ParametersFileManager->Get(PathHelper::GetFullPathToEntityFile(pathToEntity));
	integer componentsCount = file->GetListChildren(PathHelper::PathToComponentsSection);
	for (integer i = 0; i < componentsCount; ++i)
	{
		const string currentComponentType = file->Get<string>(string::Format("{0}.{1}.{2}", PathHelper::PathToComponentsSection, i, PathToComponentType));
		if (currentComponentType == componentType)
		{
			SharedPtr<ComponentDataProvider> componentDataProvider = new ComponentDataProvider(file, i, componentType);
			return componentDataProvider->Get<T>(pathToParameter);
		}
	}

	return T();
}

//===========================================================================//

template<typename T>
KeyValuePair<string, EntityManagerSingleton::CreatingComponentDelegate> EntityManagerSingleton::CreateComponentDelegate()
{
	return {T::TypeName, &EntityManagerSingleton::CreateComponent<T> };
}

//===========================================================================//

template<typename ComponentType>
void EntityManagerSingleton::AddComponent(const SharedPtr<Entity> & entity, const string & domainName,
                                          const WeakPtr<ParametersFile> & file, integer componentIndexInFile) const
{
	SharedPtr<ComponentDataProvider> componentDataProvider = new ComponentDataProvider(file, componentIndexInFile, ComponentType::TypeName);
	SharedPtr<ComponentType> component = new ComponentType(domainName, componentDataProvider);
	entity->AddComponent(component);
}

//===========================================================================//

template<typename ComponentType>
SharedPtr<ComponentType> EntityManagerSingleton::CreateComponent(const string & domainName, const WeakPtr<ParametersFile> & file,
                                                                 integer componentIndexInFile) const
{
	string pathToPathToComponent = string::Format("{0}.{1}.{2}", PathHelper::PathToComponentsSection, componentIndexInFile, PathToComponent);
	if (file->ContainsParameter(pathToPathToComponent))
	{
		string pathToComponentFile = file->Get<string>(pathToPathToComponent);
		const WeakPtr<ParametersFile> & componentFile = ParametersFileManager->Get(pathToComponentFile);
		return new ComponentType(domainName, componentFile);
	}
	else
	{
		SharedPtr<ComponentDataProvider> componentDataProvider = new ComponentDataProvider(file, componentIndexInFile, ComponentType::TypeName);
		return new ComponentType(domainName, componentDataProvider);
	}
}

//===========================================================================//

} // namespace Bru

#endif // ENTITY_MANAGER_H

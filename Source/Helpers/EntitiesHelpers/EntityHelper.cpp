//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityHelper.h"

#include "../CoordsHelper/CoordsHelper.h"
#include "../../Utils/PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

const float EntityHelper::DefaultAngle = 0.0f;
const Size EntityHelper::DefaultSize = Size(-1.0f, -1.0f);

//===========================================================================//

bool EntityHelper::IsEntityContainsPoint(const SharedPtr<Entity> & entity, const Vector3 & point)
{
	Vector3 adaptedPoint = CoordsHelper::GetProjectedOnEntityPoint(entity, point);

	const auto & node = entity->GetComponent<PositioningNode>();
	const auto & sprite = entity->GetComponent<BaseSprite>();

	Vector3 entityPosition = node->GetPosition() - sprite->GetOrigin();

	return entityPosition.X <= adaptedPoint.X && adaptedPoint.X <= entityPosition.X + sprite->GetWidth() &&
	       entityPosition.Y <= adaptedPoint.Y && adaptedPoint.Y <= entityPosition.Y + sprite->GetHeight();
}

//===========================================================================//

float EntityHelper::GetDistance(const SharedPtr<Entity> & first, const SharedPtr<Entity> & second)
{
	return first->GetComponent<PositioningNode>()->GetPosition().DistanceTo(
	    second->GetComponent<PositioningNode>()->GetPosition());
}

//===========================================================================//

void EntityHelper::ShowEntity(const SharedPtr<Entity> & entity)
{
	const auto & renderable = entity->GetComponent<Renderable>();
	if (renderable != NullPtr)
	{
		renderable->Show();
	}

	for (const auto & child : entity->GetChildren())
	{
		ShowEntity(child);
	}
}

//===========================================================================//

void EntityHelper::HideEntity(const SharedPtr<Entity> & entity)
{
	const auto & renderable = entity->GetComponent<Renderable>();
	if (renderable != NullPtr)
	{
		renderable->Hide();
	}

	for (const auto & child : entity->GetChildren())
	{
		HideEntity(child);
	}
}

//===========================================================================//

bool EntityHelper::IsEntityVisible(const SharedPtr<Entity> & entity)
{
	const auto & renderable = entity->GetComponent<Renderable>();
	return renderable != NullPtr ? renderable->IsVisible() : false;
}

//===========================================================================//

string EntityHelper::GetEntityLayerName(const SharedPtr<Entity> & entity)
{
	const auto & renderable = entity->GetComponent<Renderable>();
	return renderable != NullPtr ? renderable->GetLayer()->GetName() : string::Empty;
}

//===========================================================================//

void EntityHelper::SetEntityRenderingOrder(const SharedPtr<Entity> & entity, float renderingOrder)
{
	const auto & renderable = entity->GetComponent<Renderable>();
	if (renderable != NullPtr)
	{
		renderable->SetRenderingOrder(renderingOrder);
	}
}

//===========================================================================//

float EntityHelper::GetEntityRenderingOrder(const SharedPtr<Entity> & entity)
{
	const auto & renderable = entity->GetComponent<Renderable>();
	return renderable != NullPtr ? renderable->GetRenderingOrder() : 0.0f;
}

//===========================================================================//

void EntityHelper::FlipEntityHorizontal(const SharedPtr<Entity> & entity)
{
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	if (baseSprite != NullPtr)
	{
		baseSprite->FlipHorizontal();
	}
}

//===========================================================================//

void EntityHelper::UnflipEntityHorizontal(const SharedPtr<Entity> & entity)
{
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	if (baseSprite != NullPtr)
	{
		baseSprite->UnflipHorizontal();
	}
}

//===========================================================================//

bool EntityHelper::IsEntityFlippedHorizontal(const SharedPtr<Entity> & entity)
{
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	return baseSprite != NullPtr ? baseSprite->IsFlippedHorizontal() : false;
}

//===========================================================================//

void EntityHelper::FlipEntityVertical(const SharedPtr<Entity> & entity)
{
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	if (baseSprite != NullPtr)
	{
		baseSprite->FlipVertical();
	}
}

//===========================================================================//

void EntityHelper::UnflipEntityVertical(const SharedPtr<Entity> & entity)
{
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	if (baseSprite != NullPtr)
	{
		baseSprite->UnflipVertical();
	}
}

//===========================================================================//

bool EntityHelper::IsEntityFlippedVertical(const SharedPtr<Entity> & entity)
{
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	return baseSprite != NullPtr ? baseSprite->IsFlippedVertical() : false;
}

//===========================================================================//

void EntityHelper::SetEntityAngle(const SharedPtr<Entity> & entity, float angle)
{
	if (entity->ContainsComponent(RotationNode::TypeID))
	{
		entity->GetComponent<RotationNode>()->SetAngleZ(angle);
	}
	else if (entity->ContainsComponent(PositioningNode::TypeID))
	{
		entity->GetComponent<PositioningNode>()->SetAngleZ(angle);
	}
}

//===========================================================================//

float EntityHelper::GetEntityAngle(const SharedPtr<Entity> & entity)
{
	if (entity->ContainsComponent(RotationNode::TypeID))
	{
		return entity->GetComponent<RotationNode>()->GetAngleZ();
	}
	else if (entity->ContainsComponent(PositioningNode::TypeID))
	{
		return entity->GetComponent<PositioningNode>()->GetAngleZ();
	}

	return 0.0f;
}

//===========================================================================//

void EntityHelper::ApplyParameters(const SharedPtr<Entity> & entity, const string & layerName,
                                   const Vector3 & position, float angle, const Size & size)
{
	const auto & renderable = entity->GetComponent<Renderable>();
	if (renderable != NullPtr)
	{
		renderable->SetLayer(layerName, true);
	}

	if (size != DefaultSize)
	{
		const auto & sprite = entity->GetComponent<BaseSprite>();
		if (sprite != NullPtr)
		{
			sprite->SetSize(size);
		}
	}

	const auto & positioningNode = entity->GetComponent<PositioningNode>();
	if (positioningNode != NullPtr)
	{
		positioningNode->SetPosition(position);
	}

	const auto & rotationNode = entity->GetComponent<RotationNode>();
	if (rotationNode != NullPtr)
	{
		rotationNode->SetAngleZ(angle);
	}
}

//===========================================================================//

Array<string> EntityHelper::GetEntitiesFilesPathes(const Array<string> & filter)
{
	Array<string> filesPathes = FileSystem::GetFilesListRecursively(PathHelper::PathToData + PathHelper::PathToEntities);

	integer i = 0;
	while (i < filesPathes.GetCount())
	{
		bool isFound = false;
		for (const string & filterItem : filter)
		{
			if (filesPathes[i].FirstIndexOf(filterItem) == 0)
			{
				isFound = true;
				break;
			}
		}

		if (!isFound)
		{
			filesPathes.RemoveAt(i);
		}
		else
		{
			++i;
		}
	}

	return filesPathes;
}

//===========================================================================//

} // namespace Bru
//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityGenerator.h"

#include "../../ScriptSystem/ScriptSystem.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

SharedPtr<EntityGeneratorSingleton> EntityGenerator;

//===========================================================================//

const string EntityGeneratorSingleton::Name = "Entity generator";

//===========================================================================//

EntityGeneratorSingleton::EntityGeneratorSingleton() :
	_entityGenerated(),
	EntityGenerated(_entityGenerated)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	RegisterCommands();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));	
}

//===========================================================================//

EntityGeneratorSingleton::~EntityGeneratorSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	UnregisterCommands();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));	
}

//===========================================================================//

void EntityGeneratorSingleton::GenerateSprite(const string & path, const string & groupName) const
{
	Image image(path);
	GenerateSprite(path, groupName, image.GetWidth(), image.GetHeight());
}

//===========================================================================//

void EntityGeneratorSingleton::GenerateSprite(const string & path, const string & groupName, integer width, integer height) const
{
	GenerateTextureInfoFile(path);

	SharedPtr<ParametersFile> spriteFile = new ParametersFile(PathHelper::GetFullPathToSpriteFile(path), FileAccess::Write);
	Size size = GetSizeInCoords(width, height);
	spriteFile->Set("Width", size.Width);
	spriteFile->Set("Height", size.Height);
	spriteFile->Set("OriginFactor", Vector2(0.5f, 0.5f));
	spriteFile->Set("Color", Color(255, 255, 255, 255));
	spriteFile->Set("Texture.Path", path);
	spriteFile->Set("Texture.U0", 0.0f);
	spriteFile->Set("Texture.V0", 0.0f);
	spriteFile->Set("Texture.U1", 1.0f);
	spriteFile->Set("Texture.V1", 1.0f);
	spriteFile->Save();

	GenerateEntityFile(path, groupName, "Sprite");

	_entityGenerated.Fire(EmptyEventArgs());
}

//===========================================================================//

void EntityGeneratorSingleton::GenerateAnimatedSprite(const string & path, const string & groupName,
                                                      integer framesByWidth, integer framesByHeight) const
{
	Image image(path);
	integer frameWidth = image.GetWidth() / framesByWidth;
	integer frameHeight = image.GetHeight() / framesByHeight;
	GenerateAnimatedSprite(path, groupName, frameWidth, frameHeight, framesByWidth, framesByHeight);
}

//===========================================================================//

void EntityGeneratorSingleton::GenerateAnimatedSprite(const string & path, const string & groupName,
                                                      integer frameWidth, integer frameHeight, integer framesByWidth, integer framesByHeight) const
{
	GenerateTextureInfoFile(path);

	SharedPtr<ParametersFile> spriteFile = new ParametersFile(PathHelper::GetFullPathToAnimatedSpriteFile(path), FileAccess::Write);
	Size size = GetSizeInCoords(frameWidth, frameHeight);
	spriteFile->Set("Width", size.Width);
	spriteFile->Set("Height", size.Height);
	spriteFile->Set("OriginFactor", Vector2(0.5f, 0.5f));
	spriteFile->Set("Color", Color(255, 255, 255, 255));
	spriteFile->Set("Textures.0.Path", path);
	spriteFile->Set("Textures.0.FramesByWidth", framesByWidth);
	spriteFile->Set("Textures.0.FramesByHeight", framesByHeight);
	spriteFile->Set("Animations.0.Name", string("AnimationName"));
	spriteFile->Set("Animations.0.FirstFrameIndex", 0);
	spriteFile->Set("Animations.0.LastFrameIndex", framesByWidth * framesByHeight - 1);
	spriteFile->Set("Animations.0.FrameRate", 24);
	spriteFile->Set("Animations.0.IsLooped", true);
	spriteFile->Set("Animations.0.PlayingMode", 0);
	spriteFile->Save();

	GenerateEntityFile(path, groupName, "AnimatedSprite");

	_entityGenerated.Fire(EmptyEventArgs());
}

//===========================================================================//

void EntityGeneratorSingleton::GenerateTextureInfoFile(const string & path) const
{
	SharedPtr<ParametersFile> textureFile = new ParametersFile(PathHelper::GetFullPathToTextureFile(path), FileAccess::Write);
	textureFile->Set("IsMipMap", false);
	textureFile->Set("IsSmooth", true);
	textureFile->Set("BorderType", 1);
	textureFile->Save();
}

//===========================================================================//

void EntityGeneratorSingleton::GenerateEntityFile(const string & path, const string & groupName, const string & renderableType) const
{
	SharedPtr<ParametersFile> entityFile = new ParametersFile(PathHelper::PathToEntities + path, FileAccess::Write);
	string type = PathHelper::GetFileName(path);
	entityFile->Set("Type", type);
	entityFile->Set("GroupName", groupName);
	entityFile->Set("Components.0.Type", string("PositioningNode"));
	entityFile->Set("Components.1.Type", string("RotationNode"));
	entityFile->Set("Components.2.Type", renderableType);
	entityFile->Set("Components.2.PathToFile", path);
	entityFile->Save();
}

//===========================================================================//

Size EntityGeneratorSingleton::GetSizeInCoords(integer width, integer height) const
{
	float widthInCoords = width / Renderer->GetSurfaceCoordsWidth();
	float heightInCoords = height / Renderer->GetSurfaceCoordsWidth(); //Тут не ошибка, привязываемся только к одной стороне
	return Size(widthInCoords, heightInCoords);
}

//===========================================================================//

void EntityGeneratorSingleton::RegisterCommands()
{
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityGeneratorSingleton, void, const string &, const string &>(
	                                  "GenerateSprite", this, &EntityGeneratorSingleton::GenerateSprite));
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityGeneratorSingleton, void, const string &, const string &, integer, integer>(
	                                  "GenerateSprite", this, &EntityGeneratorSingleton::GenerateSprite));
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityGeneratorSingleton, void, const string &, const string &, integer, integer>(
	                                  "GenerateAnimatedSprite", this, &EntityGeneratorSingleton::GenerateAnimatedSprite));
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityGeneratorSingleton, void, const string &, const string &, integer, integer, integer, integer>(
	                                  "GenerateAnimatedSprite", this, &EntityGeneratorSingleton::GenerateAnimatedSprite));
}

//===========================================================================//

void EntityGeneratorSingleton::UnregisterCommands()
{
	ScriptSystem->UnregisterCommand("GenerateSprite");
	ScriptSystem->UnregisterCommand("GenerateAnimatedSprite");
}

//===========================================================================//

} // namespace Bru

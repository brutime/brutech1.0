//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityManager.h"

namespace Bru
{

//===========================================================================//

SharedPtr<EntityManagerSingleton> EntityManager;

//===========================================================================//

const string EntityManagerSingleton::PoolDomainName = "Pool";
const string EntityManagerSingleton::PoolLayerName = "Pool";

const string EntityManagerSingleton::PurityLogicGroupName = "PurityLogic";

const string EntityManagerSingleton::Name = "Entity manager";

const string EntityManagerSingleton::PathToType = "Type";
const string EntityManagerSingleton::PathToGroupName = "GroupName";
const string EntityManagerSingleton::PathToComponentType = "Type";
const string EntityManagerSingleton::PathToPathToFile = "PathToFile";
const string EntityManagerSingleton::PathToPathToEntity = "PathToEntity";
const string EntityManagerSingleton::PathToPosition = "Position";
const string EntityManagerSingleton::PathToComponent = "PathToComponent";

//===========================================================================//

EntityManagerSingleton::EntityManagerSingleton(TreeMap<string, CreatingComponentDelegate> componentsDelegates,
                                               const Array<string> & groupsNames, const Array<string> & savingGroupsNames,
                                               bool isShowingPoolWarningMessages) :
	_componentsDelegates(componentsDelegates),
	_groupsNames(groupsNames),
	_savingGroupsNames(savingGroupsNames),
	_pathsToEntities(),
	_entities(),
	_entitiesPool(),
	_entitiesParametersCache(),
	_entitiesChildrenCache(),
	_entitiesCount(0),
	_entitiesPoolCount(0),
	_totalEntitiesCount(0),
	_isShowingPoolWarningMessages(isShowingPoolWarningMessages),
	_entityCreatedEvent(),
	_entityRemovedEvent(),
	EntityCreatedEvent(_entityCreatedEvent),
	EntityRemovedEvent(_entityRemovedEvent)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	//Добавляем слой для закешированных заранее entities
	DomainManager->Add(PoolDomainName);
	Renderer->AddLayerToBack(PoolDomainName, new RenderableLayer(PoolLayerName));

	RegisterCommands();

	Watcher->RegisterVariable("Entities count", _entitiesCount);
	Watcher->RegisterVariable("Entities in the pool count", _entitiesPoolCount);
	Watcher->RegisterVariable("Total entities count", _totalEntitiesCount);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

EntityManagerSingleton::~EntityManagerSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	Watcher->UnregisterVariableOrExpression("Entities count");
	Watcher->UnregisterVariableOrExpression("Cached entities count");

	UnregisterCommands();

	Clear();

	Renderer->RemoveLayer(PoolDomainName, PoolLayerName);
	DomainManager->Remove(PoolDomainName);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void EntityManagerSingleton::AddEntity(const SharedPtr<Entity> & entity, const string & domainName)
{
	InternalAddEntity(entity, domainName);
	UpdateAllEntitiesCount();
	_entityCreatedEvent.Fire(EntityManagerEventArgs(entity));
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::CreateEntity(const string & pathToEntity, const string & domainName,
                                                       const string & layerName, const Vector3 & position, float angle, const Size & size)
{
	List<string> marks;
	SharedPtr<Entity> entity = InternalCreateEntity(pathToEntity, domainName, layerName, position, angle, size, marks);
	UpdateAllEntitiesCount();
	_entityCreatedEvent.Fire(EntityManagerEventArgs(entity));
	return entity;
}

//===========================================================================//

void EntityManagerSingleton::ReserveEntities(const string & pathToEntity, integer count)
{
	auto entityParametersItem = GetEntityParametersItem(pathToEntity);
	const string & entityType = entityParametersItem->Type;
	const string & groupName = entityParametersItem->GroupName;

	for (integer i = 0; i < count; ++i)
	{
		List<string> marks;
		PutToPool(CreateNewEntity(pathToEntity, entityType, groupName, PoolDomainName, PoolLayerName, {}, 0.0f, EntityHelper::DefaultSize, marks));
	}

	UpdateAllEntitiesCount();
}

//===========================================================================//

bool EntityManagerSingleton::ContainsEntity(const SharedPtr<Entity> & entity)
{
	if (_entities.Contains(entity->GetGroupName()))
	{
		return _entities[entity->GetGroupName()].Contains(entity);
	}

	return false;
}

//===========================================================================//

void EntityManagerSingleton::RemoveEntity(const SharedPtr<Entity> & entity)
{
	if (entity->GetParent() != NullPtr)
	{
		entity->GetParent()->RemoveChild(entity);
	}

	EntityManagerEventArgs args(entity);
	PutToPool(entity);
	UpdateAllEntitiesCount();
	_entityRemovedEvent.Fire(args);
}

//===========================================================================//

void EntityManagerSingleton::RemoveEntityWithoutPooling(const SharedPtr<Entity> & entity)
{
	if (entity->GetParent() != NullPtr)
	{
		entity->GetParent()->RemoveChild(entity);
	}

	EntityManagerEventArgs args(entity);
	IntervalRemoveEntityWithoutPooling(entity);
	UpdateAllEntitiesCount();
	_entityRemovedEvent.Fire(args);
}

//===========================================================================//

void EntityManagerSingleton::AddGroup(const string & groupName)
{
	_entities.Add(groupName, List<SharedPtr<Entity>>());
	_entitiesPool.Add(groupName, TreeMap<string, List<SharedPtr<Entity>>>());
}

//===========================================================================//

void EntityManagerSingleton::AddGroups(const Array<string> & groupsNames)
{
	for (const auto & groupName : groupsNames)
	{
		AddGroup(groupName);
	}
}

//===========================================================================//

const List<SharedPtr<Entity>> & EntityManagerSingleton::GetGroup(const string &groupName) const
{
	return _entities[groupName];
}

//===========================================================================//

bool EntityManagerSingleton::ContainsGroup(const string & groupName) const
{
	return _entities.Contains(groupName);
}

//===========================================================================//

bool EntityManagerSingleton::IsGroupEmpty(const string & groupName)
{
	return _entities[groupName].IsEmpty();
}

//===========================================================================//

void EntityManagerSingleton::ClearGroup(const string & groupName)
{
	auto group = GetGroup(groupName);
	for (const auto & entity : group)
	{
		RemoveEntityWithoutPooling(entity);
	}

	_entitiesPool[groupName].Clear();
}

//===========================================================================//

void EntityManagerSingleton::ClearGroups(const Array<string> & groupsNames)
{
	for (const auto & groupName : groupsNames)
	{
		ClearGroup(groupName);
	}
}

//===========================================================================//

void EntityManagerSingleton::RemoveGroup(const string & groupName)
{
#ifdef DEBUGGING
	if (!IsGroupEmpty(groupName))
	{
		PrintEntities(groupName);
		INVALID_STATE_EXCEPTION(string::Format("{0} trying to remove not empty group: \"{1}\"", Name, groupName));
	}
#endif // DEBUGGING

	_entities.Remove(groupName);
	_entitiesPool.Remove(groupName);
}

//===========================================================================//

void EntityManagerSingleton::RemoveGroups(const Array<string> & groupsNames)
{
	for (const auto & groupName : groupsNames)
	{
		RemoveGroup(groupName);
	}
}

//===========================================================================//

void EntityManagerSingleton::ResetGroupComponents(const string & groupName, const Array<integer> & componentsTypesIDs) const
{
	const List<SharedPtr<Entity>> & group = GetGroup(groupName);
	for (const SharedPtr<Entity> & entity : group)
	{
		for (const auto & componentsTypeID : componentsTypesIDs)
		{
			if (entity->ContainsComponent(componentsTypeID))
			{
				entity->GetComponent<EntityComponent>(componentsTypeID)->Reset();
			}
		}
	}
}

//===========================================================================//

void EntityManagerSingleton::Clear()
{
	_pathsToEntities.Clear();
	_entities.Clear();
	_entitiesPool.Clear();
	_entitiesParametersCache.Clear();
	_entitiesChildrenCache.Clear();
	UpdateAllEntitiesCount();
}

//===========================================================================//

SharedPtr<BaseSprite> EntityManagerSingleton::CreateEntityBaseSprite(const string & pathToEntity, const string & domainName,
                                                                     const string & layerName) const
{
	WeakPtr<ParametersFile> file = ParametersFileManager->Get(PathHelper::GetFullPathToEntityFile(pathToEntity));
	integer componentsCount = file->GetListChildren(PathHelper::PathToComponentsSection);
	for (integer i = 0; i < componentsCount; ++i)
	{
		string componentType = file->Get<string>(string::Format(PathHelper::PathToComponentsSection + ".{0}." + PathToComponentType, i));
		if (componentType == Sprite::TypeName || componentType == AnimatedSprite::TypeName)
		{
			string pathToFile = file->Get<string>(string::Format(PathHelper::PathToComponentsSection + ".{0}." + PathToPathToFile, i));
			if (componentType == Sprite::TypeName)
			{
				return new Sprite(domainName, layerName, pathToFile);
			}

			return new AnimatedSprite(domainName, layerName, pathToFile);
		}
	}

	return NullPtr;
}

//===========================================================================//

string EntityManagerSingleton::GetPathToEntity(const string & type)
{
	return _pathsToEntities[type];
}

//===========================================================================//

integer EntityManagerSingleton::GetCount(const string & groupName) const
{
	return _entities[groupName].GetCount();
}

//===========================================================================//

integer EntityManagerSingleton::GetCount() const
{
	integer totalCount = 0;
	for (const auto & pair : _entities)
	{
		totalCount += pair.GetValue().GetCount();
	}
	return totalCount;
}

//===========================================================================//

const TreeMap<string, List<SharedPtr<Entity>>> & EntityManagerSingleton::GetEntities() const
{
	return _entities;
}

//===========================================================================//

const Array<string> & EntityManagerSingleton::GetGroupsNames() const
{
	return _groupsNames;
}

//===========================================================================//

const Array<string> & EntityManagerSingleton::GetSavingGroupsNames() const
{
	return _savingGroupsNames;
}

//===========================================================================//

void EntityManagerSingleton::PrintEntities() const
{
	Console->Hint("Entities:");
	Console->AddOffset(2);
	for (const auto & entityPair : _entities)
	{
		Console->Hint(string::Format("{0}:", entityPair.GetKey()));
		Console->AddOffset(2);
		for (const SharedPtr<Entity> & entity : entityPair.GetValue())
		{
			Console->Hint(string::Format("- {0}", entity->GetName()));
		}
		Console->ReduceOffset(2);
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void EntityManagerSingleton::PrintEntities(const string & groupName) const
{
	Console->Hint(string::Format("Entities (\"{0}\"):", groupName));
	Console->AddOffset(2);
	for (const SharedPtr<Entity> & entity : _entities[groupName])
	{
		Console->Hint(string::Format("- {0}", entity->GetName()));
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void EntityManagerSingleton::PrintEntity(const string & entityName) const
{
	SharedPtr<Entity> entity = NullPtr;
	for (const auto & entityPair : _entities)
	{
		bool isFound = false;
		for (const auto & currentEntity : entityPair.GetValue())
		{
			if (entityName == currentEntity->GetName())
			{
				entity = currentEntity;
				isFound = true;
				break;
			}
		}

		if (isFound)
		{
			break;
		}
	}

	if (entity == NullPtr)
	{
		Console->Error(string::Format("Entity {0} not found", entityName));
		return;
	}

	entity->PrintInfo();
}

//===========================================================================//

void EntityManagerSingleton::PrintCachedEntities() const
{
	Console->Hint("Cached entities:");
	Console->AddOffset(2);
	for (const auto & groupPair : _entitiesPool)
	{
		Console->Hint(string::Format("{0}:", groupPair.GetKey()));
		Console->AddOffset(2);
		for (auto & entityPair : groupPair.GetValue())
		{
			for (const auto & entity : entityPair.GetValue())
			{
				Console->Hint(string::Format("- {0}", entity->GetName()));
			}
		}
		Console->ReduceOffset(2);
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void EntityManagerSingleton::PrintCachedEntities(const string & groupName) const
{
	Console->Hint(string::Format("Cached entities ({0}):", groupName));
	Console->AddOffset(2);
	for (const auto & entityPair : _entitiesPool[groupName])
	{
		for (const auto & entity : entityPair.GetValue())
		{
			Console->Hint(string::Format("- {0}", entity->GetName()));
		}
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

integer EntityManagerSingleton::GetEntitiesCount() const
{
	return _entitiesCount;
}

//===========================================================================//

integer EntityManagerSingleton::GetEntitiesInPoolCount() const
{
	return _entitiesPoolCount;
}

//===========================================================================//

integer EntityManagerSingleton::GetTotalEntitiesCount() const
{
	return _totalEntitiesCount;
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::InternalCreateEntity(const string & pathToEntity, const string & domainName,
                                                               const string & layerName, const Vector3 & position,
                                                               float angle, const Size & size, List<string> & marks)
{
	auto entityParametersItem = GetEntityParametersItem(pathToEntity);
	const string & entityType = entityParametersItem->Type;
	const string & groupName = entityParametersItem->GroupName;

	if (IsPoolContains(entityType, groupName))
	{
		return GetEntityFromPool(entityType, groupName, domainName, layerName, position, angle, size, marks);
	}
	else
	{
		if (_isShowingPoolWarningMessages)
		{
			Console->Warning("Not found entity in pool. Type: \"{0}\", group name: \"{1}\"", entityType, groupName);
		}
		return CreateNewEntity(pathToEntity, entityType, groupName, domainName, layerName, position, angle, size, marks);
	}
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::CreateNewEntity(const string & pathToEntity, const string & entityType,
                                                          const string & groupName, const string & domainName,
                                                          const string & layerName, const Vector3 & position,
                                                          float angle, const Size & size, List<string> & marks)
{
	if (!marks.Contains(pathToEntity))
	{
		marks.Add(pathToEntity);
	}
	else
	{
		ENTITY_RECURSIVE_DECLARATION_EXCEPTION(string::Format("Recursive declaration of entity. Path to root entity: \"{0}\"", marks[0]));
	}

	if (!_pathsToEntities.Contains(entityType))
	{
		_pathsToEntities.Add(entityType, pathToEntity);
	}

	WeakPtr<ParametersFile> file = ParametersFileManager->Get(PathHelper::GetFullPathToEntityFile(pathToEntity));

	SharedPtr<Entity> entity = new Entity(entityType, groupName);
	AddComponents(entity, domainName, file);
	EntityHelper::ApplyParameters(entity, layerName, position, angle, size);

	_entities[entity->GetGroupName()].Add(entity);

	//Children
	if (!file->ContainsSection(PathHelper::PathToChildrenSection))
	{
		return entity;
	}

	integer childrenCount = file->GetListChildren(PathHelper::PathToChildrenSection);
	bool hasEntitiesChildrenForEntityType = _entitiesChildrenCache.Contains(entityType);
	if (!hasEntitiesChildrenForEntityType && childrenCount > 0)
	{
		_entitiesChildrenCache.Add(entityType, List<SharedPtr<EntityManagerPoolItem>>());
	}

	for (integer i = 0; i < childrenCount; ++i)
	{
		string pathToChildEntity = file->Get<string>(string::Format(PathHelper::PathToChildrenSection + ".{0}." + PathToPathToEntity, i));
		Vector3 childPosition = file->Get<Vector3>(string::Format(PathHelper::PathToChildrenSection + ".{0}." + PathToPosition, i));

		SharedPtr<Entity> childEntity = InternalCreateEntity(pathToChildEntity, domainName, layerName, childPosition,
		                                                     EntityHelper::DefaultAngle, EntityHelper::DefaultSize, marks);
		entity->AddChild(childEntity);

		if (!hasEntitiesChildrenForEntityType)
		{
			_entitiesChildrenCache[entityType].Add(new EntityManagerPoolItem(pathToChildEntity, childPosition));
		}
	}

	return entity;
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::GetEntityFromPool(const string & entityType, const string & groupName,
                                                            const string & domainName, const string & layerName,
                                                            const Vector3 & position, float angle, const Size & size,
                                                            List<string> & marks)
{
	auto entity = GetFromPool(entityType, groupName, domainName);
	EntityHelper::ApplyParameters(entity, layerName, position, angle, size);
	if (!_entitiesChildrenCache.Contains(entityType))
	{
		return entity;
	}

	const auto & entityChildrenCache = _entitiesChildrenCache[entityType];
	for (const auto & childInfo : entityChildrenCache)
	{
		SharedPtr<Entity> childEntity = InternalCreateEntity(childInfo->PathToEntity, domainName, layerName, childInfo->Position,
		                                                     EntityHelper::DefaultAngle, EntityHelper::DefaultSize, marks);
		entity->AddChild(childEntity);
	}

	return entity;
}

//===========================================================================//

bool EntityManagerSingleton::IsPoolContains(const string & entityType, const string & groupName) const
{
	return _entitiesPool[groupName].Contains(entityType) && _entitiesPool[groupName][entityType].GetCount() > 0;
}

//===========================================================================//

void EntityManagerSingleton::InternalAddEntity(const SharedPtr<Entity> & entity, const string & domainName)
{
	for (const SharedPtr<Entity> & entityChild : entity->GetChildren())
	{
		InternalAddEntity(entityChild, domainName);
	}

	entity->RegisterAllComponents(domainName);

	_entities[entity->GetGroupName()].Add(entity);
}

//===========================================================================//

void EntityManagerSingleton::IntervalRemoveEntityWithoutPooling(const SharedPtr<Entity> & entity)
{
	for (const SharedPtr<Entity> & entityChild : entity->GetChildren())
	{
		IntervalRemoveEntityWithoutPooling(entityChild);
	}

	entity->UnregisterAllComponents();

	_entities[entity->GetGroupName()].Remove(entity);
}

//===========================================================================//

void EntityManagerSingleton::AddComponents(const SharedPtr<Entity> & entity, const string & domainName, const WeakPtr<ParametersFile> & file)
{
	integer componentsCount = file->GetListChildren(PathHelper::PathToComponentsSection);
	for (integer i = 0; i < componentsCount; ++i)
	{
		string componentType = file->Get<string>(string::Format("{0}.{1}.{2}", PathHelper::PathToComponentsSection, i, PathToComponentType));
		if (_componentsDelegates.Contains(componentType))
		{
			entity->AddComponent(_componentsDelegates[componentType](*this, domainName, file, i));
		}
		else
		{
			INVALID_ARGUMENT_EXCEPTION(string::Format("Unkhown component type: \"{0}\"", componentType));
		}
	}
}

//===========================================================================//

void EntityManagerSingleton::PutToPool(const SharedPtr<Entity> & entity)
{
	//Тут мы разбираем иерархию entity на отдельные entity
	for (const SharedPtr<Entity> & entityChild : entity->GetChildren())
	{
		PutToPool(entityChild);
	}

	entity->PutToPool();
	entity->RemoveAllChildren();

	const string & groupName = entity->GetGroupName();
	const string & type = entity->GetType();

	if (!_entitiesPool[groupName].Contains(type))
	{
		_entitiesPool[groupName].Add(type, List<SharedPtr<Entity>>());
	}
	_entitiesPool[groupName][type].Add(entity);
	_entities[groupName].Remove(entity);
}

//===========================================================================//

SharedPtr<Entity> EntityManagerSingleton::GetFromPool(const string & entityType, const string & groupName,
                                                      const string & domainName)
{
	auto entity = _entitiesPool[groupName][entityType].GetFirst();
	entity->ReturnFromPool(domainName);

	_entities[groupName].Add(entity);
	_entitiesPool[groupName][entityType].Remove(entity);

	return entity;
}

//===========================================================================//

SharedPtr<EntityManagerEntityParametersItem> EntityManagerSingleton::GetEntityParametersItem(const string & pathToEntity)
{
	if (!_entitiesParametersCache.Contains(pathToEntity))
	{
		WeakPtr<ParametersFile> file = ParametersFileManager->Get(PathHelper::GetFullPathToEntityFile(pathToEntity));
		_entitiesParametersCache.Add(pathToEntity, new EntityManagerEntityParametersItem(file->Get<string>(PathToType), file->Get<string>(PathToGroupName)));
	}

	return _entitiesParametersCache[pathToEntity];
}

//===========================================================================//

void EntityManagerSingleton::UpdateAllEntitiesCount()
{
	_entitiesCount = 0;
	for (const auto & entityPair : _entities)
	{
		_entitiesCount += entityPair.GetValue().GetCount();
	}

	_entitiesPoolCount = 0;
	for (const auto & groupPair : _entitiesPool)
	{
		for (const auto & entityPair : groupPair.GetValue())
		{
			_entitiesPoolCount += entityPair.GetValue().GetCount();
		}
	}

	_totalEntitiesCount = _entitiesCount + _entitiesPoolCount;
}

//===========================================================================//

void EntityManagerSingleton::RegisterCommands()
{
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityManagerSingleton, void>(
	                                  "CachedEntities", this, &EntityManagerSingleton::PrintCachedEntities));
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityManagerSingleton, void, const string &>(
	                                  "CachedEntities", this, &EntityManagerSingleton::PrintCachedEntities));
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityManagerSingleton, void, const string &>(
	                                  "Entity", this, &EntityManagerSingleton::PrintEntity));
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityManagerSingleton, void>(
	                                  "Entities", this, &EntityManagerSingleton::PrintEntities));
	ScriptSystem->RegisterCommand(new ClassConstCommand<EntityManagerSingleton, void, const string &>(
	                                  "Entities", this, &EntityManagerSingleton::PrintEntities));
}

//===========================================================================//

void EntityManagerSingleton::UnregisterCommands()
{
	ScriptSystem->UnregisterCommand("CachedEntities");
	ScriptSystem->UnregisterCommand("Entity");
	ScriptSystem->UnregisterCommand("Entities");
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityManagerPoolItem.h"

namespace Bru
{

//===========================================================================//

EntityManagerPoolItem::EntityManagerPoolItem(const string & pathToEntity, const Vector3 & position) :
	PathToEntity(pathToEntity),
	Position(position)
{
}

//===========================================================================//

EntityManagerPoolItem::~EntityManagerPoolItem()
{
}

//===========================================================================//

} // namespace Bru
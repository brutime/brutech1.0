//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef ENTITY_MANAGER_ENTITY_PARAMETERS_ITEM_H
#define ENTITY_MANAGER_ENTITY_PARAMETERS_ITEM_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

struct EntityManagerEntityParametersItem
{
	EntityManagerEntityParametersItem(const string & type, const string & groupName);
	~EntityManagerEntityParametersItem();
	
	string Type;
	string GroupName;
};

//===========================================================================//

} // namespace Bru

#endif // ENTITY_MANAGER_ENTITY_PARAMETERS_ITEM_H
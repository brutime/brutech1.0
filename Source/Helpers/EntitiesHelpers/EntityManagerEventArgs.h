//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENTITY_MANAGER_EVENT_ARGS_H
#define ENTITY_MANAGER_EVENT_ARGS_H

#include "../../EntitySystem/EntitySystem.h"

namespace Bru
{

//===========================================================================//

struct EntityManagerEventArgs
{
	EntityManagerEventArgs(const WeakPtr<Entity> & changedEntity);
	EntityManagerEventArgs(const EntityManagerEventArgs & other);
	~EntityManagerEventArgs();
	EntityManagerEventArgs & operator =(const EntityManagerEventArgs & other);

	WeakPtr<Entity> ChangedEntity;
};

//===========================================================================//

} // namespace Bru

#endif // ENTITY_MANAGER_EVENT_ARGS_H

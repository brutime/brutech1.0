//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef ENTITY_MANAGER_POOL_ITEM_H
#define ENTITY_MANAGER_POOL_ITEM_H

#include "../../Common/Common.h"
#include "../../Math/Math.h"

namespace Bru
{

//===========================================================================//

struct EntityManagerPoolItem
{
	EntityManagerPoolItem(const string & pathToEntity, const Vector3 & position);
	~EntityManagerPoolItem();
	
	string PathToEntity;
	Vector3 Position;
};

//===========================================================================//

} // namespace Bru

#endif // ENTITY_MANAGER_POOL_ITEM_H
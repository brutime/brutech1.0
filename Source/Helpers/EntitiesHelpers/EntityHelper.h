//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENTITY_HELPER_H
#define ENTITY_HELPER_H

#include "../../Positioning/Positioning.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class EntityHelper
{
public:
	static const float DefaultAngle;
	static const Size DefaultSize;

	static bool IsEntityContainsPoint(const SharedPtr<Entity> & entity, const Vector3 & point);
	static float GetDistance(const SharedPtr<Entity> & first, const SharedPtr<Entity> & second);

	static void ShowEntity(const SharedPtr<Entity> & entity);
	static void HideEntity(const SharedPtr<Entity> & entity);
	static bool IsEntityVisible(const SharedPtr<Entity> & entity);

	static string GetEntityLayerName(const SharedPtr<Entity> & entity);
	
	static void SetEntityRenderingOrder(const SharedPtr<Entity> & entity, float renderingOrder);
	static float GetEntityRenderingOrder(const SharedPtr<Entity> & entity);	
	
	static void FlipEntityHorizontal(const SharedPtr<Entity> & entity);
	static void UnflipEntityHorizontal(const SharedPtr<Entity> & entity);
	static bool IsEntityFlippedHorizontal(const SharedPtr<Entity> & entity);
	
	static void FlipEntityVertical(const SharedPtr<Entity> & entity);
	static void UnflipEntityVertical(const SharedPtr<Entity> & entity);
	static bool IsEntityFlippedVertical(const SharedPtr<Entity> & entity);
	
	static void SetEntityAngle(const SharedPtr<Entity> & entity, float angle);
	static float GetEntityAngle(const SharedPtr<Entity> & entity);

	static void ApplyParameters(const SharedPtr<Entity> & entity, const string & layerName,
	                            const Vector3 & position, float angle, const Size & size);

	template<typename T>
	static void EnableEntityComponents(const SharedPtr<Entity> & entity, const Array<integer> & componentsTypesIDs);

	template<typename T>
	static void DisableEntityComponents(const SharedPtr<Entity> & entity, const Array<integer> & componentsTypesIDs);

	template<typename T>
	static void EnableEntityDerivedComponents(const SharedPtr<Entity> & entity);

	template<typename T>
	static void DisableEntityDerivedComponents(const SharedPtr<Entity> & entity);

	static Array<string> GetEntitiesFilesPathes(const Array<string> & filter);

private:
	EntityHelper() = delete;
	EntityHelper(const EntityHelper &) = delete;
	~EntityHelper() = delete;
	EntityHelper & operator =(const EntityHelper &) = delete;
};

//===========================================================================//

template<typename T>
void EntityHelper::EnableEntityComponents(const SharedPtr<Entity> & entity, const Array<integer> & componentsTypesIDs)
{
	for (const auto & childEntity : entity->GetChildren())
	{
		EnableEntityComponents<T>(childEntity, componentsTypesIDs);
	}
	
	for (const auto & componentsTypesID : componentsTypesIDs)
	{
		if (entity->ContainsComponent(componentsTypesID))
		{
			entity->GetComponent<T>(componentsTypesID)->Enable();
		}
	}
}

//===========================================================================//

template<typename T>
void EntityHelper::DisableEntityComponents(const SharedPtr<Entity> & entity, const Array<integer> & componentsTypesIDs)
{
	for (const auto & childEntity : entity->GetChildren())
	{
		DisableEntityComponents<T>(childEntity, componentsTypesIDs);
	}
	
	for (const auto & componentsTypesID : componentsTypesIDs)
	{
		if (entity->ContainsComponent(componentsTypesID))
		{
			entity->GetComponent<T>(componentsTypesID)->Disable();
		}
	}
}

//===========================================================================//

template<typename T>
void EntityHelper::EnableEntityDerivedComponents(const SharedPtr<Entity> & entity)
{
	for (const auto & childEntity : entity->GetChildren())
	{
		EnableEntityDerivedComponents<T>(childEntity);
	}	
	
	const auto & components = entity->GetComponents();
	for (const auto & component : components)
	{
		if (component.GetValue()->IsDerivedFromType(T::TypeID))
		{
			static_pointer_cast<T>(component.GetValue())->Enable();
		}
	}
}

//===========================================================================//

template<typename T>
void EntityHelper::DisableEntityDerivedComponents(const SharedPtr<Entity> & entity)
{
	for (const auto & childEntity : entity->GetChildren())
	{
		DisableEntityDerivedComponents<T>(childEntity);
	}
	
	const auto & components = entity->GetComponents();
	for (const auto & component : components)
	{
		if (component.GetValue()->IsDerivedFromType(T::TypeID))
		{
			static_pointer_cast<T>(component.GetValue())->Disable();
		}
	}
}

//===========================================================================//

} // namespace Bru

#endif // ENTITY_HELPER_H
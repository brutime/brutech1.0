//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание:
//

#pragma once
#ifndef ENTITY_GENERATOR_H
#define ENTITY_GENERATOR_H

#include "../../Positioning/Positioning.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class EntityGeneratorSingleton
{
public:
	EntityGeneratorSingleton();
	~EntityGeneratorSingleton();

	void GenerateSprite(const string & path, const string & groupName) const;
	void GenerateSprite(const string & path, const string & groupName, integer width, integer height) const;
	void GenerateAnimatedSprite(const string & path, const string & groupName,
	                            integer framesByWidth, integer framesByHeight) const;
	void GenerateAnimatedSprite(const string & path, const string & groupName,
	                            integer frameWidth, integer frameHeight,
	                            integer framesByWidth, integer framesByHeight) const;

private:
	static const string Name;

	void GenerateTextureInfoFile(const string & path) const;
	void GenerateEntityFile(const string & path, const string & groupName, const string & renderableType) const;
	Size GetSizeInCoords(integer width, integer height) const;

	void RegisterCommands();
	void UnregisterCommands();

private:
	InnerEvent<const EmptyEventArgs &> _entityGenerated;

public:
	Event<const EmptyEventArgs &> EntityGenerated;

private:
	EntityGeneratorSingleton(const EntityGeneratorSingleton &) = delete;
	EntityGeneratorSingleton & operator = (const EntityGeneratorSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<EntityGeneratorSingleton> EntityGenerator;

//===========================================================================//

} // namespace Bru

#endif // ENTITY_GENERATOR_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityManager.h"

namespace Bru
{

//===========================================================================//

EntityManagerEventArgs::EntityManagerEventArgs(const WeakPtr<Entity> & changedEntity) : 
	ChangedEntity(changedEntity)
{
}

//===========================================================================//

EntityManagerEventArgs::EntityManagerEventArgs(const EntityManagerEventArgs & other) : 
	ChangedEntity(other.ChangedEntity)
{
}

//===========================================================================//

EntityManagerEventArgs::~EntityManagerEventArgs()
{
	
}

//===========================================================================//

EntityManagerEventArgs & EntityManagerEventArgs::operator =(const EntityManagerEventArgs & other)
{
	if (this == &other)
	{
		return *this;
	}

	ChangedEntity = other.ChangedEntity;

	return *this;	
}

//===========================================================================//

} // namespace Bru

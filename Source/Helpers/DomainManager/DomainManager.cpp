//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DomainManager.h"

namespace Bru
{

//===========================================================================//

SharedPtr<DomainManagerSingleton> DomainManager;

//===========================================================================//

const string DomainManagerSingleton::Name = "Domain manager";

//===========================================================================//

DomainManagerSingleton::DomainManagerSingleton()
{
}

//===========================================================================//

DomainManagerSingleton::~DomainManagerSingleton()
{
}

//===========================================================================//

void DomainManagerSingleton::Add(const string & domainName)
{
	InputSystem->AddDomain(domainName);
	Updater->AddDomain(domainName);
	Renderer->AddDomain(domainName);
}

//===========================================================================//

void DomainManagerSingleton::Remove(const string & domainName)
{
	InputSystem->RemoveDomain(domainName);
	Updater->RemoveDomain(domainName);
	Renderer->RemoveDomain(domainName);
}

//===========================================================================//

void DomainManagerSingleton::Select(const string & domainName)
{
	InputSystem->SelectDomain(domainName);
	Updater->SelectDomain(domainName);
	Renderer->SelectDomain(domainName);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, который берет на себя инициализацию и деинициализацию основных подсистем
//

#pragma once
#ifndef DOMAIN_MANAGER_H
#define DOMAIN_MANAGER_H

#include "../../Common/Common.h"
#include "../../OperatingSystem/OperatingSystem.h"
#include "../../Updater/Updater.h"
#include "../../Positioning/Positioning.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class DomainManagerSingleton
{
public:
	DomainManagerSingleton();
	~DomainManagerSingleton();

	void Add(const string & domainName);
	void Remove(const string & domainName);
	void Select(const string & domainName);

private:
	static const string Name;

	DomainManagerSingleton(const DomainManagerSingleton &) = delete;
	DomainManagerSingleton & operator =(const DomainManagerSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<DomainManagerSingleton> DomainManager;

//===========================================================================//

}// namespace Bru

#endif // DOMAIN_MANAGER_H

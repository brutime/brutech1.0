//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, который берет на себя инициализацию и деинициализацию основных подсистем
//

#pragma once
#ifndef ENGINE_H
#define ENGINE_H

#include "../../Common/Common.h"
#include "../../Math/Math.h"
#include "../../ScriptSystem/ScriptSystem.h"
#include "../../SoundSystem/SoundSystem.h"
#include "../../Utils/Log/Log.h"
#include "../../Utils/CommandsManager/CommandsManager.h"
#include "../../Utils/Config/Config.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/PathHelper/PathHelper.h"
#include "../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"
#include "../../Utils/StringTable/StringTable.h"
#include "../../Utils/CoreWatcher/CoreWatcher.h"
#include "../../Utils/Watcher/Watcher.h"
#include "../../EntitySystem/EntitySystem.h"
#include "../../OperatingSystem/OperatingSystem.h"
#include "../../Updater/Updater.h"
#include "../../Positioning/Positioning.h"
#include "../../Renderer/Renderer.h"
#include "../../Widgets/Widgets.h"
#include "../DomainManager/DomainManager.h"
#include "../ConsoleView/ConsoleView.h"
#include "../DebugView/DebugView.h"
#include "../CoordsHelper/CoordsHelper.h"
#include "../EntitiesHelpers/EntityGenerator.h"
#include "../EntitiesHelpers/EntityHelper.h"
#include "../EntitiesHelpers/EntityManager.h"

namespace Bru
{

//===========================================================================//

class EngineSingleton
{
public:
	EngineSingleton(integer surfaceCoordsWidth, integer surfaceCoordsHeight);
	~EngineSingleton();

	void Update();
	void ResetTiming();

	float GetDeltaTime() const;

private:
	static const string Name;
	static const string Version;
	
	static const string TechnicalLayerName;

	static const string SurfaceWidthPath;
	static const string SurfaceHeightPath;
	static const string SurfaceIsFullScreenPath;
	static const string SurfaceIsVerticalSyncPath;
	static const string SurfaceClearColorPath;

	static const float MaxDeltaTime;

	void UpdateDeltaTime();

	int64 _currentTicks;
	float _deltaTime;

	EngineSingleton(const EngineSingleton &) = delete;
	EngineSingleton & operator =(const EngineSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<EngineSingleton> Engine;

//===========================================================================//

}// namespace Bru

#endif // ENGINE_H

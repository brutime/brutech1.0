//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Engine.h"

namespace Bru
{

//===========================================================================//

SharedPtr<EngineSingleton> Engine;

//===========================================================================//

const string EngineSingleton::Name = "Engine";
const string EngineSingleton::Version = "0.7.8 alpha";

const string EngineSingleton::TechnicalLayerName = "Technical";

const string EngineSingleton::SurfaceWidthPath = "Surface.Width";
const string EngineSingleton::SurfaceHeightPath = "Surface.Height";
const string EngineSingleton::SurfaceIsFullScreenPath = "Surface.IsFullScreen";
const string EngineSingleton::SurfaceIsVerticalSyncPath = "Surface.IsVerticalSync";
const string EngineSingleton::SurfaceClearColorPath = "Surface.ClearColor";

const float EngineSingleton::MaxDeltaTime = 0.1f;

//===========================================================================//

EngineSingleton::EngineSingleton(integer surfaceCoordsWidth, integer surfaceCoordsHeight) :
	_currentTicks(0),
	_deltaTime(0.0f)
{
	OSConsoleStream = new OSConsoleStreamSingleton();
	ExceptionRegistrator = new ExceptionRegistratorSingleton();
	Console = new ConsoleSingleton(MessagePriority::Debug);

	Console->Notice(string::Format("Started at {0}", DateTimeHelper::GetCurrentLocalDateAndTime()));

	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	Console->Notice(string::Format("Engine version: \"{0}\"", Version));

	Config = new ConfigSingleton();
	ScriptSystem = new ScriptSystemSingleton();
	CoreWatcher = new CoreWatcherSingleton();
	Watcher = new WatcherSingleton();
	PerfomanceDataCollector = new PerfomanceDataCollectorSingleton(0.5f);
	ParametersFileManager = new ParametersFileManagerSingleton();
	StringTable = new StringTableSingleton("StringTable");
	OperatingSystem = new OperatingSystemSingleton();
	InputSystem = new InputSystemSingleton();
	Updater = new UpdaterSingleton();

	Console->Notice("Loading surface config...");

	integer surfaceWidth = Config->Get<integer>(SurfaceWidthPath);
	integer surfaceHeight = Config->Get<integer>(SurfaceHeightPath);
	FullScreenMode fullScreenMode = static_cast<FullScreenMode>(Config->Get<bool>(SurfaceIsFullScreenPath));
	VerticalSyncMode verticalSyncMode = static_cast<VerticalSyncMode>(Config->Get<bool>(SurfaceIsVerticalSyncPath));
	Color clearColor = Config->Get<Color>(SurfaceClearColorPath);

	Console->Notice("Surface config loaded");

	Renderer = new RendererSingleton(surfaceWidth, surfaceHeight, surfaceCoordsWidth, surfaceCoordsHeight,
	                                 fullScreenMode, verticalSyncMode, clearColor, ProjectionMode::Perspective);

	Renderer->AddLayerToFront(EntitySystem::GlobalDomainName, new RenderableLayer(TechnicalLayerName, ProjectionMode::PercentsOrtho));

	SoundSystem = new SoundSystemSingleton();

	WidgetsManager = new WidgetsManagerSingleton();
	DomainManager = new DomainManagerSingleton();
	ConsoleView = new ConsoleViewSingleton(TechnicalLayerName);
	ConsoleView->Close();
	DebugView = new DebugViewSingleton(TechnicalLayerName);
	DebugView->ClosePerfomancePanel();
	DebugView->CloseWatchesPanel();
	EntityGenerator = new EntityGeneratorSingleton();

	GlobalTimer = new GlobalTimerSingleton();
	Math::Randomize(GlobalTimer->GetTicks());

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

EngineSingleton::~EngineSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	Console->Notice("Saving surface config...");

	Config->Set<integer>(SurfaceWidthPath, Surface->GetWidth());
	Config->Set<integer>(SurfaceHeightPath, Surface->GetHeight());
	Config->Set<bool>(SurfaceIsFullScreenPath, static_cast<bool>(Renderer->GetFullScreenMode()));
	Config->Set<bool>(SurfaceIsVerticalSyncPath, static_cast<bool>(Renderer->GetVerticalSyncMode()));
	Config->Set<Color>(SurfaceClearColorPath, Renderer->GetClearColor());
	Config->Save();

	Console->Notice("Surface config saved");

	GlobalTimer = NullPtr;
	EntityGenerator = NullPtr;
	DebugView = NullPtr;
	ConsoleView = NullPtr;
	DomainManager = NullPtr;
	WidgetsManager = NullPtr;
	SoundSystem = NullPtr;
	Renderer = NullPtr;
	Updater = NullPtr;
	InputSystem = NullPtr;
	OperatingSystem = NullPtr;
	StringTable = NullPtr;
	ParametersFileManager = NullPtr;
	PerfomanceDataCollector = NullPtr;
	Watcher = NullPtr;
	CoreWatcher = NullPtr;
	ScriptSystem = NullPtr;
	Config = NullPtr;

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));

	Console = NullPtr;
	ExceptionRegistrator = NullPtr;
	OSConsoleStream = NullPtr;
}

//===========================================================================//

void EngineSingleton::Update()
{
	UpdateDeltaTime();

	InputSystem->Update();
	Updater->Update(_deltaTime);
	Renderer->Render();

	DebugView->Update();

	PerfomanceDataCollector->Update();

	OperatingSystem->Sleep(0);
}

//===========================================================================//

void EngineSingleton::ResetTiming()
{
	_currentTicks = GlobalTimer->GetTicks();
}

//===========================================================================//

float EngineSingleton::GetDeltaTime() const
{
	return _deltaTime;
}

//===========================================================================//

void EngineSingleton::UpdateDeltaTime()
{
	_deltaTime = static_cast<float>(GlobalTimer->GetTicks() - _currentTicks) / GlobalTimer->GetFrequency();
	if (_deltaTime < 0.0f)
	{
		_deltaTime = 0.0f;
	}
	else if (_deltaTime > MaxDeltaTime)
	{
		_deltaTime = MaxDeltaTime;
	}

	_currentTicks = GlobalTimer->GetTicks();
}

//===========================================================================//

} // namespace Bru

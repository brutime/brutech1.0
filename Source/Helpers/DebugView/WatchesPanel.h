//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Панель, которая представляет данные от VariablesWatcher
//

#pragma once
#ifndef WATCHES_PANEL_H
#define WATCHES_PANEL_H

#include "../../Common/Common.h"
#include "../../Widgets/Widgets.h"

namespace Bru
{

//===========================================================================//

class WatchesPanel
{
public:
	WatchesPanel(const string & layerName, const Vector2 & position, const Vector2 & size, float horizontalPadding,
	             float verticalPadding, const WeakPtr<Font> & font, const WeakPtr<Font> & titleFont,
	             const Color & backgroudColor, const Color & titleForegroundColor, const Color & foregroundColor);
	~WatchesPanel();

	void Show();
	void Hide();
	
	bool IsVisible() const;

	void Update();

private:
	void AddWatch(const string & variableName);
	void RemoveWatch(const string & variableName);
	
	void AddCoreWatch(const string & variableName);
	void RemoveCoreWatch(const string & variableName);
	
	SharedPtr<Frame> _watchesPanel;
	SharedPtr<Label> _coreWatchesTitleLabel;
	SharedPtr<Label> _watchesTitleLabel;
	SharedPtr<TextPanel> _coreVariablesNamesPanel;
	SharedPtr<TextPanel> _coreVariablesValuesPanel;
	SharedPtr<TextPanel> _variablesNamesPanel;
	SharedPtr<TextPanel> _variablesValuesPanel;	

	Color _foregroundColor;
	
	SharedPtr<EventHandler<const string &>> _coreWatcherExpressionsRegisteredHandler;
	SharedPtr<EventHandler<const string &>> _coreWatcherExpressionsUnregisteredHandler;	

	SharedPtr<EventHandler<const string &>> _watcherExpressionsRegisteredHandler;
	SharedPtr<EventHandler<const string &>> _watcherExpressionsUnregisteredHandler;

	WatchesPanel(const WatchesPanel &) = delete;
	WatchesPanel & operator =(const WatchesPanel &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // WATCHES_PANEL_H

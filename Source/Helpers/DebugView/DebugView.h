//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Панель, которая представляет совмещенные данные от PerfomanceDataPresenter и VariablesWatcher
//

#pragma once
#ifndef DEBUG_VIEW_H
#define DEBUG_VIEW_H

#include "../../Common/Common.h"
#include "../../Widgets/Widgets.h"
#include "PerfomancePanel.h"
#include "WatchesPanel.h"
#include "SummaryInfoLabel.h"
#include "DebugViewInputReceiver.h"

namespace Bru
{

//===========================================================================//

class DebugViewSingleton
{
public:
	DebugViewSingleton(const string & layerName);
	~DebugViewSingleton();

	void OpenPerfomancePanel();
	void ClosePerfomancePanel();
	void SwitchPerfomancePanel();

	void OpenWatchesPanel();
	void CloseWatchesPanel();
	void SwitchWatchesPanel();

	void OpenSummaryInfoLabel();
	void CloseSummaryInfoLabel();
	void SwitchSummaryInfoLabel();

	void Update();

	void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	void DispatchMouseEvent(const MouseEventArgs & args);
	void DispatchJoystickEvent(const JoystickEventArgs & args);

private:
	static const string Name;

	static const float HorizontalPadding;
	static const float VerticalPadding;

	static const string FontNamePath;
	static const string FontSizePath;
	static const string BackgroundColorPath;
	static const string TitleForegroundColorPath;
	static const string ForegroundColorPath;

	SharedPtr<PerfomancePanel> _perfomancePanel;
	SharedPtr<WatchesPanel> _watchesPanel;
	SharedPtr<SummaryInfoLabel> _summaryInfoLabel;

	SharedPtr<DebugViewInputReceiver> _inputReceiver;

	DebugViewSingleton(const DebugViewSingleton &) = delete;
	DebugViewSingleton & operator = (const DebugViewSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<DebugViewSingleton> DebugView;

//===========================================================================//

} // namespace Bru

#endif // DEBUG_VIEW_H

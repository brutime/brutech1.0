//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Панель, которая представляет данные от PerfomanceDataCollector
//

#pragma once
#ifndef PERFOMANCE_PANEL_H
#define PERFOMANCE_PANEL_H

#include "../../Common/Common.h"
#include "../../Widgets/Widgets.h"

namespace Bru
{

//===========================================================================//

class PerfomancePanel
{
public:
	PerfomancePanel(const string & layerName, const Vector2 & position, const Vector2 & size, float horizontalPadding,
	                float verticalPadding, const WeakPtr<Font> & font, const WeakPtr<Font> & titleFont,
	                const Color & backgroudColor, const Color & titleForegroundColor, const Color & foregroundColor);
	~PerfomancePanel();

	void Show();
	void Hide();
	
	bool IsVisible() const;

	void Update();

private:
	static const float PerfomanceChartXFactor;
	static const float PerfomanceChartYFactor;

	static const float PerfomanceChartNameXFactor;
	static const float PerfomanceChartTimeXFactor;
	static const float PerfomanceChartPercentXFactor;

	static const string ChartColorsSectionPath;
	static const string ChartColorKey;

	void AddMeasurement(const string & measurementName);
	void RemoveMeasurement(const string & measurementName);

	void AddChartPart(const string & measurementName);
	void RemoveChartPart(const string & measurementName);
	void FillChartsColors();
	
	float GetChartHeight() const;

	string _layerName;

	SharedPtr<Frame> _perfomancePanel;
	SharedPtr<Label> _perfomanceTitleLabel;
	AddingOrderedTreeMap<string ,SharedPtr<Frame>> _chartParts;
	SharedPtr<TextPanel> _chartNamesPanel;
	SharedPtr<TextPanel> _chartValuesPanel;	

	float _horizontalPadding;
	float _verticalPadding;

	Color _foregroundColor;
	Array<Color> _chartColors;
	
	SharedPtr<EventHandler<const string &>> _measurementAddedHandler;
	SharedPtr<EventHandler<const string &>> _measurementRemovedHandler;	

	PerfomancePanel(const PerfomancePanel &) = delete;
	PerfomancePanel & operator =(const PerfomancePanel &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PERFOMANCE_PANEL_H

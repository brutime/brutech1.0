//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DebugView.h"

#include "../../Utils/Config/Config.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"

namespace Bru
{

//===========================================================================//

SharedPtr<DebugViewSingleton> DebugView;

//===========================================================================//

const string DebugViewSingleton::Name = "Debug view";

const float DebugViewSingleton::HorizontalPadding = 1.5f;
const float DebugViewSingleton::VerticalPadding = 2.5f;

const string DebugViewSingleton::FontNamePath = "DebugView.Name";
const string DebugViewSingleton::FontSizePath = "DebugView.Size";
const string DebugViewSingleton::BackgroundColorPath = "DebugView.BackgroundColor";
const string DebugViewSingleton::TitleForegroundColorPath = "DebugView.TitleForegroundColor";
const string DebugViewSingleton::ForegroundColorPath = "DebugView.ForegroundColor";

//===========================================================================//

DebugViewSingleton::DebugViewSingleton(const string & layerName) :
	_perfomancePanel(),
	_watchesPanel(),
	_summaryInfoLabel(),
	_inputReceiver()
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	WeakPtr<Font> font = FontManager->Load(Config->Get<string>(FontNamePath), Config->Get<integer>(FontSizePath));
	WeakPtr<Font> titleFont = FontManager->Load(font->GetName(), font->GetSize() * 1.5f);

	Color backgroundColor = Config->Get<Color>(BackgroundColorPath);
	Color foregroundColor = Config->Get<Color>(ForegroundColorPath);
	Color titleForegroundColor = Config->Get<Color>(TitleForegroundColorPath);

	_perfomancePanel = new PerfomancePanel(layerName, {0.0, 0.0f}, {40.0, 50.0f}, HorizontalPadding, VerticalPadding,
	                                       font, titleFont, backgroundColor, titleForegroundColor, foregroundColor);
	_perfomancePanel->Hide();
	_watchesPanel = new WatchesPanel(layerName, {40.0, 0.0f}, {60.0f, 50.0f}, HorizontalPadding, VerticalPadding,
	                                 font, titleFont, backgroundColor, titleForegroundColor, foregroundColor);
	_watchesPanel->Hide();

	_summaryInfoLabel = new SummaryInfoLabel(layerName, 97.0f, font, backgroundColor, foregroundColor);
	_summaryInfoLabel->Hide();

	_inputReceiver = new DebugViewInputReceiver();

	PerfomanceDataCollector->AddSubtraction(Name);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

DebugViewSingleton::~DebugViewSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));

	PerfomanceDataCollector->RemoveSubtraction(Name);

	_inputReceiver = NullPtr;

	_summaryInfoLabel = NullPtr;
	_watchesPanel = NullPtr;
	_perfomancePanel = NullPtr;

	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void DebugViewSingleton::OpenPerfomancePanel()
{
	if (!_perfomancePanel->IsVisible())
	{
		_perfomancePanel->Show();
	}
}

//===========================================================================//

void DebugViewSingleton::ClosePerfomancePanel()
{
	if (_perfomancePanel->IsVisible())
	{
		_perfomancePanel->Hide();
	}
}

//===========================================================================//

void DebugViewSingleton::SwitchPerfomancePanel()
{
	_perfomancePanel->IsVisible() ? _perfomancePanel->Hide() : _perfomancePanel->Show();
}

//===========================================================================//

void DebugViewSingleton::OpenWatchesPanel()
{
	if (!_watchesPanel->IsVisible())
	{
		_watchesPanel->Show();
	}
}

//===========================================================================//

void DebugViewSingleton::CloseWatchesPanel()
{
	if (_watchesPanel->IsVisible())
	{
		_watchesPanel->Hide();
	}
}

//===========================================================================//

void DebugViewSingleton::SwitchWatchesPanel()
{
	_watchesPanel->IsVisible() ? _watchesPanel->Hide() : _watchesPanel->Show();
}

//===========================================================================//

void DebugViewSingleton::OpenSummaryInfoLabel()
{
	if (!_summaryInfoLabel->IsVisible())
	{
		_summaryInfoLabel->Show();
	}
}

//===========================================================================//

void DebugViewSingleton::CloseSummaryInfoLabel()
{
	if (_summaryInfoLabel->IsVisible())
	{
		_summaryInfoLabel->Hide();
	}
}

//===========================================================================//

void DebugViewSingleton::SwitchSummaryInfoLabel()
{
	_summaryInfoLabel->IsVisible() ? _summaryInfoLabel->Hide() : _summaryInfoLabel->Show();
}

//===========================================================================//

void DebugViewSingleton::Update()
{
	PerfomanceDataCollector->StartSubtraction(Name);

	_perfomancePanel->Update();
	_watchesPanel->Update();
	_summaryInfoLabel->Update();

	PerfomanceDataCollector->EndSubtraction(Name);
}

//===========================================================================//

void DebugViewSingleton::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::F10:
			SwitchPerfomancePanel();
			break;

		case KeyboardKey::F11:
			SwitchWatchesPanel();
			break;

		case KeyboardKey::F12:
			SwitchSummaryInfoLabel();
			break;

		default:
			break;
		}
	}
}

//===========================================================================//

void DebugViewSingleton::DispatchMouseEvent(const MouseEventArgs &)
{
}

//===========================================================================//

void DebugViewSingleton::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

} // namespace Bru


//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef DEBUG_VIEW_INPUT_RECEIVER_H
#define DEBUG_VIEW_INPUT_RECEIVER_H

#include "../../OperatingSystem/OperatingSystem.h"

namespace Bru
{

//===========================================================================//

class DebugViewInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	DebugViewInputReceiver();
	virtual ~DebugViewInputReceiver();	

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);

private:
	DebugViewInputReceiver(const DebugViewInputReceiver &) = delete;
	DebugViewInputReceiver & operator =(const DebugViewInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // DEBUG_VIEW_INPUT_RECEIVER_H

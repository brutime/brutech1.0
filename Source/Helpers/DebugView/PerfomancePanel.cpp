//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PerfomancePanel.h"

#include "../../Utils/Config/Config.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"

namespace Bru
{

//===========================================================================//

const float PerfomancePanel::PerfomanceChartXFactor = 0.5714f;
const float PerfomancePanel::PerfomanceChartYFactor = 0.3f;

const float PerfomancePanel::PerfomanceChartNameXFactor = 0.66f;
const float PerfomancePanel::PerfomanceChartTimeXFactor = 0.17f;
const float PerfomancePanel::PerfomanceChartPercentXFactor = 0.17f;

const string PerfomancePanel::ChartColorsSectionPath = "DebugView.ChartColors";
const string PerfomancePanel::ChartColorKey = "Color";

//===========================================================================//

PerfomancePanel::PerfomancePanel(const string & layerName, const Vector2 & position, const Vector2 & size, float horizontalPadding,
                                 float verticalPadding, const WeakPtr<Font> & font, const WeakPtr<Font> & titleFont,
                                 const Color & backgroudColor, const Color & titleForegroundColor, const Color & foregroundColor) :
	_layerName(layerName),
	_perfomancePanel(),
	_perfomanceTitleLabel(),
	_chartParts(),
	_chartNamesPanel(),
	_chartValuesPanel(),
	_horizontalPadding(horizontalPadding),
	_verticalPadding(verticalPadding),
	_foregroundColor(foregroundColor),
	_chartColors(),
	_measurementAddedHandler(new EventHandler<const string &>(this, & PerfomancePanel::AddMeasurement)),
	_measurementRemovedHandler(new EventHandler<const string &>(this, & PerfomancePanel::RemoveMeasurement))
{
	FillChartsColors();

	_perfomancePanel = new Frame(EntitySystem::GlobalDomainName, layerName, size.X, size.Y, backgroudColor);
	_perfomancePanel->MoveTo(position.X, position.Y);

	_perfomanceTitleLabel = new Label(EntitySystem::GlobalDomainName, layerName, "Perfomance", titleFont, titleForegroundColor);
	_perfomanceTitleLabel->MoveTo(_horizontalPadding, _perfomancePanel->GetSize().Y - _perfomanceTitleLabel->GetSize().Y);
	_perfomancePanel->AddChild(_perfomanceTitleLabel);

	float _chartPanelsHeight = size.Y - (_perfomanceTitleLabel->GetSize().Y + GetChartHeight() + verticalPadding * 2);

	_chartNamesPanel = new TextPanel(EntitySystem::GlobalDomainName, layerName, font, TextPanelAlignment::Up,
	                                 _horizontalPadding, 0.0f, (size.X - _horizontalPadding * 2.0f) * 0.75f, _chartPanelsHeight, backgroudColor);
	_chartNamesPanel->HideBackground();
	_chartNamesPanel->MoveTo(_horizontalPadding, 0.0f);
	_perfomancePanel->AddChild(_chartNamesPanel);

	_chartValuesPanel = new TextPanel(EntitySystem::GlobalDomainName, layerName, font, TextPanelAlignment::Up,
	                                  _horizontalPadding, 0.0f, (size.X - _horizontalPadding * 2.0f) * 0.25f,
	                                  _chartPanelsHeight, backgroudColor);
	_chartValuesPanel->HideBackground();									  
	_chartValuesPanel->MoveTo(_chartNamesPanel->GetSize().X + _horizontalPadding, 0.0f);
	_perfomancePanel->AddChild(_chartValuesPanel);

	_perfomancePanel->SetViewOrder(-1);

	const auto & percentageMeasurements = PerfomanceDataCollector->GetPercentageMeasurements();
	for (const auto & percentageMeasurement : percentageMeasurements)
	{
		AddMeasurement(percentageMeasurement.GetKey());
	}
	PerfomanceDataCollector->MeasurementAdded.AddHandler(_measurementAddedHandler);
	PerfomanceDataCollector->MeasurementRemoved.AddHandler(_measurementRemovedHandler);
}

//===========================================================================//

PerfomancePanel::~PerfomancePanel()
{
	PerfomanceDataCollector->MeasurementAdded.RemoveHandler(_measurementAddedHandler);
	PerfomanceDataCollector->MeasurementRemoved.RemoveHandler(_measurementRemovedHandler);

	_chartParts.Clear();
	_chartValuesPanel = NullPtr;
	_chartNamesPanel = NullPtr;
	_perfomancePanel = NullPtr;
	_perfomanceTitleLabel = NullPtr;
	_perfomancePanel = NullPtr;
}

//===========================================================================//

void PerfomancePanel::Show()
{
	_perfomancePanel->Show();
}

//===========================================================================//

void PerfomancePanel::Hide()
{
	_perfomancePanel->Hide();
}

//===========================================================================//

bool PerfomancePanel::IsVisible() const
{
	return _perfomancePanel->IsVisible();
}

//===========================================================================//

void PerfomancePanel::Update()
{
	if (!IsVisible())
	{
		return;
	}

	float chartPartPositionX = _horizontalPadding;
	float chartWidth = _perfomancePanel->GetSize().X - _horizontalPadding * 2.0f;

	_chartValuesPanel->DisableUpdating();
	integer lineIndex = 0;
	for (const auto & percentageMeasurement : PerfomanceDataCollector->GetPercentageMeasurements())
	{
		float currentPartWidth = chartWidth * (percentageMeasurement.GetValue()->Percent / 100.0f);
		const string & measurementName = percentageMeasurement.GetKey();

		_chartParts[measurementName]->SetSize(currentPartWidth, _chartParts[measurementName]->GetSize().Y);
		_chartParts[measurementName]->MoveTo(chartPartPositionX, _perfomanceTitleLabel->GetPosition().Y -
		                                     _chartParts[measurementName]->GetSize().Y - _verticalPadding);

		chartPartPositionX += currentPartWidth;

		_chartValuesPanel->SetLineText(lineIndex, string::Format("{0:0.2} %", percentageMeasurement.GetValue()->Percent));
		lineIndex++;
	}
	_chartValuesPanel->EnableUpdating();
}

//===========================================================================//

void PerfomancePanel::AddMeasurement(const string & measurementName)
{
	AddChartPart(measurementName);
	_chartNamesPanel->AddLine(measurementName, _chartParts[measurementName]->GetColor());
	_chartValuesPanel->AddLine(string::Empty, _chartParts[measurementName]->GetColor());
}

//===========================================================================//

void PerfomancePanel::RemoveMeasurement(const string & measurementName)
{
	RemoveChartPart(measurementName);
	integer lineIndex = _chartNamesPanel->GetLineIndexByText(measurementName);
	_chartNamesPanel->RemoveLineAt(lineIndex);
	_chartValuesPanel->RemoveLineAt(lineIndex);
}

//===========================================================================//

void PerfomancePanel::AddChartPart(const string & measurementName)
{
	if (_chartColors.GetCount() == _chartParts.GetCount())
	{
		_chartColors.Add(Color::RandomRGBA());
	}
	const auto & color = _chartColors[_chartParts.GetCount()];
	SharedPtr<Frame> chartPart = new Frame(EntitySystem::GlobalDomainName, _layerName, 0.0f, GetChartHeight(), color);
	_chartParts.Add(measurementName, chartPart);
	_perfomancePanel->AddChild(chartPart);
}

//===========================================================================//

void PerfomancePanel::RemoveChartPart(const string & measurementName)
{
	_perfomancePanel->RemoveChild(_chartParts[measurementName]);
	_chartParts.Remove(measurementName);
}

//===========================================================================//

void PerfomancePanel::FillChartsColors()
{
	integer chartColorsCount = Config->GetListChildren(ChartColorsSectionPath);
	for (integer i = 0; i < chartColorsCount; ++i)
	{
		string path = string::Format(ChartColorsSectionPath + ".{0}." + ChartColorKey, i);
		_chartColors.Add(Config->Get<Color>(path));
	}
}

//===========================================================================//

float PerfomancePanel::GetChartHeight() const
{
	return _perfomancePanel->GetSize().Y * 0.15f;
}

//===========================================================================//

} // namespace Bru

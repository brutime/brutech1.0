//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SummaryInfoLabel.h"

#include "../../Utils/CoreWatcher/CoreWatcher.h"
#include "../../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"

namespace Bru
{

//===========================================================================//

SummaryInfoLabel::SummaryInfoLabel(const string & layerName, float positionY, const WeakPtr<Font> & font,
                                   const Color & backgroudColor, const Color & foregroundColor) :
	_background(),
	_label()
{
	_background = new Frame(EntitySystem::GlobalDomainName, layerName, 100.0f, 100.0f - positionY, backgroudColor);
	_background->MoveTo(0.0f, positionY);
	_background->HideBackground();

	_label = new Label(EntitySystem::GlobalDomainName, layerName, "DIE!", font, foregroundColor, {0.0f, 0.0f},
	                   WordWrapMode::Simple, TextHorizontalAlignment::Right, TextVerticalAlignment::Center);
	_label->MoveTo(98.0f, _background->GetSize().Y * 0.5f);
	_background->AddChild(_label);

	_label->SetViewOrder(-2);
}

//===========================================================================//

SummaryInfoLabel::~SummaryInfoLabel()
{
	_label = NullPtr;
	_background = NullPtr;
}

//===========================================================================//

void SummaryInfoLabel::Show()
{
	_background->Show();
}

//===========================================================================//

void SummaryInfoLabel::Hide()
{
	_background->Hide();
}

//===========================================================================//

bool SummaryInfoLabel::IsVisible() const
{
	return _background->IsVisible();
}

//===========================================================================//

void SummaryInfoLabel::Update()
{
	if (!IsVisible())
	{
		return;
	}

	_label->SetText(string::Format("FPS: {0}", CoreWatcher->Watch("FPS")));
}

//===========================================================================//

} // namespace Bru


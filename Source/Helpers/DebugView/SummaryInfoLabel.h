//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SUMMARY_INFO_LABEL_H
#define SUMMARY_INFO_LABEL_H

#include "../../Common/Common.h"
#include "../../Widgets/Widgets.h"

namespace Bru
{

//===========================================================================//

class SummaryInfoLabel
{
public:
	SummaryInfoLabel(const string & layerName, float positionY, const WeakPtr<Font> &font,
	                 const Color & backgroudColor, const Color & foregroundColor);
	~SummaryInfoLabel();

	void Show();
	void Hide();

	bool IsVisible() const;

	void Update();

private:
	SharedPtr<Frame> _background;
	SharedPtr<Label> _label;

	SummaryInfoLabel(const SummaryInfoLabel &) = delete;
	SummaryInfoLabel & operator = (const SummaryInfoLabel &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // SUMMARY_INFO_LABEL_H
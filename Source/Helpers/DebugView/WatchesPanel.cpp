//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WatchesPanel.h"

#include "../../Utils/Config/Config.h"
#include "../../Utils/CoreWatcher/CoreWatcher.h"
#include "../../Utils/Watcher/Watcher.h"

namespace Bru
{

//===========================================================================//

WatchesPanel::WatchesPanel(const string & layerName, const Vector2 & position, const Vector2 & size, float horizontalPadding,
                           float verticalPadding, const WeakPtr<Font> & font, const WeakPtr<Font> & titleFont,
                           const Color & backgroudColor, const Color & titleForegroundColor, const Color & foregroundColor) :
	_watchesPanel(),
	_coreWatchesTitleLabel(),
	_watchesTitleLabel(),
	_coreVariablesNamesPanel(),
	_coreVariablesValuesPanel(),
	_variablesNamesPanel(),
	_variablesValuesPanel(),	
	_foregroundColor(foregroundColor),
	_coreWatcherExpressionsRegisteredHandler(new EventHandler<const string &>(this, & WatchesPanel::AddCoreWatch)),
	_coreWatcherExpressionsUnregisteredHandler(new EventHandler<const string &>(this, & WatchesPanel::RemoveCoreWatch)),
	_watcherExpressionsRegisteredHandler(new EventHandler<const string &>(this, & WatchesPanel::AddWatch)),
	_watcherExpressionsUnregisteredHandler(new EventHandler<const string &>(this, & WatchesPanel::RemoveWatch))
{
	_watchesPanel = new Frame(EntitySystem::GlobalDomainName, layerName, size.X, size.Y, backgroudColor);
	_watchesPanel->MoveTo(position.X, position.Y);

	float variablesPanelsWidth = size.X * 0.5f;

	_coreWatchesTitleLabel = new Label(EntitySystem::GlobalDomainName, layerName, "Core watches", titleFont, titleForegroundColor);
	_coreWatchesTitleLabel->MoveTo(horizontalPadding, _watchesPanel->GetSize().Y - _coreWatchesTitleLabel->GetSize().Y);
	_watchesPanel->AddChild(_coreWatchesTitleLabel);

	_watchesTitleLabel = new Label(EntitySystem::GlobalDomainName, layerName, "Watches", titleFont, titleForegroundColor);
	_watchesTitleLabel->MoveTo(variablesPanelsWidth + horizontalPadding, _watchesPanel->GetSize().Y - _watchesTitleLabel->GetSize().Y);
	_watchesPanel->AddChild(_watchesTitleLabel);

	float variablesPanelsHeight = size.Y - (_watchesTitleLabel->GetSize().Y + verticalPadding);

	//Core watches

	_coreVariablesNamesPanel = new TextPanel(EntitySystem::GlobalDomainName, layerName, font, TextPanelAlignment::Up,
	                                         horizontalPadding, 0.0f, variablesPanelsWidth * 0.75f, variablesPanelsHeight, backgroudColor);
	_coreVariablesNamesPanel->HideBackground();
	_watchesPanel->AddChild(_coreVariablesNamesPanel);

	_coreVariablesValuesPanel = new TextPanel(EntitySystem::GlobalDomainName, layerName, font, TextPanelAlignment::Up,
	                                          0.0f, 0.0f, variablesPanelsWidth - _coreVariablesNamesPanel->GetSize().X,
	                                          variablesPanelsHeight, backgroudColor);
	_coreVariablesValuesPanel->HideBackground();
	_coreVariablesValuesPanel->MoveTo(_coreVariablesNamesPanel->GetSize().X, 0.0f);
	_watchesPanel->AddChild(_coreVariablesValuesPanel);

	//Watches

	_variablesNamesPanel = new TextPanel(EntitySystem::GlobalDomainName, layerName, font, TextPanelAlignment::Up,
	                                     horizontalPadding, 0.0f, variablesPanelsWidth * 0.75f, variablesPanelsHeight, backgroudColor);
	_variablesNamesPanel->HideBackground();
	_variablesNamesPanel->MoveTo(variablesPanelsWidth, 0.0f);
	_watchesPanel->AddChild(_variablesNamesPanel);

	_variablesValuesPanel = new TextPanel(EntitySystem::GlobalDomainName, layerName, font, TextPanelAlignment::Up,
	                                      0.0f, 0.0f, variablesPanelsWidth - _variablesNamesPanel->GetSize().X,
	                                      variablesPanelsHeight, backgroudColor);
	_variablesValuesPanel->HideBackground();										  
	_variablesValuesPanel->MoveTo(_variablesNamesPanel->GetPosition().X + _variablesNamesPanel->GetSize().X, 0.0f);	
	_watchesPanel->AddChild(_variablesValuesPanel);

	_watchesPanel->SetViewOrder(-1);
	
	_coreVariablesNamesPanel->DisableUpdating();
	_coreVariablesValuesPanel->DisableUpdating();
	const auto & coreVariablesNames = CoreWatcher->GetVariablesNames();
	for (const auto & coreVariableName : coreVariablesNames)
	{
		AddCoreWatch(coreVariableName);
	}	
	CoreWatcher->ExpressionsRegistered.AddHandler(_coreWatcherExpressionsRegisteredHandler);
	CoreWatcher->ExpressionsUnregistered.AddHandler(_coreWatcherExpressionsUnregisteredHandler);

	_variablesNamesPanel->DisableUpdating();
	_variablesValuesPanel->DisableUpdating();
	const auto & variablesNames = Watcher->GetVariablesNames();
	for (const auto & variableName : variablesNames)
	{
		AddWatch(variableName);
	}
	Watcher->ExpressionsRegistered.AddHandler(_watcherExpressionsRegisteredHandler);
	Watcher->ExpressionsUnregistered.AddHandler(_watcherExpressionsUnregisteredHandler);
}

//===========================================================================//

WatchesPanel::~WatchesPanel()
{
	Watcher->ExpressionsRegistered.RemoveHandler(_watcherExpressionsRegisteredHandler);
	Watcher->ExpressionsUnregistered.RemoveHandler(_watcherExpressionsUnregisteredHandler);
	
	CoreWatcher->ExpressionsRegistered.RemoveHandler(_coreWatcherExpressionsRegisteredHandler);
	CoreWatcher->ExpressionsUnregistered.RemoveHandler(_coreWatcherExpressionsUnregisteredHandler);	

	_variablesValuesPanel = NullPtr;
	_variablesNamesPanel = NullPtr;

	_coreVariablesValuesPanel = NullPtr;
	_coreVariablesNamesPanel = NullPtr;

	_watchesTitleLabel = NullPtr;
	_coreWatchesTitleLabel = NullPtr;
	_watchesPanel = NullPtr;
}

//===========================================================================//

void WatchesPanel::Show()
{
	_coreVariablesNamesPanel->EnableUpdating();
	_coreVariablesValuesPanel->EnableUpdating();	
	
	_variablesNamesPanel->EnableUpdating();
	_variablesValuesPanel->EnableUpdating();

	_watchesPanel->Show();
}

//===========================================================================//

void WatchesPanel::Hide()
{
	_watchesPanel->Hide();

	_coreVariablesNamesPanel->DisableUpdating();
	_coreVariablesValuesPanel->DisableUpdating();

	_variablesNamesPanel->DisableUpdating();
	_variablesValuesPanel->DisableUpdating();
}

//===========================================================================//

bool WatchesPanel::IsVisible() const
{
	return _watchesPanel->IsVisible();
}

//===========================================================================//

void WatchesPanel::Update()
{
	if (!_watchesPanel->IsVisible())
	{
		return;
	}

	_coreVariablesValuesPanel->DisableUpdating();
	const auto & coreLines = _coreVariablesNamesPanel->GetLines();
	for (integer i = 0; i < coreLines.GetCount(); ++i)
	{
		_coreVariablesValuesPanel->SetLineText(i, CoreWatcher->Watch(_coreVariablesNamesPanel->GetLineText(i)));
	}
	_coreVariablesValuesPanel->EnableUpdating();
	
	_variablesValuesPanel->DisableUpdating();
	const auto & lines = _variablesNamesPanel->GetLines();
	for (integer i = 0; i < lines.GetCount(); ++i)
	{
		_variablesValuesPanel->SetLineText(i, Watcher->Watch(_variablesNamesPanel->GetLineText(i)));
	}
	_variablesValuesPanel->EnableUpdating();	
}

//===========================================================================//

void WatchesPanel::AddWatch(const string & variableName)
{
	_variablesNamesPanel->AddLine(variableName, _foregroundColor);
	_variablesValuesPanel->AddLine(Watcher->Watch(variableName), _foregroundColor);
}

//===========================================================================//

void WatchesPanel::RemoveWatch(const string & variableName)
{
	integer index = _variablesNamesPanel->GetLineIndexByText(variableName);
	_variablesNamesPanel->RemoveLineAt(index);
	_variablesValuesPanel->RemoveLineAt(index);
}

//===========================================================================//

void WatchesPanel::AddCoreWatch(const string & variableName)
{
	_coreVariablesNamesPanel->AddLine(variableName, _foregroundColor);
	_coreVariablesValuesPanel->AddLine(CoreWatcher->Watch(variableName), _foregroundColor);
}

//===========================================================================//

void WatchesPanel::RemoveCoreWatch(const string & variableName)
{
	integer index = _coreVariablesNamesPanel->GetLineIndexByText(variableName);
	_coreVariablesNamesPanel->RemoveLineAt(index);
	_coreVariablesValuesPanel->RemoveLineAt(index);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DebugViewInputReceiver.h"

#include "DebugView.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(DebugViewInputReceiver, InputReceiver)

//===========================================================================//

DebugViewInputReceiver::DebugViewInputReceiver() :
	InputReceiver(EntitySystem::GlobalDomainName)
{
}

//===========================================================================//

DebugViewInputReceiver::~DebugViewInputReceiver()
{
}

//===========================================================================//

void DebugViewInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	DebugView->DispatchKeyboardEvent(args);
}

//===========================================================================//

void DebugViewInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	DebugView->DispatchMouseEvent(args);
}

//===========================================================================//

void DebugViewInputReceiver::DispatchJoystickEvent(const JoystickEventArgs & args)
{
	DebugView->DispatchJoystickEvent(args);
}

//===========================================================================//

} // namespace Bru

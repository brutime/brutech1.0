//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef COORDS_HELPER_H
#define COORDS_HELPER_H

#include "../../Common/Common.h"
#include "../../Math/Math.h"
#include "../../Positioning/Positioning.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class CoordsHelper
{
public:
	static Vector2 WorldToSurface(const Vector3 & worldCoords,
	        const Vector3 & cameraPosition, ProjectionMode projectionMode);

	static Vector3 SurfaceToWorld(const Vector2 & surfaceCoords, float targetZ,
	        const Vector3 & cameraPosition, ProjectionMode projectionMode);

	static Vector3 GetProjectedOnEntityPoint(const SharedPtr<Entity> & entity, const Vector3 & point);

	static Vector3 GetProjection(const Vector2 & surfaceCoords, float deltaZ, float targetZ);
	static Vector2 GetUnProjection(const Vector3 & worldCoords, float deltaZ);

private:
	CoordsHelper() = delete;
	CoordsHelper(const CoordsHelper &) = delete;
	~CoordsHelper() = delete;
	CoordsHelper & operator =(const CoordsHelper &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // COORDS_HELPER_H

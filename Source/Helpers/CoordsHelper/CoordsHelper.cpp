//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "CoordsHelper.h"

namespace Bru
{

//===========================================================================//

Vector2 CoordsHelper::WorldToSurface(const Vector3 & worldCoords, const Vector3 & cameraPosition,
                                     ProjectionMode projectionMode)
{
	Vector2 screenCoords = GetUnProjection(worldCoords - cameraPosition, cameraPosition.Z - worldCoords.Z);
	screenCoords += Renderer->GetSurfaceCoordsCenter();
	return Renderer->CoordsToPixels(projectionMode, screenCoords);
}

//===========================================================================//

Vector3 CoordsHelper::SurfaceToWorld(const Vector2 & surfaceCoords, float targetZ,
                                     const Vector3 & cameraPosition, ProjectionMode projectionMode)
{
	Vector2 translatedPosition(Renderer->PixelsToCoords(projectionMode, surfaceCoords));
	translatedPosition -= Renderer->GetSurfaceCoordsCenter();

	Vector3 worldCoords = GetProjection(translatedPosition, cameraPosition.Z - targetZ, targetZ);

	return {worldCoords.X + cameraPosition.X, worldCoords.Y + cameraPosition.Y, worldCoords.Z};
}

//===========================================================================//

Vector3 CoordsHelper::GetProjectedOnEntityPoint(const SharedPtr<Entity> & entity, const Vector3 & point)
{
	Vector3 adaptedPoint = point;

	if (entity->GetComponent<RotationNode>() != NullPtr)
	{
		const auto & node = entity->GetComponent<PositioningNode>();
		const auto & rotationNode = entity->GetComponent<RotationNode>();

		SharedPtr<PositioningNode> pointNode = new PositioningNode(node->GetDomainName());
		node->AddChild(pointNode);

		pointNode->SetPosition(point - rotationNode->GetPosition());
		pointNode->Rotate2D(-rotationNode->GetAngleZ());

		adaptedPoint = pointNode->GetPosition();

		node->RemoveChild(pointNode);
	}

	return adaptedPoint;
}

//===========================================================================//

Vector3 CoordsHelper::GetProjection(const Vector2 & surfaceCoords, float deltaZ, float targetZ)
{
	return
	{
		surfaceCoords.X * deltaZ / Renderer->GetSurfaceCoordsHeightCenter(),
		surfaceCoords.Y * deltaZ / Renderer->GetSurfaceCoordsHeightCenter(),
		targetZ
	};
}

//===========================================================================//

Vector2 CoordsHelper::GetUnProjection(const Vector3 & worldCoords, float deltaZ)
{
	return
	{
		worldCoords.X * Renderer->GetSurfaceCoordsHeightCenter() / deltaZ,
		worldCoords.Y * Renderer->GetSurfaceCoordsHeightCenter() / deltaZ
	};
}

//===========================================================================//

} // namespace Bru

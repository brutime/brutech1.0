//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс представляющий консоль на экране
//

#pragma once
#ifndef CONSOLE_VIEW_H
#define CONSOLE_VIEW_H

#include "../../Common/EventSystem/InnerEvent.h"
#include "../../Common/EventSystem/Event.h"
#include "../../Positioning/Positioning.h"
#include "../../Widgets/Widgets.h"
#include "../../Utils/Console/ConsoleMessage.h"
#include "ConsoleViewInputReceiver.h"

namespace Bru
{

//===========================================================================//

class ConsoleViewSingleton
{
public:
	ConsoleViewSingleton(const string & layerName);
	~ConsoleViewSingleton();

	void Open();
	void Close();
	void Switch();

	void Clear();

	bool IsActive() const;

	void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	void DispatchMouseEvent(const MouseEventArgs & args);
	void DispatchJoystickEvent(const JoystickEventArgs & args);

private:
	static const string Name;
	
	static const KeyboardKey SwitchKey;

	static const string FontNamePath;
	static const string FontSizePath;
	static const string BackgroundColorPath;
	static const string ForegroundColorPath;
	static const string DefaultPrompt;

	void AddMessage(const ConsoleMessage & message);
	void ExecuteCommand(const string & command);
	void AutoCompleteCommand();
	
	void AutoCompleteCommandName();
	string GetSupplementedCommand(const List<string> & candidateCommands) const;
	
	void NextArgumentVariant();	

	void OnPointReached(const EmptyEventArgs &);

	void RegisterCommands();
	void UnregisterCommands();

	void PrintAvailableCommands();
	const Color & GetMessageColorByPriority(MessagePriority priority) const;

	SharedPtr<TextPanel> _panel;
	SharedPtr<InputPrompt> _commandLine;

	bool _isOnScreen;
	bool _isScrollDown;
	SharedPtr<MovementToPointController> _scrollController;
	
	Array<string> _argumentsLastValues;

	SharedPtr<ConsoleViewInputReceiver> _inputReceiver;

	SharedPtr<EventHandler<const ConsoleMessage &>> _newMessageHandler;
	SharedPtr<EventHandler<const string &>> _inputSubmittedHandler;
	SharedPtr<EventHandler<const EmptyEventArgs &>> _scrollPointReachedHandler;
	
	ConsoleViewSingleton(const ConsoleViewSingleton &) = delete;
	ConsoleViewSingleton& operator =(const ConsoleViewSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<ConsoleViewSingleton> ConsoleView;

//===========================================================================//

} // namespace Bru

#endif // CONSOLE_VIEW_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ConsoleViewInputReceiver.h"

#include "ConsoleView.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(ConsoleViewInputReceiver, InputReceiver)

//===========================================================================//

ConsoleViewInputReceiver::ConsoleViewInputReceiver() :
	InputReceiver(EntitySystem::GlobalDomainName)
{
}

//===========================================================================//

ConsoleViewInputReceiver::~ConsoleViewInputReceiver()
{
}

//===========================================================================//

void ConsoleViewInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	ConsoleView->DispatchKeyboardEvent(args);
}

//===========================================================================//

void ConsoleViewInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	ConsoleView->DispatchMouseEvent(args);
}

//===========================================================================//

void ConsoleViewInputReceiver::DispatchJoystickEvent(const JoystickEventArgs & args)
{
	ConsoleView->DispatchJoystickEvent(args);
}

//===========================================================================//

}// namespace Bru

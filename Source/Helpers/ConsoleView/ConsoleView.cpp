//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ConsoleView.h"

#include "../../ScriptSystem/ScriptSystem.h"
#include "../../Utils/Config/Config.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/Log/Log.h"
#include "../DomainManager/DomainManager.h"

namespace Bru
{

//===========================================================================//

SharedPtr<ConsoleViewSingleton> ConsoleView;

//===========================================================================//

const string ConsoleViewSingleton::Name = "Console view";

const KeyboardKey ConsoleViewSingleton::SwitchKey = KeyboardKey::Backquote;

const string ConsoleViewSingleton::FontNamePath = "ConsoleView.Name";
const string ConsoleViewSingleton::FontSizePath = "ConsoleView.Size";
const string ConsoleViewSingleton::BackgroundColorPath = "ConsoleView.BackgroundColor";
const string ConsoleViewSingleton::ForegroundColorPath = "ConsoleView.ForegroundColor";
const string ConsoleViewSingleton::DefaultPrompt = ">";

//===========================================================================//

ConsoleViewSingleton::ConsoleViewSingleton(const string & layerName) :
	_panel(),
	_commandLine(),
	_isOnScreen(false),
	_isScrollDown(false),
	_scrollController(),
	_argumentsLastValues(),
	_inputReceiver(),
	_newMessageHandler(new EventHandler<const ConsoleMessage &>(this, &ConsoleViewSingleton::AddMessage)),
	_inputSubmittedHandler(new EventHandler<const string &>(this, &ConsoleViewSingleton::ExecuteCommand)),
	_scrollPointReachedHandler(new EventHandler<const EmptyEventArgs &>(this, &ConsoleViewSingleton::OnPointReached))
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	WeakPtr<Font> font = FontManager->Load(Config->Get<string>(FontNamePath), Config->Get<integer>(FontSizePath));
	Color backgroundColor = Config->Get<Color>(BackgroundColorPath);
	Color fontColor = Config->Get<Color>(ForegroundColorPath);

	_commandLine = new InputPrompt(EntitySystem::GlobalDomainName, layerName, DefaultPrompt,
	                               0.5f, 0.0f, InputCursorOrientation::Horizontal, font, fontColor);

	_panel = new TextPanel(EntitySystem::GlobalDomainName, layerName, font, TextPanelAlignment::Down,
	                       0.5f, _commandLine->GetSize().Y + 0.5f, 100.0f, 50.0f, backgroundColor);
	_panel->MoveTo(0.0f, 100.0f);
	_panel->SetViewOrder(-1);

	_commandLine->MoveTo(0, -(integer) _commandLine->GetFont()->GetBaseLineDescender());
	_commandLine->GetInputReceiver()->Disable();
	_commandLine->InputSubmitted.AddHandler(_inputSubmittedHandler);

	_panel->AddChild(_commandLine);
	_panel->Hide();

	_scrollController = new MovementToPointController(_panel->GetPositioningNode(),
	                                                  Vector3(0.0f, 100.0f, 0.0f), 500.0f, 5.0f);
	_scrollController->PointReached.AddHandler(_scrollPointReachedHandler);

	_panel->DisableUpdating();
	for (const ConsoleMessage & message : Console->GetMessages())
	{
		_panel->AddLine(message.GetText(), GetMessageColorByPriority(message.GetPriority()));
	}
	Console->NewMessage.AddHandler(_newMessageHandler);

	InputSystem->AddKeyboardKeyUsingInGlobalDomainOnly(SwitchKey);
	_inputReceiver = new ConsoleViewInputReceiver();

	RegisterCommands();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

ConsoleViewSingleton::~ConsoleViewSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));

	UnregisterCommands();

	_inputReceiver = NullPtr;
	InputSystem->RemoveKeyboardKeyUsingInGlobalDomainOnly(SwitchKey);

	Console->NewMessage.RemoveHandler(_newMessageHandler);
	_commandLine->InputSubmitted.RemoveHandler(_inputSubmittedHandler);
	_scrollController->PointReached.RemoveHandler(_scrollPointReachedHandler);

	_scrollController = NullPtr;
	_commandLine = NullPtr;
	_panel = NullPtr;

	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void ConsoleViewSingleton::Open()
{
	if (_isOnScreen)
	{
		return;
	}

	InputSystem->EnableGlobalDomainOnlyMode();

	_panel->Show();
	_panel->EnableUpdating();

	_scrollController->SetPoint(Vector3(0.0f, 50.0f, 0.0f));
	_scrollController->Enable();
	_isOnScreen = true;
	_isScrollDown = true;
}

//===========================================================================//

void ConsoleViewSingleton::Close()
{
	if (!_isOnScreen)
	{
		return;
	}

	InputSystem->DisableGlobalDomainOnlyMode();

	_panel->DisableUpdating();

	_scrollController->SetPoint(Vector3(0.0f, 100.0f, 0.0f));
	_scrollController->Enable();
	_isOnScreen = false;
	_isScrollDown = false;
}

//===========================================================================//

void ConsoleViewSingleton::Switch()
{
	_isOnScreen ? Close() : Open();
}

//===========================================================================//

void ConsoleViewSingleton::Clear()
{
	_panel->Clear();
}

//===========================================================================//

void ConsoleViewSingleton::AddMessage(const ConsoleMessage & args)
{
	_panel->AddLine(args.GetText(), GetMessageColorByPriority(args.GetPriority()));
}

//===========================================================================//

void ConsoleViewSingleton::ExecuteCommand(const string & command)
{
	if (command.IsEmptyOrBlankCharsOnly())
	{
		Console->Command("");
	}
	else
	{
		if (IsActive())
		{
			_panel->DisableUpdating();
		}

		bool evaluateResult = Console->Evaluate(command);

		if (IsActive())
		{
			_panel->EnableUpdating();
		}

		if (!evaluateResult)
		{
			Console->ResetOffset();
		}
		_commandLine->Clear();
		_argumentsLastValues.Clear();
	}
}

//===========================================================================//

void ConsoleViewSingleton::AutoCompleteCommand()
{
	if (_commandLine->GetWordIndexAtCursor() == 0)
	{
		AutoCompleteCommandName();
	}
	else
	{
		NextArgumentVariant();
	}
}

//===========================================================================//

void ConsoleViewSingleton::AutoCompleteCommandName()
{
	string currentCommandToken = _commandLine->GetWordAtCursor();

	List<string> candidateCommands;
	for (auto & commandPair : ScriptSystem->GetEnabledCommands())
	{
		for (auto & commandVariantPair : commandPair.GetValue())
		{
			const string & commandName = commandVariantPair.GetValue()->GetName();
			if (commandName.ToUpper().FirstIndexOf(currentCommandToken.ToUpper()) == 0)
			{
				candidateCommands.Add(commandName);
			}
			break;
		}
	}

	if (candidateCommands.IsEmpty())
	{
		return;
	}

	integer wordIndex = _commandLine->GetWordIndexAtCursor();
	if (candidateCommands.GetCount() == 1)
	{
		_commandLine->ReplaceWord(wordIndex, candidateCommands[0]);
		if (_commandLine->GetWordsCount() == wordIndex + 1)
		{
			_commandLine->AddText(" ");
		}
	}
	else
	{
		_commandLine->ReplaceWord(wordIndex, GetSupplementedCommand(candidateCommands));

		for (const string & candidateCommand : candidateCommands)
		{
			Console->Hint(candidateCommand);
		}
	}
}

//===========================================================================//

string ConsoleViewSingleton::GetSupplementedCommand(const List<string> & candidateCommands) const
{
	if (candidateCommands.GetCount() == 0)
	{
		return string::Empty;
	}

	integer minLength = MaxInteger;
	for (const string & candidateCommand : candidateCommands)
	{
		if (candidateCommand.GetLength() < minLength)
		{
			minLength = candidateCommand.GetLength();
		}
	}

	string supplementedCommand;

	//Дописываем команду до первого несовпадающего символа
	for (integer i = 0; i < minLength; ++i)
	{
		bool equal = true;
		utf8_char currentChar = candidateCommands[0][i];
		for (integer j = 1; j < candidateCommands.GetCount(); ++j)
		{
			if (candidateCommands[j][i] != currentChar)
			{
				equal = false;
				break;
			}
		}

		if (equal)
		{
			supplementedCommand += currentChar;
		}
		else
		{
			break;
		}
	}

	return supplementedCommand;
}

//===========================================================================//

void ConsoleViewSingleton::NextArgumentVariant()
{
	if (_commandLine->GetWordsCount() == 0)
	{
		return;
	}

	integer wordIndex = _commandLine->GetWordIndexAtCursor();
	integer argIndex = wordIndex == -1 || wordIndex == 0 ? _commandLine->GetWordsCount() - 1 : wordIndex - 1;

	SharedPtr<IScriptCommand> command = ScriptSystem->GetCommand(_commandLine->GetWord(0));
	if (argIndex == command->GetArgsCount())
	{
		return;
	}

	const Array<string> & values = command->GetBoundedValues(argIndex);
	if (values.IsEmpty())
	{
		return;
	}

	string currentArgumentValue = _commandLine->GetWordAtCursor();
	integer indexOfCurrentArgumentValue = values.IndexOf(currentArgumentValue);
	indexOfCurrentArgumentValue++;
	if (indexOfCurrentArgumentValue == values.GetCount())
	{
		indexOfCurrentArgumentValue = 0;
	}
	if (wordIndex == -1)
	{
		_commandLine->AddText(values[indexOfCurrentArgumentValue]);
	}
	else
	{
		_commandLine->ReplaceWord(wordIndex, values[indexOfCurrentArgumentValue]);
	}
}

//===========================================================================//

void ConsoleViewSingleton::OnPointReached(const EmptyEventArgs &)
{
	if (_isScrollDown)
	{
		_panel->GetPositioningNode()->SetPosition(0.0f, 50.0f, 0.0f);
	}
	else
	{
		_panel->GetPositioningNode()->SetPosition(0.0f, 100.0f, 0.0f);
		_panel->Hide();
	}

	_scrollController->Disable();
}

//===========================================================================//

void ConsoleViewSingleton::RegisterCommands()
{
	ScriptSystem->RegisterCommand(new ClassCommand<ConsoleViewSingleton, void>(
	                                  "Help", this, &ConsoleViewSingleton::PrintAvailableCommands));
	ScriptSystem->RegisterCommand(new ClassCommand<ConsoleViewSingleton, void>(
	                                  "Clear", this, &ConsoleViewSingleton::Clear));
}

//===========================================================================//

void ConsoleViewSingleton::UnregisterCommands()
{
	ScriptSystem->UnregisterCommand("Clear");
	ScriptSystem->UnregisterCommand("Help");
}

//===========================================================================//

void ConsoleViewSingleton::PrintAvailableCommands()
{
	for (const string & command : ScriptSystem->GetEnabledCommandsList())
	{
		Console->Hint(command);
	}
}

//===========================================================================//

const Color & ConsoleViewSingleton::GetMessageColorByPriority(MessagePriority priority) const
{
	switch (priority)
	{
	case MessagePriority::Debug: return Colors::Gray70;
	case MessagePriority::Notice: return Colors::White;
	case MessagePriority::ConsoleHint: return Colors::LightSteelBlue;
	case MessagePriority::ConsoleCommand: return Colors::PaleGreen;
	case MessagePriority::Warning: return Colors::Gold;
	case MessagePriority::Error: return Colors::Red;

	default: INVALID_ARGUMENT_EXCEPTION(string::Format("priority {0} not defined", static_cast<integer>(priority)));
	}
}

//===========================================================================//

bool ConsoleViewSingleton::IsActive() const
{
	return _isOnScreen;
}

//===========================================================================//

void ConsoleViewSingleton::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	if (args.State == KeyState::Down && args.Key == SwitchKey)
	{
		Switch();
		return;
	}

	if (!_isOnScreen)
	{
		return;
	}

	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::Tab:
			AutoCompleteCommand();
			break;

		case KeyboardKey::Escape:
			_commandLine->Clear();
			_argumentsLastValues.Clear();
			break;

		case KeyboardKey::Home:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				_panel->ScrollToBegin();
			}
			break;

		case KeyboardKey::End:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				_panel->ScrollToEnd();
			}
			break;

		case KeyboardKey::PageUp:
			_panel->ScrollUp();
			break;

		case KeyboardKey::PageDown:
			_panel->ScrollDown();
			break;

		default:
			break;
		}
	}
	else if (args.State == KeyState::RepeatedDown)
	{
		switch (args.Key)
		{
		case KeyboardKey::PageUp:
			_panel->ScrollUp();
			break;

		case KeyboardKey::PageDown:
			_panel->ScrollDown();
			break;

		default:
			break;
		}
	}

	_commandLine->DispatchKeyboardEvent(args);
}

//===========================================================================//

void ConsoleViewSingleton::DispatchMouseEvent(const MouseEventArgs & args)
{
	if (!_isOnScreen)
	{
		return;
	}

	if (args.Key == MouseKey::Scroll)
	{
		if (args.State == KeyState::Up)
		{
			for (integer i = 0; i < args.ScrollClickCount; i++)
			{
				_panel->ScrollUp();
			}
		}
		else if (args.State == KeyState::Down)
		{
			for (integer i = 0; i < args.ScrollClickCount; i++)
			{
				_panel->ScrollDown();
			}
		}
	}

	_commandLine->DispatchMouseEvent(args);
}

//===========================================================================//

void ConsoleViewSingleton::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

} // namespace Bru

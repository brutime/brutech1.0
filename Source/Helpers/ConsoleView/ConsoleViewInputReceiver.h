//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef CONSOLE_VIEW_INPUT_RECEIVER_H
#define CONSOLE_VIEW_INPUT_RECEIVER_H

#include "../../OperatingSystem/OperatingSystem.h"

namespace Bru
{

//===========================================================================//

class ConsoleViewInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	ConsoleViewInputReceiver();
	virtual ~ConsoleViewInputReceiver();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);

private:
	ConsoleViewInputReceiver(const ConsoleViewInputReceiver &) = delete;
	ConsoleViewInputReceiver & operator =(const ConsoleViewInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // CONSOLE_VIEW_INPUT_RECEIVER_H

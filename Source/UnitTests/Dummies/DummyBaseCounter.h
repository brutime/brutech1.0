//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовая пустышка, подсчитывающая количество своих созданий и удалений
//

#ifndef DUMMY_BASE_COUNTER_H
#define DUMMY_BASE_COUNTER_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class DummyBaseCounter
{
public:
	DummyBaseCounter();
	DummyBaseCounter(const DummyBaseCounter & other);
	virtual ~DummyBaseCounter();

	virtual string GetValue() const;

	static void Reset();

	static integer GetCreatedCount();
	static integer GetDeletedCount();

private:
	static integer createdCount;
	static integer deletedCount;

	DummyBaseCounter & operator =(const DummyBaseCounter &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // DUMMY_BASE_COUNTER_H

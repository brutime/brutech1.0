//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, наследуемый от DummyChild, нужен для тестов, связанный с
//              иерархией классов.
//

#pragma once
#ifndef DUMMY_CHILD_H
#define DUMMY_CHILD_H

#include "DummyBase.h"

namespace Bru
{

//===========================================================================//

class DummyChild : public DummyBase
{
public:
	DummyChild();
	virtual ~DummyChild();

	virtual string GetValue() const;
};

//===========================================================================//

}//namespace

#endif // DUMMY_CHILD_H

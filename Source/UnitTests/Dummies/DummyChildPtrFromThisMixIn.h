//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый класс для производных классов (К.О. :)), нужен для тестов, связанный с
//              иерархией классов.
//

#pragma once
#ifndef DUMMY_PTR_FROM_THIS_MIX_IN_INHERITOR_H
#define DUMMY_PTR_FROM_THIS_MIX_IN_INHERITOR_H

#include "DummyBasePtrFromThisMixIn.h"

namespace Bru
{

//===========================================================================//

class DummyChildPtrFromThisMixIn : public DummyBasePtrFromThisMixIn
{
public:
	DummyChildPtrFromThisMixIn();
	virtual ~DummyChildPtrFromThisMixIn();

private:
	DummyChildPtrFromThisMixIn(const DummyChildPtrFromThisMixIn &) = delete;
	DummyChildPtrFromThisMixIn & operator =(const DummyChildPtrFromThisMixIn &) = delete;
};

//===========================================================================//

}//namespace

#endif // DUMMY_PTR_FROM_THIS_MIX_IN_INHERITOR_H

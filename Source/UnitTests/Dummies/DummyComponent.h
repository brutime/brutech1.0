//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ������ ��� ������: ������ �������� ��������
//

#pragma once
#ifndef DUMMY_COMPONENT_H
#define DUMMY_COMPONENT_H

#include "../../Common/Common.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

class DummyComponent
{
public:
	DummyComponent();
	DummyComponent(integer value);
	~DummyComponent();

	bool operator ==(const DummyComponent & someDummyComponent) const;

	void SetValue(integer value);
	integer GetValue() const;

	static int UsefulDummies;

private:
	integer _value;

	DummyComponent & operator =(const DummyComponent &) = delete;
};

//===========================================================================//

}//namespace Bru

#endif // DUMMY_COMPONENT_H

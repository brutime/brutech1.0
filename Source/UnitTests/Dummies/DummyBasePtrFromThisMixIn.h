//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый класс для производных классов (К.О. :)), нужен для тестов, связанный с
//              иерархией классов.
//

#pragma once
#ifndef DUMMY_BASE_PTR_FROM_THIS_MIX_IN
#define DUMMY_BASE_PTR_FROM_THIS_MIX_IN

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class DummyBasePtrFromThisMixIn : public PtrFromThisMixIn<DummyBasePtrFromThisMixIn>
{
public:
	DummyBasePtrFromThisMixIn();
	virtual ~DummyBasePtrFromThisMixIn();

private:
	DummyBasePtrFromThisMixIn(const DummyBasePtrFromThisMixIn &) = delete;
	DummyBasePtrFromThisMixIn & operator =(const DummyBasePtrFromThisMixIn &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // DUMMY_PTR_FROM_THIS_MIX_IN

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Дочерняя пустышка, подсчитывающая количество своих созданий и удалений
//

#ifndef DUMMY_COUNTER_H
#define DUMMY_COUNTER_H

#include "DummyBaseCounter.h"

namespace Bru
{

//===========================================================================//

class DummyChildCounter : public DummyBaseCounter
{
public:
	DummyChildCounter();
	DummyChildCounter(const DummyChildCounter & other);
	virtual ~DummyChildCounter();

	virtual string GetValue() const;

private:
	DummyChildCounter & operator =(const DummyChildCounter &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // DUMMY_COUNTER_H

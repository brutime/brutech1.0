//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DummyBaseCounter.h"

namespace Bru
{

//===========================================================================//

integer DummyBaseCounter::createdCount = 0;
integer DummyBaseCounter::deletedCount = 0;

//===========================================================================//

DummyBaseCounter::DummyBaseCounter()
{
	createdCount++;
}

//===========================================================================//

DummyBaseCounter::DummyBaseCounter(const DummyBaseCounter &)
{
	createdCount++;
}

//===========================================================================//

DummyBaseCounter::~DummyBaseCounter()
{
	deletedCount++;
}

//===========================================================================//

string DummyBaseCounter::GetValue() const
{
	return "DummyBaseCounter";
}

//===========================================================================//

void DummyBaseCounter::Reset()
{
	createdCount = 0;
	deletedCount = 0;
}

//===========================================================================//

integer DummyBaseCounter::GetCreatedCount()
{
	return createdCount;
}

//===========================================================================//

integer DummyBaseCounter::GetDeletedCount()
{
	return deletedCount;
}

//===========================================================================//

} //namespace Bru


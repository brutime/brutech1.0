//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ������ ��� ������: ��������� ������, ��������� � ����
//    DummyComponent � ������� �������� �����������
//

#pragma once
#ifndef DUMMY_COMPOUND_OBJECT_H
#define DUMMY_COMPOUND_OBJECT_H

#include "../../Common/Common.h"
#include "DummyComponent.h"

namespace Bru
{

//===========================================================================//

class DummyCompoundObject
{
public:
	class AscendingComparator : public IComparator<DummyCompoundObject>
	{
	public:
		bool Compare(const DummyCompoundObject & first, const DummyCompoundObject & second) const
		{
			return first._component->GetValue() < second._component->GetValue();
		}
	};

	class DescendingComparator : public IComparator<DummyCompoundObject>
	{
	public:
		bool Compare(const DummyCompoundObject & first, const DummyCompoundObject & second) const
		{
			return first._component->GetValue() > second._component->GetValue();
		}
	};

	DummyCompoundObject();
	DummyCompoundObject(integer someValue);
	DummyCompoundObject(const DummyCompoundObject & other);
	~DummyCompoundObject();

	DummyCompoundObject & operator =(const DummyCompoundObject & other);

	bool operator ==(const DummyCompoundObject & dummyCompoundObject) const;
	bool operator !=(const DummyCompoundObject & dummyCompoundObject) const;

	bool operator <(const DummyCompoundObject & dummyCompoundObject) const;
	bool operator <=(const DummyCompoundObject & dummyCompoundObject) const;

	bool operator >(const DummyCompoundObject & dummyCompoundObject) const;
	bool operator >=(const DummyCompoundObject & dummyCompoundObject) const;

	operator string() const;

	void SetValue(const integer someValue);
	integer GetValue() const;

	static bool IsEqualsToValue(DummyCompoundObject * compoundObject, integer value);

private:
	SharedPtr<DummyComponent> _component;

	void DeepCopy(const DummyCompoundObject & other);
};

//===========================================================================//

extern std::ostream & operator <<(std::ostream & stream, const DummyCompoundObject & dummyCompoundObject);

//===========================================================================//

}//namespace

#endif // DUMMY_COMPOUND_OBJECT_H

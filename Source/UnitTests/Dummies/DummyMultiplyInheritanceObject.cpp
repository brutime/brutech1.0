//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DummyMultiplyInheritanceObject.h"

namespace Bru
{

//===========================================================================//

DummyMultiplyInheritanceObject::DummyMultiplyInheritanceObject() :
	DummyInterface1(),
	DummyInterface2(),
	Writed(false)
{
}

//===========================================================================//

DummyMultiplyInheritanceObject::~DummyMultiplyInheritanceObject()
{

}

//===========================================================================//

void DummyMultiplyInheritanceObject::Write()
{
}

//===========================================================================//

void DummyMultiplyInheritanceObject::WriteLineEnd()
{
	Writed = true;
}

//===========================================================================//

void DummyMultiplyInheritanceObject::Read()
{
}

//===========================================================================//

void DummyMultiplyInheritanceObject::WriteDummy(SharedPtr<DummyInterface2> i)
{
	i->WriteLine();
}
//===========================================================================//

}// namespace Bru

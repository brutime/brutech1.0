//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс для использования в множественном наследовании
//

#pragma once
#ifndef DUMMY_INTERFACE_1_H
#define DUMMY_INTERFACE_1_H

namespace Bru
{

//===========================================================================//

class DummyInterface1
{
public:
	DummyInterface1();
	virtual ~DummyInterface1();

	virtual void Read() = 0;

private:
	DummyInterface1(const DummyInterface1 &) = delete;
	DummyInterface1 & operator =(const DummyInterface1 &) = delete;
};

//===========================================================================//

}//namespace

#endif // DUMMY_INTERFACE_1_H

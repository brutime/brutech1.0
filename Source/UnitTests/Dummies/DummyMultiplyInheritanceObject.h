//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Объект, наследующийся от нескольких интерфейсов
//

#pragma once
#ifndef DUMMY_MULTIPLY_INHERITANCE_OBJECT_H
#define DUMMY_MULTIPLY_INHERITANCE_OBJECT_H

#include "../../Common/Common.h"
#include "DummyInterface1.h"
#include "DummyInterface2.h"

namespace Bru
{

//===========================================================================//

class DummyMultiplyInheritanceObject : public DummyInterface1, public DummyInterface2
{
public:
	DummyMultiplyInheritanceObject();
	virtual ~DummyMultiplyInheritanceObject();

	virtual void Write();
	virtual void WriteLineEnd();
	virtual void Read();
	static void WriteDummy(SharedPtr<DummyInterface2> i);

	bool Writed;

private:
	DummyMultiplyInheritanceObject(const DummyMultiplyInheritanceObject &) = delete;
	DummyMultiplyInheritanceObject & operator =(const DummyMultiplyInheritanceObject &) = delete;
};

//===========================================================================//

}//namespace

#endif // DUMMY_MULTIPLY_INHERITANCE_OBJECT_H

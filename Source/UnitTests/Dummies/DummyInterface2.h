//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс для использования в множественном наследовании
//

#pragma once
#ifndef DUMMY_INTERFACE_2_H
#define DUMMY_INTERFACE_2_H

namespace Bru
{
//===========================================================================//

class DummyInterface2
{
public:
	DummyInterface2();
	virtual ~DummyInterface2();

	virtual void Write() = 0;
	virtual void WriteLine();

	virtual void WriteLineEnd() = 0;

private:
	DummyInterface2(const DummyInterface2 &) = delete;
	DummyInterface2 & operator =(const DummyInterface2 &) = delete;
};

//===========================================================================//

}//namespace

#endif // DUMMY_INTERFACE_2_H

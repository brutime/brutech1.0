//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DummyCompoundObject.h"

#include "../../Common/Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

DummyCompoundObject::DummyCompoundObject() :
	_component(new DummyComponent(0))
{
}

//===========================================================================//

DummyCompoundObject::DummyCompoundObject(integer value) :
	_component(new DummyComponent(value))
{
}

//===========================================================================//

DummyCompoundObject::DummyCompoundObject(const DummyCompoundObject & other) :
	_component(NullPtr)
{
	DeepCopy(other);
}

//===========================================================================//

DummyCompoundObject & DummyCompoundObject::operator =(const DummyCompoundObject & other)
{
	DeepCopy(other);
	return *this;
}

//===========================================================================//

DummyCompoundObject::~DummyCompoundObject()
{
	_component = NullPtr;
}

//===========================================================================//

bool DummyCompoundObject::operator ==(const DummyCompoundObject & someDummyCompoundObject) const
{
	return _component->GetValue() == someDummyCompoundObject._component->GetValue();
}

//===========================================================================//

bool DummyCompoundObject::operator !=(const DummyCompoundObject & someDummyCompoundObject) const
{
	return _component->GetValue() != someDummyCompoundObject._component->GetValue();
}

//===========================================================================//

bool DummyCompoundObject::operator <(const DummyCompoundObject & someDummyCompoundObject) const
{
	return _component->GetValue() < someDummyCompoundObject._component->GetValue();
}

//===========================================================================//

bool DummyCompoundObject::operator <=(const DummyCompoundObject & someDummyCompoundObject) const
{
	return _component->GetValue() <= someDummyCompoundObject._component->GetValue();
}

//===========================================================================//

bool DummyCompoundObject::operator >(const DummyCompoundObject & someDummyCompoundObject) const
{
	return _component->GetValue() > someDummyCompoundObject._component->GetValue();
}

//===========================================================================//

bool DummyCompoundObject::operator >=(const DummyCompoundObject & someDummyCompoundObject) const
{
	return _component->GetValue() >= someDummyCompoundObject._component->GetValue();
}

//===========================================================================//

DummyCompoundObject::operator string() const
{
	return _component->GetValue();
}

//===========================================================================//

void DummyCompoundObject::SetValue(const integer someValue)
{
	_component->SetValue(someValue);
}

//===========================================================================//

integer DummyCompoundObject::GetValue() const
{
	return _component->GetValue();
}

//===========================================================================//

bool DummyCompoundObject::IsEqualsToValue(DummyCompoundObject * compoundObject, integer value)
{
	return compoundObject->GetValue() == value;
}

//===========================================================================//

void DummyCompoundObject::DeepCopy(const DummyCompoundObject & other)
{
	if (this != &other)
	{
		if (_component != NullPtr)
		{
			_component = NullPtr;
		}

		_component = new DummyComponent(other._component->GetValue());
	}
}

//===========================================================================//

std::ostream & operator <<(std::ostream & stream, const DummyCompoundObject & dummyCompoundObject)
{
	stream << string(dummyCompoundObject).ToChars();
	return stream;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый класс для производных классов (К.О. :)), нужен для тестов, связанный с
//              иерархией классов.
//

#pragma once
#ifndef DUMMY_BASE_H
#define DUMMY_BASE_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class DummyBase
{
public:
	DummyBase();
	virtual ~DummyBase();

	virtual string GetValue() const;

private:
	DummyBase(const DummyBase &) = delete;
	DummyBase & operator =(const DummyBase &) = delete;
};

//===========================================================================//

}//namespace

#endif // DUMMY_CHILD_H

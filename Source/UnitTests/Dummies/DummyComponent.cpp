//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DummyComponent.h"

namespace Bru
{

//===========================================================================//

int DummyComponent::UsefulDummies = 0;

//===========================================================================//

DummyComponent::DummyComponent() :
	_value(0)
{
}

//===========================================================================//

DummyComponent::~DummyComponent()
{
	_value = 0;
}

//===========================================================================//

DummyComponent::DummyComponent(integer value) :
	_value(value)
{
	UsefulDummies++;
}

//===========================================================================//

bool DummyComponent::operator ==(const DummyComponent & dummyComponent) const
{
	return _value == dummyComponent._value;
}

//===========================================================================//

void DummyComponent::SetValue(const integer value)
{
	_value = value;
}

//===========================================================================//

integer DummyComponent::GetValue() const
{
	return _value;
}

//===========================================================================//

}// namespace Bru

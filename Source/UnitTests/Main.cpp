//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Входная точка тестов
//

#include "../../Libs/Brunit/Brunit.h"
#include "Helpers/ApplicationForUnitTests.h"

using namespace Bru;
using namespace Brunit;

int main(int, char const *[])
{
	SharedPtr<ApplicationForUnitTests> applicationForUnitTest = new ApplicationForUnitTests();
	applicationForUnitTest->Initialize();

	TestRunner * testRunner = new TestRunner();
	int result = testRunner->RunAllTests<Exception>(new OSConsoleOutputStream());
	delete testRunner;

	applicationForUnitTest->FreeResources();

	return result;
}

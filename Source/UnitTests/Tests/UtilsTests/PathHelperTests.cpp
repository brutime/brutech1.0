//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты путевого помощника
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Utils/PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

TEST(PathHelperGetPathWithoutExtension)
{
	ARE_EQUAL("SomeImage", PathHelper::GetPathWithoutExtension("SomeImage"));
	ARE_EQUAL("SomeImage", PathHelper::GetPathWithoutExtension("SomeImage.tga"));
	ARE_EQUAL("Data/Images/SomeImage", PathHelper::GetPathWithoutExtension("Data/Images/SomeImage"));
	ARE_EQUAL("Data/Images/SomeImage", PathHelper::GetPathWithoutExtension("Data/Images/SomeImage.tga"));
}

//===========================================================================//

TEST(PathHelperGetPathWithoutExtensionExceptions)
{
	IS_THROW(PathHelper::GetPathWithoutExtension(string::Empty), InvalidArgumentException);
	IS_THROW(PathHelper::GetPathWithoutExtension(".tga"), InvalidArgumentException);
}

//===========================================================================//

TEST(PathHelperGetFileName)
{
	ARE_EQUAL("SomeImage", PathHelper::GetFileName("SomeImage"));
	ARE_EQUAL("SomeImage.tga", PathHelper::GetFileName("SomeImage.tga"));
	ARE_EQUAL("SomeImage", PathHelper::GetFileName("Data/Images/SomeImage"));
	ARE_EQUAL("SomeImage.tga", PathHelper::GetFileName("Data/Images/SomeImage.tga"));
	ARE_EQUAL("SomeImage.tga", PathHelper::GetFileName("Data/Images/SomeImage.tga", 1));
	ARE_EQUAL("Images/SomeImage.tga", PathHelper::GetFileName("Data/Images/SomeImage.tga", 2));
	ARE_EQUAL("Data/Images/SomeImage.tga", PathHelper::GetFileName("Data/Images/SomeImage.tga", 3));
}

//===========================================================================//

TEST(PathHelperGetFileNameExceptions)
{
	IS_THROW(PathHelper::GetFileName(string::Empty), InvalidArgumentException);
	IS_THROW(PathHelper::GetFileName("Data/Images/SomeImage.tga", -1), InvalidArgumentException);
	IS_THROW(PathHelper::GetFileName("Data/Images/SomeImage.tga", 0), InvalidArgumentException);
	IS_THROW(PathHelper::GetFileName("Data/Images/SomeImage.tga", 4), InvalidArgumentException);
}

//===========================================================================//

TEST(PathHelperGetFileNameWithoutExtension)
{
	ARE_EQUAL("SomeImage", PathHelper::GetFileNameWithoutExtension("SomeImage.tga"));
	ARE_EQUAL("SomeImage", PathHelper::GetFileNameWithoutExtension("Data/Images/SomeImage.tga"));
	ARE_EQUAL("SomeImage.1", PathHelper::GetFileNameWithoutExtension("Data/Images/SomeImage.1.tga"));
	ARE_EQUAL("SomeImage", PathHelper::GetFileNameWithoutExtension("Data/Images./SomeImage.tga"));
	ARE_EQUAL("SomeImage", PathHelper::GetFileNameWithoutExtension("Data./Images../SomeImage.tga"));
}

//===========================================================================//

TEST(PathHelperGetFileNameWithoutExtensionExceptions)
{
	IS_THROW(PathHelper::GetFileNameWithoutExtension(string::Empty), InvalidArgumentException);
	IS_THROW(PathHelper::GetFileNameWithoutExtension("NotValidPath"), InvalidArgumentException);
	IS_THROW(PathHelper::GetFileNameWithoutExtension("Data/Images/SomeImage"), InvalidArgumentException);
}

//===========================================================================//

TEST(PathHelperGetExtension)
{
	ARE_EQUAL("tga", PathHelper::GetExtension("SomeImage.tga"));
	ARE_EQUAL("tga", PathHelper::GetExtension("Data/Images/SomeImage.tga"));
	ARE_EQUAL("tga", PathHelper::GetExtension("Data/Images/SomeImage.1.tga"));
	ARE_EQUAL("tga", PathHelper::GetExtension("Data/Images./SomeImage.tga"));
	ARE_EQUAL("tga", PathHelper::GetExtension("Data./Images../SomeImage.tga"));
}

//===========================================================================//

TEST(PathHelperGetExtensionExceptions)
{
	IS_THROW(PathHelper::GetExtension(string::Empty), InvalidArgumentException);
	IS_THROW(PathHelper::GetExtension("NotValidPath"), InvalidArgumentException);
	IS_THROW(PathHelper::GetExtension("Data/Images/SomeImage"), InvalidArgumentException);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты лог файла
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Utils/Log/Log.h"
#include "../../../Utils/Log/HTMLLogWriter.h"

namespace Bru
{

//===========================================================================//

const string SimpleTextFileName = "TestSimple.txt";
const string SimpleHTMLFileName = "TestSimple.html";

//===========================================================================//

string debugString = "Debug";

string expectedHTMLDebugString = string("<font color = #B3B3B3>")
                                 + HTMLLogWriter::ConvertTextSymbolsToHTML(LogSingleton::DebugPriorityTitle)
                                 + HTMLLogWriter::ConvertTextSymbolsToHTML(debugString) + string("</font><br>");

//===========================================================================//

string noticeString = "Notice";

string expectedHTMLNoticeString = string("<font color = #FFFFFF>")
                                  + HTMLLogWriter::ConvertTextSymbolsToHTML(LogSingleton::NoticePriorityTitle)
                                  + HTMLLogWriter::ConvertTextSymbolsToHTML(noticeString) + string("</font><br>");

//===========================================================================//

string consoleHintString = "Log hint";

string expectedHTMLConsoleHintString = string("<font color = #8E8EBC>")
        + HTMLLogWriter::ConvertTextSymbolsToHTML(LogSingleton::ConsoleHintPriorityTitle)
        + HTMLLogWriter::ConvertTextSymbolsToHTML(consoleHintString) + string("</font><br>");

//===========================================================================//

string consoleCommandString = "Log command";

string expectedHTMLConsoleCommandString = string("<font color = #8EBC8E>")
        + HTMLLogWriter::ConvertTextSymbolsToHTML(LogSingleton::ConsoleCommandPriorityTitle)
        + HTMLLogWriter::ConvertTextSymbolsToHTML(consoleCommandString) + string("</font><br>");

//===========================================================================//

string warningString = "Warning";

string expectedHTMLWarningString = string("<font color = #CC7F32>")
                                   + HTMLLogWriter::ConvertTextSymbolsToHTML(LogSingleton::WarningPriorityTitle)
                                   + HTMLLogWriter::ConvertTextSymbolsToHTML(warningString) + string("</font><br>");

//===========================================================================//

string errorString = "Error";

string expectedHTMLErrorString = string("<font color = #FF3333>")
                                 + HTMLLogWriter::ConvertTextSymbolsToHTML(LogSingleton::ErrorPriorityTitle)
                                 + HTMLLogWriter::ConvertTextSymbolsToHTML(errorString) + string("</font><br>");

//===========================================================================//

string replacingSymbolsString = "\t\t|&<Строка с заменяемыми символами>&|\n\n\n";

string expectedHTMLReplacingSymbolsString = "<font color = #FFFFFF>[Notice]:&nbsp;&nbsp;"
        "&#09;&#09;&#166;&#38;&#60;Строка&nbsp;с&nbsp;заменяемыми&nbsp;символами&#62;&#38;&#166;<br>";

//===========================================================================//

void RemoveTextLogFile()
{
	if (FileSystem::FileExists(SimpleTextFileName))
	{
		FileSystem::RemoveFile(SimpleTextFileName);
	}
}

//===========================================================================//

void RemoveHTMLLogFile()
{
	if (FileSystem::FileExists(SimpleHTMLFileName))
	{
		FileSystem::RemoveFile(SimpleHTMLFileName);
	}
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(TextLogOpenClose, RemoveTextLogFile, RemoveTextLogFile)
{
	SharedPtr<LogSingleton> log = new LogSingleton(SimpleTextFileName, LogType::Text);
	log = NullPtr;

	IS_TRUE(FileSystem::FileExists(SimpleTextFileName));
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(TextLogSimple, RemoveTextLogFile, RemoveTextLogFile)
{
	SharedPtr<LogSingleton> log = new LogSingleton(SimpleTextFileName, LogType::Text);

	log->WriteMessage(MessagePriority::Debug, debugString);
	log->WriteMessage(MessagePriority::Notice, noticeString);
	log->WriteMessage(MessagePriority::ConsoleHint, consoleHintString);
	log->WriteMessage(MessagePriority::ConsoleCommand, consoleCommandString);
	log->WriteMessage(MessagePriority::Warning, warningString);
	log->WriteMessage(MessagePriority::Error, errorString);

	log = NullPtr;

	SharedPtr<TextFileStream> logFile = new TextFileStream(SimpleTextFileName, FileAccess::Read);

	ARE_EQUAL(LogSingleton::HeaderTitle, logFile->ReadLine());
	ARE_EQUAL(string::Empty, logFile->ReadLine());

	ARE_EQUAL(LogSingleton::DebugPriorityTitle + debugString, logFile->ReadLine());
	ARE_EQUAL(LogSingleton::NoticePriorityTitle + noticeString, logFile->ReadLine());
	ARE_EQUAL(LogSingleton::ConsoleHintPriorityTitle + consoleHintString, logFile->ReadLine());
	ARE_EQUAL(LogSingleton::ConsoleCommandPriorityTitle + consoleCommandString, logFile->ReadLine());
	ARE_EQUAL(LogSingleton::WarningPriorityTitle + warningString, logFile->ReadLine());
	ARE_EQUAL(LogSingleton::ErrorPriorityTitle + errorString, logFile->ReadLine());

	ARE_EQUAL(string::Empty, logFile->ReadLine());
	ARE_EQUAL(LogSingleton::FooterTitle, logFile->ReadLine());

	logFile->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(HTMLLogOpenClose, RemoveTextLogFile, RemoveTextLogFile)
{
	SharedPtr<LogSingleton> log = new LogSingleton(SimpleHTMLFileName, LogType::HTML);
	log = NullPtr;

	IS_TRUE(FileSystem::FileExists(SimpleHTMLFileName));
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(HTMLLogSimple, RemoveTextLogFile, RemoveTextLogFile)
{
	SharedPtr<LogSingleton> log = new LogSingleton(SimpleHTMLFileName, LogType::HTML);

	log->WriteMessage(MessagePriority::Debug, debugString);
	log->WriteMessage(MessagePriority::Notice, noticeString);
	log->WriteMessage(MessagePriority::ConsoleHint, consoleHintString);
	log->WriteMessage(MessagePriority::ConsoleCommand, consoleCommandString);
	log->WriteMessage(MessagePriority::Warning, warningString);
	log->WriteMessage(MessagePriority::Error, errorString);
	log->WriteMessage(MessagePriority::Notice, replacingSymbolsString);

	log = NullPtr;

	SharedPtr<TextFileStream> textFile = new TextFileStream(SimpleHTMLFileName, FileAccess::Read);

	ARE_EQUAL("<html>", textFile->ReadLine());
	ARE_EQUAL("<head>", textFile->ReadLine());
	ARE_EQUAL("<meta http-equiv = \"Content-Type\" content = \"text/html; charset = utf-8\">",
	                  textFile->ReadLine());
	ARE_EQUAL(string::Format("<title>{0}</title>", LogSingleton::ShortHeaderTitle), textFile->ReadLine());
	ARE_EQUAL("</head>", textFile->ReadLine());
	ARE_EQUAL("<body bgcolor = \"#101010\" text = \"#FFFFFF\">", textFile->ReadLine());
	ARE_EQUAL("<font size = \"2\" face = \"Courier New\">", textFile->ReadLine());
	ARE_EQUAL("<p style = \"word-wrap: break-word; width: 100%; left: 0\">", textFile->ReadLine());
	ARE_EQUAL(string::Format("{0}<br>", LogSingleton::HeaderTitle), textFile->ReadLine());
	ARE_EQUAL("<br>", textFile->ReadLine());

	ARE_EQUAL(expectedHTMLDebugString, textFile->ReadLine());
	ARE_EQUAL(expectedHTMLNoticeString, textFile->ReadLine());
	ARE_EQUAL(expectedHTMLConsoleHintString, textFile->ReadLine());
	ARE_EQUAL(expectedHTMLConsoleCommandString, textFile->ReadLine());
	ARE_EQUAL(expectedHTMLWarningString, textFile->ReadLine());
	ARE_EQUAL(expectedHTMLErrorString, textFile->ReadLine());
	ARE_EQUAL(expectedHTMLReplacingSymbolsString, textFile->ReadLine());

	ARE_EQUAL("<br>", textFile->ReadLine());
	ARE_EQUAL("<br>", textFile->ReadLine());
	ARE_EQUAL("</font><br>", textFile->ReadLine());

	ARE_EQUAL("<br>", textFile->ReadLine());
	ARE_EQUAL(LogSingleton::FooterTitle + string("<br>"), textFile->ReadLine());
	ARE_EQUAL("</p>", textFile->ReadLine());
	ARE_EQUAL("</font>", textFile->ReadLine());
	ARE_EQUAL("</body>", textFile->ReadLine());
	ARE_EQUAL("</html>", textFile->ReadLine());

	textFile->Close();
}

//===========================================================================//

}// namespace Bru

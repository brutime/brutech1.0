//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты файла параметров
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../FileSystem/FileSystem.h"
#include "../../../Utils/ParametersFile/ParametersFile.h"
#include "../../../Utils/PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

const string TestParametersFileName = "Test";
const string TestParametersFileFullName = PathHelper::PathToData + TestParametersFileName + "." + PathHelper::ParametersFileExtension;

//===========================================================================//

void RemoveParametersFile()
{
	if (FileSystem::FileExists(TestParametersFileFullName))
	{
		FileSystem::RemoveFile(TestParametersFileFullName);
	}
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(ParametersFileConstructor, RemoveParametersFile, RemoveParametersFile)
{
	//Write

	SharedPtr<ParametersFile> writeFile = new ParametersFile(TestParametersFileName, FileAccess::Write);

	ARE_EQUAL(ParametersFile::RootSection, writeFile->_rootSection->GetName());
	IS_TRUE(writeFile->_rootSection->GetParent() == NullPtr);
	IS_TRUE(writeFile->IsOpened());
	ARE_EQUAL(TestParametersFileFullName, writeFile->_pathToFile);
	IS_TRUE(writeFile->_access == FileAccess::Write);

	writeFile->Close();

	IS_TRUE(!writeFile->IsOpened());

	//ReadWrite

	//Создаем файл-пустышку, чтобы проверить его открытие
	SharedPtr<TextFileStream> textFile = new TextFileStream(TestParametersFileFullName, FileAccess::Write);
	textFile->Close();

	SharedPtr<ParametersFile> readWriteFile = new ParametersFile(TestParametersFileName, FileAccess::ReadWrite);

	IS_TRUE(readWriteFile->IsOpened());
	ARE_EQUAL(TestParametersFileFullName, readWriteFile->_pathToFile);
	IS_TRUE(readWriteFile->_access == FileAccess::ReadWrite);

	readWriteFile->Close();

	IS_TRUE(!readWriteFile->IsOpened());

	//Read

	SharedPtr<ParametersFile> readFile = new ParametersFile(TestParametersFileName, FileAccess::Read);

	IS_TRUE(readFile->IsOpened());
	ARE_EQUAL(TestParametersFileFullName, readFile->_pathToFile);
	IS_TRUE(readFile->_access == FileAccess::Read);

	readFile->Close();

	IS_TRUE(!readFile->IsOpened());

	//Append

	SharedPtr<ParametersFile> appendFile = new ParametersFile(TestParametersFileName, FileAccess::Append);

	IS_TRUE(appendFile->IsOpened());
	ARE_EQUAL(TestParametersFileFullName, appendFile->_pathToFile);
	IS_TRUE(appendFile->_access == FileAccess::Append);

	appendFile->Close();

	IS_TRUE(!appendFile->IsOpened());
}

//===========================================================================//

TEST(ParametersFileConstructorExceptions)
{
	IS_THROW(ParametersFile(TestParametersFileFullName, FileAccess::Read), FileNotFoundException);
	IS_THROW(ParametersFile(TestParametersFileFullName, FileAccess::Append), FileNotFoundException);
	IS_THROW(ParametersFile(TestParametersFileFullName, FileAccess::ReadWrite), FileNotFoundException);
}

//===========================================================================//

TEST(ParametersFileWithoutLastCRLF)
{
	//Этот тест просто не должен вызывать исключений
	ParametersFile pf("Text/UnitTests/FileWithoutLastCRLF", FileAccess::Read);
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(ParametersFileWriteReadValues, RemoveParametersFile, RemoveParametersFile)
{
	SharedPtr<ParametersFile> writeFile = new ParametersFile(TestParametersFileName, FileAccess::Write);

	writeFile->Set<byte>("RootByteПараметер", byte(251));
	writeFile->Set<bool>("RootBoolПараметер", true);
	writeFile->Set<integer>("RootIntegerПараметер", 666);
	writeFile->Set<float>("RootFloatПараметер", 666.666f);
	writeFile->Set<string>("RootStringПараметер", "текст с . dots,  spaces,\ttabs and \"''\" а также ¥Ω и ௵");
	writeFile->Set<string>("RootEmptyStringПараметер", "");
	writeFile->Set<string>("0.Name", "One");
	writeFile->Set<string>("1.Name", "Two");
	writeFile->Set<Vector3>("RootМатематическийVector3", Vector3(0.1f, 0.2f, 0.3f));
	writeFile->Set<string>("RootСписок", "0, 1, 2, 3, 4");
	writeFile->Set("НулеваяСекция", NullPtr);

	writeFile->Save();
	writeFile->Close();

	SharedPtr<ParametersFile> readFile = new ParametersFile(TestParametersFileName, FileAccess::Read);

	ARE_EQUAL(byte(251), readFile->Get<byte>("RootByteПараметер"));
	IS_TRUE(readFile->Get<bool>("RootBoolПараметер"));
	ARE_EQUAL(666, readFile->Get<integer>("RootIntegerПараметер"));
	ARE_CLOSE(666.666, readFile->Get<float>("RootFloatПараметер"), Math::Accuracy);
	ARE_EQUAL("текст с . dots,  spaces,\ttabs and \"''\" а также ¥Ω и ௵", readFile->Get<string>("RootStringПараметер"));
	ARE_EQUAL("", readFile->Get<string>("RootEmptyStringПараметер"));
	ARE_EQUAL("One", readFile->Get<string>("0.Name"));
	ARE_EQUAL("Two", readFile->Get<string>("1.Name"));
	Vector3 vector = readFile->Get<Vector3>("RootМатематическийVector3");
	ARE_CLOSE(0.1f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.2f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.3f, vector.Z, Math::Accuracy);
	Array<string> array = readFile->GetArray<string>("RootСписок");
	const integer ExpectedArrayCount = 5;
	const string ExpectedArray[ExpectedArrayCount] = { "0", "1", "2", "3", "4" };
	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_ARRAYS_EQUAL(ExpectedArray, array, ExpectedArrayCount);
	Nullable<string> nullable = readFile->Get<Nullable<string>>("НулеваяСекция");
	IS_FALSE(nullable.HasValue());
	IS_THROW(nullable.GetValue(), NullPointerException);

	readFile->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(ParametersFileHierarchyWriteValues, RemoveParametersFile, RemoveParametersFile)
{
	SharedPtr<ParametersFile> writeFile = new ParametersFile(TestParametersFileName, FileAccess::Write);

	const integer InnersCount = 16;
	string parameterPath;
	for (integer i = 0; i < InnersCount; i++)
	{
		if (i > 0)
		{
			parameterPath += ParametersSection::PathDelimiter;
		}
		parameterPath += string("Section") + Converter::ToString(i);

		writeFile->Set<byte>(parameterPath + string("Byte"), byte(i));
		writeFile->Set<bool>(parameterPath + string("Bool"), Math::Odd(i));
		writeFile->Set<integer>(parameterPath + string("Integer"), i);
		writeFile->Set<float>(parameterPath + string("Float"), i + 0.666f);
		writeFile->Set<string>(parameterPath + string("String"), string("Value") + Converter::ToString(i));
		writeFile->Set<string>(parameterPath + ".0.Name", string("One") + Converter::ToString(i));
		writeFile->Set<string>(parameterPath + ".1.Name", string("Two") + Converter::ToString(i));
		writeFile->Set<Vector3>(parameterPath + "Vector3", Vector3(i + 0.1f, i + 0.2f, i + 0.3f));
		writeFile->Set(parameterPath + "NullPtr", NullPtr);
	}

	writeFile->Save();
	writeFile->Close();

	SharedPtr<ParametersFile> readFile = new ParametersFile(TestParametersFileName, FileAccess::Read);

	parameterPath = string::Empty;
	for (integer i = 0; i < InnersCount; i++)
	{
		if (i > 0)
		{
			parameterPath += ParametersSection::PathDelimiter;
		}
		parameterPath += string("Section") + Converter::ToString(i);

		ARE_EQUAL(byte(i), readFile->Get<byte>(parameterPath + string("Byte")));
		ARE_EQUAL(Math::Odd(i), readFile->Get<bool>(parameterPath + string("Bool")));
		ARE_EQUAL(i, readFile->Get<integer>(parameterPath + string("Integer")));
		ARE_CLOSE(i + 0.666f, readFile->Get<float>(parameterPath + string("Float")), Math::Accuracy);
		ARE_EQUAL(string("Value") + Converter::ToString(i), readFile->Get<string>(parameterPath + string("String")));
		ARE_EQUAL(string("One") + Converter::ToString(i), readFile->Get<string>(parameterPath + ".0.Name"));
		ARE_EQUAL(string("Two") + Converter::ToString(i), readFile->Get<string>(parameterPath + ".1.Name"));
		Vector3 vector = readFile->Get<Vector3>(parameterPath + "Vector3");
		ARE_CLOSE(i + 0.1f, vector.X, Math::Accuracy);
		ARE_CLOSE(i + 0.2f, vector.Y, Math::Accuracy);
		ARE_CLOSE(i + 0.3f, vector.Z, Math::Accuracy);
		Nullable<string> nullable = readFile->Get<Nullable<string>>(parameterPath + "NullPtr");
		IS_FALSE(nullable.HasValue());
		IS_THROW(nullable.GetValue(), NullPointerException);
	}

	readFile->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(ParametersFileWriteAlreadyContainsParameter, RemoveParametersFile, RemoveParametersFile)
{
	SharedPtr<ParametersFile> writeFile = new ParametersFile(TestParametersFileName, FileAccess::Write);

	//В итоге parameter должен быть равен вектору
	writeFile->Set<byte>("Parameter", byte(251));
	writeFile->Set<bool>("Parameter", false);
	writeFile->Set<integer>("Parameter", 666);
	writeFile->Set<float>("Parameter", 666.666f);
	writeFile->Set<string>("Parameter", "666!!!");
	writeFile->Set<Vector3>("Parameter", Vector3(0.1f, 0.2f, 0.3f));

	writeFile->Save();
	writeFile->Close();

	SharedPtr<ParametersFile> readFile = new ParametersFile(TestParametersFileName, FileAccess::Read);

	Vector3 vector = readFile->Get<Vector3>("Parameter");
	ARE_CLOSE(0.1f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.2f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.3f, vector.Z, Math::Accuracy);

	readFile->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(ParametersFileStreamReadWriteExceptions, RemoveParametersFile, RemoveParametersFile)
{
	SharedPtr<ParametersFile> writeFile = new ParametersFile(TestParametersFileName, FileAccess::Write);

	writeFile->Save();
	writeFile->Close();

	SharedPtr<ParametersFile> readFile = new ParametersFile(TestParametersFileName, FileAccess::Read);

	IS_THROW(readFile->Get<bool>(string::Empty), InvalidArgumentException);
	IS_THROW(readFile->Get<bool>("Parameter"), InvalidArgumentException);

	readFile->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(ParametersFileStreamStateExceptions, RemoveParametersFile, RemoveParametersFile)
{
	//Write

	SharedPtr<ParametersFile> writeFile = new ParametersFile(TestParametersFileName, FileAccess::Write);

	IS_THROW(writeFile->Get<byte>("Parameter"), InvalidStateException);
	IS_THROW(writeFile->Get<bool>("Parameter"), InvalidStateException);
	IS_THROW(writeFile->Get<integer>("Parameter"), InvalidStateException);
	IS_THROW(writeFile->Get<float>("Parameter"), InvalidStateException);
	IS_THROW(writeFile->Get<string>("Parameter"), InvalidStateException);
	IS_THROW(writeFile->Get<Vector3>("Parameter"), InvalidStateException);

	writeFile->Save();
	writeFile->Close();

	//Read

	SharedPtr<ParametersFile> readFile = new ParametersFile(TestParametersFileName, FileAccess::Read);

	IS_THROW(readFile->Set<byte>("Parameter", false), InvalidStateException);
	IS_THROW(readFile->Set<bool>("Parameter", false), InvalidStateException);
	IS_THROW(readFile->Set<integer>("Parameter", 666), InvalidStateException);
	IS_THROW(readFile->Set<float>("Parameter", 666.666f), InvalidStateException);
	IS_THROW(readFile->Set<string>("Parameter", "666!!!"), InvalidStateException);
	IS_THROW(readFile->Set<Vector3>("Parameter", Vector3(0.1f, 0.2f, 0.3)), InvalidStateException);

	readFile->Close();

	//Append

	SharedPtr<ParametersFile> appendFile = new ParametersFile(TestParametersFileName, FileAccess::Append);

	IS_THROW(appendFile->Get<byte>("Parameter"), InvalidStateException);
	IS_THROW(appendFile->Get<bool>("Parameter"), InvalidStateException);
	IS_THROW(appendFile->Get<integer>("Parameter"), InvalidStateException);
	IS_THROW(appendFile->Get<float>("Parameter"), InvalidStateException);
	IS_THROW(appendFile->Get<string>("Parameter"), InvalidStateException);
	IS_THROW(appendFile->Get<Vector3>("Parameter"), InvalidStateException);

	appendFile->Close();

	//Closed

	SharedPtr<ParametersFile> closedFile = new ParametersFile(TestParametersFileName, FileAccess::ReadWrite);
	closedFile->Close();

	IS_THROW(closedFile->Set<byte>("Parameter", byte(251)), InvalidStateException);
	IS_THROW(closedFile->Set<bool>("Parameter", false), InvalidStateException);
	IS_THROW(closedFile->Set<integer>("Parameter", 666), InvalidStateException);
	IS_THROW(closedFile->Set<float>("Parameter", 666.666f), InvalidStateException);
	IS_THROW(closedFile->Set<string>("CustomParameter", "666!!!"), InvalidStateException);
	IS_THROW(closedFile->Set<Vector3>("Parameter", Vector3(0.1f, 0.2f, 0.3)), InvalidStateException);

	IS_THROW(closedFile->Get<byte>("Parameter"), InvalidStateException);
	IS_THROW(closedFile->Get<bool>("Parameter"), InvalidStateException);
	IS_THROW(closedFile->Get<integer>("Parameter"), InvalidStateException);
	IS_THROW(closedFile->Get<float>("Parameter"), InvalidStateException);
	IS_THROW(closedFile->Get<string>("Parameter"), InvalidStateException);
	IS_THROW(closedFile->Get<Vector3>("Parameter"), InvalidStateException);
}

//===========================================================================//

} // namespace Bru

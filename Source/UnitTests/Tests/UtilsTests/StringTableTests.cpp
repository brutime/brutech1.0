//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Utils/StringTable/StringTable.h"
#include "../../../Utils/PathHelper/PathHelper.h"


namespace Bru
{

//===========================================================================//

const string TestParametersFileName = "UnitTests/StringTable";

//===========================================================================//

TEST(SimpleStringTableTest)
{
	StringTableSingleton st(TestParametersFileName);
	
	ARE_EQUAL("Подземелье 666", st.GetString("UnitTests.DungeonName"));
	ARE_EQUAL("Подземелье 666", st.GetString("UnitTests.DungeonName1"));
}

//===========================================================================//

TEST(ComplexStringTableTest)
{
	StringTableSingleton st(TestParametersFileName);
	
	ARE_EQUAL("Подземелье 666_123_6660823", st.GetString("UnitTests.ComplexString"));
	ARE_EQUAL("Подземелье 666_123_6660823", st.GetString("UnitTests.ComplexString2Depth"));
}

//===========================================================================//

TEST(MissingRefsTest)
{
	StringTableSingleton st(TestParametersFileName);
		
	IS_THROW(st.GetString("UnitTests.MissingRef"), InvalidArgumentException);
	IS_THROW(st.GetString("UnitTests.EmptyRef"), InvalidArgumentException);
}

//===========================================================================//

TEST(RecursiveRefsTest)
{
	StringTableSingleton st(TestParametersFileName);
		
	IS_THROW(st.GetString("UnitTests.RecursiveRefString1Level1"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.RecursiveRefString1Level2"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.RecursiveRefString1Level3"), StringTableRecursiveEntryException);
	
	IS_THROW(st.GetString("UnitTests.RecursiveRefString2Level"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.RecursiveRefString3Level"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.RecursiveRefString4Level"), StringTableRecursiveEntryException);
	
	IS_THROW(st.GetString("UnitTests.ChainRef1"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.ChainRef2"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.ChainRef3"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.ChainRef4"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.ChainRef5"), StringTableRecursiveEntryException);
	IS_THROW(st.GetString("UnitTests.ChainRef6"), StringTableRecursiveEntryException);
}

//===========================================================================//

TEST(ReferenceResolutionTest)
{
	StringTableSingleton st(TestParametersFileName);
	
	ARE_EQUAL("Подземелье 666", st.GetString("UnitTestNameResultion.DungeonName"));
	ARE_EQUAL("LevelName_666", st.GetString("UnitTestNameResultion.DungeonName1"));

	IS_THROW(st.GetString("UnitTestNameResultion.BadDungeonName"), InvalidArgumentException);
	
	ARE_EQUAL("LevelName_666", st.GetString("UnitTestNameResultion.UnitTests.DungeonName2"));
}

//===========================================================================//

} //namespace 

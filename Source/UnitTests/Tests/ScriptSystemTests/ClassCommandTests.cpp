//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты ClassCommand'а
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../ScriptSystem/ScriptSystem.h"

namespace Bru
{

namespace ClassCommandTests
{

//===========================================================================//

class PseudoRenderer
{
public:
	PseudoRenderer() :
		LandscapeType('z'),
		Width(0),
		Height(0),
		ColorDepth(0)
	{
	}

	~PseudoRenderer()
	{
	}

	void InitLandscape()
	{
		LandscapeType = 'a';
	}

	void SetLandscapeType(char landscapeType)
	{
		LandscapeType = landscapeType;
	}

	float GetPixelsPerimeter()
	{
		return (Width + Height) * 2.0f;
	}

	void SetResolution(integer width, float height)
	{
		Width = width;
		Height = height;
	}

	void SetResolution(integer width, float height, byte colorDepth)
	{
		Width = width;
		Height = height;
		ColorDepth = colorDepth;
	}

	string Concat(const string & a, const string & b)
	{
		return a + b;
	}

	void Thrower(integer a)
	{
		EXCEPTION(string(a));
	}

	char LandscapeType;
	integer Width;
	float Height;
	byte ColorDepth;

private:
	PseudoRenderer(const PseudoRenderer &) = delete;
	PseudoRenderer & operator =(const PseudoRenderer &) = delete;
};

PseudoRenderer pseudoRenderer;

//===========================================================================//
// Wrappers availability tests
//===========================================================================//

TEST(ClassCommandNoArgs)
{
	ClassCommand<PseudoRenderer, void> initLandscape("InitLandscape", &pseudoRenderer, &PseudoRenderer::InitLandscape);
	initLandscape.Execute();
	ARE_EQUAL('a', pseudoRenderer.LandscapeType);
}

//===========================================================================//

TEST(ClassCommandOneArg)
{
	ClassCommand<PseudoRenderer, void, char> setLandscapeType("SetLandscapeType", &pseudoRenderer,
	        &PseudoRenderer::SetLandscapeType);
	setLandscapeType.AddParameter(0, 'b');
	setLandscapeType.Execute();
	ARE_EQUAL('b', pseudoRenderer.LandscapeType);
}

//===========================================================================//

TEST(ClassCommandTwoArgs)
{
	ClassCommand<PseudoRenderer, void, integer, float> setResolution("SetResolution", &pseudoRenderer,
	        &PseudoRenderer::SetResolution);
	setResolution.AddParameter(0, 800);
	setResolution.AddParameter(1, 600.6f);
	setResolution.Execute();
	ARE_EQUAL(800, pseudoRenderer.Width);
	ARE_CLOSE(600.6f, pseudoRenderer.Height, Math::Accuracy);
}

//===========================================================================//

TEST(ClassCommandThreeArgs)
{
	ClassCommand<PseudoRenderer, void, integer, float, byte> setResolution("SetResolution", &pseudoRenderer,
	        &PseudoRenderer::SetResolution);
	setResolution.AddParameter(0, 800);
	setResolution.AddParameter(1, 600.6f);
	setResolution.AddParameter(2, 65);
	setResolution.Execute();
	ARE_EQUAL(800, pseudoRenderer.Width);
	ARE_CLOSE(600.6f, pseudoRenderer.Height, Math::Accuracy);
	ARE_EQUAL(65, pseudoRenderer.ColorDepth);
}

//===========================================================================//
// Exceptions tests
//===========================================================================//

TEST(ClassCommandNoArgExceptions)
{
	ClassCommand<PseudoRenderer, void> initLandscape("InitLandscape", &pseudoRenderer, &PseudoRenderer::InitLandscape);
	IS_THROW(initLandscape.AddParameter(0, "extraParam"), InvalidArgumentException);
}

//===========================================================================//

TEST(ClassCommandOneArgsExceptions)
{
	ClassCommand<PseudoRenderer, void, char> setLandscapeType("SetLandscapeType", &pseudoRenderer,
	        &PseudoRenderer::SetLandscapeType);
	IS_THROW(setLandscapeType.AddParameter(-1, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setLandscapeType.AddParameter(1, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setLandscapeType.Execute(), InvalidFunctionArgumentException);
}

//===========================================================================//

TEST(ClassCommandTwoArgsExceptions)
{
	ClassCommand<PseudoRenderer, void, integer, float> setResolution("SetResolution", &pseudoRenderer,
	        &PseudoRenderer::SetResolution);
	IS_THROW(setResolution.AddParameter(-1, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setResolution.AddParameter(2, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setResolution.Execute(), InvalidFunctionArgumentException);
}

//===========================================================================//
// Returned results tests
//===========================================================================//

TEST(ClassCommandNoReturnValue)
{
	ClassCommand<PseudoRenderer, void, char> setLandscapeType("SetLandscapeType", &pseudoRenderer,
	        &PseudoRenderer::SetLandscapeType);
	CommandExecutionResult result = setLandscapeType.AddParameter(0, 'b').Execute();
	IS_TRUE(result.ExecutedSuccessfully);
	IS_FALSE(result.ContainsValue);
}

//===========================================================================//

TEST(ClassCommandReturnWithoutArguments)
{
	ClassCommand<PseudoRenderer, float> getPixelsPerimeter("GetPixelsPerimeter", &pseudoRenderer, &PseudoRenderer::GetPixelsPerimeter);
	CommandExecutionResult result = getPixelsPerimeter.Execute();
	IS_TRUE(result.ExecutedSuccessfully);
	IS_TRUE(result.ContainsValue);
	ARE_CLOSE((pseudoRenderer.Width + pseudoRenderer.Height) * 2.0f, Converter::ToFloat(result.ResultString), Math::Accuracy);
}

//===========================================================================//

TEST(ClassCommandReturnCorrectValue)
{
	ClassCommand<PseudoRenderer, string, const string &, const string &> concat("Concat", &pseudoRenderer,
	        &PseudoRenderer::Concat);
	concat.AddParameter(0, "Bru").AddParameter(1, "Time");
	CommandExecutionResult result = concat.Execute();
	IS_TRUE(result.ExecutedSuccessfully);
	IS_TRUE(result.ContainsValue);
	ARE_EQUAL("BruTime", result.ResultString);
}

//===========================================================================//

TEST(ClassCommandReturnException)
{
	ClassCommand<PseudoRenderer, void, integer> thrower("Thrower", &pseudoRenderer, &PseudoRenderer::Thrower);
	CommandExecutionResult result = thrower.AddParameter(0, 135).Execute();
	IS_FALSE(result.ExecutedSuccessfully);
	IS_FALSE(result.ContainsValue);
	ARE_EQUAL("135", result.ResultString);
}

//===========================================================================//

}// namespace ClassCommandTests

} // namespace Bru

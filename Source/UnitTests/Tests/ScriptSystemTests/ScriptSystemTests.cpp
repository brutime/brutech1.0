//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты скриптовой системы
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../ScriptSystem/ScriptSystem.h"

namespace Bru
{

namespace ScriptSystemTests
{

//===========================================================================//

float Sum3(integer a, float b, int64 c)
{
	return a + b + c;
}

class TestHelper
{
public:
	string Concat(const string & a, const string & b)
	{
		return a + b;
	}
};

TestHelper helper;

//===========================================================================//

TEST(ScriptSystemCommandsRegistration)
{
	ScriptSystem->RegisterCommand(new StaticCommand<float, integer, float, int64>("Sum3", &Sum3));
	IS_TRUE(ScriptSystem->ContainsCommand("Sum3"));

	ScriptSystem->RegisterCommand(new ClassCommand<TestHelper, string, const string &, const string &>("Concat", &helper, &TestHelper::Concat));
	IS_TRUE(ScriptSystem->ContainsCommand("concat"));
}

//===========================================================================//

TEST(ScriptSystemEvaluationNormalFlow)
{
	CommandExecutionResult result = ScriptSystem->Evaluate("sum3 2 3.65 7000000");
	IS_TRUE(result.ExecutedSuccessfully && result.ContainsValue && Math::Equals(Converter::ToFloat(result.ResultString), 7000005.65f));

	result = ScriptSystem->Evaluate("concat Bru Time");
	IS_TRUE(result.ExecutedSuccessfully && result.ContainsValue && result.ResultString == "BruTime");

	IS_FALSE(ScriptSystem->ContainsCommand(string::Empty));
	IS_FALSE(ScriptSystem->ContainsCommand("sum"));
}

//===========================================================================//

TEST(ScriptSystemEvaluationException)
{
	IS_THROW(CommandExecutionResult result = ScriptSystem->Evaluate("sum3 2 3.65"), CommandNotRegisteredException);
}

//===========================================================================//

TEST(ScriptSystemParseCommandText)
{
	ParsedCommandText parsedCommandText = ScriptSystem->ParseCommandText("sum3 2 3.65 7000000");
	ARE_EQUAL("sum3", parsedCommandText.CommandName);
	ARE_EQUAL(3, parsedCommandText.Args.GetCount());
	ARE_EQUAL("2", parsedCommandText.Args[0]);
	ARE_EQUAL("3.65", parsedCommandText.Args[1]);
	ARE_EQUAL("7000000", parsedCommandText.Args[2]);
}

//===========================================================================//

TEST(ScriptSystemIsCommandRegistered)
{
	ParsedCommandText parsedCommandText = ScriptSystem->ParseCommandText("sum3 2 3.65 7000000");
	IS_TRUE(ScriptSystem->IsCommandRegistered(parsedCommandText));

	ParsedCommandText wrongParsedCommandText;
	wrongParsedCommandText.CommandName = "sum33";
	IS_FALSE(ScriptSystem->IsCommandRegistered(wrongParsedCommandText));
}

//===========================================================================//

TEST(ScriptSystemIsCommandArgumentsCorrect)
{
	ParsedCommandText parsedCommandText = ScriptSystem->ParseCommandText("sum3 2 3.65 7000000");
	IS_TRUE(ScriptSystem->IsCommandArgumentsCorrect(parsedCommandText));

	ParsedCommandText wrongParsedCommandText;
	wrongParsedCommandText.CommandName = "sum3";
	wrongParsedCommandText.Args.Add("2");
	wrongParsedCommandText.Args.Add("3.65");
	IS_FALSE(ScriptSystem->IsCommandArgumentsCorrect(wrongParsedCommandText));

	wrongParsedCommandText.Args.Add("7000000");
	wrongParsedCommandText.Args.Add("666");
	IS_FALSE(ScriptSystem->IsCommandArgumentsCorrect(wrongParsedCommandText));
}

//===========================================================================//

TEST(ScriptSystemEnableDisableCommands)
{
	ScriptSystem->DisableCommand("Sum3");
	IS_TRUE(ScriptSystem->ContainsCommand("Sum3"));
	IS_FALSE(ScriptSystem->IsCommandEnabled("Sum3"));
	
	IS_THROW(ScriptSystem->Evaluate("Sum3 2 3.65 7000000"), InvalidStateException);
	
	ScriptSystem->EnableCommand("Sum3");
	IS_TRUE(ScriptSystem->ContainsCommand("Sum3"));
	IS_TRUE(ScriptSystem->IsCommandEnabled("Sum3"));	
}

//===========================================================================//

TEST(ScriptSystemCommandsUnregistration)
{
	ScriptSystem->UnregisterCommand("Sum3");
	IS_THROW(ScriptSystem->Evaluate("sum3 2 3.65 7000000"), CommandNotRegisteredException);

	ScriptSystem->UnregisterCommand("Concat");
	IS_THROW(ScriptSystem->Evaluate("concat Bru Time"), CommandNotRegisteredException);
}

//===========================================================================//

} // namespace ScriptSystemTests

} // namespace Bru

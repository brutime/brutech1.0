//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты StaticCommand'а
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../ScriptSystem/ScriptSystem.h"

namespace Bru
{

namespace StaticCommandTests
{

//===========================================================================//

struct PseudoRenderer
{
	static char LandscapeType;
	static integer Width;
	static float Height;
	static byte ColorDepth;
};

//===========================================================================//

char PseudoRenderer::LandscapeType = NullTerminator;
integer PseudoRenderer::Width = 0;
float PseudoRenderer::Height = 0;
byte PseudoRenderer::ColorDepth = 0;

//===========================================================================//

void InitLandscape()
{
	PseudoRenderer::LandscapeType = 'a';
}

//===========================================================================//

void SetLandscapeType(char landscapeType)
{
	PseudoRenderer::LandscapeType = landscapeType;
}

//===========================================================================//

float GetPixelsPerimeter()
{
	return (PseudoRenderer::Width + PseudoRenderer::Height) * 2.0f;
}

//===========================================================================//

void SetResolution(integer width, float height)
{
	PseudoRenderer::Width = width;
	PseudoRenderer::Height = height;
}

//===========================================================================//

void SetResolution(integer width, float height, byte colorDepth)
{
	PseudoRenderer::Width = width;
	PseudoRenderer::Height = height;
	PseudoRenderer::ColorDepth = colorDepth;
}

//===========================================================================//

string Concat(const string & a, const string & b)
{
	return a + b;
}

//===========================================================================//

void Thrower(integer a)
{
	EXCEPTION(string(a));
}

//===========================================================================//
// Wrappers availability tests
//===========================================================================//

TEST(StaticCommandNoArgs)
{
	StaticCommand<void> initLandscape("InitLandscape", &InitLandscape);
	initLandscape.Execute();
	ARE_EQUAL('a', PseudoRenderer::LandscapeType);
}

//===========================================================================//

TEST(StaticCommandOneArg)
{
	StaticCommand<void, char> setLandscapeType("SetLandscapeType", &SetLandscapeType);
	setLandscapeType.AddParameter(0, 'b');
	setLandscapeType.Execute();
	ARE_EQUAL('b', PseudoRenderer::LandscapeType);
}

//===========================================================================//

TEST(StaticCommandTwoArgs)
{
	StaticCommand<void, integer, float> setResolution("SetResolution", &SetResolution);
	setResolution.AddParameter(0, 800);
	setResolution.AddParameter(1, 600.6f);
	setResolution.Execute();
	ARE_EQUAL(800, PseudoRenderer::Width);
	ARE_CLOSE(600.6f, PseudoRenderer::Height, Math::Accuracy);
}

//===========================================================================//

TEST(StaticCommandThreeArgs)
{
	StaticCommand<void, integer, float, byte> setResolution("SetResolution", &SetResolution);
	setResolution.AddParameter(0, 800);
	setResolution.AddParameter(1, 600.6f);
	setResolution.AddParameter(2, 65);
	setResolution.Execute();
	ARE_EQUAL(800, PseudoRenderer::Width);
	ARE_CLOSE(600.6f, PseudoRenderer::Height, Math::Accuracy);
	ARE_EQUAL(65, PseudoRenderer::ColorDepth);
}

//===========================================================================//
// Exceptions tests
//===========================================================================//

TEST(StaticCommandNoArgExceptions)
{
	StaticCommand<void> initLandscape("InitLandscape", &InitLandscape);
	IS_THROW(initLandscape.AddParameter(0, "extraParam"), InvalidArgumentException);
}

//===========================================================================//

TEST(StaticCommandOneArgsExceptions)
{
	StaticCommand<void, char> setLandscapeType("SetLandscapeType", &SetLandscapeType);
	IS_THROW(setLandscapeType.AddParameter(-1, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setLandscapeType.AddParameter(1, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setLandscapeType.Execute(), InvalidFunctionArgumentException);
}

//===========================================================================//

TEST(StaticCommandTwoArgsExceptions)
{
	StaticCommand<void, integer, float> setResolution("SetResolution", &SetResolution);
	IS_THROW(setResolution.AddParameter(-1, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setResolution.AddParameter(2, "extraParam"), IndexOutOfBoundsException);
	IS_THROW(setResolution.Execute(), InvalidFunctionArgumentException);
}

//===========================================================================//
// Returned results tests
//===========================================================================//

TEST(StaticCommandNoReturnValue)
{
	StaticCommand<void, char> setLandscapeType("SetLandscapeType", &SetLandscapeType);
	CommandExecutionResult result = setLandscapeType.AddParameter(0, 'b').Execute();
	IS_TRUE(result.ExecutedSuccessfully && !result.ContainsValue);
}

//===========================================================================//

TEST(StaticCommandReturnWithoutArguments)
{
	StaticCommand<float> getPixelsPerimeter("GetPixelsPerimeter", &GetPixelsPerimeter);
	CommandExecutionResult result = getPixelsPerimeter.Execute();
	IS_TRUE(result.ExecutedSuccessfully);
	IS_TRUE(result.ContainsValue);
	ARE_CLOSE((PseudoRenderer::Width + PseudoRenderer::Height) * 2.0f, Converter::ToFloat(result.ResultString), Math::Accuracy);
}

//===========================================================================//

TEST(StaticCommandReturnCorrectValue)
{
	StaticCommand<string, const string &, const string &> concat("Concat", &Concat);
	concat.AddParameter(0, "Bru").AddParameter(1, "Time");
	CommandExecutionResult result = concat.Execute();
	IS_TRUE(result.ExecutedSuccessfully && result.ContainsValue);
	ARE_EQUAL("BruTime", result.ResultString);
}

//===========================================================================//

TEST(StaticCommandReturnException)
{
	StaticCommand<void, integer> thrower("Thrower", &Thrower);
	thrower.AddParameter(0, 135);
	CommandExecutionResult result = thrower.Execute();
	IS_FALSE(result.ExecutedSuccessfully && !result.ContainsValue);
	ARE_EQUAL("135", result.ResultString);
}

//===========================================================================//

}// namespace StaticCommandTests

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты размера
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(IntSizeDefaultConstructor)
{
	IntSize size;

	ARE_EQUAL(0, size.Width);
	ARE_EQUAL(0, size.Height);
}

//===========================================================================//

TEST(IntSizeConstructor)
{
	IntSize size(-10, 22);

	ARE_EQUAL(-10, size.Width);
	ARE_EQUAL(22, size.Height);
}

//===========================================================================//

TEST(IntSizeStringConstructor)
{
	IntSize size("-10, 22");

	ARE_EQUAL(-10, size.Width);
	ARE_EQUAL(22, size.Height);
}

//===========================================================================//

TEST(IntSizeStringConstructorExceptions)
{
	IS_THROW(IntSize("-10"), InvalidArgumentException);
	IS_THROW(IntSize("-10, 22, 1"), InvalidArgumentException);
}

//===========================================================================//

TEST(IntSizeCopyConstructor)
{
	IntSize sourceSize(66, -1);
	IntSize size(sourceSize);

	sourceSize.Clear();

	ARE_EQUAL(66, size.Width);
	ARE_EQUAL(-1, size.Height);
}

//===========================================================================//

TEST(IntSizeCopyOperator)
{
	IntSize sourceIntSize(66, -1);
	IntSize size;

	size = sourceIntSize;

	sourceIntSize.Clear();

	ARE_EQUAL(66, size.Width);
	ARE_EQUAL(-1, size.Height);
}

//===========================================================================//

TEST(IntSizeCopyOperatorSelf)
{
	IntSize size(66, -1);

	size = size;

	ARE_EQUAL(66, size.Width);
	ARE_EQUAL(-1, size.Height);
}

//===========================================================================//

TEST(IntSizeEquals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	IntSize size(-10, 13);
	IntSize equalsIntSize(-10, 13);
	IntSize notEqualsIntSize(10, 14);

	IS_TRUE(size == equalsIntSize);
	IS_TRUE(size != notEqualsIntSize);
}

//===========================================================================//

TEST(IntSizeClear)
{
	IntSize size(-10, 22);

	size.Clear();

	ARE_EQUAL(0, size.Width);
	ARE_EQUAL(0, size.Height);
}

//===========================================================================//

TEST(IntSizeToString)
{
	IntSize size(5, 10);

	ARE_EQUAL("5, 10", size.ToString());
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: тесты математики Graphics'а
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/SpaceMaths/MatrixMath.h"

namespace Bru
{

//===========================================================================//

TEST(MatrixMathOtrho)
{
	Matrix4x4 matrix = MatrixMath::Ortho(0, 400, 0, 300, -2, 3);

	float elements[16] = { 0.005f, 0.0f, 0.0f, 0.0f, 0.0f, 0.006667f, 0.0f, 0.0f, 0.0f, 0.0f, -0.4f, 0.0f, -1.0f, -1.0f,
	                       -0.2f, 1.0f
	                     };

	ARE_ARRAYS_CLOSE(elements, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(MatrixMathOtrho2D)
{
	Matrix4x4 matrix = MatrixMath::Ortho2D(0, 400, 0, 300);

	float elements[16] = { 0.005f, 0.0f, 0.0f, 0.0f, 0.0f, 0.006667f, 0.0f, 0.0f, 0.0f, 0.0f, -1.0f, 0.0f, -1.0f, -1.0f,
	                       -0.0f, 1.0f
	                     };

	ARE_ARRAYS_CLOSE(elements, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(MatrixMathFrustum)
{
	Matrix4x4 matrix = MatrixMath::Frustum(0, 400, 0, 300, 0.1f, 100.0f);

	float elements[16] = { 0.0005f, 0.0f, 0.0f, 0.0f, 0.0f, 0.000667f, 0.0f, 0.0f, 1.0f, 1.0f, -1.002002f, -1.0f, 0.0f,
	                       0.0f, -0.2002f, 0.0f
	                     };

	ARE_ARRAYS_CLOSE(elements, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(MatrixMathFrustumExceptions)
{
	IS_THROW(MatrixMath::Frustum(0, 400, 0, 300, -0.1f, 100.0f), InvalidArgumentException);
	IS_THROW(MatrixMath::Frustum(0, 400, 0, 300, 0.1f, -100.0f), InvalidArgumentException);
}

//===========================================================================//

TEST(MatrixMathPerspective)
{
	Matrix4x4 matrix = MatrixMath::Perspective(45.0f, 400.0f / 300.0f, 0.1f, 100.0f);

	float elements[16] = { 1.810660f, 0.0f, 0.0f, 0.0f, 0.0f, 2.414214f, 0.0f, 0.0f, 0.0f, 0.0f, -1.002002f, -1.0f,
	                       0.0f, 0.0f, -0.2002f, 0.0f
	                     };

	ARE_ARRAYS_CLOSE(elements, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(MatrixMathPerspectiveExceptions)
{
	IS_THROW(MatrixMath::Perspective(45.0f, 400.0f / 300.0f, -0.1f, 100.0f), InvalidArgumentException);
	IS_THROW(MatrixMath::Perspective(45.0f, 400.0f / 300.0f, 0.1f, -100.0f), InvalidArgumentException);
}

//===========================================================================//

TEST(MatrixMathLookAt)
{
	Matrix4x4 matrix = MatrixMath::LookAt(10.0f, 20.0f, 30.0f, 1.0f, 2.0f, 3.0f, 0.0f, 1.0f, 0.0f);

	float elements[16] = { 0.948683f, -0.169031f, 0.267261f, 0.0f, 0.0f, 0.845154f, 0.534522f, 0.0f, -0.316228f,
	                       -0.507093f, 0.801784f, 0.0f, -0.000001f, 0.000001f, -37.416573f, 1.0f
	                     };

	ARE_ARRAYS_CLOSE(elements, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(MatrixMathLookAtVectors)
{
	Matrix4x4 matrix = MatrixMath::LookAt(Vector3(10.0f, 20.0f, 30.0f), Vector3(1.0f, 2.0f, 3.0f),
	                                      Vector3(0.0f, 1.0f, 0.0f));

	float elements[16] = { 0.948683f, -0.169031f, 0.267261f, 0.0f, 0.0f, 0.845154f, 0.534522f, 0.0f, -0.316228f,
	                       -0.507093f, 0.801784f, 0.0f, -0.000001f, 0.000001f, -37.416573f, 1.0f
	                     };

	ARE_ARRAYS_CLOSE(elements, matrix.GetData(), 16, Math::Accuracy);

}

//===========================================================================//

}// namespace Bru

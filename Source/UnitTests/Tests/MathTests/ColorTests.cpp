//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты графики
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(ColorDefaultConstructor)
{
	Color color;

	ARE_CLOSE(Color::Default.GetRed(), color.GetRed(), Math::Accuracy);
	ARE_CLOSE(Color::Default.GetGreen(), color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(Color::Default.GetBlue(), color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(Color::Default.GetAlpha(), color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorConstructor)
{
	Color color(0.33f, 0.66f, 0.99f, 1.00f);

	ARE_CLOSE(0.33f, color.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.66f, color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.99f, color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(1.00f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorConstructorFromBytes)
{
	Color color(84, 168, 252, 255);

	ARE_CLOSE(0.3294f, color.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.6588f, color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.9882f, color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(1.00f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorConstructorFromString)
{
	Color color("84, 168, 252, 255");

	ARE_CLOSE(0.3294f, color.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.6588f, color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.9882f, color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(1.00f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorConstructorFromStringExceptions)
{
	IS_THROW(Color("85, 169, 253"), InvalidArgumentException);
	IS_THROW(Color("85, 169, 253, 255, -100"), InvalidArgumentException);
}

//===========================================================================//

TEST(ColorConstructorFromOtherColor)
{
	Color color(Colors::Aqua);

	ARE_CLOSE(Colors::Aqua.GetRed(), color.GetRed(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetGreen(), color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetBlue(), color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetAlpha(), color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorConstructorFromOtherColorWithAlpha)
{
	Color color(Colors::Aqua, Colors::Red.GetAlpha());

	ARE_CLOSE(Colors::Aqua.GetRed(), color.GetRed(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetGreen(), color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetBlue(), color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(Colors::Red.GetAlpha(), color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorOperatorAssign)
{
	Color color;

	color = Colors::Aqua;

	ARE_CLOSE(Colors::Aqua.GetRed(), color.GetRed(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetGreen(), color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetBlue(), color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(Colors::Aqua.GetAlpha(), color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorAppend)
{
	Color color(0.1f, 0.2f, 0.3f, 0.4f);
	Color appendingColor(0.3f, 0.4f, 0.2f, 0.5f);

	color += appendingColor;

	ARE_CLOSE(0.4f, color.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.6f, color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.5f, color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(0.9f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorAppendOutOfBounds)
{
	Color color(Colors::Gray90);

	color += Color(0.9f, 0.0f, 0.0f, 0.0f);
	ARE_CLOSE(1.0f, color.GetRed(), Math::Accuracy);

	color += Color(0.0f, 0.9f, 0.0f, 0.0f);
	ARE_CLOSE(1.0f, color.GetGreen(), Math::Accuracy);

	color += Color(0.0f, 0.0f, 0.9f, 0.0f);
	ARE_CLOSE(1.0f, color.GetBlue(), Math::Accuracy);

	color += Color(0.0f, 0.0f, 0.0f, 0.9f);
	ARE_CLOSE(1.0f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorDetach)
{
	Color color(0.4f, 0.6f, 0.5f, 0.9f);
	Color detachingColor(0.3f, 0.4f, 0.2f, 0.5f);

	color -= detachingColor;

	ARE_CLOSE(0.1f, color.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.2f, color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.3f, color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(0.4f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorDetachOutOfBounds)
{
	Color color(Colors::Gray05);

	color -= Color(0.9f, 0.0f, 0.0f, 0.0f);
	ARE_CLOSE(0.0f, color.GetRed(), Math::Accuracy);

	color -= Color(0.0f, 0.9f, 0.0f, 0.0f);
	ARE_CLOSE(0.0f, color.GetGreen(), Math::Accuracy);

	color -= Color(0.0f, 0.0f, 0.9f, 0.0f);
	ARE_CLOSE(0.0f, color.GetBlue(), Math::Accuracy);

	color -= Color(0.0f, 0.0f, 0.0f, 0.9f);
	ARE_CLOSE(0.1f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorPlus)
{
	Color color(0.1f, 0.2f, 0.3f, 0.4f);
	Color appendingColor(0.3f, 0.4f, 0.2f, 0.5f);

	Color resultColor = color + appendingColor;

	ARE_CLOSE(0.4f, resultColor.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.6f, resultColor.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.5f, resultColor.GetBlue(), Math::Accuracy);
	ARE_CLOSE(0.9f, resultColor.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorPlusOutOfBounds)
{
	Color color(Colors::Gray90);

	Color resultColor = color + Color(0.9f, 0.0f, 0.0f, 0.0f);
	ARE_CLOSE(1.0f, resultColor.GetRed(), Math::Accuracy);

	resultColor = color + Color(0.0f, 0.9f, 0.0f, 0.0f);
	ARE_CLOSE(1.0f, resultColor.GetGreen(), Math::Accuracy);

	resultColor = color + Color(0.0f, 0.0f, 0.9f, 0.0f);
	ARE_CLOSE(1.0f, resultColor.GetBlue(), Math::Accuracy);

	resultColor = color + Color(0.0f, 0.0f, 0.0f, 0.9f);
	ARE_CLOSE(1.0f, resultColor.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorMinus)
{
	Color color(0.4f, 0.6f, 0.5f, 0.9f);
	Color detachingColor(0.3f, 0.4f, 0.2f, 0.5f);

	Color resultColor = color - detachingColor;

	ARE_CLOSE(0.1f, resultColor.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.2f, resultColor.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.3f, resultColor.GetBlue(), Math::Accuracy);
	ARE_CLOSE(0.4f, resultColor.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorMinusOutOfBounds)
{
	Color color(Colors::Gray05);

	Color resultColor = color - Color(0.9f, 0.0f, 0.0f, 0.0f);
	ARE_CLOSE(0.0f, resultColor.GetRed(), Math::Accuracy);

	resultColor = color - Color(0.0f, 0.9f, 0.0f, 0.0f);
	ARE_CLOSE(0.0f, resultColor.GetGreen(), Math::Accuracy);

	resultColor = color - Color(0.0f, 0.0f, 0.9f, 0.0f);
	ARE_CLOSE(0.0f, resultColor.GetBlue(), Math::Accuracy);

	resultColor = color - Color(0.0f, 0.0f, 0.0f, 0.9f);
	ARE_CLOSE(0.1f, resultColor.GetAlpha(), Math::Accuracy);
}

//===========================================================================//]

TEST(ColorEquals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	Color color(0.4f, 0.6f, 0.5f, 0.9f);
	Color equalsColor(0.4f, 0.6f, 0.5f, 0.9f);
	Color notEqualsColor(0.9f, 0.5f, 0.6f, 0.4f);

	IS_TRUE(color == equalsColor);
	IS_TRUE(color != notEqualsColor);
}

//===========================================================================//

TEST(ColorLess)
{
	Color color(0.2f, 0.3f, 0.4f, 0.5f);
	Color greaterColor(0.5f, 0.6f, 0.7f, 0.8f);
	Color greaterOrEqualsColor(0.2f, 0.6f, 0.4f, 0.8f);

	IS_TRUE(color < greaterColor);
	IS_TRUE(color <= greaterOrEqualsColor);
}

//===========================================================================//

TEST(ColorGreater)
{
	Color color(0.5f, 0.6f, 0.7f, 0.8f);
	Color lessColor(0.2f, 0.3f, 0.4f, 0.5f);
	Color lessOrEqualsColor(0.2f, 0.6f, 0.4f, 0.8f);

	IS_TRUE(color > lessColor);
	IS_TRUE(color >= lessOrEqualsColor);
}

//===========================================================================//

TEST(ColorGetInverted)
{
	Color color(0.5f, 0.6f, 0.7f, 0.8f);
	Color invertedColor = color.GetInverted();

	ARE_CLOSE(0.5f, invertedColor.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.4f, invertedColor.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.3f, invertedColor.GetBlue(), Math::Accuracy);
	ARE_CLOSE(0.2f, invertedColor.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorGetInvertedExceptAlpha)
{
	Color color(0.5f, 0.6f, 0.7f, 0.8f);
	Color invertedColor = color.GetInvertedExceptAlpha();

	ARE_CLOSE(0.5f, invertedColor.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.4f, invertedColor.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.3f, invertedColor.GetBlue(), Math::Accuracy);
	ARE_CLOSE(0.8f, invertedColor.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

TEST(ColorMakeRandomRGB)
{
	Color color;
	Color previousColor = color + Color(0.5, 0.0f, 0.0f, 0.0f);
	const integer RandomColorRepeat = 1000;
	for (integer i = 0; i < RandomColorRepeat; i++)
	{
		color.MakeRandomRGB();
		IS_TRUE(0.0f <= color.GetRed() && color.GetRed() <= 1.0f);
		IS_TRUE(0.0f <= color.GetGreen() && color.GetGreen() <= 1.0f);
		IS_TRUE(0.0f <= color.GetBlue() && color.GetBlue() <= 1.0f);
		ARE_CLOSE(color.GetAlpha(), 1.0f, Math::Accuracy);
		IS_TRUE(color != previousColor);
		previousColor = color;
	}	
}

//===========================================================================//

TEST(ColorMakeRandomRGBA)
{
	Color color;
	Color previousColor = color + Color(0.5, 0.0f, 0.0f, 0.0f);
	const integer RandomColorRepeat = 1000;
	for (integer i = 0; i < RandomColorRepeat; i++)
	{
		color.MakeRandomRGBA();
		IS_TRUE(0.0f <= color.GetRed() && color.GetRed() <= 1.0f);
		IS_TRUE(0.0f <= color.GetGreen() && color.GetGreen() <= 1.0f);
		IS_TRUE(0.0f <= color.GetBlue() && color.GetBlue() <= 1.0f);
		IS_TRUE(0.0f <= color.GetAlpha() && color.GetAlpha() <= 1.0f);
		IS_TRUE(color != previousColor);
		previousColor = color;
	}		
}

//===========================================================================//

TEST(ColorRandomRGB)
{
	Color color;
	Color previousColor = color + Color(0.5, 0.0f, 0.0f, 0.0f);
	const integer RandomColorRepeat = 1000;
	for (integer i = 0; i < RandomColorRepeat; i++)
	{
		color = Color::RandomRGB();
		IS_TRUE(0.0f <= color.GetRed() && color.GetRed() <= 1.0f);
		IS_TRUE(0.0f <= color.GetGreen() && color.GetGreen() <= 1.0f);
		IS_TRUE(0.0f <= color.GetBlue() && color.GetBlue() <= 1.0f);
		ARE_CLOSE(color.GetAlpha(), 1.0f, Math::Accuracy);
		IS_TRUE(color != previousColor);
		previousColor = color;
	}	
}

//===========================================================================//

TEST(ColorRandomRGBA)
{
	Color color;
	Color previousColor = color + Color(0.5, 0.0f, 0.0f, 0.0f);
	const integer RandomColorRepeat = 1000;
	for (integer i = 0; i < RandomColorRepeat; i++)
	{
		color = Color::RandomRGBA();
		IS_TRUE(0.0f <= color.GetRed() && color.GetRed() <= 1.0f);
		IS_TRUE(0.0f <= color.GetGreen() && color.GetGreen() <= 1.0f);
		IS_TRUE(0.0f <= color.GetBlue() && color.GetBlue() <= 1.0f);
		IS_TRUE(0.0f <= color.GetAlpha() && color.GetAlpha() <= 1.0f);
		IS_TRUE(color != previousColor);
		previousColor = color;
	}		
}

//===========================================================================//

TEST(ColorToString)
{
	Color color(0.33f, 0.66f, 0.99f, 1.00f);

	ARE_EQUAL("84, 168, 252, 255", color.ToString());
}

//===========================================================================//

TEST(ColorSetters)
{
	Color color;

	color.SetRed(0.2f);
	color.SetGreen(0.3f);
	color.SetBlue(0.4f);
	color.SetAlpha(0.5f);

	ARE_CLOSE(0.2f, color.GetRed(), Math::Accuracy);
	ARE_CLOSE(0.3f, color.GetGreen(), Math::Accuracy);
	ARE_CLOSE(0.4f, color.GetBlue(), Math::Accuracy);
	ARE_CLOSE(0.5f, color.GetAlpha(), Math::Accuracy);
}

//===========================================================================//

}// namespace Bru

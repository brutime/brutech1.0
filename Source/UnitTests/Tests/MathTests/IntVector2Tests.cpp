//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса вектора, состоящего из 2-ух компонентов целого типа
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(IntVector2DefaultConstructor)
{
	IntVector2 vector;

	ARE_EQUAL(0, vector.X);
	ARE_EQUAL(0, vector.Y);
}

//===========================================================================//

TEST(IntVector2Constructor)
{
	IntVector2 vector(-10, 22);

	ARE_EQUAL(-10, vector.X);
	ARE_EQUAL(22, vector.Y);
}

//===========================================================================//

TEST(IntVector2StringConstructor)
{
	IntVector2 vector("-10, 22");

	ARE_EQUAL(-10, vector.X);
	ARE_EQUAL(22, vector.Y);
}

//===========================================================================//

TEST(IntVector2ConstructorExceptions)
{
	IS_THROW(IntVector2("-10"), InvalidArgumentException);
	IS_THROW(IntVector2("-10, 22, 0"), InvalidArgumentException);
}

//===========================================================================//

TEST(IntVector2CopyConstructor)
{
	IntVector2 sourceVector(66, -1);
	IntVector2 vector(sourceVector);

	sourceVector.Clear();

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-1, vector.Y);
}

//===========================================================================//

TEST(IntVector2MoveConstructor)
{
	IntVector2 vector(std::move(IntVector2(66, -1)));

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-1, vector.Y);
}

//===========================================================================//

TEST(IntVector2CopyOperator)
{
	IntVector2 sourceVector(66, -1);
	IntVector2 vector;

	vector = sourceVector;

	sourceVector.Clear();

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-1, vector.Y);
}

//===========================================================================//

TEST(IntVector2CopyOperatorSelf)
{
	IntVector2 vector(66, -1);

	vector = vector;

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-1, vector.Y);
}

//===========================================================================//

TEST(IntVector2MoveOperator)
{
	IntVector2 vector;

	vector = std::move(IntVector2(66, -1));

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-1, vector.Y);
}

//===========================================================================//

TEST(IntVector2Append)
{
	IntVector2 vector(-10, 13);
	IntVector2 appendingVector(10, 14);

	vector += appendingVector;

	ARE_EQUAL(0, vector.X);
	ARE_EQUAL(27, vector.Y);
}

//===========================================================================//

TEST(IntVector2Detach)
{
	IntVector2 vector(-10, 13);
	IntVector2 detachingVector(10, 14);

	vector -= detachingVector;

	ARE_EQUAL(-20, vector.X);
	ARE_EQUAL(-1, vector.Y);
}

//===========================================================================//

TEST(IntVector2Multiply)
{
	IntVector2 vector(2, 3);
	IntVector2 multiplyingVector(3, 4);

	vector *= multiplyingVector;

	ARE_EQUAL(6, vector.X);
	ARE_EQUAL(12, vector.Y);
}

//===========================================================================//

TEST(IntVector2MultiplyFactor)
{
	IntVector2 vector(2, 3);

	vector *= 2;

	ARE_EQUAL(4, vector.X);
	ARE_EQUAL(6, vector.Y);
}

//===========================================================================//

TEST(IntVector2Divide)
{
	IntVector2 vector(6, 12);
	IntVector2 dividingVector(2, 3);

	vector /= dividingVector;

	ARE_EQUAL(3, vector.X);
	ARE_EQUAL(4, vector.Y);
}

//===========================================================================//

TEST(IntVector2Plus)
{
	IntVector2 vector(-10, 13);
	IntVector2 appendingVector(10, 14);

	IntVector2 resultVector = vector + appendingVector;

	ARE_EQUAL(0, resultVector.X);
	ARE_EQUAL(27, resultVector.Y);
}

//===========================================================================//

TEST(IntVector2Minus)
{
	IntVector2 vector(-10, 13);
	IntVector2 detachingVector(10, 14);

	IntVector2 resultVector = vector - detachingVector;

	ARE_EQUAL(-20, resultVector.X);
	ARE_EQUAL(-1, resultVector.Y);
}

//===========================================================================//

TEST(IntVector2Multiplication)
{
	IntVector2 vector(2, 3);
	IntVector2 multiplyingVector(3, 4);

	IntVector2 resultVector = vector * multiplyingVector;

	ARE_EQUAL(6, resultVector.X);
	ARE_EQUAL(12, resultVector.Y);
}

//===========================================================================//

TEST(IntVector2MultiplicationFactor)
{
	IntVector2 vector(2, 3);

	IntVector2 resultVector = vector * 2;

	ARE_EQUAL(4, resultVector.X);
	ARE_EQUAL(6, resultVector.Y);
}

//===========================================================================//

TEST(IntVector2Division)
{
	IntVector2 vector(6, 12);
	IntVector2 dividingVector(2, 3);

	IntVector2 resultVector = vector / dividingVector;

	ARE_EQUAL(3, resultVector.X);
	ARE_EQUAL(4, resultVector.Y);
}

//===========================================================================//

TEST(IntVector2Equals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	IntVector2 vector(-10, 13);
	IntVector2 equalsVector(-10, 13);
	IntVector2 notEqualsVector(10, 14);

	IS_TRUE(vector == equalsVector);
	IS_TRUE(vector != notEqualsVector);
}

//===========================================================================//

TEST(IntVector2OperatorNegative)
{
	IntVector2 vector(5, 10);

	IntVector2 resultVector = -vector;

	ARE_EQUAL(-5, resultVector.X);
	ARE_EQUAL(-10, resultVector.Y);
}

//===========================================================================//

TEST(IntVector2Clear)
{
	IntVector2 vector(-10, 13);

	vector.Clear();

	ARE_EQUAL(0, vector.X);
	ARE_EQUAL(0, vector.Y);
}

//===========================================================================//

TEST(IntVector2DistanceTo)
{
	IntVector2 vector(5, 10);
	IntVector2 otherVector(8, 4);

	ARE_CLOSE(6.708204f, vector.DistanceTo(otherVector), Math::Accuracy);
}

//===========================================================================//

TEST(IntVector2Negative)
{
	IntVector2 vector(5, 10);

	vector.Negative();

	ARE_EQUAL(-5, vector.X);
	ARE_EQUAL(-10, vector.Y);
}

//===========================================================================//

TEST(IntVector2NegativeX)
{
	IntVector2 vector(5, 10);

	vector.NegativeX();

	ARE_EQUAL(-5, vector.X);
	ARE_EQUAL(10, vector.Y);
}

//===========================================================================//

TEST(IntVector2NegativeY)
{
	IntVector2 vector(5, 10);

	vector.NegativeY();

	ARE_EQUAL(5, vector.X);
	ARE_EQUAL(-10, vector.Y);
}

//===========================================================================//

TEST(IntVector2ToString)
{
	IntVector2 vector(5, 10);

	ARE_EQUAL("5, 10", vector.ToString());
}

//===========================================================================//

}// namespace Bru

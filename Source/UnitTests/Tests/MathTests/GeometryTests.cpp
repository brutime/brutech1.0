//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты геометрии
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(GeometryPointInPolygon)
{
	const integer width = 6;
	const integer height = 6;

	bool points[width][height] = { { 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 1, 0 }, { 0, 1, 1, 1, 0, 0 },
		{ 0, 1, 1, 0, 0, 0 }, { 0, 1, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }
	};

	Array<Vector2> polygonsPoints;
	polygonsPoints.Add(Vector2(1.0f, 1.0f));
	polygonsPoints.Add(Vector2(5.0f, 1.0f));
	polygonsPoints.Add(Vector2(1.0f, 5.0f));

	for (integer y = 0; y < height; y++)
	{
		for (integer x = 0; x < width; x++)
		{
			Vector2 point(x, y);
			ARE_EQUAL(points[y][x], Geometry::PointInPolygon(polygonsPoints, point));
		}
	}
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса вектора, состоящего из 3-ех компонентов целого типа
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(IntVector3DefaultConstructor)
{
	IntVector3 vector;

	ARE_EQUAL(0, vector.X);
	ARE_EQUAL(0, vector.Y);
	ARE_EQUAL(0, vector.Z);
}

//===========================================================================//

TEST(IntVector3Constructor)
{
	IntVector3 vector(-10, 22, 0);

	ARE_EQUAL(-10, vector.X);
	ARE_EQUAL(22, vector.Y);
	ARE_EQUAL(0, vector.Z);
}

//===========================================================================//

TEST(IntVector3StringConstructor)
{
	IntVector3 vector("-10, 22, 0");

	ARE_EQUAL(-10, vector.X);
	ARE_EQUAL(22, vector.Y);
	ARE_EQUAL(0, vector.Z);
}

//===========================================================================//

TEST(IntVector3StringConstructorExceptions)
{
	IS_THROW(IntVector3("-10, 22"), InvalidArgumentException);
	IS_THROW(IntVector3("-10, 22, 0, -2"), InvalidArgumentException);
}

//===========================================================================//

TEST(IntVector3CopyConstructor)
{
	IntVector3 sourceVector(66, -666, 1);
	IntVector3 vector(sourceVector);

	sourceVector.Clear();

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-666, vector.Y);
	ARE_EQUAL(1, vector.Z);
}

//===========================================================================//

TEST(IntVector3MoveConstructor)
{
	IntVector3 vector(std::move(IntVector3(66, -666, 1)));

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-666, vector.Y);
	ARE_EQUAL(1, vector.Z);
}

//===========================================================================//

TEST(IntVector3CopyOperator)
{
	IntVector3 sourceVector(66, -666, 1);
	IntVector3 vector;

	vector = sourceVector;

	sourceVector.Clear();

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-666, vector.Y);
	ARE_EQUAL(1, vector.Z);
}

//===========================================================================//

TEST(IntVector3CopyOperatorSelf)
{
	IntVector3 vector(66, -666, 1);

	vector = vector;

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-666, vector.Y);
	ARE_EQUAL(1, vector.Z);
}

//===========================================================================//

TEST(IntVector3MoveOperator)
{
	IntVector3 vector;

	vector = std::move(IntVector3(66, -666, 1));

	ARE_EQUAL(66, vector.X);
	ARE_EQUAL(-666, vector.Y);
	ARE_EQUAL(1, vector.Z);
}

//===========================================================================//

TEST(IntVector3Append)
{
	IntVector3 vector(-10, 13, 22);
	IntVector3 appendingVector(10, 14, 11);

	vector += appendingVector;

	ARE_EQUAL(0, vector.X);
	ARE_EQUAL(27, vector.Y);
	ARE_EQUAL(33, vector.Z);
}

//===========================================================================//
//
TEST(IntVector3Detach)
{
	IntVector3 vector(-10, 13, 22);
	IntVector3 detachingVector(10, 14, 11);

	vector -= detachingVector;

	ARE_EQUAL(-20, vector.X);
	ARE_EQUAL(-1, vector.Y);
	ARE_EQUAL(11, vector.Z);
}

//===========================================================================//

TEST(IntVector3Multiply)
{
	IntVector3 vector(2, 3, 4);
	IntVector3 multiplyingVector(3, 4, 5);

	vector *= multiplyingVector;

	ARE_EQUAL(6, vector.X);
	ARE_EQUAL(12, vector.Y);
	ARE_EQUAL(20, vector.Z);
}

//===========================================================================//

TEST(IntVector3MultiplyFactor)
{
	IntVector3 vector(2, 3, 4);

	vector *= 2;

	ARE_EQUAL(4, vector.X);
	ARE_EQUAL(6, vector.Y);
	ARE_EQUAL(8, vector.Z);
}

//===========================================================================//

TEST(IntVector3Divide)
{
	IntVector3 vector(6, 12, 20);
	IntVector3 dividingVector(2, 3, 4);

	vector /= dividingVector;

	ARE_EQUAL(3, vector.X);
	ARE_EQUAL(4, vector.Y);
	ARE_EQUAL(5, vector.Z);
}

//===========================================================================//

TEST(IntVector3Plus)
{
	IntVector3 vector(-10, 13, 22);
	IntVector3 appendingVector(10, 14, 11);

	IntVector3 resultVector = vector + appendingVector;

	ARE_EQUAL(0, resultVector.X);
	ARE_EQUAL(27, resultVector.Y);
	ARE_EQUAL(33, resultVector.Z);
}

//===========================================================================//

TEST(IntVector3Minus)
{
	IntVector3 vector(-10, 13, 22);
	IntVector3 detachingVector(10, 14, 11);

	IntVector3 resultVector = vector - detachingVector;

	ARE_EQUAL(-20, resultVector.X);
	ARE_EQUAL(-1, resultVector.Y);
	ARE_EQUAL(11, resultVector.Z);
}

//===========================================================================//

TEST(IntVector3Multiplication)
{
	IntVector3 vector(2, 3, 4);
	IntVector3 multiplyingVector(3, 4, 5);

	IntVector3 resultVector = vector * multiplyingVector;

	ARE_EQUAL(6, resultVector.X);
	ARE_EQUAL(12, resultVector.Y);
	ARE_EQUAL(20, resultVector.Z);
}

//===========================================================================//

TEST(IntVector3MultiplicationFactor)
{
	IntVector3 vector(2, 3, 4);

	IntVector3 resultVector = vector * 2;

	ARE_EQUAL(4, resultVector.X);
	ARE_EQUAL(6, resultVector.Y);
	ARE_EQUAL(8, resultVector.Z);
}

//===========================================================================//

TEST(IntVector3Division)
{
	IntVector3 vector(6, 12, 20);
	IntVector3 dividingVector(2, 3, 4);

	IntVector3 resultVector = vector / dividingVector;

	ARE_EQUAL(3, resultVector.X);
	ARE_EQUAL(4, resultVector.Y);
	ARE_EQUAL(5, resultVector.Z);
}

//===========================================================================//

TEST(IntVector3Equals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	IntVector3 vector(-10.5f, 13.4f, 22.3f);
	IntVector3 equalsVector(-10.5f, 13.4f, 22.3f);
	IntVector3 notEqualsVector(10.5f, 14.6f, 11.6f);

	IS_TRUE(vector == equalsVector);
	IS_TRUE(vector != notEqualsVector);
}

//===========================================================================//

TEST(IntVector3OperatorNegative)
{
	IntVector3 vector(5, 10, 15);

	IntVector3 resultVector = -vector;

	ARE_EQUAL(-5, resultVector.X);
	ARE_EQUAL(-10, resultVector.Y);
	ARE_EQUAL(-15, resultVector.Z);
}

//===========================================================================//

TEST(IntVector3Clear)
{
	IntVector3 vector(-10.5f, 13.4f, 22.3f);

	vector.Clear();

	ARE_EQUAL(0, vector.X);
	ARE_EQUAL(0, vector.Y);
	ARE_EQUAL(0, vector.Z);
}

//===========================================================================//

TEST(IntVector3DistanceTo)
{
	IntVector3 vector(5, 10, 15);
	IntVector3 otherVector(8, 4, 22);

	ARE_CLOSE(9.695360f, vector.DistanceTo(otherVector), Math::Accuracy);
}

//===========================================================================//

TEST(IntVector3Negative)
{
	IntVector3 vector(5, 10, 15);

	vector.Negative();

	ARE_EQUAL(-5, vector.X);
	ARE_EQUAL(-10, vector.Y);
	ARE_EQUAL(-15, vector.Z);
}

//===========================================================================//

TEST(IntVector3NegativeX)
{
	IntVector3 vector(5, 10, 15);

	vector.NegativeX();

	ARE_EQUAL(-5, vector.X);
	ARE_EQUAL(10, vector.Y);
	ARE_EQUAL(15, vector.Z);
}

//===========================================================================//

TEST(IntVector3NegativeY)
{
	IntVector3 vector(5, 10, 15);

	vector.NegativeY();

	ARE_EQUAL(5, vector.X);
	ARE_EQUAL(-10, vector.Y);
	ARE_EQUAL(15, vector.Z);
}

//===========================================================================//

TEST(IntVector3NegativeZ)
{
	IntVector3 vector(5, 10, 15);

	vector.NegativeZ();

	ARE_EQUAL(5, vector.X);
	ARE_EQUAL(10, vector.Y);
	ARE_EQUAL(-15, vector.Z);
}

//===========================================================================//

TEST(IntVector3ToString)
{
	IntVector3 vector(5, 10, 15);

	ARE_EQUAL("5, 10, 15", vector.ToString());
}

//===========================================================================//

}// namespace Bru

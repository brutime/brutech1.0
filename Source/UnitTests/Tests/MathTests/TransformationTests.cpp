//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса трансформации
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(TransformationRotate)
{
	Transformation transformation;
	transformation.MakeIdentity();

	//    Matrix4x4 rotated1(0.f, -1.f, 0.f, 0.f,
//                       1.f, 0.f, 0.f, 0.f,
//                       0.f, 0.f, 1.f, 0.f,
//                       0.f, 0.f, 0.f, 1.f);

	Matrix4x4 rotated1(0.f, 1.f, 0.f, 0.f, -1.f, 0.f, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);

	//в openGL ось Z смотрит НА нас, т.е. поворот на 90 градусов - это поворот ПРОТИВ ч.с.
	transformation.Rotate(0, 0, 1, -90);
	ARE_ARRAYS_CLOSE(rotated1.GetElements(), transformation.GetMatrix().GetElements(), 16, Math::Accuracy);
	IS_TRUE(transformation.GetPosition() == Vector3(0.f, 0.f, 0.f));
	ARE_CLOSE(0.f, transformation.GetAngleX(), Math::Accuracy);
	ARE_CLOSE(0.f, transformation.GetAngleY(), Math::Accuracy);
	ARE_CLOSE(270.f, transformation.GetAngleZ(), Math::Accuracy);

	transformation.Rotate(0, 0, 1, 90); // вернули

	Matrix4x4 rotated2(1.f, 0.f, 0.f, 0.f, 0.f, 0.f, -1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 0.f, 1.f);

	transformation.Rotate(1, 0, 0, 90);
	IS_TRUE(transformation.GetMatrix() == rotated2);
	IS_TRUE(transformation.GetPosition() == Vector3(0.f, 0.f, 0.f));
	ARE_CLOSE(90.f, transformation.GetAngleX(), Math::Accuracy);
	ARE_CLOSE(0.f, transformation.GetAngleY(), Math::Accuracy);
	ARE_CLOSE(0.f, transformation.GetAngleZ(), Math::Accuracy);
}

//===========================================================================//

TEST(TransformationRotate2D)
{
	Transformation transformation;
	transformation.MakeIdentity();

	const float hsr2 = Math::Sqrt(2.f) * .5f;
	Matrix4x4 rotated1(hsr2, hsr2, 0.f, 0.f, -hsr2, hsr2, 0.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 0.f, 1.f);

	//в 2д угол растет против часовой стрелки
	transformation.Rotate2D(-45);
	IS_TRUE(transformation.GetMatrix() == rotated1);
	IS_TRUE(transformation.GetPosition() == Vector3(0.f, 0.f, 0.f));
	ARE_CLOSE(0.f, transformation.GetAngleX(), Math::Accuracy);
	ARE_CLOSE(0.f, transformation.GetAngleY(), Math::Accuracy);
	ARE_CLOSE(315.f, transformation.GetAngleZ(), Math::Accuracy);
}

//===========================================================================//

TEST(TransformationToString)
{
	Transformation transformation;
	transformation.MakeIdentity();
	transformation.Translate(5.151515f, 10.666f, 15.333666f);
	transformation.Rotate(0.33f, 0.66f, 0.99f, 45.6666f);

	ARE_EQUAL("Position: 6.2914, 12.1798, 17.7618\nAngles: 15.07, 30.14, 45.21", transformation.ToString());
}

//===========================================================================//

}// namespace Bru

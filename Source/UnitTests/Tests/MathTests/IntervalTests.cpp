//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты интервала (float)
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(IntervalDefaultConstructor)
{
	Interval interval;

	ARE_CLOSE(0.0f, interval.From, Math::Accuracy);
	ARE_CLOSE(0.0f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalConstructor)
{
	Interval interval(-10.4f, 22.1614f);

	ARE_CLOSE(-10.4f, interval.From, Math::Accuracy);
	ARE_CLOSE(22.1614f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalStringConstructor)
{
	Interval interval("-10.4:22.1614");

	ARE_CLOSE(-10.4f, interval.From, Math::Accuracy);
	ARE_CLOSE(22.1614f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalStringConstructorExceptions)
{
	IS_THROW(Interval("-10.4"), InvalidArgumentException);
	IS_THROW(Interval("-10.4:22.1614:0.666"), InvalidArgumentException);
}

//===========================================================================//

TEST(IntervalCopyConstructor)
{
	Interval sourceInterval(66.6f, -0.1415f);
	Interval interval(sourceInterval);

	sourceInterval.Clear();

	ARE_CLOSE(66.6f, interval.From, Math::Accuracy);
	ARE_CLOSE(-0.1415f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalCopyOperator)
{
	Interval sourceInterval(66.6f, -0.1415f);
	Interval interval;

	interval = sourceInterval;

	sourceInterval.Clear();

	ARE_CLOSE(66.6f, interval.From, Math::Accuracy);
	ARE_CLOSE(-0.1415f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalCopyOperatorSelf)
{
	Interval interval(66.6f, -0.1415f);

	interval = interval;

	ARE_CLOSE(66.6f, interval.From, Math::Accuracy);
	ARE_CLOSE(-0.1415f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalAppend)
{
	Interval interval(-10.5f, 13.4f);
	Interval appendingInterval(10.5f, 14.6f);

	interval += appendingInterval;

	ARE_CLOSE(0.0f, interval.From, Math::Accuracy);
	ARE_CLOSE(28.0f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalDetach)
{
	Interval interval(-10.5f, 13.4f);
	Interval detachingInterval(10.5f, 14.6f);

	interval -= detachingInterval;

	ARE_CLOSE(-21.0f, interval.From, Math::Accuracy);
	ARE_CLOSE(-1.2f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalPlus)
{
	Interval interval(-10.5f, 13.4f);
	Interval appendingInterval(10.5f, 14.6f);

	Interval resultInterval = interval + appendingInterval;

	ARE_CLOSE(0.0f, resultInterval.From, Math::Accuracy);
	ARE_CLOSE(28.0f, resultInterval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalMinus)
{
	Interval interval(-10.5f, 13.4f);
	Interval detachingInterval(10.5f, 14.6f);

	Interval resultInterval = interval - detachingInterval;

	ARE_CLOSE(-21.0f, resultInterval.From, Math::Accuracy);
	ARE_CLOSE(-1.2f, resultInterval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalEquals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	Interval interval(-10.5f, 13.4f);
	Interval equalsInterval(-10.5f, 13.4f);
	Interval notEqualsInterval(10.5f, 14.6f);

	IS_TRUE(interval == equalsInterval);
	IS_TRUE(interval != notEqualsInterval);
}

//===========================================================================//

TEST(IntervalLess)
{
	Interval interval(5.0f, 10.0f);
	Interval lessInterval(3.0f, 8.0f);

	IS_TRUE(lessInterval < interval);
	IS_FALSE((lessInterval > interval));
	IS_FALSE((lessInterval == interval));
}

//===========================================================================//

TEST(IntervalGreater)
{
	Interval interval(5.0f, 10.0f);
	Interval greaterInterval(7.0f, 12.0f);

	IS_TRUE(greaterInterval > interval);
	IS_FALSE((greaterInterval < interval));
	IS_FALSE((greaterInterval == interval));
}

//===========================================================================//

TEST(IntervalGetIntersection)
{
	Interval innerInterval = Interval(5.0f, 10.0f).GetIntersection(Interval(7.0f, 8.0f));
	ARE_CLOSE(7.0f, innerInterval.From, Math::Accuracy);
	ARE_CLOSE(8.0f, innerInterval.To, Math::Accuracy);
	
	Interval leftInterval = Interval(5.0f, 10.0f).GetIntersection(Interval(4.0f, 8.0f));
	ARE_CLOSE(5.0f, leftInterval.From, Math::Accuracy);
	ARE_CLOSE(8.0f, leftInterval.To, Math::Accuracy);	
	
	Interval rightInterval = Interval(5.0f, 10.0f).GetIntersection(Interval(7.0f, 12.0f));
	ARE_CLOSE(7.0f, rightInterval.From, Math::Accuracy);
	ARE_CLOSE(10.0f, rightInterval.To, Math::Accuracy);
	
	Interval equalInterval = Interval(5.0f, 10.0f).GetIntersection(Interval(5.0f, 10.0f));
	ARE_CLOSE(5.0f, equalInterval.From, Math::Accuracy);
	ARE_CLOSE(10.0f, equalInterval.To, Math::Accuracy);
	
	Interval coveringInterval = Interval(7.0f, 8.0f).GetIntersection(Interval(5.0f, 10.0f));
	ARE_CLOSE(7.0f, coveringInterval.From, Math::Accuracy);
	ARE_CLOSE(8.0f, coveringInterval.To, Math::Accuracy);	
	
	Interval outherInterval = Interval(5.0f, 10.0f).GetIntersection(Interval(11.0f, 16.0f));
	ARE_CLOSE(11.0f, outherInterval.From, Math::Accuracy);
	ARE_CLOSE(10.0f, outherInterval.To, Math::Accuracy);		
}

//===========================================================================//

TEST(IntervalCut)
{
	Array<Interval> innerIntervalCutResult = Interval(5.0f, 10.0f).Cut(Interval(7.0f, 8.0f));
	ARE_EQUAL(2, innerIntervalCutResult.GetCount());
	ARE_CLOSE(5.0f, innerIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(7.0f, innerIntervalCutResult[0].To, Math::Accuracy);
	ARE_CLOSE(8.0f, innerIntervalCutResult[1].From, Math::Accuracy);
	ARE_CLOSE(10.0f, innerIntervalCutResult[1].To, Math::Accuracy);
	
	Array<Interval> leftIntervalCutResult = Interval(5.0f, 10.0f).Cut(Interval(4.0f, 8.0f));
	ARE_EQUAL(1, leftIntervalCutResult.GetCount());
	ARE_CLOSE(8.0f, leftIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(10.0f, leftIntervalCutResult[0].To, Math::Accuracy);

	Array<Interval> leftEqualIntervalCutResult = Interval(5.0f, 10.0f).Cut(Interval(5.0f, 8.0f));
	ARE_EQUAL(1, leftEqualIntervalCutResult.GetCount());
	ARE_CLOSE(8.0f, leftEqualIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(10.0f, leftEqualIntervalCutResult[0].To, Math::Accuracy);

	Array<Interval> rightIntervalCutResult = Interval(5.0f, 10.0f).Cut(Interval(7.0f, 12.0f));
	ARE_EQUAL(1, rightIntervalCutResult.GetCount());
	ARE_CLOSE(5.0f, rightIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(7.0f, rightIntervalCutResult[0].To, Math::Accuracy);	
	
	Array<Interval> rightEqualIntervalCutResult = Interval(5.0f, 10.0f).Cut(Interval(7.0f, 10.0f));
	ARE_EQUAL(1, rightEqualIntervalCutResult.GetCount());
	ARE_CLOSE(5.0f, rightEqualIntervalCutResult[0].From, Math::Accuracy);	
	ARE_CLOSE(7.0f, rightEqualIntervalCutResult[0].To, Math::Accuracy);	
	
	Array<Interval> equalIntervalCutResult = Interval(5.0f, 10.0f).Cut(Interval(5.0f, 10.0f));
	ARE_EQUAL(0, equalIntervalCutResult.GetCount());	
	
	Array<Interval> coveringIntervalCutResult = Interval(7.0f, 8.0f).Cut(Interval(5.0f, 10.0f));
	ARE_EQUAL(0, coveringIntervalCutResult.GetCount());		
	
	Array<Interval> outherIntervalCutResult = Interval(5.0f, 10.0f).Cut(Interval(11.0f, 16.0f));
	ARE_EQUAL(0, outherIntervalCutResult.GetCount());
}

//===========================================================================//

TEST(IntervalContains)
{
	Interval interval(-10.5f, 13.4f);

	IS_FALSE(interval.Contains(-10.6f));
	IS_TRUE(interval.Contains(-10.5f));
	IS_TRUE(interval.Contains(0.0f));
	IS_TRUE(interval.Contains(13.4f));
	IS_FALSE(interval.Contains(13.5f));
}

//===========================================================================//

TEST(IntervalGetLength)
{
	Interval interval(-10.5f, 13.4f);

	ARE_CLOSE(23.9f, interval.GetLength(), Math::Accuracy);
}

//===========================================================================//

TEST(IntervalGetRandom)
{
	Interval interval(100.0f, 1500.0f);

	const integer GenerateRandomCount = 100;
	for (integer i = 0; i < GenerateRandomCount; i++)
	{
		float randomValue = interval.GetRandom();
		IS_TRUE(randomValue >= interval.From);
		IS_TRUE(randomValue <= interval.To);
	}
}

//===========================================================================//

TEST(IntervalClear)
{
	Interval interval(-10.4f, 22.1614f);

	interval.Clear();

	ARE_CLOSE(0.0f, interval.From, Math::Accuracy);
	ARE_CLOSE(0.0f, interval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntervalToString)
{
	Interval interval(5.151515f, 10.666f);

	ARE_EQUAL("5.1515:10.6660", interval.ToString());
}

//===========================================================================//

}// namespace Bru

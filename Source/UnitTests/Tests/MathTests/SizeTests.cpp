//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты размера (float)
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(SizeDefaultConstructor)
{
	Size size;

	ARE_CLOSE(0.0f, size.Width, Math::Accuracy);
	ARE_CLOSE(0.0f, size.Height, Math::Accuracy);
}

//===========================================================================//

TEST(SizeConstructor)
{
	Size size(-10.4f, 22.1614f);

	ARE_CLOSE(-10.4f, size.Width, Math::Accuracy);
	ARE_CLOSE(22.1614f, size.Height, Math::Accuracy);
}

//===========================================================================//

TEST(SizeStringConstructor)
{
	Size size("-10.4, 22.1614");

	ARE_CLOSE(-10.4f, size.Width, Math::Accuracy);
	ARE_CLOSE(22.1614f, size.Height, Math::Accuracy);
}

//===========================================================================//

TEST(SizeStringConstructorExceptions)
{
	IS_THROW(Size("-10.4"), InvalidArgumentException);
	IS_THROW(Size("-10.4:22.1614:0.666"), InvalidArgumentException);
}

//===========================================================================//

TEST(SizeCopyConstructor)
{
	Size sourceSize(66.6f, -0.1415f);
	Size size(sourceSize);

	sourceSize.Clear();

	ARE_CLOSE(66.6f, size.Width, Math::Accuracy);
	ARE_CLOSE(-0.1415f, size.Height, Math::Accuracy);
}

//===========================================================================//

TEST(SizeCopyOperator)
{
	Size sourceSize(66.6f, -0.1415f);
	Size size;

	size = sourceSize;

	sourceSize.Clear();

	ARE_CLOSE(66.6f, size.Width, Math::Accuracy);
	ARE_CLOSE(-0.1415f, size.Height, Math::Accuracy);
}

//===========================================================================//

TEST(SizeCopyOperatorSelf)
{
	Size size(66.6f, -0.1415f);

	size = size;

	ARE_CLOSE(66.6f, size.Width, Math::Accuracy);
	ARE_CLOSE(-0.1415f, size.Height, Math::Accuracy);
}

//===========================================================================//

TEST(SizeEquals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	Size size(-10.5f, 13.4f);
	Size equalsSize(-10.5f, 13.4f);
	Size notEqualsSize(10.5f, 14.6f);

	IS_TRUE(size == equalsSize);
	IS_TRUE(size != notEqualsSize);
}

//===========================================================================//

TEST(SizeClear)
{
	Size size(-10.4f, 22.1614f);

	size.Clear();

	ARE_CLOSE(0.0f, size.Width, Math::Accuracy);
	ARE_CLOSE(0.0f, size.Height, Math::Accuracy);
}

//===========================================================================//

TEST(SizeToString)
{
	Size size(5.151515f, 10.666f);

	ARE_EQUAL("5.1515, 10.6660", size.ToString());
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса матрицы 4x4
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(Matrix4x4DefaultConstructor)
{
	Matrix4x4 matrix;

	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			ARE_CLOSE(0.0f, matrix.GetElement(i, j), Math::Accuracy);
		}
	}
}

//===========================================================================//

TEST(Matrix4x4ElementsConstructor)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	float value = 1.0f;
	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			ARE_CLOSE(value, matrix.GetElement(i, j), Math::Accuracy);
			value += 1.0f;
		}
	}
}

//===========================================================================//

TEST(Matrix4x4ArrayConstructor)
{
	Array<float>::SimpleType expectedArray(16);
	float sourceValue = 1.0f;
	for (integer i = 0; i < 16; i++)
	{
		expectedArray[i] = sourceValue;
		sourceValue += 1.0f;
	}

	Matrix4x4 matrix(expectedArray);

	float value = 1.0f;
	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			ARE_CLOSE(value, matrix.GetElement(i, j), Math::Accuracy);
			value += 1.0f;
		}
	}
}

//===========================================================================//

TEST(Matrix4x4VectorsConstructor)
{
	Vector4 firstLine(1.0f, 2.0f, 3.0f, 4.0f);
	Vector4 secondLine(5.0f, 6.0f, 7.0f, 8.0f);
	Vector4 thirdLine(9.0f, 10.0f, 11.0f, 12.0f);
	Vector4 fourthLine(13.0f, 14.0f, 15.0f, 16.0f);

	Matrix4x4 matrix(firstLine, secondLine, thirdLine, fourthLine);

	float value = 1.0f;
	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			ARE_CLOSE(value, matrix.GetElement(i, j), Math::Accuracy);
			value += 1.0f;
		}
	}
}

//===========================================================================//

TEST(Matrix4x4CopyConstructor)
{
	Array<float>::SimpleType expectedArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f,
	                         12.0f, 13.0f, 14.0f, 15.0f, 16.0f
	                                         };

	Matrix4x4 sourceMatrix(expectedArray);
	Matrix4x4 matrix(sourceMatrix);

	sourceMatrix.MakeZero();

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4MoveConstructor)
{
	Array<float>::SimpleType expectedArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f,
	                         12.0f, 13.0f, 14.0f, 15.0f, 16.0f
	                                         };

	Matrix4x4 matrix(std::move(Matrix4x4(expectedArray)));

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4CopyOperator)
{
	Array<float>::SimpleType expectedArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f,
	                         12.0f, 13.0f, 14.0f, 15.0f, 16.0f
	                                         };

	Matrix4x4 sourceMatrix(expectedArray);

	Matrix4x4 matrix;
	matrix = sourceMatrix;

	sourceMatrix.MakeZero();

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4CopyOperatorSelf)
{
	Array<float>::SimpleType expectedArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f,
	                         12.0f, 13.0f, 14.0f, 15.0f, 16.0f
	                                         };

	Matrix4x4 matrix(expectedArray);

	matrix = matrix;

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4MoveOperator)
{
	Array<float>::SimpleType expectedArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f,
	                         12.0f, 13.0f, 14.0f, 15.0f, 16.0f
	                                         };

	Matrix4x4 matrix;
	matrix = std::move(Matrix4x4(expectedArray));

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4Append)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	Matrix4x4 appendingMatrix = { 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 27.0f, 28.0f,
	                              29.0f, 30.0f, 31.0f, 32.0f
	                            };

	matrix += appendingMatrix;

	Array<float>::SimpleType expectedArray = { 18.0f, 20.0f, 22.0f, 24.0f, 26.0f, 28.0f, 30.0f, 32.0f, 34.0f, 36.0f,
	                         38.0f, 40.0f, 42.0f, 44.0f, 46.0f, 48.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4Detach)
{
	Matrix4x4 matrix = { 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 27.0f, 28.0f, 29.0f,
	                     30.0f, 31.0f, 32.0f
	                   };

	Matrix4x4 detachingMatrix = { 16.0f, 15.0f, 14.0f, 13.0f, 12.0f, 11.0f, 10.0f, 9.0f, 8.0f, 7.0f, 6.0f, 5.0f, 4.0f,
	                              3.0f, 2.0f, 1.0f
	                            };

	matrix -= detachingMatrix;

	Array<float>::SimpleType expectedArray = { 1.0f, 3.0f, 5.0f, 7.0f, 9.0f, 11.0f, 13.0f, 15.0f, 17.0f, 19.0f, 21.0f,
	                         23.0f, 25.0f, 27.0f, 29.0f, 31.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4Multiply)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	Matrix4x4 multiplyingMatrix = { 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 27.0f, 28.0f,
	                                29.0f, 30.0f, 31.0f, 32.0f
	                              };

	matrix *= multiplyingMatrix;

	Array<float>::SimpleType expectedArray = { 250.0f, 260.0f, 270.0f, 280.0f, 618.0f, 644.0f, 670.0f, 696.0f, 986.0f,
	                         1028.0f, 1070.0f, 1112.0f, 1354.0f, 1412.0f, 1470.0f, 1528.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4ScalarMultiply)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	float scalarElement = 2.0f;

	matrix *= scalarElement;

	Array<float>::SimpleType expectedArray = { 2.0f, 4.0f, 6.0f, 8.0f, 10.0f, 12.0f, 14.0f, 16.0f, 18.0f, 20.0f, 22.0f,
	                         24.0f, 26.0f, 28.0f, 30.0f, 32.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4ScalarDivide)
{
	Matrix4x4 matrix = { 2.0f, 4.0f, 6.0f, 8.0f, 10.0f, 12.0f, 14.0f, 16.0f, 18.0f, 20.0f, 22.0f, 24.0f, 26.0f, 28.0f,
	                     30.0f, 32.0f
	                   };

	float scalarElement = 2.0f;

	matrix /= scalarElement;

	Array<float>::SimpleType expectedArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f,
	                         12.0f, 13.0f, 14.0f, 15.0f, 16.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4Plus)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	Matrix4x4 appendingMatrix = { 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 27.0f, 28.0f,
	                              29.0f, 30.0f, 31.0f, 32.0f
	                            };

	Matrix4x4 resultMatrix = matrix + appendingMatrix;

	Array<float>::SimpleType expectedArray = { 18.0f, 20.0f, 22.0f, 24.0f, 26.0f, 28.0f, 30.0f, 32.0f, 34.0f, 36.0f,
	                         38.0f, 40.0f, 42.0f, 44.0f, 46.0f, 48.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, resultMatrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4Minus)
{
	Matrix4x4 matrix = { 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 27.0f, 28.0f, 29.0f,
	                     30.0f, 31.0f, 32.0f
	                   };

	Matrix4x4 detachingMatrix = { 16.0f, 15.0f, 14.0f, 13.0f, 12.0f, 11.0f, 10.0f, 9.0f, 8.0f, 7.0f, 6.0f, 5.0f, 4.0f,
	                              3.0f, 2.0f, 1.0f
	                            };

	Matrix4x4 resultMatrix = matrix - detachingMatrix;

	Array<float>::SimpleType expectedArray = { 1.0f, 3.0f, 5.0f, 7.0f, 9.0f, 11.0f, 13.0f, 15.0f, 17.0f, 19.0f, 21.0f,
	                         23.0f, 25.0f, 27.0f, 29.0f, 31.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, resultMatrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4Multiplication)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	Matrix4x4 multiplyingMatrix = { 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 27.0f, 28.0f,
	                                29.0f, 30.0f, 31.0f, 32.0f
	                              };

	Matrix4x4 resultMatrix = matrix * multiplyingMatrix;

	Array<float>::SimpleType expectedArray = { 250.0f, 260.0f, 270.0f, 280.0f, 618.0f, 644.0f, 670.0f, 696.0f, 986.0f,
	                         1028.0f, 1070.0f, 1112.0f, 1354.0f, 1412.0f, 1470.0f, 1528.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, resultMatrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4ScalarMultiplication)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	float scalarElement = 2.0f;

	Matrix4x4 resultMatrix = matrix * scalarElement;

	Array<float>::SimpleType expectedArray = { 2.0f, 4.0f, 6.0f, 8.0f, 10.0f, 12.0f, 14.0f, 16.0f, 18.0f, 20.0f, 22.0f,
	                         24.0f, 26.0f, 28.0f, 30.0f, 32.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, resultMatrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4ScalarDivision)
{
	Matrix4x4 matrix = { 2.0f, 4.0f, 6.0f, 8.0f, 10.0f, 12.0f, 14.0f, 16.0f, 18.0f, 20.0f, 22.0f, 24.0f, 26.0f, 28.0f,
	                     30.0f, 32.0f
	                   };

	float scalarElement = 2.0f;

	Matrix4x4 resultMatrix = matrix / scalarElement;

	Array<float>::SimpleType expectedArray = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f,
	                         12.0f, 13.0f, 14.0f, 15.0f, 16.0f
	                                         };

	ARE_ARRAYS_CLOSE(expectedArray, resultMatrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4Equals)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	Matrix4x4 equalsMatrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f,
	                           15.0f, 16.0f
	                         };

	IS_TRUE(matrix == equalsMatrix);

	Matrix4x4 notEqualsMatrix = { 7.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f, 27.0f, 28.0f,
	                              29.0f, 30.0f, 31.0f, 32.0f
	                            };

	Matrix4x4 justOneElementNotEqualsMatrix;

	IS_TRUE(matrix != notEqualsMatrix);
	//Даже если одно число не совпадает - матрицы не равны
	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			justOneElementNotEqualsMatrix = equalsMatrix;
			justOneElementNotEqualsMatrix.SetElement(i, j, notEqualsMatrix.GetElement(i, j));
			IS_TRUE(matrix != justOneElementNotEqualsMatrix);
		}
	}
}

//===========================================================================//

TEST(Matrix4x4MakeZero)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	matrix.MakeZero();

	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			ARE_CLOSE(0, matrix.GetElement(i, j), Math::Accuracy);
		}
	}
}

//===========================================================================//

TEST(Matrix4x4MakeIdentity)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	matrix.MakeIdentity();

	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			if (i == j)
			{
				ARE_CLOSE(1, matrix.GetElement(i, j), Math::Accuracy);
			}
			else
			{
				ARE_CLOSE(0, matrix.GetElement(i, j), Math::Accuracy);
			}
		}
	}
}

//===========================================================================//

TEST(Matrix4x4Transpose)
{
	Matrix4x4 matrix = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                     16.0f
	                   };

	Array<float>::SimpleType expectedArray = { 1.0f, 5.0f, 9.0f, 13.0f, 2.0f, 6.0f, 10.0f, 14.0f, 3.0f, 7.0f, 11.0f,
	                         15.0f, 4.0f, 8.0f, 12.0f, 16.0f
	                                         };

	matrix.Transpose();

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

TEST(Matrix4x4SettersGetters)
{
	Matrix4x4 matrix;

	Array<float>::SimpleType expectedArray = { 17.0f, 18.0f, 19.0f, 20.0f, 21.0f, 22.0f, 23.0f, 24.0f, 25.0f, 26.0f,
	                         27.0f, 28.0f, 29.0f, 30.0f, 31.0f, 32.0f
	                                         };

	integer settingIndex = 0;
	for (integer i = 0; i < 4; i++)
	{
		for (integer j = 0; j < 4; j++)
		{
			matrix.SetElement(i, j, expectedArray[settingIndex]);
			settingIndex++;
		}
	}

	ARE_ARRAYS_CLOSE(expectedArray, matrix.GetData(), 16, Math::Accuracy);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса вектора, состоящего из 4-ех компонентов
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../Common/Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

TEST(Vector4DefaultConstructor)
{
	Vector4 vector;

	ARE_CLOSE(0.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Constructor)
{
	Vector4 vector(-10.4f, 22.1614f, 0.666f, -2.0001f);

	ARE_CLOSE(-10.4f, vector.X, Math::Accuracy);
	ARE_CLOSE(22.1614f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.666f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-2.0001f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4StringConstructor)
{
	Vector4 vector("-10.4, 22.1614, 0.666, -2.0001");

	ARE_CLOSE(-10.4f, vector.X, Math::Accuracy);
	ARE_CLOSE(22.1614f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.666f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-2.0001f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4StringConstructorExceptions)
{
	IS_THROW(Vector4("-10.4, 22.1614, 0.666"), InvalidArgumentException);
	IS_THROW(Vector4("-10.4, 22.1614, 0.666, -2.0001, -100.0"), InvalidArgumentException);
}

//===========================================================================//

TEST(Vector4CopyConstructor)
{
	Vector4 sourceVector(66.6f, -0.1415f, 1.5f, -0.7);
	Vector4 vector(sourceVector);

	sourceVector.Clear();

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-0.7f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4MoveConstructor)
{
	Vector4 vector(std::move(Vector4(66.6f, -0.1415f, 1.5f, -0.7)));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-0.7f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4CopyOperator)
{
	Vector4 sourceVector(66.6f, -0.1415f, 1.5f, -0.7);
	Vector4 vector;

	vector = sourceVector;

	sourceVector.Clear();

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-0.7f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4CopyOperatorSeft)
{
	Vector4 vector(66.6f, -0.1415f, 1.5f, -0.7);

	vector = vector;

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-0.7f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4MoveOperator)
{
	Vector4 vector;

	vector = std::move(Vector4(66.6f, -0.1415f, 1.5f, -0.7));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-0.7f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Append)
{
	Vector4 vector(-10.5f, 13.4f, 22.3f, -11.3);
	Vector4 appendingVector(10.5f, 14.6f, 11.6f, 8.3);

	vector += appendingVector;

	ARE_CLOSE(0.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(28.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(33.9f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-3.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Detach)
{
	Vector4 vector(-10.5f, 13.4f, 22.3f, -11.3f);
	Vector4 detachingVector(10.5f, 14.6f, 11.6f, 8.3f);

	vector -= detachingVector;

	ARE_CLOSE(-21.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-1.2f, vector.Y, Math::Accuracy);
	ARE_CLOSE(10.7f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-19.6f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Multiply)
{
	Vector4 vector(2.0f, 3.0f, 4.0f, 5.0f);
	Vector4 multiplyingVector(3.0f, 4.0f, 5.0f, 6.0f);

	vector *= multiplyingVector;

	ARE_CLOSE(6.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(12.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(20.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(30.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4MultiplyFactor)
{
	Vector4 vector(2.0f, 3.0f, 4.0f, 5.0f);

	vector *= 2.0f;

	ARE_CLOSE(4.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(6.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(8.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(10.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4MultiplyMatrix)
{
	Matrix4x4 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                 16.0f);

	Vector4 vector(2.0f, 3.0f, 4.0f, 5.0f);

	vector *= matrix;

	ARE_CLOSE(118.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(132.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(146.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(160.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Divide)
{
	Vector4 vector(6.0f, 12.0f, 20.0f, 30.0f);
	Vector4 dividingVector(2.0f, 3.0f, 4.0f, 5.0f);

	vector /= dividingVector;

	ARE_CLOSE(3.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(4.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(5.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(6.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Plus)
{
	Vector4 vector(-10.5f, 13.4f, 22.3f, -11.3);
	Vector4 appendingVector(10.5f, 14.6f, 11.6f, 8.3);

	Vector4 resultVector = vector + appendingVector;

	ARE_CLOSE(0.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(28.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(33.9f, resultVector.Z, Math::Accuracy);
	ARE_CLOSE(-3.0f, resultVector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Minus)
{
	Vector4 vector(-10.5f, 13.4f, 22.3f, -11.3f);
	Vector4 detachingVector(10.5f, 14.6f, 11.6f, 8.3f);

	Vector4 resultVector = vector - detachingVector;

	ARE_CLOSE(-21.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(-1.2f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(10.7f, resultVector.Z, Math::Accuracy);
	ARE_CLOSE(-19.6f, resultVector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Multiplication)
{
	Vector4 vector(2.0f, 3.0f, 4.0f, 5.0f);
	Vector4 multiplyingVector(3.0f, 4.0f, 5.0f, 6.0f);

	Vector4 resultVector = vector * multiplyingVector;

	ARE_CLOSE(6.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(12.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(20.0f, resultVector.Z, Math::Accuracy);
	ARE_CLOSE(30.0f, resultVector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4MultiplicationFactor)
{
	Vector4 vector(2.0f, 3.0f, 4.0f, 5.0f);

	Vector4 resultVector = vector * 2.0f;

	ARE_CLOSE(4.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(6.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(8.0f, resultVector.Z, Math::Accuracy);
	ARE_CLOSE(10.0f, resultVector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4MultiplicationMatrix)
{
	Matrix4x4 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                 16.0f);

	Vector4 vector(2.0f, 3.0f, 4.0f, 5.0f);

	Vector4 resultVector = vector * matrix;

	ARE_CLOSE(118.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(132.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(146.0f, resultVector.Z, Math::Accuracy);
	ARE_CLOSE(160.0f, resultVector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Division)
{
	Vector4 vector(6.0f, 12.0f, 20.0f, 30.0f);
	Vector4 dividingVector(2.0f, 3.0f, 4.0f, 5.0f);

	Vector4 resultVector = vector / dividingVector;

	ARE_CLOSE(3.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(4.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(5.0f, resultVector.Z, Math::Accuracy);
	ARE_CLOSE(6.0f, resultVector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Equals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	Vector4 vector(-10.5f, 13.4f, 22.3f, -11.3f);
	Vector4 equalsVector(-10.5f, 13.4f, 22.3f, -11.3f);
	Vector4 notEqualsVector(10.5f, 14.6f, 11.6f, 8.3f);

	IS_TRUE(vector == equalsVector);
	IS_TRUE(vector != notEqualsVector);
}

//===========================================================================//

TEST(Vector4OperatorNegative)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	Vector4 resultVector = -vector;

	ARE_CLOSE(-5.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(-15.0f, resultVector.Z, Math::Accuracy);
	ARE_CLOSE(-20.0f, resultVector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Clear)
{
	Vector4 vector(-10.5f, 13.4f, 22.3f, -11.3f);

	vector.Clear();

	ARE_CLOSE(0, vector.X, Math::Accuracy);
	ARE_CLOSE(0, vector.Y, Math::Accuracy);
	ARE_CLOSE(0, vector.Z, Math::Accuracy);
	ARE_CLOSE(0, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Length)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	ARE_CLOSE(27.386128f, vector.Length(), Math::Accuracy);
}

//===========================================================================//

TEST(Vector4ZeroLength)
{
	Vector4 vector(0.0f, 0.0f, 0.0f, 0.0f);

	ARE_CLOSE(0.0f, vector.Length(), Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Normalize)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	vector.Normalize();

	ARE_CLOSE(0.182574f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.365148f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.547723f, vector.Z, Math::Accuracy);
	ARE_CLOSE(0.730297f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4DistanceTo)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);
	Vector4 otherVector(8.0f, 4.0f, 22.0f, 11.0f);

	ARE_CLOSE(13.228757f, vector.DistanceTo(otherVector), Math::Accuracy);
}

//===========================================================================//

TEST(Vector4Negative)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	vector.Negative();

	ARE_CLOSE(-5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(-15.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-20.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4NegativeX)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	vector.NegativeX();

	ARE_CLOSE(-5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(15.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(20.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4NegativeY)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	vector.NegativeY();

	ARE_CLOSE(5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(15.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(20.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4NegativeZ)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	vector.NegativeZ();

	ARE_CLOSE(5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(-15.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(20.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4NegativeW)
{
	Vector4 vector(5.0f, 10.0f, 15.0f, 20.0f);

	vector.NegativeW();

	ARE_CLOSE(5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(15.0f, vector.Z, Math::Accuracy);
	ARE_CLOSE(-20.0f, vector.W, Math::Accuracy);
}

//===========================================================================//

TEST(Vector4ToString)
{
	Vector4 vector(5.151515f, 10.666f, 15.333666f, 20.0044f);

	ARE_EQUAL("5.1515, 10.6660, 15.3337, 20.0044", vector.ToString());
}

//===========================================================================//

}// namespace Bru

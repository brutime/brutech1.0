//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса вектора, состоящего из 3-ех компонентов
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(Vector3DefaultConstructor)
{
	Vector3 vector;

	ARE_CLOSE(0.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Constructor)
{
	Vector3 vector(-10.4f, 22.1614f, 0.666f);

	ARE_CLOSE(-10.4f, vector.X, Math::Accuracy);
	ARE_CLOSE(22.1614f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.666f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3StringConstructor)
{
	Vector3 vector("-10.4, 22.1614, 0.666");

	ARE_CLOSE(-10.4f, vector.X, Math::Accuracy);
	ARE_CLOSE(22.1614f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.666f, vector.Z, Math::Accuracy);
	
	Vector3 zeroZVector("-10.4, 22.1614");

	ARE_CLOSE(-10.4f, zeroZVector.X, Math::Accuracy);
	ARE_CLOSE(22.1614f, zeroZVector.Y, Math::Accuracy);
	ARE_CLOSE(0.0f, zeroZVector.Z, Math::Accuracy);	
}

//===========================================================================//

TEST(Vector3StringConstructorExceptions)
{
	IS_THROW(Vector3("-10.4"), InvalidArgumentException);
	IS_THROW(Vector3("-10.4, 22.1614, 0.666, -2.0001"), InvalidArgumentException);
}

//===========================================================================//

TEST(Vector3Vector2Constructor)
{
	Vector3 vector(Vector2(66.6f, -0.1415f));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3CopyConstructor)
{
	Vector3 sourceVector(66.6f, -0.1415f, 1.5f);
	Vector3 vector(sourceVector);

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Vector2MoveConstructor)
{
	Vector3 vector(std::move(Vector2(66.6f, -0.1415f)));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3MoveConstructor)
{
	Vector3 vector(std::move(Vector3(66.6f, -0.1415f, 1.5f)));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Vector2CopyOperator)
{
	Vector3 sourceVector(66.6f, -0.1415f, 1.5f);
	Vector3 vector;

	vector = sourceVector;

	sourceVector.Clear();

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3CopyOperator)
{
	Vector3 sourceVector(66.6f, -0.1415f, 1.5f);
	Vector3 vector;

	vector = sourceVector;

	sourceVector.Clear();

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3CopyOperatorSelf)
{
	Vector3 vector(66.6f, -0.1415f, 1.5f);

	vector = vector;

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Vector2MoveOperator)
{
	Vector3 vector;

	vector = std::move(Vector2(66.6f, -0.1415f));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3MoveOperator)
{
	Vector3 vector;

	vector = std::move(Vector3(66.6f, -0.1415f, 1.5f));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
	ARE_CLOSE(1.5f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Append)
{
	Vector3 vector(-10.5f, 13.4f, 22.3f);
	Vector3 appendingVector(10.5f, 14.6f, 11.6f);

	vector += appendingVector;

	ARE_CLOSE(0.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(28.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(33.9f, vector.Z, Math::Accuracy);
}

//===========================================================================//
//
TEST(Vector3Detach)
{
	Vector3 vector(-10.5f, 13.4f, 22.3f);
	Vector3 detachingVector(10.5f, 14.6f, 11.6f);

	vector -= detachingVector;

	ARE_CLOSE(-21.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-1.2f, vector.Y, Math::Accuracy);
	ARE_CLOSE(10.7f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Multiply)
{
	Vector3 vector(2.0f, 3.0f, 4.0f);
	Vector3 multiplyingVector(3.0f, 4.0f, 5.0f);

	vector *= multiplyingVector;

	ARE_CLOSE(6.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(12.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(20.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3MultiplyFactor)
{
	Vector3 vector(2.0f, 3.0f, 4.0f);

	vector *= 2.0f;

	ARE_CLOSE(4.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(6.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(8.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Divide)
{
	Vector3 vector(6.0f, 12.0f, 20.0f);
	Vector3 dividingVector(2.0f, 3.0f, 4.0f);

	vector /= dividingVector;

	ARE_CLOSE(3.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(4.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(5.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Plus)
{
	Vector3 vector(-10.5f, 13.4f, 22.3f);
	Vector3 appendingVector(10.5f, 14.6f, 11.6f);

	Vector3 resultVector = vector + appendingVector;

	ARE_CLOSE(0.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(28.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(33.9f, resultVector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Minus)
{
	Vector3 vector(-10.5f, 13.4f, 22.3f);
	Vector3 detachingVector(10.5f, 14.6f, 11.6f);

	Vector3 resultVector = vector - detachingVector;

	ARE_CLOSE(-21.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(-1.2f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(10.7f, resultVector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Multiplication)
{
	Vector3 vector(2.0f, 3.0f, 4.0f);
	Vector3 multiplyingVector(3.0f, 4.0f, 5.0f);

	Vector3 resultVector = vector * multiplyingVector;

	ARE_CLOSE(6.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(12.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(20.0f, resultVector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3MultiplicationFactor)
{
	Vector3 vector(2.0f, 3.0f, 4.0f);

	Vector3 resultVector = vector * 2.0f;

	ARE_CLOSE(4.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(6.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(8.0f, resultVector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Division)
{
	Vector3 vector(6.0f, 12.0f, 20.0f);
	Vector3 dividingVector(2.0f, 3.0f, 4.0f);

	Vector3 resultVector = vector / dividingVector;

	ARE_CLOSE(3.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(4.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(5.0f, resultVector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Equals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	Vector3 vector(-10.5f, 13.4f, 22.3f);
	Vector3 equalsVector(-10.5f, 13.4f, 22.3f);
	Vector3 notEqualsVector(10.5f, 14.6f, 11.6f);

	IS_TRUE(vector == equalsVector);
	IS_TRUE(vector != notEqualsVector);
}

//===========================================================================//

TEST(Vector3OperatorNegative)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);

	Vector3 resultVector = -vector;

	ARE_CLOSE(-5.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(-15.0f, resultVector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(VectorMultiplyByMatrix4x4AsPoint)
{
	Matrix4x4 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                 16.0f);

	Vector3 vector(1.f, 2.f, 3.f);

	vector = vector.MultiplyByMatrix4x4AsPoint(matrix);

	ARE_CLOSE(51.f, vector.X, Math::Accuracy);
	ARE_CLOSE(58.f, vector.Y, Math::Accuracy);
	ARE_CLOSE(65.f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(VectorMultiplyByMatrix4x4AsVector)
{
	Matrix4x4 matrix(1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f,
	                 16.0f);

	Vector3 vector(1.f, 2.f, 3.f);

	vector = vector.MultiplyByMatrix4x4AsVector(matrix);

	ARE_CLOSE(38.f, vector.X, Math::Accuracy);
	ARE_CLOSE(44.f, vector.Y, Math::Accuracy);
	ARE_CLOSE(50.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Clear)
{
	Vector3 vector(-10.5f, 13.4f, 22.3f);

	vector.Clear();

	ARE_CLOSE(0, vector.X, Math::Accuracy);
	ARE_CLOSE(0, vector.Y, Math::Accuracy);
	ARE_CLOSE(0, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Length)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);

	ARE_CLOSE(18.708287f, vector.Length(), Math::Accuracy);
}

//===========================================================================//

TEST(Vector3ZeroLength)
{
	Vector3 vector(0.0f, 0.0f, 0.0f);

	ARE_CLOSE(0.0f, vector.Length(), Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Normalize)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);

	vector.Normalize();

	ARE_CLOSE(0.267261f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.534522f, vector.Y, Math::Accuracy);
	ARE_CLOSE(0.801784f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3DistanceTo)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);
	Vector3 otherVector(8.0f, 4.0f, 22.0f);

	ARE_CLOSE(9.695360f, vector.DistanceTo(otherVector), Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Dot)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);
	Vector3 otherVector(8.0f, 4.0f, 22.0f);

	ARE_CLOSE(410.0f, vector.Dot(otherVector), Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Cross)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);
	Vector3 otherVector(8.0f, 4.0f, 22.0f);

	Vector3 resultVector = vector.Cross(otherVector);

	ARE_CLOSE(160.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(10.0f, resultVector.Y, Math::Accuracy);
	ARE_CLOSE(-60.0f, resultVector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3Negative)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);

	vector.Negative();

	ARE_CLOSE(-5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(-15.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3NegativeX)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);

	vector.NegativeX();

	ARE_CLOSE(-5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(15.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3NegativeY)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);

	vector.NegativeY();

	ARE_CLOSE(5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(15.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3NegativeZ)
{
	Vector3 vector(5.0f, 10.0f, 15.0f);

	vector.NegativeZ();

	ARE_CLOSE(5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(10.0f, vector.Y, Math::Accuracy);
	ARE_CLOSE(-15.0f, vector.Z, Math::Accuracy);
}

//===========================================================================//

TEST(Vector3ToString)
{
	Vector3 vector(5.151515f, 10.666f, 15.333666f);

	ARE_EQUAL("5.1515, 10.6660, 15.3337", vector.ToString());
}

//===========================================================================//

}// namespace Bru

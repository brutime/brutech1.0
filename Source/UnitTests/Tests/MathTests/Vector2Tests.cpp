//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса вектора, состоящего из 2-ух компонентов
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(Vector2DefaultConstructor)
{
	Vector2 vector;

	ARE_CLOSE(0.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Constructor)
{
	Vector2 vector(-10.4f, 22.1614f);

	ARE_CLOSE(-10.4f, vector.X, Math::Accuracy);
	ARE_CLOSE(22.1614f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2StringConstructor)
{
	Vector2 vector("-10.4, 22.1614");

	ARE_CLOSE(-10.4f, vector.X, Math::Accuracy);
	ARE_CLOSE(22.1614f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2StringConstructorExceptions)
{
	IS_THROW(Vector2("-10.4"), InvalidArgumentException);
	IS_THROW(Vector2("-10.4, 22.1614, 0.666"), InvalidArgumentException);
}

//===========================================================================//

TEST(Vector2CopyConstructor)
{
	Vector2 sourceVector(66.6f, -0.1415f);
	Vector2 vector(sourceVector);

	sourceVector.Clear();

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2MoveConstructor)
{
	Vector2 vector(std::move(Vector2(66.6f, -0.1415f)));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2CopyOperator)
{
	Vector2 sourceVector(66.6f, -0.1415f);
	Vector2 vector;

	vector = sourceVector;

	sourceVector.Clear();

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2CopyOperatorSelf)
{
	Vector2 vector(66.6f, -0.1415f);

	vector = vector;

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2MoveOperator)
{
	Vector2 vector;

	vector = std::move(Vector2(66.6f, -0.1415f));

	ARE_CLOSE(66.6f, vector.X, Math::Accuracy);
	ARE_CLOSE(-0.1415f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Append)
{
	Vector2 vector(-10.5f, 13.4f);
	Vector2 appendingVector(10.5f, 14.6f);

	vector += appendingVector;

	ARE_CLOSE(0.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(28.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Detach)
{
	Vector2 vector(-10.5f, 13.4f);
	Vector2 detachingVector(10.5f, 14.6f);

	vector -= detachingVector;

	ARE_CLOSE(-21.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-1.2f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Multiply)
{
	Vector2 vector(2.0f, 3.0f);
	Vector2 multiplyingVector(3.0f, 4.0f);

	vector *= multiplyingVector;

	ARE_CLOSE(6.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(12.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2MultiplyFactor)
{
	Vector2 vector(2.0f, 3.0f);

	vector *= 2.0f;

	ARE_CLOSE(4.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(6.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Divide)
{
	Vector2 vector(6.0f, 12.0f);
	Vector2 dividingVector(2.0f, 3.0f);

	vector /= dividingVector;

	ARE_CLOSE(3.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(4.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Plus)
{
	Vector2 vector(-10.5f, 13.4f);
	Vector2 appendingVector(10.5f, 14.6f);

	Vector2 resultVector = vector + appendingVector;

	ARE_CLOSE(0.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(28.0f, resultVector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Minus)
{
	Vector2 vector(-10.5f, 13.4f);
	Vector2 detachingVector(10.5f, 14.6f);

	Vector2 resultVector = vector - detachingVector;

	ARE_CLOSE(-21.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(-1.2f, resultVector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Multiplication)
{
	Vector2 vector(2.0f, 3.0f);
	Vector2 multiplyingVector(3.0f, 4.0f);

	Vector2 resultVector = vector * multiplyingVector;

	ARE_CLOSE(6.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(12.0f, resultVector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2MultiplicationFactor)
{
	Vector2 vector(2.0f, 3.0f);

	Vector2 resultVector = vector * 2.0f;

	ARE_CLOSE(4.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(6.0f, resultVector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Division)
{
	Vector2 vector(6.0f, 12.0f);
	Vector2 dividingVector(2.0f, 3.0f);

	Vector2 resultVector = vector / dividingVector;

	ARE_CLOSE(3.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(4.0f, resultVector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Equals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	Vector2 vector(-10.5f, 13.4f);
	Vector2 equalsVector(-10.5f, 13.4f);
	Vector2 notEqualsVector(10.5f, 14.6f);

	IS_TRUE(vector == equalsVector);
	IS_TRUE(vector != notEqualsVector);
}

//===========================================================================//

TEST(Vector2OperatorNegative)
{
	Vector2 vector(5.0f, 10.0f);

	Vector2 resultVector = -vector;

	ARE_CLOSE(-5.0f, resultVector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, resultVector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Clear)
{
	Vector2 vector(-10.5f, 13.4f);

	vector.Clear();

	ARE_CLOSE(0, vector.X, Math::Accuracy);
	ARE_CLOSE(0, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Length)
{
	Vector2 vector(5.0f, 10.0f);

	ARE_CLOSE(11.180340f, vector.Length(), Math::Accuracy);
}

//===========================================================================//

TEST(Vector2ZeroLength)
{
	Vector2 vector(0.0f, 0.0f);

	ARE_CLOSE(0.0f, vector.Length(), Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Normalize)
{
	Vector2 vector(5.0f, 10.0f);

	vector.Normalize();

	ARE_CLOSE(0.447214f, vector.X, Math::Accuracy);
	ARE_CLOSE(0.894427f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2DistanceTo)
{
	Vector2 vector(5.0f, 10.0f);
	Vector2 otherVector(8.0f, 4.0f);

	ARE_CLOSE(6.708204f, vector.DistanceTo(otherVector), Math::Accuracy);
}

//===========================================================================//

TEST(Vector2Negative)
{
	Vector2 vector(5.0f, 10.0f);

	vector.Negative();

	ARE_CLOSE(-5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2NegativeX)
{
	Vector2 vector(5.0f, 10.0f);

	vector.NegativeX();

	ARE_CLOSE(-5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(10.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2NegativeY)
{
	Vector2 vector(5.0f, 10.0f);

	vector.NegativeY();

	ARE_CLOSE(5.0f, vector.X, Math::Accuracy);
	ARE_CLOSE(-10.0f, vector.Y, Math::Accuracy);
}

//===========================================================================//

TEST(Vector2ToString)
{
	Vector2 vector(5.151515f, 10.666f);

	ARE_EQUAL("5.1515, 10.6660", vector.ToString());
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты математического класса
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(MathDigreesAndRadiansConvertation)
{
	float digreeValue = 0.0f;
	float radianValue = 0.0f;
	for (integer i = 0; i < 360 * 2 + 1; i++)
	{
		ARE_CLOSE(radianValue, Math::DigreesToRadians(digreeValue), Math::Accuracy);
		ARE_CLOSE(digreeValue, Math::Round(Math::RadiansToDigrees(radianValue), 0.01f), Math::Accuracy);
		digreeValue += 0.5f;
		radianValue += 0.0087266462599716f;
	}
}

//===========================================================================//

TEST(MathMin)
{
	ARE_CLOSE(10.0f, Math::Min(20.0f, 10.0f), Math::Accuracy);
	ARE_CLOSE(10.0f, Math::Min(10.0f, 20.0f), Math::Accuracy);
	ARE_CLOSE(-1.0f, Math::Min(-1.0f, 1.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Min(0.0f, 0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathMax)
{
	ARE_CLOSE(20.0f, Math::Max(20.0f, 10.0f), Math::Accuracy);
	ARE_CLOSE(20.0f, Math::Max(10.0f, 20.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Max(-1.0f, 1.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Min(0.0f, 0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathSwap)
{
	float one = 1.0f;
	float two = 2.0f;

	Math::Swap(one, two);

	ARE_CLOSE(2.0f, one, Math::Accuracy);
	ARE_CLOSE(1.0f, two, Math::Accuracy);
}

//===========================================================================//

TEST(MathEven)
{
	const integer EvenTestNumbersCount = 1000;

	for (integer i = -EvenTestNumbersCount; i < EvenTestNumbersCount; i += 2)
	{
		ARE_CLOSE(true, Math::Even(i), Math::Accuracy);
		ARE_CLOSE(false, Math::Even(i + 1), Math::Accuracy);
	}
}

//===========================================================================//

TEST(MathOdd)
{
	const integer OddTestNumbersCount = 1000;

	for (integer i = -OddTestNumbersCount; i < OddTestNumbersCount; i += 2)
	{
		ARE_CLOSE(false, Math::Odd(i), Math::Accuracy);
		ARE_CLOSE(true, Math::Odd(i + 1), Math::Accuracy);
	}
}

//===========================================================================//

TEST(MathAbs)
{
	ARE_CLOSE(3.1415f, Math::Abs(3.1415f), Math::Accuracy);
	ARE_CLOSE(3.1415f, Math::Abs(-3.1415f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Abs(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathSign)
{
	ARE_CLOSE(1.0f, Math::Sign(3.1415f), Math::Accuracy);
	ARE_CLOSE(-1.0f, Math::Sign(-3.1415f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Sign(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathCeiling)
{
	ARE_CLOSE(4.0f, Math::Ceiling(3.1415f), Math::Accuracy);
	ARE_CLOSE(4.0f, Math::Ceiling(3.5f), Math::Accuracy);
	ARE_CLOSE(-3.0f, Math::Ceiling(-3.1415f), Math::Accuracy);
	ARE_CLOSE(-3.0f, Math::Ceiling(-3.5f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Ceiling(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathCeilingWithStep)
{
	ARE_CLOSE(3.15f, Math::Ceiling(3.1415f, 0.01f), Math::Accuracy);
	ARE_CLOSE(3.5f, Math::Ceiling(3.5f, 0.1f), Math::Accuracy);
	ARE_CLOSE(-3.14f, Math::Ceiling(-3.1415f, 0.01f), Math::Accuracy);
	ARE_CLOSE(-3.5f, Math::Ceiling(-3.5f, 0.1f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Ceiling(0.0f, 0.01f), Math::Accuracy);
}

//===========================================================================//

TEST(MathFloor)
{
	ARE_CLOSE(3.0f, Math::Floor(3.1415f), Math::Accuracy);
	ARE_CLOSE(3.0f, Math::Floor(3.5f), Math::Accuracy);
	ARE_CLOSE(-4.0f, Math::Floor(-3.1415f), Math::Accuracy);
	ARE_CLOSE(-4.0f, Math::Floor(-3.5f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Floor(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathFloorWithStep)
{
	ARE_CLOSE(3.14f, Math::Floor(3.1415f, 0.01f), Math::Accuracy);
	ARE_CLOSE(3.5f, Math::Floor(3.5f, 0.1f), Math::Accuracy);
	ARE_CLOSE(-3.15f, Math::Floor(-3.1415f, 0.01f), Math::Accuracy);
	ARE_CLOSE(-3.5f, Math::Floor(-3.5f, 0.1f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Floor(0.0f, 0.01f), Math::Accuracy);
}

//===========================================================================//

TEST(MathRound)
{
	ARE_CLOSE(3.0f, Math::Round(3.1415f), Math::Accuracy);
	ARE_CLOSE(4.0f, Math::Round(3.5f), Math::Accuracy);
	ARE_CLOSE(-3.0f, Math::Round(-3.1415f), Math::Accuracy);
	ARE_CLOSE(-4.0f, Math::Round(-3.5f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Round(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathRoundWithStep)
{
	ARE_CLOSE(3.14f, Math::Round(3.1415f, 0.01f), Math::Accuracy);
	ARE_CLOSE(3.5f, Math::Round(3.5f, 0.1f), Math::Accuracy);
	ARE_CLOSE(-3.14f, Math::Round(-3.1415f, 0.01f), Math::Accuracy);
	ARE_CLOSE(-3.5f, Math::Round(-3.5f, 0.1f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Round(0.0f, 0.01f), Math::Accuracy);
}

//===========================================================================//

TEST(MathRoundWithPrecision)
{
	ARE_CLOSE(3.9f, Math::RoundWithPrecision(3.85f, 1), Math::Accuracy);
	ARE_CLOSE(3.8f, Math::RoundWithPrecision(3.84f, 1), Math::Accuracy);
	ARE_CLOSE(3.16f, Math::RoundWithPrecision(3.156f, 2), Math::Accuracy);
	ARE_CLOSE(3.15f, Math::RoundWithPrecision(3.154f, 2), Math::Accuracy);
	ARE_CLOSE(-3.9f, Math::RoundWithPrecision(-3.85f, 1), Math::Accuracy);
	ARE_CLOSE(-3.8f, Math::RoundWithPrecision(-3.84f, 1), Math::Accuracy);
	ARE_CLOSE(-3.16f, Math::RoundWithPrecision(-3.156f, 2), Math::Accuracy);
	ARE_CLOSE(-3.15f, Math::RoundWithPrecision(-3.154f, 2), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::RoundWithPrecision(0.0f, 1), Math::Accuracy);
}

//===========================================================================//

TEST(MathSqr)
{
	ARE_CLOSE(9.869022f, Math::Sqr(3.1415f), Math::Accuracy);
	ARE_CLOSE(12.25f, Math::Sqr(3.5f), Math::Accuracy);
	ARE_CLOSE(9.869022f, Math::Sqr(-3.1415f), Math::Accuracy);
	ARE_CLOSE(12.25f, Math::Sqr(-3.5f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Sqr(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathPow)
{
	ARE_CLOSE(31.003533f, Math::Pow(3.1415f, 3), Math::Accuracy);
	ARE_CLOSE(150.0625f, Math::Pow(3.5f, 4), Math::Accuracy);
	ARE_CLOSE(-31.003533f, Math::Pow(-3.1415f, 3), Math::Accuracy);
	ARE_CLOSE(150.0625f, Math::Pow(-3.5f, 4), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Pow(0.0f, 10), Math::Accuracy);
}

//===========================================================================//

TEST(MathSqrt)
{
	ARE_CLOSE(1.772428f, Math::Sqrt(3.1415f), Math::Accuracy);
	ARE_CLOSE(1.870829f, Math::Sqrt(3.5f), Math::Accuracy);
	IS_THROW(Math::Sqrt(-3.1415f), InvalidArgumentException);
	ARE_CLOSE(0.0f, Math::Sqrt(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathExp)
{
	ARE_CLOSE(23.138548f, Math::Exp(3.1415f), Math::Accuracy);
	ARE_CLOSE(33.115452f, Math::Exp(3.5f), Math::Accuracy);
	ARE_CLOSE(0.043218f, Math::Exp(-3.1415f), Math::Accuracy);
	ARE_CLOSE(0.030197f, Math::Exp(-3.5f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Exp(0.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathLog)
{
	ARE_CLOSE(1.144700f, Math::Log(3.1415f), Math::Accuracy);
	ARE_CLOSE(1.252763f, Math::Log(3.5f), Math::Accuracy);
	IS_THROW(Math::Log(-3.1415f), InvalidArgumentException);
	IS_THROW(Math::Log(0.0f), InvalidArgumentException);
}

//===========================================================================//

TEST(MathLog10)
{
	ARE_CLOSE(0.497137f, Math::Log10(3.1415f), Math::Accuracy);
	ARE_CLOSE(0.544068f, Math::Log10(3.5f), Math::Accuracy);
	IS_THROW(Math::Log10(-3.1415f), InvalidArgumentException);
	IS_THROW(Math::Log10(0.0f), InvalidArgumentException);
}

//===========================================================================//

TEST(MathMod)
{
	ARE_CLOSE(0.423219f, Math::Mod(3.1415f, 2.718281f), Math::Accuracy);
	ARE_CLOSE(-0.423219f, Math::Mod(-3.1415f, -2.718281f), Math::Accuracy);
	ARE_CLOSE(-0.423219f, Math::Mod(-3.1415f, 2.718281f), Math::Accuracy);
	ARE_CLOSE(0.423219f, Math::Mod(3.1415f, -2.718281f), Math::Accuracy);
	ARE_CLOSE(1.3f, Math::Mod(3.5f, 2.2f), Math::Accuracy);
	ARE_CLOSE(-1.3f, Math::Mod(-3.5f, -2.2f), Math::Accuracy);
	ARE_CLOSE(-1.3f, Math::Mod(-3.5f, 2.2f), Math::Accuracy);
	ARE_CLOSE(1.3f, Math::Mod(3.5f, -2.2f), Math::Accuracy);
	IS_THROW(Math::Mod(3.1415f, 0.0f), DivisionByZeroException);
	ARE_CLOSE(0.0f, Math::Mod(0.0f, 2.718281f), Math::Accuracy);
}

//===========================================================================//

TEST(MathSin)
{
	ARE_CLOSE(-0.0f, Math::Sin(-360.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Sin(-315.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Sin(-270.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Sin(-225.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Sin(-180.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Sin(-135.0f), Math::Accuracy);
	ARE_CLOSE(-1.0f, Math::Sin(-90.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Sin(-45.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Sin(0.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Sin(45.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Sin(90.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Sin(135.0f), Math::Accuracy);
	ARE_CLOSE(-0.0f, Math::Sin(180.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Sin(225.0f), Math::Accuracy);
	ARE_CLOSE(-1.0f, Math::Sin(270.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Sin(315.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Sin(360.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathCos)
{
	ARE_CLOSE(1.0f, Math::Cos(-360.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Cos(-315.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Cos(-270.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Cos(-225.0f), Math::Accuracy);
	ARE_CLOSE(-1.0f, Math::Cos(-180.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Cos(-135.0f), Math::Accuracy);
	ARE_CLOSE(-0.0f, Math::Cos(-90.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Cos(-45.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Cos(0.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Cos(45.0f), Math::Accuracy);
	ARE_CLOSE(-0.0f, Math::Cos(90.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Cos(135.0f), Math::Accuracy);
	ARE_CLOSE(-1.0f, Math::Cos(180.0f), Math::Accuracy);
	ARE_CLOSE(-0.707107f, Math::Cos(225.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Cos(270.0f), Math::Accuracy);
	ARE_CLOSE(0.707107f, Math::Cos(315.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Cos(360.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathTan)
{
	ARE_CLOSE(-0.0f, Math::Tan(-360.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Tan(-315.0f), Math::Accuracy);
	IS_THROW(Math::Tan(-270.0f), InvalidArgumentException);
	ARE_CLOSE(-1.0f, Math::Tan(-225.0f), Math::Accuracy);
	ARE_CLOSE(-0.0f, Math::Tan(-180.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Tan(-135.0f), Math::Accuracy);
	IS_THROW(Math::Tan(-90.0f), InvalidArgumentException);
	ARE_CLOSE(-1.0f, Math::Tan(-45.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Tan(0.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Tan(45.0f), Math::Accuracy);
	IS_THROW(Math::Tan(90.0f), InvalidArgumentException);
	ARE_CLOSE(-1.0f, Math::Tan(135.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Tan(180.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::Tan(225.0f), Math::Accuracy);
	IS_THROW(Math::Tan(270.0f), InvalidArgumentException);
	ARE_CLOSE(-1.0f, Math::Tan(315.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::Tan(360.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathHyperSin)
{
	ARE_CLOSE(-267.744934f, Math::HyperSin(-360.0f), Math::Accuracy);
	ARE_CLOSE(-122.073463f, Math::HyperSin(-315.0f), Math::Accuracy);
	ARE_CLOSE(-55.654400f, Math::HyperSin(-270.0f), Math::Accuracy);
	ARE_CLOSE(-25.367157f, Math::HyperSin(-225.0f), Math::Accuracy);
	ARE_CLOSE(-11.548740f, Math::HyperSin(-180.0f), Math::Accuracy);
	ARE_CLOSE(-5.227972f, Math::HyperSin(-135.0f), Math::Accuracy);
	ARE_CLOSE(-2.301299f, Math::HyperSin(-90.0f), Math::Accuracy);
	ARE_CLOSE(-0.868671f, Math::HyperSin(-45.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::HyperSin(0.0f), Math::Accuracy);
	ARE_CLOSE(0.868671f, Math::HyperSin(45.0f), Math::Accuracy);
	ARE_CLOSE(2.301299f, Math::HyperSin(90.0f), Math::Accuracy);
	ARE_CLOSE(5.227972f, Math::HyperSin(135.0f), Math::Accuracy);
	ARE_CLOSE(11.548740f, Math::HyperSin(180.0f), Math::Accuracy);
	ARE_CLOSE(25.367157f, Math::HyperSin(225.0f), Math::Accuracy);
	ARE_CLOSE(55.654400f, Math::HyperSin(270.0f), Math::Accuracy);
	ARE_CLOSE(122.073463f, Math::HyperSin(315.0f), Math::Accuracy);
	ARE_CLOSE(267.744934f, Math::HyperSin(360.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathHyperCos)
{
	ARE_CLOSE(267.746796f, Math::HyperCos(-360.0f), Math::Accuracy);
	ARE_CLOSE(122.077560f, Math::HyperCos(-315.0f), Math::Accuracy);
	ARE_CLOSE(55.663380f, Math::HyperCos(-270.0f), Math::Accuracy);
	ARE_CLOSE(25.386860f, Math::HyperCos(-225.0f), Math::Accuracy);
	ARE_CLOSE(11.591954f, Math::HyperCos(-180.0f), Math::Accuracy);
	ARE_CLOSE(5.322752f, Math::HyperCos(-135.0f), Math::Accuracy);
	ARE_CLOSE(2.509179f, Math::HyperCos(-90.0f), Math::Accuracy);
	ARE_CLOSE(1.324609f, Math::HyperCos(-45.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::HyperCos(0.0f), Math::Accuracy);
	ARE_CLOSE(1.324609f, Math::HyperCos(45.0f), Math::Accuracy);
	ARE_CLOSE(2.509179f, Math::HyperCos(90.0f), Math::Accuracy);
	ARE_CLOSE(5.322752f, Math::HyperCos(135.0f), Math::Accuracy);
	ARE_CLOSE(11.591954f, Math::HyperCos(180.0f), Math::Accuracy);
	ARE_CLOSE(25.386860f, Math::HyperCos(225.0f), Math::Accuracy);
	ARE_CLOSE(55.663380f, Math::HyperCos(270.0f), Math::Accuracy);
	ARE_CLOSE(122.077560f, Math::HyperCos(315.0f), Math::Accuracy);
	ARE_CLOSE(267.746796f, Math::HyperCos(360.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathHyperTan)
{
	ARE_CLOSE(-0.999993f, Math::HyperTan(-360.0f), Math::Accuracy);
	ARE_CLOSE(-0.999966f, Math::HyperTan(-315.0f), Math::Accuracy);
	ARE_CLOSE(-0.999839f, Math::HyperTan(-270.0f), Math::Accuracy);
	ARE_CLOSE(-0.999224f, Math::HyperTan(-225.0f), Math::Accuracy);
	ARE_CLOSE(-0.996272f, Math::HyperTan(-180.0f), Math::Accuracy);
	ARE_CLOSE(-0.982193f, Math::HyperTan(-135.0f), Math::Accuracy);
	ARE_CLOSE(-0.917152f, Math::HyperTan(-90.0f), Math::Accuracy);
	ARE_CLOSE(-0.655794f, Math::HyperTan(-45.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::HyperTan(0.0f), Math::Accuracy);
	ARE_CLOSE(0.655794f, Math::HyperTan(45.0f), Math::Accuracy);
	ARE_CLOSE(0.917152f, Math::HyperTan(90.0f), Math::Accuracy);
	ARE_CLOSE(0.982193f, Math::HyperTan(135.0f), Math::Accuracy);
	ARE_CLOSE(0.996272f, Math::HyperTan(180.0f), Math::Accuracy);
	ARE_CLOSE(0.999224f, Math::HyperTan(225.0f), Math::Accuracy);
	ARE_CLOSE(0.999839f, Math::HyperTan(270.0f), Math::Accuracy);
	ARE_CLOSE(0.999966f, Math::HyperTan(315.0f), Math::Accuracy);
	ARE_CLOSE(0.999993f, Math::HyperTan(360.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathArcSin)
{
	ARE_CLOSE(-90.0f, Math::ArcSin(-1), Math::Accuracy);
	ARE_CLOSE(-81.890386f, Math::ArcSin(-0.99f), Math::Accuracy);
	ARE_CLOSE(-30.0f, Math::ArcSin(-0.5), Math::Accuracy);
	ARE_CLOSE(-0.572967f, Math::ArcSin(-0.01f), Math::Accuracy);
	ARE_CLOSE(0, Math::ArcSin(0), Math::Accuracy);
	ARE_CLOSE(0.572967f, Math::ArcSin(0.01f), Math::Accuracy);
	ARE_CLOSE(30.0f, Math::ArcSin(0.5), Math::Accuracy);
	ARE_CLOSE(81.890386f, Math::ArcSin(0.99f), Math::Accuracy);
	ARE_CLOSE(90.0f, Math::ArcSin(1), Math::Accuracy);

	IS_THROW(Math::ArcSin(-1.1f), InvalidArgumentException);
	IS_THROW(Math::ArcSin(1.1f), InvalidArgumentException);
}

//===========================================================================//

TEST(MathArcCos)
{
	ARE_CLOSE(180.0f, Math::ArcCos(-1), Math::Accuracy);
	ARE_CLOSE(171.890386f, Math::ArcCos(-0.99f), Math::Accuracy);
	ARE_CLOSE(120.0f, Math::ArcCos(-0.5), Math::Accuracy);
	ARE_CLOSE(90.572967f, Math::ArcCos(-0.01f), Math::Accuracy);
	ARE_CLOSE(90.0f, Math::ArcCos(0), Math::Accuracy);
	ARE_CLOSE(89.427033f, Math::ArcCos(0.01f), Math::Accuracy);
	ARE_CLOSE(60.0f, Math::ArcCos(0.5), Math::Accuracy);
	ARE_CLOSE(8.109614f, Math::ArcCos(0.99f), Math::Accuracy);
	ARE_CLOSE(0, Math::ArcCos(1), Math::Accuracy);

	IS_THROW(Math::ArcCos(-1.1f), InvalidArgumentException);
	IS_THROW(Math::ArcCos(1.1f), InvalidArgumentException);
}

//===========================================================================//

TEST(MathArcTan)
{
	ARE_CLOSE(-45.0f, Math::ArcTan(-1), Math::Accuracy);
	ARE_CLOSE(-44.712084f, Math::ArcTan(-0.99f), Math::Accuracy);
	ARE_CLOSE(-26.565051f, Math::ArcTan(-0.5), Math::Accuracy);
	ARE_CLOSE(-0.572939f, Math::ArcTan(-0.01f), Math::Accuracy);
	ARE_CLOSE(0, Math::ArcTan(0), Math::Accuracy);
	ARE_CLOSE(0.572939f, Math::ArcTan(0.01f), Math::Accuracy);
	ARE_CLOSE(26.565051f, Math::ArcTan(0.5), Math::Accuracy);
	ARE_CLOSE(44.712084f, Math::ArcTan(0.99f), Math::Accuracy);
	ARE_CLOSE(45, Math::ArcTan(1), Math::Accuracy);
}

//===========================================================================//

TEST(MathArcTan2)
{
	ARE_CLOSE(-159.774277f, Math::ArcTan(-11.23f, -30.48f), Math::Accuracy);
	ARE_CLOSE(159.774277f, Math::ArcTan(11.23f, -30.48f), Math::Accuracy);
	ARE_CLOSE(-20.225731f, Math::ArcTan(-11.23f, 30.48f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::ArcTan(0.0f, 0.0f), Math::Accuracy);
	ARE_CLOSE(20.225731f, Math::ArcTan(11.23f, 30.48f), Math::Accuracy);
}

//===========================================================================//

TEST(MathRandomInRange)
{
	float randomFrom = 100.0f;
	float randomTo = 1500.0f;
	const integer GenerateRandomCount = 100;
	for (integer i = 0; i < GenerateRandomCount; i++)
	{
		float randomValue = Math::RandomInRange(randomFrom, randomTo);
		IS_TRUE(randomValue >= randomFrom);
		IS_TRUE(randomValue <= randomTo);
	}
}

//===========================================================================//

TEST(MathCompareEquals)
{
	float firstValue = 13;
	float secondValue = 13.0f + Math::Accuracy / 2.0f;

	IS_TRUE(Math::Equals(firstValue, secondValue));
	IS_FALSE(Math::Equals(firstValue, secondValue + Math::Accuracy));
}

//===========================================================================//

TEST(MathCompareEqualsAccuracy)
{
	const float Accuracy = Math::Accuracy * 2.0f;

	float firstValue = 13;
	float secondValue = 13.0f + Accuracy / 2.0f;

	IS_TRUE(Math::Equals(firstValue, secondValue, Accuracy));
	IS_FALSE(Math::Equals(firstValue, secondValue + Accuracy, Accuracy));
}

//===========================================================================//

TEST(MathCompareEqualsZero)
{
	float value = Math::Accuracy / 2.0f;

	IS_TRUE(Math::EqualsZero(value));
	IS_FALSE(Math::EqualsZero(value + Math::Accuracy));
}

//===========================================================================//

TEST(MathCompareEqualsZeroAccuracy)
{
	const float Accuracy = Math::Accuracy * 2.0f;

	float value = Accuracy / 2.0f;

	IS_TRUE(Math::EqualsZero(value, Accuracy));
	IS_FALSE(Math::EqualsZero(value + Accuracy, Accuracy));
}

//===========================================================================//

TEST(MathIsPowerOfTwo)
{
	//Тест был сокращен до 20 степеней
	const integer PowerOfTwoTestNumbersCount = 20;
	List<integer> testPowersOfTwo;
	integer currentPowerOfTwo = 0;
	for (integer i = 0; i < PowerOfTwoTestNumbersCount; i++)
	{
		testPowersOfTwo.Add(currentPowerOfTwo);
		currentPowerOfTwo = (currentPowerOfTwo > 0) ? currentPowerOfTwo * 2 : 1;
	}

	List<integer> powersOfTwo;
	const integer MaxPowerOfTwo = 524288;
	for (integer i = 0; i < MaxPowerOfTwo; i++)
	{
		if (Math::IsPowerOfTwo(i))
		{
			powersOfTwo.Add(i);
		}
	}

	ARE_EQUAL(PowerOfTwoTestNumbersCount, powersOfTwo.GetCount());
	ARE_ARRAYS_EQUAL(testPowersOfTwo, powersOfTwo, PowerOfTwoTestNumbersCount);
}

//===========================================================================//

TEST(MathNearestPowerOfTwo)
{
	ARE_EQUAL(1, Math::GetNearestPowerOfTwo(0));
	ARE_EQUAL(128, Math::GetNearestPowerOfTwo(100));
	ARE_EQUAL(1048576, Math::GetNearestPowerOfTwo(1048576));
}

//===========================================================================//

TEST(MathIsDivisibleBy)
{
	IS_TRUE(Math::IsDivisibleBy(1, 1));
	IS_TRUE(Math::IsDivisibleBy(0, 1));
	IS_TRUE(Math::IsDivisibleBy(0, 3));
	IS_THROW(Math::IsDivisibleBy(3, 0), DivisionByZeroException);

	IS_TRUE(Math::IsDivisibleBy(33, 3));
	IS_FALSE(Math::IsDivisibleBy(33, 2));
}

//===========================================================================//

TEST(MathToPercentInteger)
{
	ARE_EQUAL(22, Math::ToPercent(22, 100));
	ARE_EQUAL(22, Math::ToPercent(22, 101));
	ARE_EQUAL(0, Math::ToPercent(0, 100));
}

//===========================================================================//

TEST(MathToPercentFloat)
{
	ARE_CLOSE(22.0f, Math::ToPercent(22.0f, 100.0f), Math::Accuracy);
	ARE_CLOSE(21.78218f, Math::ToPercent(22.0f, 101.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::ToPercent(0.0f, 100.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathFromPercentInteger)
{
	ARE_EQUAL(22, Math::FromPercent(22, 100));
	ARE_EQUAL(22, Math::FromPercent(22, 101));
	ARE_EQUAL(0, Math::FromPercent(0, 100));
}

//===========================================================================//

TEST(MathFromPercentFloat)
{
	ARE_CLOSE(22.0f, Math::FromPercent(22.0f, 100.0f), Math::Accuracy);
	ARE_CLOSE(22.22f, Math::FromPercent(22.0f, 101.0f), Math::Accuracy);
	ARE_CLOSE(0.0f, Math::FromPercent(0.0f, 100.0f), Math::Accuracy);
}

//===========================================================================//

TEST(MathFitInRange)
{
	ARE_CLOSE(0.0f, Math::FitInRange(-1.0f, 0.0f, 1.0f), Math::Accuracy);
	ARE_CLOSE(1.0f, Math::FitInRange(22.0f, 0.0f, 1.0f), Math::Accuracy);
	ARE_CLOSE(0.5f, Math::FitInRange(0.5f, 0.0f, 1.0f), Math::Accuracy);
}

//===========================================================================//

} // namespace Bru

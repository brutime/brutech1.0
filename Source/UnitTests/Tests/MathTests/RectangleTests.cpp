//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты прямоугольника
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(RectangleSubtractionTest)
{
	Rectangle a(2, 2, 5, 5);

	//	XXX
	//	XXX
	// 	XXX
	IS_TRUE(a.SubtractRectangle(Rectangle(0, 0, 10, 10)).GetCount() == 0);

	//	+++
	//	+X+
	// 	+++	
	Array<Rectangle> result = a.SubtractRectangle(Rectangle(3, 3, 1, 1));
	IS_TRUE(result.GetCount() == 4);
	IS_TRUE(result.Contains(Rectangle(2, 2, 1, 5)));
	IS_TRUE(result.Contains(Rectangle(3, 2, 1, 1)));
	IS_TRUE(result.Contains(Rectangle(3, 4, 1, 3)));
	IS_TRUE(result.Contains(Rectangle(4, 2, 3, 5)));

	//	++X
	//	+++
	// 	+++	
	result = a.SubtractRectangle(Rectangle(6, 6, 2, 2));
	IS_TRUE(result.GetCount() == 2);
	IS_TRUE(result.Contains(Rectangle(2, 2, 4, 5)));
	IS_TRUE(result.Contains(Rectangle(6, 2, 1, 4)));
	
	//	+++
	//	++X
	// 	+++
	result = a.SubtractRectangle(Rectangle(6, 3, 2, 1));
	IS_TRUE(result.GetCount() == 3);
	IS_TRUE(result[0] == Rectangle(2, 2, 4, 5));
	IS_TRUE(result[1] == Rectangle(6, 2, 1, 1));
	IS_TRUE(result[2] == Rectangle(6, 4, 1, 3));
	
	//	+++
	//	+++
	// 	++X
	result = a.SubtractRectangle(Rectangle(6, 1, 2, 2));
	IS_TRUE(result.GetCount() == 2);
	IS_TRUE(result.Contains(Rectangle(2, 2, 4, 5)));
	IS_TRUE(result.Contains(Rectangle(6, 3, 1, 4)));

	//	+++
	//	+++
	// 	+X+
	result = a.SubtractRectangle(Rectangle(4, 2, 1, 1));
	IS_TRUE(result.GetCount() == 3);
	IS_TRUE(result.Contains(Rectangle(2, 2, 2, 5)));
	IS_TRUE(result.Contains(Rectangle(4, 3, 1, 4)));
	IS_TRUE(result.Contains(Rectangle(5, 2, 2, 5)));
		
	//	+++
	//	+++
	// 	X++	
	result = a.SubtractRectangle(Rectangle(2, 2, 1, 1));
	IS_TRUE(result.GetCount() == 2);
	IS_TRUE(result.Contains(Rectangle(2, 3, 1, 4)));
	IS_TRUE(result.Contains(Rectangle(3, 2, 4, 5)));
	
	//	+++
	//	X++
	// 	+++	
	result = a.SubtractRectangle(Rectangle(1, 4, 2, 1));
	IS_TRUE(result.GetCount() == 3);
	IS_TRUE(result.Contains(Rectangle(2, 2, 1, 2)));
	IS_TRUE(result.Contains(Rectangle(2, 5, 1, 2)));
	IS_TRUE(result.Contains(Rectangle(3, 2, 4, 5)));
	
	//	X++
	//	+++
	// 	+++	
	result = a.SubtractRectangle(Rectangle(2, 6, 1, 1));
	IS_TRUE(result.GetCount() == 2);
	IS_TRUE(result.Contains(Rectangle(2, 2, 1, 4)));
	IS_TRUE(result.Contains(Rectangle(3, 2, 4, 5)));
	
	//	+X+
	//	+++
	// 	+++	
	result = a.SubtractRectangle(Rectangle(4, 6, 1, 2));
	IS_TRUE(result.GetCount() == 3);
	IS_TRUE(result.Contains(Rectangle(2, 2, 2, 5)));
	IS_TRUE(result.Contains(Rectangle(4, 2, 1, 4)));
	IS_TRUE(result.Contains(Rectangle(5, 2, 2, 5)));
	
	//	+++
	//	XXX
	// 	+++
	result = a.SubtractRectangle(Rectangle(1, 4, 10, 2));
	IS_TRUE(result.GetCount() == 2);
	IS_TRUE(result.Contains(Rectangle(2, 2, 5, 2)));
	IS_TRUE(result.Contains(Rectangle(2, 6, 5, 1)));
	
	//	+X+
	//	+X+
	// 	+X+	
	result = a.SubtractRectangle(Rectangle(4, 1, 2, 10));
	IS_TRUE(result.GetCount() == 2);
	IS_TRUE(result.Contains(Rectangle(2, 2, 2, 5)));
	IS_TRUE(result.Contains(Rectangle(6, 2, 1, 5)));
	
	IS_TRUE(a.SubtractRectangle(Rectangle(0, 0, 1, 1))[0] == a);
}

//===========================================================================//

}// namespace Bru

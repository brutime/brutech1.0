//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты интервала (float)
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

TEST(IntIntervalDefaultConstructor)
{
	IntInterval interval;

	ARE_EQUAL(0, interval.From);
	ARE_EQUAL(0, interval.To);
}

//===========================================================================//

TEST(IntIntervalConstructor)
{
	IntInterval interval(-10, 22);

	ARE_EQUAL(-10, interval.From);
	ARE_EQUAL(22, interval.To);
}

//===========================================================================//

TEST(IntIntervalStringConstructor)
{
	IntInterval interval("-10:22");

	ARE_EQUAL(-10, interval.From);
	ARE_EQUAL(22, interval.To);
}

//===========================================================================//

TEST(IntIntervalStringConstructorExceptions)
{
	IS_THROW(IntInterval("-10"), InvalidArgumentException);
	IS_THROW(IntInterval("-10:22:1"), InvalidArgumentException);
}

//===========================================================================//

TEST(IntIntervalCopyConstructor)
{
	IntInterval sourceIntInterval(66, -1);
	IntInterval interval(sourceIntInterval);

	sourceIntInterval.Clear();

	ARE_EQUAL(66, interval.From);
	ARE_EQUAL(-1, interval.To);
}

//===========================================================================//

TEST(IntIntervalCopyOperator)
{
	IntInterval sourceIntInterval(66, -1);
	IntInterval interval;

	interval = sourceIntInterval;

	sourceIntInterval.Clear();

	ARE_EQUAL(66, interval.From);
	ARE_EQUAL(-1, interval.To);
}

//===========================================================================//

TEST(IntIntervalCopyOperatorSelf)
{
	IntInterval interval(66, -1);

	interval = interval;

	ARE_EQUAL(66, interval.From);
	ARE_EQUAL(-1, interval.To);
}

//===========================================================================//

TEST(IntIntervalAppend)
{
	IntInterval interval(-10, 13);
	IntInterval appendingInterval(10, 14);

	interval += appendingInterval;

	ARE_EQUAL(0, interval.From);
	ARE_EQUAL(27, interval.To);
}

//===========================================================================//

TEST(IntIntervalDetach)
{
	IntInterval interval(-10, 13);
	IntInterval detachingInterval(10, 14);

	interval -= detachingInterval;

	ARE_EQUAL(-20, interval.From);
	ARE_EQUAL(-1, interval.To);
}

//===========================================================================//

TEST(IntIntervalPlus)
{
	IntInterval interval(-10, 13);
	IntInterval appendingInterval(10, 14);

	IntInterval resultIntInterval = interval + appendingInterval;

	ARE_EQUAL(0, resultIntInterval.From);
	ARE_EQUAL(27, resultIntInterval.To);
}

//===========================================================================//

TEST(IntIntervalMinus)
{
	IntInterval interval(-10, 13);
	IntInterval detachingInterval(10, 14);

	IntInterval resultIntInterval = interval - detachingInterval;

	ARE_EQUAL(-20, resultIntInterval.From);
	ARE_EQUAL(-1, resultIntInterval.To);
}

//===========================================================================//

TEST(IntIntervalEquals)
{
	//Тест нужно улучшить - чтобы часть переменных была равна, часть нет
	//перебрать все возможные варианты
	IntInterval interval(-10, 13);
	IntInterval equalsInterval(-10, 13);
	IntInterval notEqualsInterval(10, 14);

	IS_TRUE(interval == equalsInterval);
	IS_TRUE(interval != notEqualsInterval);
}

//===========================================================================//

TEST(IntIntervalLess)
{
	IntInterval interval(5, 10);
	IntInterval lessInterval(3, 8);

	IS_TRUE(lessInterval < interval);
	IS_FALSE((lessInterval > interval));
	IS_FALSE((lessInterval == interval));
}

//===========================================================================//

TEST(IntIntervalGreater)
{
	IntInterval interval(5, 10);
	IntInterval greaterInterval(7, 12);

	IS_TRUE(greaterInterval > interval);
	IS_FALSE((greaterInterval < interval));
	IS_FALSE((greaterInterval == interval));
}

//===========================================================================//

TEST(IntIntervalGetIntersection)
{
	IntInterval innerInterval = IntInterval(5, 10).GetIntersection(IntInterval(7, 8));
	ARE_CLOSE(7, innerInterval.From, Math::Accuracy);
	ARE_CLOSE(8, innerInterval.To, Math::Accuracy);

	IntInterval leftInterval = IntInterval(5, 10).GetIntersection(IntInterval(4, 8));
	ARE_CLOSE(5, leftInterval.From, Math::Accuracy);
	ARE_CLOSE(8, leftInterval.To, Math::Accuracy);

	IntInterval rightInterval = IntInterval(5, 10).GetIntersection(IntInterval(7, 12));
	ARE_CLOSE(7, rightInterval.From, Math::Accuracy);
	ARE_CLOSE(10, rightInterval.To, Math::Accuracy);

	IntInterval equalInterval = IntInterval(5, 10).GetIntersection(IntInterval(5, 10));
	ARE_CLOSE(5, equalInterval.From, Math::Accuracy);
	ARE_CLOSE(10, equalInterval.To, Math::Accuracy);

	IntInterval coveringInterval = IntInterval(7, 8).GetIntersection(IntInterval(5, 10));
	ARE_CLOSE(7, coveringInterval.From, Math::Accuracy);
	ARE_CLOSE(8, coveringInterval.To, Math::Accuracy);

	IntInterval outherInterval = IntInterval(5, 10).GetIntersection(IntInterval(11, 16));
	ARE_CLOSE(11, outherInterval.From, Math::Accuracy);
	ARE_CLOSE(10, outherInterval.To, Math::Accuracy);
}

//===========================================================================//

TEST(IntIntervalCut)
{
	Array<IntInterval> innerIntervalCutResult = IntInterval(5, 10).Cut(IntInterval(7, 8));
	ARE_EQUAL(2, innerIntervalCutResult.GetCount());
	ARE_CLOSE(5, innerIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(7, innerIntervalCutResult[0].To, Math::Accuracy);
	ARE_CLOSE(8, innerIntervalCutResult[1].From, Math::Accuracy);
	ARE_CLOSE(10, innerIntervalCutResult[1].To, Math::Accuracy);	

	Array<IntInterval> leftIntervalCutResult = IntInterval(5, 10).Cut(IntInterval(4, 8));
	ARE_EQUAL(1, leftIntervalCutResult.GetCount());
	ARE_CLOSE(8, leftIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(10, leftIntervalCutResult[0].To, Math::Accuracy);	

	Array<IntInterval> leftEqualIntervalCutResult = IntInterval(5, 10).Cut(IntInterval(5, 8));
	ARE_EQUAL(1, leftEqualIntervalCutResult.GetCount());
	ARE_CLOSE(8, leftEqualIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(10, leftEqualIntervalCutResult[0].To, Math::Accuracy);
	
	Array<IntInterval> rightIntervalCutResult = IntInterval(5, 10).Cut(IntInterval(7, 12));
	ARE_EQUAL(1, rightIntervalCutResult.GetCount());
	ARE_CLOSE(5, rightIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(7, rightIntervalCutResult[0].To, Math::Accuracy);
	
	Array<IntInterval> rightEqualIntervalCutResult = IntInterval(5, 10).Cut(IntInterval(7, 10));
	ARE_EQUAL(1, rightEqualIntervalCutResult.GetCount());
	ARE_CLOSE(5, rightEqualIntervalCutResult[0].From, Math::Accuracy);
	ARE_CLOSE(7, rightEqualIntervalCutResult[0].To, Math::Accuracy);
	
	Array<IntInterval> equalIntervalCutResult = IntInterval(5, 10).Cut(IntInterval(5, 10));
	ARE_EQUAL(0, equalIntervalCutResult.GetCount());	
	
	Array<IntInterval> coveringIntervalCutResult = IntInterval(7, 8).Cut(IntInterval(5, 10));
	ARE_EQUAL(0, coveringIntervalCutResult.GetCount());		
	
	Array<IntInterval> outherIntervalCutResult = IntInterval(5, 10).Cut(IntInterval(11, 16));
	ARE_EQUAL(0, outherIntervalCutResult.GetCount());
}

//===========================================================================//

TEST(IntIntervalContains)
{
	IntInterval interval(-10, 13);

	IS_FALSE(interval.Contains(-11));
	IS_TRUE(interval.Contains(-10));
	IS_TRUE(interval.Contains(0));
	IS_TRUE(interval.Contains(13));
	IS_FALSE(interval.Contains(14));
}

//===========================================================================//

TEST(IntIntervalGetLength)
{
	IntInterval interval(-10, 13);

	ARE_EQUAL(23, interval.GetLength());
}

//===========================================================================//

TEST(IntIntervalGetRandom)
{
	IntInterval interval(100, 1500);

	const integer GenerateRandomCount = 100;
	for (integer i = 0; i < GenerateRandomCount; i++)
	{
		float randomValue = interval.GetRandom();
		IS_TRUE(randomValue >= interval.From);
		IS_TRUE(randomValue <= interval.To);
	}
}

//===========================================================================//

TEST(IntIntervalClear)
{
	IntInterval interval(-10, 22);

	interval.Clear();

	ARE_EQUAL(0, interval.From);
	ARE_EQUAL(0, interval.To);
}

//===========================================================================//

TEST(IntIntervalToString)
{
	IntInterval interval(5, 10);

	ARE_EQUAL("5:10", interval.ToString());
}

//===========================================================================//

}// namespace Bru

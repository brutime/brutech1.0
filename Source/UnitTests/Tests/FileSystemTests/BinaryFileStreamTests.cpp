//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты файловой системы
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../FileSystem/FileSystem.h"

namespace Bru
{

//===========================================================================//

const string TestBinaryFileName = "Test.bin";

//===========================================================================//

void RemoveBinaryFile()
{
	if (FileSystem::FileExists(TestBinaryFileName))
	{
		FileSystem::RemoveFile(TestBinaryFileName);
	}
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(BinaryFileStreamConstructor, RemoveBinaryFile, RemoveBinaryFile)
{
	//Write

	SharedPtr<BinaryFileStream> writeFile = new BinaryFileStream(TestBinaryFileName, FileAccess::Write);

	IS_TRUE(FileSystem::FileExists(TestBinaryFileName));
	IS_TRUE(writeFile->IsOpened());
	ARE_EQUAL(TestBinaryFileName, writeFile->_pathToFile);
	IS_TRUE(writeFile->_access == FileAccess::Write);

	writeFile->Close();

	IS_FALSE(writeFile->IsOpened());

	//Read

	SharedPtr<BinaryFileStream> readFile = new BinaryFileStream(TestBinaryFileName, FileAccess::Read);

	ARE_EQUAL(TestBinaryFileName, readFile->_pathToFile);
	IS_TRUE(readFile->_access == FileAccess::Read);

	readFile->Close();

	IS_FALSE(readFile->IsOpened());

	//Append

	SharedPtr<BinaryFileStream> appendFile = new BinaryFileStream(TestBinaryFileName, FileAccess::Append);

	ARE_EQUAL(TestBinaryFileName, appendFile->_pathToFile);
	IS_TRUE(appendFile->_access == FileAccess::Append);

	appendFile->Close();

	IS_FALSE(appendFile->IsOpened());

	//ReadWrite

	SharedPtr<BinaryFileStream> readWriteFile = new BinaryFileStream(TestBinaryFileName, FileAccess::ReadWrite);

	IS_TRUE(FileSystem::FileExists(TestBinaryFileName));
	IS_TRUE(readWriteFile->IsOpened());
	ARE_EQUAL(TestBinaryFileName, readWriteFile->_pathToFile);
	IS_TRUE(readWriteFile->_access == FileAccess::ReadWrite);

	readWriteFile->Close();

	IS_FALSE(readWriteFile->IsOpened());
}

//===========================================================================//

TEST(TBinaryFileStreamConstructorExceptions)
{
	IS_THROW(BinaryFileStream(TestBinaryFileName, FileAccess::Read), FileNotFoundException);
	IS_THROW(BinaryFileStream(TestBinaryFileName, FileAccess::Append), FileNotFoundException);
	IS_THROW(BinaryFileStream(TestBinaryFileName, FileAccess::ReadWrite), FileNotFoundException);
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(BinaryFileStreamReadWrite, RemoveBinaryFile, RemoveBinaryFile)
{
	//Создаем файл
	BinaryFileStream(TestBinaryFileName, FileAccess::Write);
	
	string outputString = "B¥𠀀௵rΩ";
	Array<byte>::SimpleType outputArray = {0x4, 0x8, 0x35, 0x31};
	
	const integer OutputRawArrayCount = 4;
	byte * outputRawArray = new byte[OutputRawArrayCount];

	SharedPtr<BinaryFileStream> file = new BinaryFileStream(TestBinaryFileName, FileAccess::ReadWrite);

	file->Write(byte(13));
	file->WriteLine(byte(13));

	file->Write(byte(42), 2);
	file->WriteLine(byte(42), 2);

	file->Write(666);
	file->WriteLine(666);

	file->Write(666, 2);
	file->WriteLine(666, 2);

	file->Write(666.666f);
	file->WriteLine(666.666f);

	file->Write(13.0f, 2);
	file->WriteLine(13.0f, 2);

	file->Write(outputString);
	file->WriteLine(outputString);

	file->Write(outputArray);
	file->WriteLine(outputArray);
	
	file->Write(outputRawArray, OutputRawArrayCount);
	file->WriteLine(outputRawArray, OutputRawArrayCount);

	file->Reset();

	ARE_EQUAL(13, file->ReadByte());
	ARE_EQUAL(13, file->ReadByte());
	ARE_EQUAL('\n', file->ReadByte());

	ARE_EQUAL(42, file->ReadByte());
	ARE_EQUAL(42, file->ReadByte());
	ARE_EQUAL(42, file->ReadByte());
	ARE_EQUAL(42, file->ReadByte());
	ARE_EQUAL('\n', file->ReadByte());

	ARE_EQUAL(666, file->ReadInteger());
	ARE_EQUAL(666, file->ReadInteger());
	ARE_EQUAL('\n', file->ReadByte());

	ARE_EQUAL(666, file->ReadInteger(2));
	ARE_EQUAL(666, file->ReadInteger(2));
	ARE_EQUAL('\n', file->ReadByte());

	ARE_CLOSE(666.666f, file->ReadFloat(), Math::Accuracy);
	ARE_CLOSE(666.666f, file->ReadFloat(), Math::Accuracy);
	ARE_EQUAL('\n', file->ReadByte());

	ARE_CLOSE(13.0f, file->ReadFloat(2), Math::Accuracy);
	ARE_CLOSE(13.0f, file->ReadFloat(2), Math::Accuracy);
	ARE_EQUAL('\n', file->ReadByte());

	ARE_EQUAL(outputString, file->ReadString(outputString.GetLengthInBytes()));
	ARE_EQUAL(outputString, file->ReadLine());

	Array<byte>::SimpleType inputArray = file->ReadByteArray(outputArray.GetCount());
	ARE_ARRAYS_EQUAL(outputArray, inputArray, outputArray.GetCount());
	inputArray = file->ReadByteArray(outputArray.GetCount());
	ARE_ARRAYS_EQUAL(outputArray, inputArray, outputArray.GetCount());
	ARE_EQUAL('\n', file->ReadByte());
	
	byte * inputRawArray = new byte[OutputRawArrayCount];
	
	file->ReadByteArray(inputRawArray, OutputRawArrayCount);
	ARE_ARRAYS_EQUAL(outputRawArray, inputRawArray, OutputRawArrayCount);
	file->ReadByteArray(inputRawArray, OutputRawArrayCount);
	ARE_ARRAYS_EQUAL(outputRawArray, inputRawArray, OutputRawArrayCount);
	ARE_EQUAL('\n', file->ReadByte());
	
	delete [] inputRawArray;

	file->Close();
	
	delete [] outputRawArray;
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(BinaryFileStreamReadWriteExceptions, RemoveBinaryFile, RemoveBinaryFile)
{
	//Создаем файл
	BinaryFileStream(TestBinaryFileName, FileAccess::Write);	
	
	SharedPtr<BinaryFileStream> readWriteFile = new BinaryFileStream(TestBinaryFileName, FileAccess::ReadWrite);

	IS_THROW(readWriteFile->Write(byte(42), -1), InvalidArgumentException);
	IS_THROW(readWriteFile->WriteLine(byte(42), -1), InvalidArgumentException);
	IS_THROW(readWriteFile->Write(666, -1), InvalidArgumentException);
	IS_THROW(readWriteFile->WriteLine(666, -1), InvalidArgumentException);
	IS_THROW(readWriteFile->Write(13.0f, -1), InvalidArgumentException);
	IS_THROW(readWriteFile->WriteLine(13.0f, -1), InvalidArgumentException);

	readWriteFile->Reset();

	IS_THROW(readWriteFile->ReadInteger(-1), InvalidArgumentException);
	IS_THROW(readWriteFile->ReadFloat(-1), InvalidArgumentException);
	IS_THROW(readWriteFile->ReadString(-1), InvalidArgumentException);

	readWriteFile->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(BinaryFileStreamStateExceptions, RemoveBinaryFile, RemoveBinaryFile)
{
	Array<byte>::SimpleType byteArray;
	const integer RawByteArrayCount = 1;
	byte * rawByteArray = new byte[RawByteArrayCount] {0x4};
	
	//Write	

	SharedPtr<BinaryFileStream> writeFile = new BinaryFileStream(TestBinaryFileName, FileAccess::Write);

	IS_THROW(writeFile->ReadByte(), InvalidStateException);
	IS_THROW(writeFile->ReadInteger(), InvalidStateException);
	IS_THROW(writeFile->ReadInteger(1), InvalidStateException);
	IS_THROW(writeFile->ReadFloat(), InvalidStateException);
	IS_THROW(writeFile->ReadFloat(1), InvalidStateException);
	IS_THROW(writeFile->ReadString(1), InvalidStateException);
	IS_THROW(writeFile->ReadLine(), InvalidStateException);
	IS_THROW(writeFile->ReadByteArray(1), InvalidStateException);	
	IS_THROW(writeFile->ReadByteArray(rawByteArray, RawByteArrayCount), InvalidStateException);

	writeFile->Close();

	//Read

	SharedPtr<BinaryFileStream> readFile = new BinaryFileStream(TestBinaryFileName, FileAccess::Read);

	IS_THROW(readFile->Write(byte(13)), InvalidStateException);
	IS_THROW(readFile->WriteLine(byte(13)), InvalidStateException);

	IS_THROW(readFile->Write(byte(42), 2), InvalidStateException);
	IS_THROW(readFile->WriteLine(byte(42), 2), InvalidStateException);

	IS_THROW(readFile->Write(666), InvalidStateException);
	IS_THROW(readFile->WriteLine(666), InvalidStateException);

	IS_THROW(readFile->Write(666, 2), InvalidStateException);
	IS_THROW(readFile->WriteLine(666, 2), InvalidStateException);

	IS_THROW(readFile->Write(666.666f), InvalidStateException);
	IS_THROW(readFile->WriteLine(666.666f), InvalidStateException);

	IS_THROW(readFile->Write(13.0f, 2), InvalidStateException);
	IS_THROW(readFile->WriteLine(13.0f, 2), InvalidStateException);

	IS_THROW(readFile->Write("BruTime"), InvalidStateException);
	IS_THROW(readFile->WriteLine("BruTime"), InvalidStateException);

	IS_THROW(readFile->Write(Array<byte>::SimpleType()), InvalidStateException);
	IS_THROW(readFile->WriteLine(Array<byte>::SimpleType()), InvalidStateException);
	
	IS_THROW(readFile->Write(rawByteArray, RawByteArrayCount), InvalidStateException);
	IS_THROW(readFile->WriteLine(rawByteArray, RawByteArrayCount), InvalidStateException);	

	readFile->Close();

	//Append

	SharedPtr<BinaryFileStream> appendFile = new BinaryFileStream(TestBinaryFileName, FileAccess::Append);

	IS_THROW(appendFile->ReadByte(), InvalidStateException);
	IS_THROW(appendFile->ReadInteger(), InvalidStateException);
	IS_THROW(appendFile->ReadInteger(1), InvalidStateException);
	IS_THROW(appendFile->ReadFloat(), InvalidStateException);
	IS_THROW(appendFile->ReadFloat(1), InvalidStateException);
	IS_THROW(appendFile->ReadString(1), InvalidStateException);
	IS_THROW(appendFile->ReadLine(), InvalidStateException);
	IS_THROW(appendFile->ReadByteArray(1), InvalidStateException);
	IS_THROW(appendFile->ReadByteArray(rawByteArray, RawByteArrayCount), InvalidStateException);

	appendFile->Close();

	//Closed

	SharedPtr<BinaryFileStream> closedFile = new BinaryFileStream(TestBinaryFileName, FileAccess::ReadWrite);
	closedFile->Close();

	IS_THROW(closedFile->ReadByte(), InvalidStateException);
	IS_THROW(closedFile->ReadInteger(), InvalidStateException);
	IS_THROW(closedFile->ReadInteger(1), InvalidStateException);
	IS_THROW(closedFile->ReadFloat(), InvalidStateException);
	IS_THROW(closedFile->ReadFloat(1), InvalidStateException);
	IS_THROW(closedFile->ReadString(1), InvalidStateException);
	IS_THROW(closedFile->ReadLine(), InvalidStateException);
	IS_THROW(closedFile->ReadByteArray(1), InvalidStateException);
	IS_THROW(closedFile->ReadByteArray(rawByteArray, RawByteArrayCount), InvalidStateException);

	IS_THROW(closedFile->Write(byte(13)), InvalidStateException);
	IS_THROW(closedFile->WriteLine(byte(13)), InvalidStateException);

	IS_THROW(closedFile->Write(byte(42), 2), InvalidStateException);
	IS_THROW(closedFile->WriteLine(byte(42), 2), InvalidStateException);

	IS_THROW(closedFile->Write(666), InvalidStateException);
	IS_THROW(closedFile->WriteLine(666), InvalidStateException);

	IS_THROW(closedFile->Write(666, 2), InvalidStateException);
	IS_THROW(closedFile->WriteLine(666, 2), InvalidStateException);

	IS_THROW(closedFile->Write(666.666f), InvalidStateException);
	IS_THROW(closedFile->WriteLine(666.666f), InvalidStateException);

	IS_THROW(closedFile->Write(13.0f, 2), InvalidStateException);
	IS_THROW(closedFile->WriteLine(13.0f, 2), InvalidStateException);

	IS_THROW(closedFile->Write("BruTime"), InvalidStateException);
	IS_THROW(closedFile->WriteLine("BruTime"), InvalidStateException);

	IS_THROW(closedFile->Write(Array<byte>::SimpleType()), InvalidStateException);
	IS_THROW(closedFile->WriteLine(Array<byte>::SimpleType()), InvalidStateException);
	
	IS_THROW(closedFile->Write(rawByteArray, RawByteArrayCount), InvalidStateException);
	IS_THROW(closedFile->WriteLine(rawByteArray, RawByteArrayCount), InvalidStateException);	
	
	delete [] rawByteArray;
}

//===========================================================================//

} // namespace Bru

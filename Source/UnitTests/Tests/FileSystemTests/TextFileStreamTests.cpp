//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты файловой системы
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../FileSystem/FileSystem.h"

namespace Bru
{

//===========================================================================//

const string TestTextFileName = "Test.txt";

//===========================================================================//

void RemoveTextFile()
{
	if (FileSystem::FileExists(TestTextFileName))
	{
		FileSystem::RemoveFile(TestTextFileName);
	}
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(TextFileStreamConstructor, RemoveTextFile, RemoveTextFile)
{
	//Write

	SharedPtr<TextFileStream> writeFile = new TextFileStream(TestTextFileName, FileAccess::Write);

	IS_TRUE(FileSystem::FileExists(TestTextFileName));
	IS_TRUE(writeFile->IsOpened());
	ARE_EQUAL(TestTextFileName, writeFile->_pathToFile);
	IS_TRUE(writeFile->_access == FileAccess::Write);

	writeFile->Close();

	IS_FALSE(writeFile->IsOpened());

	//ReadWrite

	SharedPtr<TextFileStream> readWriteFile = new TextFileStream(TestTextFileName, FileAccess::ReadWrite);

	IS_TRUE(FileSystem::FileExists(TestTextFileName));
	IS_TRUE(readWriteFile->IsOpened());
	ARE_EQUAL(TestTextFileName, readWriteFile->_pathToFile);
	IS_TRUE(readWriteFile->_access == FileAccess::ReadWrite);

	readWriteFile->Close();

	IS_FALSE(readWriteFile->IsOpened());

	//Read

	SharedPtr<TextFileStream> readFile = new TextFileStream(TestTextFileName, FileAccess::Read);

	IS_TRUE(readFile->IsOpened());
	ARE_EQUAL(TestTextFileName, readFile->_pathToFile);
	IS_TRUE(readFile->_access == FileAccess::Read);

	readFile->Close();

	IS_FALSE(readFile->IsOpened());

	//Append

	SharedPtr<TextFileStream> appendFile = new TextFileStream(TestTextFileName, FileAccess::Append);

	IS_TRUE(appendFile->IsOpened());
	ARE_EQUAL(TestTextFileName, appendFile->_pathToFile);
	IS_TRUE(appendFile->_access == FileAccess::Append);

	appendFile->Close();

	IS_FALSE(appendFile->IsOpened());
}

//===========================================================================//

TEST(TextFileStreamConstructorExceptions)
{
	IS_THROW(TextFileStream(TestTextFileName, FileAccess::Read), FileNotFoundException);
	IS_THROW(TextFileStream(TestTextFileName, FileAccess::Append), FileNotFoundException);
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(TextFileStreamReadWrite, RemoveTextFile, RemoveTextFile)
{
	string outputString = "B¥𠀀௵rΩ";
	
	//Создаем файл
	TextFileStream(TestTextFileName, FileAccess::Write);
	
	SharedPtr<TextFileStream> file = new TextFileStream(TestTextFileName, FileAccess::ReadWrite);

	file->Write(666);
	file->Write(" ");
	file->WriteLine(666);

	file->Write(666.666f);
	file->Write(" ");
	file->WriteLine(666.666f);
	
	file->Write(outputString);
	file->WriteLine(outputString);

	file->Reset();

	ARE_EQUAL(666, file->ReadInteger());
	ARE_EQUAL(" ", file->ReadString(1));
	ARE_EQUAL(666, file->ReadInteger());
	ARE_EQUAL("\n", file->ReadString(1));

	ARE_CLOSE(666.666f, file->ReadFloat(), Math::Accuracy);
	ARE_EQUAL(" ", file->ReadString(1));
	ARE_CLOSE(666.666f, file->ReadFloat(), Math::Accuracy);
	ARE_EQUAL("\n", file->ReadString(1));

	ARE_EQUAL(outputString, file->ReadString(outputString.GetLengthInBytes()));
	ARE_EQUAL(outputString, file->ReadLine());

	file->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(TextFileStreamReadWriteExceptions, RemoveTextFile, RemoveTextFile)
{
	//Создаем файл
	TextFileStream(TestTextFileName, FileAccess::Write);
	
	SharedPtr<TextFileStream> file = new TextFileStream(TestTextFileName, FileAccess::ReadWrite);

	IS_THROW(file->Write(byte(13)), InvalidStateException);
	IS_THROW(file->WriteLine(byte(13)), InvalidStateException);
	IS_THROW(file->Write(byte(42), 2), InvalidStateException);
	IS_THROW(file->WriteLine(byte(42), 2), InvalidStateException);
	IS_THROW(file->Write(666, 2), InvalidStateException);
	IS_THROW(file->WriteLine(666, 2), InvalidStateException);
	IS_THROW(file->Write(13.0f, 2), InvalidStateException);
	IS_THROW(file->WriteLine(13.0f, 2), InvalidStateException);

	file->Reset();

	IS_THROW(file->ReadByte(), InvalidStateException);
	IS_THROW(file->ReadInteger(2), InvalidStateException);
	IS_THROW(file->ReadFloat(2), InvalidStateException);
	IS_THROW(file->ReadString(-1), InvalidArgumentException);

	file->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(TextFileStreamLineWithoutCRLF, RemoveTextFile, RemoveTextFile)
{
	string lineWithoutCRLF = "I'm a line. I haven't CRLF. Hahaha, try to read me!";
	
	SharedPtr<TextFileStream> writeFile = new TextFileStream(TestTextFileName, FileAccess::Write);
	writeFile->Write(lineWithoutCRLF);
	writeFile->Close();
	
	SharedPtr<TextFileStream> readFile = new TextFileStream(TestTextFileName, FileAccess::Read);
	ARE_EQUAL(lineWithoutCRLF, readFile->ReadLine());
	readFile->Close();
}

//===========================================================================//

SETUP_TEAR_DOWN_TEST(TextFileStreamStateExceptions, RemoveTextFile, RemoveTextFile)
{
	Array<byte>::SimpleType byteArray;
	
	const integer RawByteArrayCount = 1;
	byte * rawByteArray = new byte[RawByteArrayCount] {0x4};
	
	//Write

	SharedPtr<TextFileStream> writeFile = new TextFileStream(TestTextFileName, FileAccess::Write);

	IS_THROW(writeFile->ReadByte(), InvalidStateException);
	IS_THROW(writeFile->ReadInteger(), InvalidStateException);
	IS_THROW(writeFile->ReadInteger(1), InvalidStateException);
	IS_THROW(writeFile->ReadFloat(), InvalidStateException);
	IS_THROW(writeFile->ReadFloat(1), InvalidStateException);
	IS_THROW(writeFile->ReadString(1), InvalidStateException);
	IS_THROW(writeFile->ReadLine(), InvalidStateException);
	IS_THROW(writeFile->ReadByteArray(1), InvalidStateException);
	IS_THROW(writeFile->ReadByteArray(rawByteArray, RawByteArrayCount), InvalidStateException);
	
	IS_THROW(writeFile->Write(byte(13)), InvalidStateException);
	IS_THROW(writeFile->WriteLine(byte(13)), InvalidStateException);
	
	IS_THROW(writeFile->Write(byte(13), 13), InvalidStateException);
	IS_THROW(writeFile->WriteLine(byte(13), 13), InvalidStateException);	
	
	IS_THROW(writeFile->Write(666, 13), InvalidStateException);
	IS_THROW(writeFile->WriteLine(666, 13), InvalidStateException);	
	
	IS_THROW(writeFile->Write(13.0f, 13), InvalidStateException);
	IS_THROW(writeFile->WriteLine(13.0f, 13), InvalidStateException);
	
	IS_THROW(writeFile->Write(byteArray), InvalidStateException);
	IS_THROW(writeFile->WriteLine(byteArray), InvalidStateException);
	
	IS_THROW(writeFile->Write(rawByteArray, RawByteArrayCount), InvalidStateException);
	IS_THROW(writeFile->WriteLine(rawByteArray, RawByteArrayCount), InvalidStateException);	

	writeFile->Close();

	//Read

	SharedPtr<TextFileStream> readFile = new TextFileStream(TestTextFileName, FileAccess::Read);

	IS_THROW(readFile->Write(byte(13)), InvalidStateException);
	IS_THROW(readFile->WriteLine(byte(13)), InvalidStateException);

	IS_THROW(readFile->Write(byte(42), 2), InvalidStateException);
	IS_THROW(readFile->WriteLine(byte(42), 2), InvalidStateException);

	IS_THROW(readFile->Write(666), InvalidStateException);
	IS_THROW(readFile->WriteLine(666), InvalidStateException);

	IS_THROW(readFile->Write(666, 2), InvalidStateException);
	IS_THROW(readFile->WriteLine(666, 2), InvalidStateException);

	IS_THROW(readFile->Write(666.666f), InvalidStateException);
	IS_THROW(readFile->WriteLine(666.666f), InvalidStateException);

	IS_THROW(readFile->Write(13.0f, 2), InvalidStateException);
	IS_THROW(readFile->WriteLine(13.0f, 2), InvalidStateException);

	IS_THROW(readFile->Write("BruTime"), InvalidStateException);
	IS_THROW(readFile->WriteLine("BruTime"), InvalidStateException);

	IS_THROW(readFile->Write(Array<byte>::SimpleType()), InvalidStateException);
	IS_THROW(readFile->WriteLine(Array<byte>::SimpleType()), InvalidStateException);
	
	IS_THROW(readFile->ReadByte(), InvalidStateException);
	IS_THROW(readFile->ReadInteger(13), InvalidStateException);
	IS_THROW(readFile->ReadFloat(13), InvalidStateException);
	IS_THROW(readFile->ReadByteArray(13), InvalidStateException);
	IS_THROW(readFile->ReadByteArray(rawByteArray, 13), InvalidStateException);

	readFile->Close();

	//Append

	SharedPtr<TextFileStream> appendFile = new TextFileStream(TestTextFileName, FileAccess::Append);

	IS_THROW(appendFile->ReadByte(), InvalidStateException);
	IS_THROW(appendFile->ReadInteger(), InvalidStateException);
	IS_THROW(appendFile->ReadInteger(1), InvalidStateException);
	IS_THROW(appendFile->ReadFloat(), InvalidStateException);
	IS_THROW(appendFile->ReadFloat(1), InvalidStateException);
	IS_THROW(appendFile->ReadString(1), InvalidStateException);
	IS_THROW(appendFile->ReadLine(), InvalidStateException);
	IS_THROW(appendFile->ReadByteArray(1), InvalidStateException);
	IS_THROW(appendFile->ReadByteArray(rawByteArray, RawByteArrayCount), InvalidStateException);

	appendFile->Close();

	//Closed

	SharedPtr<TextFileStream> closedFile = new TextFileStream(TestTextFileName, FileAccess::ReadWrite);
	closedFile->Close();

	IS_THROW(closedFile->ReadByte(), InvalidStateException);
	IS_THROW(closedFile->ReadInteger(), InvalidStateException);
	IS_THROW(closedFile->ReadInteger(1), InvalidStateException);
	IS_THROW(closedFile->ReadFloat(), InvalidStateException);
	IS_THROW(closedFile->ReadFloat(1), InvalidStateException);
	IS_THROW(closedFile->ReadString(1), InvalidStateException);
	IS_THROW(closedFile->ReadLine(), InvalidStateException);
	IS_THROW(closedFile->ReadByteArray(1), InvalidStateException);
	IS_THROW(closedFile->ReadByteArray(rawByteArray, RawByteArrayCount), InvalidStateException);

	IS_THROW(closedFile->Write(byte(13)), InvalidStateException);
	IS_THROW(closedFile->WriteLine(byte(13)), InvalidStateException);

	IS_THROW(closedFile->Write(byte(42), 2), InvalidStateException);
	IS_THROW(closedFile->WriteLine(byte(42), 2), InvalidStateException);

	IS_THROW(closedFile->Write(666), InvalidStateException);
	IS_THROW(closedFile->WriteLine(666), InvalidStateException);

	IS_THROW(closedFile->Write(666, 2), InvalidStateException);
	IS_THROW(closedFile->WriteLine(666, 2), InvalidStateException);

	IS_THROW(closedFile->Write(666.666f), InvalidStateException);
	IS_THROW(closedFile->WriteLine(666.666f), InvalidStateException);

	IS_THROW(closedFile->Write(13.0f, 2), InvalidStateException);
	IS_THROW(closedFile->WriteLine(13.0f, 2), InvalidStateException);

	IS_THROW(closedFile->Write("BruTime"), InvalidStateException);
	IS_THROW(closedFile->WriteLine("BruTime"), InvalidStateException);

	IS_THROW(closedFile->Write(Array<byte>::SimpleType()), InvalidStateException);
	IS_THROW(closedFile->WriteLine(Array<byte>::SimpleType()), InvalidStateException);
	
	delete [] rawByteArray;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты файловой системы
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Math/Math.h"
#include "../../../FileSystem/FileSystem.h"

namespace Bru
{

//===========================================================================//

TEST(FileSystemFileExistsRemoveFile)
{
	const string TestFileName = "File.bin";
	SharedPtr<BinaryFileStream> file = new BinaryFileStream(TestFileName, FileAccess::Write);
	file->Close();

	IS_TRUE(FileSystem::FileExists(TestFileName));
	FileSystem::RemoveFile(TestFileName);
	IS_FALSE(FileSystem::FileExists(TestFileName));
}

//===========================================================================//

}// namespace Bru

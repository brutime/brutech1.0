//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: тесты работы умного указателя
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Dummies/DummyCompoundObject.h"
#include "../../Dummies/DummyBase.h"
#include "../../Dummies/DummyChild.h"
#include "../../Dummies/DummyBaseCounter.h"
#include "../../Dummies/DummyChildCounter.h"
#include "../../Dummies/DummyMultiplyInheritanceObject.h"
#include "../../Helpers/SharedPtrFactory.h"

namespace Bru
{

//===========================================================================//

TEST(SharedPtrDefaultConstructor)
{
	SharedPtr<DummyCompoundObject> sharedPtr;

	IS_TRUE(sharedPtr._objectPtr == NullPtr);
	IS_TRUE(sharedPtr._refsCounter == NullPtr);
}

//===========================================================================//

TEST(SharedPtrNullPtrConstructor)
{
	SharedPtr<DummyCompoundObject> sharedPtr(NullPtr);

	IS_TRUE(sharedPtr._objectPtr == NullPtr);
	IS_TRUE(sharedPtr._refsCounter == NullPtr);
}

//===========================================================================//

TEST(SharedPtrNullObjectConstructor)
{
	DummyCompoundObject * simplePtr = NullPtr;
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);

	IS_TRUE(sharedPtr._objectPtr == NullPtr);
	IS_TRUE(sharedPtr._refsCounter == NullPtr);
}

//===========================================================================//

TEST(SharedPtrObjectConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);

	IS_TRUE(sharedPtr._objectPtr == simplePtr);
	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrObjectSharedPtrConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sourceSharedPtr(simplePtr);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr, sourceSharedPtr);

	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);

	ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

	sourceSharedPtr = NullPtr;

	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrObjectSharedPtrInheritanceConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sourceSharedPtr(simplePtr);
	SharedPtr<DummyBase> sharedPtr(simplePtr, sourceSharedPtr);

	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);

	ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

	sourceSharedPtr = NullPtr;

	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrObjectSharedPtrCastConstructor)
{
	DummyBase * simplePtr = new DummyBase();
	SharedPtr<DummyBase> sourceSharedPtr(simplePtr);
	SharedPtr<DummyChild> sharedPtr(static_cast<DummyChild *>(simplePtr), sourceSharedPtr);

	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);

	ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

	sourceSharedPtr = NullPtr;

	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrObjectSharedNullPtrConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(NullPtr, nullSharedPtr);

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrObjectSharedNullPtrInheritanceConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyChildCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(NullPtr, nullSharedPtr);

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrObjectSharedNullPtrCastConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyChildCounter> sharedPtr(NullPtr, nullSharedPtr);

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrObjectSharedPtrConstructorExceptions)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sourceSharedPtr(simplePtr);
	IS_THROW(SharedPtr<DummyCompoundObject>(new DummyCompoundObject(666), sourceSharedPtr), InvalidArgumentException);
}

//===========================================================================//

TEST(SharedPtrObjectSharedPtrInheritanceConstructorExceptions)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sourceSharedPtr(simplePtr);
	IS_THROW(SharedPtr<DummyBase>(new DummyChild(), sourceSharedPtr), InvalidArgumentException);
}

//===========================================================================//

TEST(SharedPtrObjectSharedPtrCastConstructorExceptions)
{
	DummyBase * simplePtr = new DummyBase();
	SharedPtr<DummyBase> sourceSharedPtr(simplePtr);
	IS_THROW(SharedPtr<DummyChild>(static_cast<DummyChild *>(new DummyBase()), sourceSharedPtr), InvalidArgumentException);
}

//===========================================================================//

TEST(SharedPtrCopyConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sourceSharedPtr(simplePtr);
	SharedPtr<DummyCompoundObject> sharedPtr(sourceSharedPtr);

	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);

	ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

	sourceSharedPtr = NullPtr;

	IS_TRUE(sharedPtr._objectPtr == simplePtr);
	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrInheritanceCopyConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sourceSharedPtr(simplePtr);
	SharedPtr<DummyBase> sharedPtr(sourceSharedPtr);
	
	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);

	ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());	

	sourceSharedPtr = NullPtr;

	IS_TRUE(sharedPtr._objectPtr == simplePtr);
	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrCopyConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(nullSharedPtr);

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrInheritanceCopyConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyChildCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(nullSharedPtr);

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrWeakPtrConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sourceSharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(sourceSharedPtr);
	SharedPtr<DummyCompoundObject> sharedPtr(weakPtr);

	IS_TRUE(sharedPtr._objectPtr == weakPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == weakPtr._refsCounter);

	ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr._refsCounter->GetWeakRefsCount());

	sourceSharedPtr = NullPtr;
	weakPtr = NullPtr;

	IS_TRUE(sharedPtr._objectPtr == simplePtr);
	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrWeakPtrInheritanceConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sourceSharedPtr(simplePtr);
	WeakPtr<DummyChild> weakPtr(sourceSharedPtr);
	SharedPtr<DummyBase> sharedPtr(weakPtr);
	
	IS_TRUE(sharedPtr._objectPtr == weakPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == weakPtr._refsCounter);

	ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr._refsCounter->GetWeakRefsCount());	

	sourceSharedPtr = NullPtr;
	weakPtr = NullPtr;

	IS_TRUE(sharedPtr._objectPtr == simplePtr);
	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrWeakNullPtrConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullWeakPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(nullWeakPtr);

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrWeakNullPtrInheritanceConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyChildCounter> nullWeakPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(nullWeakPtr);

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrMoveConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(std::move(SharedPtr<DummyCompoundObject>(simplePtr)));

	IS_TRUE(sharedPtr._objectPtr == simplePtr);

	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrInheritanceMoveConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyBase> sharedPtr(std::move(SharedPtr<DummyChild>(simplePtr)));

	IS_TRUE(sharedPtr._objectPtr == simplePtr);

	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrMoveConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> sharedPtr(std::move(SharedPtr<DummyBaseCounter>(NullPtr)));

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrInheritanceMoveConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> sharedPtr(std::move(SharedPtr<DummyChildCounter>(NullPtr)));

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrDestructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> sourceSharedPtr = new DummyBaseCounter();
		SharedPtr<DummyBaseCounter> sharedPtr = sourceSharedPtr;
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrNullPtrCopyOperator)
{
	DummyBaseCounter::Reset();
	SharedPtr<DummyBaseCounter> sharedPtr = new DummyBaseCounter();

	sharedPtr = NullPtr;

	IS_TRUE(sharedPtr._objectPtr == NullPtr);
	IS_TRUE(sharedPtr._refsCounter == NullPtr);

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrObjectCopyOperator)
{
	DummyBaseCounter::Reset();
	{
		SharedPtr<DummyBaseCounter> sharedPtr = new DummyBaseCounter();

		DummyBaseCounter * simplePtr = new DummyBaseCounter();
		sharedPtr = simplePtr;

		IS_TRUE(sharedPtr._objectPtr == simplePtr);

		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrSameObjectCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyBaseCounter * simplePtr = new DummyBaseCounter();

		SharedPtr<DummyBaseCounter> sharedPtr = simplePtr;
		sharedPtr = simplePtr;

		IS_TRUE(sharedPtr._objectPtr == simplePtr);

		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyBaseCounter * firstSimplePtr = new DummyBaseCounter();
		DummyBaseCounter * secondSimplePtr = new DummyBaseCounter();
		SharedPtr<DummyBaseCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyBaseCounter> secondSharedPtr(secondSimplePtr);
		SharedPtr<DummyBaseCounter> sharedPtr;

		sharedPtr = firstSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(2, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == firstSimplePtr);
		IS_TRUE(sharedPtr._refsCounter == firstSharedPtr._refsCounter);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

		sharedPtr = secondSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		IS_TRUE(sharedPtr._refsCounter == secondSharedPtr._refsCounter);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

		firstSharedPtr = NullPtr;
		secondSharedPtr = NullPtr;

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyChildCounter * firstSimplePtr = new DummyChildCounter();
		DummyChildCounter * secondSimplePtr = new DummyChildCounter();
		SharedPtr<DummyChildCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyChildCounter> secondSharedPtr(secondSimplePtr);
		SharedPtr<DummyBaseCounter> sharedPtr;

		sharedPtr = firstSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(2, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

		sharedPtr = secondSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

		firstSharedPtr = NullPtr;
		secondSharedPtr = NullPtr;

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr = new DummyBaseCounter();

		sharedPtr = nullSharedPtr;

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyChildCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());

		sharedPtr = nullSharedPtr;

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrWeakPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyBaseCounter * firstSimplePtr = new DummyBaseCounter();
		DummyBaseCounter * secondSimplePtr = new DummyBaseCounter();
		SharedPtr<DummyBaseCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyBaseCounter> secondSharedPtr(secondSimplePtr);
		WeakPtr<DummyBaseCounter> weakPtr(secondSharedPtr);

		SharedPtr<DummyBaseCounter> sharedPtr;

		sharedPtr = firstSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(2, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

		sharedPtr = weakPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, sharedPtr._refsCounter->GetWeakRefsCount());

		firstSharedPtr = NullPtr;
		secondSharedPtr = NullPtr;
		weakPtr = NullPtr;

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrWeakPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyChildCounter * firstSimplePtr = new DummyChildCounter();
		DummyChildCounter * secondSimplePtr = new DummyChildCounter();
		SharedPtr<DummyChildCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyChildCounter> secondSharedPtr(secondSimplePtr);
		WeakPtr<DummyChildCounter> weakPtr(secondSharedPtr);

		SharedPtr<DummyBaseCounter> sharedPtr;

		sharedPtr = firstSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(2, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());

		sharedPtr = weakPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(2, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, sharedPtr._refsCounter->GetWeakRefsCount());

		firstSharedPtr = NullPtr;
		secondSharedPtr = NullPtr;
		weakPtr = NullPtr;

		IS_TRUE(sharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrWeakNullPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullWeakPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());

		sharedPtr = nullWeakPtr;

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrWeakNullPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyChildCounter> nullWeakPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());

		sharedPtr = nullWeakPtr;

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrCopyOperatorSelf)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);

	sharedPtr = simplePtr;

	IS_TRUE(sharedPtr._objectPtr == simplePtr);
	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(SharedPtrMoveOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyBaseCounter * simplePtr = new DummyBaseCounter();
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());

		sharedPtr = std::move(SharedPtr<DummyBaseCounter>(simplePtr));

		IS_TRUE(sharedPtr._objectPtr == simplePtr);

		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrInheritanceMoveOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyChildCounter * simplePtr = new DummyChildCounter();
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());

		sharedPtr = std::move(SharedPtr<DummyChildCounter>(simplePtr));

		IS_TRUE(sharedPtr._objectPtr == simplePtr);

		ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrMoveOperator)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> sharedPtr = new DummyBaseCounter();

		sharedPtr = std::move(SharedPtr<DummyBaseCounter>(NullPtr));

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrSharedNullPtrInheritanceMoveOperator)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> sharedPtr = new DummyBaseCounter();

		sharedPtr = std::move(SharedPtr<DummyChildCounter>(NullPtr));

		IS_TRUE(sharedPtr._objectPtr == NullPtr);
		IS_TRUE(sharedPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(SharedPtrDereferenceOperator)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr = simplePtr;

	IS_TRUE(*sharedPtr == *simplePtr);
	IS_TRUE((*sharedPtr).GetValue() == 13);
}

//===========================================================================//

TEST(SharedPtrDereferenceOperatorExceptions)
{
	SharedPtr<DummyCompoundObject> sharedNullPtr = NullPtr;

	IS_THROW((*sharedNullPtr).GetValue(), NullPointerException);

	SharedPtr<DummyCompoundObject> sharedPtr = new DummyCompoundObject(13);
	sharedPtr = NullPtr;

	IS_THROW((*sharedPtr).GetValue(), NullPointerException);
}

//===========================================================================//

TEST(SharedPtrSelectorOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr = new DummyCompoundObject(13);

	IS_TRUE(sharedPtr->GetValue() == 13);
}

//===========================================================================//

TEST(SharedPtrSelectorOperatorExceptions)
{
	SharedPtr<DummyCompoundObject> sharedNullPtr = NullPtr;

	IS_THROW(sharedNullPtr->GetValue(), NullPointerException);

	SharedPtr<DummyCompoundObject> sharedPtr = new DummyCompoundObject(13);
	sharedPtr = NullPtr;

	IS_THROW(sharedPtr->GetValue(), NullPointerException);
}

//===========================================================================//

TEST(SharedPtrTypeOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr = new DummyCompoundObject(13);

	IS_TRUE(DummyCompoundObject::IsEqualsToValue(sharedPtr, 13));
	IS_TRUE(!DummyCompoundObject::IsEqualsToValue(sharedPtr, 12));
}

//===========================================================================//

TEST(SharedPtrNullPtrEqualsOperator)
{
	SharedPtr<DummyCompoundObject> nullSharedPtr;
	SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));

	IS_TRUE(nullSharedPtr == NullPtr);
	IS_TRUE(sharedPtr != NullPtr);

	sharedPtr = NullPtr;

	IS_TRUE(sharedPtr == NullPtr);
}

//===========================================================================//

TEST(SharedPtrObjectEqualsOperator)
{
	DummyCompoundObject * equalsSimplePtr = new DummyCompoundObject(13);
	DummyCompoundObject * notEqualsSimplePtr = new DummyCompoundObject(666);
	SharedPtr<DummyCompoundObject> sharedPtr = equalsSimplePtr;

	IS_TRUE(sharedPtr == equalsSimplePtr);
	IS_TRUE(sharedPtr != notEqualsSimplePtr);

	delete notEqualsSimplePtr;
}

//===========================================================================//

TEST(SharedPtrEqualsOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> equalsSharedPtr = sharedPtr;
	SharedPtr<DummyCompoundObject> notEqualsSharedPtr = new DummyCompoundObject(666);

	IS_TRUE(sharedPtr == equalsSharedPtr);
	IS_TRUE(sharedPtr != notEqualsSharedPtr);
}

//===========================================================================//

TEST(SharedPtrInheritanceEqualsOperator)
{
	SharedPtr<DummyChild> sharedPtr = new DummyChild();
	SharedPtr<DummyBase> equalsSharedPtr = sharedPtr;
	SharedPtr<DummyBase> notEqualsSharedPtr = new DummyChild();

	IS_TRUE(sharedPtr == equalsSharedPtr);
	IS_TRUE(sharedPtr != notEqualsSharedPtr);
}

//===========================================================================//

TEST(SharedPtrWeakPtrEqualsOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr = new DummyCompoundObject(13);
	WeakPtr<DummyCompoundObject> equalsWeakPtr = sharedPtr;
	SharedPtr<DummyCompoundObject> notEqualsSharedPtr = new DummyCompoundObject(666);
	WeakPtr<DummyCompoundObject> notEqualsWeakPtr = notEqualsSharedPtr;

	IS_TRUE(sharedPtr == equalsWeakPtr);
	IS_TRUE(sharedPtr != notEqualsWeakPtr);
}

//===========================================================================//

TEST(SharedPtrWeakPtrInheritanceEqualsOperator)
{
	SharedPtr<DummyChild> sharedPtr = new DummyChild();
	WeakPtr<DummyBase> equalsWeakPtr = sharedPtr;
	SharedPtr<DummyChild> notEqualsSharedPtr = new DummyChild();
	WeakPtr<DummyBase> notEqualsWeakPtr = notEqualsSharedPtr;

	IS_TRUE(sharedPtr == equalsWeakPtr);
	IS_TRUE(sharedPtr != notEqualsWeakPtr);
}

//===========================================================================//

TEST(SharedPtrGet)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr = simplePtr;

	IS_TRUE(sharedPtr.Get() == simplePtr);
}

//===========================================================================//

TEST(SharedPtrMultiplyInheritance)
{
	SharedPtr<DummyMultiplyInheritanceObject> object = new DummyMultiplyInheritanceObject();
	DummyMultiplyInheritanceObject::WriteDummy(object);

	ARE_EQUAL(true, object->Writed);
}

//===========================================================================//

TEST(SharedPtrFactory)
{
	SharedPtr<SharedPtrFactory> factory = new SharedPtrFactory();
	SharedPtr<DummyBase> basePtr = factory->CreateBase();

	ARE_EQUAL("DummyBase", basePtr->GetValue());
	ARE_EQUAL(1, basePtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, basePtr._refsCounter->GetWeakRefsCount());

	SharedPtr<DummyBase> childPtr = factory->CreateChild();

	ARE_EQUAL("DummyChild", childPtr->GetValue());
	ARE_EQUAL(1, childPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, childPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

}// namespace Bru

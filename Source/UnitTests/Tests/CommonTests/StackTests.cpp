//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты стека
//              Проверки Push(const T &) проходит в StackTests
//              Проверки Push(T &&) проходит в SimplyTypeStackTests
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(StackGetters)
{
	Stack<DummyCompoundObject> stack;

	stack._count = 10;
	ARE_EQUAL(10, stack.GetCount());
}

//===========================================================================//

TEST(StackDefaultConstructor)
{
	Stack<DummyCompoundObject> stack;

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(StackConstructor)
{
	Stack<DummyCompoundObject> stack(10);

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(10, stack._reservedCount);
	IS_TRUE(stack._dataPtr != NullPtr);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(StackConstructorExceptions)
{
	IS_THROW(Stack<DummyCompoundObject> stack(-1), InvalidArgumentException);
}
#endif

//===========================================================================//

TEST(StackInitializerListConstructor)
{
	const integer ExpectedArrayCount = 10;
	const DummyCompoundObject ExpectedArray[ExpectedArrayCount] = { d0, d1, d2, d3, d4, d5, d6, d7, d8, d9 };

	Stack<DummyCompoundObject> stack = { d0, d1, d2, d3, d4, d5, d6, d7, d8, d9 };

	ARE_EQUAL(ExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(ExpectedArrayCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(ExpectedArray, stack, ExpectedArrayCount);
}

//===========================================================================//

TEST(StackCopyConstructor)
{
	Stack<DummyCompoundObject> sourceStack = ContainersHelper::CreateTestDummyCompoundObjectStack();

	Stack<DummyCompoundObject> stack(sourceStack);

	sourceStack.Clear();

	ARE_EQUAL(TestDummyesCount, stack.GetCount());
	ARE_EQUAL(TestDummyesCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, stack, TestDummyesCount);
}

//===========================================================================//

TEST(StackMoveConstructor)
{
	Stack<DummyCompoundObject> stack(std::move(ContainersHelper::CreateTestDummyCompoundObjectStack()));

	ARE_EQUAL(TestDummyesCount, stack.GetCount());
	ARE_EQUAL(TestDummyesCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, stack, TestDummyesCount);
}

//===========================================================================//

TEST(StackCopyOperator)
{
	Stack<DummyCompoundObject> sourceStack = ContainersHelper::CreateTestDummyCompoundObjectStack();

	Stack<DummyCompoundObject> stack;
	stack = sourceStack;

	sourceStack.Clear();

	ARE_EQUAL(TestDummyesCount, stack.GetCount());
	ARE_EQUAL(TestDummyesCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, stack, TestDummyesCount);
}

//===========================================================================//

TEST(StackCopyOperatorSelf)
{
	Stack<DummyCompoundObject> stack = ContainersHelper::CreateTestDummyCompoundObjectStack();

	stack = stack;

	ARE_EQUAL(TestDummyesCount, stack.GetCount());
	ARE_EQUAL(TestDummyesCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, stack, TestDummyesCount);
}

//===========================================================================//

TEST(StackMoveOperator)
{
	Stack<DummyCompoundObject> stack;
	stack = ContainersHelper::CreateTestDummyCompoundObjectStack();

	ARE_EQUAL(TestDummyesCount, stack.GetCount());
	ARE_EQUAL(TestDummyesCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, stack, TestDummyesCount);
}

//===========================================================================//

TEST(StackReserve)
{
	integer const smallerCount = Math::Floor(TestDummyesCount / 2.0f);
	Stack<DummyCompoundObject> stack(smallerCount);

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(smallerCount, stack._reservedCount);

	integer const greaterCount = TestDummyesCount;

	stack.Reserve(greaterCount);

	for (integer i = 0; i < greaterCount; i++)
	{
		stack.Push(TestDummyes[i]);
	}

	ARE_EQUAL(greaterCount, stack._count);
	ARE_EQUAL(greaterCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, stack, greaterCount);

	stack.Reserve(smallerCount);

	ARE_EQUAL(smallerCount, stack._count);
	ARE_EQUAL(smallerCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, stack, smallerCount);

	stack.Reserve(0);

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(StackPush)
{
	Stack<DummyCompoundObject> stack;

	stack.Push(d1);

	const integer FirstExpectedArrayCount = 1;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d1 };

	ARE_EQUAL(FirstExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(1, stack._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, stack, FirstExpectedArrayCount);

	stack.Push(d2);

	const integer SecondExpectedArrayCount = 2;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d1, d2 };

	ARE_EQUAL(SecondExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(2, stack._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, stack, SecondExpectedArrayCount);

	stack.Push(d3);

	const integer ThirdExpectedArrayCount = 3;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d1, d2, d3 };

	ARE_EQUAL(ThirdExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(4, stack._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, stack, ThirdExpectedArrayCount);
}

//===========================================================================//

TEST(StackTopPop)
{
	Stack<DummyCompoundObject> stack;

	stack.Push(d1);
	stack.Push(d2);
	stack.Push(d3);
	stack.Push(d4);

	IS_TRUE(stack.Top() == d4);
	stack.Pop();

	const integer FirstExpectedStackCount = 3;
	const DummyCompoundObject FirstExpectedStack[FirstExpectedStackCount] = { d1, d2, d3 };

	ARE_EQUAL(FirstExpectedStackCount, stack.GetCount());
	ARE_EQUAL(4, stack._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedStack, stack, FirstExpectedStackCount);

	IS_TRUE(stack.Top() == d3);
	stack.Pop();

	const integer SecondExpectedStackCount = 2;
	const DummyCompoundObject SecondExpectedStack[SecondExpectedStackCount] = { d1, d2 };

	ARE_EQUAL(SecondExpectedStackCount, stack.GetCount());
	ARE_EQUAL(4, stack._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedStack, stack, SecondExpectedStackCount);

	IS_TRUE(stack.Top() == d2);
	stack.Pop();

	const integer ThirdExpectedStackCount = 1;
	const DummyCompoundObject ThirdExpectedStack[ThirdExpectedStackCount] = { d1 };

	ARE_EQUAL(ThirdExpectedStackCount, stack.GetCount());
	ARE_EQUAL(2, stack._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedStack, stack, ThirdExpectedStackCount);

	IS_TRUE(stack.Top() == d1);
	stack.Pop();

	ARE_EQUAL(0, stack.GetCount());
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(StackTopExceptions)
{
	Stack<DummyCompoundObject> emptyStack;
	IS_THROW(emptyStack.Top(), InvalidStateException);
}

//===========================================================================//

TEST(StackPopExceptions)
{
	Stack<DummyCompoundObject> emptyStack;
	IS_THROW(emptyStack.Pop(), InvalidStateException);

	Stack<DummyCompoundObject> stack;

	stack.Push(d1);
	stack.Pop();

	IS_THROW(stack.Pop(), InvalidStateException);
}

//===========================================================================//

TEST(StackGet)
{
	Stack<DummyCompoundObject> stack;
	integer expectedReservedCount = 0;

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		if (stack.GetCount() == expectedReservedCount)
		{
			expectedReservedCount += stack.GetDeltaGrowCount();
		}

		stack.Push(TestDummyes[i]);
	}

	ARE_EQUAL(TestDummyesCount, stack.GetCount());
	ARE_EQUAL(expectedReservedCount, stack._reservedCount);
	//Тут и провериться []
	ARE_ARRAYS_EQUAL(TestDummyes, stack, TestDummyesCount);
}

//===========================================================================//

TEST(StackGetExceptions)
{
	Stack<DummyCompoundObject> emptyStack;
	IS_THROW(emptyStack[-1], IndexOutOfBoundsException);
	IS_THROW(emptyStack[0], IndexOutOfBoundsException);

	Stack<DummyCompoundObject> stack(TestDummyesCount);
	IS_THROW(stack[-1], IndexOutOfBoundsException);
	IS_THROW(stack[TestDummyesCount], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(StackClear)
{
	Stack<DummyCompoundObject> stack = ContainersHelper::CreateTestDummyCompoundObjectStack();

	stack.Clear();

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(StackIsEmpty)
{
	Stack<DummyCompoundObject> stack;
	ARE_EQUAL(true, stack.IsEmpty());

	stack.Push(d1);
	ARE_EQUAL(false, stack.IsEmpty());
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: тесты умного указателя который при выходе из области видимости удаляет объект
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Dummies/DummyCompoundObject.h"
#include "../../Dummies/DummyBaseCounter.h"

namespace Bru
{

//===========================================================================//

TEST(ScopedPtrConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	ScopedPtr<DummyCompoundObject> scopedPtr(simplePtr);

	IS_TRUE(scopedPtr.object == simplePtr);
}

//===========================================================================//

TEST(ScopedPtrDestructor)
{
	DummyBaseCounter::Reset();
	{
		ScopedPtr<DummyBaseCounter> firstScopedPtr(new DummyBaseCounter());
		ScopedPtr<DummyBaseCounter> secondScopedPtr(new DummyBaseCounter());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(ScopedPtrConstructorWithNull)
{
	ScopedPtr<DummyCompoundObject> scopedPtr(NullPtr);

	IS_TRUE(scopedPtr.object == NullPtr);
}

//===========================================================================//

TEST(ScopedPtrOperatorDereference)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	ScopedPtr<DummyCompoundObject> scopedPtr(simplePtr);

	IS_TRUE(*scopedPtr == *simplePtr);
	ARE_EQUAL(13, (*scopedPtr).GetValue());
}

//===========================================================================//

TEST(ScopedPtrOperatorDereferenceExceptions)
{
	ScopedPtr<DummyCompoundObject> scopedNullPtr(NullPtr);

	IS_THROW((*scopedNullPtr).GetValue(), NullPointerException);

	ScopedPtr<DummyCompoundObject> scopedPtr(new DummyCompoundObject(13));
	scopedPtr.Release();

	IS_THROW((*scopedPtr).GetValue(), NullPointerException);
}

//===========================================================================//

TEST(ScopedPtrOperatorSelector)
{
	ScopedPtr<DummyCompoundObject> scopedPtr(new DummyCompoundObject(13));

	ARE_EQUAL(13, scopedPtr->GetValue());
}

//===========================================================================//

TEST(ScopedPtrOperatorSelectorExceptions)
{
	ScopedPtr<DummyCompoundObject> scopedNullPtr(NullPtr);

	IS_THROW(scopedNullPtr->GetValue(), NullPointerException);

	ScopedPtr<DummyCompoundObject> scopedPtr(new DummyCompoundObject(13));
	scopedPtr.Release();

	IS_THROW(scopedPtr->GetValue(), NullPointerException);
}

//===========================================================================//

TEST(ScopedPtrRelease)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	ScopedPtr<DummyCompoundObject> scopedPtr(simplePtr);

	IS_TRUE(scopedPtr.object == simplePtr);

	scopedPtr.Release();

	IS_TRUE(scopedPtr.object == NullPtr);
}

//===========================================================================//

}// namespace Bru

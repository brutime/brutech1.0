//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты AddingOrderedAddingOrderedTreeMapIterator
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorConstructor)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestAddingOrderedMap();
	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it(&map, 0);

	ARE_EQUAL(&map, it._owner);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorForEmpty)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> emptyMap;
	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it(&emptyMap, 0);

	integer count = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorForOnePair)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;
	map.Add(1, d1);
	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it(&map, 0);

	integer count = 0;
	integer cursorExpectedValue = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &map);
		ARE_EQUAL(cursorExpectedValue, it._cursor);
		IS_TRUE(it.GetCurrent() == map.FindNode(1)->Pair);
		ARE_EQUAL(1, it.GetCurrent().GetKey());
		ARE_EQUAL(d1, it.GetCurrent().GetValue());
		count++;
		cursorExpectedValue++;
	}

	ARE_EQUAL(1, count);
}

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorFor)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestAddingOrderedMap();
	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it(&map, 0);

	integer index = 0;
	integer cursorExpectedValue = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &map);
		ARE_EQUAL(cursorExpectedValue, it._cursor);
		IS_TRUE(it.GetCurrent() == map.FindNode(TreeMapExpectedKeys[index])->Pair);
		ARE_EQUAL(TreeMapExpectedKeys[index], it.GetCurrent().GetKey());
		ARE_EQUAL(TreeMapExpectedValues[index], it.GetCurrent().GetValue());
		index++;
		cursorExpectedValue++;
	}

	ARE_EQUAL(TreeMapElementsCount, index);
}

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorRangeForEmpty)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	integer count = 0;
	for (auto & pair : map)
	{
		pair.GetKey();
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorRangeForOnePair)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;
	map.Add(1, d1);

	integer count = 0;
	for (auto & pair : map)
	{
		ARE_EQUAL(1, pair.GetKey());
		ARE_EQUAL(d1, pair.GetValue());
		count++;
	}

	ARE_EQUAL(1, count);
}

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorRangeFor)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestAddingOrderedMap();

	integer index = 0;
	for (auto & pair : map)
	{
		ARE_EQUAL(TreeMapExpectedKeys[index], pair.GetKey());
		ARE_EQUAL(TreeMapExpectedValues[index], pair.GetValue());
		index++;
	}

	ARE_EQUAL(TreeMapElementsCount, index);
}

//===========================================================================//

TEST(AddingOrderedTreeMapIteratorExceptions)
{
	IS_THROW((AddingOrderedTreeMapIterator<integer, DummyCompoundObject>(NullPtr, 0)), InvalidArgumentException);

	AddingOrderedTreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestAddingOrderedMap();
	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it(&map, 0);

	it._cursor = -1;
	IS_THROW(it.GetCurrent().GetKey(), IteratorOutOfBoundsException);
	IS_THROW(it.GetCurrent().GetValue(), IteratorOutOfBoundsException);
	IS_THROW(it.GetCurrent(), IteratorOutOfBoundsException);

	it._cursor = AddingOrderedTreeMapElementsCount;
	IS_THROW(it.GetCurrent().GetKey(), IteratorOutOfBoundsException);
	IS_THROW(it.GetCurrent().GetValue(), IteratorOutOfBoundsException);
	IS_THROW(it.GetCurrent(), IteratorOutOfBoundsException);

	it.First();

	for (it.First(); it.HasNext(); it.Next())
	{
	}

	IS_THROW(it.Next(), IteratorOutOfBoundsException);
}

//===========================================================================//

}// namespace Bru


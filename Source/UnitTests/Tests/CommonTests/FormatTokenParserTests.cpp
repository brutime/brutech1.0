//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты парсера токенов формата 
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Common/Helpers/FormatTokenParser.h"

namespace Bru
{

//===========================================================================//

TEST(FormatTokenParserConstructor)
{
	FormatTokenParser parser(string::Empty);
	
    ARE_EQUAL(0, parser._index);
    ARE_EQUAL(string::Empty, parser._source);
}

//===========================================================================//

TEST(FormatTokenParserWithoutTokens1)
{
    FormatTokenParser parser(" , \n ;");
    ARE_EQUAL(-1, parser.NextToken().GetPosition());
}

//===========================================================================//

TEST(FormatTokenParserWithoutTokens2)
{
    FormatTokenParser parser("");
    ARE_EQUAL(-1, parser.NextToken().GetPosition());
}

//===========================================================================//

TEST(FormatTokenParserTest1)
{
    FormatTokenParser parser("{one}, {two};   {three}\n{four}");
    ARE_EQUAL("one", parser.GetString(parser.NextToken()));
    ARE_EQUAL("two", parser.GetString(parser.NextToken()));
    ARE_EQUAL("three", parser.GetString(parser.NextToken()));
    ARE_EQUAL("four", parser.GetString(parser.NextToken()));
	IS_NULL(parser.GetString(parser.NextToken()))
}

//===========================================================================//

TEST(FormatTokenParserTest2)
{
    FormatTokenParser parser("{}  {w}ord, 𠀀{}\nw; 42 й");

    ARE_EQUAL("", parser.NextTokenString());
    ARE_EQUAL("w", parser.NextTokenString());
    ARE_EQUAL("", parser.NextTokenString());
    IS_NULL(parser.NextTokenString());
}

//===========================================================================//

TEST(FormatTokenParserTest3)
{
    FormatTokenParser parser("{}  {w}ord, {𠀀}\nw; 42 й");

    ARE_EQUAL("", parser.NextTokenString());
    ARE_EQUAL("w", parser.NextTokenString());
    ARE_EQUAL("𠀀", parser.NextTokenString());
    IS_NULL(parser.NextTokenString());
}

//===========================================================================//

TEST(FormatTokenParserGetTokenOneWord)
{
    FormatTokenParser parser("{word}");

    ARE_EQUAL("word", parser.NextTokenString());
    IS_NULL(parser.NextTokenString());
}

//===========================================================================//

TEST(FormatTokenParserWithoutTokens)
{
    FormatTokenParser parser(" word ");

    IS_NULL(parser.NextTokenString());
}

//===========================================================================//

TEST(FormatTokenParserWithoutClosingBracket)
{
    FormatTokenParser parser("{word ");

    IS_THROW(parser.NextTokenString(), ParsingException);
}

//===========================================================================//

TEST(FormatTokenParserDoubleClosingBracket)
{
    FormatTokenParser parser("{word} hjsd } ");

    ARE_EQUAL("word", parser.NextTokenString());
    IS_THROW(parser.NextTokenString(), ParsingException);
}

//===========================================================================//

TEST(FormatTokenParserDoubleOpeningBrackets)
{
    FormatTokenParser parser("{{word} hjsd } ");

    IS_THROW(parser.NextTokenString(), ParsingException);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты конвертера строки
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Common/Types/String/StringConverter.h"
#include "../../Helpers/StringTestsHelper.h"
#include "../../Helpers/StringsTestData.h"

namespace Bru
{

//===========================================================================//

TEST(StringConverterUTF8ToUTF16)
{
	utf16_string resultString = StringConverter::UTF8ToUTF16(UTF8TestLongBytes);
	ARE_ARRAYS_EQUAL(UTF16TestLongWords, resultString.ToChars(), UTF16TestLongWordsCount + 1);
}

//===========================================================================//

TEST(StringConverterUTF8ToUTF16MaxLength)
{
	utf16_string resultString = StringConverter::UTF8ToUTF16(StringTestsHelper->GetMaxLengthChars());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthUTF16Chars(), resultString.ToChars(), string::MaxLength + 1);
}

//===========================================================================//

TEST(StringConverterUTF8ToUTF32)
{
	utf32_string resultString = StringConverter::UTF8ToUTF32(UTF8TestLongBytes);
	ARE_ARRAYS_EQUAL(UTF32TestLongChars, resultString.ToChars(), UTF32TestLongCharsCount + 1);
}

//===========================================================================//

TEST(StringConverterUTF8ToUTF32MaxLength)
{
	utf32_string resultString = StringConverter::UTF8ToUTF32(StringTestsHelper->GetMaxLengthChars());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthUTF32Chars(), resultString.ToChars(), string::MaxLength + 1);
}

//===========================================================================//

#ifdef DEBUGGING_STRINGS
TEST(StringConverterUTF8ToUTF16AndUTF8ToUTF32Exceptions)
{
	IS_THROW(StringConverter::UTF8ToUTF16(UTF8TestInvalidBytes), InvalidUTF8SequenceException);
	IS_THROW(StringConverter::UTF8ToUTF32(UTF8TestInvalidBytes), InvalidUTF8SequenceException);
}
#endif // DEBUGGING_STRINGS
//===========================================================================//

TEST(StringConverterUTF16ToUTF8)
{
	string resultString = StringConverter::UTF16ToUTF8(UTF16TestLongWords);
	ARE_ARRAYS_EQUAL(UTF8TestLongBytes, resultString.ToChars(), UTF8TestLongBytesCount + 1);
}

//===========================================================================//

TEST(StringConverterUTF16ToUTF8MaxLength)
{
	string resultString = StringConverter::UTF16ToUTF8(StringTestsHelper->GetMaxLengthUTF16Chars());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthChars(), resultString.ToChars(), string::MaxLength + 1);
}

//===========================================================================//

TEST(StringConverterUTF16ToUTF32)
{
	utf32_string resultString = StringConverter::UTF16ToUTF32(UTF16TestLongWords);
	ARE_ARRAYS_EQUAL(UTF32TestLongChars, resultString.ToChars(), UTF32TestLongCharsCount + 1);
}

//===========================================================================//

TEST(StringConverterUTF16ToUTF32MaxLength)
{
	utf32_string resultString = StringConverter::UTF16ToUTF32(StringTestsHelper->GetMaxLengthUTF16Chars());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthUTF32Chars(), resultString.ToChars(), string::MaxLength + 1);
}

//===========================================================================//

TEST(StringConverterUTF16ToUTF8AndUTF16ToUTF32Exception)
{
#ifdef DEBUGGING_STRINGS
	IS_THROW(StringConverter::UTF16ToUTF8(UTF16TestInvalidWords), InvalidUTF16SequenceException);
	IS_THROW(StringConverter::UTF16ToUTF32(UTF16TestInvalidWords), InvalidUTF16SequenceException);
#endif // DEBUGGING_STRINGS
}

//===========================================================================//

TEST(StringConverterUTF32ToUTF8)
{
	string resultString = StringConverter::UTF32ToUTF8(UTF32TestLongChars);
	ARE_ARRAYS_EQUAL(UTF8TestLongBytes, resultString.ToChars(), UTF8TestLongBytesCount + 1);
}

//===========================================================================//

TEST(StringConverterUTF32ToUTF8MaxLength)
{
	string resultString = StringConverter::UTF32ToUTF8(StringTestsHelper->GetMaxLengthUTF32Chars());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthChars(), resultString.ToChars(), string::MaxLength + 1);
}

//===========================================================================//

#ifdef DEBUGGING_STRINGS
TEST(StringConverterUTF32ToUTF8Exception)
{
	IS_THROW(StringConverter::UTF32ToUTF8(UTF32TestInvalidChars), InvalidUTF32SequenceException);
}
#endif // DEBUGGING_STRINGS
//===========================================================================//

TEST(StringConverterUTF32ToUTF16)
{
	utf16_string resultString = StringConverter::UTF32ToUTF16(UTF32TestLongChars);
	ARE_ARRAYS_EQUAL(UTF16TestLongWords, resultString.ToChars(), UTF16TestLongWordsCount);
}

//===========================================================================//

TEST(StringConverterUTF32ToUTF16MaxLength)
{
	utf16_string resultString = StringConverter::UTF32ToUTF16(StringTestsHelper->GetMaxLengthUTF32Chars());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthUTF16Chars(), resultString.ToChars(), string::MaxLength + 1);
}

//===========================================================================//

#ifdef DEBUGGING_STRINGS
TEST(StringConverterUTF32ToUTF16Exception)
{
	IS_THROW(StringConverter::UTF32ToUTF16(UTF32TestInvalidChars), InvalidUTF32SequenceException);
}
#endif // DEBUGGING_STRINGS
//===========================================================================//

}// namespace Bru

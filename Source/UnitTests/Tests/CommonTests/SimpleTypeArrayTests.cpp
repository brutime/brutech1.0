//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты массива хранящего простые типы
//              Проверки Add(const T &), Insert(const T &) проходят в ArrayTests
//              Проверки Add(T &&), Insert(T &&) проходят в SimplyTypeArrayTests
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(SimpleTypeArrayGetters)
{
	Array<float>::SimpleType::SimpleType array;

	array._count = 10;

	ARE_EQUAL(10, array.GetCount());
}

//===========================================================================//

TEST(SimpleTypeArrayDefaultConstructor)
{
	Array<float>::SimpleType::SimpleType array;

	ARE_EQUAL(0, array._count);
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeArrayConstructor)
{
	Array<float>::SimpleType array(10);

	ARE_EQUAL(10, array._count);
	ARE_EQUAL(10, array._reservedCount);
	IS_TRUE(array._dataPtr != NullPtr);
}

//===========================================================================//

TEST(SimpleTypeArrayConstructorExceptions)
{
	IS_THROW(Array<float>::SimpleType array(-1), InvalidArgumentException);
}

//===========================================================================//

TEST(SimpleTypeArrayInitializerListConstructor)
{
	const integer ExpectedArrayCount = 10;
	const DummyCompoundObject ExpectedArray[ExpectedArrayCount] = { 0.0f, 1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f,
	        8.8f, 9.9f
	                                                              };

	Array<float>::SimpleType array = { 0.0f, 1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f, 8.8f, 9.9f };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(ExpectedArrayCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(ExpectedArray, array, ExpectedArrayCount);
}

//===========================================================================//

TEST(SimpleTypeArrayCopyConstructor)
{
	Array<float>::SimpleType sourceArray = ContainersHelper::CreateTestFloatsArray();

	Array<float>::SimpleType array(sourceArray);

	sourceArray.Clear();

	ARE_EQUAL(TestFloatsCount, array.GetCount());
	ARE_EQUAL(TestFloatsCount, array._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, array, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeArrayCopyOperator)
{
	Array<float>::SimpleType sourceArray = ContainersHelper::CreateTestFloatsArray();

	Array<float>::SimpleType array;
	array = sourceArray;

	sourceArray.Clear();

	ARE_EQUAL(TestFloatsCount, array.GetCount());
	ARE_EQUAL(TestFloatsCount, array._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, array, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeArrayCopyOperatorSelf)
{
	Array<float>::SimpleType array = ContainersHelper::CreateTestFloatsArray();

	array = array;

	ARE_EQUAL(TestFloatsCount, array.GetCount());
	ARE_EQUAL(TestFloatsCount, array._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, array, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeArrayResize)
{
	integer const InitSize = Math::Floor(TestFloatsCount / 2.0f);
	Array<float>::SimpleType array(InitSize);

	integer const GreaterSize = TestFloatsCount;

	array.Resize(GreaterSize);

	for (integer i = 0; i < GreaterSize; i++)
	{
		array[i] = TestFloats[i];
	}

	ARE_EQUAL(GreaterSize, array._count);
	ARE_EQUAL(GreaterSize, array._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, array, GreaterSize, Math::Accuracy);

	integer const SmallerSize = Math::Floor(TestFloatsCount / 2.0f);

	array.Resize(SmallerSize);

	for (integer i = 0; i < SmallerSize; i++)
	{
		array[i] = TestFloats[i];
	}

	ARE_EQUAL(SmallerSize, array._count);
	ARE_EQUAL(SmallerSize, array._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, array, SmallerSize, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeArrayAdd)
{
	Array<byte>::SimpleType array;

	array.Add(1);

	const integer FirstExpectedArrayCount = 1;
	const integer FirstExpectedArray[FirstExpectedArrayCount] = { 1 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(1, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Add(2);

	const integer SecondExpectedArrayCount = 2;
	const integer SecondExpectedArray[SecondExpectedArrayCount] = { 1, 2 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Add(3);

	const integer ThirdExpectedArrayCount = 3;
	const integer ThirdExpectedArray[ThirdExpectedArrayCount] = { 1, 2, 3 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.Add(4);

	const integer FourthExpectedArrayCount = 4;
	const DummyCompoundObject FourthExpectedArray[FourthExpectedArrayCount] = { 1, 2, 3, 4 };

	ARE_EQUAL(FourthExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FourthExpectedArray, array, FourthExpectedArrayCount);
}

//===========================================================================//

TEST(SimpleTypeArrayAddOtherArray)
{
	Array<integer>::SimpleType sourceArray;

	sourceArray.Add(1);
	sourceArray.Add(2);
	sourceArray.Add(3);

	Array<integer>::SimpleType array;

	array.Add(sourceArray);

	const integer FirstExpectedArrayCount = 3;
	const integer FirstExpectedArray[FirstExpectedArrayCount] = { 1, 2, 3 };

	ARE_EQUAL(FirstExpectedArrayCount, array._count);
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Add(sourceArray);

	const integer SecondExpectedArrayCount = 6;
	const integer SecondExpectedArray[SecondExpectedArrayCount] = { 1, 2, 3, 1, 2, 3 };

	ARE_EQUAL(SecondExpectedArrayCount, array._count);
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	Array<integer>::SimpleType emptyArray;
	array.Add(emptyArray);

	ARE_EQUAL(SecondExpectedArrayCount, array._count);
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);
}

//===========================================================================//

TEST(SimpleTypeArrayAddOtherArrayExceptions)
{
	Array<integer>::SimpleType array;
	IS_THROW(array.Add(array), InvalidArgumentException);
}

//===========================================================================//

TEST(SimpleTypeArrayInsert)
{
	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);
	array.Add(3);

	array.Insert(0, 4);

	const integer FirstExpectedArrayCount = 4;
	const integer FirstExpectedArray[FirstExpectedArrayCount] = { 4, 1, 2, 3 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Insert(2, 5);

	const integer SecondExpectedArrayCount = 5;
	const integer SecondExpectedArray[SecondExpectedArrayCount] = { 4, 1, 5, 2, 3 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Insert(5, 6);

	const integer ThirdExpectedArrayCount = 6;
	const integer ThirdExpectedArray[ThirdExpectedArrayCount] = { 4, 1, 5, 2, 3, 6 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(SimpleArrayInsertExceptions)
{
	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.Insert(-1, 1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Insert(1, 1), IndexOutOfBoundsException);
}
#endif

//===========================================================================//

TEST(SimpleTypeArrayInsertOtherArray)
{
	Array<integer>::SimpleType sourceArray;

	sourceArray.Add(1);
	sourceArray.Add(2);
	sourceArray.Add(3);

	Array<integer>::SimpleType array;

	//Пустой массив
	array.Insert(0, Array<integer>::SimpleType());

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);

	array.Insert(0, sourceArray);

	const integer FirstExpectedArrayCount = 3;
	const integer FirstExpectedArray[FirstExpectedArrayCount] = { 1, 2, 3 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Insert(1, sourceArray);

	const integer SecondExpectedArrayCount = 6;
	const integer SecondExpectedArray[SecondExpectedArrayCount] = { 1, 1, 2, 3, 2, 3 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Insert(6, sourceArray);

	const integer ThirdExpectedArrayCount = 9;
	const integer ThirdExpectedArray[ThirdExpectedArrayCount] = { 1, 1, 2, 3, 2, 3, 1, 2, 3 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(16, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(SimpleTypeArrayInsertOtherArrayExceptions)
{
	Array<integer>::SimpleType array = ContainersHelper::CreateTestIntegersArray();
	IS_THROW(array.Insert(0, array), InvalidArgumentException);

	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.Insert(-1, array), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Insert(1, array), IndexOutOfBoundsException);
}
#endif

//===========================================================================//

TEST(SimpleTypeArrayRemoveAt)
{
	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);
	array.Add(3);
	array.Add(4);

	array.RemoveAt(2);

	const integer FirstExpectedArrayCount = 3;
	const integer FirstExpectedArray[FirstExpectedArrayCount] = { 1, 2, 4 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.RemoveAt(0);

	const integer SecondExpectedArrayCount = 2;
	const integer SecondExpectedArray[SecondExpectedArrayCount] = { 2, 4 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.RemoveAt(1);

	const integer ThirdExpectedArrayCount = 1;
	const integer ThirdExpectedArray[ThirdExpectedArrayCount] = { 2 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.RemoveAt(0);

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeArrayRemoveAtExceptions)
{
	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.RemoveAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(0), IndexOutOfBoundsException);

	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);

	IS_THROW(emptyArray.RemoveAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(2), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(SimpleTypeArrayRemoveAtIndexCount)
{
	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);
	array.Add(3);
	array.Add(4);
	array.Add(5);
	array.Add(6);
	array.Add(7);

	array.RemoveAt(0, 2);

	const integer FirstExpectedArrayCount = 5;
	const integer FirstExpectedArray[FirstExpectedArrayCount] = { 3, 4, 5, 6, 7 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.RemoveAt(1, 2);

	const integer SecondExpectedArrayCount = 3;
	const integer SecondExpectedArray[SecondExpectedArrayCount] = { 3, 6, 7 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.RemoveAt(1, 2);

	const integer ThirdExpectedArrayCount = 1;
	const integer ThirdExpectedArray[ThirdExpectedArrayCount] = { 3 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.RemoveAt(0, 1);

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeArrayRemoveAtIndexCountExceptions)
{
	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.RemoveAt(-1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(-1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(-1, 1), IndexOutOfBoundsException);

	IS_THROW(emptyArray.RemoveAt(0, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(0, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(0, 1), IndexOutOfBoundsException);

	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);

	IS_THROW(array.RemoveAt(-1, -1), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(-1, 0), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(-1, 1), IndexOutOfBoundsException);

	IS_THROW(array.RemoveAt(2, -1), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(2, 0), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(2, 1), IndexOutOfBoundsException);

	IS_THROW(array.RemoveAt(0, -1), InvalidArgumentException);
	IS_THROW(array.RemoveAt(0, 3), InvalidArgumentException);
}

//===========================================================================//

TEST(SimpleTypeArrayRemove)
{
	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);
	array.Add(3);
	array.Add(4);

	array.Remove(3);

	const integer FirstExpectedArrayCount = 3;
	const integer FirstExpectedArray[FirstExpectedArrayCount] = { 1, 2, 4 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Remove(1);

	const integer SecondExpectedArrayCount = 2;
	const integer SecondExpectedArray[SecondExpectedArrayCount] = { 2, 4 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Remove(4);

	const integer ThirdExpectedArrayCount = 1;
	const integer ThirdExpectedArray[ThirdExpectedArrayCount] = { 2 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.Remove(2);

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeArrayRemoveExceptions)
{
	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.Remove(666), InvalidArgumentException);

	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);

	IS_THROW(array.Remove(666), InvalidArgumentException);
}

//===========================================================================//

TEST(SimpleTypeArrayClear)
{
	Array<float>::SimpleType array = ContainersHelper::CreateTestFloatsArray();

	array.Clear();

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeArrayReset)
{
	Array<integer>::SimpleType array(2);

	array[0] = 1;
	array[1] = 2;

	array.Reset();

	ARE_EQUAL(2, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
}

//===========================================================================//

TEST(SimpleTypeArrayResetCount)
{
	integer const InitSize = 2;
	Array<integer>::SimpleType array(InitSize);
	array[0] = 1;
	array[1] = 2;

	integer const GreaterSize = 4;
	array.Reset(GreaterSize);

	ARE_EQUAL(4, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);

	array[0] = 3;
	array[1] = 4;

	integer const SmallerSize = 3;
	array.Reset(SmallerSize);

	ARE_EQUAL(3, array.GetCount());
	ARE_EQUAL(3, array._reservedCount);
}

//===========================================================================//

TEST(SimpleTypeArrayContains)
{
	Array<integer>::SimpleType array = ContainersHelper::CreateTestIntegersArray();

	IS_TRUE(array.Contains(TestIntegers[0]));
	IS_TRUE(array.Contains(TestIntegers[(TestIntegersCount - 1) / 2]));
	IS_TRUE(array.Contains(TestIntegers[TestIntegersCount - 1]));
	IS_TRUE(!array.Contains(integer(TestIntegersCount + 666)));
}

//===========================================================================//

TEST(SimpleTypeArrayIndexOf)
{
	Array<integer>::SimpleType array = ContainersHelper::CreateTestIntegersArray();

	for (integer i = 0; i < TestIntegersCount; i++)
	{
		ARE_EQUAL(i, array.IndexOf(TestIntegers[i]));
	}

	ARE_EQUAL(-1, array.IndexOf(integer(TestIntegersCount + 666)));
}

//===========================================================================//

TEST(SimpleTypeArraySort)
{
	const integer ArraySize = 100;
	Array<integer>::SimpleType array(ArraySize);

	for (integer i = 0; i < ArraySize; i++)
	{
		array[i] = Math::Random(ArraySize);
	}

	array.Sort();

	for (integer i = 0; i < ArraySize - 1; i++)
	{
		IS_TRUE(array[i] <= array[i + 1]);
	}
}

//===========================================================================//

TEST(SimpleTypeArrayOperatorSet)
{
	Array<float>::SimpleType array(TestFloatsCount);

	for (integer i = 0; i < TestFloatsCount; i++)
	{
		array[i] = TestFloats[i];
	}

	ARE_EQUAL(TestFloatsCount, array.GetCount());
	ARE_EQUAL(TestFloatsCount, array._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, array, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeArrayOperatorSetOverwrite)
{
	Array<integer>::SimpleType array(1);

	array[0] = 1;
	ARE_EQUAL(1, array.GetCount());
	ARE_EQUAL(1, array._reservedCount);
	ARE_EQUAL(1, 1);

	array[0] = 2;
	ARE_EQUAL(1, array.GetCount());
	ARE_EQUAL(1, array._reservedCount);
	ARE_EQUAL(2, 2);
}

//===========================================================================//

TEST(SimpleTypeArrayOperatorSetExceptions)
{
	Array<float>::SimpleType emptyArray;
	IS_THROW(emptyArray[-1] = 11.0f, IndexOutOfBoundsException);
	IS_THROW(emptyArray[0] = 11.0f, IndexOutOfBoundsException);

	Array<float>::SimpleType array(TestFloatsCount);
	IS_THROW(array[-1] = 11.0f, IndexOutOfBoundsException);
	IS_THROW(array[TestFloatsCount] = 11.0f, IndexOutOfBoundsException);
}

//===========================================================================//

TEST(SimpleTypeArrayOperatorGet)
{
	const Array<float>::SimpleType array = ContainersHelper::CreateTestFloatsArray();

	ARE_EQUAL(TestFloatsCount, array.GetCount());
	ARE_EQUAL(TestFloatsCount, array._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, array, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeArrayOperatorGetExceptions)
{
	const Array<float>::SimpleType emptyArray;
	IS_THROW(emptyArray[-1], IndexOutOfBoundsException);
	IS_THROW(emptyArray[0], IndexOutOfBoundsException);

	const Array<float>::SimpleType array(TestFloatsCount);
	IS_THROW(array[-1], IndexOutOfBoundsException);
	IS_THROW(array[TestFloatsCount], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(SimpleTypeArraySetArray)
{
	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(2);
	array.Add(3);
	array.Add(4);
	array.Add(5);
	array.Add(6);
	array.Add(7);

	Array<integer>::SimpleType otherArray;

	otherArray.Add(11);
	otherArray.Add(22);

	array.SetArray(0, otherArray);

	const integer ExpectedArrayCount = 7;
	const integer FirstExpectedArray[ExpectedArrayCount] = { 11, 22, 3, 4, 5, 6, 7 };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, ExpectedArrayCount);

	array.SetArray(5, otherArray);

	const integer SecondExpectedArray[ExpectedArrayCount] = { 11, 22, 3, 4, 5, 11, 22 };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, ExpectedArrayCount);

	array.SetArray(3, otherArray);

	const integer ThirdExpectedArray[ExpectedArrayCount] = { 11, 22, 3, 11, 22, 11, 22 };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ExpectedArrayCount);
}

//===========================================================================//

TEST(SimpleTypeArraySetArrayExceptions)
{
	Array<integer>::SimpleType sourceArray = ContainersHelper::CreateTestIntegersArray();

	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.SetArray(-1, sourceArray), IndexOutOfBoundsException);
	IS_THROW(emptyArray.SetArray(0, sourceArray), IndexOutOfBoundsException);

	IS_THROW(sourceArray.SetArray(0, sourceArray), InvalidArgumentException);

	Array<integer>::SimpleType array(TestIntegersCount - 1);
	IS_THROW(array.SetArray(0, sourceArray), InvalidArgumentException);
}

//===========================================================================//

TEST(SimpleTypeArrayGetArray)
{
	Array<integer>::SimpleType array = ContainersHelper::CreateTestIntegersArray();

	Array<integer>::SimpleType emptySubArray = array.GetArray(0, 0);
	ARE_EQUAL(0, emptySubArray.GetCount());
	ARE_EQUAL(0, emptySubArray._reservedCount);

	Array<integer>::SimpleType beginSubArray = array.GetArray(0, TestIntegersCount / 2);

	ARE_EQUAL(TestIntegersCount / 2, beginSubArray.GetCount());
	ARE_EQUAL(TestIntegersCount / 2, beginSubArray._reservedCount);
	ARE_ARRAYS_EQUAL(TestIntegers, beginSubArray, TestIntegersCount / 2);

	Array<integer>::SimpleType middleSubArray = array.GetArray(TestIntegersCount / 4, TestIntegersCount / 2);

	ARE_EQUAL(TestIntegersCount / 2, middleSubArray.GetCount());
	ARE_EQUAL(TestIntegersCount / 2, middleSubArray._reservedCount);
	ARE_ARRAYS_EQUAL(TestIntegers + TestIntegersCount / 4, middleSubArray, TestIntegersCount / 2);

	Array<integer>::SimpleType endSubArray = array.GetArray(TestIntegersCount / 2, TestIntegersCount / 2);

	ARE_EQUAL(TestIntegersCount / 2, endSubArray.GetCount());
	ARE_EQUAL(TestIntegersCount / 2, endSubArray._reservedCount);
	ARE_ARRAYS_EQUAL(TestIntegers + TestIntegersCount / 2, endSubArray, TestIntegersCount / 2);
}

//===========================================================================//

TEST(SimpleTypeArrayGetArrayExceptions)
{
	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.GetArray(-1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(-1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(-1, 1), IndexOutOfBoundsException);

	IS_THROW(emptyArray.GetArray(0, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(0, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(0, 1), IndexOutOfBoundsException);

	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(1);

	IS_THROW(array.GetArray(-1, -1), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(-1, 0), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(-1, 1), IndexOutOfBoundsException);

	IS_THROW(array.GetArray(2, -1), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(2, 0), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(2, 1), IndexOutOfBoundsException);

	IS_THROW(array.GetArray(0, -1), InvalidArgumentException);
	IS_THROW(array.GetArray(0, 3), InvalidArgumentException);
}

//===========================================================================//

TEST(SimpleTypeArrayFill)
{
	const integer arraySize = 10;
	Array<integer>::SimpleType array(arraySize);
	array.Fill(666);

	ARE_EQUAL(arraySize, array.GetCount());
	ARE_EQUAL(arraySize, array._reservedCount);
	for (integer i = 0; i < arraySize; i++)
	{
		ARE_EQUAL(666, array[i]);
	}
}

//===========================================================================//

TEST(SimpleTypeArrayFillIndexCount)
{
	const integer arraySize = 10;
	Array<integer>::SimpleType array(arraySize);
	array.Fill(555);
	array.Fill(0, arraySize, 666);

	ARE_EQUAL(arraySize, array.GetCount());
	ARE_EQUAL(arraySize, array._reservedCount);
	for (integer i = 0; i < arraySize; i++)
	{
		ARE_EQUAL(666, array[i]);
	}

	array.Fill(555);
	array.Fill(0, arraySize / 2, 666);

	integer expectedArray[arraySize];

	for (integer i = 0; i < arraySize; i++)
	{
		expectedArray[i] = (i < arraySize / 2) ? 666 : 555;
	}

	ARE_EQUAL(arraySize, array.GetCount());
	ARE_EQUAL(arraySize, array._reservedCount);
	ARE_ARRAYS_EQUAL(expectedArray, array, arraySize);

	array.Fill(555);
	array.Fill(arraySize / 2, arraySize / 2, 666);

	for (integer i = 0; i < arraySize; i++)
	{
		expectedArray[i] = (i >= arraySize / 2) ? 666 : 555;
	}

	ARE_EQUAL(arraySize, array.GetCount());
	ARE_EQUAL(arraySize, array._reservedCount);
	ARE_ARRAYS_EQUAL(expectedArray, array, arraySize);

	array.Fill(555);
	array.Fill(arraySize / 2, arraySize / 4, 666);

	for (integer i = 0; i < arraySize; i++)
	{
		expectedArray[i] = ((i >= arraySize / 2) && (i < (arraySize / 2 + arraySize / 4))) ? 666 : 555;
	}

	ARE_EQUAL(arraySize, array.GetCount());
	ARE_EQUAL(arraySize, array._reservedCount);
	ARE_ARRAYS_EQUAL(expectedArray, array, arraySize);
}

//===========================================================================//

TEST(SimpleTypeArrayFillIndexCountExceptions)
{
	Array<integer>::SimpleType emptyArray;
	IS_THROW(emptyArray.Fill(-1, -1, 666), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Fill(-1, 0, 666), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Fill(-1, 1, 666), IndexOutOfBoundsException);

	IS_THROW(emptyArray.Fill(0, -1, 666), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Fill(0, 0, 666), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Fill(0, 1, 666), IndexOutOfBoundsException);

	Array<integer>::SimpleType array;

	array.Add(1);
	array.Add(1);

	IS_THROW(array.Fill(-1, -1, 666), IndexOutOfBoundsException);
	IS_THROW(array.Fill(-1, 0, 666), IndexOutOfBoundsException);
	IS_THROW(array.Fill(-1, 1, 666), IndexOutOfBoundsException);

	IS_THROW(array.Fill(2, -1, 666), IndexOutOfBoundsException);
	IS_THROW(array.Fill(2, 0, 666), IndexOutOfBoundsException);
	IS_THROW(array.Fill(2, 1, 666), IndexOutOfBoundsException);

	IS_THROW(array.Fill(0, -1, 666), InvalidArgumentException);
	IS_THROW(array.Fill(0, 3, 666), InvalidArgumentException);
}

//===========================================================================//

TEST(SimpleTypeArray2D)
{
	integer const ArraysCount = 50;
	integer const ElementsCount = 100;

	Array<Array<integer>::SimpleType>::SimpleType array(ArraysCount);

	for (integer i = 0; i < ArraysCount; i++)
	{
		array[i].Resize(ElementsCount);
	}

	ARE_EQUAL(ArraysCount, array.GetCount());

	for (integer i = 0; i < ArraysCount; i++)
	{
		ARE_EQUAL(ElementsCount, array[i].GetCount());
	}

	for (int i = 0; i < ArraysCount; i++)
	{
		for (int j = 0; j < ElementsCount; j++)
		{
			array[i][j] = i * j + i + j;
		}
	}

	for (int i = 0; i < ArraysCount; i++)
	{
		for (int j = 0; j < ElementsCount; j++)
		{
			IS_TRUE(array[i][j] == i * j + i + j);
		}
	}
}

//===========================================================================//

TEST(SimpleTypeArray2DExceptions)
{
	integer const ArraysCount = 50;
	integer const ElementsCount = 100;

	Array<Array<integer>::SimpleType>::SimpleType array(ArraysCount);

	for (integer i = 0; i < ArraysCount; i++)
	{
		array[i].Resize(ElementsCount);
	}

	IS_THROW(array[-1], IndexOutOfBoundsException);
	IS_THROW(array[ArraysCount], IndexOutOfBoundsException);
	IS_THROW(array[0][-1], IndexOutOfBoundsException);
	IS_THROW(array[0][ElementsCount], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(SimpleTypeArrayCreateIterator)
{
	Array<integer>::SimpleType array = ContainersHelper::CreateTestIntegersArray();

	ArrayIterator<integer>::SimpleType it = array.CreateIterator();

	IS_TRUE(it._owner == &array);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(SimpleTypeArrayBegin)
{
	Array<integer>::SimpleType array = ContainersHelper::CreateTestIntegersArray();

	ArrayIterator<integer>::SimpleType it = array.begin();

	IS_TRUE(it._owner == &array);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(SimpleTypeArrayEnd)
{
	Array<integer>::SimpleType array = ContainersHelper::CreateTestIntegersArray();

	ArrayIterator<integer>::SimpleType it = array.end();

	IS_TRUE(it._owner == &array);
	ARE_EQUAL(array.GetCount(), it._cursor);
}

//===========================================================================//

}// namespace Bru

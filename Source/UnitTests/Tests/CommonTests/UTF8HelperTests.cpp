//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF8 помощника
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringTestsHelper.h"
#include "../../Helpers/StringsTestData.h"

namespace Bru
{

//===========================================================================//

TEST(UTF8HelperGetLength)
{
	UTFLength nullTerminatorStringLength = UTF8Helper::GetLength(NullTerminator);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInChars);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInComponents);

	UTFLength emptyStringLength = UTF8Helper::GetLength("");
	ARE_EQUAL(0, emptyStringLength.LengthInChars);
	ARE_EQUAL(0, emptyStringLength.LengthInComponents);

	UTFLength stringLength = UTF8Helper::GetLength("termинатор");
	ARE_EQUAL(10, stringLength.LengthInChars);
	ARE_EQUAL(16, stringLength.LengthInComponents);

	ARE_EQUAL(string::MaxLength, UTF8Helper::GetLength(StringTestsHelper->GetMaxLengthChars()).LengthInComponents);
	ARE_EQUAL(string::MaxLength, UTF8Helper::GetLength(StringTestsHelper->GetMaxLengthChars()).LengthInChars);
}

//===========================================================================//

TEST(UTF8HelperGetLengthInterval)
{
	UTFLength nullTerminatorStringLength = UTF8Helper::GetLength(NullTerminator, 0, 0);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInChars);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInComponents);

	UTFLength nullCountStringLength = UTF8Helper::GetLength("B¥𠀀௵rΩ", 5, 0);
	ARE_EQUAL(0, nullCountStringLength.LengthInChars);
	ARE_EQUAL(0, nullCountStringLength.LengthInComponents);

	UTFLength beginStringLength = UTF8Helper::GetLength("B¥𠀀௵rΩ", 0, 4);
	ARE_EQUAL(4, beginStringLength.LengthInChars);
	ARE_EQUAL(10, beginStringLength.LengthInComponents);

	UTFLength middleStringLength = UTF8Helper::GetLength("B¥𠀀௵rΩ", 3, 3);
	ARE_EQUAL(3, middleStringLength.LengthInChars);
	ARE_EQUAL(8, middleStringLength.LengthInComponents);

	UTFLength endStringLength = UTF8Helper::GetLength("B¥𠀀௵rΩ", 7, 3);
	ARE_EQUAL(3, endStringLength.LengthInChars);
	ARE_EQUAL(7, endStringLength.LengthInComponents);

	UTFLength stringLength = UTF8Helper::GetLength("B¥𠀀௵rΩ", 0, -1);
	ARE_EQUAL(6, stringLength.LengthInChars);
	ARE_EQUAL(14, stringLength.LengthInComponents);
}

//===========================================================================//

TEST(UTF8HelperGetCharIndex)
{
	ARE_EQUAL(0, UTF8Helper::GetCharIndex("B¥𠀀௵rΩ", 14, 0));
	ARE_EQUAL(3, UTF8Helper::GetCharIndex("B¥𠀀௵rΩ", 14, 7));
	ARE_EQUAL(5, UTF8Helper::GetCharIndex("B¥𠀀௵rΩ", 14, 11));
}

//===========================================================================//

TEST(UTF8HelperGetByteIndex)
{
	ARE_EQUAL(0, UTF8Helper::GetByteIndex("B¥𠀀௵rΩ", 14, 0));
	ARE_EQUAL(7, UTF8Helper::GetByteIndex("B¥𠀀௵rΩ", 14, 3));
	ARE_EQUAL(11, UTF8Helper::GetByteIndex("B¥𠀀௵rΩ", 14, 5));

	ARE_EQUAL(0, UTF8Helper::GetByteIndex("B¥𠀀௵rΩ", 14, 0, 0, 0));
	ARE_EQUAL(7, UTF8Helper::GetByteIndex("B¥𠀀௵rΩ", 14, 3, 1, 1));
	ARE_EQUAL(11, UTF8Helper::GetByteIndex("B¥𠀀௵rΩ", 14, 5, 3, 7));
	ARE_EQUAL(7, UTF8Helper::GetByteIndex("B¥𠀀௵rΩ", 14, 3, 3, 7));
}

//===========================================================================//

TEST(UTF8HelperGetPreviousByteIndex)
{
	ARE_EQUAL(0, UTF8Helper::GetPreviousByteIndex("B¥𠀀௵rΩ", 0, 0, 0));
	ARE_EQUAL(0, UTF8Helper::GetPreviousByteIndex("B¥𠀀௵rΩ", 0, 3, 7));
	ARE_EQUAL(7, UTF8Helper::GetPreviousByteIndex("B¥𠀀௵rΩ", 3, 3, 7));
	ARE_EQUAL(7, UTF8Helper::GetPreviousByteIndex("B¥𠀀௵rΩ", 3, 5, 11));
}

//===========================================================================//

TEST(UTF8HelperGetLengthInChars)
{
	ARE_EQUAL(0, UTF8Helper::GetLengthInChars(NullTerminator, 0));
	ARE_EQUAL(0, UTF8Helper::GetLengthInChars("", 0));
	ARE_EQUAL(6, UTF8Helper::GetLengthInChars("B¥𠀀௵rΩ", 14));
}

//===========================================================================//

TEST(UTF8HelperGetCharLengthInBytes)
{
	for (integer signedChar = static_cast<integer>(UTF8Helper::FirstCodeInOneByteSequenceCode);
	        signedChar <= static_cast<integer>(UTF8Helper::LastCodeInOneByteSequenceCode); signedChar++)
	{
		ARE_EQUAL(1, UTF8Helper::GetCharLengthInBytes(static_cast<char>(signedChar)));
	}

	for (integer signedChar = static_cast<integer>(UTF8Helper::FirstCodeInTwoByteSequenceCode);
	        signedChar <= static_cast<integer>(UTF8Helper::LastCodeInTwoByteSequenceCode); signedChar++)
	{
		ARE_EQUAL(2, UTF8Helper::GetCharLengthInBytes(static_cast<char>(signedChar)));
	}

	for (integer signedChar = static_cast<integer>(UTF8Helper::FirstCodeInThreeByteSequenceCode);
	        signedChar <= static_cast<integer>(UTF8Helper::LastCodeInThreeByteSequenceCode); signedChar++)
	{
		ARE_EQUAL(3, UTF8Helper::GetCharLengthInBytes(static_cast<char>(signedChar)));
	}

	for (integer signedChar = static_cast<integer>(UTF8Helper::FirstCodeInFourByteSequenceCode);
	        signedChar <= static_cast<integer>(UTF8Helper::LastCodeInFourByteSequenceCode); signedChar++)
	{
		ARE_EQUAL(4, UTF8Helper::GetCharLengthInBytes(static_cast<char>(signedChar)));
	}
}

//===========================================================================//

TEST(UTF8HelperCheck)
{
	//Этот тест просто не должен вызывать исключений
	UTF8Helper::Check(NullTerminator);
	UTF8Helper::Check(UTF8TestLongBytes);
	UTF8Helper::Check(StringTestsHelper->GetMaxLengthChars());
}

//===========================================================================//

TEST(UTF8HelperCheckLength)
{
	//Этот тест просто не должен вызывать исключений
	UTF8Helper::Check(UTF8TestLongBytes, 1);
}

//===========================================================================//

TEST(UTF8HelperCheckOutOfSequenceException)
{
	const char lessThanLastCodeInUTF8SequenceChars[2] =
	{
		static_cast<char>(UTF8Helper::FirstCodeInUTF8Sequence - 1),
		NullTerminator
	};

	IS_THROW(UTF8Helper::Check(lessThanLastCodeInUTF8SequenceChars), InvalidUTF8SequenceException);

	static const integer GreaterThanLastCodeInUTF8SequenceCharsCount = 4;
	static const char GreaterThanLastCodeInUTF8SequenceChars[GreaterThanLastCodeInUTF8SequenceCharsCount][5] =
	{
		{
			static_cast<char>(UTF8Helper::LastCodeInUTF8Sequence[0] + 1), UTF8Helper::LastCodeInUTF8Sequence[1],
			UTF8Helper::LastCodeInUTF8Sequence[2], UTF8Helper::LastCodeInUTF8Sequence[3],
			NullTerminator
		},

		{
			UTF8Helper::LastCodeInUTF8Sequence[0], static_cast<char>(UTF8Helper::LastCodeInUTF8Sequence[1] + 1),
			UTF8Helper::LastCodeInUTF8Sequence[2], UTF8Helper::LastCodeInUTF8Sequence[3],
			NullTerminator
		},

		{
			UTF8Helper::LastCodeInUTF8Sequence[0], UTF8Helper::LastCodeInUTF8Sequence[1],
			static_cast<char>(UTF8Helper::LastCodeInUTF8Sequence[2] + 1), UTF8Helper::LastCodeInUTF8Sequence[3],
			NullTerminator
		},

		{
			UTF8Helper::LastCodeInUTF8Sequence[0], UTF8Helper::LastCodeInUTF8Sequence[1],
			UTF8Helper::LastCodeInUTF8Sequence[2], static_cast<char>(UTF8Helper::LastCodeInUTF8Sequence[3] + 1),
			NullTerminator
		}
	};

	for (integer i = 0; i < GreaterThanLastCodeInUTF8SequenceCharsCount; i++)
	{
		IS_THROW(UTF8Helper::Check(GreaterThanLastCodeInUTF8SequenceChars[0]), InvalidUTF8SequenceException);
	}
}

//===========================================================================//

TEST(UTF8HelperCheckNoncharsExceptions)
{
	static const integer SourceNoncharSurrogatesCharsCount = 32;

	static const char SourceNoncharSurrogatesChars[SourceNoncharSurrogatesCharsCount][4] =
	{
		{ -16, -97, -65, -66 }, { -16, -97, -65, -65 }, { -16, -81, -65, -66 }, { -16, -81, -65, -65 },
		{ -16, -65, -65, -66 }, { -16, -65, -65, -65 }, { -15, -113, -65, -66 }, { -15, -113, -65, -65 },
		{ -15, -97, -65, -66 }, { -15, -97, -65, -65 }, { -15, -81, -65, -66 }, { -15, -81, -65, -65 },
		{ -15, -65, -65, -66 }, { -15, -65, -65, -65 }, { -14, -113, -65, -66 }, { -14, -113, -65, -65 },
		{ -14, -97, -65, -66 }, { -14, -97, -65, -65 }, { -14, -81, -65, -66 }, { -14, -81, -65, -65 },
		{ -14, -65, -65, -66 }, { -14, -65, -65, -65}, { -13, -113, -65, -66 }, { -13, -113, -65, -65 },
		{ -13, -97, -65, -66 }, { -13, -97, -65, -65 }, { -13, -81, -65, -66 }, { -13, -81, -65, -65 },
		{ -13, -65, -65, -66 }, { -13, -65, -65, -65}, { -12, -113, -65, -66 }, { -12, -113, -65, -65 }
	};

	for (integer i = 0; i < SourceNoncharSurrogatesCharsCount; i++)
	{
		char noncharSurrogatesChars[5] =
		{
			SourceNoncharSurrogatesChars[i][0], SourceNoncharSurrogatesChars[i][1],
			SourceNoncharSurrogatesChars[i][2], SourceNoncharSurrogatesChars[i][3],
			NullTerminator
		};
		IS_THROW(UTF8Helper::Check(noncharSurrogatesChars), InvalidUTF8SequenceException);
	}
}

//===========================================================================//

TEST(UTF8HelperIsFirstByte)
{
	const char * someChar = "𠀀";
	ARE_EQUAL(true, UTF8Helper::IsFirstByte(someChar[0]));
	ARE_EQUAL(false, UTF8Helper::IsFirstByte(someChar[1]));
	ARE_EQUAL(false, UTF8Helper::IsFirstByte(someChar[2]));
	ARE_EQUAL(false, UTF8Helper::IsFirstByte(someChar[3]));
}

//===========================================================================//

}// namespace Bru

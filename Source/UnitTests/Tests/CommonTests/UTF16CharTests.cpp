//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF16Char
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringsTestData.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

TEST(UTF16CharDefaultConstructor)
{
	utf16_char someChar;

	ARE_EQUAL(0, someChar._lengthInWords);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[0]);
}

//===========================================================================//

TEST(UTF16CharConstructorFromOneWordChar)
{
	utf16_char someChar = UTF16TestOneWordChar;

	ARE_EQUAL(1, someChar._lengthInWords);
	ARE_EQUAL(0x0042, someChar._dataPtr[0]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[1]);
}

//===========================================================================//

TEST(UTF16CharConstructorFromTwoWordsChar)
{
	utf16_char someChar = UTF16TestTwoWordsChar;

	ARE_EQUAL(2, someChar._lengthInWords);
	ARE_EQUAL(0xD840, someChar._dataPtr[0]);
	ARE_EQUAL(0xDC00, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF16CharConstructorFromOtherUTF16Char)
{
	utf16_char sourceChar = UTF16TestTwoWordsChar;
	utf16_char someChar(sourceChar);

	ARE_EQUAL(2, someChar._lengthInWords);
	ARE_EQUAL(0xD840, someChar._dataPtr[0]);
	ARE_EQUAL(0xDC00, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF16CharAssingOneWordChar)
{
	utf16_char someChar;
	someChar = UTF16TestOneWordChar;

	ARE_EQUAL(1, someChar._lengthInWords);
	ARE_EQUAL(0x0042, someChar._dataPtr[0]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[1]);
}

//===========================================================================//

TEST(UTF16CharAssingTwoWordsChar)
{
	utf16_char someChar;
	someChar = UTF16TestTwoWordsChar;

	ARE_EQUAL(2, someChar._lengthInWords);
	ARE_EQUAL(0xD840, someChar._dataPtr[0]);
	ARE_EQUAL(0xDC00, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF16CharOperatorOtherUTF16Char)
{
	utf16_char sourceChar = UTF16TestTwoWordsChar;
	utf16_char someChar(sourceChar);

	someChar = sourceChar;

	ARE_EQUAL(2, someChar._lengthInWords);
	ARE_EQUAL(0xD840, someChar._dataPtr[0]);
	ARE_EQUAL(0xDC00, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF16CharOperatorEqualsToChars)
{
	utf16_char someChar = UTF16TestOneWordChar;

	IS_TRUE(someChar == UTF16TestOneWordChar);
	//Не совпадает длина по количество слов
	IS_TRUE(someChar != UTF16TestTwoWordsChar);
	//Не совпадают по значению
	IS_TRUE(someChar != UTF16TestOneWordGreaterChar);
}

//===========================================================================//

TEST(UTF16CharOperatorEqualsToUTF16Char)
{
	utf16_char someChar = UTF16TestOneWordChar;
	utf16_char equalsChar = UTF16TestOneWordChar;
	utf16_char notEqualsByWordsChar = UTF16TestTwoWordsChar;
	utf16_char notEqualsChar = UTF16TestOneWordGreaterChar;

	IS_TRUE(someChar == equalsChar);
	IS_TRUE(someChar != notEqualsByWordsChar);
	IS_TRUE(someChar != notEqualsChar);
}

//===========================================================================//

TEST(UTF16CharToUnsignedShort)
{
	utf16_char someChar = UTF16TestTwoWordsChar;

	ARE_EQUAL(1, someChar.GetLength());
	ARE_EQUAL(2, someChar.GetLengthInWords());
	ARE_ARRAYS_EQUAL(UTF16TestTwoWordsChar, static_cast<const ushort *>(someChar), 3);
}

//===========================================================================//

}// namespace Bru

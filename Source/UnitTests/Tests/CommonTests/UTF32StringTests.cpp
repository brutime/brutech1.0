//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF32 строки
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringTestsHelper.h"
#include "../../Helpers/StringsTestData.h"

namespace Bru
{

//===========================================================================//

TEST(UTF32StringConstructorFromEmpty)
{
	utf32_string someString(utf32_string::Empty);

	ARE_EQUAL(0, someString.GetLengthInChars());
	ARE_EQUAL(static_cast<utf32_char>(NullTerminator), someString.ToChars()[0]);
	ARE_EQUAL(0, someString.GetLength());
}

//===========================================================================//

TEST(UTF32StringConstructorFromChars)
{
	utf32_string someString(UTF32TestChars);

	ARE_EQUAL(UTF32TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF32TestCharsCount, someString.GetLengthInChars());
	ARE_ARRAYS_EQUAL(UTF32TestChars, someString.ToChars(), someString.GetLengthInChars() + 1);
}

//===========================================================================//

TEST(UTF32StringConstructorFromUTF32Chars)
{
	utf32_string someString(UTF32TestChars);
	ARE_ARRAYS_EQUAL(UTF32TestChars, someString, UTF32TestCharsCount);
}

//===========================================================================//

TEST(UTF32StringConstructorOtherUTF32String)
{
	utf32_string sourceString(UTF32TestChars);
	utf32_string someString(sourceString);

	ARE_EQUAL(UTF32TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF32TestCharsCount, someString.GetLengthInChars());
	ARE_ARRAYS_EQUAL(UTF32TestChars, someString.ToChars(), someString.GetLengthInChars() + 1);
}

//===========================================================================//

TEST(UTF32StringConstructorMaxLength)
{
	utf32_string someString(StringTestsHelper->GetMaxLengthUTF32Chars());

	ARE_EQUAL(string::MaxLength, someString.GetLength());
	ARE_EQUAL(string::MaxLength, someString.GetLengthInChars());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthUTF32Chars(), someString.ToChars(), someString.GetLengthInChars() + 1);
}

//===========================================================================//

TEST(UTF32StringConstructorExceptions)
{
#ifdef DEBUGGING_STRINGS
	IS_THROW(utf32_string someString = UTF32TestInvalidChars, InvalidUTF32SequenceException);
#endif // DEBUGGING_STRINGS
	IS_THROW(utf32_string(StringTestsHelper->GetMoreThanMaxLengthUTF32Chars()), OverflowException);
}

//===========================================================================//

TEST(UTF32StringOperatorAssignEmpty)
{
	utf32_string sourceString(utf32_string::Empty);
	utf32_string someString(UTF32TestChars);

	someString = sourceString;

	ARE_EQUAL(0, someString.GetLengthInChars());
	ARE_EQUAL(static_cast<utf32_char>(NullTerminator), someString.ToChars()[0]);
	ARE_EQUAL(0, someString.GetLength());
}

//===========================================================================//

TEST(UTF32StringOperatorAssignToEmpty)
{
	utf32_string sourceString(UTF32TestChars);
	utf32_string someString(utf32_string::Empty);

	someString = sourceString;

	ARE_EQUAL(UTF32TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF32TestCharsCount, someString.GetLengthInChars());
	ARE_ARRAYS_EQUAL(UTF32TestChars, someString.ToChars(), someString.GetLengthInChars());
}

//===========================================================================//

TEST(UTF32StringOperatorAssign)
{
	utf32_string sourceString(UTF32TestChars);
	utf32_string someString(UTF32TestLongChars);

	someString = sourceString;

	ARE_EQUAL(UTF32TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF32TestCharsCount, someString.GetLengthInChars());
	ARE_ARRAYS_EQUAL(UTF32TestChars, someString.ToChars(), someString.GetLengthInChars() + 1);
}

//===========================================================================//

TEST(UTF32StringOperatorAssignExceptions)
{
	utf32_string someString(UTF32TestChars);

	IS_THROW(someString = StringTestsHelper->GetMoreThanMaxLengthUTF32Chars(), OverflowException);
}

//===========================================================================//

}// namespace Bru

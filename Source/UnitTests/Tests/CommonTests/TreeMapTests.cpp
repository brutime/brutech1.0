//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты TreeMap
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(TreeMapGetters)
{
	TreeMap<integer, string> map;

	map._count = 666;
	ARE_EQUAL(666, map.GetCount());
}

//===========================================================================//

TEST(TreeMapDefaultConstructor)
{
	TreeMap<string, string> map;

	ARE_EQUAL(0, map.GetCount());
	IS_TRUE(map._rootNode == NullPtr);
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(map._mostLeftNode == NullPtr);
}

//===========================================================================//

TEST(TreeListInitializerListConstructor)
{
	TreeMap<integer, integer> map = { { 1, 1 }, { 13, 13 }, { 54, 54 }, { 41, 41 } };

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, integer>(1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, integer>(13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, integer>(41, 41, TreeMapNodeColor::Red);
	expectedData[3] = TreeMapNodeTestData<integer, integer>(54, 54, TreeMapNodeColor::Black);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<integer, integer>::CheckNode(map._mostLeftNode, 1, 1)));
	IS_TRUE((TreeMapChecker<integer, integer>::CheckTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(TreeMapCopyConstructor)
{
	TreeMap<integer, DummyCompoundObject> sourceMap(ContainersHelper::CreateTestTreeMap());

	TreeMap<integer, DummyCompoundObject> map(sourceMap);

	sourceMap.Clear();

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(
	    (TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, TreeMapExpectedKeys[0], TreeMapExpectedValues[0])));
	IS_TRUE(ContainersHelper::CheckTestTreeMap(map));
}

//==========================================================================//

TEST(TreeMapMoveConstructor)
{
	TreeMap<integer, DummyCompoundObject> map(std::move(ContainersHelper::CreateTestTreeMap()));

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(
	    (TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, TreeMapExpectedKeys[0], TreeMapExpectedValues[0])));
	IS_TRUE(ContainersHelper::CheckTestTreeMap(map));
}

//==========================================================================//

TEST(TreeMapCopyOperator)
{
	TreeMap<integer, DummyCompoundObject> sourceMap(ContainersHelper::CreateTestTreeMap());

	TreeMap<integer, DummyCompoundObject> map;
	map = sourceMap;

	sourceMap.Clear();

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(
	    (TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, TreeMapExpectedKeys[0], TreeMapExpectedValues[0])));
	IS_TRUE(ContainersHelper::CheckTestTreeMap(map));
}

//===========================================================================//

TEST(TreeMapCopyOperatorSelf)
{
	TreeMap<integer, DummyCompoundObject> map(ContainersHelper::CreateTestTreeMap());

	map = map;

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(
	    (TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, TreeMapExpectedKeys[0], TreeMapExpectedValues[0])));
	IS_TRUE(ContainersHelper::CheckTestTreeMap(map));
}

//===========================================================================//

TEST(TreeMapMoveOperator)
{
	TreeMap<integer, DummyCompoundObject> map;

	map = std::move(ContainersHelper::CreateTestTreeMap());

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(
	    (TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, TreeMapExpectedKeys[0], TreeMapExpectedValues[0])));
	IS_TRUE(ContainersHelper::CheckTestTreeMap(map));
}

//==========================================================================//

TEST(TreeMapAddKeyValue)
{
	TreeMap<DummyCompoundObject, DummyCompoundObject> map;

	map.Add(d1, d1);

	const integer FirstExpectedDataCount = 1;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> firstExpectedData(FirstExpectedDataCount);
	firstExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1,
	                       TreeMapNodeColor::Black);

	ARE_EQUAL(FirstExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckNode(map._mostLeftNode, d1, d1)));
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckTreeMap(map, firstExpectedData)));

	//===========================================================================//

	map.Add(d13, d13);

	const integer SecondExpectedDataCount = 2;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> secondExpectedData(SecondExpectedDataCount);
	secondExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1,
	                        TreeMapNodeColor::Black);
	secondExpectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13,
	                        TreeMapNodeColor::Red);

	ARE_EQUAL(SecondExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckNode(map._mostLeftNode, d1, d1)));
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckTreeMap(map, secondExpectedData)));

	//===========================================================================//

	map.Add(d54, d54);

	const integer ThirdExpectedDataCount = 3;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> thirdExpectedData(ThirdExpectedDataCount);
	thirdExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1, TreeMapNodeColor::Red);
	thirdExpectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13,
	                       TreeMapNodeColor::Black);
	thirdExpectedData[2] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d54, d54,
	                       TreeMapNodeColor::Red);

	ARE_EQUAL(ThirdExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckNode(map._mostLeftNode, d1, d1)));
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckTreeMap(map, thirdExpectedData)));

	//===========================================================================//

	map.Add(d41, d41);

	const integer FourthExpectedDataCount = 4;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> fourthExpectedData(FourthExpectedDataCount);
	fourthExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1,
	                        TreeMapNodeColor::Black);
	fourthExpectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13,
	                        TreeMapNodeColor::Black);
	fourthExpectedData[2] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d41, d41,
	                        TreeMapNodeColor::Red);
	fourthExpectedData[3] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d54, d54,
	                        TreeMapNodeColor::Black);

	ARE_EQUAL(FourthExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckNode(map._mostLeftNode, d1, d1)));
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckTreeMap(map, fourthExpectedData)));
}

//===========================================================================//

TEST(TreeMapAddKeyValueExceptions)
{
	TreeMap<DummyCompoundObject, DummyCompoundObject> map;

	//Нарушение уникальностей ключей
	map.Add(d1, d1);
	IS_THROW(map.Add(d1, d1), InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapAddMoveKey)
{
	TreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(13, d13);
	map.Add(54, d54);
	map.Add(41, d41);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(1, d1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(13, d13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Red);
	expectedData[3] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Black);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, 1, d1)));
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(TreeMapAddMoveKeyExceptions)
{
	TreeMap<integer, DummyCompoundObject> map;

	//Нарушение уникальностей ключей
	map.Add(1, d1);
	IS_THROW(map.Add(1, d1), InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapAddMoveValue)
{
	TreeMap<DummyCompoundObject, integer> map;

	map.Add(d1, 1);
	map.Add(d13, 13);
	map.Add(d54, 54);
	map.Add(d41, 41);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<DummyCompoundObject, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<DummyCompoundObject, integer>(d1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<DummyCompoundObject, integer>(d13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<DummyCompoundObject, integer>(d41, 41, TreeMapNodeColor::Red);
	expectedData[3] = TreeMapNodeTestData<DummyCompoundObject, integer>(d54, 54, TreeMapNodeColor::Black);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<DummyCompoundObject, integer>::CheckNode(map._mostLeftNode, d1, 1)));
	IS_TRUE((TreeMapChecker<DummyCompoundObject, integer>::CheckTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(TreeMapAddMoveValueExceptions)
{
	TreeMap<DummyCompoundObject, integer> map;

	//Нарушение уникальностей ключей
	map.Add(d1, 1);
	IS_THROW(map.Add(d1, 1), InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapAddMoveKeyValue)
{
	TreeMap<integer, integer> map;

	map.Add(1, 1);
	map.Add(13, 13);
	map.Add(54, 54);
	map.Add(41, 41);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, integer>(1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, integer>(13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, integer>(41, 41, TreeMapNodeColor::Red);
	expectedData[3] = TreeMapNodeTestData<integer, integer>(54, 54, TreeMapNodeColor::Black);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<integer, integer>::CheckNode(map._mostLeftNode, 1, 1)));
	IS_TRUE((TreeMapChecker<integer, integer>::CheckTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(TreeMapAddMoveKeyValueExceptions)
{
	TreeMap<integer, integer> map;

	//Нарушение уникальностей ключей
	map.Add(1, 1);
	IS_THROW(map.Add(1, 1), InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapAddKeyValuePair)
{
	TreeMap<DummyCompoundObject, DummyCompoundObject> map;

	KeyValuePair<DummyCompoundObject, DummyCompoundObject> firstKeyValuePair(d1, d1);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> secondKeyValuePair(d13, d13);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> thirdKeyValuePair(d54, d54);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> fourthKeyValuePair(d41, d41);

	map.Add(firstKeyValuePair);
	map.Add(secondKeyValuePair);
	map.Add(thirdKeyValuePair);
	map.Add(fourthKeyValuePair);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d41, d41, TreeMapNodeColor::Red);
	expectedData[3] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d54, d54, TreeMapNodeColor::Black);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckNode(map._mostLeftNode, d1, d1)));
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(TreeMapAddKeyValuePairExceptions)
{
	TreeMap<DummyCompoundObject, DummyCompoundObject> map;

	//Нарушение уникальностей ключей
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d1, d1);
	map.Add(keyValuePair);
	IS_THROW(map.Add(keyValuePair), InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapAddMoveKeyValuePair)
{
	TreeMap<integer, integer> map;

	map.Add(KeyValuePair<integer, integer>(1, 1));
	map.Add(KeyValuePair<integer, integer>(13, 13));
	map.Add(KeyValuePair<integer, integer>(54, 54));
	map.Add(KeyValuePair<integer, integer>(41, 41));

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, integer>(1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, integer>(13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, integer>(41, 41, TreeMapNodeColor::Red);
	expectedData[3] = TreeMapNodeTestData<integer, integer>(54, 54, TreeMapNodeColor::Black);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<integer, integer>::CheckNode(map._mostLeftNode, 1, 1)));
	IS_TRUE((TreeMapChecker<integer, integer>::CheckTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(TreeMapAddMoveKeyValuePairExceptions)
{
	TreeMap<integer, integer> map;

	//Нарушение уникальностей ключей
	map.Add(KeyValuePair<integer, integer>(1, 1));
	IS_THROW(map.Add(KeyValuePair<integer, integer>(1, 1)), InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapRemove)
{
	TreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(13, d13);
	map.Add(54, d54);
	map.Add(41, d41);

	//===========================================================================//

	map.Remove(13);

	const integer FirstExpectedDataCount = 3;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> firstExpectedData(FirstExpectedDataCount);
	firstExpectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(1, d1, TreeMapNodeColor::Black);
	firstExpectedData[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Black);
	firstExpectedData[2] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Black);

	ARE_EQUAL(FirstExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, 1, d1)));
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckTreeMap(map, firstExpectedData)));

	//===========================================================================//

	map.Remove(1);

	const integer SecondExpectedDataCount = 2;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> secondExpectedData(SecondExpectedDataCount);
	secondExpectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Black);
	secondExpectedData[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Red);

	ARE_EQUAL(SecondExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, 41, d41)));
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckTreeMap(map, secondExpectedData)));

	//===========================================================================//

	map.Remove(41);

	const integer ThirdExpectedDataCount = 1;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> thirdExpectedData(ThirdExpectedDataCount);
	thirdExpectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Black);

	ARE_EQUAL(ThirdExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._mostLeftNode, 54, d54)));
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckTreeMap(map, thirdExpectedData)));

	//===========================================================================//

	map.Remove(54);

	ARE_EQUAL(0, map.GetCount());
	IS_TRUE(map._rootNode == NullPtr);
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(map._mostLeftNode == NullPtr);
}

//===========================================================================//

TEST(TreeMapRemoveExceptions)
{
	TreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(2, d2);

	IS_THROW(map.Remove(3), InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapOperatorSetIntegerKey)
{
	TreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(2, d2);
	map.Add(3, d3);
	map.Add(4, d4);

	IS_TRUE(map[1] == d1);
	IS_TRUE(map[2] == d2);
	IS_TRUE(map[3] == d3);
	IS_TRUE(map[4] == d4);

	//запутываем :)
	map[1] = d2;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 1, d2);

	map[2] = d3;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 2, d3);

	map[3] = d1;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 3, d1);

	map[4] = d4;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 4, d4);

	ARE_EQUAL(4, map.GetCount());

	IS_TRUE(map[1] == d2);
	IS_TRUE(map[2] == d3);
	IS_TRUE(map[3] == d1);
	IS_TRUE(map[4] == d4);
}

//===========================================================================//

TEST(TreeMapOperatorSetStringKey)
{
	TreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	IS_TRUE(map["one"] == d1);
	IS_TRUE(map["two"] == d2);
	IS_TRUE(map["three"] == d3);
	IS_TRUE(map["four"] == d4);

	//запутываем :)
	map["one"] = d2;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d2);

	map["two"] = d3;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "two", d3);

	map["three"] = d1;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "three", d1);

	map["four"] = d4;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "four", d4);

	ARE_EQUAL(4, map.GetCount());

	IS_TRUE(map["one"] == d2);
	IS_TRUE(map["two"] == d3);
	IS_TRUE(map["three"] == d1);
	IS_TRUE(map["four"] == d4);
}

//===========================================================================//

TEST(TreeMapOperatorSetExceptions)
{
	TreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);

	IS_THROW(map["three"] = d1, InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapOperatorGetIntegerKey)
{
	TreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(2, d2);
	map.Add(3, d3);
	map.Add(4, d4);

	IS_TRUE(map[1] == d1);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 1, d1);

	IS_TRUE(map[2] == d2);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 2, d2);

	IS_TRUE(map[3] == d3);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 3, d3);

	IS_TRUE(map[4] == d4);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 4, d4);
}

//===========================================================================//

TEST(TreeMapOperatorGetStringKey)
{
	TreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	IS_TRUE(map["one"] == d1);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	IS_TRUE(map["two"] == d2);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "two", d2);

	IS_TRUE(map["three"] == d3);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "three", d3);

	IS_TRUE(map["four"] == d4);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "four", d4);
}

//===========================================================================//

TEST(TreeMapOperatorGetExceptions)
{
	TreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);

	IS_THROW(map["three"] == d3, InvalidArgumentException);
}

//===========================================================================//

TEST(TreeMapContains)
{
	TreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	IS_TRUE(!map.Contains(""));
	IS_TRUE(map._lastFoundNode == NullPtr);

	IS_TRUE(!map.Contains("zero"));
	IS_TRUE(map._lastFoundNode == NullPtr);

	IS_TRUE(map.Contains("one"));
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	IS_TRUE(map.Contains("two"));

	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "two", d2);

	IS_TRUE(map.Contains("three"));

	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "three", d3);

	IS_TRUE(map.Contains("four"));

	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "four", d4);

	IS_TRUE(!map.Contains("five"));
	IS_TRUE(map._lastFoundNode == NullPtr);
}

//===========================================================================//

TEST(TreeMapClear)
{
	TreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	map.Clear();

	ARE_EQUAL(0, map.GetCount());
	IS_TRUE(map._rootNode == NullPtr);
	IS_TRUE(map._lastFoundNode == NullPtr);
	IS_TRUE(map._mostLeftNode == NullPtr);

	IS_TRUE(!map.Contains("one"));
	IS_TRUE(!map.Contains("two"));
	IS_TRUE(!map.Contains("three"));
	IS_TRUE(!map.Contains("four"));
}

//===========================================================================//

TEST(TreeMapIsEmpty)
{
	TreeMap<string, DummyCompoundObject> map;
	ARE_EQUAL(true, map.IsEmpty());

	map.Add("one", d1);
	ARE_EQUAL(false, map.IsEmpty());
}

//===========================================================================//

TEST(TreeMapCreateIterator)
{
	TreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestTreeMap();

	TreeMapIterator<integer, DummyCompoundObject> it = map.CreateIterator();

	IS_TRUE(it._owner == &map);
	IS_TRUE(it._cursor == map._mostLeftNode);
}

//===========================================================================//

TEST(TreeMapBegin)
{
	TreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestTreeMap();

	TreeMapIterator<integer, DummyCompoundObject> it = map.begin();

	IS_TRUE(it._owner == &map);
	IS_TRUE(it._cursor == map._mostLeftNode);
}

//===========================================================================//

TEST(TreeMapEnd)
{
	TreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestTreeMap();

	TreeMapIterator<integer, DummyCompoundObject> it = map.end();

	IS_TRUE(it._owner == &map);
	IS_TRUE(it._cursor == NullPtr);
}

//===========================================================================//

TEST(TreeMapLastFoundNode)
{
	TreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map["one"];

	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	map.Add("two", d2);
	IS_TRUE(map._lastFoundNode == NullPtr);

	map["one"];
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	map.Remove("two");
	IS_TRUE(map._lastFoundNode == NullPtr);
}

//===========================================================================//

class TreeMapHierarchy
{
public:
	TreeMapHierarchy() :
		friends()
	{
	}

	TreeMapHierarchy(const TreeMapHierarchy & other) :
		friends(other.friends)
	{
	}

	virtual ~TreeMapHierarchy()
	{
	}

	TreeMapHierarchy & operator =(const TreeMapHierarchy & other)
	{
		friends = other.friends;
		return *this;
	}

	TreeMap<string, TreeMapHierarchy> friends;
};

//===========================================================================//

TEST(TreeMapHierarchyClass)
{
	//Просто должен проходить, а не застревать в стеке
	TreeMapHierarchy treeMapHierarchy;
	ARE_EQUAL(0, treeMapHierarchy.friends.GetCount());
}

//===========================================================================//

//Для отлова утечек памяти при работе с узлами дерева.
//Чтобы тест работал - нужно также раскоментировать поле data в TreeMapNode, его создание и удаление
//TEST(TreeMapStressTest)
//{
//    TreeMap<integer, string> map;
//
//    const integer Count = 10;
//    integer numbers[Count] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
//
//    while (true)
//    {
//        for (integer i = 0; i < Count; i++)
//        {
//            map.Add(numbers[i], numbers[i]);
//        }
//
//        for (integer i = 0; i < Count; i++)
//        {
//            map.Remove(numbers[i]);
//        }
//    }
//}

//===========================================================================//

}// namespace Bru

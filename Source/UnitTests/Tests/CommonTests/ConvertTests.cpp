//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты конвертера типов
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

TEST(ConvertToBool)
{
	IS_TRUE((Convert<string, bool>("1")));
	IS_TRUE((Convert<string, bool>("22")));
	IS_TRUE((Convert<string, bool>("true")));
	IS_FALSE((Convert<string, bool>("tru!")));
	IS_FALSE((Convert<string, bool>("0")));
	IS_FALSE((Convert<string, bool>("false")));
}

//===========================================================================//

TEST(ConvertToChar)
{
	ARE_EQUAL(NullTerminator, (Convert<string, char>("")));
	ARE_EQUAL('B', (Convert<string, char>("B")));
}

//===========================================================================//

TEST(ConvertToCharExceptions)
{
	IS_THROW((Convert<string, char>("11")), InvalidArgumentException);
	IS_THROW((Convert<string, char>("¥")), InvalidArgumentException);
}

//===========================================================================//

TEST(ConvertToByte)
{
	ARE_EQUAL(byte(0), (Convert<string, byte>("0")));
	ARE_EQUAL(byte(128), (Convert<string, byte>("128")));
	ARE_EQUAL(byte(255), (Convert<string, byte>("255")));
}

//===========================================================================//

TEST(ConvertToByteExceptions)
{
	IS_THROW((Convert<string, byte>("-1")), InvalidArgumentException);
	IS_THROW((Convert<string, byte>("256")), InvalidArgumentException);
}

//===========================================================================//

TEST(ConvertToUnsignedShort)
{
	ARE_EQUAL(ushort(0), (Convert<string, ushort>("0")));
	ARE_EQUAL(ushort(255), (Convert<string, ushort>("255")));
	ARE_EQUAL(ushort(32000), (Convert<string, ushort>("32000")));
}

//===========================================================================//

TEST(ConvertToUnsignedShortExceptions)
{
	IS_THROW((Convert<string, ushort>("-1")), InvalidArgumentException);
}

//===========================================================================//

TEST(ConvertToUnsignedInteger)
{
	ARE_EQUAL(0u, (Convert<string, uint>("0")));
	ARE_EQUAL(255u, (Convert<string, uint>("255")));
	ARE_EQUAL(32000u, (Convert<string, uint>("32000")));
}

//===========================================================================//

TEST(ConvertToUnsignedIntegerExceptions)
{
	IS_THROW((Convert<string, uint>("-1")), InvalidArgumentException);
}

//===========================================================================//

TEST(ConvertToUnsignedLong)
{
	ARE_EQUAL(ulong(0), (Convert<string, ulong>("0")));
	ARE_EQUAL(ulong(255), (Convert<string, ulong>("255")));
	ARE_EQUAL(ulong(6000000), (Convert<string, ulong>("6000000")));
}

//===========================================================================//

TEST(ConvertToUnsignedLongExceptions)
{
	IS_THROW((Convert<string, uint>("-1")), InvalidArgumentException);
}

//===========================================================================//

TEST(ConvertToInteger)
{
	ARE_EQUAL(-6000000, (Convert<string, integer>("-6000000")));
	ARE_EQUAL(-255, (Convert<string, integer>("-255")));
	ARE_EQUAL(0, (Convert<string, integer>("0")));
	ARE_EQUAL(255, (Convert<string, integer>("255")));
	ARE_EQUAL(6000000, (Convert<string, integer>("6000000")));
}

//===========================================================================//

TEST(ConvertToInteger64)
{
	ARE_EQUAL(-6000000000, (Convert<string, int64>("-6000000000")));
	ARE_EQUAL(-255, (Convert<string, int64>("-255")));
	ARE_EQUAL(0, (Convert<string, int64>("0")));
	ARE_EQUAL(255, (Convert<string, int64>("255")));
	ARE_EQUAL(6000000000, (Convert<string, int64>("6000000000")));
}

//===========================================================================//

TEST(ConvertToFloat)
{
	ARE_CLOSE(-600000000.1f, (Convert<string, float>("-600000000.1")), 0.0001f);
	ARE_CLOSE(-255.654321, (Convert<string, float>("-255.654321")), 0.0001f);
	ARE_CLOSE(0.0f, (Convert<string, float>("0.0f")), 0.0001f);
	ARE_CLOSE(255.654321, (Convert<string, float>("255.654321")), 0.0001f);
	ARE_CLOSE(600000000.1f, (Convert<string, float>("600000000.1")), 0.0001f);
}

//===========================================================================//

}// namespace Bru

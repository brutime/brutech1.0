//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты массива
//              Проверки Add(const T &), Insert(const T &) проходят в ArrayTests
//              Проверки Add(T &&), Insert(T &&) проходят в SimplyTypeArrayTests
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"
#include "../../Helpers/ContainersHelper.h"
#include "../../Dummies/DummyBaseCounter.h"

namespace Bru
{

//===========================================================================//

TEST(ArrayGetters)
{
	Array<DummyCompoundObject> array;

	array._count = 10;
	ARE_EQUAL(10, array.GetCount());
}

//===========================================================================//

TEST(ArrayDefaultConstructor)
{
	Array<DummyCompoundObject> array;

	ARE_EQUAL(0, array._count);
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(ArrayConstructor)
{
	Array<DummyCompoundObject> array(10);

	ARE_EQUAL(10, array._count);
	ARE_EQUAL(10, array._reservedCount);
	IS_TRUE(array._dataPtr != NullPtr);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(ArrayConstructorExceptions)
{
	IS_THROW(Array<DummyCompoundObject> array(-1), InvalidArgumentException);
}
#endif

//===========================================================================//

TEST(ArrayInitializerListConstructor)
{
	const integer ExpectedArrayCount = 10;
	const DummyCompoundObject ExpectedArray[ExpectedArrayCount] = { d0, d1, d2, d3, d4, d5, d6, d7, d8, d9 };

	Array<DummyCompoundObject> array = { d0, d1, d2, d3, d4, d5, d6, d7, d8, d9 };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(ExpectedArrayCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(ExpectedArray, array, ExpectedArrayCount);
}

//===========================================================================//

TEST(ArrayCopyConstructor)
{
	Array<DummyCompoundObject> sourceArray = ContainersHelper::CreateTestDummyCompoundObjectArray();

	Array<DummyCompoundObject> array(sourceArray);

	sourceArray.Clear();

	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayMoveConstructor)
{
	Array<DummyCompoundObject> array(std::move(ContainersHelper::CreateTestDummyCompoundObjectArray()));

	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayCopyOperator)
{
	Array<DummyCompoundObject> sourceArray = ContainersHelper::CreateTestDummyCompoundObjectArray();

	Array<DummyCompoundObject> array;
	array = sourceArray;

	sourceArray.Clear();

	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayCopyOperatorSelf)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	array = array;

	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayMoveOperator)
{
	Array<DummyCompoundObject> array;
	array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayEqualsOperators)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();
	Array<DummyCompoundObject> equalsArray = ContainersHelper::CreateTestDummyCompoundObjectArray();
	Array<DummyCompoundObject> notEqualsArray = ContainersHelper::CreateTestDummyCompoundObjectArray();
	notEqualsArray.GetLast() = d666;

	IS_TRUE(array == equalsArray);
	IS_TRUE(array != notEqualsArray);
}

//===========================================================================//

TEST(ArrayResize)
{
	integer const InitialSize = Math::Floor(TestDummyesCount / 2.0f);
	Array<DummyCompoundObject> array(InitialSize);

	integer const GreaterSize = TestDummyesCount;

	array.Resize(GreaterSize);

	for (integer i = 0; i < GreaterSize; i++)
	{
		array[i] = TestDummyes[i];
	}

	ARE_EQUAL(GreaterSize, array._count);
	ARE_EQUAL(GreaterSize, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, GreaterSize);

	integer const SmallerSize = Math::Floor(TestDummyesCount / 2.0f);

	array.Resize(SmallerSize);

	for (integer i = 0; i < SmallerSize; i++)
	{
		array[i] = TestDummyes[i];
	}

	ARE_EQUAL(SmallerSize, array._count);
	ARE_EQUAL(SmallerSize, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, SmallerSize);
}

//===========================================================================//

TEST(ArrayAdd)
{
	Array<DummyCompoundObject> array;

	array.Add(d1);

	const integer FirstExpectedArrayCount = 1;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d1 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(1, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Add(d2);

	const integer SecondExpectedArrayCount = 2;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d1, d2 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Add(d3);

	const integer ThirdExpectedArrayCount = 3;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d1, d2, d3 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.Add(d4);

	const integer FourthExpectedArrayCount = 4;
	const DummyCompoundObject FourthExpectedArray[FourthExpectedArrayCount] = { d1, d2, d3, d4 };

	ARE_EQUAL(FourthExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FourthExpectedArray, array, FourthExpectedArrayCount);
}

//===========================================================================//

TEST(ArrayAddOtherArray)
{
	Array<DummyCompoundObject> array = { d1, d2, d3 };

	array.Add(Array<DummyCompoundObject> { d4, d5, d6 });

	const integer FirstExpectedArrayCount = 6;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d1, d2, d3, d4, d5, d6 };

	ARE_EQUAL(FirstExpectedArrayCount, array._count);
	ARE_EQUAL(6, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Add(Array<DummyCompoundObject>());

	ARE_EQUAL(FirstExpectedArrayCount, array._count);
	ARE_EQUAL(6, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);
}

//===========================================================================//

TEST(ArrayAddOtherArrayExceptions)
{
	Array<DummyCompoundObject> array;
	IS_THROW(array.Add(array), InvalidArgumentException);
}

//===========================================================================//

TEST(ArrayAddList)
{
	Array<DummyCompoundObject> array = { d1, d2, d3 };

	array.Add(List<DummyCompoundObject> { d4, d5, d6 });

	const integer FirstExpectedArrayCount = 6;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d1, d2, d3, d4, d5, d6 };

	ARE_EQUAL(FirstExpectedArrayCount, array._count);
	ARE_EQUAL(6, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Add(List<DummyCompoundObject>());

	ARE_EQUAL(FirstExpectedArrayCount, array._count);
	ARE_EQUAL(6, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);
}

//===========================================================================//

TEST(ArrayInsert)
{
	Array<DummyCompoundObject> array = { d1, d2, d3 };

	array.Insert(0, d4);

	const integer FirstExpectedArrayCount = 4;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d4, d1, d2, d3 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(6, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Insert(2, d5);

	const integer SecondExpectedArrayCount = 5;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d4, d1, d5, d2, d3 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(6, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Insert(5, d6);

	const integer ThirdExpectedArrayCount = 6;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d4, d1, d5, d2, d3, d6 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(6, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(ArrayInsertExceptions)
{
	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray.Insert(-1, d1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Insert(1, d1), IndexOutOfBoundsException);
}
#endif

//===========================================================================//

TEST(ArrayInsertOtherArray)
{
	Array<DummyCompoundObject> sourceArray = { d1, d2, d3 };

	Array<DummyCompoundObject> array;

	//Пустой массив
	array.Insert(0, Array<DummyCompoundObject>());

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);

	array.Insert(0, sourceArray);

	const integer FirstExpectedArrayCount = 3;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d1, d2, d3 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Insert(1, sourceArray);

	const integer SecondExpectedArrayCount = 6;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d1, d1, d2, d3, d2, d3 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Insert(6, sourceArray);

	const integer ThirdExpectedArrayCount = 9;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d1, d1, d2, d3, d2, d3, d1, d2, d3 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(16, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(ArrayInsertOtherArrayExceptions)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();
	IS_THROW(array.Insert(0, array), InvalidArgumentException);

	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray.Insert(-1, array), IndexOutOfBoundsException);
	IS_THROW(emptyArray.Insert(1, array), IndexOutOfBoundsException);
}
#endif

//===========================================================================//

TEST(ArrayRemove)
{
	Array<DummyCompoundObject> array = { d1, d2, d3, d4 };

	array.Remove(d3);

	const integer FirstExpectedArrayCount = 3;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d1, d2, d4 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Remove(d1);

	const integer SecondExpectedArrayCount = 2;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d2, d4 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Remove(d4);

	const integer ThirdExpectedArrayCount = 1;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d2 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.Remove(d2);

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(ArrayRemoveWithPredicate)
{
	Array<DummyCompoundObject> array = { d1, d1, d2, d3, d1, d4, d5, d6 };

	array.Remove([] (const DummyCompoundObject & number)
	             {
	                 return number == 1;
				 });

	const integer FirstExpectedArrayCount = 5;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d2, d3, d4, d5, d6 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(8, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.Remove([] (const DummyCompoundObject & number)
	             {
	                 return number <= 3;
				 });

	const integer SecondExpectedArrayCount = 3;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d4, d5, d6 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.Remove([] (const DummyCompoundObject & number)
	             {
	                 return (number > 3 && number < 6);
				 });

	const integer ThirdExpectedArrayCount = 1;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d6 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.Remove([] (const DummyCompoundObject & number)
	             {
	                 return number != 0;
				 });

	ARE_EQUAL(0, array.GetCount());
	IS_TRUE(array.IsEmpty());
}

//===========================================================================//

TEST(ArrayRemoveExceptions)
{
	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray.Remove(DummyCompoundObject(666)), InvalidArgumentException);

	Array<DummyCompoundObject> array;

	array.Add(d1);
	array.Add(d2);

	IS_THROW(array.Remove(DummyCompoundObject(666)), InvalidArgumentException);
}

//===========================================================================//

TEST(ArrayRemoveAt)
{
	Array<DummyCompoundObject> array = { d1, d2, d3, d4 };

	array.RemoveAt(3);

	const integer FirstExpectedArrayCount = 3;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d1, d2, d3 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.RemoveAt(2);

	const integer SecondExpectedArrayCount = 2;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d1, d2 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(4, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.RemoveAt(0);

	const integer ThirdExpectedArrayCount = 1;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d2 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(2, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.RemoveAt(0);

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(ArrayRemoveAtExceptions)
{
	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray.RemoveAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(0), IndexOutOfBoundsException);

	Array<DummyCompoundObject> array = { d1, d2 };

	IS_THROW(emptyArray.RemoveAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(2), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ArrayRemoveAtIndexCount)
{
	Array<DummyCompoundObject> array = { d1, d2, d3, d4, d5, d6, d7 };

	array.RemoveAt(0, 2);

	const integer FirstExpectedArrayCount = 5;
	const DummyCompoundObject FirstExpectedArray[FirstExpectedArrayCount] = { d3, d4, d5, d6, d7 };

	ARE_EQUAL(FirstExpectedArrayCount, array.GetCount());
	ARE_EQUAL(7, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, FirstExpectedArrayCount);

	array.RemoveAt(1, 2);

	const integer SecondExpectedArrayCount = 3;
	const DummyCompoundObject SecondExpectedArray[SecondExpectedArrayCount] = { d3, d6, d7 };

	ARE_EQUAL(SecondExpectedArrayCount, array.GetCount());
	ARE_EQUAL(7, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, SecondExpectedArrayCount);

	array.RemoveAt(1, 2);

	const integer ThirdExpectedArrayCount = 1;
	const DummyCompoundObject ThirdExpectedArray[ThirdExpectedArrayCount] = { d3 };

	ARE_EQUAL(ThirdExpectedArrayCount, array.GetCount());
	ARE_EQUAL(3, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ThirdExpectedArrayCount);

	array.RemoveAt(0, 1);

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(ArrayRemoveAtIndexCountExceptions)
{
	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray.RemoveAt(-1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(-1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(-1, 1), IndexOutOfBoundsException);

	IS_THROW(emptyArray.RemoveAt(0, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(0, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.RemoveAt(0, 1), IndexOutOfBoundsException);

	Array<DummyCompoundObject> array = { d1, d2 };

	IS_THROW(array.RemoveAt(-1, -1), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(-1, 0), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(-1, 1), IndexOutOfBoundsException);

	IS_THROW(array.RemoveAt(2, -1), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(2, 0), IndexOutOfBoundsException);
	IS_THROW(array.RemoveAt(2, 1), IndexOutOfBoundsException);

	IS_THROW(array.RemoveAt(0, -1), InvalidArgumentException);
	IS_THROW(array.RemoveAt(0, 3), InvalidArgumentException);
}

//===========================================================================//

TEST(ArrayOperatorSet)
{
	Array<DummyCompoundObject> array(TestDummyesCount);

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		array[i] = TestDummyes[i];
	}

	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayOperatorSetOverwrite)
{
	Array<DummyCompoundObject> array(1);

	array[0] = d1;
	ARE_EQUAL(1, array.GetCount());
	ARE_EQUAL(1, array._reservedCount);
	ARE_EQUAL(1, array[0].GetValue());

	array[0] = d2;
	ARE_EQUAL(1, array.GetCount());
	ARE_EQUAL(1, array._reservedCount);
	ARE_EQUAL(2, array[0].GetValue());
}

//===========================================================================//

TEST(ArrayOperatorSetExceptions)
{
	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray[-1] = d11, IndexOutOfBoundsException);
	IS_THROW(emptyArray[0] = d11, IndexOutOfBoundsException);

	Array<DummyCompoundObject> array(TestDummyesCount);
	IS_THROW(array[-1] = d11, IndexOutOfBoundsException);
	IS_THROW(array[TestDummyesCount] = d11, IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ArrayOperatorGet)
{
	const Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	//Вот тут и провериться []
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayOperatorGetExceptions)
{
	const Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray[-1], IndexOutOfBoundsException);
	IS_THROW(emptyArray[0], IndexOutOfBoundsException);

	const Array<DummyCompoundObject> array(TestDummyesCount);
	IS_THROW(array[-1], IndexOutOfBoundsException);
	IS_THROW(array[TestDummyesCount], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ArraySetArray)
{
	Array<DummyCompoundObject> array = { d1, d2, d3, d4, d5, d6, d7 };

	Array<DummyCompoundObject> otherArray;

	otherArray.Add(d11);
	otherArray.Add(d22);

	array.SetArray(0, otherArray);

	const integer ExpectedArrayCount = 7;
	const DummyCompoundObject FirstExpectedArray[ExpectedArrayCount] = { d11, d22, d3, d4, d5, d6, d7 };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(7, array._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, array, ExpectedArrayCount);

	array.SetArray(5, otherArray);

	const DummyCompoundObject SecondExpectedArray[ExpectedArrayCount] = { d11, d22, d3, d4, d5, d11, d22 };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(7, array._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, array, ExpectedArrayCount);

	array.SetArray(3, otherArray);

	const DummyCompoundObject ThirdExpectedArray[ExpectedArrayCount] = { d11, d22, d3, d11, d22, d11, d22 };

	ARE_EQUAL(ExpectedArrayCount, array.GetCount());
	ARE_EQUAL(7, array._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, array, ExpectedArrayCount);
}

//===========================================================================//

TEST(ArraySetArrayExceptions)
{
	Array<DummyCompoundObject> sourceArray = ContainersHelper::CreateTestDummyCompoundObjectArray();

	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray.SetArray(-1, sourceArray), IndexOutOfBoundsException);
	IS_THROW(emptyArray.SetArray(0, sourceArray), IndexOutOfBoundsException);

	IS_THROW(sourceArray.SetArray(0, sourceArray), InvalidArgumentException);

	Array<DummyCompoundObject> array(TestDummyesCount - 1);
	IS_THROW(array.SetArray(0, sourceArray), InvalidArgumentException);
}

//===========================================================================//

TEST(ArrayGetArray)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	Array<DummyCompoundObject> emptySubArray = array.GetArray(0, 0);
	ARE_EQUAL(0, emptySubArray.GetCount());
	ARE_EQUAL(0, emptySubArray._reservedCount);

	Array<DummyCompoundObject> beginSubArray = array.GetArray(0, TestDummyesCount / 2);

	ARE_EQUAL(TestDummyesCount / 2, beginSubArray.GetCount());
	ARE_EQUAL(TestDummyesCount / 2, beginSubArray._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, beginSubArray, TestDummyesCount / 2);

	Array<DummyCompoundObject> middleSubArray = array.GetArray(TestDummyesCount / 4, TestDummyesCount / 2);

	ARE_EQUAL(TestDummyesCount / 2, middleSubArray.GetCount());
	ARE_EQUAL(TestDummyesCount / 2, middleSubArray._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes + TestDummyesCount / 4, middleSubArray, TestDummyesCount / 2);

	Array<DummyCompoundObject> endSubArray = array.GetArray(TestDummyesCount / 2, TestDummyesCount / 2);

	ARE_EQUAL(TestDummyesCount / 2, endSubArray.GetCount());
	ARE_EQUAL(TestDummyesCount / 2, endSubArray._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes + TestDummyesCount / 2, endSubArray, TestDummyesCount / 2);
}

//===========================================================================//

TEST(ArrayGetArrayExceptions)
{
	Array<DummyCompoundObject> emptyArray;
	IS_THROW(emptyArray.GetArray(-1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(-1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(-1, 1), IndexOutOfBoundsException);

	IS_THROW(emptyArray.GetArray(0, -1), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(0, 0), IndexOutOfBoundsException);
	IS_THROW(emptyArray.GetArray(0, 1), IndexOutOfBoundsException);

	Array<DummyCompoundObject> array = { d1, d2 };

	IS_THROW(array.GetArray(-1, -1), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(-1, 0), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(-1, 1), IndexOutOfBoundsException);

	IS_THROW(array.GetArray(2, -1), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(2, 0), IndexOutOfBoundsException);
	IS_THROW(array.GetArray(2, 1), IndexOutOfBoundsException);

	IS_THROW(array.GetArray(0, -1), InvalidArgumentException);
	IS_THROW(array.GetArray(0, 3), InvalidArgumentException);
}

//===========================================================================//

TEST(ArrayContains)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	IS_TRUE(array.Contains(TestDummyes[0]));
	IS_TRUE(array.Contains(TestDummyes[(TestDummyesCount - 1) / 2]));
	IS_TRUE(array.Contains(TestDummyes[TestDummyesCount - 1]));
	IS_FALSE(array.Contains(DummyCompoundObject(TestDummyesCount + 666)));
}

//===========================================================================//

TEST(ArrayContainsWithPredicate)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	IS_TRUE(array.Contains([] (const DummyCompoundObject & number)
	                       {
	                           return number == TestDummyes[TestDummyesCount / 2];
						   }));
	IS_TRUE(!array.Contains([] (const DummyCompoundObject & number)
	                        {
	                            return number == -1;
							}));
}

//===========================================================================//

TEST(ArrayIndexOf)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		ARE_EQUAL(i, array.IndexOf(TestDummyes[i]));
	}

	ARE_EQUAL(-1, array.IndexOf(DummyCompoundObject(TestDummyesCount + 666)));
}

//===========================================================================//

TEST(ArraySort)
{
	const integer ArraySize = 100;
	Array<DummyCompoundObject> array(ArraySize);

	for (integer i = 0; i < ArraySize; i++)
	{
		array[i] = DummyCompoundObject(Math::Random(ArraySize));
	}

	DummyCompoundObject::AscendingComparator ascendingComparator;
	array.Sort(ascendingComparator);

	for (integer i = 0; i < ArraySize - 1; i++)
	{
		IS_TRUE(array[i] <= array[i + 1]);
	}

	DummyCompoundObject::DescendingComparator descendingComparator;
	array.Sort(descendingComparator);

	for (integer i = 0; i < ArraySize - 1; i++)
	{
		IS_TRUE(array[i + 1] <= array[i]);
	}
}

//===========================================================================//

TEST(ArrayToList)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();
	List<DummyCompoundObject> list = array.ToList();

	//Не изменился-ли массив
	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);

	//Проверяем полученный список
	ARE_EQUAL(TestDummyesCount, list.GetCount());
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ArrayClear)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	array.Clear();

	ARE_EQUAL(0, array.GetCount());
	ARE_EQUAL(0, array._reservedCount);
	IS_TRUE(array._dataPtr == NullPtr);
}

//===========================================================================//

TEST(ArrayReset)
{
	DummyBaseCounter::Reset();

	{
		Array<SharedPtr<DummyBaseCounter>> array(2);
		array[0] = new DummyBaseCounter();
		array[1] = new DummyBaseCounter();

		array.Reset();

		ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
		ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

		ARE_EQUAL(2, array.GetCount());
		ARE_EQUAL(2, array._reservedCount);

		array[0] = new DummyBaseCounter();
		array[1] = new DummyBaseCounter();

		ARE_EQUAL(4, DummyBaseCounter::GetCreatedCount());
		ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

		ARE_EQUAL(2, array.GetCount());
		ARE_EQUAL(2, array._reservedCount);
	}

	ARE_EQUAL(4, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(4, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(ArrayResetCount)
{
	{
		integer const InitialSize = 2;
		Array<SharedPtr<DummyBaseCounter>> array(InitialSize);
		array[0] = new DummyBaseCounter();
		array[1] = new DummyBaseCounter();

		integer const GreaterSize = 4;
		array.Reset(GreaterSize);

		ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
		ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

		ARE_EQUAL(4, array.GetCount());
		ARE_EQUAL(4, array._reservedCount);

		array[0] = new DummyBaseCounter();
		array[1] = new DummyBaseCounter();

		integer const SmallerSize = 3;
		array.Reset(SmallerSize);

		ARE_EQUAL(4, DummyBaseCounter::GetCreatedCount());
		ARE_EQUAL(4, DummyBaseCounter::GetDeletedCount());

		ARE_EQUAL(3, array.GetCount());
		ARE_EQUAL(3, array._reservedCount);
	}

	ARE_EQUAL(4, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(4, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(ArrayIsEmpty)
{
	Array<string> array;
	ARE_EQUAL(true, array.IsEmpty());

	array.Add("one");
	ARE_EQUAL(false, array.IsEmpty());
}

//===========================================================================//

TEST(Array2D)
{
	integer const ArraysCount = 50;
	integer const ElementsCount = 100;

	Array<Array<DummyCompoundObject>> array(ArraysCount);

	for (integer i = 0; i < ArraysCount; i++)
	{
		array[i].Resize(ElementsCount);
	}

	ARE_EQUAL(ArraysCount, array.GetCount());

	for (integer i = 0; i < ArraysCount; i++)
	{
		ARE_EQUAL(ElementsCount, array[i].GetCount());
	}

	for (int i = 0; i < ArraysCount; i++)
	{
		for (int j = 0; j < ElementsCount; j++)
		{
			array[i][j] = DummyCompoundObject(i * j + i + j);
		}
	}

	for (int i = 0; i < ArraysCount; i++)
	{
		for (int j = 0; j < ElementsCount; j++)
		{
			IS_TRUE(array[i][j] == DummyCompoundObject(i * j + i + j));
		}
	}
}

//===========================================================================//

TEST(Array2DExceptions)
{
	integer const ArraysCount = 50;
	integer const ElementsCount = 100;

	Array<Array<DummyCompoundObject>> array(ArraysCount);

	for (integer i = 0; i < ArraysCount; i++)
	{
		array[i].Resize(ElementsCount);
	}

	IS_THROW(array[-1], IndexOutOfBoundsException);
	IS_THROW(array[ArraysCount], IndexOutOfBoundsException);
	IS_THROW(array[0][-1], IndexOutOfBoundsException);
	IS_THROW(array[0][ElementsCount], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ArrayCreateIterator)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	ArrayIterator<DummyCompoundObject> it = array.CreateIterator();

	IS_TRUE(it._owner == & array);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(ArrayBegin)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	ArrayIterator<DummyCompoundObject> it = array.begin();

	IS_TRUE(it._owner == & array);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(ArrayEnd)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	ArrayIterator<DummyCompoundObject> it = array.end();

	IS_TRUE(it._owner == & array);
	ARE_EQUAL(array.GetCount(), it._cursor);
}

//===========================================================================//

} // namespace Bru

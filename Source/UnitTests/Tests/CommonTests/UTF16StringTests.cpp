//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF16 строки
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringTestsHelper.h"
#include "../../Helpers/StringsTestData.h"

namespace Bru
{

//===========================================================================//

TEST(UTF16StringConstructorFromEmpty)
{
	utf16_string someString(utf16_string::Empty);

	ARE_EQUAL(0, someString.GetLengthInWords());
	ARE_EQUAL(NullTerminator, someString.ToChars()[0]);
	ARE_EQUAL(0, someString.GetLength());
}

//===========================================================================//

TEST(UTF16StringConstructorFromWords)
{
	utf16_string someString(UTF16TestWords);

	ARE_EQUAL(UTF16TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF16TestWordsCount, someString.GetLengthInWords());
	ARE_ARRAYS_EQUAL(UTF16TestWords, someString.ToChars(), someString.GetLengthInWords() + 1);
}

//===========================================================================//

TEST(UTF16StringConstructorOtherUTF16String)
{
	utf16_string sourceString(UTF16TestWords);
	utf16_string someString(sourceString);

	ARE_EQUAL(UTF16TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF16TestWordsCount, someString.GetLengthInWords());
	ARE_ARRAYS_EQUAL(UTF16TestWords, someString.ToChars(), someString.GetLengthInWords() + 1);
}

//===========================================================================//

TEST(UTF16StringConstructorMaxLength)
{
	utf16_string someString(StringTestsHelper->GetMaxLengthUTF16Chars());

	ARE_EQUAL(string::MaxLength, someString.GetLengthInWords());
	ARE_EQUAL(string::MaxLength, someString.GetLength());
	ARE_ARRAYS_EQUAL(StringTestsHelper->GetMaxLengthUTF16Chars(), someString.ToChars(), string::MaxLength + 1);
}

//===========================================================================//

TEST(UTF16StringConstructorExceptions)
{
#ifdef DEBUGGING_STRINGS
	IS_THROW(utf16_string someString = UTF16TestInvalidWords, InvalidUTF16SequenceException);
#endif // DEBUGGING_STRINGS
	IS_THROW(utf16_string(StringTestsHelper->GetMoreThanMaxLengthUTF16Chars()), OverflowException);
}

//===========================================================================//

TEST(UTF16StringOperatorAssignEmpty)
{
	utf16_string sourceString(utf16_string::Empty);
	utf16_string someString(UTF16TestWords);

	someString = sourceString;

	ARE_EQUAL(0, someString.GetLengthInWords());
	ARE_EQUAL(NullTerminator, someString.ToChars()[0]);
	ARE_EQUAL(0, someString.GetLength());
}

//===========================================================================//

TEST(UTF16StringOperatorAssignToEmpty)
{
	utf16_string sourceString(UTF16TestWords);
	utf16_string someString(NullTerminator);

	someString = sourceString;

	ARE_EQUAL(UTF16TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF16TestWordsCount, someString.GetLengthInWords());
	ARE_ARRAYS_EQUAL(UTF16TestWords, someString.ToChars(), someString.GetLengthInWords() + 1);
}

//===========================================================================//

TEST(UTF16StringOperatorAssign)
{
	utf16_string sourceString(UTF16TestWords);
	utf16_string someString(UTF16TestLongWords);

	someString = sourceString;

	ARE_EQUAL(UTF16TestCharsCount, someString.GetLength());
	ARE_EQUAL(UTF16TestWordsCount, someString.GetLengthInWords());
	ARE_ARRAYS_EQUAL(UTF16TestWords, someString.ToChars(), someString.GetLengthInWords() + 1);
}

//===========================================================================//

TEST(UTF16StringOperatorAssignExceptions)
{
	utf16_string someString(UTF16TestWords);

	IS_THROW(someString = StringTestsHelper->GetMoreThanMaxLengthUTF16Chars(), OverflowException);
}

//===========================================================================//

}// namespace Bru

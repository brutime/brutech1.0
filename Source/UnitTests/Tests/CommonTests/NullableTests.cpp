//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты Nullable
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"

using namespace Brunit;

namespace Bru
{

//===========================================================================//

TEST(NullableDefaultConstructor)
{
	Nullable<string> nullNullable;

	ARE_EQUAL(string::Empty, nullNullable._value);
	ARE_EQUAL(true, nullNullable._isNull);
}

//===========================================================================//

TEST(NullableNullPtrConstructor)
{
	Nullable<string> nullNullable(NullPtr);

	ARE_EQUAL(string::Empty, nullNullable._value);
	ARE_EQUAL(true, nullNullable._isNull);
}

//===========================================================================//

TEST(NullableConstructor)
{
	Nullable<string> nullable("B¥𠀀௵rΩ");

	ARE_EQUAL("B¥𠀀௵rΩ", nullable._value);
	ARE_EQUAL(false, nullable._isNull);
}

//===========================================================================//

TEST(NullableCopyConstructor)
{
	Nullable<string> sourceNullNullable(NullPtr);
	Nullable<string> nullNullable(sourceNullNullable);

	ARE_EQUAL(string::Empty, nullNullable._value);
	ARE_EQUAL(true, nullNullable._isNull);

	Nullable<string> sourceNullable("B¥𠀀௵rΩ");
	Nullable<string> nullable(sourceNullable);

	ARE_EQUAL("B¥𠀀௵rΩ", nullable._value);
	ARE_EQUAL(false, nullable._isNull);
}

//===========================================================================//

TEST(NullableNullPtrCopyOperator)
{
	Nullable<string> nullable("B¥𠀀௵rΩ");

	nullable = NullPtr;

	ARE_EQUAL(string::Empty, nullable._value);
	ARE_EQUAL(true, nullable._isNull);
}

//===========================================================================//

TEST(NullableValueCopyOperator)
{
	Nullable<string> nullNullable(NullPtr);

	nullNullable = "∫𠁥♫u";

	ARE_EQUAL("∫𠁥♫u", nullNullable._value);
	ARE_EQUAL(false, nullNullable._isNull);

	Nullable<string> nullable("B¥𠀀௵rΩ");

	nullable = "∫𠁥♫u";

	ARE_EQUAL("∫𠁥♫u", nullable._value);
	ARE_EQUAL(false, nullable._isNull);
}

//===========================================================================//

TEST(NullableCopyOperator)
{
	Nullable<string> sourceNullable("B¥𠀀௵rΩ");
	Nullable<string> nullable(NullPtr);
	Nullable<string> nullableWithValue("∫𠁥♫u");

	nullable = sourceNullable;
	nullableWithValue = sourceNullable;

	sourceNullable = NullPtr;

	ARE_EQUAL("B¥𠀀௵rΩ", nullable._value);
	ARE_EQUAL(false, nullable._isNull);

	ARE_EQUAL("B¥𠀀௵rΩ", nullableWithValue._value);
	ARE_EQUAL(false, nullableWithValue._isNull);
}

//===========================================================================//

TEST(NullableNullNulableCopyOperator)
{
	Nullable<string> sourceNullNullable(NullPtr);
	Nullable<string> nullable("B¥𠀀௵rΩ");

	nullable = sourceNullNullable;

	ARE_EQUAL(string::Empty, nullable._value);
	ARE_EQUAL(true, nullable._isNull);
}

//===========================================================================//

TEST(NullableNullPtrEqualsOperator)
{
	Nullable<string> equalsNullable(NullPtr);
	Nullable<string> notEqualsNullable("B¥𠀀௵rΩ");

	IS_TRUE(equalsNullable == NullPtr);
	IS_TRUE(notEqualsNullable != NullPtr);
	IS_TRUE(NullPtr == equalsNullable);
	IS_TRUE(NullPtr != notEqualsNullable);
}

//===========================================================================//

TEST(NullableValueEqualsOperator)
{
	Nullable<string> nullNullable(NullPtr);
	Nullable<string> equalsNullable("∫𠁥♫u");
	Nullable<string> notEqualsNullable("B¥𠀀௵rΩ");

	IS_FALSE(nullNullable == "∫𠁥♫u");
	IS_TRUE(equalsNullable == "∫𠁥♫u");

	IS_TRUE(nullNullable != "∫𠁥♫u");
	IS_TRUE(notEqualsNullable != "∫𠁥♫u");
}

//===========================================================================//

TEST(NullableEqualsOperator)
{
	Nullable<string> nullNullable(NullPtr);
	Nullable<string> equalsNullable("∫𠁥♫u");
	Nullable<string> notEqualsNullable("B¥𠀀௵rΩ");
	Nullable<string> nullable("∫𠁥♫u");

	IS_FALSE(nullNullable == nullable);
	IS_TRUE(equalsNullable == nullable);

	IS_TRUE(nullNullable != nullable);
	IS_TRUE(notEqualsNullable != nullable);
}

//===========================================================================//

TEST(NullableHasValue)
{
	Nullable<string> nullNullable(NullPtr);
	ARE_EQUAL(false, nullNullable.HasValue());

	Nullable<string> nullable("B¥𠀀௵rΩ");
	ARE_EQUAL(true, nullable.HasValue());

	nullable = NullPtr;

	ARE_EQUAL(false, nullable.HasValue());
}

//===========================================================================//

TEST(NullableGetValue)
{
	Nullable<string> nullNullable(NullPtr);
	IS_THROW(nullNullable.GetValue(), NullPointerException);

	Nullable<string> nullable("B¥𠀀௵rΩ");
	ARE_EQUAL("B¥𠀀௵rΩ", nullable.GetValue());

	nullable = NullPtr;
	IS_THROW(nullable.GetValue(), NullPointerException);
}

//===========================================================================//

TEST(NullableGetValueOrDefault)
{
	Nullable<string> nullNullable(NullPtr);
	ARE_EQUAL("∫𠁥♫u", nullNullable.GetValueOrDefault("∫𠁥♫u"));

	Nullable<string> nullable("B¥𠀀௵rΩ");
	ARE_EQUAL("B¥𠀀௵rΩ", nullable.GetValueOrDefault("∫𠁥♫u"));

	nullable = NullPtr;

	ARE_EQUAL("∫𠁥♫u", nullable.GetValueOrDefault("∫𠁥♫u"));
}

//===========================================================================//

} // namespace Bru

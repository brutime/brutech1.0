//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF32 помощника
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringTestsHelper.h"
#include "../../Helpers/StringsTestData.h"

namespace Bru
{

//===========================================================================//

TEST(UTF32HelperGetLength)
{
	UTFLength nullTerminatorStringLength = UTF32Helper::GetLength(NullTerminator);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInChars);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInComponents);

	UTFLength stringLength = UTF32Helper::GetLength(UTF32TestChars);
	//-1 чтобы не учитывать длину нуль-терминатора
	ARE_EQUAL(UTF32TestCharsCount, stringLength.LengthInChars);
	ARE_EQUAL(UTF32TestCharsCount, stringLength.LengthInComponents);

	ARE_EQUAL(string::MaxLength,
	          UTF32Helper::GetLength(StringTestsHelper->GetMaxLengthUTF32Chars()).LengthInComponents);
	ARE_EQUAL(string::MaxLength, UTF32Helper::GetLength(StringTestsHelper->GetMaxLengthUTF32Chars()).LengthInChars);
}

//===========================================================================//

TEST(UTF32HelperCheck)
{
	//Эта тест просто не должен вызывать экспшенов
	UTF32Helper::Check(static_cast<const ulong *>(NullTerminator));
	UTF32Helper::Check(UTF32TestCharsCount);
	UTF32Helper::Check(StringTestsHelper->GetMaxLengthUTF32Chars());
}

//===========================================================================//

TEST(UTF32HelperCheckExceptions)
{
	ulong lessThanLastCodeInUTF32SequenceChars[2] = { UTF32Helper::FirstCodeInUTF32Sequence - 1, NullTerminator };
	IS_THROW(UTF32Helper::Check(lessThanLastCodeInUTF32SequenceChars), InvalidArgumentException);

	ulong greaterThanLastCodeInUTF32SequenceChars[2] = { UTF32Helper::LastCodeInUTF32Sequence + 1, NullTerminator };
	IS_THROW(UTF32Helper::Check(greaterThanLastCodeInUTF32SequenceChars), InvalidArgumentException);

	for (ulong currentChar = UTF32Helper::FirstCodeInFirstNoncharRange;
	        currentChar <= UTF32Helper::LastCodeInFirstNoncharRange; currentChar++)
	{
		ulong nonCharChars[2] = { currentChar, NullTerminator };
		IS_THROW(UTF32Helper::Check(nonCharChars), InvalidArgumentException);
	}

	for (ulong currentChar = UTF32Helper::FirstCodeInSecondNoncharRange;
	        currentChar <= UTF32Helper::LastCodeInSecondNoncharRange; currentChar++)
	{
		ulong nonCharChars[2] = { currentChar, NullTerminator };
		IS_THROW(UTF32Helper::Check(nonCharChars), InvalidArgumentException);
	}
}

//===========================================================================//

TEST(UTF32HelperCheckNoncharsExceptions)
{
	static const integer SourceNoncharSurrogatesCharsCount = 32;

	static const ulong SourceNoncharSurrogatesChars[SourceNoncharSurrogatesCharsCount] = { 0x01FFFE, 0x01FFFF, 0x02FFFE,
	        0x02FFFF, 0x03FFFE, 0x03FFFF, 0x04FFFE, 0x04FFFF, 0x05FFFE, 0x05FFFF, 0x06FFFE, 0x06FFFF, 0x07FFFE, 0x07FFFF,
	        0x08FFFE, 0x08FFFF, 0x09FFFE, 0x09FFFF, 0x0AFFFE, 0x0AFFFF, 0x0BFFFE, 0x0BFFFF, 0x0CFFFE, 0x0CFFFF, 0x0DFFFE,
	        0x0DFFFF, 0x0EFFFE, 0x0EFFFF, 0x0FFFFE, 0x0FFFFF, 0x10FFFE, 0x10FFFF,
	                                                                                     };

	for (integer i = 0; i < SourceNoncharSurrogatesCharsCount; i++)
	{
		ulong noncharSurrogatesChars[2] = { SourceNoncharSurrogatesChars[i], NullTerminator };
		IS_THROW(UTF32Helper::Check(noncharSurrogatesChars), InvalidArgumentException);
	}
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты памяти
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"

#include <memory.h>

namespace Bru
{

//===========================================================================//

TEST(MemoryCopy)
{
	const integer ElementsCount = 16;

	byte * source = new byte[ElementsCount];
	for (integer i = 0; i < ElementsCount; i++)
	{
		source[i] = i;
	}

	byte * destination_one = new byte[ElementsCount];
	for (integer i = 0; i < ElementsCount; i++)
	{
		destination_one[i] = 0;
	}
	Memory::Copy(destination_one, source, ElementsCount * sizeof(byte));

	byte * destination_two = new byte[ElementsCount];
	Memory::Copy(destination_two, destination_one, ElementsCount * sizeof(byte));

	delete[] destination_one;

	for (integer i = 0; i < ElementsCount; i++)
	{
		ARE_EQUAL(source[i], destination_two[i]);
	}

	delete[] source;
	delete[] destination_two;
}

//===========================================================================//

TEST(MemoryCopyExceptions)
{
	byte source[2] = { 128, 128 };
	byte destination[2] = { 128, 128 };

	IS_THROW(Memory::Copy(NullPtr, source, 2 * sizeof(byte)), InvalidArgumentException);
	IS_THROW(Memory::Copy(destination, NullPtr, 2 * sizeof(byte)), InvalidArgumentException);
	IS_THROW(Memory::Copy(destination, destination, 2 * sizeof(byte)), InvalidArgumentException);
}

//===========================================================================//

TEST(MemoryFillWithNull)
{
	const integer ElementsCount = 2;
	byte source[ElementsCount] = { 128, 128 };

	Memory::FillWithNull(source, ElementsCount * sizeof(byte));

	for (integer i = 0; i < ElementsCount; i++)
	{
		ARE_EQUAL(0, source[i]);
	}
}

//===========================================================================//

TEST(MemoryFillWithNullExceptions)
{
	const integer ElementsCount = 2;
	byte source[ElementsCount] = { 128, 128 };

	IS_THROW(Memory::FillWithNull(NullPtr, ElementsCount * sizeof(byte)), InvalidArgumentException);
	IS_THROW(Memory::FillWithNull(source, 0), InvalidArgumentException);
}

//===========================================================================//

TEST(MemoryFillWithValue)
{
	const integer ElementsCount = 16;
	const byte value = 128;

	byte * source = new byte[ElementsCount];
	for (integer i = 0; i < ElementsCount; i++)
	{
		source[i] = (i + 1) * 2;
	}

	Memory::FillWithValue(source, value, ElementsCount * sizeof(byte));

	for (integer i = 0; i < ElementsCount; i++)
	{
		ARE_EQUAL(value, source[i]);
	}

	delete[] source;
}

//===========================================================================//

TEST(MemoryFillWithValueExceptions)
{
	const integer ElementsCount = 2;
	byte source[ElementsCount] = { 128, 128 };
	const byte value = 128;

	IS_THROW(Memory::FillWithValue(NullPtr, value, ElementsCount * sizeof(byte)), InvalidArgumentException);
	IS_THROW(Memory::FillWithValue(source, value, 0), InvalidArgumentException);
}

//===========================================================================//

TEST(MemoryCompareToLessByCount)
{
	const integer ElementsCount = 2;
	byte firstArray[ElementsCount] = { 28, 128 };
	byte secondArray = 128;

	IS_TRUE(Memory::Compare(&firstArray, &secondArray, sizeof(byte)) == ComapareResult::Less);
}

//===========================================================================//

TEST(MemoryCompareToLessByValues)
{
	const integer ElementsCount = 2;
	byte firstArray[ElementsCount] = { 128, 28 };
	byte secondArray[ElementsCount] = { 128, 128 };

	IS_TRUE(Memory::Compare(&firstArray, &secondArray, ElementsCount * sizeof(byte)) == ComapareResult::Less);
}

//===========================================================================//

TEST(MemoryCompareToEqualByCount)
{
	const integer ElementsCount = 2;
	byte firstArray[ElementsCount] = { 128, 28 };
	byte secondArray[ElementsCount] = { 128, 128 };

	//Размер сравнения - только 1 байт, поэтому результат - ComapareResult::Equals
	IS_TRUE(Memory::Compare(&firstArray, &secondArray, sizeof(byte)) == ComapareResult::Equals);
}

//===========================================================================//

TEST(MemoryCompareToEqualByValues)
{
	const integer ElementsCount = 2;
	byte firstArray[ElementsCount] = { 128, 128 };
	byte secondArray[ElementsCount] = { 128, 128 };

	IS_TRUE(Memory::Compare(&firstArray, &secondArray, ElementsCount * sizeof(byte)) == ComapareResult::Equals);
}

//===========================================================================//

TEST(MemoryCompareToGreaterByCount)
{
	const integer ElementsCount = 2;
	byte firstArray[ElementsCount] = { 128, 14 };
	byte secondArray = 28;

	IS_TRUE(Memory::Compare(&firstArray, &secondArray, sizeof(byte)) == ComapareResult::Greater);
}

//===========================================================================//

TEST(MemoryCompareToGreaterByValues)
{
	const integer ElementsCount = 2;
	byte firstArray[ElementsCount] = { 128, 128 };
	byte secondArray[ElementsCount] = { 128, 28 };

	IS_TRUE(Memory::Compare(&firstArray, &secondArray, ElementsCount * sizeof(byte)) == ComapareResult::Greater);
}

//===========================================================================//

}// namespace Bru

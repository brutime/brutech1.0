//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты строки
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"

namespace Bru
{

//===========================================================================//

TEST(TypesSizes)
{
	IS_TRUE(sizeof(byte) >= 1);
	IS_TRUE(sizeof(integer) >= 4);
	IS_TRUE(sizeof(float) >= 4);
	IS_TRUE(sizeof(double) >= 8);
}

//===========================================================================//

}// namespace Bru

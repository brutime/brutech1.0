//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты AddingOrderedTreeMap
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersHelper.h"
#include "../../Helpers/TreeMapChecker.h"

namespace Bru
{

//===========================================================================//

TEST(AddingOrderedTreeMapGetters)
{
	AddingOrderedTreeMap<integer, string> map;

	map._count = 666;
	ARE_EQUAL(666, map.GetCount());
}

//===========================================================================//

TEST(AddingOrderedTreeMapDefaulConstructor)
{
	AddingOrderedTreeMap<string, string> map;

	ARE_EQUAL(0, map.GetCount());
	IS_TRUE(map._rootNode == NullPtr);
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(0, map._ordinalArray.GetCount());
}

//===========================================================================//

TEST(AddingOrderedTreeListInitializerListConstructor)
{
	AddingOrderedTreeMap<integer, integer> map = { { 1, 1 }, { 13, 13 }, { 54, 54 }, { 41, 41 } };

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, integer>(1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, integer>(13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, integer>(54, 54, TreeMapNodeColor::Black);
	expectedData[3] = TreeMapNodeTestData<integer, integer>(41, 41, TreeMapNodeColor::Red);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<integer, integer>::CheckAddingOrderedTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(AddingOrderedTreeMapCopyConstructor)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> sourceMap = ContainersHelper::CreateTestAddingOrderedMap();

	AddingOrderedTreeMap<integer, DummyCompoundObject> map(sourceMap);

	sourceMap.Clear();

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(TreeMapElementsCount, map._ordinalArray.GetCount());
	IS_TRUE(ContainersHelper::CheckTestAddingOrderedTreeMap(map));
}

//===========================================================================//

TEST(AddingOrderedTreeMapMoveConstructor)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map(std::move(ContainersHelper::CreateTestAddingOrderedMap()));

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(TreeMapElementsCount, map._ordinalArray.GetCount());
	IS_TRUE(ContainersHelper::CheckTestAddingOrderedTreeMap(map));
}

//===========================================================================//

TEST(AddingOrderedTreeMapCopyOperator)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> sourceMap = ContainersHelper::CreateTestAddingOrderedMap();

	AddingOrderedTreeMap<integer, DummyCompoundObject> map;
	map = sourceMap;

	sourceMap.Clear();

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(TreeMapElementsCount, map._ordinalArray.GetCount());
	IS_TRUE(ContainersHelper::CheckTestAddingOrderedTreeMap(map));
}

//===========================================================================//

TEST(AddingOrderedTreeMapCopyOperatorSelf)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map(ContainersHelper::CreateTestAddingOrderedMap());

	map = map;

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(TreeMapElementsCount, map._ordinalArray.GetCount());
	IS_TRUE(ContainersHelper::CheckTestAddingOrderedTreeMap(map));
}

//===========================================================================//

TEST(AddingOrderedTreeMapMoveOperator)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	map = std::move(ContainersHelper::CreateTestAddingOrderedMap());

	ARE_EQUAL(TreeMapElementsCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(TreeMapElementsCount, map._ordinalArray.GetCount());
	IS_TRUE(ContainersHelper::CheckTestAddingOrderedTreeMap(map));
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddKeyValue)
{
	AddingOrderedTreeMap<DummyCompoundObject, DummyCompoundObject> map;

	map.Add(d1, d1);

	const integer FirstExpectedDataCount = 1;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> firstExpectedData(FirstExpectedDataCount);
	firstExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1,
	                       TreeMapNodeColor::Black);

	ARE_EQUAL(FirstExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(FirstExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE(
	    (TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, firstExpectedData)));

	//===========================================================================//

	map.Add(d13, d13);

	const integer SecondExpectedDataCount = 2;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> secondExpectedData(SecondExpectedDataCount);
	secondExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1,
	                        TreeMapNodeColor::Black);
	secondExpectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13,
	                        TreeMapNodeColor::Red);

	ARE_EQUAL(SecondExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(SecondExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE(
	    (TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, secondExpectedData)));

	//===========================================================================//

	map.Add(d54, d54);

	const integer ThirdExpectedDataCount = 3;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> thirdExpectedData(ThirdExpectedDataCount);
	thirdExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1, TreeMapNodeColor::Red);
	thirdExpectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13,
	                       TreeMapNodeColor::Black);
	thirdExpectedData[2] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d54, d54,
	                       TreeMapNodeColor::Red);

	ARE_EQUAL(ThirdExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ThirdExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE(
	    (TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, thirdExpectedData)));

	//===========================================================================//

	map.Add(d41, d41);

	const integer FourthExpectedDataCount = 4;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> fourthExpectedData(FourthExpectedDataCount);
	fourthExpectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1,
	                        TreeMapNodeColor::Black);
	fourthExpectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13,
	                        TreeMapNodeColor::Black);
	fourthExpectedData[2] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d54, d54,
	                        TreeMapNodeColor::Black);
	fourthExpectedData[3] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d41, d41,
	                        TreeMapNodeColor::Red);

	ARE_EQUAL(FourthExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(FourthExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE(
	    (TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, fourthExpectedData)));
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddKeyValueExceptions)
{
	AddingOrderedTreeMap<DummyCompoundObject, DummyCompoundObject> map;

	//Нарушение уникальностей ключей
	map.Add(d1, d1);
	IS_THROW(map.Add(d1, d1), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveKey)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(13, d13);
	map.Add(54, d54);
	map.Add(41, d41);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(1, d1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(13, d13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Black);
	expectedData[3] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Red);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveKeyExceptions)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	//Нарушение уникальностей ключей
	map.Add(1, d1);
	IS_THROW(map.Add(1, d1), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveValue)
{
	AddingOrderedTreeMap<DummyCompoundObject, integer> map;

	map.Add(d1, 1);
	map.Add(d13, 13);
	map.Add(d54, 54);
	map.Add(d41, 41);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<DummyCompoundObject, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<DummyCompoundObject, integer>(d1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<DummyCompoundObject, integer>(d13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<DummyCompoundObject, integer>(d54, 54, TreeMapNodeColor::Black);
	expectedData[3] = TreeMapNodeTestData<DummyCompoundObject, integer>(d41, 41, TreeMapNodeColor::Red);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<DummyCompoundObject, integer>::CheckAddingOrderedTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveValueExceptions)
{
	AddingOrderedTreeMap<DummyCompoundObject, integer> map;

	//Нарушение уникальностей ключей
	map.Add(d1, 1);
	IS_THROW(map.Add(d1, 1), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveKeyValue)
{
	AddingOrderedTreeMap<integer, integer> map;

	map.Add(1, 1);
	map.Add(13, 13);
	map.Add(54, 54);
	map.Add(41, 41);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, integer>(1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, integer>(13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, integer>(54, 54, TreeMapNodeColor::Black);
	expectedData[3] = TreeMapNodeTestData<integer, integer>(41, 41, TreeMapNodeColor::Red);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<integer, integer>::CheckAddingOrderedTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveKeyValueExceptions)
{
	AddingOrderedTreeMap<integer, integer> map;

	//Нарушение уникальностей ключей
	map.Add(1, 1);
	IS_THROW(map.Add(1, 1), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddKeyValuePair)
{
	AddingOrderedTreeMap<DummyCompoundObject, DummyCompoundObject> map;

	KeyValuePair<DummyCompoundObject, DummyCompoundObject> firstKeyValuePair(d1, d1);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> secondKeyValuePair(d13, d13);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> thirdKeyValuePair(d54, d54);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> fourthKeyValuePair(d41, d41);

	map.Add(firstKeyValuePair);
	map.Add(secondKeyValuePair);
	map.Add(thirdKeyValuePair);
	map.Add(fourthKeyValuePair);

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d1, d1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d13, d13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d54, d54, TreeMapNodeColor::Black);
	expectedData[3] = TreeMapNodeTestData<DummyCompoundObject, DummyCompoundObject>(d41, d41, TreeMapNodeColor::Red);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<DummyCompoundObject, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddKeyValuePairExceptions)
{
	AddingOrderedTreeMap<DummyCompoundObject, DummyCompoundObject> map;

	//Нарушение уникальностей ключей
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d1, d1);
	map.Add(keyValuePair);
	IS_THROW(map.Add(keyValuePair), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveKeyValuePair)
{
	AddingOrderedTreeMap<integer, integer> map;

	map.Add(KeyValuePair<integer, integer>(1, 1));
	map.Add(KeyValuePair<integer, integer>(13, 13));
	map.Add(KeyValuePair<integer, integer>(54, 54));
	map.Add(KeyValuePair<integer, integer>(41, 41));

	const integer ExpectedDataCount = 4;
	Array<TreeMapNodeTestData<integer, integer>> expectedData(ExpectedDataCount);
	expectedData[0] = TreeMapNodeTestData<integer, integer>(1, 1, TreeMapNodeColor::Black);
	expectedData[1] = TreeMapNodeTestData<integer, integer>(13, 13, TreeMapNodeColor::Black);
	expectedData[2] = TreeMapNodeTestData<integer, integer>(54, 54, TreeMapNodeColor::Black);
	expectedData[3] = TreeMapNodeTestData<integer, integer>(41, 41, TreeMapNodeColor::Red);

	ARE_EQUAL(ExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<integer, integer>::CheckAddingOrderedTreeMap(map, expectedData)));
}

//===========================================================================//

TEST(AddingOrderedTreeMapAddMoveKeyValuePairExceptions)
{
	AddingOrderedTreeMap<integer, integer> map;

	//Нарушение уникальностей ключей
	map.Add(KeyValuePair<integer, integer>(1, 1));
	IS_THROW(map.Add(KeyValuePair<integer, integer>(1, 1)), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapRemove)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(13, d13);
	map.Add(54, d54);
	map.Add(41, d41);

	//===========================================================================//

	map.Remove(13);

	const integer FirstExpectedDataCount = 3;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> firstExpectedData(FirstExpectedDataCount);
	firstExpectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(1, d1, TreeMapNodeColor::Black);
	firstExpectedData[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Black);
	firstExpectedData[2] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Black);

	ARE_EQUAL(FirstExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(FirstExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, firstExpectedData)));

	//===========================================================================//

	map.Remove(1);

	const integer SecondExpectedDataCount = 2;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> secondExpectedData(SecondExpectedDataCount);
	secondExpectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Red);
	secondExpectedData[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Black);

	ARE_EQUAL(SecondExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(SecondExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, secondExpectedData)));

	//===========================================================================//

	map.Remove(41);

	const integer ThirdExpectedDataCount = 1;
	Array<TreeMapNodeTestData<integer, DummyCompoundObject>> thirdExpectedData(ThirdExpectedDataCount);
	thirdExpectedData[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Black);

	ARE_EQUAL(ThirdExpectedDataCount, map.GetCount());
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(ThirdExpectedDataCount, map._ordinalArray.GetCount());
	IS_TRUE((TreeMapChecker<integer, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, thirdExpectedData)));

	//===========================================================================//

	map.Remove(54);

	ARE_EQUAL(0, map.GetCount());
	IS_TRUE(map._rootNode == NullPtr);
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(0, map._ordinalArray.GetCount());
}

//===========================================================================//

TEST(AddingOrderedTreeMapRemoveExceptions)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	map.Add(1, DummyCompoundObject(1));
	map.Add(2, DummyCompoundObject(2));

	IS_THROW(map.Remove(3), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapOperatorSetIntegerKey)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(2, d2);
	map.Add(3, d3);
	map.Add(4, d4);

	IS_TRUE(map[1] == d1);
	IS_TRUE(map[2] == d2);
	IS_TRUE(map[3] == d3);
	IS_TRUE(map[4] == d4);

	//запутываем :)
	map[1] = d2;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 1, d2);

	map[2] = d3;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 2, d3);

	map[3] = d1;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 3, d1);

	map[4] = d4;
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 4, d4);

	ARE_EQUAL(4, map.GetCount());
	ARE_EQUAL(4, map._ordinalArray.GetCount());

	IS_TRUE(map[1] == d2);
	IS_TRUE(map[2] == d3);
	IS_TRUE(map[3] == d1);
	IS_TRUE(map[4] == d4);
}

//===========================================================================//

TEST(AddingOrderedTreeMapOperatorSetStringKey)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	IS_TRUE(map["one"] == d1);
	IS_TRUE(map["two"] == d2);
	IS_TRUE(map["three"] == d3);
	IS_TRUE(map["four"] == d4);

	//запутываем :)
	map["one"] = d2;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d2);

	map["two"] = d3;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "two", d3);

	map["three"] = d1;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "three", d1);

	map["four"] = d4;
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "four", d4);

	ARE_EQUAL(4, map.GetCount());
	ARE_EQUAL(4, map._ordinalArray.GetCount());

	IS_TRUE(map["one"] == d2);
	IS_TRUE(map["two"] == d3);
	IS_TRUE(map["three"] == d1);
	IS_TRUE(map["four"] == d4);
}

//===========================================================================//

TEST(AddingOrderedTreeMapOperatorSetExceptions)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;

	map.Add("one", DummyCompoundObject(1));
	map.Add("two", DummyCompoundObject(2));

	IS_THROW(map["three"] = DummyCompoundObject(1), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapOperatorGetIntegerKey)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	map.Add(1, d1);
	map.Add(2, d2);
	map.Add(3, d3);
	map.Add(4, d4);

	IS_TRUE(map[1] == d1);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 1, d1);

	IS_TRUE(map[2] == d2);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 2, d2);

	IS_TRUE(map[3] == d3);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 3, d3);

	IS_TRUE(map[4] == d4);
	TreeMapChecker<integer, DummyCompoundObject>::CheckNode(map._lastFoundNode, 4, d4);
}

//===========================================================================//

TEST(AddingOrderedTreeMapOperatorGetStringKey)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	IS_TRUE(map["one"] == d1);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	IS_TRUE(map["two"] == d2);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "two", d2);

	IS_TRUE(map["three"] == d3);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "three", d3);

	IS_TRUE(map["four"] == d4);
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "four", d4);
}

//===========================================================================//

TEST(AddingOrderedTreeMapOperatorGetExceptions)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;

	map.Add("one", DummyCompoundObject(1));
	map.Add("two", DummyCompoundObject(2));

	IS_THROW(map["three"] == DummyCompoundObject(3), InvalidArgumentException);
}

//===========================================================================//

TEST(AddingOrderedTreeMapContains)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	IS_TRUE(!map.Contains(""));
	IS_TRUE(map._lastFoundNode == NullPtr);

	IS_TRUE(!map.Contains("zero"));
	IS_TRUE(map._lastFoundNode == NullPtr);

	IS_TRUE(map.Contains("one"));
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	IS_TRUE(map.Contains("two"));
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "two", d2);

	IS_TRUE(map.Contains("three"));
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "three", d3);

	IS_TRUE(map.Contains("four"));
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "four", d4);

	IS_TRUE(!map.Contains("five"));
	IS_TRUE(map._lastFoundNode == NullPtr);
}

//===========================================================================//

TEST(AddingOrderedTreeMapClear)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map.Add("two", d2);
	map.Add("three", d3);
	map.Add("four", d4);

	map.Clear();

	ARE_EQUAL(0, map.GetCount());
	IS_TRUE(map._rootNode == NullPtr);
	IS_TRUE(map._lastFoundNode == NullPtr);
	ARE_EQUAL(0, map._ordinalArray.GetCount());

	IS_TRUE(!map.Contains("one"));
	IS_TRUE(!map.Contains("two"));
	IS_TRUE(!map.Contains("three"));
	IS_TRUE(!map.Contains("four"));
}

//===========================================================================//

TEST(AddingOrderedTreeMapIsEmpty)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;
	IS_TRUE(map.IsEmpty());

	map.Add("one", d1);
	IS_FALSE(map.IsEmpty());
}

//===========================================================================//

TEST(AddingOrderedTreeMapCreateIterator)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestAddingOrderedMap();

	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it = map.CreateIterator();

	IS_TRUE(it._owner == &map);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(AddingOrderedTreeMapBegin)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestAddingOrderedMap();

	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it = map.begin();

	IS_TRUE(it._owner == &map);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(AddingOrderedTreeMapEnd)
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestAddingOrderedMap();

	AddingOrderedTreeMapIterator<integer, DummyCompoundObject> it = map.end();

	IS_TRUE(it._owner == &map);
	ARE_EQUAL(map.GetCount(), it._cursor);
}

//===========================================================================//

TEST(AddingOrderedTreeMapLastFoundNode)
{
	AddingOrderedTreeMap<string, DummyCompoundObject> map;

	map.Add("one", d1);
	map["one"];

	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	map.Add("two", d2);
	IS_TRUE(map._lastFoundNode == NullPtr);

	map["one"];
	TreeMapChecker<string, DummyCompoundObject>::CheckNode(map._lastFoundNode, "one", d1);

	map.Remove("two");
	IS_TRUE(map._lastFoundNode == NullPtr);
}

//===========================================================================//

class AddingOrderedTreeMapHierarchy
{
public:
	AddingOrderedTreeMapHierarchy() :
		friends()
	{
	}

	AddingOrderedTreeMapHierarchy(const AddingOrderedTreeMapHierarchy & otherAddingOrderedTreeMapHierarchy) :
		friends(otherAddingOrderedTreeMapHierarchy.friends)
	{
	}

	virtual ~AddingOrderedTreeMapHierarchy()
	{
	}

	AddingOrderedTreeMapHierarchy & operator =(const AddingOrderedTreeMapHierarchy & otherAddingOrderedTreeMapHierarchy)
	{
		friends = otherAddingOrderedTreeMapHierarchy.friends;
		return *this;
	}

	AddingOrderedTreeMap<string, AddingOrderedTreeMapHierarchy> friends;
};

//===========================================================================//

TEST(AddingOrderedTreeMapHierarchyClass)
{
	//Просто должен проходить
	AddingOrderedTreeMapHierarchy addingOrderedTreeMapHierarchy;
	ARE_EQUAL(0, addingOrderedTreeMapHierarchy.friends.GetCount());
}

//===========================================================================//

}// namespace Bru

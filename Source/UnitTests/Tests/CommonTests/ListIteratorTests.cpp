//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты итератора массива
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(ListIteratorConstructor)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	ListIterator<DummyCompoundObject> it(&list, list._head);

	IS_TRUE(it._owner == &list);
	IS_TRUE(it._cursor == list._head);
}

//===========================================================================//

TEST(ListIteratorForEmpty)
{
	List<DummyCompoundObject> list;
	ListIterator<DummyCompoundObject> it(&list, 0);

	integer count = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(ListIteratorForOneElement)
{
	List<DummyCompoundObject> list;
	list.Add(d1);
	ListIterator<DummyCompoundObject> it(&list, 0);

	integer count = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &list);
		IS_TRUE(it._cursor->Next == NullPtr);
		ARE_EQUAL(d1, it._cursor->Value);
		ARE_EQUAL(d1, it.GetCurrent());
		count++;
	}

	ARE_EQUAL(1, count);
}

//===========================================================================//

TEST(ListIteratorFor)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	ListIterator<DummyCompoundObject> it(&list, 0);

	ListElement<DummyCompoundObject> * element = list._head;
	integer index = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &list);
		IS_TRUE(it._cursor->Next == element->Next);
		ARE_EQUAL(element->Value, it._cursor->Value);
		ARE_EQUAL(TestDummyes[index], it.GetCurrent());
		element = element->Next;
		index++;
	}

	ARE_EQUAL(TestDummyesCount, index);
}

//===========================================================================//

TEST(ListIteratorRangeForEmpty)
{
	List<DummyCompoundObject> list;

	integer count = 0;
	for (const DummyCompoundObject & object : list)
	{
		object.GetValue();
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(ListIteratorRangeForOneElement)
{
	List<DummyCompoundObject> list;
	list.Add(d1);

	integer count = 0;
	for (const DummyCompoundObject & object : list)
	{
		ARE_EQUAL(d1, object);
		count++;
	}

	ARE_EQUAL(1, count);
}

//===========================================================================//

TEST(ListIteratorRangeFor)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	integer index = 0;
	for (const DummyCompoundObject & object : list)
	{
		ARE_EQUAL(TestDummyes[index], object);
		index++;
	}

	ARE_EQUAL(TestDummyesCount, index);
}

//===========================================================================//

TEST(ListIteratorExceptions)
{
	IS_THROW(ListIterator<DummyCompoundObject>(NullPtr, NullPtr), InvalidArgumentException);

	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	ListIterator<DummyCompoundObject> it(&list, list._head);

	it._cursor = NullPtr;
	IS_THROW(it.GetCurrent(), IteratorOutOfBoundsException);

	for (it.First(); it.HasNext(); it.Next())
	{
	}
	IS_THROW(it.Next(), IteratorOutOfBoundsException);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса хранящего длину строки
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"

namespace Bru
{

//===========================================================================//

TEST(UTFLengthDefaultAssign)
{
	UTFLength stringLength;

	ARE_EQUAL(0, stringLength.LengthInChars);
	ARE_EQUAL(0, stringLength.LengthInComponents);
}

//===========================================================================//

TEST(UTFLengthAssign)
{
	UTFLength stringLength(11, 22);

	ARE_EQUAL(11, stringLength.LengthInChars);
	ARE_EQUAL(22, stringLength.LengthInComponents);
}

//===========================================================================//

TEST(UTFLengthAssignOtherStringLength)
{
	UTFLength sourceStringLength(11, 22);
	UTFLength stringLength(sourceStringLength);

	ARE_EQUAL(11, stringLength.LengthInChars);
	ARE_EQUAL(22, stringLength.LengthInComponents);
}

//===========================================================================//

TEST(UTFLengthOperatorAssign)
{
	UTFLength sourceStringLength(11, 22);
	UTFLength stringLength;
	stringLength = sourceStringLength;

	ARE_EQUAL(11, stringLength.LengthInChars);
	ARE_EQUAL(22, stringLength.LengthInComponents);
}

//===========================================================================//

TEST(UTFLengthSetters)
{
	UTFLength stringLength;

	stringLength.LengthInChars = 11;
	stringLength.LengthInComponents = 22;

	ARE_EQUAL(11, stringLength.LengthInChars);
	ARE_EQUAL(22, stringLength.LengthInComponents);
}

//===========================================================================//

TEST(UTFLengthGetters)
{
	UTFLength stringLength(11, 22);

	ARE_EQUAL(11, stringLength.LengthInChars);
	ARE_EQUAL(22, stringLength.LengthInComponents);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты TreeMapIterator
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(TreeMapIteratorConstructor)
{
	TreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestTreeMap();
	TreeMapIterator<integer, DummyCompoundObject> it(&map, map._mostLeftNode);

	IS_TRUE(it._owner == &map);
	ARE_EQUAL(map.FindNode(TreeMapExpectedKeys[0]), it._cursor);
}

//===========================================================================//

TEST(TreeMapIteratorForEmpty)
{
	TreeMap<integer, DummyCompoundObject> map;
	TreeMapIterator<integer, DummyCompoundObject> it(&map, map._mostLeftNode);

	integer count = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(TreeMapIteratorForOnePair)
{
	TreeMap<integer, DummyCompoundObject> map;
	map.Add(1, d1);
	TreeMapIterator<integer, DummyCompoundObject> it(&map, map._mostLeftNode);

	integer count = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &map);
		ARE_EQUAL(map.FindNode(1), it._cursor);
		IS_TRUE(it.GetCurrent() == map.FindNode(1)->Pair);
		ARE_EQUAL(1, it.GetCurrent().GetKey());
		ARE_EQUAL(d1, it.GetCurrent().GetValue());
		count++;
	}

	ARE_EQUAL(1, count);
}

//===========================================================================//

TEST(TreeMapIteratorFor)
{
	TreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestTreeMap();
	TreeMapIterator<integer, DummyCompoundObject> it(&map, map._mostLeftNode);

	integer index = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &map);
		ARE_EQUAL(map.FindNode(TreeMapExpectedKeys[index]), it._cursor);
		IS_TRUE(it.GetCurrent() == map.FindNode(TreeMapExpectedKeys[index])->Pair);
		ARE_EQUAL(TreeMapExpectedKeys[index], it.GetCurrent().GetKey());
		ARE_EQUAL(TreeMapExpectedValues[index], it.GetCurrent().GetValue());
		index++;
	}

	ARE_EQUAL(TreeMapElementsCount, index);
}

//===========================================================================//

TEST(TreeMapIteratorRangeForEmpty)
{
	TreeMap<integer, DummyCompoundObject> map;

	integer count = 0;
	for (auto & pair : map)
	{
		pair.GetKey();
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(TreeMapIteratorRangeForOnePair)
{
	TreeMap<integer, DummyCompoundObject> map;
	map.Add(1, d1);

	integer count = 0;
	for (auto & pair : map)
	{
		ARE_EQUAL(1, pair.GetKey());
		ARE_EQUAL(d1, pair.GetValue());
		count++;
	}

	ARE_EQUAL(1, count);
}

//===========================================================================//

TEST(TreeMapIteratorRangeFor)
{
	TreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestTreeMap();

	integer index = 0;
	for (auto & pair : map)
	{
		ARE_EQUAL(TreeMapExpectedKeys[index], pair.GetKey());
		ARE_EQUAL(TreeMapExpectedValues[index], pair.GetValue());
		index++;
	}

	ARE_EQUAL(TreeMapElementsCount, index);
}

//===========================================================================//

TEST(TreeMapIteratorExceptions)
{
	IS_THROW((TreeMapIterator<integer, DummyCompoundObject>(NullPtr, NullPtr)), InvalidArgumentException);

	TreeMap<integer, DummyCompoundObject> map = ContainersHelper::CreateTestTreeMap();
	TreeMapIterator<integer, DummyCompoundObject> it(&map, map._mostLeftNode);

	it._cursor = NullPtr;

	IS_THROW(it.GetCurrent().GetKey(), IteratorOutOfBoundsException);
	IS_THROW(it.GetCurrent().GetValue(), IteratorOutOfBoundsException);
	IS_THROW(it.GetCurrent(), IteratorOutOfBoundsException);

	for (it.First(); it.HasNext(); it.Next())
	{
	}
	IS_THROW(it.Next(), IteratorOutOfBoundsException);
}

//===========================================================================//

}// namespace Bru

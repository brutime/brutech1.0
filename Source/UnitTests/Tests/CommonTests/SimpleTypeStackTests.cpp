//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты стека
//              Проверки Push(const T &) проходит в StackTests
//              Проверки Push(T &&) проходит в SimplyTypeStackTests
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(SimpleTypeStackGetters)
{
	Stack<float>::SimpleType stack;

	stack._count = 10;
	ARE_EQUAL(10, stack.GetCount());
}

//===========================================================================//

TEST(SimpleTypeStackDefaultConstructor)
{
	Stack<float>::SimpleType stack;

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeStackConstructor)
{
	Stack<float>::SimpleType stack(10);

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(10, stack._reservedCount);
	IS_TRUE(stack._dataPtr != NullPtr);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(SimpleTypeStackConstructorExceptions)
{
	IS_THROW(Stack<float>::SimpleType stack(-1), InvalidArgumentException);
}
#endif

//===========================================================================//

TEST(SimpleTypeStackInitializerListConstructor)
{
	const integer ExpectedArrayCount = 10;
	const DummyCompoundObject ExpectedArray[ExpectedArrayCount] = { 0.0f, 1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f,
	        8.8f, 9.9f
	                                                              };

	Stack<float>::SimpleType stack = { 0.0f, 1.1f, 2.2f, 3.3f, 4.4f, 5.5f, 6.6f, 7.7f, 8.8f, 9.9f };

	ARE_EQUAL(ExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(ExpectedArrayCount, stack._reservedCount);
	ARE_ARRAYS_EQUAL(ExpectedArray, stack, ExpectedArrayCount);
}

//===========================================================================//

TEST(SimpleTypeStackCopyConstructor)
{
	Stack<float>::SimpleType sourceStack = ContainersHelper::CreateTestFloatsStack();

	Stack<float>::SimpleType stack(sourceStack);

	sourceStack.Clear();

	ARE_EQUAL(TestFloatsCount, stack.GetCount());
	ARE_EQUAL(TestFloatsCount, stack._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, stack, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeStackMoveConstructor)
{
	Stack<float>::SimpleType stack(std::move(ContainersHelper::CreateTestFloatsStack()));

	ARE_EQUAL(TestFloatsCount, stack.GetCount());
	ARE_EQUAL(TestFloatsCount, stack._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, stack, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeStackCopyOperator)
{
	Stack<float>::SimpleType sourceStack = ContainersHelper::CreateTestFloatsStack();

	Stack<float>::SimpleType stack;
	stack = sourceStack;

	sourceStack.Clear();

	ARE_EQUAL(TestFloatsCount, stack.GetCount());
	ARE_EQUAL(TestFloatsCount, stack._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, stack, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeStackCopyOperatorSelf)
{
	Stack<float>::SimpleType stack = ContainersHelper::CreateTestFloatsStack();

	stack = stack;

	ARE_EQUAL(TestFloatsCount, stack.GetCount());
	ARE_EQUAL(TestFloatsCount, stack._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, stack, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeStackMoveOperator)
{
	Stack<float>::SimpleType stack;
	stack = ContainersHelper::CreateTestFloatsStack();

	ARE_EQUAL(TestFloatsCount, stack.GetCount());
	ARE_EQUAL(TestFloatsCount, stack._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, stack, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeStackReserve)
{
	integer const smallerCount = Math::Floor(TestFloatsCount / 2.0f);
	Stack<float>::SimpleType stack(smallerCount);

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(smallerCount, stack._reservedCount);

	integer const greaterCount = TestFloatsCount;

	stack.Reserve(greaterCount);

	for (integer i = 0; i < greaterCount; i++)
	{
		stack.Push(TestFloats[i]);
	}

	ARE_EQUAL(greaterCount, stack._count);
	ARE_EQUAL(greaterCount, stack._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, stack, greaterCount, Math::Accuracy);

	stack.Reserve(smallerCount);

	ARE_EQUAL(smallerCount, stack._count);
	ARE_EQUAL(smallerCount, stack._reservedCount);
	ARE_ARRAYS_CLOSE(TestFloats, stack, smallerCount, Math::Accuracy);

	stack.Reserve(0);

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeStackPush)
{
	Stack<byte>::SimpleType stack;

	stack.Push(1);

	const integer FirstExpectedArrayCount = 1;
	const byte FirstExpectedArray[FirstExpectedArrayCount] = { 1 };

	ARE_EQUAL(FirstExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(1, stack._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedArray, stack, FirstExpectedArrayCount);

	stack.Push(2);

	const integer SecondExpectedArrayCount = 2;
	const byte SecondExpectedArray[SecondExpectedArrayCount] = { 1, 2 };

	ARE_EQUAL(SecondExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(2, stack._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedArray, stack, SecondExpectedArrayCount);

	stack.Push(3);

	const integer ThirdExpectedArrayCount = 3;
	const byte ThirdExpectedArray[ThirdExpectedArrayCount] = { 1, 2, 3 };

	ARE_EQUAL(ThirdExpectedArrayCount, stack.GetCount());
	ARE_EQUAL(4, stack._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedArray, stack, ThirdExpectedArrayCount);
}

//===========================================================================//

TEST(SimpleTypeStackTopPop)
{
	Stack<byte>::SimpleType stack;

	stack.Push(1);
	stack.Push(2);
	stack.Push(3);
	stack.Push(4);

	IS_TRUE(stack.Top() == 4);
	stack.Pop();

	const integer FirstExpectedStackCount = 3;
	const byte FirstExpectedStack[FirstExpectedStackCount] = { 1, 2, 3 };

	ARE_EQUAL(FirstExpectedStackCount, stack.GetCount());
	ARE_EQUAL(4, stack._reservedCount);
	ARE_ARRAYS_EQUAL(FirstExpectedStack, stack, FirstExpectedStackCount);

	IS_TRUE(stack.Top() == 3);
	stack.Pop();

	const integer SecondExpectedStackCount = 2;
	const byte SecondExpectedStack[SecondExpectedStackCount] = { 1, 2 };

	ARE_EQUAL(SecondExpectedStackCount, stack.GetCount());
	ARE_EQUAL(4, stack._reservedCount);
	ARE_ARRAYS_EQUAL(SecondExpectedStack, stack, SecondExpectedStackCount);

	IS_TRUE(stack.Top() == 2);
	stack.Pop();

	const integer ThirdExpectedStackCount = 1;
	const byte ThirdExpectedStack[ThirdExpectedStackCount] = { 1 };

	ARE_EQUAL(ThirdExpectedStackCount, stack.GetCount());
	ARE_EQUAL(2, stack._reservedCount);
	ARE_ARRAYS_EQUAL(ThirdExpectedStack, stack, ThirdExpectedStackCount);

	IS_TRUE(stack.Top() == 1);
	stack.Pop();

	ARE_EQUAL(0, stack.GetCount());
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeStackTopExceptions)
{
	Stack<float>::SimpleType emptyStack;
	IS_THROW(emptyStack.Top(), InvalidStateException);
}

//===========================================================================//

TEST(SimpleTypeStackPopExceptions)
{
	Stack<byte>::SimpleType emptyStack;
	IS_THROW(emptyStack.Pop(), InvalidStateException);

	Stack<byte>::SimpleType stack;

	stack.Push(1);
	stack.Pop();

	IS_THROW(stack.Pop(), InvalidStateException);
}

//===========================================================================//

TEST(SimpleTypeStackGet)
{
	Stack<float>::SimpleType stack;
	integer expectedReservedCount = 0;

	for (integer i = 0; i < TestFloatsCount; i++)
	{
		if (stack.GetCount() == expectedReservedCount)
		{
			expectedReservedCount += stack.GetDeltaGrowCount();
		}

		stack.Push(TestFloats[i]);
	}

	ARE_EQUAL(TestFloatsCount, stack.GetCount());
	ARE_EQUAL(expectedReservedCount, stack._reservedCount);
	//Тут и провериться []
	ARE_ARRAYS_CLOSE(TestFloats, stack, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(SimpleTypeStackGetExceptions)
{
	Stack<float>::SimpleType emptyStack;
	IS_THROW(emptyStack[-1], IndexOutOfBoundsException);
	IS_THROW(emptyStack[0], IndexOutOfBoundsException);

	Stack<float>::SimpleType stack(TestFloatsCount);
	IS_THROW(stack[-1], IndexOutOfBoundsException);
	IS_THROW(stack[TestFloatsCount], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(SimpleTypeStackClear)
{
	Stack<float>::SimpleType stack = ContainersHelper::CreateTestFloatsStack();

	stack.Clear();

	ARE_EQUAL(0, stack._count);
	ARE_EQUAL(0, stack._reservedCount);
	IS_TRUE(stack._dataPtr == NullPtr);
}

//===========================================================================//

TEST(SimpleTypeStackIsEmpty)
{
	Stack<byte>::SimpleType stack;
	ARE_EQUAL(true, stack.IsEmpty());

	stack.Push(1);
	ARE_EQUAL(false, stack.IsEmpty());
}

//===========================================================================//

}// namespace Bru

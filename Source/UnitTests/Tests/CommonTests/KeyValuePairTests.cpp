//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты пары ключ-значение
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersTestData.h"
#include "../../Dummies/DummyCompoundObject.h"

namespace Bru
{

//===========================================================================//

TEST(KeyValuePairGetters)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d1, d2);

	keyValuePair._key = d3;
	keyValuePair._value = d4;

	ARE_EQUAL(d3, keyValuePair.GetKey());
	ARE_EQUAL(d4, keyValuePair.GetValue());

	keyValuePair.GetValue() = d5;
	ARE_EQUAL(d5, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairConstructor)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d1, d2);

	ARE_EQUAL(d1, keyValuePair.GetKey());
	ARE_EQUAL(d2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairConstructorMoveKey)
{
	KeyValuePair<integer, DummyCompoundObject> keyValuePair(1, d2);

	ARE_EQUAL(1, keyValuePair.GetKey());
	ARE_EQUAL(d2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairConstructorMoveValue)
{
	KeyValuePair<DummyCompoundObject, integer> keyValuePair(d1, 2);

	ARE_EQUAL(d1, keyValuePair.GetKey());
	ARE_EQUAL(2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairConstructorMoveKeyValue)
{
	KeyValuePair<integer, integer> keyValuePair(1, 2);

	ARE_EQUAL(1, keyValuePair.GetKey());
	ARE_EQUAL(2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairCopyConstructor)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> sourceKeyValuePair(d1, d2);

	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(sourceKeyValuePair);

	ARE_EQUAL(d1, keyValuePair.GetKey());
	ARE_EQUAL(d2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairMoveConstructor)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(
	    std::move(KeyValuePair<DummyCompoundObject, DummyCompoundObject>(d1, d2)));

	ARE_EQUAL(d1, keyValuePair.GetKey());
	ARE_EQUAL(d2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairCopyOperator)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> sourceKeyValuePair(d1, d2);

	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d3, d4);
	keyValuePair = sourceKeyValuePair;

	ARE_EQUAL(d1, keyValuePair.GetKey());
	ARE_EQUAL(d2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairCopyOperatorSelf)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d1, d2);

	keyValuePair = keyValuePair;

	ARE_EQUAL(d1, keyValuePair.GetKey());
	ARE_EQUAL(d2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairMoveOperator)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d3, d4);
	keyValuePair = std::move(KeyValuePair<DummyCompoundObject, DummyCompoundObject>(d1, d2));

	ARE_EQUAL(d1, keyValuePair.GetKey());
	ARE_EQUAL(d2, keyValuePair.GetValue());
}

//===========================================================================//

TEST(KeyValuePairEquals)
{
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> keyValuePair(d1, d2);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> equalsKeyValuePair(d1, d2);
	KeyValuePair<DummyCompoundObject, DummyCompoundObject> notEqualsKeyValuePair(d3, d5);

	IS_TRUE(keyValuePair == equalsKeyValuePair);
	IS_TRUE(keyValuePair != notEqualsKeyValuePair);
}

//===========================================================================//

TEST(KeyValuePairLess)
{
	KeyValuePair<integer, DummyCompoundObject> keyValuePair(22, d22);
	KeyValuePair<integer, DummyCompoundObject> lessKeyValuePair(11, d11);

	IS_TRUE(keyValuePair > lessKeyValuePair);
}

//===========================================================================//

TEST(KeyValuePairGreater)
{
	KeyValuePair<integer, DummyCompoundObject> keyValuePair(11, d11);
	KeyValuePair<integer, DummyCompoundObject> greaterKeyValuePair(22, d22);

	IS_TRUE(keyValuePair < greaterKeyValuePair);
}

//===========================================================================//

}// namespace Bru

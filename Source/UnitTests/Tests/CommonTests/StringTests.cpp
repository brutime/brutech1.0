//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты строки
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringTestsHelper.h"
#include "../../Helpers/StringsTestData.h"

namespace Bru
{

//===========================================================================//

TEST(StringDefaultConstructor)
{
	string someString;

	ARE_EQUAL(0, someString.GetLength());
	ARE_EQUAL(0, someString.GetLengthInBytes());
	ARE_EQUAL(string::Empty, someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringEmptyConstructor)
{
	string someString = string::Empty;

	ARE_EQUAL(0, someString.GetLength());
	ARE_EQUAL(0, someString.GetLengthInBytes());
	ARE_EQUAL(string::Empty, someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringBoolConstructor)
{
	string trueString(true);
	ARE_EQUAL(4, trueString.GetLength());
	ARE_EQUAL(4, trueString.GetLengthInBytes());
	ARE_EQUAL("true", trueString);
	ARE_EQUAL(0, trueString._cashedIndex);
	ARE_EQUAL(0, trueString._cashedByteIndex);

	string falseString(false);
	ARE_EQUAL(5, falseString.GetLength());
	ARE_EQUAL(5, falseString.GetLengthInBytes());
	ARE_EQUAL("false", falseString);
	ARE_EQUAL(0, falseString._cashedIndex);
	ARE_EQUAL(0, falseString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCharConstructor)
{
	string someString = 'B';

	ARE_EQUAL(1, someString.GetLength());
	ARE_EQUAL(1, someString.GetLengthInBytes());
	ARE_EQUAL("B", someString);
	ARE_EQUAL('B', someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringByteConstructor)
{
	byte someByte = 66;
	string someString(someByte);

	ARE_EQUAL(2, someString.GetLength());
	ARE_EQUAL(2, someString.GetLengthInBytes());
	ARE_EQUAL("66", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringUnsignedShortConstructor)
{
	string someString = ushort(32000);

	ARE_EQUAL(5, someString.GetLength());
	ARE_EQUAL(5, someString.GetLengthInBytes());
	ARE_EQUAL("32000", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringUnsegnedIntegerConstructor)
{
	string someString = 32000u;

	ARE_EQUAL(5, someString.GetLength());
	ARE_EQUAL(5, someString.GetLengthInBytes());
	ARE_EQUAL("32000", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringUnsegnedLongConstructor)
{
	string someString = ulong(6000000);

	ARE_EQUAL(7, someString.GetLength());
	ARE_EQUAL(7, someString.GetLengthInBytes());
	ARE_EQUAL("6000000", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringIntegerConstructor)
{
	string someString = integer(666);

	ARE_EQUAL(3, someString.GetLength());
	ARE_EQUAL(3, someString.GetLengthInBytes());
	ARE_EQUAL("666", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInteger64Constructor)
{
	string someString = int64(6000000000);

	ARE_EQUAL(10, someString.GetLength());
	ARE_EQUAL(10, someString.GetLengthInBytes());
	ARE_EQUAL("6000000000", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringFloatConstructor)
{
	float someFloat = 666.666f;
	string someString = someFloat;

	ARE_EQUAL(8, someString.GetLength());
	ARE_EQUAL(8, someString.GetLengthInBytes());
	ARE_EQUAL("666.6660", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringUTF8CharConstructor)
{
	utf8_char someChar = "𠀀";
	string someString = someChar;

	ARE_EQUAL(1, someString.GetLength());
	ARE_EQUAL(4, someString.GetLengthInBytes());
	ARE_EQUAL("𠀀", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringUTF32CharConstructor)
{
	utf32_char someChar(UTF32TestChars[UTF32TestCharsCount / 2]);
	string someString = someChar;

	ARE_EQUAL(1, someString.GetLength());
	ARE_EQUAL(3, someString.GetLengthInBytes());
	ARE_EQUAL(UTF32TestChars[UTF32TestCharsCount / 2], someString.ToUTF32()[0]);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCharsConstructor)
{
	string someString = "B¥𠀀௵rΩ";

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCharsAndLengthInBytesConstructor)
{
	string someString("B¥𠀀௵rΩ", 14);

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCharsAndLengthsConstructor)
{
	string someString("B¥𠀀௵rΩ", 6, 14);

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringUTF16StringConstructor)
{
	utf16_string sourceString(UTF16TestWords);
	string someString = sourceString;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL(UTF8TestBytes, someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringUTF32StringConstructor)
{
	utf32_string sourceString(UTF32TestChars);
	string someString = sourceString;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL(UTF8TestBytes, someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCopyConstructor)
{
	string sourceString = "B¥𠀀௵rΩ";
	string someString = sourceString;

	sourceString = string::Empty;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringMaxLengthConstructor)
{
	string someString = StringTestsHelper->GetMaxLengthChars();

	ARE_EQUAL(string::MaxLength, someString.GetLength());
	ARE_EQUAL(string::MaxLength, someString.GetLengthInBytes());
	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringConstructorExceptions)
{
#ifdef DEBUGGING_STRINGS
	IS_THROW(string someString = UTF8TestInvalidBytes, InvalidUTF8SequenceException);
	IS_THROW(string someString(UTF8TestInvalidBytes, 2), InvalidUTF8SequenceException);
	IS_THROW(string someString(UTF8TestInvalidBytes, 2, 4), InvalidUTF8SequenceException);
#endif // DEBUGGING_STRINGS
	IS_THROW(string(StringTestsHelper->GetMoreThanMaxLengthChars()), OverflowException);
	IS_THROW(string(StringTestsHelper->GetMoreThanMaxLengthChars(), string::MaxLength + 1), OverflowException);
	IS_THROW(string(StringTestsHelper->GetMoreThanMaxLengthChars(), string::MaxLength + 1, string::MaxLength + 1), OverflowException);
}

//===========================================================================//

TEST(StringMoveConstructor)
{
	string someString = std::move(string("B¥𠀀௵rΩ"));

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//
//  Operator =
//===========================================================================//

TEST(StringCopyOperator)
{
	string sourceString = "B¥𠀀௵rΩ";
	string someString;

	someString = sourceString;

	sourceString = string::Empty;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringEmptyCopyOperator)
{
	string someString = "B¥𠀀௵rΩ";
	someString = string::Empty;

	ARE_EQUAL(0, someString.GetLength());
	ARE_EQUAL(0, someString.GetLengthInBytes());
	ARE_EQUAL(string::Empty, someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringNullTerminatorCopyOperator)
{
	string someString = "B¥𠀀௵rΩ";
	someString = NullTerminator;

	ARE_EQUAL(0, someString.GetLength());
	ARE_EQUAL(0, someString.GetLengthInBytes());
	ARE_EQUAL(string::Empty, someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCharCopyOperator)
{
	utf8_char someChar = "𠀀";
	string someString;
	someString = someChar;

	ARE_EQUAL(1, someString.GetLength());
	ARE_EQUAL(4, someString.GetLengthInBytes());
	ARE_EQUAL("𠀀", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCharsCopyOperator)
{
	string someString;
	someString = "B¥𠀀௵rΩ";

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringMaxLengthCopyOperator)
{
	string someString;
	someString = StringTestsHelper->GetMaxLengthChars();

	ARE_EQUAL(string::MaxLength, someString.GetLength());
	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), someString);
	ARE_EQUAL(string::MaxLength, someString.GetLengthInBytes());
}

//===========================================================================//

TEST(StringCopyOperatorSelf)
{
	string someString = "B¥𠀀௵rΩ";

	someString = someString;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringCopyOperatorExceptions)
{
	string someString;

#ifdef DEBUGGING_STRINGS
	IS_THROW(someString = UTF8TestInvalidBytes, InvalidUTF8SequenceException);
#endif // DEBUGGING_STRINGS
	IS_THROW(someString = StringTestsHelper->GetMoreThanMaxLengthChars(), OverflowException);
}

//===========================================================================//

TEST(StringMoveOperator)
{
	string someString;

	someString = std::move(string("B¥𠀀௵rΩ"));

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//
//  Append
//===========================================================================//

TEST(StringAppendToEmpty)
{
	string someString;
	someString += "B¥𠀀௵rΩ";

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringAppendEmpty)
{
	string someString = "B¥𠀀௵rΩ";
	someString += string::Empty;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringAppendNullTerminator)
{
	string someString = "B¥𠀀௵rΩ";
	someString += NullTerminator;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringAppendChar)
{
	utf8_char appendingChar = "𠁥";
	string someString = "B¥𠀀௵rΩ";
	someString += appendingChar;

	ARE_EQUAL(7, someString.GetLength());
	ARE_EQUAL(18, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ𠁥", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringAppendCharExceptions)
{
	utf8_char appendingChar = "𠁥";
	string otherString = StringTestsHelper->GetMaxLengthChars();
	IS_THROW(otherString += appendingChar, OverflowException);
}

//===========================================================================//

TEST(StringAppendChars)
{
	string someString = "B¥𠀀௵rΩ";
	const char * appendingChars = "∫𠁥♫u";
	someString += appendingChars;

	ARE_EQUAL(10, someString.GetLength());
	ARE_EQUAL(25, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ∫𠁥♫u", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringAppendCharsExceptions)
{
	string someString = "B¥𠀀௵rΩ";
	string otherString = StringTestsHelper->GetMaxLengthChars();
	IS_THROW(otherString += someString, OverflowException);
}

//===========================================================================//

TEST(StringAppendOtherString)
{
	string someString = "B¥𠀀௵rΩ";
	string appendingString = "∫𠁥♫u";
	someString += appendingString;

	ARE_EQUAL(10, someString.GetLength());
	ARE_EQUAL(25, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ∫𠁥♫u", someString);
	ARE_EQUAL(0, someString._cashedIndex);
	ARE_EQUAL(0, someString._cashedByteIndex);
}

//===========================================================================//

TEST(StringAppendMaxLength)
{
	string someString;
	someString += StringTestsHelper->GetHalfOfMaxLengthChars();
	someString += StringTestsHelper->GetHalfOfMaxLengthChars();

	ARE_EQUAL(string::MaxLength, someString.GetLength());
	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), someString);
	ARE_EQUAL(string::MaxLength, someString.GetLengthInBytes());
}

//===========================================================================//

TEST(StringAppendExceptions)
{
	string someString;

#ifdef DEBUGGING_STRINGS
	IS_THROW(someString += UTF8TestInvalidBytes, InvalidUTF8SequenceException);
#endif // DEBUGGING_STRINGS
	someString = StringTestsHelper->GetMaxLengthChars();
	IS_THROW(someString += utf8_char("1"), OverflowException);
	IS_THROW(someString += "1", OverflowException);
	IS_THROW(someString += string("1"), OverflowException);
}

//===========================================================================//
//  Plus
//===========================================================================//

TEST(StringPlusToEmpty)
{
	string someString = "B¥𠀀௵rΩ";
	string resultString = string::Empty + someString;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringPlusEmpty)
{
	string someString = "B¥𠀀௵rΩ";
	string resultString = someString + string::Empty;

	ARE_EQUAL(6, someString.GetLength());
	ARE_EQUAL(14, someString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", someString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringPlusChar)
{
	string someString = "B¥𠀀௵rΩ";
	utf8_char appendingChar = "𠁥";
	string resultString = someString + appendingChar;

	ARE_EQUAL(7, resultString.GetLength());
	ARE_EQUAL(18, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ𠁥", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringPlusCharExceptions)
{
	utf8_char appendingChar = "𠁥";
	string otherString = StringTestsHelper->GetMaxLengthChars();
	string resultString;
	IS_THROW(resultString = otherString + appendingChar, OverflowException);
}

//===========================================================================//

TEST(StringPlusChars)
{
	string someString = "B¥𠀀௵rΩ";
	const char * appendingChars = "∫𠁥♫u";
	string resultString = someString + appendingChars;

	ARE_EQUAL(10, resultString.GetLength());
	ARE_EQUAL(25, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ∫𠁥♫u", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringPlusCharsExceptions)
{
	string someString = "B¥𠀀௵rΩ";
	string otherString = StringTestsHelper->GetMaxLengthChars();
	string resultString;
	IS_THROW(resultString = otherString + someString, OverflowException);
}

//===========================================================================//

TEST(StringPlusOtherString)
{
	string someString = "B¥𠀀௵rΩ";
	string appendingString = "∫𠁥♫u";
	string resultString = someString + appendingString;

	ARE_EQUAL(10, resultString.GetLength());
	ARE_EQUAL(25, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ∫𠁥♫u", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringPlusMaxLength)
{
	string someString = StringTestsHelper->GetHalfOfMaxLengthChars();
	string resultString = someString + StringTestsHelper->GetHalfOfMaxLengthChars();

	ARE_EQUAL(string::MaxLength, resultString.GetLength());
	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), resultString);
	ARE_EQUAL(string::MaxLength, resultString.GetLengthInBytes());
}

//===========================================================================//

TEST(StringPlusExceptions)
{
	string someString = StringTestsHelper->GetMaxLengthChars();
	string resultString;
	IS_THROW(resultString = someString + utf8_char("1"), OverflowException);
	IS_THROW(resultString = someString + "1", OverflowException);
	IS_THROW(resultString = someString + string("1"), OverflowException);
}

//===========================================================================//
//  Equals and not equals
//===========================================================================//

TEST(StringEquals)
{
	IS_TRUE(string::Empty == "");

	IS_TRUE(string(string::Empty) == "");
	IS_TRUE(string(string::Empty) == string::Empty);

	IS_TRUE(string("𠀀") == utf8_char("𠀀"));
	IS_FALSE((string("𠀀") != utf8_char("𠀀")));

	IS_TRUE(string("𠀀") != utf8_char("¥"));

	IS_TRUE(string("B¥𠀀௵rΩ") == "B¥𠀀௵rΩ");
	IS_FALSE((string("B¥𠀀௵rΩ") != "B¥𠀀௵rΩ"));

	IS_TRUE(string("B¥𠀀௵rΩ") != "∫𠁥♫u");

	IS_TRUE(string("B¥𠀀௵rΩ") == string("B¥𠀀௵rΩ"));
	IS_FALSE((string("B¥𠀀௵rΩ") != string("B¥𠀀௵rΩ")));

	IS_TRUE(string("B¥𠀀௵rΩ") != string("∫𠁥♫u"));
}

//===========================================================================//

TEST(StringEqualsMaxLength)
{
	string someString = StringTestsHelper->GetMaxLengthChars();
	IS_TRUE(someString == StringTestsHelper->GetMaxLengthChars());

	someString = "B¥𠀀௵rΩ";
	IS_TRUE(someString != StringTestsHelper->GetMaxLengthChars());
}

//===========================================================================//
//  Less
//===========================================================================//

TEST(StringLess)
{
	IS_TRUE(string::Empty < "B");
	IS_FALSE((string::Empty > "B"));
	IS_FALSE((string::Empty == "B"));

	IS_TRUE(string("B¥𠀀௵rΩ") < "Z¥𠀀௵rΩ");
	IS_FALSE((string("B¥𠀀௵rΩ") > "Z¥𠀀௵rΩ"));
	IS_FALSE((string("B¥𠀀௵rΩ") == "Z¥𠀀௵rΩ"));

	IS_TRUE(string("B¥𠀀௵rΩ") < "B¥𠀀௵rΩ!");
	IS_FALSE((string("B¥𠀀௵rΩ") > "B¥𠀀௵rΩ!"));
	IS_FALSE((string("B¥𠀀௵rΩ") == "B¥𠀀௵rΩ!"));

	IS_TRUE(string::Empty < utf8_char("B"));
	IS_FALSE((string::Empty > utf8_char("B")));
	IS_FALSE((string::Empty == utf8_char("B")));

	IS_TRUE(string("B") < utf8_char("Z"));
	IS_FALSE((string("B") > utf8_char("Z")));
	IS_FALSE((string("B") == utf8_char("Z")));

	IS_TRUE(string::Empty < string("B"));
	IS_FALSE((string::Empty > string("B")));
	IS_FALSE((string::Empty == string("B")));

	IS_TRUE(string("B") < string("Z"));
	IS_FALSE((string("B") > string("Z")));
	IS_FALSE((string("B") == string("Z")));

	IS_TRUE(string("B¥𠀀௵rΩ") < string("b¥𠀀௵rΩ"));
	IS_FALSE((string("B¥𠀀௵rΩ") > string("b¥𠀀௵rΩ")));
	IS_FALSE((string("B¥𠀀௵rΩ") == string("b¥𠀀௵rΩ")));

	IS_TRUE(string("B¥𠀀௵rΩ") < string("B¥𠀀௵rΩ!"));
	IS_FALSE((string("B¥𠀀௵rΩ") > string("B¥𠀀௵rΩ!")));
	IS_FALSE((string("B¥𠀀௵rΩ") == string("B¥𠀀௵rΩ!")));
}

//===========================================================================//
//  Greater
//===========================================================================//

TEST(StringGreater)
{
	IS_TRUE(string("B") > string::Empty.ToChars());
	IS_FALSE((string("B") < string::Empty.ToChars()));
	IS_FALSE((string("B") == string::Empty.ToChars()));

	IS_TRUE(string("Z¥𠀀௵rΩ") > "B¥𠀀௵rΩ");
	IS_FALSE((string("Z¥𠀀௵rΩ") < "B¥𠀀௵rΩ"));
	IS_FALSE((string("Z¥𠀀௵rΩ") == "B¥𠀀௵rΩ"));

	IS_TRUE(string("B¥𠀀௵rΩ!") > "B¥𠀀௵rΩ");
	IS_FALSE((string("B¥𠀀௵rΩ!") < "B¥𠀀௵rΩ"));
	IS_FALSE((string("B¥𠀀௵rΩ!") == "B¥𠀀௵rΩ"));

	IS_TRUE(string("Z") > utf8_char("B"));
	IS_FALSE((string("Z") < utf8_char("B")));
	IS_FALSE((string("Z") == utf8_char("B")));

	IS_TRUE(string("Z") > string("B"));
	IS_FALSE((string("Z") < string("B")));
	IS_FALSE((string("Z") == string("B")));

	IS_TRUE(string("Z¥𠀀௵rΩ") > string("B¥𠀀௵rΩ"));
	IS_FALSE((string("Z¥𠀀௵rΩ") < string("B¥𠀀௵rΩ")));
	IS_FALSE((string("Z¥𠀀௵rΩ") == string("B¥𠀀௵rΩ")));

	IS_TRUE(string("B¥𠀀௵rΩ!") > string("B¥𠀀௵rΩ"));
	IS_FALSE((string("B¥𠀀௵rΩ!") < string("B¥𠀀௵rΩ")));
	IS_FALSE((string("B¥𠀀௵rΩ!") == string("B¥𠀀௵rΩ")));
}

//===========================================================================//
//  Operator [], GetChar
//===========================================================================//

TEST(StringOperatorGetChar)
{
	string someString = "B¥𠀀௵rΩ";

	for (integer i = 0; i < someString.GetLength(); i++)
	{
		IS_TRUE(someString[i] == UTF8TestChars[i]);
		ARE_EQUAL(i, someString._cashedIndex);
		ARE_EQUAL(UTF8TestByteIndices[i], someString._cashedByteIndex);
	}

	for (integer i = someString.GetLength() - 1; i >= 0; i--)
	{
		IS_TRUE(someString[i] == UTF8TestChars[i]);
		ARE_EQUAL(i, someString._cashedIndex);
		ARE_EQUAL(UTF8TestByteIndices[i], someString._cashedByteIndex);
	}
}

//===========================================================================//

TEST(StringOperatorGetCharExceptions)
{
	string emptyString;
	IS_THROW(emptyString[-1], IndexOutOfBoundsException);
	IS_THROW(emptyString[0], IndexOutOfBoundsException);
	IS_THROW(emptyString[1], IndexOutOfBoundsException);

	string someString = "B¥𠀀௵rΩ";
	IS_THROW(someString[-1], IndexOutOfBoundsException);
	IS_THROW(someString[6], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(StringGetChar)
{
	string someString = "B¥𠀀௵rΩ";

	for (integer i = 0; i < someString.GetLength(); i++)
	{
		IS_TRUE(someString.GetChar(i) == UTF8TestChars[i]);
		ARE_EQUAL(i, someString._cashedIndex);
		ARE_EQUAL(UTF8TestByteIndices[i], someString._cashedByteIndex);
	}

	for (integer i = someString.GetLength() - 1; i >= 0; i--)
	{
		IS_TRUE(someString.GetChar(i) == UTF8TestChars[i]);
		ARE_EQUAL(i, someString._cashedIndex);
		ARE_EQUAL(UTF8TestByteIndices[i], someString._cashedByteIndex);
	}
}

//===========================================================================//

TEST(StringGetCharExceptions)
{
	string emptyString;
	IS_THROW(emptyString.GetChar(-1), IndexOutOfBoundsException);
	IS_THROW(emptyString.GetChar(0), IndexOutOfBoundsException);
	IS_THROW(emptyString.GetChar(1), IndexOutOfBoundsException);

	string someString = "B¥𠀀௵rΩ";
	IS_THROW(someString.GetChar(-1), IndexOutOfBoundsException);
	IS_THROW(someString.GetChar(6), IndexOutOfBoundsException);
}

//===========================================================================//
//  To chars
//===========================================================================//

TEST(StringEmptyToChars)
{
	string someString;
	ARE_EQUAL(string::Empty, someString.ToChars());
}

//===========================================================================//

TEST(StringToChars)
{
	string someString = UTF8TestBytes;
	for (integer i = 0; i < UTF8TestBytesCount; i++)
	{
		ARE_EQUAL(UTF8TestBytes[i], someString.ToChars()[i]);
	}
}

//===========================================================================//
//  ToUTF*Chars
//===========================================================================//

TEST(StringEmptyToUTF16)
{
	string someString;
	utf16_string resultString = someString.ToUTF16();
	ARE_EQUAL(static_cast<ushort>(NullTerminator), resultString.ToChars()[0]);
}

//===========================================================================//

TEST(StringToUTF16)
{
	string someString = UTF8TestBytes;
	utf16_string resultString = someString.ToUTF16();
	ARE_ARRAYS_EQUAL(UTF16TestWords, resultString.ToChars(), UTF16TestWordsCount + 1);
}

//===========================================================================//

TEST(StringEmptyToUTF32)
{
	string someString;
	utf32_string resultString = someString.ToUTF32();
	ARE_EQUAL(static_cast<utf32_char>(NullTerminator), resultString.ToChars()[0]);
}

//===========================================================================//

TEST(StringToUTF32)
{
	string someString = UTF8TestBytes;
	utf32_string resultString = someString.ToUTF32();
	ARE_ARRAYS_EQUAL(UTF32TestChars, resultString.ToChars(), UTF32TestCharsCount + 1);
}

//===========================================================================//
//  Format
//===========================================================================//

TEST(StringFormatEmtpy)
{
	string emptyString = string::Format("{0}", string::Empty);
	ARE_EQUAL(string::Empty, emptyString);
}

//===========================================================================//

TEST(StringFormat)
{
	string oneArgumentString = string::Format("{0}", 1);
	ARE_EQUAL("1", oneArgumentString);

	string twoArgumentsString = string::Format("{0}, {1}", 1, 2);
	ARE_EQUAL("1, 2", twoArgumentsString);

	string threeArgumentsString = string::Format("{0}, {1}, {2}", 1, 2, 3);
	ARE_EQUAL("1, 2, 3", threeArgumentsString);

	string fourArgumentsString = string::Format("{0}, {1}, {2}, {3}", 1, 2, 3, 4);
	ARE_EQUAL("1, 2, 3, 4", fourArgumentsString);

	string fiveArgumentsString = string::Format("{0}, {1}, {2}, {3}, {4}", 1, 2, 3, 4, 5);
	ARE_EQUAL("1, 2, 3, 4, 5", fiveArgumentsString);

	string sixArgumentsString = string::Format("{0}, {1}, {2}, {3}, {4}, {5}", 1, 2, 3, 4, 5, 6);
	ARE_EQUAL("1, 2, 3, 4, 5, 6", sixArgumentsString);

	string sevenArgumentsString = string::Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}", 1, 2, 3, 4, 5, 6, 7);
	ARE_EQUAL("1, 2, 3, 4, 5, 6, 7", sevenArgumentsString);

	string eightArgumentsString = string::Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}", 1, 2, 3, 4, 5, 6, 7, 8);
	ARE_EQUAL("1, 2, 3, 4, 5, 6, 7, 8", eightArgumentsString);

	string nineArgumentsString = string::Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}", 1, 2, 3, 4, 5, 6, 7, 8, 9);
	ARE_EQUAL("1, 2, 3, 4, 5, 6, 7, 8, 9", nineArgumentsString);

	string tenArgumentsString = string::Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	ARE_EQUAL("1, 2, 3, 4, 5, 6, 7, 8, 9, 10", tenArgumentsString);
}

//===========================================================================//

TEST(StringFormatFloat)
{
	string justValue = string::Format("{0}", 666.222222f);
	ARE_CLOSE(666.222222f, Converter::ToFloat(justValue), 0.0001f);
	ARE_EQUAL("666", string::Format("{0:.0}", 666.222222f));
	ARE_EQUAL("666.67", string::Format("{0:.2}", 666.666666f));
}

//===========================================================================//

TEST(StringFormatAdditionalFormatting)
{
	ARE_EQUAL("01:02:2012", string::Format("{0:00}:{1:00}:{2:2000}", 1, 2, 12));
	ARE_EQUAL("111-222-333", string::Format("{0:00}-{1:00}-{2:00}", 111, 222, 333));
	ARE_EQUAL("{ Иван }", string::Format("{{ {0} }}", "Иван"));
	ARE_EQUAL("John } { Иван", string::Format("{0} }} {{ {1}", "John", "Иван"));
}

//===========================================================================//

TEST(StringFormatMixed)
{
	string name = "Ω-Death";

	string someString = string::Format("Yeah! {0} is {1:.2}x{2} and {3}. {0}!", name, 13.885622f, 45.0f, 255);
	ARE_EQUAL("Yeah! Ω-Death is 13.89x45.0000 and 255. Ω-Death!", someString);
}

//===========================================================================//

TEST(StringFormatMaxLength)
{
	string resultString = string::Format("{0}", StringTestsHelper->GetMaxLengthChars());
	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), resultString);

	resultString = string::Format("{0}{1}", StringTestsHelper->GetHalfOfMaxLengthChars(),
	                              StringTestsHelper->GetHalfOfMaxLengthChars());
	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), resultString);
}

//===========================================================================//

TEST(StringFormatExceptions)
{
	IS_THROW(string::Format("{", 666), InvalidFormatException);
	IS_THROW(string::Format("}", 666), InvalidFormatException);
	IS_THROW(string::Format("{{}", 666), InvalidFormatException);
	IS_THROW(string::Format("{-1}", 666), InvalidFormatException);
	IS_THROW(string::Format("{1}", 666), InvalidFormatException);
	IS_THROW(string::Format("{Z}", 666), InvalidFormatException);
	IS_THROW(string::Format("{0:0.Z}", 666), InvalidFormatException);
	IS_THROW(string::Format("{0:0.0Z}", 666), InvalidFormatException);
	IS_THROW(string::Format("Die {0}!", StringTestsHelper->GetMaxLengthChars()), OverflowException);
	IS_THROW(string::Format("{0}", StringTestsHelper->GetMoreThanMaxLengthChars()), OverflowException);
}

//===========================================================================//
//  IsEmptyOrBlankCharsOnly
//===========================================================================//

TEST(StringIsEmpty)
{
	string someString = string::Empty;
	ARE_EQUAL(true, someString.IsEmpty());

	someString = "BruТайм";
	ARE_EQUAL(false, someString.IsEmpty());

	someString = static_cast<utf8_char>(" ");
	ARE_EQUAL(false, someString.IsEmpty());

	someString = static_cast<utf8_char>("\t");
	ARE_EQUAL(false, someString.IsEmpty());
}

//===========================================================================//

TEST(StringIsEmptyOrBlankCharsOnly)
{
	string someString = string::Empty;
	ARE_EQUAL(true, someString.IsEmptyOrBlankCharsOnly());

	someString = "BruТайм";
	ARE_EQUAL(false, someString.IsEmptyOrBlankCharsOnly());

	someString = static_cast<utf8_char>(" ");
	ARE_EQUAL(true, someString.IsEmptyOrBlankCharsOnly());

	someString = static_cast<utf8_char>("\t");
	ARE_EQUAL(true, someString.IsEmptyOrBlankCharsOnly());

	someString = static_cast<utf8_char>("\n");
	ARE_EQUAL(true, someString.IsEmptyOrBlankCharsOnly());

	someString = static_cast<utf8_char>("\r");
	ARE_EQUAL(true, someString.IsEmptyOrBlankCharsOnly());

	someString = " \t \n \r";
	ARE_EQUAL(true, someString.IsEmptyOrBlankCharsOnly());

	someString = "1       ";
	ARE_EQUAL(false, someString.IsEmptyOrBlankCharsOnly());

	someString = "      1";
	ARE_EQUAL(false, someString.IsEmptyOrBlankCharsOnly());
}

//===========================================================================//
//  IsBlankCharAt
//===========================================================================//

TEST(IsBlankCharAt)
{
	string someString = "BruТайм";
	for (integer i = 0; i < someString.GetLength(); i++)
	{
		ARE_EQUAL(false, someString.IsBlankCharAt(i));
	}

	someString = " B\tr\nu\r";
	ARE_EQUAL(true, someString.IsBlankCharAt(0));
	ARE_EQUAL(true, someString.IsBlankCharAt(2));
	ARE_EQUAL(true, someString.IsBlankCharAt(4));
	ARE_EQUAL(true, someString.IsBlankCharAt(6));
}

//===========================================================================//

TEST(IsBlankCharAtExceptions)
{
	string someString = "BruТайм";
	IS_THROW(someString.IsBlankCharAt(-1), IndexOutOfBoundsException);
	IS_THROW(someString.IsBlankCharAt(7), IndexOutOfBoundsException);
}

//===========================================================================//
//  IsNumber
//===========================================================================//

TEST(StringIsNumber)
{
	string someString = string::Empty;
	ARE_EQUAL(false, someString.IsNumber());

	someString = "666";
	ARE_EQUAL(true, someString.IsNumber());

	someString = "666!";
	ARE_EQUAL(false, someString.IsNumber());
}

//===========================================================================//
//  Insert
//===========================================================================//

TEST(StringInsertEmpty)
{
	string someString = "B¥𠀀௵rΩ";
	string resultString = someString.Insert(0, string::Empty);

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertInEmpty)
{
	string someString;
	string resultString = someString.Insert(0, "B¥𠀀௵rΩ");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertInBegin)
{
	string someString = "¥𠀀௵rΩ";
	string resultString = someString.Insert(0, "B");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertIntervalInBegin)
{
	string someString = "𠀀௵rΩ";
	string resultString = someString.Insert(0, "B¥");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertInMiddle)
{
	string someString = "B¥𠀀rΩ";
	string resultString = someString.Insert(3, "௵");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertIntervalInMiddle)
{
	string someString = "B¥rΩ";
	string resultString = someString.Insert(2, "𠀀௵");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertInEnd)
{
	string someString = "B¥𠀀௵r";
	string resultString = someString.Insert(5, "Ω");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertInIntervalEnd)
{
	string someString = "B¥𠀀௵";
	string resultString = someString.Insert(4, "rΩ");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringInsertMaxLength)
{
	string resultString;
	resultString = resultString.Insert(0, StringTestsHelper->GetHalfOfMaxLengthChars());
	resultString = resultString.Insert(0, StringTestsHelper->GetHalfOfMaxLengthChars());

	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), resultString);
}

//===========================================================================//

TEST(StringInsertExceptions)
{
	string someString = "B¥𠀀௵rΩ";
	IS_THROW(someString.Insert(-1, "♫௵Ω"), IndexOutOfBoundsException);
	IS_THROW(someString.Insert(7, "♫௵Ω"), IndexOutOfBoundsException);

	IS_THROW(someString.Insert(0, StringTestsHelper->GetMaxLengthChars()), OverflowException);

	string maxLengthString = StringTestsHelper->GetMaxLengthChars();
	IS_THROW(maxLengthString.Insert(0, "1"), OverflowException);
}

//===========================================================================//
//  Remove
//===========================================================================//

TEST(StringRemove)
{
	string someString = "крuк𠀀зΩбр";
	string resultString;

	//Удаление нулевого количества символов
	resultString = someString.Remove(3, 0);

	ARE_EQUAL(9, resultString.GetLength());
	ARE_EQUAL(20, resultString.GetLengthInBytes());
	ARE_EQUAL("крuк𠀀зΩбр", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	//Удаление сначала
	resultString = someString.Remove(0);

	ARE_EQUAL(8, resultString.GetLength());
	ARE_EQUAL(18, resultString.GetLengthInBytes());
	ARE_EQUAL("рuк𠀀зΩбр", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Remove(0, 3);

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(15, resultString.GetLengthInBytes());
	ARE_EQUAL("к𠀀зΩбр", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	//Удаление из середины
	resultString = someString.Remove(3);

	ARE_EQUAL(8, resultString.GetLength());
	ARE_EQUAL(18, resultString.GetLengthInBytes());
	ARE_EQUAL("крu𠀀зΩбр", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Remove(1, 4);

	ARE_EQUAL(5, resultString.GetLength());
	ARE_EQUAL(11, resultString.GetLengthInBytes());
	ARE_EQUAL("кзΩбр", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	//Удаление из конца
	resultString = someString.Remove(5);

	ARE_EQUAL(8, resultString.GetLength());
	ARE_EQUAL(18, resultString.GetLengthInBytes());
	ARE_EQUAL("крuк𠀀Ωбр", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Remove(3, 3);

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(12, resultString.GetLengthInBytes());
	ARE_EQUAL("крuΩбр", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	//Удаление всего
	resultString = someString.Remove(0, someString.GetLength());

	ARE_EQUAL(0, resultString.GetLength());
	ARE_EQUAL(0, resultString.GetLengthInBytes());
	ARE_EQUAL("", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringRemoveExceptions)
{
	string emptyString;
	IS_THROW(emptyString.Remove(-1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(-1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(-1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(-1, 1), IndexOutOfBoundsException);

	IS_THROW(emptyString.Remove(0), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(0, -1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(0, 0), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(0, 1), IndexOutOfBoundsException);

	IS_THROW(emptyString.Remove(1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyString.Remove(1, 1), IndexOutOfBoundsException);

	string someString = "B¥𠀀௵rΩ";
	IS_THROW(someString.Remove(-1), IndexOutOfBoundsException);
	IS_THROW(someString.Remove(-1, -1), IndexOutOfBoundsException);
	IS_THROW(someString.Remove(-1, 0), IndexOutOfBoundsException);
	IS_THROW(someString.Remove(-1, 1), IndexOutOfBoundsException);

	IS_THROW(someString.Remove(6), IndexOutOfBoundsException);
	IS_THROW(someString.Remove(6, -1), IndexOutOfBoundsException);
	IS_THROW(someString.Remove(6, 0), IndexOutOfBoundsException);
	IS_THROW(someString.Remove(6, 1), IndexOutOfBoundsException);

	IS_THROW(someString.Remove(0, -1), InvalidArgumentException);
	IS_THROW(someString.Remove(0, 7), InvalidArgumentException);
}

//===========================================================================//
//  Substring
//===========================================================================//

TEST(StringSubstringBegin)
{
	string someString = "B¥𠀀௵rΩ";
	string resultString;

	resultString = someString.Substring(0, 1);

	ARE_EQUAL(1, resultString.GetLength());
	ARE_EQUAL(1, resultString.GetLengthInBytes());
	ARE_EQUAL("B", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(0, 4);

	ARE_EQUAL(4, resultString.GetLength());
	ARE_EQUAL(10, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(0, 6);

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(14, resultString.GetLengthInBytes());
	ARE_EQUAL("B¥𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringSubstringMiddle)
{
	string someString = "B¥𠀀௵rΩ";
	string resultString;

	resultString = someString.Substring(3, 1);

	ARE_EQUAL(1, resultString.GetLength());
	ARE_EQUAL(3, resultString.GetLengthInBytes());
	ARE_EQUAL("௵", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(1, 4);

	ARE_EQUAL(4, resultString.GetLength());
	ARE_EQUAL(10, resultString.GetLengthInBytes());
	ARE_EQUAL("¥𠀀௵r", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringSubstringEnd)
{
	string someString = "B¥𠀀௵rΩ";
	string resultString;

	resultString = someString.Substring(2);

	ARE_EQUAL(4, resultString.GetLength());
	ARE_EQUAL(11, resultString.GetLengthInBytes());
	ARE_EQUAL("𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(2, 4);

	ARE_EQUAL(4, resultString.GetLength());
	ARE_EQUAL(11, resultString.GetLengthInBytes());
	ARE_EQUAL("𠀀௵rΩ", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(5);

	ARE_EQUAL(1, resultString.GetLength());
	ARE_EQUAL(3, resultString.GetLengthInBytes());
	ARE_EQUAL("Ω", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(5, 1);

	ARE_EQUAL(1, resultString.GetLength());
	ARE_EQUAL(3, resultString.GetLengthInBytes());
	ARE_EQUAL("Ω", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(6);

	ARE_EQUAL(0, resultString.GetLength());
	ARE_EQUAL(0, resultString.GetLengthInBytes());
	ARE_EQUAL(string::Empty, resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Substring(6, 0);

	ARE_EQUAL(0, resultString.GetLength());
	ARE_EQUAL(0, resultString.GetLengthInBytes());
	ARE_EQUAL(string::Empty, resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringSubstringOneChar)
{
	string oneCharString = "й";
	string resultString = oneCharString.Substring(0, 1);

	ARE_EQUAL(1, resultString.GetLength());
	ARE_EQUAL(2, resultString.GetLengthInBytes());
	ARE_EQUAL("й", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringSubstringExceptions)
{
	string emptyString = string::Empty;
	IS_THROW(emptyString.Substring(-1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Substring(-1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Substring(-1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyString.Substring(-1, 7), IndexOutOfBoundsException);

	IS_THROW(emptyString.Substring(0, -1), InvalidArgumentException);
	IS_THROW(emptyString.Substring(0, 7), InvalidArgumentException);

	IS_THROW(emptyString.Substring(1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Substring(1, -1), IndexOutOfBoundsException);
	IS_THROW(emptyString.Substring(1, 0), IndexOutOfBoundsException);
	IS_THROW(emptyString.Substring(1, 7), IndexOutOfBoundsException);

	string someString = "B¥𠀀௵rΩ";
	IS_THROW(someString.Substring(-1), IndexOutOfBoundsException);
	IS_THROW(someString.Substring(-1, -1), IndexOutOfBoundsException);
	IS_THROW(someString.Substring(-1, 0), IndexOutOfBoundsException);
	IS_THROW(someString.Substring(-1, 7), IndexOutOfBoundsException);

	IS_THROW(someString.Substring(6, -1), InvalidArgumentException);
	IS_THROW(someString.Substring(6, 7), InvalidArgumentException);

	IS_THROW(someString.Substring(0, -1), InvalidArgumentException);
	IS_THROW(someString.Substring(0, 7), InvalidArgumentException);
	IS_THROW(someString.Substring(5, 2), InvalidArgumentException);
}

//===========================================================================//
//  Replace
//===========================================================================//

TEST(StringEmptyReplace)
{
	ARE_EQUAL(string::Empty, string::Empty.Replace("", "♫௵Ω"));
}

//===========================================================================//

TEST(StringReplaceBegin)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";
	string resultString;

	resultString = someString.Replace("H", string::Empty);

	ARE_EQUAL(10, resultString.GetLength());
	ARE_EQUAL(26, resultString.GetLengthInBytes());
	ARE_EQUAL("𠀀¥¥𠀀-𠀀¥¥𠀀!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("H", "𠀀");

	ARE_EQUAL(12, resultString.GetLength());
	ARE_EQUAL(34, resultString.GetLengthInBytes());
	ARE_EQUAL("𠀀𠀀¥¥𠀀-𠀀𠀀¥¥𠀀!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("H", "♫௵Ω");

	ARE_EQUAL(16, resultString.GetLength());
	ARE_EQUAL(44, resultString.GetLengthInBytes());
	ARE_EQUAL("♫௵Ω𠀀¥¥𠀀-♫௵Ω𠀀¥¥𠀀!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("H𠀀¥", "♫");

	ARE_EQUAL(8, resultString.GetLength());
	ARE_EQUAL(20, resultString.GetLengthInBytes());
	ARE_EQUAL("♫¥𠀀-♫¥𠀀!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringReplaceMiddle)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";
	string resultString;

	resultString = someString.Replace("𠀀", string::Empty);

	ARE_EQUAL(8, resultString.GetLength());
	ARE_EQUAL(12, resultString.GetLengthInBytes());
	ARE_EQUAL("H¥¥-H¥¥!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("¥", "𠀀");

	ARE_EQUAL(12, resultString.GetLength());
	ARE_EQUAL(36, resultString.GetLengthInBytes());
	ARE_EQUAL("H𠀀𠀀𠀀𠀀-H𠀀𠀀𠀀𠀀!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("𠀀", "♫௵Ω");

	ARE_EQUAL(20, resultString.GetLength());
	ARE_EQUAL(48, resultString.GetLengthInBytes());
	ARE_EQUAL("H♫௵Ω¥¥♫௵Ω-H♫௵Ω¥¥♫௵Ω!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("𠀀¥¥𠀀", "a");

	ARE_EQUAL(6, resultString.GetLength());
	ARE_EQUAL(6, resultString.GetLengthInBytes());
	ARE_EQUAL("Ha-Ha!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringReplaceEnd)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";
	string resultString;

	resultString = someString.Replace("!", string::Empty);

	ARE_EQUAL(11, resultString.GetLength());
	ARE_EQUAL(27, resultString.GetLengthInBytes());
	ARE_EQUAL("H𠀀¥¥𠀀-H𠀀¥¥𠀀", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("!", "Ω");

	ARE_EQUAL(12, resultString.GetLength());
	ARE_EQUAL(30, resultString.GetLengthInBytes());

	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("!", "♫௵Ω");

	ARE_EQUAL(14, resultString.GetLength());
	ARE_EQUAL(36, resultString.GetLengthInBytes());
	ARE_EQUAL("H𠀀¥¥𠀀-H𠀀¥¥𠀀♫௵Ω", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);

	resultString = someString.Replace("¥𠀀!", "♫");

	ARE_EQUAL(10, resultString.GetLength());
	ARE_EQUAL(24, resultString.GetLengthInBytes());
	ARE_EQUAL("H𠀀¥¥𠀀-H𠀀¥♫", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringReplaceSingleCharToDoubleChats)
{
	string someString = "♫♫♫♫♫♫♫";
	string resultString = someString.Replace("♫", "♫♫");

	ARE_EQUAL(14, resultString.GetLength());
	ARE_EQUAL(42, resultString.GetLengthInBytes());
	ARE_EQUAL("♫♫♫♫♫♫♫♫♫♫♫♫♫♫", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringReplaceSameLength)
{
	string sourceString = "H¥¥HH¥¥H";
	string resultString = sourceString.Replace("H¥¥H", "Die!");

	ARE_EQUAL(8, resultString.GetLength());
	ARE_EQUAL(8, resultString.GetLengthInBytes());
	ARE_EQUAL("Die!Die!", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringReplaceAll)
{
	string someString = "H¥¥H";
	string resultString = someString.Replace("H¥¥H", "H¥¥HH¥¥H");

	ARE_EQUAL(8, resultString.GetLength());
	ARE_EQUAL(12, resultString.GetLengthInBytes());
	ARE_EQUAL("H¥¥HH¥¥H", resultString);
	ARE_EQUAL(0, resultString._cashedIndex);
	ARE_EQUAL(0, resultString._cashedByteIndex);
}

//===========================================================================//

TEST(StringReplaceMaxLength)
{
	string someString = StringTestsHelper->GetHalfOfMaxLengthChars();
	someString = someString.Replace(StringTestsHelper->GetHalfOfMaxLengthChars(),
	                                StringTestsHelper->GetMaxLengthChars());
	ARE_EQUAL(string::MaxLength, someString.GetLength());
	ARE_EQUAL(StringTestsHelper->GetMaxLengthChars(), someString);
	ARE_EQUAL(string::MaxLength, someString.GetLengthInBytes());
}

//===========================================================================//

TEST(StringReplaceExceptions)
{
	string someString = StringTestsHelper->GetMaxLengthChars();
	IS_THROW(someString.Replace(StringTestsHelper->GetMaxLengthChars(), StringTestsHelper->GetMoreThanMaxLengthChars()),OverflowException);
}

//===========================================================================//
//  Reverse
//===========================================================================//

TEST(StringReverse)
{
	ARE_EQUAL(string::Empty, string::Empty.Reverse());

	string someString = "B¥𠀀௵rΩ";
	ARE_EQUAL("Ωr௵𠀀¥B", someString.Reverse());
}

//===========================================================================//
//  Trims and clean up
//===========================================================================//

TEST(StringTrims)
{
	ARE_EQUAL(string::Empty, string::Empty.TrimLeft());
	ARE_EQUAL(string::Empty, string::Empty.TrimRight());
	ARE_EQUAL(string::Empty, string::Empty.Trim());

	string blankString = "  \t\n\r  ";
	ARE_EQUAL(string::Empty, blankString.TrimLeft());
	ARE_EQUAL(string::Empty, blankString.TrimRight());
	ARE_EQUAL(string::Empty, blankString.Trim());

	string someString = "  \t\n\r B¥𠀀 \t\n\r ௵rΩ \t\n\r  ";
	ARE_EQUAL("B¥𠀀 \t\n\r ௵rΩ \t\n\r  ", someString.TrimLeft());
	ARE_EQUAL("  \t\n\r B¥𠀀 \t\n\r ௵rΩ", someString.TrimRight());
	ARE_EQUAL("B¥𠀀 \t\n\r ௵rΩ", someString.Trim());
}

//===========================================================================//

TEST(StringCleanUp)
{
	ARE_EQUAL(string::Empty, string::Empty.CleanUp());

	string someString = "   \t\n\r    B ¥ 𠀀    \t\n\r    ௵rΩ    \t\n\r ";
	ARE_EQUAL("B ¥ 𠀀 ௵rΩ", someString.CleanUp());
}

//===========================================================================//
//  ToLower, ToUpper
//===========================================================================//

TEST(StringToLower)
{
	ARE_EQUAL(string::Empty, string::Empty.ToLower());

	string someEnglishString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	ARE_EQUAL("abcdefghijklmnopqrstuvwxyz", someEnglishString.ToLower());

	string someRussianString = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
	ARE_EQUAL("абвгдеёжзийклмнопрстуфхцчшщъыьэюя", someRussianString.ToLower());

	string someCharsWithoutCasePairString = "!@#$%^&*()";
	ARE_EQUAL("!@#$%^&*()", someCharsWithoutCasePairString.ToLower());
}

//===========================================================================//

TEST(StringToUpper)
{
	ARE_EQUAL(string::Empty, string::Empty.ToUpper());

	string someEnglishString = "abcdefghijklmnopqrstuvwxyz";
	ARE_EQUAL("ABCDEFGHIJKLMNOPQRSTUVWXYZ", someEnglishString.ToUpper());

	string someRussianString = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
	ARE_EQUAL("АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ", someRussianString.ToUpper());

	string someCharsWithoutCasePairString = "!@#$%^&*()";
	ARE_EQUAL("!@#$%^&*()", someCharsWithoutCasePairString.ToUpper());
}

//===========================================================================//
//  Contains
//===========================================================================//

TEST(StringContains)
{
	ARE_EQUAL(false, string::Empty.Contains(string::Empty));
	ARE_EQUAL(false, string::Empty.Contains("B"));

	string someString = "B¥𠀀௵rΩ";

	ARE_EQUAL(false, someString.Contains(string::Empty));
	ARE_EQUAL(true, someString.Contains("B"));

	ARE_EQUAL(true, someString.Contains("B"));
	ARE_EQUAL(true, someString.Contains("B¥𠀀"));
	ARE_EQUAL(true, someString.Contains("௵rΩ"));
	ARE_EQUAL(true, someString.Contains("B¥𠀀௵rΩ"));

	ARE_EQUAL(false, someString.Contains(string::Empty));
	ARE_EQUAL(false, someString.Contains("௵௵"));
	//другой порядок букв
	ARE_EQUAL(false, someString.Contains("B𠀀¥௵rΩ"));
}

//===========================================================================//

TEST(StringContainsCount)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	ARE_EQUAL(2, someString.ContainsCount("H𠀀¥"));
	ARE_EQUAL(1, someString.ContainsCount("𠀀-H"));
	ARE_EQUAL(1, someString.ContainsCount("𠀀!"));
	ARE_EQUAL(0, someString.ContainsCount("HaΩ"));
}

//===========================================================================//
//  FirstIndexOf
//===========================================================================//

TEST(StringFirstIndexOf)
{
	ARE_EQUAL(-1, string::Empty.FirstIndexOf(string::Empty));
	ARE_EQUAL(-1, string::Empty.FirstIndexOf("B"));

	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	ARE_EQUAL(-1, someString.FirstIndexOf(string::Empty));
	ARE_EQUAL(-1, someString.FirstIndexOf(string::Empty, 5));

	ARE_EQUAL(0, someString.FirstIndexOf("H𠀀¥"));
	ARE_EQUAL(6, someString.FirstIndexOf("H𠀀¥", 1));
	ARE_EQUAL(4, someString.FirstIndexOf("𠀀-H"));
	ARE_EQUAL(10, someString.FirstIndexOf("𠀀!"));
	ARE_EQUAL(-1, someString.FirstIndexOf("HaΩ"));
	ARE_EQUAL(-1, someString.FirstIndexOf("H𠀀¥", 9));
	ARE_EQUAL(-1, someString.FirstIndexOf("!H𠀀¥¥𠀀-H𠀀¥¥𠀀!"));
	ARE_EQUAL(-1, someString.FirstIndexOf("𠀀¥¥𠀀!", 10));
}

//===========================================================================//

TEST(StringFirstIndexOfExceptions)
{
	string emptyString = string::Empty;
	IS_THROW(emptyString.FirstIndexOf(string::Empty, -1), IndexOutOfBoundsException);
	IS_THROW(emptyString.FirstIndexOf(string::Empty, 1), IndexOutOfBoundsException);
	IS_THROW(emptyString.FirstIndexOf("B", -1), IndexOutOfBoundsException);
	IS_THROW(emptyString.FirstIndexOf("B", 1), IndexOutOfBoundsException);

	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	IS_THROW(someString.FirstIndexOf("H𠀀¥", -1), IndexOutOfBoundsException);
	IS_THROW(someString.FirstIndexOf("H𠀀¥", 12), IndexOutOfBoundsException);
}

//===========================================================================//
//  FirstNumberIndexOf
//===========================================================================//

TEST(StringFirstIndexNumberOf)
{
	ARE_EQUAL(-1, string::Empty.FirstIndexNumberOf(string::Empty, 1));
	ARE_EQUAL(-1, string::Empty.FirstIndexNumberOf("B", 1));

	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	ARE_EQUAL(-1, someString.FirstIndexNumberOf(string::Empty, 1));

	ARE_EQUAL(0, someString.FirstIndexNumberOf("H𠀀¥", 1));
	ARE_EQUAL(6, someString.FirstIndexNumberOf("H𠀀¥", 2));
	ARE_EQUAL(-1, someString.FirstIndexNumberOf("H𠀀¥", 3));
	ARE_EQUAL(4, someString.FirstIndexNumberOf("𠀀-H", 1));
	ARE_EQUAL(-1, someString.FirstIndexNumberOf("𠀀-H", 2));
	ARE_EQUAL(10, someString.FirstIndexNumberOf("𠀀!", 1));
	ARE_EQUAL(-1, someString.FirstIndexNumberOf("𠀀!", 2));
	ARE_EQUAL(-1, someString.FirstIndexNumberOf("HaΩ", 1));
	ARE_EQUAL(-1, someString.FirstIndexNumberOf("!H𠀀¥¥𠀀-H𠀀¥¥𠀀!", 1));
}

//===========================================================================//

TEST(StringFirstIndexNumberOfExceptions)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	IS_THROW(someString.FirstIndexNumberOf("H𠀀¥", -1), InvalidArgumentException);
	IS_THROW(someString.FirstIndexNumberOf("H𠀀¥", 0), InvalidArgumentException);
}

//===========================================================================//
//  LastIndexOf
//===========================================================================//

TEST(StringLastIndexOf)
{
	ARE_EQUAL(-1, string::Empty.LastIndexOf(string::Empty));
	ARE_EQUAL(-1, string::Empty.LastIndexOf("B"));

	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	ARE_EQUAL(-1, someString.LastIndexOf(string::Empty));
	ARE_EQUAL(-1, someString.LastIndexOf(string::Empty, 5));

	ARE_EQUAL(6, someString.LastIndexOf("H𠀀¥"));
	ARE_EQUAL(6, someString.LastIndexOf("H𠀀¥", 8));
	ARE_EQUAL(0, someString.LastIndexOf("H𠀀¥", 7));
	ARE_EQUAL(4, someString.LastIndexOf("𠀀-H"));
	ARE_EQUAL(10, someString.LastIndexOf("𠀀!"));
	ARE_EQUAL(-1, someString.LastIndexOf("HaΩ"));
	ARE_EQUAL(-1, someString.LastIndexOf("H𠀀¥", 1));
}

//===========================================================================//

TEST(StringLastIndexOfExceptions)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	IS_THROW(someString.LastIndexOf("H𠀀¥", -1), IndexOutOfBoundsException);
	IS_THROW(someString.LastIndexOf("H𠀀¥", 12), IndexOutOfBoundsException);
}

//===========================================================================//
//  LastIndexNumberOf
//===========================================================================//

TEST(StringEmptyLastNumberIndexOf)
{
	string emptyString = string::Empty;
	ARE_EQUAL(-1, emptyString.LastIndexNumberOf(string::Empty, 1));
	ARE_EQUAL(-1, emptyString.LastIndexNumberOf("B", 1));
}

//===========================================================================//

TEST(StringLastIndexNumberOfEmpty)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";
	ARE_EQUAL(-1, someString.LastIndexNumberOf(string::Empty, 1));
}

//===========================================================================//

TEST(StringLastIndexNumberOf)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	ARE_EQUAL(6, someString.LastIndexNumberOf("H𠀀¥", 1));
	ARE_EQUAL(0, someString.LastIndexNumberOf("H𠀀¥", 2));
	ARE_EQUAL(-1, someString.LastIndexNumberOf("H𠀀¥", 3));
	ARE_EQUAL(4, someString.LastIndexNumberOf("𠀀-H", 1));
	ARE_EQUAL(-1, someString.LastIndexNumberOf("𠀀-H", 2));
	ARE_EQUAL(10, someString.LastIndexNumberOf("𠀀!", 1));
	ARE_EQUAL(-1, someString.LastIndexNumberOf("𠀀!", 2));
	ARE_EQUAL(-1, someString.LastIndexNumberOf("HaΩ", 1));
	ARE_EQUAL(-1, someString.LastIndexNumberOf("!H𠀀¥¥𠀀-H𠀀¥¥𠀀!", 1));
}

//===========================================================================//

TEST(StringLastIndexNumberOfExceptions)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	IS_THROW(someString.LastIndexNumberOf("H𠀀¥", 0), InvalidArgumentException);
	IS_THROW(someString.LastIndexNumberOf("H𠀀¥", -1), InvalidArgumentException);
}

//===========================================================================//

TEST(StringFirstIndexOfNotBlankChar)
{
	ARE_EQUAL(-1, string("  \n\r    \t").FirstIndexOfNotBlankChar());
	ARE_EQUAL(2, string("  ¥  ").FirstIndexOfNotBlankChar());
	ARE_EQUAL(2, string("  ¥  ").FirstIndexOfNotBlankChar(2));
	ARE_EQUAL(-1, string("  ¥  ").FirstIndexOfNotBlankChar(3));
}

//===========================================================================//

TEST(StringFirstIndexOfNotBlankCharExceptions)
{
	string someString = "H𠀀¥¥𠀀-H𠀀¥¥𠀀!";

	IS_THROW(someString.FirstIndexOfNotBlankChar(-1), IndexOutOfBoundsException);
	IS_THROW(someString.FirstIndexOfNotBlankChar(12), IndexOutOfBoundsException);
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: тесты работы слабого указателя
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Dummies/DummyBaseCounter.h"
#include "../../Dummies/DummyChildCounter.h"
#include "../../Dummies/DummyCompoundObject.h"
#include "../../Dummies/DummyChild.h"

namespace Bru
{

//===========================================================================//

TEST(WeakPtrDefaultConstructor)
{
	WeakPtr<DummyCompoundObject> weakPtr;

	IS_TRUE(weakPtr._objectPtr == NullPtr);
	IS_TRUE(weakPtr._refsCounter == NullPtr);
}

//===========================================================================//

TEST(WeakPtrNullPtrConstructor)
{
	WeakPtr<DummyCompoundObject> weakPtr(NullPtr);

	IS_TRUE(weakPtr._objectPtr == NullPtr);
	IS_TRUE(weakPtr._refsCounter == NullPtr);
}

//===========================================================================//

TEST(WeakPtrObjectWeakPtrConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> sourceWeakPtr(sharedPtr);
	WeakPtr<DummyCompoundObject> weakPtr(simplePtr, sourceWeakPtr);

	IS_TRUE(weakPtr._objectPtr == sourceWeakPtr._objectPtr);
	IS_TRUE(weakPtr._refsCounter == sourceWeakPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

	sourceWeakPtr = NullPtr;

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrObjectWeakPtrInheritanceConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sharedPtr(simplePtr);
	WeakPtr<DummyBase> sourceWeakPtr(sharedPtr);
	WeakPtr<DummyBase> weakPtr(simplePtr, sourceWeakPtr);

	IS_TRUE(weakPtr._objectPtr == sourceWeakPtr._objectPtr);
	IS_TRUE(weakPtr._refsCounter == sourceWeakPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

	sourceWeakPtr = NullPtr;

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrObjectWeakPtrCastConstructor)
{
	DummyBase * simplePtr = new DummyBase();
	SharedPtr<DummyBase> sharedPtr(simplePtr);
	WeakPtr<DummyBase> sourceWeakPtr(sharedPtr);
	WeakPtr<DummyChild> weakPtr(static_cast<DummyChild *>(simplePtr), sourceWeakPtr);

	IS_TRUE(weakPtr._objectPtr == sourceWeakPtr._objectPtr);
	IS_TRUE(weakPtr._refsCounter == sourceWeakPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

	sourceWeakPtr = NullPtr;

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrObjectWeakNullPtrConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullWeakPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(NullPtr, nullWeakPtr);

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrObjectWeakNullPtrInheritanceConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyChildCounter> nullWeakPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(NullPtr, nullWeakPtr);

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrObjectWeakNullPtrCastConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullWeakPtr(NullPtr);
		WeakPtr<DummyChildCounter> weakPtr(NullPtr, nullWeakPtr);

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrObjectWeakPtrConstructorExceptions)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> sourceWeakPtr(sharedPtr);
	IS_THROW(WeakPtr<DummyCompoundObject>(new DummyCompoundObject(666), sourceWeakPtr), InvalidArgumentException);
}

//===========================================================================//

TEST(WeakPtrObjectWeakPtrInheritanceConstructorExceptions)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sharedPtr(simplePtr);
	SharedPtr<DummyChild> sourceWeakPtr(sharedPtr);
	IS_THROW(SharedPtr<DummyBase>(new DummyChild(), sourceWeakPtr), InvalidArgumentException);
}

//===========================================================================//

TEST(WeakPtrObjectWeakPtrCastConstructorExceptions)
{
	DummyBase * simplePtr = new DummyBase();
	SharedPtr<DummyBase> sharedPtr(simplePtr);
	SharedPtr<DummyBase> sourceWeakPtr(sharedPtr);
	IS_THROW(SharedPtr<DummyChild>(static_cast<DummyChild *>(new DummyBase()), sourceWeakPtr), InvalidArgumentException);
}

//===========================================================================//

TEST(WeakPtrCopyConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> sourceWeakPtr(sharedPtr);
	WeakPtr<DummyCompoundObject> weakPtr(sourceWeakPtr);

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sourceWeakPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

	sourceWeakPtr = NullPtr;

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrInheritanceCopyConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sharedPtr(simplePtr);
	WeakPtr<DummyChild> sourceWeakPtr(sharedPtr);
	WeakPtr<DummyBase> weakPtr(sourceWeakPtr);

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sourceWeakPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

	sourceWeakPtr = NullPtr;

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrWeakNullPtrCopyConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullWeakPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(nullWeakPtr);

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrWeakNullPtrInheritanceCopyConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyChildCounter> nullWeakPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(nullWeakPtr);

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrSharedPtrConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

	sharedPtr = NullPtr;

	IS_TRUE(weakPtr == NullPtr);
	ARE_EQUAL(0, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrSharedPtrInheritanceConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sharedPtr(simplePtr);
	WeakPtr<DummyBase> weakPtr(sharedPtr);

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

	sharedPtr = NullPtr;

	ARE_EQUAL(0, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrSharedNullPtrConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> nullSharedPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(nullSharedPtr);

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrSharedNullPtrInheritanceConstructor)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyChildCounter> nullSharedPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(nullSharedPtr);

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrMoveConstructor)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(std::move(WeakPtr<DummyCompoundObject>(sharedPtr)));

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrInheritanceMoveConstructor)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sharedPtr(simplePtr);
	WeakPtr<DummyBase> weakPtr(std::move(WeakPtr<DummyChild>(sharedPtr)));

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

	sharedPtr = NullPtr;

	ARE_EQUAL(0, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrWeakNullPtrMoveConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> weakPtr(std::move(WeakPtr<DummyBaseCounter>(NullPtr)));

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrWeakNullPtrInheritanceMoveConstructor)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> weakPtr(std::move(WeakPtr<DummyChildCounter>(NullPtr)));

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(0, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(0, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrNullPtrCopyOperator)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	weakPtr = NullPtr;

	IS_TRUE(weakPtr._objectPtr == NullPtr);
	IS_TRUE(weakPtr._refsCounter == NullPtr);

	ARE_EQUAL(1, sharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(0, sharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyBaseCounter * firstSimplePtr = new DummyBaseCounter();
		DummyBaseCounter * secondSimplePtr = new DummyBaseCounter();

		SharedPtr<DummyBaseCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyBaseCounter> secondSharedPtr(secondSimplePtr);

		WeakPtr<DummyBaseCounter> firstWeakPtr(firstSharedPtr);
		WeakPtr<DummyBaseCounter> secondWeakPtr(secondSharedPtr);
		WeakPtr<DummyBaseCounter> weakPtr;

		weakPtr = firstWeakPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(firstWeakPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, firstWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondWeakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == firstSimplePtr);
		IS_TRUE(weakPtr._refsCounter == firstWeakPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

		weakPtr = secondWeakPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(firstWeakPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, firstWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondWeakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, secondWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		IS_TRUE(weakPtr._refsCounter == secondWeakPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

		firstWeakPtr = NullPtr;
		secondWeakPtr = NullPtr;

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyChildCounter * firstSimplePtr = new DummyChildCounter();
		DummyChildCounter * secondSimplePtr = new DummyChildCounter();

		SharedPtr<DummyChildCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyChildCounter> secondSharedPtr(secondSimplePtr);

		WeakPtr<DummyChildCounter> firstWeakPtr(firstSharedPtr);
		WeakPtr<DummyChildCounter> secondWeakPtr(secondSharedPtr);
		WeakPtr<DummyBaseCounter> weakPtr;

		weakPtr = firstWeakPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(firstWeakPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, firstWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondWeakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == firstSimplePtr);
		IS_TRUE(weakPtr._refsCounter == firstWeakPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

		weakPtr = secondWeakPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(firstWeakPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, firstWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondWeakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondWeakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, secondWeakPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		IS_TRUE(weakPtr._refsCounter == secondWeakPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(2, weakPtr._refsCounter->GetWeakRefsCount());

		firstWeakPtr = NullPtr;
		secondWeakPtr = NullPtr;

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());
}

//===========================================================================//

TEST(WeakPtrWeakNullPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullWeakPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());
		WeakPtr<DummyBaseCounter> weakPtr(sharedPtr);

		weakPtr = nullWeakPtr;

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

#ifdef DEBUGGING_POINTERS
TEST(WeakPtrWeakNullPtrTemporarySharedPtrCopyOperator)
{
	DummyBaseCounter::Reset();
	RefsCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullWeakPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(SharedPtr<DummyBaseCounter>(new DummyBaseCounter()));

		weakPtr = nullWeakPtr;

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	ARE_EQUAL(1, RefsCounter::GetCreatedCount());
	ARE_EQUAL(1, RefsCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
	RefsCounter::Reset();
}
#endif // DEBUGGING_POINTER

//===========================================================================//

TEST(WeakPtrWeakNullPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyChildCounter> nullWeakPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());
		WeakPtr<DummyBaseCounter> weakPtr(sharedPtr);

		weakPtr = nullWeakPtr;

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

#ifdef DEBUGGING_POINTERS
TEST(WeakPtrWeakNullPtrTemporarySharedPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();
	RefsCounter::Reset();

	{
		WeakPtr<DummyChildCounter> nullWeakPtr(NullPtr);
		WeakPtr<DummyBaseCounter> weakPtr(SharedPtr<DummyBaseCounter>(new DummyBaseCounter()));

		weakPtr = nullWeakPtr;

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	ARE_EQUAL(1, RefsCounter::GetCreatedCount());
	ARE_EQUAL(1, RefsCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
	RefsCounter::Reset();
}
#endif // DEBUGGING_POINTER

//===========================================================================//

TEST(WeakPtrSharedPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyBaseCounter * firstSimplePtr = new DummyBaseCounter();
		DummyBaseCounter * secondSimplePtr = new DummyBaseCounter();

		SharedPtr<DummyBaseCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyBaseCounter> secondSharedPtr(secondSimplePtr);

		WeakPtr<DummyBaseCounter> weakPtr;

		weakPtr = firstSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == firstSimplePtr);
		IS_TRUE(weakPtr._refsCounter == firstSharedPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

		weakPtr = secondSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		IS_TRUE(weakPtr._refsCounter == secondSharedPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
		
		firstSharedPtr = NullPtr;
		secondSharedPtr = NullPtr;

		ARE_EQUAL(0, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());		
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrSharedPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		DummyChildCounter * firstSimplePtr = new DummyChildCounter();
		DummyChildCounter * secondSimplePtr = new DummyChildCounter();

		SharedPtr<DummyChildCounter> firstSharedPtr(firstSimplePtr);
		SharedPtr<DummyChildCounter> secondSharedPtr(secondSimplePtr);

		WeakPtr<DummyBaseCounter> weakPtr;

		weakPtr = firstSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == firstSimplePtr);
		IS_TRUE(weakPtr._refsCounter == firstSharedPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());

		weakPtr = secondSharedPtr;

		IS_TRUE(firstSharedPtr._objectPtr == firstSimplePtr);
		ARE_EQUAL(1, firstSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(0, firstSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(secondSharedPtr._objectPtr == secondSimplePtr);
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, secondSharedPtr._refsCounter->GetWeakRefsCount());

		IS_TRUE(weakPtr._objectPtr == secondSimplePtr);
		IS_TRUE(weakPtr._refsCounter == secondSharedPtr._refsCounter);
		ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
		
		firstSharedPtr = NullPtr;
		secondSharedPtr = NullPtr;

		ARE_EQUAL(0, weakPtr._refsCounter->GetSharedRefsCount());
		ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());			
	}

	ARE_EQUAL(2, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(2, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrSharedNullPtrCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());
		WeakPtr<DummyBaseCounter> weakPtr(sharedPtr);

		weakPtr = nullSharedPtr;

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrSharedNullPtrInheritanceCopyOperator)
{
	DummyBaseCounter::Reset();

	{
		WeakPtr<DummyChildCounter> nullSharedPtr(NullPtr);
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());
		WeakPtr<DummyBaseCounter> weakPtr(sharedPtr);

		weakPtr = nullSharedPtr;

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

TEST(WeakPtrCopyOperatorSelf)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	weakPtr = weakPtr;

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrMoveOperator)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr;

	weakPtr = std::move(WeakPtr<DummyCompoundObject>(sharedPtr));

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrInheritanceMoveOperator)
{
	DummyChild * simplePtr = new DummyChild();
	SharedPtr<DummyChild> sharedPtr(simplePtr);
	WeakPtr<DummyBase> weakPtr;

	weakPtr = std::move(WeakPtr<DummyChild>(sharedPtr));

	IS_TRUE(weakPtr._objectPtr == simplePtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(1, weakPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, weakPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(WeakPtrWeakNullPtrMoveOperator)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());
		WeakPtr<DummyBaseCounter> weakPtr(sharedPtr);

		weakPtr = std::move(WeakPtr<DummyBaseCounter>(NullPtr));

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

#ifdef DEBUGGING_POINTERS
TEST(WeakPtrWeakNullPtrTemporarySharedPtrMoveOperator)
{
	DummyBaseCounter::Reset();
	RefsCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> weakPtr(SharedPtr<DummyBaseCounter>(new DummyBaseCounter()));

		weakPtr = std::move(WeakPtr<DummyBaseCounter>(NullPtr));

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	ARE_EQUAL(1, RefsCounter::GetCreatedCount());
	ARE_EQUAL(1, RefsCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
	RefsCounter::Reset();
}
#endif // DEBUGGING_POINTER

//===========================================================================//

TEST(WeakPtrWeakNullPtrInheritanceMoveOperator)
{
	DummyBaseCounter::Reset();

	{
		SharedPtr<DummyBaseCounter> sharedPtr(new DummyBaseCounter());
		WeakPtr<DummyBaseCounter> weakPtr(sharedPtr);

		weakPtr = std::move(WeakPtr<DummyChildCounter>(NullPtr));

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
}

//===========================================================================//

#ifdef DEBUGGING_POINTERS
TEST(WeakPtrWeakNullPtrTemporarySharedPtrInheritanceMoveOperator)
{
	DummyBaseCounter::Reset();
	RefsCounter::Reset();

	{
		WeakPtr<DummyBaseCounter> weakPtr(SharedPtr<DummyBaseCounter>(new DummyBaseCounter()));

		weakPtr = std::move(WeakPtr<DummyChildCounter>(NullPtr));

		IS_TRUE(weakPtr._objectPtr == NullPtr);
		IS_TRUE(weakPtr._refsCounter == NullPtr);
	}

	ARE_EQUAL(1, DummyBaseCounter::GetCreatedCount());
	ARE_EQUAL(1, DummyBaseCounter::GetDeletedCount());

	ARE_EQUAL(1, RefsCounter::GetCreatedCount());
	ARE_EQUAL(1, RefsCounter::GetDeletedCount());

	DummyBaseCounter::Reset();
	RefsCounter::Reset();
}
#endif // DEBUGGING_POINTER

//===========================================================================//

TEST(WeakPtrDereferenceOperator)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	IS_TRUE(*weakPtr == *simplePtr);
	IS_TRUE((*weakPtr).GetValue() == 13);
}

//===========================================================================//

TEST(WeakPtrDereferenceOperatorExceptions)
{
	SharedPtr<DummyCompoundObject> sharedNullPtr(NullPtr);
	WeakPtr<DummyCompoundObject> weakNullPtr(sharedNullPtr);

	IS_THROW((*weakNullPtr).GetValue(), NullPointerException);

	WeakPtr<DummyCompoundObject> weakPtr;
	{
		SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));
		weakPtr = sharedPtr;
	}

	IS_THROW((*weakPtr).GetValue(), NullPointerException);
}

//===========================================================================//

TEST(WeakPtrSelectorOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	IS_TRUE(weakPtr->GetValue() == 13);
}

//===========================================================================//

TEST(WeakPtrSelectorOperatorExceptions)
{
	SharedPtr<DummyCompoundObject> sharedNullPtr(NullPtr);
	WeakPtr<DummyCompoundObject> weakNullPtr(sharedNullPtr);

	IS_THROW(weakNullPtr->GetValue(), NullPointerException);

	WeakPtr<DummyCompoundObject> weakPtr;
	{
		SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));
		weakPtr = sharedPtr;
	}

	IS_THROW(weakPtr->GetValue(), NullPointerException);
}

//===========================================================================//

TEST(WeakPtrTypeOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	IS_TRUE(DummyCompoundObject::IsEqualsToValue(weakPtr, 13));
	IS_FALSE(DummyCompoundObject::IsEqualsToValue(weakPtr, 12));
}

//===========================================================================//

TEST(WeakPtrNullPtrEqualsOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));
	
	WeakPtr<DummyCompoundObject> nullWeakPtr;
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	IS_TRUE(nullWeakPtr == NullPtr);
	IS_TRUE(weakPtr != NullPtr);
	
	sharedPtr = NullPtr;
	
	IS_TRUE(weakPtr == NullPtr);
}

//===========================================================================//

TEST(WeakPtrObjectEqualsOperator)
{
	DummyCompoundObject * equalsSimplePtr = new DummyCompoundObject(13);
	DummyCompoundObject * notEqualsSimplePtr = new DummyCompoundObject(666);
	SharedPtr<DummyCompoundObject> sharedPtr(equalsSimplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	IS_TRUE(weakPtr == equalsSimplePtr);
	IS_TRUE(weakPtr != notEqualsSimplePtr);

	delete notEqualsSimplePtr;
}

//===========================================================================//

TEST(WeakPtrEqualsOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);
	WeakPtr<DummyCompoundObject> equalsWeakPtr(sharedPtr);
	SharedPtr<DummyCompoundObject> notEqualsSharedPtr(new DummyCompoundObject(666));
	WeakPtr<DummyCompoundObject> notEqualsWeakPtr(notEqualsSharedPtr);

	IS_TRUE(weakPtr == equalsWeakPtr);
	IS_TRUE(weakPtr != notEqualsWeakPtr);
}

//===========================================================================//

TEST(WeakPtrEqualsOperatorInheritance)
{
	SharedPtr<DummyChild> sharedPtr(new DummyChild());
	WeakPtr<DummyBase> weakPtr(sharedPtr);
	WeakPtr<DummyBase> equalsWeakPtr(sharedPtr);
	SharedPtr<DummyChild> notEqualsSharedPtr(new DummyChild());
	WeakPtr<DummyBase> notEqualsWeakPtr(notEqualsSharedPtr);

	IS_TRUE(weakPtr == equalsWeakPtr);
	IS_TRUE(weakPtr != notEqualsWeakPtr);
}

//===========================================================================//

TEST(WeakPtrEqualsSharedPtrOperator)
{
	SharedPtr<DummyCompoundObject> sharedPtr(new DummyCompoundObject(13));
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);
	SharedPtr<DummyCompoundObject> equalsSharedPtr(sharedPtr);
	SharedPtr<DummyCompoundObject> notEqualsSharedPtr(new DummyCompoundObject(666));

	IS_TRUE(weakPtr == equalsSharedPtr);
	IS_TRUE(weakPtr != notEqualsSharedPtr);
}

//===========================================================================//

TEST(WeakPtrEqualsSharedPtrInheritanceOperator)
{
	SharedPtr<DummyChild> sharedPtr(new DummyChild());
	WeakPtr<DummyBase> weakPtr(sharedPtr);
	SharedPtr<DummyBase> equalsSharedPtr(sharedPtr);
	SharedPtr<DummyBase> notEqualsSharedPtr(new DummyChild());

	IS_TRUE(weakPtr == equalsSharedPtr);
	IS_TRUE(weakPtr != notEqualsSharedPtr);
}

//===========================================================================//

TEST(WeakPtrLock)
{
	DummyCompoundObject * simplePtr = new DummyCompoundObject(13);
	SharedPtr<DummyCompoundObject> sharedPtr(simplePtr);
	WeakPtr<DummyCompoundObject> weakPtr(sharedPtr);

	SharedPtr<DummyCompoundObject> lockedSharedPtr = weakPtr.Lock();

	IS_TRUE(lockedSharedPtr._objectPtr == simplePtr);
	IS_TRUE(lockedSharedPtr._refsCounter == sharedPtr._refsCounter);

	ARE_EQUAL(2, lockedSharedPtr._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, lockedSharedPtr._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

}// namespace Bru

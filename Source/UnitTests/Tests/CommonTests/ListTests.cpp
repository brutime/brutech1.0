//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты класса списка
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(ListGetters)
{
	List<DummyCompoundObject> list;

	list._count = 10;
	ARE_EQUAL(10, list.GetCount());
}

//===========================================================================//

TEST(ListDefaultConstructor)
{
	List<DummyCompoundObject> list;

	ARE_EQUAL(0, list.GetCount());
	IS_TRUE(list._head == NullPtr);
	IS_TRUE(list._tail == NullPtr);
}

//===========================================================================//

TEST(ListInitializerListConstructor)
{
	const integer ExpectedListCount = 10;
	const DummyCompoundObject ExpectedList[ExpectedListCount] = { d0, d1, d2, d3, d4, d5, d6, d7, d8, d9 };

	List<DummyCompoundObject> list = { d0, d1, d2, d3, d4, d5, d6, d7, d8, d9 };

	ARE_EQUAL(ExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ExpectedList, list, ExpectedListCount);
}

//===========================================================================//

TEST(ListCopyConstructor)
{
	List<float> sourceList = ContainersHelper::CreateTestFloatsList();

	List<float> list(sourceList);

	sourceList.Clear();

	ARE_EQUAL(TestFloatsCount, list.GetCount());
	ARE_ARRAYS_CLOSE(TestFloats, list, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(ListMoveConstructor)
{
	List<float> list(std::move(ContainersHelper::CreateTestFloatsList()));

	ARE_EQUAL(TestFloatsCount, list.GetCount());
	ARE_ARRAYS_CLOSE(TestFloats, list, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(ListCopyOperator)
{
	List<float> sourceList = ContainersHelper::CreateTestFloatsList();

	List<float> list;
	list = sourceList;

	sourceList.Clear();

	ARE_EQUAL(TestFloatsCount, list.GetCount());
	ARE_ARRAYS_CLOSE(TestFloats, list, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(ListCopyOperatorSelf)
{
	List<float> list = ContainersHelper::CreateTestFloatsList();

	list = list;

	ARE_EQUAL(TestFloatsCount, list.GetCount());
	ARE_ARRAYS_CLOSE(TestFloats, list, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(ListMoveOperator)
{
	List<float> list;
	list = ContainersHelper::CreateTestFloatsList();

	ARE_EQUAL(TestFloatsCount, list.GetCount());
	ARE_ARRAYS_CLOSE(TestFloats, list, TestFloatsCount, Math::Accuracy);
}

//===========================================================================//

TEST(ListEqualsOperators)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	List<DummyCompoundObject> equalsList = ContainersHelper::CreateTestDummyCompoundObjectList();
	List<DummyCompoundObject> notEqualsList = ContainersHelper::CreateTestDummyCompoundObjectList();
	notEqualsList.GetLast() = d666;

	IS_TRUE(list == equalsList);
	IS_TRUE(list != notEqualsList);
}

//===========================================================================//

TEST(ListAdd)
{
	List<DummyCompoundObject> list;

	list.Add(d1);

	const integer FirstExpectedListCount = 1;
	const DummyCompoundObject FirstExpectedList[FirstExpectedListCount] = { d1 };

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);

	list.Add(d2);

	const integer SecondExpectedListCount = 2;
	const DummyCompoundObject SecondExpectedList[SecondExpectedListCount] = { d1, d2 };

	ARE_EQUAL(SecondExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(SecondExpectedList, list, SecondExpectedListCount);

	list.Add(d3);

	const integer ThirdExpectedListCount = 3;
	const DummyCompoundObject ThirdExpectedList[ThirdExpectedListCount] = { d1, d2, d3 };

	ARE_EQUAL(ThirdExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ThirdExpectedList, list, ThirdExpectedListCount);
}

//===========================================================================//

TEST(ListSimpleTypeValue)
{
	List<integer> list;

	list.Add(1);

	const integer FirstExpectedListCount = 1;
	const integer FirstExpectedList[FirstExpectedListCount] = { 1 };

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);

	list.Add(2);

	const integer SecondExpectedListCount = 2;
	const integer SecondExpectedList[SecondExpectedListCount] = { 1, 2 };

	ARE_EQUAL(SecondExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(SecondExpectedList, list, SecondExpectedListCount);

	list.Add(3);

	const integer ThirdExpectedListCount = 3;
	const integer ThirdExpectedList[ThirdExpectedListCount] = { 1, 2, 3 };

	ARE_EQUAL(ThirdExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ThirdExpectedList, list, ThirdExpectedListCount);
}

//===========================================================================//

TEST(ListAddOtherList)
{
	List<DummyCompoundObject> list = { d1, d2, d3 };

	list.Add(List<DummyCompoundObject> { d4, d5, d6 });

	const integer FirstExpectedListCount = 6;
	const DummyCompoundObject FirstExpectedList[FirstExpectedListCount] = { d1, d2, d3, d4, d5, d6 };

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);

	list.Add(List<DummyCompoundObject>());

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);
}

//===========================================================================//

TEST(ListAddOtherArrayExceptions)
{
	List<DummyCompoundObject> list;
	IS_THROW(list.Add(list), InvalidArgumentException);
}

//===========================================================================//

TEST(ListAddArray)
{
	List<DummyCompoundObject> list = { d1, d2, d3 };

	list.Add(Array<DummyCompoundObject> { d4, d5, d6 });

	const integer FirstExpectedListCount = 6;
	const DummyCompoundObject FirstExpectedList[FirstExpectedListCount] = { d1, d2, d3, d4, d5, d6 };

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);

	list.Add(Array<DummyCompoundObject>());

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);
}

//===========================================================================//

TEST(ListInsert)
{
	List<DummyCompoundObject> list;

	DummyCompoundObject i1(4);
	DummyCompoundObject i2(5);
	DummyCompoundObject i3(6);

	list.Add(d1);
	list.Add(d2);
	list.Add(d3);

	list.Insert(0, i1);
	list.Insert(2, i2);
	list.Insert(5, i3);

	const integer ExpectedListCount = 6;
	const DummyCompoundObject ExpectedList[ExpectedListCount] = { i1, d1, i2, d2, d3, i3 };

	ARE_EQUAL(ExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ExpectedList, list, ExpectedListCount);
}

//===========================================================================//

TEST(ListInsertSimpleTypeValue)
{
	List<integer> list;

	list.Add(2);
	list.Add(4);
	list.Add(5);

	list.Insert(0, 1);
	list.Insert(2, 3);
	list.Insert(5, 6);

	const integer ExpectedListCount = 6;
	const integer ExpectedList[ExpectedListCount] = { 1, 2, 3, 4, 5, 6 };

	ARE_EQUAL(ExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ExpectedList, list, ExpectedListCount);
}

//===========================================================================//

#ifdef DEBUGGING
TEST(ListInsertExceptions)
{
	List<DummyCompoundObject> emptyList;
	IS_THROW(emptyList.Insert(-1, d1), IndexOutOfBoundsException);
	IS_THROW(emptyList.Insert(1, d1), IndexOutOfBoundsException);
}
#endif

//===========================================================================//

TEST(ListRemove)
{
	List<DummyCompoundObject> list;

	list.Add(d1);
	list.Add(d2);
	list.Add(d3);
	list.Add(d4);

	list.Remove(d3);

	const integer FirstExpectedListCount = 3;
	const DummyCompoundObject FirstExpectedList[FirstExpectedListCount] = { d1, d2, d4 };

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);

	list.Remove(d1);

	const integer SecondExpectedListCount = 2;
	const DummyCompoundObject SecondExpectedList[SecondExpectedListCount] = { d2, d4 };

	ARE_EQUAL(SecondExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(SecondExpectedList, list, SecondExpectedListCount);

	list.Remove(d4);

	const integer ThirdExpectedListCount = 1;
	const DummyCompoundObject ThirdExpectedList[ThirdExpectedListCount] = { d2 };

	ARE_EQUAL(ThirdExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ThirdExpectedList, list, ThirdExpectedListCount);

	list.Remove(d2);

	ARE_EQUAL(0, list.GetCount());
	IS_TRUE(list._head == NullPtr);
	IS_TRUE(list._tail == NullPtr);
}

//===========================================================================//

TEST(ListRemoveCycle)
{
	List<integer> list;

	list.Add(1);

	for (integer i = 0; i < 20; i++)
	{
		list.Add(2);
		list.Remove(2);
	}

	ARE_EQUAL(1, list.GetCount());
	IS_TRUE(list.Contains(1));
}

//===========================================================================//

TEST(ListRemoveExceptions)
{
	List<DummyCompoundObject> map;

	map.Add(d1);
	map.Add(d2);

	IS_THROW(map.Remove(d3), InvalidArgumentException);
}

//===========================================================================//

TEST(ListRemoveIfExists)
{
	List<DummyCompoundObject> list = { d1, d2, d3, d2, d4 };
	list.RemoveIfExists(d2);
	ARE_EQUAL(4, list.GetCount());
	IS_TRUE(list == List<DummyCompoundObject>({d1, d3, d2, d4}));
	list.RemoveIfExists(d5);
	ARE_EQUAL(4, list.GetCount());
}

//===========================================================================//

TEST(ListRemoveAt)
{
	List<DummyCompoundObject> list = { d1, d2, d3, d4 };

	list.RemoveAt(3);

	const integer FirstExpectedListCount = 3;
	const DummyCompoundObject FirstExpectedList[FirstExpectedListCount] = { d1, d2, d3 };

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);

	list.RemoveAt(2);

	const integer SecondExpectedListCount = 2;
	const DummyCompoundObject SecondExpectedList[SecondExpectedListCount] = { d1, d2 };

	ARE_EQUAL(SecondExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(SecondExpectedList, list, SecondExpectedListCount);

	list.RemoveAt(0);

	const integer ThirdExpectedListCount = 1;
	const DummyCompoundObject ThirdExpectedList[ThirdExpectedListCount] = { d2 };

	ARE_EQUAL(ThirdExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ThirdExpectedList, list, ThirdExpectedListCount);

	list.RemoveAt(0);

	ARE_EQUAL(0, list.GetCount());
	IS_TRUE(list._head == NullPtr);
	IS_TRUE(list._tail == NullPtr);
}

//===========================================================================//

TEST(ListRemoveAtExceptions)
{
	List<DummyCompoundObject> emptyList;
	IS_THROW(emptyList.RemoveAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyList.RemoveAt(0), IndexOutOfBoundsException);

	List<DummyCompoundObject> list;

	list.Add(d1);
	list.Add(d2);

	IS_THROW(emptyList.RemoveAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyList.RemoveAt(2), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ListRemoveAllStartingAt)
{
	List<float> list = ContainersHelper::CreateTestFloatsList();
	list.RemoveAllStartingAt(2);
	ARE_EQUAL(2, list.GetCount());
	ARE_CLOSE(1.11f, list[0], Math::Accuracy);
	ARE_CLOSE(2.22f, list[1], Math::Accuracy);
}

//===========================================================================//

TEST(ListRemoveAllStartingAtExceptions)
{
	List<DummyCompoundObject> emptyList;
	IS_THROW(emptyList.RemoveAllStartingAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyList.RemoveAllStartingAt(0), IndexOutOfBoundsException);

	List<DummyCompoundObject> list;

	list.Add(d1);
	list.Add(d2);

	IS_THROW(emptyList.RemoveAllStartingAt(-1), IndexOutOfBoundsException);
	IS_THROW(emptyList.RemoveAllStartingAt(2), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ListRemoveWithPredicate)
{
	List<DummyCompoundObject> list = { d1, d1, d2, d3, d1, d4, d5, d6 };

	list.Remove([] (const DummyCompoundObject & number)
	            {
	                return number == 1;
				});

	const integer FirstExpectedListCount = 5;
	const DummyCompoundObject FirstExpectedList[FirstExpectedListCount] = { d2, d3, d4, d5, d6 };

	ARE_EQUAL(FirstExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(FirstExpectedList, list, FirstExpectedListCount);

	list.Remove([] (const DummyCompoundObject & number)
	            {
	                return number <= 3;
				});

	const integer SecondExpectedListCount = 3;
	const DummyCompoundObject SecondExpectedList[SecondExpectedListCount] = { d4, d5, d6 };

	ARE_EQUAL(SecondExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(SecondExpectedList, list, SecondExpectedListCount);

	list.Remove([] (const DummyCompoundObject & number)
	            {
	                return (number > 3 && number < 6);
				});

	const integer ThirdExpectedListCount = 1;
	const DummyCompoundObject ThirdExpectedList[ThirdExpectedListCount] = { d6 };

	ARE_EQUAL(ThirdExpectedListCount, list.GetCount());
	ARE_ARRAYS_EQUAL(ThirdExpectedList, list, ThirdExpectedListCount);

	list.Remove([] (const DummyCompoundObject & number)
	            {
	                return number != 0;
				});

	ARE_EQUAL(0, list.GetCount());
	IS_TRUE(list.IsEmpty());
}

//===========================================================================//

TEST(ListReverse)
{
	List<DummyCompoundObject> list = { d1, d2, d3, d4 };

	list.Reverse();

	const integer ExpectedListCount = 4;
	const DummyCompoundObject ExpectedList[ExpectedListCount] = { d4, d3, d2, d1 };

	ARE_EQUAL(ExpectedListCount, list.GetCount());
	//тут руками перебираем все чтобы убедиться в правильности head, tail
	integer index = 0;
	for (const DummyCompoundObject & object : list)
	{
		ARE_EQUAL(ExpectedList[index], object);
		++index;
	}

	ARE_EQUAL(ExpectedListCount, index);
}

//===========================================================================//

TEST(ListOperatorSet)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	DummyCompoundObject expectedArray[TestDummyesCount];

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		list[i] = DummyCompoundObject(i);
		expectedArray[i] = DummyCompoundObject(i);
	}

	ARE_EQUAL(TestDummyesCount, list.GetCount());
	ARE_ARRAYS_EQUAL(expectedArray, list, TestDummyesCount);
}

//===========================================================================//

TEST(ListOperatorSetExceptions)
{
	List<DummyCompoundObject> emptyList;
	IS_THROW(emptyList[-1] = d11, IndexOutOfBoundsException);
	IS_THROW(emptyList[0] = d11, IndexOutOfBoundsException);

	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	IS_THROW(list[-1] = d1, IndexOutOfBoundsException);
	IS_THROW(list[list.GetCount()] = d1, IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ListOperatorGet)
{
	const List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	//Вот тут и провериться []
	ARE_ARRAYS_EQUAL(TestDummyes, list, TestDummyesCount);
}

//===========================================================================//

TEST(ListOperatorGetExceptions)
{
	const List<DummyCompoundObject> emptyList;
	IS_THROW(emptyList[-1], IndexOutOfBoundsException);
	IS_THROW(emptyList[0], IndexOutOfBoundsException);

	const List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	IS_THROW(list[-1], IndexOutOfBoundsException);
	IS_THROW(list[list.GetCount()], IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ListGetFirst)
{
	const List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	ARE_EQUAL(TestDummyes[0], list.GetFirst());
}

//===========================================================================//

TEST(ListGetFirstExceptions)
{
	const List<DummyCompoundObject> emptyList;
	IS_THROW(emptyList.GetFirst(), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ListGetLast)
{
	const List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	ARE_EQUAL(TestDummyes[TestDummyesCount - 1], list.GetLast());
}

//===========================================================================//

TEST(ListGetLastExceptions)
{
	const List<DummyCompoundObject> emptyList;
	IS_THROW(emptyList.GetLast(), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(ListContains)
{
	const List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	IS_TRUE(list.Contains(TestDummyes[0]));
	IS_TRUE(list.Contains(TestDummyes[(TestDummyesCount - 1) / 2]));
	IS_TRUE(list.Contains(TestDummyes[TestDummyesCount - 1]));
	IS_TRUE(!list.Contains(DummyCompoundObject(TestDummyesCount + 666)));
}

//===========================================================================//

TEST(ListContainsWithPredicate)
{
	List<DummyCompoundObject> list = {d1, d3, d4};
	IS_TRUE(list.Contains([] (const DummyCompoundObject & number) -> bool {return number > 1; }))
	IS_TRUE(!list.Contains([] (const DummyCompoundObject & number) -> bool {return number > 4; }))
}

//===========================================================================//

TEST(ListIndexOf)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		ARE_EQUAL(i, list.IndexOf(TestDummyes[i]));
	}

	ARE_EQUAL(-1, list.IndexOf(DummyCompoundObject(TestDummyesCount + 666)));
}

//===========================================================================//

TEST(ListSort)
{
	const integer ListSize = 100;
	List<DummyCompoundObject> list;

	for (integer i = 0; i < ListSize; i++)
	{
		list.Add(DummyCompoundObject(Math::Random(ListSize)));
	}

	DummyCompoundObject::AscendingComparator ascendingComparator;
	list.Sort(ascendingComparator);

	for (integer i = 0; i < ListSize - 1; i++)
	{
		IS_TRUE(list[i] <= list[i + 1]);
	}

	DummyCompoundObject::DescendingComparator descendingComparator;
	list.Sort(descendingComparator);

	for (integer i = 0; i < ListSize - 1; i++)
	{
		IS_TRUE(list[i + 1] <= list[i]);
	}
}

//===========================================================================//

TEST(ListFillFromEmptyArray)
{
	Array<DummyCompoundObject> emptyArray;
	List<DummyCompoundObject> emptyList;

	emptyList.FillFromArray(emptyArray);

	ARE_EQUAL(0, emptyList.GetCount());
	IS_TRUE(emptyList._head == NullPtr);
	IS_TRUE(emptyList._tail == NullPtr);

	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	list.FillFromArray(emptyArray);

	ARE_EQUAL(0, list.GetCount());
	IS_TRUE(list._head == NullPtr);
	IS_TRUE(list._tail == NullPtr);
}

//===========================================================================//

TEST(ListFillFromArray)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();
	List<DummyCompoundObject> list;

	list.FillFromArray(array);

	ARE_EQUAL(TestDummyesCount, list.GetCount());
	ARE_ARRAYS_EQUAL(TestDummyes, list, TestDummyesCount);
}

//===========================================================================//

TEST(ListToArray)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();
	Array<DummyCompoundObject> array = list.ToArray();

	//Не изменился-ли список
	ARE_EQUAL(TestDummyesCount, list.GetCount());
	ARE_ARRAYS_EQUAL(TestDummyes, list, TestDummyesCount);

	//Проверяем полученный массив
	ARE_EQUAL(TestDummyesCount, array.GetCount());
	ARE_EQUAL(TestDummyesCount, array._reservedCount);
	ARE_ARRAYS_EQUAL(TestDummyes, array, TestDummyesCount);
}

//===========================================================================//

TEST(ListClear)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	list.Clear();

	ARE_EQUAL(0, list.GetCount());
	IS_TRUE(list._head == NullPtr);
	IS_TRUE(list._tail == NullPtr);
}

//===========================================================================//

TEST(ListIsEmpty)
{
	List<string> list;
	IS_TRUE(list.IsEmpty());

	list.Add("one");
	IS_FALSE(list.IsEmpty());
}

//===========================================================================//

TEST(ListCreateIterator)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	ListIterator<DummyCompoundObject> it = list.CreateIterator();

	IS_TRUE(it._owner == & list);
	IS_TRUE(it._cursor == list._head);
}

//===========================================================================//

TEST(ListBegin)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	ListIterator<DummyCompoundObject> it = list.begin();

	IS_TRUE(it._owner == & list);
	IS_TRUE(it._cursor == list._head);
}

//===========================================================================//

TEST(ListEnd)
{
	List<DummyCompoundObject> list = ContainersHelper::CreateTestDummyCompoundObjectList();

	ListIterator<DummyCompoundObject> it = list.end();

	IS_TRUE(it._owner == & list);
	IS_TRUE(it._cursor == NullPtr);
}

//===========================================================================//

} // namespace Bru

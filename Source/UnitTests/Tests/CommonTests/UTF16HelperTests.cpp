//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF16 помощника
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringTestsHelper.h"
#include "../../Helpers/StringsTestData.h"

namespace Bru
{

//===========================================================================//

TEST(UTF16HelperGetLength)
{
	UTFLength nullTerminatorStringLength = UTF16Helper::GetLength(NullTerminator);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInChars);
	ARE_EQUAL(0, nullTerminatorStringLength.LengthInComponents);

	UTFLength stringLength = UTF16Helper::GetLength(UTF16TestWords);
	ARE_EQUAL(UTF16TestCharsCount, stringLength.LengthInChars);
	ARE_EQUAL(UTF16TestWordsCount, stringLength.LengthInComponents);

	ARE_EQUAL(string::MaxLength,
	          UTF16Helper::GetLength(StringTestsHelper->GetMaxLengthUTF16Chars()).LengthInComponents);
	ARE_EQUAL(string::MaxLength, UTF16Helper::GetLength(StringTestsHelper->GetMaxLengthUTF16Chars()).LengthInChars);
}

//===========================================================================//

TEST(UTF16HelperGetCharLengthInWords)
{
	for (integer signedChar = static_cast<integer>(UTF16Helper::FirstCodeInUTF16Sequence);
	        signedChar <= static_cast<integer>(UTF16Helper::FirstCodeInFirstSurrogateRange - 1); signedChar++)
	{
		ARE_EQUAL(1, UTF16Helper::GetCharLengthInWords(static_cast<ushort>(signedChar)));
	}

	for (integer signedChar = static_cast<integer>(UTF16Helper::FirstCodeInFirstSurrogateRange);
	        signedChar <= static_cast<integer>(UTF16Helper::LastCodeInFirstSurrogateRange); signedChar++)
	{
		ARE_EQUAL(2, UTF16Helper::GetCharLengthInWords(static_cast<ushort>(signedChar)));
	}

	for (integer signedChar = static_cast<integer>(UTF16Helper::LastCodeInSecondSurrogateRange + 1);
	        signedChar <= static_cast<integer>(UTF16Helper::LastCodeInUTF16Sequence); signedChar++)
	{
		ARE_EQUAL(1, UTF16Helper::GetCharLengthInWords(static_cast<ushort>(signedChar)));
	}
}

//===========================================================================//

TEST(UTF16HelperGetCharLengthInWordsExceptions)
{
	//Для всех чисел из интервала выбрасывать Exception очень долго
	IS_THROW(UTF16Helper::GetCharLengthInWords(UTF16Helper::FirstCodeInSecondSurrogateRange),
	         InvalidUTF16SequenceException);
	IS_THROW(
	    UTF16Helper::GetCharLengthInWords(UTF16Helper::FirstCodeInSecondSurrogateRange + (UTF16Helper::LastCodeInSecondSurrogateRange - UTF16Helper::FirstCodeInSecondSurrogateRange) / 2),
	    InvalidUTF16SequenceException);
	IS_THROW(UTF16Helper::GetCharLengthInWords(UTF16Helper::LastCodeInSecondSurrogateRange),
	         InvalidUTF16SequenceException);
}

//===========================================================================//

TEST(UTF16HelperCheck)
{
	//Этот тест просто не должен вызывать экспшенов
	UTF16Helper::Check(NullTerminator);
	UTF16Helper::Check(UTF16TestWords);
	UTF16Helper::Check(StringTestsHelper->GetMaxLengthUTF16Chars());
}

//===========================================================================//

TEST(UTF16HelperCheckExceptions)
{
	for (integer signedChar = static_cast<integer>(UTF16Helper::FirstCodeInFirstNoncharRange);
	        signedChar <= static_cast<integer>(UTF16Helper::LastCodeInFirstNoncharRange); signedChar++)
	{
		ushort nonCharChars[2] = { static_cast<ushort>(signedChar), NullTerminator };
		IS_THROW(UTF16Helper::Check(nonCharChars), InvalidUTF16SequenceException);
	}

	for (integer signedChar = static_cast<integer>(UTF16Helper::FirstCodeInSecondNoncharRange);
	        signedChar <= static_cast<integer>(UTF16Helper::LastCodeInSecondNoncharRange); signedChar++)
	{
		ushort nonCharChars[2] = { static_cast<ushort>(signedChar), NullTerminator };
		IS_THROW(UTF16Helper::Check(nonCharChars), InvalidUTF16SequenceException);
	}

	ushort firstCodeInSecondSurrogateRange[2] = { UTF16Helper::FirstCodeInSecondSurrogateRange, NullTerminator };
	IS_THROW(UTF16Helper::Check(firstCodeInSecondSurrogateRange), InvalidArgumentException);

	ushort middleCodeInSecondSurrogateRange[2] = { static_cast<ushort>(UTF16Helper::FirstCodeInSecondSurrogateRange
	        + (UTF16Helper::LastCodeInSecondSurrogateRange - UTF16Helper::FirstCodeInSecondSurrogateRange) / 2),
	        NullTerminator
	                                             };
	IS_THROW(UTF16Helper::Check(middleCodeInSecondSurrogateRange), InvalidArgumentException);

	ushort lastCodeInSecondSurrogateRange[2] = { UTF16Helper::LastCodeInSecondSurrogateRange, NullTerminator };
	IS_THROW(UTF16Helper::Check(lastCodeInSecondSurrogateRange), InvalidArgumentException);
}

//===========================================================================//

TEST(UTF16HelperCheckNoncharsExceptions)
{
	static const integer SourceNonCharSurrogatesCharsCount = 32;
	static const ushort SourceNonCharSurrogatesChars[SourceNonCharSurrogatesCharsCount][2] = { { 0xD83F, 0xDFFE }, {
			0xD83F, 0xDFFF
		}, { 0xD87F, 0xDFFE }, { 0xD87F, 0xDFFF }, { 0xD8BF, 0xDFFE }, { 0xD8BF, 0xDFFF }, {
			0xD8FF,
			0xDFFE
		}, { 0xD8FF, 0xDFFF }, { 0xD93F, 0xDFFE }, { 0xD93F, 0xDFFF }, { 0xD97F, 0xDFFE }, { 0xD97F, 0xDFFF }, {
			0xD9BF, 0xDFFE
		}, { 0xD9BF, 0xDFFF }, { 0xD9FF, 0xDFFE }, { 0xD9FF, 0xDFFF }, { 0xDA3F, 0xDFFE }, {
			0xDA3F,
			0xDFFF
		}, { 0xDA7F, 0xDFFE }, { 0xDA7F, 0xDFFF }, { 0xDABF, 0xDFFE }, { 0xDABF, 0xDFFF }, { 0xDAFF, 0xDFFE }, {
			0xDAFF, 0xDFFF
		}, { 0xDB3F, 0xDFFE }, { 0xDB3F, 0xDFFF }, { 0xDB7F, 0xDFFE }, { 0xDB7F, 0xDFFF }, {
			0xDBBF,
			0xDFFE
		}, { 0xDBBF, 0xDFFF }, { 0xDBFF, 0xDFFE }, { 0xDBFF, 0xDFFF },
	};

	for (integer i = 0; i < SourceNonCharSurrogatesCharsCount; i++)
	{
		ushort nonCharSurrogatesChars[3] = { SourceNonCharSurrogatesChars[i][0], SourceNonCharSurrogatesChars[i][1],
		                                     NullTerminator
		                                   };
		IS_THROW(UTF16Helper::Check(nonCharSurrogatesChars), InvalidArgumentException);
	}
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты конвертера типов
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Math/Math.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

TEST(BitConverterToInteger)
{
	Array<byte>::SimpleType byteArray;
	byteArray.Add(232);
	byteArray.Add(64);
	byteArray.Add(0);
	byteArray.Add(0);

	integer fullInteger = BitConverter::ToInteger(byteArray);
	ARE_EQUAL(16616, fullInteger);

	Array<byte>::SimpleType notFullByteArray;
	notFullByteArray.Add(232);
	notFullByteArray.Add(64);

	integer notFullInteger = BitConverter::ToInteger(notFullByteArray);
	ARE_EQUAL(16616, notFullInteger);
}

//===========================================================================//

TEST(BitConverterIntegerToBytes)
{
	integer i = 16616;
	Array<byte>::SimpleType byteArray = BitConverter::ToBytes(i);

	const integer ControlArrayCount = 4;
	const integer ControlArray[ControlArrayCount] = { 232, 64, 0, 0 };

	ARE_EQUAL(ControlArrayCount, byteArray.GetCount());
	ARE_ARRAYS_EQUAL(ControlArray, byteArray, ControlArrayCount);
}

//===========================================================================//

TEST(BitConverterIntegerToBytesCount)
{
	integer i = 16616;
	Array<byte>::SimpleType byteArray = BitConverter::ToBytes(i, 2);

	const integer ControlArrayCount = 2;
	const integer ControlArray[ControlArrayCount] = { 232, 64 };

	ARE_EQUAL(ControlArrayCount, byteArray.GetCount());
	ARE_ARRAYS_EQUAL(ControlArray, byteArray, ControlArrayCount);
}

//===========================================================================//

TEST(BitConverterToFloat)
{
	Array<byte>::SimpleType byteArray;
	byteArray.Add(65);
	byteArray.Add(80);
	byteArray.Add(0);
	byteArray.Add(0);

	float fullFloat = BitConverter::ToFloat(byteArray);
	ARE_CLOSE(13.0f, fullFloat, Math::Accuracy);

	Array<byte>::SimpleType notFullByteArray;
	notFullByteArray.Add(65);
	notFullByteArray.Add(80);

	float notFullFloat = BitConverter::ToFloat(notFullByteArray);
	ARE_CLOSE(13.0f, notFullFloat, Math::Accuracy);
}

//===========================================================================//

TEST(BitConverterFloatToBytes)
{
	float f = 13.0f;
	Array<byte>::SimpleType byteArray = BitConverter::ToBytes(f);

	const integer ControlArrayCount = 4;
	const integer ControlArray[ControlArrayCount] = { 65, 80, 0, 0 };

	ARE_EQUAL(ControlArrayCount, byteArray.GetCount());
	ARE_ARRAYS_EQUAL(ControlArray, byteArray, ControlArrayCount);
}

//===========================================================================//

TEST(BitConverterFloatToBytesCount)
{
	float f = 13.0f;
	Array<byte>::SimpleType byteArray = BitConverter::ToBytes(f, 2);

	const integer ControlArrayCount = 2;
	const integer ControlArray[ControlArrayCount] = { 65, 80 };

	ARE_EQUAL(ControlArrayCount, byteArray.GetCount());
	ARE_ARRAYS_EQUAL(ControlArray, byteArray, ControlArrayCount);
}

//===========================================================================//

}// namespace Bru

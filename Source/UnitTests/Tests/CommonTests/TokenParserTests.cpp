//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты парсера строки
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"

namespace Bru
{

//===========================================================================//

TEST(TokenParserConstructorEmpty)
{
	TokenParser parser(string::Empty, string::Empty);
	ARE_EQUAL(0, parser._tokenInfos.GetCount());
	ARE_EQUAL(0, parser._currentTokenIndex);
	ARE_EQUAL(string::Empty, parser._delimiters);
}

//===========================================================================//

TEST(TokenParserConstructorJustDelimiters)
{
	TokenParser parser(" , \n ;", TokenParser::DefaultDelimiters);
	ARE_EQUAL(0, parser._tokenInfos.GetCount());
	ARE_EQUAL(0, parser._currentTokenIndex);
	ARE_EQUAL(TokenParser::DefaultDelimiters, parser._delimiters);
}

//===========================================================================//

TEST(TokenParserConstructor)
{
	TokenParser parser("one, two;   three\nfour", TokenParser::DefaultDelimiters);
	ARE_EQUAL(4, parser._tokenInfos.GetCount());
	ARE_EQUAL(0, parser._currentTokenIndex);
	ARE_EQUAL(TokenParser::DefaultDelimiters, parser._delimiters);

	ARE_EQUAL("one", parser._tokenInfos[0]->Token);
	ARE_EQUAL("two", parser._tokenInfos[1]->Token);
	ARE_EQUAL("three", parser._tokenInfos[2]->Token);
	ARE_EQUAL("four", parser._tokenInfos[3]->Token);
}

//===========================================================================//

TEST(TokenParserGetNextTokenReset)
{
	TokenParser parser("one two");

	ARE_EQUAL("one", parser.GetNextToken());
	ARE_EQUAL("two", parser.GetNextToken());

	parser.Reset();

	ARE_EQUAL("one", parser.GetNextToken());
	ARE_EQUAL("two", parser.GetNextToken());
}

//===========================================================================//

TEST(TokenParserHasMore)
{
	IS_TRUE(!TokenParser(" , \n ;").HasMore());
	IS_TRUE(TokenParser("  ,word;").HasMore());
}

//===========================================================================//

TEST(TokenParserGetNextToken)
{
	TokenParser parser("one, two, three, four");

	ARE_EQUAL("one", parser.GetNextToken());
	ARE_EQUAL("two", parser.GetNextToken());
	ARE_EQUAL("three", parser.GetNextToken());
	ARE_EQUAL("four", parser.GetNextToken());
}

//===========================================================================//

TEST(TokenParserGetNextTokenExceptions)
{
	TokenParser parser("one");
	parser.GetNextToken();
	IS_THROW(parser.GetNextToken(), InvalidStateException);
}

//===========================================================================//

TEST(TokenParserGetToken)
{
	TokenParser parser("  word, 𠀀\nw; 42 й", TokenParser::DefaultDelimiters);

	ARE_EQUAL(5, parser.GetTokensCount());
	ARE_EQUAL("word", parser.GetToken(0));
	ARE_EQUAL("𠀀", parser.GetToken(1));
	ARE_EQUAL("w", parser.GetToken(2));
	ARE_EQUAL("42", parser.GetToken(3));
	ARE_EQUAL("й", parser.GetToken(4));
}

//===========================================================================//

TEST(TokenParserGetTokenOneWord)
{
	TokenParser parser("word", TokenParser::DefaultDelimiters);

	ARE_EQUAL(1, parser.GetTokensCount());
	ARE_EQUAL("word", parser.GetToken(0));
}

//===========================================================================//

TEST(TokenParserGetTokenOneSpacedWord)
{
	TokenParser parser(" word ", TokenParser::DefaultDelimiters);

	ARE_EQUAL(1, parser.GetTokensCount());
	ARE_EQUAL("word", parser.GetToken(0));
}

//===========================================================================//

TEST(TokenParserGetTokenExceptions)
{
	TokenParser parser("one, two;   three\nfour", TokenParser::DefaultDelimiters);

	IS_THROW(parser.GetToken(-1), IndexOutOfBoundsException);
	IS_THROW(parser.GetToken(4), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(TokenParserGetTokenInfo)
{
	TokenParser parser("  word, 𠀀\nw; 42 й", TokenParser::DefaultDelimiters);

	ARE_EQUAL(5, parser.GetTokensCount());
	ARE_EQUAL("word", parser.GetTokenInfo(0)->Token);
	ARE_EQUAL(2, parser.GetTokenInfo(0)->TokenStart);
	ARE_EQUAL(5, parser.GetTokenInfo(0)->TokenEnd);
	
	ARE_EQUAL("𠀀", parser.GetTokenInfo(1)->Token);
	ARE_EQUAL(8, parser.GetTokenInfo(1)->TokenStart);
	ARE_EQUAL(8, parser.GetTokenInfo(1)->TokenEnd);
	
	ARE_EQUAL("w", parser.GetTokenInfo(2)->Token);
	ARE_EQUAL(10, parser.GetTokenInfo(2)->TokenStart);
	ARE_EQUAL(10, parser.GetTokenInfo(2)->TokenEnd);
	
	ARE_EQUAL("42", parser.GetTokenInfo(3)->Token);
	ARE_EQUAL(13, parser.GetTokenInfo(3)->TokenStart);
	ARE_EQUAL(14, parser.GetTokenInfo(3)->TokenEnd);
	
	ARE_EQUAL("й", parser.GetTokenInfo(4)->Token);
	ARE_EQUAL(16, parser.GetTokenInfo(4)->TokenStart);
	ARE_EQUAL(16, parser.GetTokenInfo(4)->TokenEnd);	
}

//===========================================================================//

TEST(TokenParserGetTokenAt)
{
	TokenParser parser(" one, two;   three\nfour", TokenParser::DefaultDelimiters);
	
	IS_NULL(parser.GetTokenAt(0));
	ARE_EQUAL("one", parser.GetTokenAt(1));
	ARE_EQUAL("three", parser.GetTokenAt(13));
	ARE_EQUAL("three", parser.GetTokenAt(15));
	ARE_EQUAL("three", parser.GetTokenAt(17));
	IS_NULL(parser.GetTokenAt(18));
	ARE_EQUAL("four", parser.GetTokenAt(22));
}

//===========================================================================//

TEST(TokenParserGetTokenAtExceptions)
{
	TokenParser parser("one, two;   three\nfour", TokenParser::DefaultDelimiters);
	
	IS_THROW(parser.GetTokenAt(-1), IndexOutOfBoundsException);
	IS_THROW(parser.GetTokenAt(23), IndexOutOfBoundsException);
}

//===========================================================================//

TEST(TokenParserGetTokenIndex)
{
	TokenParser parser(" one, two;   three\nfour", TokenParser::DefaultDelimiters);
	
	ARE_EQUAL(-1, parser.GetTokenIndexAt(-1));
	ARE_EQUAL(0, parser.GetTokenIndexAt(1));
	ARE_EQUAL(1, parser.GetTokenIndexAt(6));
	ARE_EQUAL(2, parser.GetTokenIndexAt(13));
	ARE_EQUAL(2, parser.GetTokenIndexAt(15));
	ARE_EQUAL(2, parser.GetTokenIndexAt(17));
	ARE_EQUAL(3, parser.GetTokenIndexAt(22));
}

//===========================================================================//

} // namespace Bru

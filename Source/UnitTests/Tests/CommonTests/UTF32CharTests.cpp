//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF32Char
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringsTestData.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

TEST(UTF32CharDefaultConstructor)
{
	utf32_char someChar;

	ARE_EQUAL(static_cast<ulong>(NullTerminator), someChar._char);
}

//===========================================================================//

TEST(UTF32CharConstructorFromChar)
{
	utf32_char someChar = UTF32TestChar;

	ARE_EQUAL(UTF32TestChar, someChar._char);
}

//===========================================================================//

TEST(UTF32CharConstructorFromOtherUTF32Char)
{
	utf32_char sourceChar = UTF32TestChar;
	utf32_char someChar(sourceChar);

	ARE_EQUAL(UTF32TestChar, someChar._char);
}

//===========================================================================//

TEST(UTF32CharOperatorAssignChar)
{
	utf32_char someChar;
	someChar = UTF32TestChar;

	ARE_EQUAL(UTF32TestChar, someChar._char);
}

//===========================================================================//

TEST(UTF32CharOperatorOtherUTF32Char)
{
	utf32_char sourceChar = UTF32TestChar;
	utf32_char someChar(sourceChar);

	someChar = sourceChar;

	ARE_EQUAL(UTF32TestChar, someChar._char);
}

//===========================================================================//

TEST(UTF32CharOperatorEqualsToChars)
{
	utf32_char someChar = UTF32TestChar;

	IS_TRUE(someChar == UTF32TestChar);
	IS_TRUE(someChar != UTF32TestGreaterChar);
}

//===========================================================================//

TEST(UTF32CharOperatorEqualsToUTF32Char)
{
	utf32_char someChar = UTF32TestChar;
	utf32_char equalsChar = UTF32TestChar;
	utf32_char notEqualsChar = UTF32TestGreaterChar;

	IS_TRUE(someChar == equalsChar);
	IS_TRUE(someChar != notEqualsChar);
}

//===========================================================================//

TEST(UTF32CharOperatorLess)
{
	IS_TRUE(utf32_char(UTF32TestChar) < UTF32TestGreaterChar);
	IS_FALSE((utf32_char(UTF32TestChar) > UTF32TestGreaterChar));
	IS_FALSE((utf32_char(UTF32TestChar) == UTF32TestGreaterChar));

	IS_TRUE(utf32_char(UTF32TestChar) < utf32_char(UTF32TestGreaterChar));
	IS_FALSE((utf32_char(UTF32TestChar) > utf32_char(UTF32TestGreaterChar)));
	IS_FALSE((utf32_char(UTF32TestChar) == utf32_char(UTF32TestGreaterChar)));
}

//===========================================================================//

TEST(UTF32CharOperatorGreater)
{
	IS_TRUE(utf32_char(UTF32TestGreaterChar) > UTF32TestChar);
	IS_FALSE((utf32_char(UTF32TestGreaterChar) < UTF32TestChar));
	IS_FALSE((utf32_char(UTF32TestGreaterChar) == UTF32TestChar));

	IS_TRUE(utf32_char(UTF32TestGreaterChar) > utf32_char(UTF32TestChar));
	IS_FALSE((utf32_char(UTF32TestGreaterChar) < utf32_char(UTF32TestChar)));
	IS_FALSE((utf32_char(UTF32TestGreaterChar) == utf32_char(UTF32TestChar)));
}

//===========================================================================//

TEST(UTF32CharToUnsignedLong)
{
	utf32_char someChar = UTF32TestChar;

	ARE_EQUAL(UTF32TestChar, static_cast<ulong>(someChar));
}

//===========================================================================//

}// namespace Bru

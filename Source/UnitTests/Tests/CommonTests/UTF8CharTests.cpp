//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты UTF8Char
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/StringsTestData.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

TEST(UTF8CharDefaultConstructor)
{
	utf8_char someChar;

	ARE_EQUAL(0, someChar._lengthInBytes);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[0]);
}

//===========================================================================//

TEST(UTF8CharConstructorFromOneByteChar)
{
	utf8_char someChar = "B";

	ARE_EQUAL(1, someChar._lengthInBytes);
	ARE_EQUAL(66, someChar._dataPtr[0]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[1]);
}

//===========================================================================//

TEST(UTF8CharConstructorFromTwoBytesChar)
{
	utf8_char someChar = "¥";

	ARE_EQUAL(2, someChar._lengthInBytes);
	ARE_EQUAL(-62, someChar._dataPtr[0]);
	ARE_EQUAL(-91, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF8CharConstructorFromThreeBytesChar)
{
	utf8_char someChar = "ন";

	ARE_EQUAL(3, someChar._lengthInBytes);
	ARE_EQUAL(-32, someChar._dataPtr[0]);
	ARE_EQUAL(-90, someChar._dataPtr[1]);
	ARE_EQUAL(-88, someChar._dataPtr[2]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[3]);
}

//===========================================================================//

TEST(UTF8CharConstructorFromFourBytesChar)
{
	utf8_char someChar = "𤭢";

	ARE_EQUAL(4, someChar._lengthInBytes);
	ARE_EQUAL(-16, someChar._dataPtr[0]);
	ARE_EQUAL(-92, someChar._dataPtr[1]);
	ARE_EQUAL(-83, someChar._dataPtr[2]);
	ARE_EQUAL(-94, someChar._dataPtr[3]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[4]);
}

//===========================================================================//

TEST(UTF8CharConstructorFromOtherUTF8Char)
{
	utf8_char sourceChar = "¥";
	utf8_char someChar(sourceChar);

	ARE_EQUAL(2, someChar._lengthInBytes);
	ARE_EQUAL(-62, someChar._dataPtr[0]);
	ARE_EQUAL(-91, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF8CharInternalConstructor)
{
	const char * chars = "Bru¥";
	utf8_char someChar(chars + 3, 2);

	ARE_EQUAL(2, someChar._lengthInBytes);
	ARE_EQUAL(-62, someChar._dataPtr[0]);
	ARE_EQUAL(-91, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF8CharOperatorAssignOneByteChar)
{
	utf8_char someChar;
	someChar = "B";

	ARE_EQUAL(1, someChar._lengthInBytes);
	ARE_EQUAL(66, someChar._dataPtr[0]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[1]);
}

//===========================================================================//

TEST(UTF8CharOperatorAssignTwoBytesChar)
{
	utf8_char someChar;
	someChar = "¥";

	ARE_EQUAL(2, someChar._lengthInBytes);
	ARE_EQUAL(-62, someChar._dataPtr[0]);
	ARE_EQUAL(-91, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF8CharOperatorAssignThreeBytesChar)
{
	utf8_char someChar;
	someChar = "ন";

	ARE_EQUAL(3, someChar._lengthInBytes);
	ARE_EQUAL(-32, someChar._dataPtr[0]);
	ARE_EQUAL(-90, someChar._dataPtr[1]);
	ARE_EQUAL(-88, someChar._dataPtr[2]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[3]);
}

//===========================================================================//

TEST(UTF8CharOperatorAssignFourBytesChar)
{
	utf8_char someChar;
	someChar = "𤭢";

	ARE_EQUAL(4, someChar._lengthInBytes);
	ARE_EQUAL(-16, someChar._dataPtr[0]);
	ARE_EQUAL(-92, someChar._dataPtr[1]);
	ARE_EQUAL(-83, someChar._dataPtr[2]);
	ARE_EQUAL(-94, someChar._dataPtr[3]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[4]);
}

//===========================================================================//

TEST(UTF8CharOperatorAssignFromOtherUTF8Char)
{
	utf8_char sourceChar = "¥";
	utf8_char someChar;
	someChar = sourceChar;

	ARE_EQUAL(2, someChar._lengthInBytes);
	ARE_EQUAL(-62, someChar._dataPtr[0]);
	ARE_EQUAL(-91, someChar._dataPtr[1]);
	ARE_EQUAL(NullTerminator, someChar._dataPtr[2]);
}

//===========================================================================//

TEST(UTF8CharOperatorEqualsToChars)
{
	utf8_char someChar = "¥";

	IS_TRUE(someChar == "¥");
	//Не совпадает длина по байтам
	IS_TRUE(someChar != "1");
	//Не совпадают по значению
	IS_TRUE(someChar != "Ω");
}

//===========================================================================//

TEST(UTF8CharOperatorEqualsToUTF8Char)
{
	utf8_char someChar = "¥";
	utf8_char equalsChar = "¥";
	utf8_char notEqualsByBytesChar = "1";
	utf8_char notEqualsChar = "Ω";

	IS_TRUE(someChar == equalsChar);
	IS_TRUE(someChar != notEqualsByBytesChar);
	IS_TRUE(someChar != notEqualsChar);
}

//===========================================================================//

TEST(UTF8CharOperatorLess)
{
	IS_TRUE(utf8_char("B") < "b");
	IS_FALSE((utf8_char("B") > "b"));
	IS_FALSE((utf8_char("B") == "b"));

	IS_TRUE(utf8_char("B") < "¥");
	IS_FALSE((utf8_char("B") > "¥"));
	IS_FALSE((utf8_char("B") == "¥"));

	IS_TRUE(utf8_char("B") < utf8_char("b"));
	IS_FALSE((utf8_char("B") > utf8_char("b")));
	IS_FALSE((utf8_char("B") == utf8_char("b")));

	IS_TRUE(utf8_char("B") < utf8_char("¥"));
	IS_FALSE((utf8_char("B") > utf8_char("¥")));
	IS_FALSE((utf8_char("B") == utf8_char("¥")));
}

//===========================================================================//

TEST(UTF8CharOperatorGreater)
{
	IS_TRUE(utf8_char("b") > "B");
	IS_FALSE((utf8_char("b") < "B"));
	IS_FALSE((utf8_char("b") == "B"));

	IS_TRUE(utf8_char("¥") > "B");
	IS_FALSE((utf8_char("¥") < "B"));
	IS_FALSE((utf8_char("¥") == "B"));

	IS_TRUE(utf8_char("b") > utf8_char("B"));
	IS_FALSE((utf8_char("b") < utf8_char("B")));
	IS_FALSE((utf8_char("b") == utf8_char("B")));

	IS_TRUE(utf8_char("¥") > utf8_char("B"));
	IS_FALSE((utf8_char("¥") < utf8_char("B")));
	IS_FALSE((utf8_char("¥") == utf8_char("B")));
}

//===========================================================================//

TEST(UTF8CharToChars)
{
	utf8_char someChar = "¥";

	ARE_EQUAL("¥", someChar.ToChars());
}

//===========================================================================//

TEST(UTF8IsDigit)
{
	utf8_char zeroChar = "0";
	IS_TRUE(zeroChar.IsDigit());

	utf8_char oneChar = "1";
	IS_TRUE(oneChar.IsDigit());

	utf8_char twoChar = "2";
	IS_TRUE(twoChar.IsDigit());

	utf8_char threeChar = "3";
	IS_TRUE(threeChar.IsDigit());

	utf8_char fourChar = "4";
	IS_TRUE(fourChar.IsDigit());

	utf8_char fiveChar = "5";
	IS_TRUE(fiveChar.IsDigit());

	utf8_char sixChar = "6";
	IS_TRUE(sixChar.IsDigit());

	utf8_char sevenChar = "7";
	IS_TRUE(sevenChar.IsDigit());

	utf8_char eightChar = "8";
	IS_TRUE(eightChar.IsDigit());

	utf8_char nineChar = "9";
	IS_TRUE(nineChar.IsDigit());

	utf8_char percentChar = "%";
	ARE_EQUAL(false, percentChar.IsDigit());

	utf8_char someChar = "Z";
	ARE_EQUAL(false, someChar.IsDigit());
}

//===========================================================================//

}// namespace Bru

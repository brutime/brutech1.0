//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты конвертера типов
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

TEST(ConverterToBool)
{
	IS_TRUE(Converter::ToBool("1"));
	IS_TRUE(Converter::ToBool("22"));
	IS_TRUE(Converter::ToBool("true"));
	IS_FALSE(Converter::ToBool("tru!"));
	IS_FALSE(Converter::ToBool("0"));
	IS_FALSE(Converter::ToBool("false"));
}

//===========================================================================//

TEST(ConverterToChar)
{
	ARE_EQUAL(NullTerminator, Converter::ToChar(""));
	ARE_EQUAL('B', Converter::ToChar("B"));
}

//===========================================================================//

TEST(ConverterToCharExceptions)
{
	IS_THROW(Converter::ToChar("11"), InvalidArgumentException);
	IS_THROW(Converter::ToChar("¥"), InvalidArgumentException);
}

//===========================================================================//

TEST(ConverterToByte)
{
	ARE_EQUAL(byte(0), Converter::ToByte("0"));
	ARE_EQUAL(byte(128), Converter::ToByte("128"));
	ARE_EQUAL(byte(255), Converter::ToByte("255"));
}

//===========================================================================//

TEST(ConverterToByteExceptions)
{
	IS_THROW(Converter::ToByte("-1"), InvalidArgumentException);
	IS_THROW(Converter::ToByte("256"), InvalidArgumentException);
}

//===========================================================================//

TEST(ConverterToUnsignedShort)
{
	ARE_EQUAL(0u, Converter::ToUnsignedShort("0"));
	ARE_EQUAL(255u, Converter::ToUnsignedShort("255"));
	ARE_EQUAL(32000u, Converter::ToUnsignedShort("32000"));
}

//===========================================================================//

TEST(ConverterToUnsignedShortExceptions)
{
	IS_THROW(Converter::ToUnsignedShort("-1"), InvalidArgumentException);
}

//===========================================================================//

TEST(ConverterToUnsignedInteger)
{
	ARE_EQUAL(0u, Converter::ToUnsignedInteger("0"));
	ARE_EQUAL(255u, Converter::ToUnsignedInteger("255"));
	ARE_EQUAL(32000u, Converter::ToUnsignedInteger("32000"));
}

//===========================================================================//

TEST(ConverterToUnsignedIntegerExceptions)
{
	IS_THROW(Converter::ToUnsignedInteger("-1"), InvalidArgumentException);
}

//===========================================================================//

TEST(ConverterToUnsignedLong)
{
	ARE_EQUAL(static_cast<ulong>(0), Converter::ToUnsignedLong("0"));
	ARE_EQUAL(static_cast<ulong>(255), Converter::ToUnsignedLong("255"));
	ARE_EQUAL(static_cast<ulong>(6000000), Converter::ToUnsignedLong("6000000"));
}

//===========================================================================//

TEST(ConverterToUnsignedLongExceptions)
{
	IS_THROW(Converter::ToUnsignedLong("-1"), InvalidArgumentException);
}

//===========================================================================//

TEST(ConverterToInteger)
{
	ARE_EQUAL(-6000000, Converter::ToInteger("-6000000"));
	ARE_EQUAL(-255, Converter::ToInteger("-255"));
	ARE_EQUAL(0, Converter::ToInteger("0"));
	ARE_EQUAL(255, Converter::ToInteger("255"));
	ARE_EQUAL(6000000, Converter::ToInteger("6000000"));
}

//===========================================================================//

TEST(ConverterToInteger64)
{
	ARE_EQUAL(int64(-6000000000), Converter::ToInteger64("-6000000000"));
	ARE_EQUAL(int64(-255), Converter::ToInteger64("-255"));
	ARE_EQUAL(int64(0), Converter::ToInteger64("0"));
	ARE_EQUAL(int64(255), Converter::ToInteger64("255"));
	ARE_EQUAL(int64(6000000000), Converter::ToInteger64("6000000000"));
}

//===========================================================================//

TEST(ConverterToFloat)
{
	ARE_CLOSE(-600000000.1f, Converter::ToFloat("-600000000.1"), 0.0001f);
	ARE_CLOSE(-255.654321, Converter::ToFloat("-255.654321"), 0.0001f);
	ARE_CLOSE(0.0f, Converter::ToFloat("0.0f"), 0.0001f);
	ARE_CLOSE(255.654321, Converter::ToFloat("255.654321"), 0.0001f);
	ARE_CLOSE(600000000.1f, Converter::ToFloat("600000000.1"), 0.0001f);
}

//===========================================================================//

TEST(ConverterBoolToString)
{
	ARE_EQUAL("true", Converter::ToString(true));
	ARE_EQUAL("false", Converter::ToString(false));
}

//===========================================================================//

TEST(ConverterCharToString)
{
	ARE_EQUAL("B", Converter::ToString('B'));
	ARE_EQUAL("", Converter::ToString(NullTerminator));
}

//===========================================================================//

TEST(ConverterByteToString)
{
	ARE_EQUAL("0", Converter::ToString(byte(0)));
	ARE_EQUAL("128", Converter::ToString(byte(128)));
	ARE_EQUAL("255", Converter::ToString(byte(255)));
}

//===========================================================================//

TEST(ConverterUnsignedShortToString)
{
	ARE_EQUAL("0", Converter::ToString(ushort(0)));
	ARE_EQUAL("255", Converter::ToString(ushort(255)));
	ARE_EQUAL("32000", Converter::ToString(ushort(32000)));
}

//===========================================================================//

TEST(ConverterUnsignedIntegerToString)
{
	ARE_EQUAL("0", Converter::ToString(0u));
	ARE_EQUAL("255", Converter::ToString(255u));
	ARE_EQUAL("32000", Converter::ToString(32000u));
}

//===========================================================================//

TEST(ConverterUnsignedLongToString)
{
	ARE_EQUAL("0", Converter::ToString(ulong(0)));
	ARE_EQUAL("255", Converter::ToString(ulong(255)));
	ARE_EQUAL("6000000", Converter::ToString(ulong(6000000)));
}

//===========================================================================//

TEST(ConverterIntegerToString)
{
	ARE_EQUAL("-6000000", Converter::ToString(-6000000));
	ARE_EQUAL("-255", Converter::ToString(-255));
	ARE_EQUAL("0", Converter::ToString(0));
	ARE_EQUAL("255", Converter::ToString(255));
	ARE_EQUAL("6000000", Converter::ToString(6000000));
}

//===========================================================================//

TEST(ConverterInteger64ToString)
{
	ARE_EQUAL("-6000000000", Converter::ToString(int64(-6000000000)));
	ARE_EQUAL("-255", Converter::ToString(int64(-255)));
	ARE_EQUAL("0", Converter::ToString(int64(0)));
	ARE_EQUAL("255", Converter::ToString(int64(255)));
	ARE_EQUAL("6000000000", Converter::ToString(int64(6000000000)));
}

//===========================================================================//

TEST(ConverterFloatToString)
{
	ARE_CLOSE(-600000000.1f, Converter::ToFloat(Converter::ToString(-600000000.1f)), 0.0001f);
	ARE_CLOSE(-255.654321, Converter::ToFloat(Converter::ToString(-255.654321f)), 0.0001f);
	ARE_CLOSE(0.0f, Converter::ToFloat(Converter::ToString(0.0f)), 0.0001f);
	ARE_CLOSE(255.654321, Converter::ToFloat(Converter::ToString(255.654321f)), 0.0001f);
	ARE_CLOSE(600000000.1f, Converter::ToFloat(Converter::ToString(600000000.1f)), 0.0001f);
	ARE_CLOSE(256.0f, Converter::ToFloat(Converter::ToString(255.654321f, 0)), 0.0001f);
	ARE_CLOSE(255.7f, Converter::ToFloat(Converter::ToString(255.654321f, 1)), 0.0001f);
	ARE_CLOSE(255.65f, Converter::ToFloat(Converter::ToString(255.654321f, 2)), 0.0001f);
}

//===========================================================================//

TEST(ConverterFloatToStringFormatting)
{
	ARE_CLOSE(666.222222f, Converter::ToFloat(Converter::ToString(666.222222f)), 0.0001f);
	ARE_EQUAL("666", Converter::ToString(666.222222f, 0));
	ARE_EQUAL("666.00", Converter::ToString(666.0f, 2));
}

//===========================================================================//

TEST(ConverterFloatToStringExeptions)
{
	IS_THROW(Converter::ToString(666.666f, -1), InvalidArgumentException);
	IS_THROW(Converter::ToString(666.666f, Converter::MaxFloatConvertingPrecision + 1), InvalidArgumentException);
}

//===========================================================================//

}// namespace Bru

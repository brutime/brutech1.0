//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты PtrFromThisMixIn
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Dummies/DummyCompoundObject.h"
#include "../../Dummies/DummyChildPtrFromThisMixIn.h"

namespace Bru
{

//===========================================================================//

TEST(PtrFromThisMixInConstructor)
{
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr(new DummyBasePtrFromThisMixIn());
	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInCopyConstructor)
{
	SharedPtr<DummyBasePtrFromThisMixIn> sourceSharedPtr(new DummyBasePtrFromThisMixIn());
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr(sourceSharedPtr);
	
	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);	

	sourceSharedPtr = NullPtr;

	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInInheritanceCopyConstructor)
{
	SharedPtr<DummyChildPtrFromThisMixIn> sourceSharedPtr(new DummyChildPtrFromThisMixIn());
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr(sourceSharedPtr);
	
	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);	

	sourceSharedPtr = NullPtr;

	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInCopyOperator)
{
	SharedPtr<DummyBasePtrFromThisMixIn> sourceSharedPtr(new DummyBasePtrFromThisMixIn());
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr;

	sharedPtr = sourceSharedPtr;

	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);	

	sourceSharedPtr = NullPtr;

	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInInheritanceCopyOperator)
{
	SharedPtr<DummyChildPtrFromThisMixIn> sourceSharedPtr(new DummyChildPtrFromThisMixIn());
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr;

	sharedPtr = sourceSharedPtr;

	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);	

	sourceSharedPtr = NullPtr;

	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInGetSharedPtr)
{
	SharedPtr<DummyBasePtrFromThisMixIn> sourceSharedPtr(new DummyBasePtrFromThisMixIn());
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr(sourceSharedPtr->GetSharedPtr());
	
	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);	

	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(2, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInGetInheritanceSharedPtr)
{
	SharedPtr<DummyChildPtrFromThisMixIn> sourceSharedPtr(new DummyChildPtrFromThisMixIn());
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr(sourceSharedPtr->GetSharedPtr());
	
	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);	

	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(2, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInGetCastedSharedPtr)
{
	SharedPtr<DummyBasePtrFromThisMixIn> sourceSharedPtr(new DummyBasePtrFromThisMixIn());
	SharedPtr<DummyChildPtrFromThisMixIn> sharedPtr(static_pointer_cast<DummyChildPtrFromThisMixIn>(sourceSharedPtr->GetSharedPtr()));
	
	IS_TRUE(sharedPtr._objectPtr == sourceSharedPtr._objectPtr);
	IS_TRUE(sharedPtr._refsCounter == sourceSharedPtr._refsCounter);	

	IS_TRUE(sharedPtr->_weakPtrOnThis._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(sharedPtr->_weakPtrOnThis._refsCounter == sharedPtr._refsCounter);
	ARE_EQUAL(2, sharedPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(1, sharedPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInGetWeakPtr)
{
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr(new DummyBasePtrFromThisMixIn());
	WeakPtr<DummyBasePtrFromThisMixIn> weakPtr(sharedPtr->GetWeakPtr());
	
	IS_TRUE(weakPtr._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);	

	IS_TRUE(weakPtr->_weakPtrOnThis._objectPtr == weakPtr._objectPtr);
	IS_TRUE(weakPtr->_weakPtrOnThis._refsCounter == weakPtr._refsCounter);
	ARE_EQUAL(1, weakPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInGetInheritanceWeakPtr)
{
	SharedPtr<DummyChildPtrFromThisMixIn> sharedPtr(new DummyChildPtrFromThisMixIn());
	WeakPtr<DummyBasePtrFromThisMixIn> weakPtr(sharedPtr->GetWeakPtr());
	
	IS_TRUE(weakPtr._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);	

	IS_TRUE(weakPtr->_weakPtrOnThis._objectPtr == weakPtr._objectPtr);
	IS_TRUE(weakPtr->_weakPtrOnThis._refsCounter == weakPtr._refsCounter);
	ARE_EQUAL(1, weakPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

TEST(PtrFromThisMixInGetCastedWeakPtr)
{
	SharedPtr<DummyBasePtrFromThisMixIn> sharedPtr(new DummyBasePtrFromThisMixIn());
	WeakPtr<DummyChildPtrFromThisMixIn> weakPtr(static_pointer_cast<DummyChildPtrFromThisMixIn>(sharedPtr->GetWeakPtr()));
	
	IS_TRUE(weakPtr._objectPtr == sharedPtr._objectPtr);
	IS_TRUE(weakPtr._refsCounter == sharedPtr._refsCounter);	

	IS_TRUE(weakPtr->_weakPtrOnThis._objectPtr == weakPtr._objectPtr);
	IS_TRUE(weakPtr->_weakPtrOnThis._refsCounter == weakPtr._refsCounter);
	ARE_EQUAL(1, weakPtr->_weakPtrOnThis._refsCounter->GetSharedRefsCount());
	ARE_EQUAL(2, weakPtr->_weakPtrOnThis._refsCounter->GetWeakRefsCount());
}

//===========================================================================//

} // namespace Bru

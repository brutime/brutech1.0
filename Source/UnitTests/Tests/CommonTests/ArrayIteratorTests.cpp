//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты итератора массива
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersHelper.h"

namespace Bru
{

//===========================================================================//

TEST(ArrayIteratorConstructor)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();
	ArrayIterator<DummyCompoundObject> it(&array, 0);

	IS_TRUE(it._owner == &array);
	ARE_EQUAL(0, it._cursor);
}

//===========================================================================//

TEST(ArrayIteratorForEmpty)
{
	Array<DummyCompoundObject> array;
	ArrayIterator<DummyCompoundObject> it(&array, 0);

	integer count = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(ArrayIteratorForOneElement)
{
	Array<DummyCompoundObject> array;
	array.Add(d1);
	ArrayIterator<DummyCompoundObject> it(&array, 0);

	integer index = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &array);
		ARE_EQUAL(index, it._cursor);
		ARE_EQUAL(d1, it.GetCurrent());
		index++;
	}

	ARE_EQUAL(1, index);
}

//===========================================================================//

TEST(ArrayIteratorFor)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();
	ArrayIterator<DummyCompoundObject> it(&array, 0);

	integer index = 0;
	for (it.First(); it.HasNext(); it.Next())
	{
		IS_TRUE(it._owner == &array);
		ARE_EQUAL(index, it._cursor);
		ARE_EQUAL(TestDummyes[index], it.GetCurrent());
		index++;
	}

	ARE_EQUAL(TestDummyesCount, index);
}

//===========================================================================//

TEST(ArrayIteratorRangeForEmpty)
{
	Array<DummyCompoundObject> array;

	integer count = 0;
	for (const DummyCompoundObject & object : array)
	{
		object.GetValue();
		count++;
	}

	ARE_EQUAL(0, count);
}

//===========================================================================//

TEST(ArrayIteratorRangeForOneElement)
{
	Array<DummyCompoundObject> array;
	array.Add(d1);

	integer count = 0;
	for (const DummyCompoundObject & object : array)
	{
		ARE_EQUAL(d1, object);
		count++;
	}

	ARE_EQUAL(1, count);
}

//===========================================================================//

TEST(ArrayIteratorRangeFor)
{
	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();

	integer index = 0;
	for (const DummyCompoundObject & object : array)
	{
		ARE_EQUAL(TestDummyes[index], object);
		index++;
	}

	ARE_EQUAL(TestDummyesCount, index);
}

//===========================================================================//

TEST(ArrayIteratorExceptions)
{
	IS_THROW(ArrayIterator<DummyCompoundObject>(NullPtr, 0), InvalidArgumentException);

	Array<DummyCompoundObject> array = ContainersHelper::CreateTestDummyCompoundObjectArray();
	ArrayIterator<DummyCompoundObject> it(&array, 0);

	it._cursor = -1;
	IS_THROW(it.GetCurrent(), IteratorOutOfBoundsException);

	it._cursor = TestDummyesCount;
	IS_THROW(it.GetCurrent(), IteratorOutOfBoundsException);

	for (it.First(); it.HasNext(); it.Next())
	{
	}
	IS_THROW(it.Next(), IteratorOutOfBoundsException);
}

//===========================================================================//

}// namespace Bru

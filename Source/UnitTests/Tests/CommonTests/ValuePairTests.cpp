//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты ValuePair
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../Helpers/ContainersTestData.h"
#include "../../Dummies/DummyCompoundObject.h"

namespace Bru
{

//===========================================================================//

TEST(ValuePairConstructor)
{
	ValuePair<DummyCompoundObject, DummyCompoundObject> valuePair(d1, d2);

	ARE_EQUAL(d1, valuePair.First);
	ARE_EQUAL(d2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairConstructorMoveKey)
{
	ValuePair<integer, DummyCompoundObject> valuePair(1, d2);

	ARE_EQUAL(1, valuePair.First);
	ARE_EQUAL(d2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairConstructorMoveValue)
{
	ValuePair<DummyCompoundObject, integer> valuePair(d1, 2);

	ARE_EQUAL(d1, valuePair.First);
	ARE_EQUAL(2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairConstructorMoveKeyValue)
{
	ValuePair<integer, integer> valuePair(1, 2);

	ARE_EQUAL(1, valuePair.First);
	ARE_EQUAL(2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairCopyConstructor)
{
	ValuePair<DummyCompoundObject, DummyCompoundObject> sourceValuePair(d1, d2);

	ValuePair<DummyCompoundObject, DummyCompoundObject> valuePair(sourceValuePair);

	ARE_EQUAL(d1, valuePair.First);
	ARE_EQUAL(d2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairMoveConstructor)
{
	ValuePair<DummyCompoundObject, DummyCompoundObject> valuePair(
	    std::move(ValuePair<DummyCompoundObject, DummyCompoundObject>(d1, d2)));

	ARE_EQUAL(d1, valuePair.First);
	ARE_EQUAL(d2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairCopyOperator)
{
	ValuePair<DummyCompoundObject, DummyCompoundObject> sourceValuePair(d1, d2);

	ValuePair<DummyCompoundObject, DummyCompoundObject> valuePair(d3, d4);
	valuePair = sourceValuePair;

	ARE_EQUAL(d1, valuePair.First);
	ARE_EQUAL(d2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairCopyOperatorSelf)
{
	ValuePair<DummyCompoundObject, DummyCompoundObject> valuePair(d1, d2);

	valuePair = valuePair;

	ARE_EQUAL(d1, valuePair.First);
	ARE_EQUAL(d2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairMoveOperator)
{
	ValuePair<DummyCompoundObject, DummyCompoundObject> valuePair(d3, d4);
	valuePair = std::move(ValuePair<DummyCompoundObject, DummyCompoundObject>(d1, d2));

	ARE_EQUAL(d1, valuePair.First);
	ARE_EQUAL(d2, valuePair.Second);
}

//===========================================================================//

TEST(ValuePairEquals)
{
	ValuePair<DummyCompoundObject, DummyCompoundObject> valuePair(d1, d2);
	ValuePair<DummyCompoundObject, DummyCompoundObject> equalsValuePair(d1, d2);
	ValuePair<DummyCompoundObject, DummyCompoundObject> notEqualsValuePair(d3, d5);

	IS_TRUE(valuePair == equalsValuePair);
	IS_TRUE(valuePair != notEqualsValuePair);
}

//===========================================================================//

TEST(ValuePairLess)
{
	ValuePair<integer, DummyCompoundObject> valuePair(22, d22);
	ValuePair<integer, DummyCompoundObject> lessValuePair(11, d11);

	IS_TRUE(valuePair > lessValuePair);
}

//===========================================================================//

TEST(ValuePairGreater)
{
	ValuePair<integer, DummyCompoundObject> valuePair(11, d11);
	ValuePair<integer, DummyCompoundObject> greaterValuePair(22, d22);

	IS_TRUE(valuePair < greaterValuePair);
}

//===========================================================================//

}// namespace Bru

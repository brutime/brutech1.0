//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты EntityManager
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

TEST_FIXTURE_SETUP(EntityManager)
{
	DomainManager->Add("Gameplay");

	WeakPtr<ParametersFile> componentsFile = ParametersFileManager->Get(PathHelper::PathToComponents);
	Updater->SetUpdatingGroups(componentsFile->GetArray<string>("UpdatingGroupsNames"));

	Renderer->AddLayerToBack("Gameplay", new RenderableLayer("Gameplay"));

	EntityManager = new EntityManagerSingleton(
	    {
	        //Standard components
	        EntityManagerSingleton::CreateComponentDelegate<PositioningNode>(),
	        EntityManagerSingleton::CreateComponentDelegate<RotationNode>(),
	        EntityManagerSingleton::CreateComponentDelegate<LinearMovementController>(),
	        EntityManagerSingleton::CreateComponentDelegate<MovementToPointController>(),
	        EntityManagerSingleton::CreateComponentDelegate<FloatingMovementToPointController>(),
	        EntityManagerSingleton::CreateComponentDelegate<TrajectoryMovementController>(),
	        EntityManagerSingleton::CreateComponentDelegate<RotationController>(),
	        EntityManagerSingleton::CreateComponentDelegate<RotationToAngleController>(),
	        EntityManagerSingleton::CreateComponentDelegate<Sprite>(),
	        EntityManagerSingleton::CreateComponentDelegate<AnimatedSprite>(),
	        EntityManagerSingleton::CreateComponentDelegate<RenderableText>(),
	        EntityManagerSingleton::CreateComponentDelegate<AnimationController>(),
		}, 
		{"Units", "HUD"}, {}, false);
	EntityManager->AddGroup("Units");
	EntityManager->AddGroup("HUD");
}

//===========================================================================//

TEST(EntityManagerAfterInitialization)
{
	ARE_EQUAL(0, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL(2, EntityManager->_entities.GetCount());
	ARE_EQUAL(0, EntityManager->_entities["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entities["HUD"].GetCount());	
	ARE_EQUAL(2, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["HUD"].GetCount());	
	ARE_EQUAL(0, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesChildrenCache.GetCount());
	
	ARE_EQUAL(0, EntityManager->GetEntitiesCount());
	ARE_EQUAL(0, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(0, EntityManager->GetTotalEntitiesCount());	
}

//===========================================================================//

TEST(EntityManagerCreateAndRemoveEntity)
{
	//Создаем
	auto entity = EntityManager->CreateEntity("NotAnimatedUnit", "Gameplay", "Gameplay", {});
	auto childEntity = entity->GetChildren()[0];

	ARE_EQUAL(2, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_pathsToEntities[entity->GetType()]);
	ARE_EQUAL("Children/Moon", EntityManager->_pathsToEntities[childEntity->GetType()]);

	ARE_EQUAL(2, EntityManager->_entities.GetCount());
	ARE_EQUAL(1, EntityManager->_entities["Units"].GetCount());
	ARE_EQUAL(1, EntityManager->_entities["HUD"].GetCount());	

	IS_TRUE(EntityManager->_entities["Units"][0] == entity);
	IS_TRUE(EntityManager->_entities["HUD"][0] == childEntity);

	ARE_EQUAL(2, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["HUD"].GetCount());	
	
	ARE_EQUAL(2, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_entitiesParametersCache["NotAnimatedUnit"]->Type);
	ARE_EQUAL("Moon", EntityManager->_entitiesParametersCache["Children/Moon"]->Type);

	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache[entity->GetType()].GetCount());

	ARE_EQUAL("Children/Moon", EntityManager->_entitiesChildrenCache[entity->GetType()][0]->PathToEntity);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.X, Math::Accuracy);
	ARE_CLOSE(2.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Y, Math::Accuracy);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Z, Math::Accuracy);
	
	ARE_EQUAL(2, EntityManager->GetEntitiesCount());
	ARE_EQUAL(0, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(2, EntityManager->GetTotalEntitiesCount());

	//Удаляем
	EntityManager->RemoveEntity(entity);

	ARE_EQUAL(2, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_pathsToEntities[entity->GetType()]);
	ARE_EQUAL("Children/Moon", EntityManager->_pathsToEntities[childEntity->GetType()]);

	ARE_EQUAL(2, EntityManager->_entities.GetCount());
	ARE_EQUAL(0, EntityManager->_entities["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entities["HUD"].GetCount());	

	ARE_EQUAL(2, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["Units"].GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["HUD"].GetCount());

	ARE_EQUAL(1, EntityManager->_entitiesPool["Units"]["NotAnimatedUnit"].GetCount());
	IS_TRUE(EntityManager->_entitiesPool["Units"]["NotAnimatedUnit"][0] == entity);

	ARE_EQUAL(1, EntityManager->_entitiesPool["HUD"]["Moon"].GetCount());
	IS_TRUE(EntityManager->_entitiesPool["HUD"]["Moon"][0] == childEntity);	
	
	ARE_EQUAL(2, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_entitiesParametersCache["NotAnimatedUnit"]->Type);
	ARE_EQUAL("Moon", EntityManager->_entitiesParametersCache["Children/Moon"]->Type);	

	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache[entity->GetType()].GetCount());

	ARE_EQUAL("Children/Moon", EntityManager->_entitiesChildrenCache[entity->GetType()][0]->PathToEntity);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.X, Math::Accuracy);
	ARE_CLOSE(2.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Y, Math::Accuracy);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Z, Math::Accuracy);
	
	ARE_EQUAL(0, EntityManager->GetEntitiesCount());
	ARE_EQUAL(2, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(2, EntityManager->GetTotalEntitiesCount());

	//Возвращаем из пулла
	auto resurrectedEntity = EntityManager->CreateEntity("NotAnimatedUnit", "Gameplay", "Gameplay", {});
	auto resurrectedChildEntity = resurrectedEntity->GetChildren()[0];

	ARE_EQUAL(2, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_pathsToEntities[resurrectedEntity->GetType()]);
	ARE_EQUAL("Children/Moon", EntityManager->_pathsToEntities[resurrectedChildEntity->GetType()]);

	ARE_EQUAL(2, EntityManager->_entities.GetCount());
	ARE_EQUAL(1, EntityManager->_entities["Units"].GetCount());
	ARE_EQUAL(1, EntityManager->_entities["HUD"].GetCount());

	IS_TRUE(EntityManager->_entities["Units"][0] == entity);
	IS_TRUE(EntityManager->_entities["Units"][0] == resurrectedEntity);
	IS_TRUE(EntityManager->_entities["HUD"][0] == childEntity);
	IS_TRUE(EntityManager->_entities["HUD"][0] == resurrectedChildEntity);

	ARE_EQUAL(2, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["Units"]["NotAnimatedUnit"].GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["HUD"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["HUD"]["Moon"].GetCount());

	ARE_EQUAL(2, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_entitiesParametersCache["NotAnimatedUnit"]->Type);
	ARE_EQUAL("Moon", EntityManager->_entitiesParametersCache["Children/Moon"]->Type);

	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache[resurrectedEntity->GetType()].GetCount());

	ARE_EQUAL("Children/Moon", EntityManager->_entitiesChildrenCache[resurrectedEntity->GetType()][0]->PathToEntity);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[resurrectedEntity->GetType()][0]->Position.X, Math::Accuracy);
	ARE_CLOSE(2.0, EntityManager->_entitiesChildrenCache[resurrectedEntity->GetType()][0]->Position.Y, Math::Accuracy);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[resurrectedEntity->GetType()][0]->Position.Z, Math::Accuracy);
	
	ARE_EQUAL(2, EntityManager->GetEntitiesCount());
	ARE_EQUAL(0, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(2, EntityManager->GetTotalEntitiesCount());	
}

//===========================================================================//

TEST(EntityManagerAddEntityAndRemoveEntityWithoutPooling)
{
	auto entity = EntityManager->GetGroup("Units")[0];
	auto childEntity = entity->GetChildren()[0];
	
	//Удаляем без помещения в пулл
	EntityManager->RemoveEntityWithoutPooling(entity);
	
	ARE_EQUAL(2, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_pathsToEntities[entity->GetType()]);
	ARE_EQUAL("Children/Moon", EntityManager->_pathsToEntities[childEntity->GetType()]);

	ARE_EQUAL(2, EntityManager->_entities.GetCount());
	ARE_EQUAL(0, EntityManager->_entities["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entities["HUD"].GetCount());

	ARE_EQUAL(2, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["Units"]["NotAnimatedUnit"].GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["HUD"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["HUD"]["Moon"].GetCount());

	ARE_EQUAL(2, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_entitiesParametersCache["NotAnimatedUnit"]->Type);
	ARE_EQUAL("Moon", EntityManager->_entitiesParametersCache["Children/Moon"]->Type);

	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache[entity->GetType()].GetCount());

	ARE_EQUAL("Children/Moon", EntityManager->_entitiesChildrenCache[entity->GetType()][0]->PathToEntity);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.X, Math::Accuracy);
	ARE_CLOSE(2.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Y, Math::Accuracy);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Z, Math::Accuracy);
	
	ARE_EQUAL(0, EntityManager->GetEntitiesCount());
	ARE_EQUAL(0, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(0, EntityManager->GetTotalEntitiesCount());
	
	ARE_EQUAL(1, entity->GetChildren().GetCount());
	IS_TRUE(entity->GetChildren()[0] == childEntity);
	
	//Просто добавляем в EntityManager
	EntityManager->AddEntity(entity, "Gameplay");
	
	ARE_EQUAL(2, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_pathsToEntities[entity->GetType()]);
	ARE_EQUAL("Children/Moon", EntityManager->_pathsToEntities[childEntity->GetType()]);	
	
	ARE_EQUAL(2, EntityManager->_entities.GetCount());
	ARE_EQUAL(1, EntityManager->_entities["Units"].GetCount());
	ARE_EQUAL(1, EntityManager->_entities["HUD"].GetCount());

	IS_TRUE(EntityManager->_entities["Units"][0] == entity);
	IS_TRUE(EntityManager->_entities["HUD"][0] == childEntity);

	ARE_EQUAL(2, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["Units"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["Units"]["NotAnimatedUnit"].GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesPool["HUD"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["HUD"]["Moon"].GetCount());

	ARE_EQUAL(2, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_entitiesParametersCache["NotAnimatedUnit"]->Type);
	ARE_EQUAL("Moon", EntityManager->_entitiesParametersCache["Children/Moon"]->Type);

	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache[entity->GetType()].GetCount());

	ARE_EQUAL("Children/Moon", EntityManager->_entitiesChildrenCache[entity->GetType()][0]->PathToEntity);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.X, Math::Accuracy);
	ARE_CLOSE(2.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Y, Math::Accuracy);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache[entity->GetType()][0]->Position.Z, Math::Accuracy);
	
	ARE_EQUAL(2, EntityManager->GetEntitiesCount());
	ARE_EQUAL(0, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(2, EntityManager->GetTotalEntitiesCount());

	ARE_EQUAL(1, entity->GetChildren().GetCount());
	IS_TRUE(entity->GetChildren()[0] == childEntity);
}

//===========================================================================//

TEST(EntityManagerReserveEntities)
{
	EntityManager->ReserveEntities("Cloud", 10);

	ARE_EQUAL(3, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_pathsToEntities["NotAnimatedUnit"]);
	ARE_EQUAL("Children/Moon", EntityManager->_pathsToEntities["Moon"]);
	ARE_EQUAL("Cloud", EntityManager->_pathsToEntities["Cloud"]);

	ARE_EQUAL(2, EntityManager->_entities.GetCount());
	ARE_EQUAL(1, EntityManager->_entities["Units"].GetCount());
	ARE_EQUAL(1, EntityManager->_entities["HUD"].GetCount());

	ARE_EQUAL(2, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(2, EntityManager->_entitiesPool["Units"].GetCount());

	ARE_EQUAL(0, EntityManager->_entitiesPool["Units"]["NotAnimatedUnit"].GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool["HUD"]["Moon"].GetCount());

	ARE_EQUAL(10, EntityManager->_entitiesPool["Units"]["Cloud"].GetCount());
	for (integer i = 0; i < 10; ++i)
	{
		ARE_EQUAL("Cloud", EntityManager->_entitiesPool["Units"]["Cloud"][i]->GetType());
	}
	
	ARE_EQUAL(3, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL("NotAnimatedUnit", EntityManager->_entitiesParametersCache["NotAnimatedUnit"]->Type);
	ARE_EQUAL("Moon", EntityManager->_entitiesParametersCache["Children/Moon"]->Type);	
	ARE_EQUAL("Cloud", EntityManager->_entitiesParametersCache["Cloud"]->Type);
	
	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache.GetCount());
	ARE_EQUAL(1, EntityManager->_entitiesChildrenCache["NotAnimatedUnit"].GetCount());
	
	ARE_EQUAL("Children/Moon", EntityManager->_entitiesChildrenCache["NotAnimatedUnit"][0]->PathToEntity);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache["NotAnimatedUnit"][0]->Position.X, Math::Accuracy);
	ARE_CLOSE(2.0, EntityManager->_entitiesChildrenCache["NotAnimatedUnit"][0]->Position.Y, Math::Accuracy);
	ARE_CLOSE(0.0, EntityManager->_entitiesChildrenCache["NotAnimatedUnit"][0]->Position.Z, Math::Accuracy);
	
	ARE_EQUAL(2, EntityManager->GetEntitiesCount());
	ARE_EQUAL(10, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(12, EntityManager->GetTotalEntitiesCount());
}

//===========================================================================//

TEST(EntityManagerCreateEntityExceptions)
{
	IS_THROW(EntityManager->CreateEntity("SelfRecursiveUnit", "Gameplay", "Gameplay", {}), EntityRecursiveDeclarationException);
	IS_THROW(EntityManager->CreateEntity("FirstChainRecursiveUnit", "Gameplay", "Gameplay", {}), EntityRecursiveDeclarationException);
	IS_THROW(EntityManager->CreateEntity("UnknownComponents", "Gameplay", "Gameplay", {}), InvalidArgumentException);
}

//===========================================================================//

TEST(EntityManagerClear)
{
	EntityManager->CreateEntity("NotAnimatedUnit", "Gameplay", "Gameplay", {});
	EntityManager->Clear();

	ARE_EQUAL(0, EntityManager->_pathsToEntities.GetCount());
	ARE_EQUAL(0, EntityManager->_entities.GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesPool.GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesParametersCache.GetCount());
	ARE_EQUAL(0, EntityManager->_entitiesChildrenCache.GetCount());
	ARE_EQUAL(0, EntityManager->GetEntitiesCount());
	ARE_EQUAL(0, EntityManager->GetEntitiesInPoolCount());
	ARE_EQUAL(0, EntityManager->GetTotalEntitiesCount());
}

//===========================================================================//

TEST_FIXTURE_TEAR_DOWN(EntityManager)
{
	EntityManager = NullPtr;

	Updater->SetUpdatingGroups({});

	Renderer->RemoveLayer("Gameplay", "Gameplay");
	DomainManager->Remove("Gameplay");
}

//===========================================================================//

} // namespace Bru


//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты иерархии EntityComponents
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../../EntitySystem/EntitySystem.h"
#include "../../../OperatingSystem/OperatingSystem.h"
#include "../../../Positioning/Positioning.h"
#include "../../../Renderer/Renderer.h"
#include "../../../Updater/Updater.h"
#include "../../../Helpers/DomainManager/DomainManager.h"
#include "../../../Helpers/ConsoleView/ConsoleView.h"
#include "../../../Helpers/DebugView/DebugView.h"

namespace Bru
{

//===========================================================================//

TEST_FIXTURE_SETUP(EntityComponents)
{
	TextureManager->Load("Shield");
	TextureManager->Load("Earth");
	ParametersFileManager->Load("AnimatedSprites/Shield");
	ParametersFileManager->Load("Sprites/Earth");
	ParametersFileManager->Load("RenderableTexts/RPG/DungeonName");

	DomainManager->Add("EntitySystemTests");

	Renderer->AddLayerToBack("EntitySystemTests", new RenderableLayer("Layer"));
}

//===========================================================================//

TEST(UpdaterEntityComponents)
{
	SharedPtr<ActionList> actionList = new ActionList("EntitySystemTests", 0);
	IS_TRUE(actionList->IsDerivedFromType(ActionList::TypeID));
	IS_TRUE(actionList->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(actionList->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(actionList->IsDerivedFromType(PositioningNode::TypeID));
}

//===========================================================================//

TEST(PositioningEntityComponents)
{
	SharedPtr<PositioningNode> positioningNode = new PositioningNode("EntitySystemTests");
	IS_TRUE(positioningNode->IsDerivedFromType(PositioningNode::TypeID));
	IS_TRUE(positioningNode->IsDerivedFromType(BasePositioningNode::TypeID));
	IS_TRUE(positioningNode->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(positioningNode->IsDerivedFromType(RotationNode::TypeID));
	IS_FALSE(positioningNode->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<RotationNode> rotationNode = new RotationNode("EntitySystemTests");
	IS_TRUE(rotationNode->IsDerivedFromType(RotationNode::TypeID));
	IS_TRUE(rotationNode->IsDerivedFromType(BasePositioningNode::TypeID));
	IS_TRUE(rotationNode->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(rotationNode->IsDerivedFromType(PositioningNode::TypeID));
	IS_FALSE(rotationNode->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<LinearMovementController> linearMovementController = new LinearMovementController(positioningNode, {}, 0.0f, 0.0f);
	IS_TRUE(linearMovementController->IsDerivedFromType(LinearMovementController::TypeID));
	IS_TRUE(linearMovementController->IsDerivedFromType(BaseMovementController::TypeID));
	IS_TRUE(linearMovementController->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(linearMovementController->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(linearMovementController->IsDerivedFromType(PositioningNode::TypeID));

	SharedPtr<MovementToPointController> movementToPointController = new MovementToPointController(positioningNode, {}, 0.0f, 0.0f);
	IS_TRUE(movementToPointController->IsDerivedFromType(MovementToPointController::TypeID));
	IS_TRUE(movementToPointController->IsDerivedFromType(BaseMovementController::TypeID));
	IS_TRUE(movementToPointController->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(movementToPointController->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(movementToPointController->IsDerivedFromType(PositioningNode::TypeID));

	SharedPtr<MovementToTargetController> movementToTargetController = new MovementToTargetController(positioningNode, positioningNode, 0.0f, 0.0f);
	IS_TRUE(movementToTargetController->IsDerivedFromType(MovementToTargetController::TypeID));
	IS_TRUE(movementToTargetController->IsDerivedFromType(BaseMovementController::TypeID));
	IS_TRUE(movementToTargetController->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(movementToTargetController->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(movementToTargetController->IsDerivedFromType(PositioningNode::TypeID));

	SharedPtr<FloatingMovementToPointController> floatingMovementToPointController = new FloatingMovementToPointController(positioningNode, {}, 0.0f, 0.0f, 0.0f, 0.0f);
	IS_TRUE(floatingMovementToPointController->IsDerivedFromType(FloatingMovementToPointController::TypeID));
	IS_TRUE(floatingMovementToPointController->IsDerivedFromType(MovementToPointController::TypeID));
	IS_TRUE(floatingMovementToPointController->IsDerivedFromType(BaseMovementController::TypeID));
	IS_TRUE(floatingMovementToPointController->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(floatingMovementToPointController->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(floatingMovementToPointController->IsDerivedFromType(PositioningNode::TypeID));

	SharedPtr<TrajectoryMovementController> frajectoryMovementController = new TrajectoryMovementController(positioningNode, 0.0f, 0.0f);
	IS_TRUE(frajectoryMovementController->IsDerivedFromType(TrajectoryMovementController::TypeID));
	IS_TRUE(frajectoryMovementController->IsDerivedFromType(BaseMovementController::TypeID));
	IS_TRUE(frajectoryMovementController->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(frajectoryMovementController->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(frajectoryMovementController->IsDerivedFromType(PositioningNode::TypeID));

	SharedPtr<RotationController> rotationController = new RotationController(positioningNode, 0.0f);
	IS_TRUE(rotationController->IsDerivedFromType(RotationController::TypeID));
	IS_TRUE(rotationController->IsDerivedFromType(BaseRotationController::TypeID));
	IS_TRUE(rotationController->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(rotationController->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(rotationController->IsDerivedFromType(PositioningNode::TypeID));

	SharedPtr<RotationToAngleController> rotationToAngleController = new RotationToAngleController(positioningNode, 0.0f, Angle(0.0f), 0.0f);
	IS_TRUE(rotationToAngleController->IsDerivedFromType(RotationToAngleController::TypeID));
	IS_TRUE(rotationToAngleController->IsDerivedFromType(BaseRotationController::TypeID));
	IS_TRUE(rotationToAngleController->IsDerivedFromType(Updatable::TypeID));
	IS_TRUE(rotationToAngleController->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(rotationToAngleController->IsDerivedFromType(PositioningNode::TypeID));
}

//===========================================================================//

TEST(RenderableEntityComponents)
{
	SharedPtr<PrimitivesContainer> primitivesContainer = new PrimitivesContainer("EntitySystemTests", string("Layer"));
	IS_TRUE(primitivesContainer->IsDerivedFromType(PrimitivesContainer::TypeID));
	IS_TRUE(primitivesContainer->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(primitivesContainer->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<RenderableCircle> renderableCircle = new RenderableCircle("EntitySystemTests", "Layer", {}, 0.0f, false, Colors::Red);
	IS_TRUE(renderableCircle->IsDerivedFromType(RenderableCircle::TypeID));
	IS_TRUE(renderableCircle->IsDerivedFromType(BaseRenderablePrimitive::TypeID));
	IS_TRUE(renderableCircle->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(renderableCircle->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(renderableCircle->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<RenderableLine> renderableLine = new RenderableLine("EntitySystemTests", "Layer", {}, Colors::Red);
	IS_TRUE(renderableLine->IsDerivedFromType(RenderableLine::TypeID));
	IS_TRUE(renderableLine->IsDerivedFromType(BaseRenderablePrimitive::TypeID));
	IS_TRUE(renderableLine->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(renderableLine->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(renderableLine->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<RenderablePoints> renderablePoints = new RenderablePoints("EntitySystemTests", "Layer", {}, Colors::Red);
	IS_TRUE(renderablePoints->IsDerivedFromType(RenderablePoints::TypeID));
	IS_TRUE(renderablePoints->IsDerivedFromType(BaseRenderablePrimitive::TypeID));
	IS_TRUE(renderablePoints->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(renderablePoints->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(renderablePoints->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<RenderablePolygon> renderablePolygon = new RenderablePolygon("EntitySystemTests", "Layer", {}, false, Colors::Red);
	IS_TRUE(renderablePolygon->IsDerivedFromType(RenderablePolygon::TypeID));
	IS_TRUE(renderablePolygon->IsDerivedFromType(BaseRenderablePrimitive::TypeID));
	IS_TRUE(renderablePolygon->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(renderablePolygon->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(renderablePolygon->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<RenderableRectangle> renderableRectangle = new RenderableRectangle("EntitySystemTests", "Layer", {}, {}, false, Colors::Red);
	IS_TRUE(renderableRectangle->IsDerivedFromType(RenderableRectangle::TypeID));
	IS_TRUE(renderableRectangle->IsDerivedFromType(BaseRenderablePrimitive::TypeID));
	IS_TRUE(renderableRectangle->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(renderableRectangle->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(renderableRectangle->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<RenderableText> renderableText = new RenderableText("EntitySystemTests", "Layer", "RPG/DungeonName");
	IS_TRUE(renderableText->IsDerivedFromType(RenderableText::TypeID));
	IS_TRUE(renderableText->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(renderableText->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(renderableText->IsDerivedFromType(Updatable::TypeID));

	SharedPtr<Sprite> sprite = new Sprite("EntitySystemTests", "Layer", "Earth");
	IS_TRUE(sprite->IsDerivedFromType(Sprite::TypeID));
	IS_TRUE(sprite->IsDerivedFromType(BaseSprite::TypeID));
	IS_TRUE(sprite->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(sprite->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(sprite->IsDerivedFromType(Updatable::TypeID));
	IS_FALSE(sprite->IsDerivedFromType(AnimatedSprite::TypeID));

	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("EntitySystemTests", "Layer", "Shield");
	IS_TRUE(animatedSprite->IsDerivedFromType(AnimatedSprite::TypeID));
	IS_TRUE(animatedSprite->IsDerivedFromType(BaseSprite::TypeID));
	IS_TRUE(animatedSprite->IsDerivedFromType(Renderable::TypeID));
	IS_TRUE(animatedSprite->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(animatedSprite->IsDerivedFromType(Updatable::TypeID));
	IS_FALSE(animatedSprite->IsDerivedFromType(Sprite::TypeID));
}

//===========================================================================//

TEST(HelpersEntityComponents)
{
	SharedPtr<ConsoleViewInputReceiver> consoleViewInputReceiver = new ConsoleViewInputReceiver();
	IS_TRUE(consoleViewInputReceiver->IsDerivedFromType(ConsoleViewInputReceiver::TypeID));
	IS_TRUE(consoleViewInputReceiver->IsDerivedFromType(InputReceiver::TypeID));
	IS_TRUE(consoleViewInputReceiver->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(consoleViewInputReceiver->IsDerivedFromType(Updatable::TypeID));
	IS_FALSE(consoleViewInputReceiver->IsDerivedFromType(DebugViewInputReceiver::TypeID));

	SharedPtr<DebugViewInputReceiver> debugViewInputReceiver = new DebugViewInputReceiver();
	IS_TRUE(debugViewInputReceiver->IsDerivedFromType(DebugViewInputReceiver::TypeID));
	IS_TRUE(debugViewInputReceiver->IsDerivedFromType(InputReceiver::TypeID));
	IS_TRUE(debugViewInputReceiver->IsDerivedFromType(EntityComponent::TypeID));
	IS_FALSE(debugViewInputReceiver->IsDerivedFromType(Updatable::TypeID));
	IS_FALSE(debugViewInputReceiver->IsDerivedFromType(ConsoleViewInputReceiver::TypeID));
}

//===========================================================================//

TEST_FIXTURE_TEAR_DOWN(EntityComponents)
{
	Renderer->RemoveLayer("EntitySystemTests", "Layer");

	DomainManager->Remove("EntitySystemTests");

	TextureManager->Remove("Shield");
	TextureManager->Remove("Earth");
	ParametersFileManager->Remove("AnimatedSprites/Shield");
	ParametersFileManager->Remove("Sprites/Earth");
	ParametersFileManager->Remove("RenderableTexts/RPG/DungeonName");
}

//===========================================================================//

} // namespace Bru

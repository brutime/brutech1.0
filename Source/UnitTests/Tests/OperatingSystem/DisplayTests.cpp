//#include "../../UnitTest++Source/UnitTest++.h"
//#include "../../../OperatingSystem/Display.h"
//#include "../../../OperatingSystem/DisplayMode.h"
//#include "../../../OperatingSystem/Window.h"
//#include "../../../OperatingSystem/Thread.h"
//#include <stdio.h>
//
//namespace Bru
//{
////#define DEBUG_WINDOW_TEST
////#define ENABLE_MANUAL_TESTS
//class WinTask: public CTask
//{
//public:
//    Window* win;
//    bool Task()
//    {
//        win = new Window("test window", 200, 80, false);
//        if (win->IsCreated())
//        {
//            MSG msg;
//            while (GetMessage(&msg, NULL, 0, 0))
//            {
//                TranslateMessage(&msg);
//                DispatchMessage(&msg);
//            }
//            return true;
//        }
//        else
//        {
//            printf("Window wasn't created!!!\n");
//            return false;
//        }
//
//        return true;
//    }
//
//    ~WinTask()
//    {
//        win->Close();
//    }
//};
//
///*TEST(TestBasicDisplayMode)
//{
//    CHECK(Display->SetDisplayMode(800, 600, 32, 0));
//    Display->RestoreDisplayMode();
//}*/
//
//#ifdef ENABLE_MANUAL_TESTS
//TEST(TestDisplayModes)
//{
//    FixedArray<DisplayMode> modes = Display->GetDisplayModes();
//    DisplayMode* m;
//    for (integer i = 0; i < modes.GetSize(); i++)
//    {
//        m = &(*modes)[i];
//        printf("%d) %dx%d, %d bits per pixel, %d herz\n", i, m->GetWidthInPixels(), m->GetHeightInPixels(), m->GetBitsPerPixel(), m->GetFrequency());
//        if ((i % 25) == 24)
//        {
//            printf("more modes available. press any key to continue...\n");
//            getchar();
//        }
//    }
//    int index;
//    printf("Choose display mode to switch to: ");
//    scanf("%d", &index);
//    CHECK(Display->SetDisplayMode((*modes)[index].GetIndex()));
//    CThread::Sleep(3000);
//    CHECK(Display->RestoreDisplayMode());
//    CThread::Sleep(1000);
//}
//#endif
//
//TEST(TestCreateWindow)
//{
//    WinTask task;
//    CThread worker(THREAD_TYPE_EVENT_DRIVEN, true);
//    worker.AddTask(&task);
//#ifdef DEBUG_WINDOW_TEST
//    printf("Creating window...\n");
//#endif
//    worker.Resume();
//    CThread::Sleep(500);
//    task.win->Minimize();
//    CThread::Sleep(500);
//    task.win->Normalize();
//    CThread::Sleep(500);
//#ifdef DEBUG_WINDOW_TEST
//    printf("Closing window...\n");
//#endif
//    task.win->Close();
//    CThread::Sleep(1000);
//#ifdef DEBUG_WINDOW_TEST
//    printf("Window should be destroyed already...\n");
//#endif
//}
//
//} // namespace

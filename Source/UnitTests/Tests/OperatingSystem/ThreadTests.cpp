//#include "../../UnitTest++Source/UnitTest++.h"
//#include "../../../OperatingSystem/Thread.h"
//
//using namespace Bru;
//
////#define THREAD_TEST_DEBUG
////#define INCLUDE_TIME_CONSUMING_TESTS
//
//class MyTask1: public CTask
//{
//private:
//    integer result;
//
//public:
//    MyTask1(): result(0){}
//
//    bool Task()
//    {
//        //ThreadId_t id;
//        //Thread(&id);
//        for (integer i = 1;i < 10;i++)
//        {
//            result += i;
//            //printf("%d\n",id);
//        }
//#ifdef THREAD_TEST_DEBUG
//        printf("Task1 completed\n");
//#endif
//        return true;
//    }
//
//    integer GetResult()
//    {
//        return result;
//    }
//};
//
//class MyTask2: public CTask
//{
//private:
//    integer result;
//
//public:
//    bool Task()
//    {
//        integer r = 1;
//        //ThreadId_t id;
//        //Thread(&id);
//        for (integer i = 1;i < 10;i++)
//        {
//            CThread::Sleep(50);
//            r *= i;
//            //printf("%d\n",id);
//        }
//        result = r;
//#ifdef THREAD_TEST_DEBUG
//        printf("Task2 completed\n");
//#endif
//        return true;
//    }
//
//    integer GetResult()
//    {
//        return result;
//    }
//};
//
//class SimpleTask: public CTask
//{
//public:
//    integer cnt;
//
//    bool Task()
//    {
//        cnt++;
//#ifdef THREAD_TEST_DEBUG
//        printf("still alive...\n");
//#endif
//        return true;
//    }
//
//    SimpleTask(): cnt(0){}
//};
//
//TEST (EventSetWorks)
//{
//    CEvent testEvent;
//    testEvent.Set();
//    CHECK (testEvent.Wait());
//}
//
//#ifdef INCLUDE_TIME_CONSUMING_TESTS
//TEST (ThreadExecuteIntervalDrivenTask)
//{
//    SimpleTask task;
//    CThread t1(THREAD_TYPE_INTERVAL_DRIVEN, true);
//    t1.SetIdle(1000);
//    t1.AddTask(&task);
//    t1.Resume();
//    CThread::Sleep(2000);
//    t1.Suspend();
//    CThread::Sleep(2000);
//    t1.Resume();
//    CThread::Sleep(2000);
//    t1.Terminate();
//    CThread::Sleep(1000);
//    CHECK(task.cnt >= 4 && task.cnt <= 6);
//}
//#endif
///**/
//
//TEST (ThreadExecuteTaskAndJoin)
//{
//    integer r1, r2;
//    MyTask1 task1; MyTask2 task2;
//
//    CThread t1(THREAD_TYPE_EVENT_DRIVEN, true);
//    CThread t2(THREAD_TYPE_EVENT_DRIVEN, true);
//    t1.AddTask(&task1), t2.AddTask(&task2);
//    t1.Resume();t2.Resume();
//    t2.Join();t1.Join();
//
//    r1 = task1.GetResult();r2 = task2.GetResult();
//    CHECK(r1 == 45);
//    CHECK(r2 == 362880);
//}
///**/
//#ifdef INCLUDE_TIME_CONSUMING_TESTS
//TEST (ThreadExecuteTaskAndJoin2)
//{
//    integer r1, r2;
//    MyTask1 task1; MyTask2 task2;
//
//    CThread t1(THREAD_TYPE_EVENT_DRIVEN, true);
//    CThread t2(THREAD_TYPE_EVENT_DRIVEN, true);
//    t1.AddTask(&task1), t2.AddTask(&task2);
//    t1.Resume();
//    t2.Resume();
//    t2.Join(1000);t1.Join(1000);
//
//    r1 = task1.GetResult();
//    r2 = task2.GetResult();
//    CHECK(r1 == 45);
//    CHECK(r2 == 362880);
//}
//#endif
//
//#ifdef INCLUDE_TIME_CONSUMING_TESTS
//TEST (ThreadExecuteTaskAndSleep)
//{
//    integer r1, r2;
//    MyTask1 task1; MyTask2 task2;
//
//    CThread t1(THREAD_TYPE_EVENT_DRIVEN, true);
//    CThread t2(THREAD_TYPE_EVENT_DRIVEN, true);
//    t1.AddTask(&task1), t2.AddTask(&task2);
//    t1.Resume();t2.Resume();
//    CThread::Sleep(1000);
//
//    r1 = task1.GetResult(), r2 = task2.GetResult();
//    CHECK(r1 == 45);
//    CHECK(r2 == 362880);
//}
//#endif
///**/

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты анимированного спрайта
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../../Renderer/Renderer.h"
#include "../../../Renderer/Sprites/AnimatedSprite/Animations/PingPongAnimation.h"

namespace Bru
{

//===========================================================================//

TEST_FIXTURE_SETUP(AnimatedSprite)
{
	TextureManager->Load("Shield");
	ParametersFileManager->Load("AnimatedSprites/Shield");
	Renderer->AddDomain("AnimatedSpriteTests");

	Renderer->AddLayerToBack("AnimatedSpriteTests", new RenderableLayer("Layer"));
}

//===========================================================================//

TEST(AnimatedSpriteNormalSingleNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalSingle");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 15; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = i;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite->UpdateCurrentAnimation(oneFrameTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(15, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_TRUE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalSingleFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalSingle");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalHalfSingleNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalHalfSingle");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 6; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = 5 + i % 6;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite->UpdateCurrentAnimation(oneFrameTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(10, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_TRUE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalHalfSingleFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalHalfSingle");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(10, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalRepeatNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Normal");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 60; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = i % 16;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalRepeatFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Normal");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalRepeatBigSteps)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Normal");

	const float twelveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 12.0f;
	animatedSprite->UpdateCurrentAnimation(twelveFramesTime);
	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(12, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	const float twentyOneFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 21.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(twentyOneFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(1, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalRepeatSuperiorStep)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Normal");

	const float sixtyFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 60.0f;
	animatedSprite->UpdateCurrentAnimation(sixtyFramesTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(12, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalHalfRepeatNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalHalf");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 60; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = 5 + i % 6;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalHalfRepeatFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalHalf");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(10, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalHalfRepeatBigSteps)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalHalf");

	const float twelveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 12.0f;
	animatedSprite->UpdateCurrentAnimation(twelveFramesTime);
	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	const float twentyOneFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 21.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(twentyOneFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(8, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteNormalHalfRepeatSuperiorStep)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("NormalHalf");

	const float sixtyFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 60.0f;
	animatedSprite->UpdateCurrentAnimation(sixtyFramesTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseSingleNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseSingle");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 15; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = 15 - i;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite->UpdateCurrentAnimation(oneFrameTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(0, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_TRUE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseSingleFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseSingle");

	const float sixFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 6.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(sixFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(9, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseHalfSingleNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseHalfSingle");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 6; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = 10 - i;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite->UpdateCurrentAnimation(oneFrameTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_TRUE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseHalfSingleFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseHalfSingle");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseRepeatNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Reverse");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 60; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = 15 - i % 16;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseRepeatFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Reverse");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(10, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseRepeatBigSteps)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Reverse");

	const float twelveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 12.0f;
	animatedSprite->UpdateCurrentAnimation(twelveFramesTime);
	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(3, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	const float twentyOneFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 21.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(twentyOneFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(14, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseRepeatSuperiorStep)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("Reverse");

	const float sixtyFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 60.0f;
	animatedSprite->UpdateCurrentAnimation(sixtyFramesTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(3, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseHalfRepeatNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseHalf");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 60; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = 10 - i % 6;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseHalfRepeatFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseHalf");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseHalfRepeatBigSteps)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseHalf");

	const float twelveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 12.0f;
	animatedSprite->UpdateCurrentAnimation(twelveFramesTime);
	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(10, animatedSprite->_currentAnimation->GetCurrentFrameIndex());

	const float twentyOneFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 21.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(twentyOneFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(7, animatedSprite->_currentAnimation->GetCurrentFrameIndex());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpriteReverseHalfRepeatSuperiorStep)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("ReverseHalf");

	const float sixtyFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 60.0f;
	animatedSprite->UpdateCurrentAnimation(sixtyFramesTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(10, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongSingleNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongSingle");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 30; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = i % 30;
		if (expectedFrameIndex >= 16)
		{
			expectedFrameIndex = 30 - expectedFrameIndex;
		}
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite->UpdateCurrentAnimation(oneFrameTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(0, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_TRUE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongSingleFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongSingle");

	const float fiveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 5.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(fiveFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongHalfSingleNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongHalfSingle");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 11; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = i % 10;
		if (expectedFrameIndex >= 6)
		{
			expectedFrameIndex = 10 - expectedFrameIndex;
		}
		expectedFrameIndex += 5;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite->UpdateCurrentAnimation(oneFrameTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_TRUE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongHalfSingleFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongHalfSingle");

	const float sevenFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 7.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(sevenFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(8, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongRepeatNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPong");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 60; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = i % 30;
		if (expectedFrameIndex >= 16)
		{
			expectedFrameIndex = 30 - expectedFrameIndex;
		}
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite->UpdateCurrentAnimation(oneFrameTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(1, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongRepeatFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPong");

	const float seventeenFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 17.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(seventeenFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(13, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongRepeatBigSteps)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPong");

	const float twelveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 12.0f;
	animatedSprite->UpdateCurrentAnimation(twelveFramesTime);
	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(12, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	const float twentyOneFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 21.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(twentyOneFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(3, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongRepeatSuperiorStep)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPong");

	const float seventyFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 70.0f;
	animatedSprite->UpdateCurrentAnimation(seventyFramesTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(10, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongHalfRepeatNormalFlow)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongHalf");

	const float oneFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame();

	for (integer i = 0; i < 60; i++)
	{
		ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
		integer expectedFrameIndex = i % 10;
		if (expectedFrameIndex >= 6)
		{
			expectedFrameIndex = 10 - expectedFrameIndex;
		}
		expectedFrameIndex += 5;
		ARE_EQUAL(expectedFrameIndex, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
		IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

		animatedSprite->UpdateCurrentAnimation(oneFrameTime);
	}

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongHalfRepeatFewFrames)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongHalf");

	const float sevenFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 7.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(sevenFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(8, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongHalfRepeatBigSteps)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongHalf");

	const float twelveFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 12.0f;
	animatedSprite->UpdateCurrentAnimation(twelveFramesTime);
	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(7, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	const float twentyOneFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 21.0f;
	const float halfFrameTime = animatedSprite->_currentAnimation->GetTimePerFrame() / 2.0f;
	animatedSprite->UpdateCurrentAnimation(twentyOneFramesTime + halfFrameTime);

	ARE_CLOSE(halfFrameTime, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(8, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST(AnimatedSpritePingPongHalfRepeatSuperiorStep)
{
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("AnimatedSpriteTests", "Layer", "Shield");

	animatedSprite->SelectAnimation("PingPongHalf");

	const float sixtyFramesTime = animatedSprite->_currentAnimation->GetTimePerFrame() * 60.0f;
	animatedSprite->UpdateCurrentAnimation(sixtyFramesTime);

	ARE_CLOSE(0.0f, animatedSprite->_currentFrameTime, Math::Accuracy);
	ARE_EQUAL(5, animatedSprite->_currentAnimation->GetCurrentFrameIndex());
	IS_FALSE(animatedSprite->_currentAnimation->IsFinished());

	animatedSprite = NullPtr;
}

//===========================================================================//

TEST_FIXTURE_TEAR_DOWN(AnimatedSprite)
{
	Renderer->RemoveLayer("AnimatedSpriteTests", "Layer");
	Renderer->RemoveDomain("AnimatedSpriteTests");

	TextureManager->Remove("Shield");
	ParametersFileManager->Remove("AnimatedSprites/Shield");
}

//===========================================================================//

} // namespace Bru

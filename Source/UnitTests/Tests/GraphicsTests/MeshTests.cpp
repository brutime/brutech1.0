//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты меша
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Graphics/Graphics.h"
#include "../../Helpers/MeshHelper.h"
#include "../../Helpers/RenderingTestData.h"

namespace Bru
{

//===========================================================================//

TEST(MeshDefaultConstructor)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	ARE_EQUAL(0, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());
}

//===========================================================================//

TEST(MeshReserveConstructor)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles, 4, 6);

	ARE_EQUAL(4, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(6, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());
}

//===========================================================================//

TEST(MeshCopyConstructor)
{
	SharedPtr<Mesh> mesh = new Mesh(*MeshHelper::CreateFirstTestMesh());

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestVerticesCount - 1, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == FirstTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == FirstTestVertices[i].VertexColor);
	}
}

//===========================================================================//

TEST(MeshCopyOperator)
{
	SharedPtr<Mesh> sourceMesh = new Mesh(*MeshHelper::CreateFirstTestMesh());

	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);	
	
	*mesh = *sourceMesh;

	sourceMesh->Clear();

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestVerticesCount - 1, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == FirstTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == FirstTestVertices[i].VertexColor);
	}
}

//===========================================================================//

TEST(MeshReserve)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);
	mesh->Resize(4, 6);

	ARE_EQUAL(4, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);

	ARE_EQUAL(6, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());
}

//===========================================================================//

TEST(MeshAdd)
{
	SharedPtr<Mesh> mesh = MeshHelper::CreateFirstTestMesh();
	SharedPtr<Mesh> secondMesh = MeshHelper::CreateSecondTestMesh();

	mesh->Add(*secondMesh);

	ARE_EQUAL(FirstTestVerticesCount + SecondTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount + SecondTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestVerticesCount + SecondTestVerticesCount - 1, mesh->GetMaxIndex());

	integer vertexIndex = 0;
	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[vertexIndex].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[vertexIndex].TextureCoord == FirstTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[vertexIndex].VertexColor == FirstTestVertices[i].VertexColor);
		vertexIndex++;
	}

	for (integer i = 0; i < SecondTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[vertexIndex].Position == SecondTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[vertexIndex].TextureCoord == SecondTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[vertexIndex].VertexColor == SecondTestVertices[i].VertexColor);
		vertexIndex++;
	}

	integer indexIndex = 0;
	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[indexIndex] == FirstTestIndices[i]);
		indexIndex++;
	}

	for (integer i = 0; i < SecondTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[indexIndex] == FirstTestMaxIndex + 1 + SecondTestIndices[i]);
		indexIndex++;
	}
}

//===========================================================================//

TEST(MeshAddVertex)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord, FirstTestVertices[i].VertexColor);
	}

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == FirstTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == FirstTestVertices[i].VertexColor);
	}
}

//===========================================================================//

TEST(MeshSetVertex)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles, 4, 0);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->SetVertex(i, FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord, FirstTestVertices[i].VertexColor);
	}

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == FirstTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == FirstTestVertices[i].VertexColor);
	}
}

//===========================================================================//

TEST(MeshClearVertices)
{
	SharedPtr<Mesh> mesh = MeshHelper::CreateFirstTestMesh();
	mesh->ClearVertices();

	ARE_EQUAL(0, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestMaxIndex, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[i] == FirstTestIndices[i]);
	}
}

//===========================================================================//

TEST(MeshSetVertexPosition)
{
	const Vector3 offsetVector(11.0f, 22.0f, 33.0f);
	Vertex updatedTestVertices[FirstTestVerticesCount];

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		updatedTestVertices[i] = FirstTestVertices[i];
		updatedTestVertices[i].Position += offsetVector * (i + 1);
	}

	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord, FirstTestVertices[i].VertexColor);
	}

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->SetVertexPosition(i, FirstTestVertices[i].Position + offsetVector * (i + 1));
	}

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == updatedTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == updatedTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == updatedTestVertices[i].VertexColor);
	}
}

//===========================================================================//

#ifdef DEBUGGING
TEST(MeshSetVertexPositionExceptions)
{
	SharedPtr<Mesh> emptyMesh = new Mesh(VerticesMode::Triangles);

	IS_THROW(emptyMesh->SetVertexPosition(-1, Vector3(1.0f, 2.0f, 3.0f)), IndexOutOfBoundsException);
	IS_THROW(emptyMesh->SetVertexPosition(0, Vector3(1.0f, 2.0f, 3.0f)), IndexOutOfBoundsException);

	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord);
	}

	IS_THROW(mesh->SetVertexPosition(-1, Vector3(1.0f, 2.0f, 3.0f)), IndexOutOfBoundsException);
	IS_THROW(mesh->SetVertexPosition(FirstTestVerticesCount, Vector3(1.0f, 2.0f, 3.0f)), IndexOutOfBoundsException);
}
#endif

//===========================================================================//

TEST(MeshTranslateVertexPosition)
{
	const Vector3 offsetVector(11.0f, 22.0f, 33.0f);
	Vertex updatedTestVertices[FirstTestVerticesCount];

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		updatedTestVertices[i] = FirstTestVertices[i];
		updatedTestVertices[i].Position += offsetVector;
	}

	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord, FirstTestVertices[i].VertexColor);
	}

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->TranslateVertexPosition(i, offsetVector);
	}

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == updatedTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == updatedTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == updatedTestVertices[i].VertexColor);
	}
}

//===========================================================================//

TEST(MeshSetVertexTextureCoord)
{
	const Vector2 offsetVector(11.0f, 22.0f);
	Vertex updatedTestVertices[FirstTestVerticesCount];

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		updatedTestVertices[i] = FirstTestVertices[i];
		updatedTestVertices[i].TextureCoord *= offsetVector * (i + 1);
	}

	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord, FirstTestVertices[i].VertexColor);
	}

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->SetVertexTextureCoord(i, FirstTestVertices[i].TextureCoord * offsetVector * (i + 1));
	}

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == updatedTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == updatedTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == updatedTestVertices[i].VertexColor);
	}
}

//===========================================================================//

#ifdef DEBUGGING
TEST(MeshSetVertexTextureCoordExceptions)
{
	Mesh emptyMesh(VerticesMode::Triangles);

	IS_THROW(emptyMesh.SetVertexTextureCoord(-1, Vector2(1.0f, 2.0f)), IndexOutOfBoundsException);
	IS_THROW(emptyMesh.SetVertexTextureCoord(0, Vector2(1.0f, 2.0f)), IndexOutOfBoundsException);

	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord, FirstTestVertices[i].VertexColor);
	}

	IS_THROW(mesh->SetVertexTextureCoord(-1, Vector2(1.0f, 2.0f)), IndexOutOfBoundsException);
	IS_THROW(mesh->SetVertexTextureCoord(FirstTestVerticesCount, Vector2(1.0f, 2.0f)), IndexOutOfBoundsException);
}
#endif

//===========================================================================//

TEST(MeshSetVerticesColor)
{
	const Color TestColorAlpha = Colors::Purple;

	SharedPtr<Mesh> mesh = MeshHelper::CreateFirstTestMesh();
	mesh->SetVerticesColor(TestColorAlpha);

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestMaxIndex, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == FirstTestVertices[i].TextureCoord);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == TestColorAlpha);
	}

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[i] == FirstTestIndices[i]);
	}
}

//===========================================================================//

TEST(MeshSetVerticesColorAlpha)
{
	const float TestColorAlpha = 0.5f;

	SharedPtr<Mesh> mesh = MeshHelper::CreateFirstTestMesh();

	mesh->SetVerticesColorAlpha(TestColorAlpha);

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestMaxIndex, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == FirstTestVertices[i].TextureCoord);
		ARE_CLOSE(FirstTestVertices[i].VertexColor.GetRed(), mesh->GetVertices()[i].VertexColor.GetRed(), Math::Accuracy);
		ARE_CLOSE(FirstTestVertices[i].VertexColor.GetGreen(), mesh->GetVertices()[i].VertexColor.GetGreen(), Math::Accuracy);
		ARE_CLOSE(FirstTestVertices[i].VertexColor.GetBlue(), mesh->GetVertices()[i].VertexColor.GetBlue(), Math::Accuracy);
		ARE_CLOSE(TestColorAlpha, mesh->GetVertices()[i].VertexColor.GetAlpha(), Math::Accuracy);
	}

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[i] == FirstTestIndices[i]);
	}
}

//===========================================================================//

TEST(MeshAddIndex)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		mesh->AddIndex(FirstTestIndices[i]);
	}

	ARE_EQUAL(0, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestMaxIndex, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[i] == FirstTestIndices[i]);
	}
}

//===========================================================================//

TEST(MeshSetIndex)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles, 0, 6);

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		mesh->SetIndex(i, FirstTestIndices[i]);
	}

	ARE_EQUAL(0, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestMaxIndex, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[i] == FirstTestIndices[i]);
	}
}

//===========================================================================//

TEST(MeshCopyIndicesFromOtherMesh)
{
	uint updatedTestIndices[FirstTestIndicesCount];

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		updatedTestIndices[i] = i;
	}

	SharedPtr<Mesh> mesh = MeshHelper::CreateFirstTestMesh();

	SharedPtr<Mesh> updatedMesh = MeshHelper::CreateFirstTestMesh();
	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		updatedMesh->SetIndex(i, updatedTestIndices[i]);
	}

	mesh->CopyIndicesFromOtherMesh(*updatedMesh);

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestIndicesCount - 1, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[i] == updatedTestIndices[i]);
	}
}

//===========================================================================//

TEST(MeshEmptyCopyIndicesFromOtherMesh)
{
	SharedPtr<Mesh> sourceMesh = MeshHelper::CreateFirstTestMesh();
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);

	mesh->CopyIndicesFromOtherMesh(*sourceMesh);

	ARE_EQUAL(0, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestMaxIndex, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		IS_TRUE(mesh->GetIndices()[i] == FirstTestIndices[i]);
	}
}

//===========================================================================//

TEST(MeshScalePosition)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);
	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord);
	}

	mesh->ScalePosition(2.0f, 3.0f, 4.0f);

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		ARE_CLOSE(FirstTestVertices[i].Position.X * 2.0f, mesh->GetVertices()[i].Position.X, Math::Accuracy);
		ARE_CLOSE(FirstTestVertices[i].Position.Y * 3.0f, mesh->GetVertices()[i].Position.Y, Math::Accuracy);
		ARE_CLOSE(FirstTestVertices[i].Position.Z * 4.0f, mesh->GetVertices()[i].Position.Z, Math::Accuracy);
	}
}

//===========================================================================//

TEST(MeshGetMinVertexPosition)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);
	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord);
	}

	Vector3 minVerexPosition = mesh->GetMinVertexPosition();
	IS_TRUE(minVerexPosition == FirstTestMinVertexPosition);
}

//===========================================================================//

TEST(MeshGetMaxVertexPosition)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);
	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->AddVertex(FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord);
	}

	Vector3 maxVerexPosition = mesh->GetMaxVertexPosition();
	IS_TRUE(maxVerexPosition == FirstTestMaxVertexPosition);
}

//===========================================================================//

TEST(MeshCopyTextureCoordFromOtherMesh)
{
	const Vector2 offsetVector(11.0f, 22.0f);
	Vector2 updatedTestTextureCoords[FirstTestVerticesCount];

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		updatedTestTextureCoords[i] = FirstTestVertices[i].TextureCoord;
		updatedTestTextureCoords[i] *= offsetVector * (i + 1);
	}

	SharedPtr<Mesh> mesh = MeshHelper::CreateFirstTestMesh();
	SharedPtr<Mesh> updatedMesh = MeshHelper::CreateFirstTestMesh();

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		updatedMesh->SetVertexTextureCoord(i, updatedTestTextureCoords[i]);
	}

	mesh->CopyTextureCoordFromOtherMesh(*updatedMesh);

	ARE_EQUAL(FirstTestVerticesCount, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(FirstTestIndicesCount, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(FirstTestMaxIndex, mesh->GetMaxIndex());

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		IS_TRUE(mesh->GetVertices()[i].Position == FirstTestVertices[i].Position);
		IS_TRUE(mesh->GetVertices()[i].TextureCoord == updatedTestTextureCoords[i]);
		IS_TRUE(mesh->GetVertices()[i].VertexColor == FirstTestVertices[i].VertexColor);
	}
}

//===========================================================================//

TEST(MeshCopyTextureCoordFromOtherMeshExceptions)
{
	SharedPtr<Mesh> firstMesh = MeshHelper::CreateFirstTestMesh();
	SharedPtr<Mesh> secondMesh = MeshHelper::CreateSecondTestMesh();

	IS_THROW(firstMesh->CopyTextureCoordFromOtherMesh(*secondMesh), InvalidArgumentException);
}

//===========================================================================//

TEST(MeshClear)
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles);
	mesh->_vertices.Resize(1000);
	mesh->_indices.Resize(1000);
	mesh->_maxIndex = 666;

	mesh->Clear();

	ARE_EQUAL(0, mesh->GetVertices().GetCount());
	IS_TRUE(mesh->GetVerticesMode() == VerticesMode::Triangles);
	ARE_EQUAL(0, mesh->GetIndices().GetCount());
	ARE_EQUAL(3, mesh->GetVertexPerFace());
	ARE_EQUAL(0, mesh->GetMaxIndex());
}

//===========================================================================//

} // namespace Bru

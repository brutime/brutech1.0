//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты графики
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../OperatingSystem/OperatingSystem.h"
#include "../../../Graphics/Graphics.h"
#include "../../../Graphics/Image/Image.h"
#include "../../../Graphics/VideoCardCapabilities.h"

namespace Bru
{

//===========================================================================//

TEST(ImageExpandDataForPowerOfTwoMonochrome)
{
	const integer Width = 6;
	const integer Height = 6;
	const integer pixelSize = GraphicsHelper::GetBytesPerPixel(PixelFormat::Monochrome);
	const integer dataSize = Width * Height * pixelSize;

	const byte expectedData[64] = { 13, 13, 13, 13, 13, 13, 13, 0, 13, 13, 13, 13, 13, 13, 13, 0, 13, 13, 13, 13, 13,
	                                13, 13, 0, 13, 13, 13, 13, 13, 13, 13, 0, 13, 13, 13, 13, 13, 13, 13, 0, 13, 13, 13, 13, 13, 13, 13, 0, 13, 13,
	                                13, 13, 13, 13, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0
	                              };

	Array<byte>::SimpleType data(dataSize);
	Memory::FillWithValue(data.GetData(), 13, data.GetCount());

	Image image("Test", Width, Height, PixelFormat::Monochrome, data);

	Image::ExpandDataForPowerOfTwo(image);

	integer powerOfTwoWidth = Math::GetNearestPowerOfTwo(Width);
	integer powerOfTwoHeight = Math::GetNearestPowerOfTwo(Height);

	ARE_EQUAL("Test", image.GetName());
	ARE_EQUAL(powerOfTwoWidth, image.GetWidth());
	ARE_EQUAL(powerOfTwoHeight, image.GetHeight());
	IS_TRUE(image.GetPixelFormat() == PixelFormat::Monochrome);
	ARE_ARRAYS_EQUAL(expectedData, image.GetData(), image.GetData().GetCount());
}

//===========================================================================//

TEST(ImageExpandDataForPowerOfTwoMonochromeAlpha)
{
	const integer Width = 6;
	const integer Height = 6;
	const integer pixelSize = GraphicsHelper::GetBytesPerPixel(PixelFormat::MonochromeAlpha);
	const integer dataSize = Width * Height * pixelSize;

	//По 2 байна на пиксель
	const byte expectedData[128] = { 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0,
	                                 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	                               };

	Array<byte>::SimpleType data(dataSize);
	Memory::FillWithValue(data.GetData(), 13, data.GetCount());

	Image image("Test", Width, Height, PixelFormat::MonochromeAlpha, data);

	Image::ExpandDataForPowerOfTwo(image);

	integer powerOfTwoWidth = Math::GetNearestPowerOfTwo(Width);
	integer powerOfTwoHeight = Math::GetNearestPowerOfTwo(Height);

	ARE_EQUAL("Test", image.GetName());
	ARE_EQUAL(powerOfTwoWidth, image.GetWidth());
	ARE_EQUAL(powerOfTwoHeight, image.GetHeight());
	IS_TRUE(image.GetPixelFormat() == PixelFormat::MonochromeAlpha);
	ARE_ARRAYS_EQUAL(expectedData, image.GetData(), image.GetData().GetCount());
}

//===========================================================================//

TEST(ImageExpandDataForPowerOfTwoRGB)
{
	const integer Width = 6;
	const integer Height = 6;
	const integer pixelSize = GraphicsHelper::GetBytesPerPixel(PixelFormat::RGB);
	const integer dataSize = Width * Height * pixelSize;

	//По 3 байна на пиксель
	const byte expectedData[192] = { 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0,
	                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	                               };

	Array<byte>::SimpleType data(dataSize);
	Memory::FillWithValue(data.GetData(), 13, data.GetCount());

	Image image("Test", Width, Height, PixelFormat::RGB, data);

	Image::ExpandDataForPowerOfTwo(image);

	integer powerOfTwoWidth = Math::GetNearestPowerOfTwo(Width);
	integer powerOfTwoHeight = Math::GetNearestPowerOfTwo(Height);

	ARE_EQUAL("Test", image.GetName());
	ARE_EQUAL(powerOfTwoWidth, image.GetWidth());
	ARE_EQUAL(powerOfTwoHeight, image.GetHeight());
	IS_TRUE(image.GetPixelFormat() == PixelFormat::RGB);
	ARE_ARRAYS_EQUAL(expectedData, image.GetData(), image.GetData().GetCount());
}

//===========================================================================//

TEST(ImageExpandDataForPowerOfTwoRGBA)
{
	const integer Width = 6;
	const integer Height = 6;
	const integer pixelSize = GraphicsHelper::GetBytesPerPixel(PixelFormat::RGBA);
	const integer dataSize = Width * Height * pixelSize;

	//По 3 байна на пиксель
	const byte expectedData[256] = { 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 0, 13, 13, 13, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 0, 13, 13, 13, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 0, 13, 13, 13,
	                                 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 0, 0, 0, 0,
	                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	                               };

	Array<byte>::SimpleType data(dataSize);
	Memory::FillWithValue(data.GetData(), 13, data.GetCount());

	Image image("Test", Width, Height, PixelFormat::RGBA, data);

	Image::ExpandDataForPowerOfTwo(image);

	integer powerOfTwoWidth = Math::GetNearestPowerOfTwo(Width);
	integer powerOfTwoHeight = Math::GetNearestPowerOfTwo(Height);

	ARE_EQUAL("Test", image.GetName());
	ARE_EQUAL(powerOfTwoWidth, image.GetWidth());
	ARE_EQUAL(powerOfTwoHeight, image.GetHeight());
	IS_TRUE(image.GetPixelFormat() == PixelFormat::RGBA);
	ARE_ARRAYS_EQUAL(expectedData, image.GetData(), image.GetData().GetCount());
}

//===========================================================================//

}// namespace Bru

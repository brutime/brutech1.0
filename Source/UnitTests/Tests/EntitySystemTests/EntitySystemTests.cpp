//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тесты иерархии EntityComponents
//

#include "../../../../Libs/Brunit/Brunit.h"
#include "../../../Common/Common.h"
#include "../../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../../EntitySystem/EntitySystem.h"
#include "../../../Positioning/Positioning.h"
#include "../../../Renderer/Renderer.h"
#include "../../../Helpers/DomainManager/DomainManager.h"

namespace Bru
{

//===========================================================================//

TEST_FIXTURE_SETUP(EntitySystem)
{
	TextureManager->Load("Shield");
	ParametersFileManager->Load("AnimatedSprites/Shield");	
	DomainManager->Add("EntitySystemTests");

	Renderer->AddLayerToBack("EntitySystemTests", new RenderableLayer("Layer"));
}

//===========================================================================//

TEST(EntityGetComponent)
{
	SharedPtr<Entity> entity = new Entity("EntityForTest", "Testing");
	
	SharedPtr<PositioningNode> node = new PositioningNode("EntitySystemTests");
	entity->AddComponent(node);
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("EntitySystemTests", "Layer", "Shield");
	entity->AddComponent(animatedSprite);
	
	IS_TRUE(entity->GetComponent<PositioningNode>() == node);
	IS_TRUE(entity->GetComponent<BasePositioningNode>() == node);
	
	IS_TRUE(entity->GetComponent<AnimatedSprite>() == animatedSprite);	
	IS_TRUE(entity->GetComponent<BaseSprite>() == animatedSprite);
	IS_TRUE(entity->GetComponent<Renderable>() == animatedSprite);
	
	IS_TRUE(entity->GetComponent<PositioningNode>(BasePositioningNode::TypeID) == node);
	IS_TRUE(entity->GetComponent<AnimatedSprite>(AnimatedSprite::TypeID) == animatedSprite);
	
	IS_TRUE(entity->GetComponent<Sprite>() == NullPtr);
	IS_TRUE(entity->GetComponent<MovementToPointController>() == NullPtr);
	IS_TRUE(entity->GetComponent<RotationNode>() == NullPtr);
	
	IS_TRUE(entity->GetComponent<Renderable>(Sprite::TypeID) == NullPtr);
	
	IS_TRUE(entity->ContainsComponent(AnimatedSprite::TypeID));
	IS_TRUE(entity->ContainsComponent(BaseSprite::TypeID));
	IS_TRUE(entity->ContainsComponent(Renderable::TypeID));
	
	entity->RemoveComponent(AnimatedSprite::TypeID);
	entity->AddComponent(animatedSprite);
	entity->RemoveComponent(BaseSprite::TypeID);
	entity->AddComponent(animatedSprite);
	entity->RemoveComponent(Renderable::TypeID);

	IS_FALSE(entity->ContainsComponent(AnimatedSprite::TypeID));
	IS_FALSE(entity->ContainsComponent(BaseSprite::TypeID));
	IS_FALSE(entity->ContainsComponent(Renderable::TypeID));	
}

//===========================================================================//

#ifdef DEBUGGING
TEST(EntityGetComponentExceptions)
{
	SharedPtr<Entity> entity = new Entity("EntityForTest", "Testing");
	
	SharedPtr<PositioningNode> node = new PositioningNode("EntitySystemTests");
	entity->AddComponent(node);
	SharedPtr<AnimatedSprite> animatedSprite = new AnimatedSprite("EntitySystemTests", "Layer", "Shield");
	entity->AddComponent(animatedSprite);
	
	IS_THROW(entity->AddComponent(animatedSprite), InvalidArgumentException);
	IS_THROW(entity->RemoveComponent(MovementToPointController::TypeID), InvalidArgumentException);
	IS_THROW(entity->GetComponent<PositioningNode>(Renderable::TypeID), InvalidArgumentException);
	IS_THROW(node->GetOwnerComponent<PositioningNode>(Renderable::TypeID), InvalidArgumentException);
}
#endif // DEBUGGING

//===========================================================================//

TEST_FIXTURE_TEAR_DOWN(EntitySystem)
{
	Renderer->RemoveLayer("EntitySystemTests", "Layer");

	DomainManager->Remove("EntitySystemTests");
	TextureManager->Remove("Shield");
	ParametersFileManager->Remove("AnimatedSprites/Shield");
}

//===========================================================================//

} // namespace Bru


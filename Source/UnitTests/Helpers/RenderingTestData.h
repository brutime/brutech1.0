//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Данные для тестов меша
//

#include "../../Common/Common.h"

#ifndef RENDERING_TEST_DATA_H
#define RENDERING_TEST_DATA_H

namespace Bru
{

//===========================================================================//

const integer FirstTestVerticesCount = 4;
const integer FirstTestIndicesCount = 6;
const Color FirstTestVerticesColor = Colors::Red;

const Array<Vertex>::SimpleType FirstTestVertices =
{
	Vertex(Vector3(0.1f, 0.2f, 0.3f), Vector2(0.01f, 0.02f), FirstTestVerticesColor),
	Vertex(Vector3(0.4f, 0.5f, 0.6f), Vector2(0.03f, 0.04f), FirstTestVerticesColor),
	Vertex(Vector3(0.7f, 0.8f, 0.9f), Vector2(0.05f, 0.06f), FirstTestVerticesColor),
	Vertex(Vector3(0.10f, 0.11f, 0.12f), Vector2(0.07f, 0.08f), FirstTestVerticesColor)
};

const Vector3 FirstTestMinVertexPosition = Vector3(0.1f, 0.11f, 0.12f);
const Vector3 FirstTestMaxVertexPosition = Vector3(0.7f, 0.8f, 0.9f);

const Array<uint>::SimpleType FirstTestIndices = { 0, 1, 2, 2, 3, 0 };
const integer FirstTestMaxIndex = 3;

//===========================================================================//

const integer SecondTestVerticesCount = 6;
const integer SecondTestIndicesCount = 12;
const Color SecondTestVerticesColor = Colors::Green;

const Array<Vertex>::SimpleType SecondTestVertices =
{
	Vertex(Vector3(0.13f, 0.14f, 0.15f), Vector2(0.09f, 0.10f), SecondTestVerticesColor),
	Vertex(Vector3(0.16f, 0.17f, 0.18f), Vector2(0.11f, 0.12f), SecondTestVerticesColor),
	Vertex(Vector3(0.19f, 0.20f, 0.21f), Vector2(0.13f, 0.14f), SecondTestVerticesColor),
	Vertex(Vector3(0.22f, 0.23f, 0.24f), Vector2(0.15f, 0.16f), SecondTestVerticesColor),
	Vertex(Vector3(0.25f, 0.26f, 0.27f), Vector2(0.17f, 0.18f), SecondTestVerticesColor),
	Vertex(Vector3(0.28f, 0.29f, 0.30f), Vector2(0.19f, 0.20f), SecondTestVerticesColor)
};

const Array<uint>::SimpleType SecondTestIndices = { 0, 1, 2, 2, 3, 0, 1, 4, 5, 5, 2, 1 };
const integer SecondTestMaxIndex = 5;

//===========================================================================//

const integer ThirdTestVerticesCount = 3;
const integer ThirdTestIndicesCount = 3;
const Color ThirdTestVerticesColor = Colors::Blue;

const Array<Vertex>::SimpleType ThirdTestVertices =
{
	Vertex(Vector3(0.31f, 0.32f, 0.33f), Vector2(0.21f, 0.22f), ThirdTestVerticesColor),
	Vertex(Vector3(0.34f, 0.35f, 0.36f), Vector2(0.23f, 0.24f), ThirdTestVerticesColor),
	Vertex(Vector3(0.37f, 0.36f, 0.39f), Vector2(0.25f, 0.26f), ThirdTestVerticesColor)
};

const Array<uint>::SimpleType ThirdTestIndices = { 0, 1, 2 };
const integer ThirdTestMaxIndex = 2;

//===========================================================================//

} // namespace Bru

#endif // RENDERING_TEST_DATA_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Вспомогательный класс для тестирования строки
//

#pragma once
#ifndef STRING_TESTS_HELPER_H
#define STRING_TESTS_HELPER_H

#include "../../Common/Common.h"
#include "../../Common/Types/String/UTF8Char.h"

namespace Bru
{

//===========================================================================//

class SingletonStringTestsHelper : public SingletonMixIn<SingletonStringTestsHelper>
{
	friend class SingletonMixIn<SingletonStringTestsHelper> ;

public:
	virtual ~SingletonStringTestsHelper();

	const char * GetHalfOfMaxLengthChars() const
	{
		return halfOfMaxLengthChars;
	}
	const char * GetMaxLengthChars() const
	{
		return maxLengthChars;
	}
	const char * GetMoreThanMaxLengthChars() const
	{
		return moreThanMaxLengthChars;
	}

	const ushort * GetHalfOfMaxLengthUTF16Chars() const
	{
		return halfOfMaxLengthUTF16Chars;
	}
	const ushort * GetMaxLengthUTF16Chars() const
	{
		return maxLengthUTF16Chars;
	}
	const ushort * GetMoreThanMaxLengthUTF16Chars() const
	{
		return moreThanMaxLengthUTF16Chars;
	}

	const ulong * GetHalfOfMaxLengthUTF32Chars() const
	{
		return halfOfMaxLengthUTF32Chars;
	}
	const ulong * GetMaxLengthUTF32Chars() const
	{
		return maxLengthUTF32Chars;
	}
	const ulong * GetMoreThanMaxLengthUTF32Chars() const
	{
		return moreThanMaxLengthUTF32Chars;
	}

	const char * GetFillingChar() const
	{
		return fillingChars;
	}

protected:
	SingletonStringTestsHelper();

private:
	static const char FillingChar;

	void FillChars();
	void FillUTF16Chars();
	void FillUTF32Chars();

	char * halfOfMaxLengthChars;
	char * maxLengthChars;
	char * moreThanMaxLengthChars;

	ushort * halfOfMaxLengthUTF16Chars;
	ushort * maxLengthUTF16Chars;
	ushort * moreThanMaxLengthUTF16Chars;

	ulong * halfOfMaxLengthUTF32Chars;
	ulong * maxLengthUTF32Chars;
	ulong * moreThanMaxLengthUTF32Chars;

	char * fillingChars;

	SingletonStringTestsHelper(const SingletonStringTestsHelper &);
	SingletonStringTestsHelper & operator =(const SingletonStringTestsHelper &);
};

//===========================================================================//

#define StringTestsHelper SingletonStringTestsHelper::Instance()

//===========================================================================//

}//namespace

#endif // STRING_TESTS_HELPER_H

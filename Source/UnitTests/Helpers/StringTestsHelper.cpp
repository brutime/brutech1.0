//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StringTestsHelper.h"

#include "../../Common/Types/String/String.h"
#include "../../Common/Helpers/Memory.h"

namespace Bru
{

//===========================================================================//

const char SingletonStringTestsHelper::FillingChar = 'B';

//===========================================================================//

SingletonStringTestsHelper::SingletonStringTestsHelper() :
	SingletonMixIn<SingletonStringTestsHelper>(),
	halfOfMaxLengthChars(NullPtr),
	maxLengthChars(NullPtr),
	moreThanMaxLengthChars(NullPtr),

	halfOfMaxLengthUTF16Chars(NullPtr),
	maxLengthUTF16Chars(NullPtr),
	moreThanMaxLengthUTF16Chars(NullPtr),

	halfOfMaxLengthUTF32Chars(NullPtr),
	maxLengthUTF32Chars(NullPtr),
	moreThanMaxLengthUTF32Chars(NullPtr),

	fillingChars(NullPtr)
{
	FillChars();
	FillUTF16Chars();
	FillUTF32Chars();

	fillingChars = new char[2];
	fillingChars[0] = FillingChar;
	fillingChars[1] = NullTerminator;
}

//===========================================================================//

SingletonStringTestsHelper::~SingletonStringTestsHelper()
{
	delete[] fillingChars;
	delete[] halfOfMaxLengthChars;
	delete[] maxLengthChars;
	delete[] moreThanMaxLengthChars;
}

//===========================================================================//

void SingletonStringTestsHelper::FillChars()
{
	const integer HalfOfMaxLengthCharsCount = string::MaxLength / 2;
	halfOfMaxLengthChars = new char[HalfOfMaxLengthCharsCount + 1];
	Memory::FillWithValue(halfOfMaxLengthChars, FillingChar, HalfOfMaxLengthCharsCount);
	halfOfMaxLengthChars[HalfOfMaxLengthCharsCount] = NullTerminator;

	maxLengthChars = new char[string::MaxLength + 1];
	Memory::FillWithValue(maxLengthChars, FillingChar, string::MaxLength);
	maxLengthChars[string::MaxLength] = NullTerminator;

	moreThanMaxLengthChars = new char[string::MaxLength + 2];
	Memory::FillWithValue(moreThanMaxLengthChars, FillingChar, string::MaxLength + 1);
	moreThanMaxLengthChars[string::MaxLength + 1] = NullTerminator;
}

//===========================================================================//

void SingletonStringTestsHelper::FillUTF16Chars()
{
	const integer HalfOfMaxLengthUTF16CharsCount = string::MaxLength / 2;
	halfOfMaxLengthUTF16Chars = new ushort[HalfOfMaxLengthUTF16CharsCount + 1];
	for (integer i = 0; i < HalfOfMaxLengthUTF16CharsCount; i++)
	{
		halfOfMaxLengthUTF16Chars[i] = static_cast<ushort>(FillingChar);
	}
	halfOfMaxLengthUTF16Chars[HalfOfMaxLengthUTF16CharsCount] = NullTerminator;

	maxLengthUTF16Chars = new ushort[string::MaxLength + 1];
	for (integer i = 0; i < string::MaxLength; i++)
	{
		maxLengthUTF16Chars[i] = static_cast<ushort>(FillingChar);
	}
	maxLengthUTF16Chars[string::MaxLength] = NullTerminator;

	moreThanMaxLengthUTF16Chars = new ushort[string::MaxLength + 2];
	for (integer i = 0; i < string::MaxLength + 1; i++)
	{
		moreThanMaxLengthUTF16Chars[i] = static_cast<ushort>(FillingChar);
	}
	moreThanMaxLengthUTF16Chars[string::MaxLength + 1] = NullTerminator;
}

//===========================================================================//

void SingletonStringTestsHelper::FillUTF32Chars()
{
	const integer HalfOfMaxLengthUTF32CharsCount = string::MaxLength / 2;
	halfOfMaxLengthUTF32Chars = new ulong[HalfOfMaxLengthUTF32CharsCount + 1];
	for (integer i = 0; i < HalfOfMaxLengthUTF32CharsCount; i++)
	{
		halfOfMaxLengthUTF32Chars[i] = static_cast<ulong>(FillingChar);
	}
	halfOfMaxLengthUTF32Chars[HalfOfMaxLengthUTF32CharsCount] = NullTerminator;

	maxLengthUTF32Chars = new ulong[string::MaxLength + 1];
	for (integer i = 0; i < string::MaxLength; i++)
	{
		maxLengthUTF32Chars[i] = static_cast<ulong>(FillingChar);
	}
	maxLengthUTF32Chars[string::MaxLength] = NullTerminator;

	moreThanMaxLengthUTF32Chars = new ulong[string::MaxLength + 2];
	for (integer i = 0; i < string::MaxLength + 1; i++)
	{
		moreThanMaxLengthUTF32Chars[i] = static_cast<ulong>(FillingChar);
	}
	moreThanMaxLengthUTF32Chars[string::MaxLength + 1] = NullTerminator;
}

//===========================================================================//

}//namespace

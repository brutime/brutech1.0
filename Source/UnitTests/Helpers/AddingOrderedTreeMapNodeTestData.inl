//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "AddingOrderedTreeMapNodeTestData.h"

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapNodeTestData<K, V>::AddingOrderedTreeMapNodeTestData() :
	TreeMapNodeTestData<K, V>(),
	ordinalIndex(0)
{
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapNodeTestData<K, V>::AddingOrderedTreeMapNodeTestData(const K & initKey, const V & initValue,
        TreeMapNodeColor initColor, integer initLocation, integer initOrdinalIndex) :
	TreeMapNodeTestData<K, V>(initKey, initValue, initColor, initLocation),
	ordinalIndex(initOrdinalIndex)
{
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapNodeTestData<K, V>::AddingOrderedTreeMapNodeTestData(
    const AddingOrderedTreeMapNodeTestData<K, V> & other)
{

}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapNodeTestData<K, V> & AddingOrderedTreeMapNodeTestData<K, V>::operator =(
    const AddingOrderedTreeMapNodeTestData<K, V> & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapNodeTestData<K, V>::~AddingOrderedTreeMapNodeTestData()
{
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMapNodeTestData<K, V>::DeepCopy(const AddingOrderedTreeMapNodeTestData<K, V> & other)
{
	TreeMapNodeTestData<K, V>::DeepCopy(other);
	ordinalIndex = other.ordinalIndex;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Данные для тестов контейнеров
//

#include "../Dummies/DummyCompoundObject.h"

#pragma once
#ifndef CONTAINERS_TEST_DATA_H
#define CONTAINERS_TEST_DATA_H

namespace Bru
{

//===========================================================================//

const integer TestIntegersCount = 10;
const integer TestIntegers[TestIntegersCount] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

//===========================================================================//

const integer TestFloatsCount = 10;
const float TestFloats[TestFloatsCount] = { 1.11f, 2.22f, 3.33f, 5.55f, 8.88f, 13.33f, 21.11f, 34.44f, 55.55f, 89.99f };

//===========================================================================//

const DummyCompoundObject d0(0);
const DummyCompoundObject d1(1);
const DummyCompoundObject d2(2);
const DummyCompoundObject d3(3);
const DummyCompoundObject d4(4);
const DummyCompoundObject d5(5);
const DummyCompoundObject d6(6);
const DummyCompoundObject d7(7);
const DummyCompoundObject d8(8);
const DummyCompoundObject d9(9);
const DummyCompoundObject d10(10);
const DummyCompoundObject d11(11);
const DummyCompoundObject d12(12);
const DummyCompoundObject d13(13);
const DummyCompoundObject d14(14);
const DummyCompoundObject d15(15);
const DummyCompoundObject d17(17);
const DummyCompoundObject d22(22);
const DummyCompoundObject d41(41);
const DummyCompoundObject d42(42);
const DummyCompoundObject d52(52);
const DummyCompoundObject d54(54);
const DummyCompoundObject d62(62);
const DummyCompoundObject d666(666);

//===========================================================================//

const integer SmallTestDummyesCount = 5;
const DummyCompoundObject SmallTestDummyes[SmallTestDummyesCount] = { d1, d2, d3, d4, d5, };

//===========================================================================//

const integer TestDummyesCount = 10;
const DummyCompoundObject TestDummyes[TestDummyesCount] = { d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, };

//===========================================================================//

const integer TreeMapElementsCount = 7;
const integer TreeMapExpectedKeys[TreeMapElementsCount] = { 0, 1, 10, 13, 41, 42, 54 };
const DummyCompoundObject TreeMapExpectedValues[TreeMapElementsCount] = { d0, d1, d10, d13, d41, d42, d54 };

//===========================================================================//

const integer AddingOrderedTreeMapElementsCount = 7;
const integer AddingOrderedTreeMapExpectedKeys[] = { 0, 1, 10, 13, 41, 42, 54 };
const DummyCompoundObject AddingOrderedTreeMapExpectedValues[] = { d0, d1, d10, d13, d41, d42, d54 };

//===========================================================================//

}//namespace

#endif // CONTAINERS_TEST_DATA_H

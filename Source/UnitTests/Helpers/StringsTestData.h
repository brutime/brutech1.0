//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Данные для тестов контейнеров
//

#include "../../Common/Common.h"
#include "../../Common/Types/String/UTF8Helper.h"
#include "../../Common/Types/String/UTF16Helper.h"
#include "../../Common/Types/String/UTF32Helper.h"

#pragma once
#ifndef STRINGS_TEST_DATA_H
#define STRINGS_TEST_DATA_H

namespace Bru
{

//===========================================================================//

//"B¥𠀀௵rΩ"
static const integer UTF8TestBytesCount = 15;
static const char UTF8TestBytes[UTF8TestBytesCount] =
{
	66, -62, -91, -16, -96, -128, -128, -32, -81, -75, 114, -30, -124, -90, NullTerminator
};

//===========================================================================//

static const integer UTF8TestLongBytesCount = 30;
static const char UTF8TestLongBytes[UTF8TestLongBytesCount + 1] =
{
	1, 64, 127, -62, -128, -48, -85, -33, -65, -32, -96, -128, -29, -128, -68, -17, -65, -67, -16, -112, -128, -128,
	-16, -99, -124, -98, -12, -113, -65, -67, NullTerminator
};

//===========================================================================//

static const utf8_char UTF8TestChars[] = { "B", "¥", "𠀀", "௵", "r", "Ω" };
static const integer UTF8TestByteIndices[] = { 0, 1, 3, 7, 10, 11, 13 };

//===========================================================================//

static const char UTF8TestInvalidBytes[] =
{
	UTF8Helper::FirstCodeInWrongFirstByteSequence,
	UTF8Helper::LastCodeInWrongFirstByteSequence, UTF8Helper::FirstCodeInOverlongSequenceCode,
	UTF8Helper::LastCodeInOverlongSequenceCode, UTF8Helper::FirstCodeInRestrictedByRFC3629Code,
	UTF8Helper::LastRestrictedByRFC3629Code, UTF8Helper::FirstNotDefinedBySpecificationCode,
	UTF8Helper::LastNotDefinedBySpecificationCode, NullTerminator
};

//===========================================================================//

static const ushort UTF16TestOneWordChar[2] = { 0x0042, NullTerminator };
static const ushort UTF16TestOneWordGreaterChar[2] = { 0x0043, NullTerminator };
static const ushort UTF16TestTwoWordsChar[3] = { 0xD840, 0xDC00, NullTerminator };

//===========================================================================//

//"B¥𠀀௵rΩ"
static const integer UTF16TestWordsCount = 7;
static const integer UTF16TestCharsCount = 6;
static const ushort UTF16TestWords[UTF16TestWordsCount + 1] =
{
	0x0042, 0x00A5, 0xD840, 0xDC00, 0x0BF5, 0x0072, 0x2126, NullTerminator
};

//===========================================================================//

static const integer UTF16TestLongWordsCount = 15;
static const integer UTF16TestLongCharsCount = 12;
static const ushort UTF16TestLongWords[UTF16TestLongWordsCount + 1] =
{
	0x0001, 0x0040, 0x007F, 0x0080, 0x00042B, 0x07FF, 0x0800, 0x303C, 0xFFFD, 0xD800, 0xDC00,
	0xD834, 0xDD1E, 0xDBFF, 0xDFFD, NullTerminator
};

//===========================================================================//

static const ushort UTF16TestInvalidWords[] =
{
	UTF16Helper::FirstCodeInFirstNoncharRange, UTF16Helper::LastCodeInFirstNoncharRange,
	UTF16Helper::FirstCodeInSecondNoncharRange, UTF16Helper::LastCodeInSecondNoncharRange, NullTerminator
};

//===========================================================================//

static const ulong UTF32TestChar = 0x20000;
static const ulong UTF32TestGreaterChar = 0x20001;

//===========================================================================//

//"B¥𠀀௵rΩ"
static const integer UTF32TestCharsCount = 6;
static const ulong UTF32TestChars[UTF32TestCharsCount + 1] =
{
	0x0042, 0x00A5, 0x20000, 0x0BF5, 0x0072, 0x2126, NullTerminator
};

//===========================================================================//

static const integer UTF32TestLongCharsCount = 12;
static const ulong UTF32TestLongChars[UTF32TestLongCharsCount + 1] =
{
	0x000001, 0x000040, 0x00007F, 0x000080, 0x00042B, 0x0007FF, 0x000800, 0x00303C, 0x00FFFD,
	0x010000, 0x01D11E, 0x10FFFD, NullTerminator
};

//===========================================================================//

static const ulong UTF32TestInvalidChars[] =
{
	UTF32Helper::FirstCodeInFirstNoncharRange, UTF32Helper::LastCodeInFirstNoncharRange, 
	UTF32Helper::FirstCodeInSecondNoncharRange, UTF32Helper::LastCodeInSecondNoncharRange, NullTerminator
};

//===========================================================================//

} // namespace Bru

#endif // STRINGS_TEST_DATA_H

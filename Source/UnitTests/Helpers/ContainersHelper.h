//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Вспомогательный класс для тестов контроллеров
//

#pragma once
#ifndef CONTAINERS_HELPER_H
#define CONTAINERS_HELPER_H

#include "../../Common/Common.h"

#include "ContainersTestData.h"
#include "TreeMapChecker.h"

namespace Bru
{

//===========================================================================//

class ContainersHelper
{
public:
	static AddingOrderedTreeMap<integer, DummyCompoundObject> CreateTestAddingOrderedMap();
	static bool CheckTestAddingOrderedTreeMap(const AddingOrderedTreeMap<integer, DummyCompoundObject> & map);

	static Array<integer>::SimpleType CreateTestIntegersArray();
	static Array<float>::SimpleType CreateTestFloatsArray();
	static Array<DummyCompoundObject> CreateTestDummyCompoundObjectArray();

	static List<float> CreateTestFloatsList();
	static List<DummyCompoundObject> CreateTestDummyCompoundObjectList();

	static Stack<float>::SimpleType CreateTestFloatsStack();
	static Stack<DummyCompoundObject> CreateTestDummyCompoundObjectStack();

	static TreeMap<integer, DummyCompoundObject> CreateTestTreeMap();
	static bool CheckTestTreeMap(const TreeMap<integer, DummyCompoundObject> & map);

private:
	ContainersHelper() = delete;
	ContainersHelper(const ContainersHelper &) = delete;
	~ContainersHelper() = delete;
	ContainersHelper & operator =(const ContainersHelper &) = delete;
};

//===========================================================================//

}//namespace

#endif // CONTAINERS_HELPER_H

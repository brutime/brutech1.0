//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
bool TreeMapChecker<K, V>::CheckTreeMap(const TreeMap<K, V> & map,
                                        const Array<TreeMapNodeTestData<K, V> > & expectedData)
{
	if (map.GetCount() != expectedData.GetCount())
	{
		OSConsoleStream->WriteLine(
		    string::Format("Expected map count {0} but was {1}", expectedData.GetCount(), map.GetCount()));
		return false;
	}

	if (map._rootNode == NullPtr)
	{
		return true;
	}

	return CheckTreeMapNode(map, map._mostLeftNode, expectedData, 0);
}

//===========================================================================//

template<typename K, typename V>
bool TreeMapChecker<K, V>::CheckAddingOrderedTreeMap(const AddingOrderedTreeMap<K, V> & map,
        const Array<TreeMapNodeTestData<K, V> > & expectedData)
{
	if (map.GetCount() != expectedData.GetCount())
	{
		OSConsoleStream->WriteLine(
		    string::Format("Expected map count {0} but was {1}", expectedData.GetCount(), map.GetCount()));
	}

	if (map._rootNode == NullPtr)
	{
		return true;
	}

	return CheckAddingOrderedTreeMapNode(map, expectedData, 0);
}

//===========================================================================//

template<typename K, typename V>
bool TreeMapChecker<K, V>::CheckNode(const TreeMapNode<K, V> * node, const K & key, const V & value)
{
	if (node == NullPtr)
	{
		OSConsoleStream->WriteLine("node == NullPtr");
		return false;
	}

	if (node->Pair.GetKey() != key)
	{
		OSConsoleStream->WriteLine(string::Format("Expected key {0} but was {1}", key, node->Pair.GetKey()));
		return false;
	}

	if (node->Pair.GetValue() != value)
	{
		OSConsoleStream->WriteLine(
		    string::Format("Expected value {0} but was {1}. Key: {2}", value, node->Pair.GetValue(),
		                   node->Pair.GetKey()));
		return false;
	}

	return true;
}

//===========================================================================//

template<typename K, typename V>
bool TreeMapChecker<K, V>::CheckTreeMapNode(const TreeMap<K, V> & map, TreeMapNode<K, V> * node,
        const Array<TreeMapNodeTestData<K, V> > & expectedData, integer index)
{
	bool commonCheckPassed = CommonCheck(node, expectedData[index]);
	if (!commonCheckPassed)
	{
		return false;
	}

	if (node->RightNode != NullPtr)
	{
		node = node->RightNode;
		while (node->LeftNode != NullPtr)
		{
			node = node->LeftNode;
		}
	}
	else if (node->ParentNode == NullPtr)
	{
		return true;
	}
	else
	{
		TreeMapNode<K, V> * parentNode = node->ParentNode;
		while (node == parentNode->RightNode)
		{
			node = parentNode;
			parentNode = parentNode->ParentNode;
			if (parentNode == NullPtr)
			{
				return true;
			}
		}
		if (node->RightNode != parentNode)
		{
			node = parentNode;
		}
	}

	return CheckTreeMapNode(map, node, expectedData, ++index);
}

//===========================================================================//

template<typename K, typename V>
bool TreeMapChecker<K, V>::CheckAddingOrderedTreeMapNode(const AddingOrderedTreeMap<K, V> & map,
        const Array<TreeMapNodeTestData<K, V> > & expectedData, integer index)
{
	TreeMapNode<K, V> * node = map._ordinalArray[index];
	bool commonCheckPassed = CommonCheck(node, expectedData[index]);
	if (!commonCheckPassed)
	{
		return false;
	}

	if (index == map.GetCount() - 1)
	{
		return true;
	}

	return CheckAddingOrderedTreeMapNode(map, expectedData, ++index);
}

//===========================================================================//

template<typename K, typename V>
bool TreeMapChecker<K, V>::CommonCheck(TreeMapNode<K, V> * node, const TreeMapNodeTestData<K, V> & expectedDataElement)
{
	if (node->Pair.GetKey() != expectedDataElement.GetKey())
	{
		OSConsoleStream->WriteLine(
		    string::Format("Expected key {0} but was {1}", expectedDataElement.GetKey(), node->Pair.GetKey()));
		return false;
	}

	if (node->Pair.GetValue() != expectedDataElement.GetValue())
	{
		OSConsoleStream->WriteLine(
		    string::Format("Expected value {0} but was {1}. Key: {2}", expectedDataElement.GetValue(),
		                   node->Pair.GetValue(), node->Pair.GetKey()));
		return false;
	}

	if (node->Color != expectedDataElement.GetColor())
	{
		OSConsoleStream->WriteLine(
		    string::Format("Expected color {0} but was {1}. Key: {2}",
		                   static_cast<integer>(expectedDataElement.GetColor()), static_cast<integer>(node->Color),
		                   node->Pair.GetKey()));
		return false;
	}

	return true;
}

//===========================================================================//

}//namespace

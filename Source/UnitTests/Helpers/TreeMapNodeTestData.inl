//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
TreeMapNodeTestData<K, V>::TreeMapNodeTestData() :
	_key(),
	_value(),
	_color(TreeMapNodeColor::Black)
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapNodeTestData<K, V>::TreeMapNodeTestData(const K & key, const V & value, TreeMapNodeColor color) :
	_key(key),
	_value(value),
	_color(color)
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapNodeTestData<K, V>::TreeMapNodeTestData(const TreeMapNodeTestData & other) :
	_key(other.key),
	_value(other.value),
	_color(other.color)
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapNodeTestData<K, V>::~TreeMapNodeTestData()
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapNodeTestData<K, V> & TreeMapNodeTestData<K, V>::operator =(const TreeMapNodeTestData<K, V> & other)
{
	if (this == &other)
	{
		return *this;
	}

	_key = other._key;
	_value = other._value;
	_color = other._color;

	return *this;
}

//===========================================================================//

}// namespace Bru

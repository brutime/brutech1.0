//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Вспомогательный класс для тестирования строки
//

#pragma once
#ifndef TREE_MAP_NODE_TEST_DATA_H
#define TREE_MAP_NODE_TEST_DATA_H

#include "../../Common/Common.h"
#include "../Dummies/DummyCompoundObject.h"

namespace Bru
{

//===========================================================================//

//для тестирования
template<typename K, typename V>
class TreeMapChecker;

//===========================================================================//

template<typename K, typename V>
class TreeMapNodeTestData
{
public:
	TreeMapNodeTestData();
	TreeMapNodeTestData(const K & _key, const V & _value, TreeMapNodeColor _color);
	TreeMapNodeTestData(const TreeMapNodeTestData<K, V> & other);

	virtual ~TreeMapNodeTestData();

	TreeMapNodeTestData<K, V> & operator =(const TreeMapNodeTestData<K, V> & other);

	const K & GetKey() const
	{
		return _key;
	}
	const V & GetValue() const
	{
		return _value;
	}
	TreeMapNodeColor GetColor() const
	{
		return _color;
	}

private:
	K _key;
	V _value;
	TreeMapNodeColor _color;

};

//===========================================================================//

}// namespace Bru

#include "TreeMapNodeTestData.inl"

#endif // TREE_MAP_NODE_TEST_DATA_H

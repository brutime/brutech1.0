//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание: приложение для запуска unit тестов
//

#pragma once
#ifndef APPLICATION_FOR_UNIT_TESTS_H
#define APPLICATION_FOR_UNIT_TESTS_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class ApplicationForUnitTests : public BaseApplication
{
public:
	ApplicationForUnitTests();
	virtual ~ApplicationForUnitTests();

	void Initialize();
	void MainLoop();
	void FreeResources();

private:
	static const string WindowTitle;
};

//===========================================================================//

} // namespace Bru

#endif // APPLICATION_FOR_UNIT_TESTS_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Вспомогательный класс для тестирования adding ordered словаря
//

#pragma once
#ifndef ADDING_ORDERED_TREE_MAP_NODE_TEST_DATA_H
#define ADDING_ORDERED_TREE_MAP_NODE_TEST_DATA_H

#include "TreeMapNodeTestData.h"

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
class AddingOrderedTreeMapNodeTestData : public TreeMapNodeTestData<K, V>
{
public:
	AddingOrderedTreeMapNodeTestData();
	///location -1: left, 0: root, 1: right
	AddingOrderedTreeMapNodeTestData(const K & initKey, const V & initValue, TreeMapNodeColor initColor,
	                                 integer initLocation, integer initOrdinalIndex);
	AddingOrderedTreeMapNodeTestData(const AddingOrderedTreeMapNodeTestData<K, V> & other);
	AddingOrderedTreeMapNodeTestData & operator =(const AddingOrderedTreeMapNodeTestData<K, V> & other);
	~AddingOrderedTreeMapNodeTestData();

	integer GetOrdinalIndex() const
	{
		return ordinalIndex;
	}

protected:
	void DeepCopy(const AddingOrderedTreeMapNodeTestData<K, V> & other);

private:
	integer ordinalIndex;
};

//===========================================================================//

}//namespace

#include "AddingOrderedTreeMapNodeTestData.inl"

#endif // ADDING_ORDERED_TREE_MAP_NODE_TEST_DATA_H

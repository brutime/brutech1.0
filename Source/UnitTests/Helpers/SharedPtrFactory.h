//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс фабрики для проверки SharedPtr
//

#pragma once
#ifndef SHARED_PTR_FACTORY_H
#define SHARED_PTR_FACTORY_H

#include "../../Common/Common.h"
#include "../Dummies/DummyChild.h"

namespace Bru
{

//===========================================================================//

class SharedPtrFactory
{
public:
	SharedPtrFactory()
	{
	}
	virtual ~SharedPtrFactory()
	{
	}

	SharedPtr<DummyBase> CreateBase()
	{
		return new DummyBase();
	}
	SharedPtr<DummyChild> CreateChild()
	{
		return new DummyChild();
	}

private:
	SharedPtrFactory(const SharedPtrFactory &);
	SharedPtrFactory & operator =(const SharedPtrFactory &);
};

//===========================================================================//

}//namespace

#endif // SHARED_PTR_FACTORY_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Вспомогательный класс для тестирования словаря
//

#pragma once
#ifndef TREE_MAP_CHECKER_H
#define TREE_MAP_CHECKER_H

#include "../../Common/Common.h"

#include "../Dummies/DummyCompoundObject.h"

#include "TreeMapNodeTestData.h"

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
class TreeMapChecker
{
public:
	static bool CheckTreeMap(const TreeMap<K, V> & map, const Array<TreeMapNodeTestData<K, V> > & expectedData);

	static bool CheckAddingOrderedTreeMap(const AddingOrderedTreeMap<K, V> & map,
	                                      const Array<TreeMapNodeTestData<K, V> > & expectedData);

	static bool CheckNode(const TreeMapNode<K, V> * node, const K & key, const V & value);

private:
	static bool CheckTreeMapNode(const TreeMap<K, V> & map, TreeMapNode<K, V> * node,
	                             const Array<TreeMapNodeTestData<K, V> > & expectedData, integer index);

	static bool CheckAddingOrderedTreeMapNode(const AddingOrderedTreeMap<K, V> & map,
	        const Array<TreeMapNodeTestData<K, V> > & expectedData, integer index);

	static bool CommonCheck(TreeMapNode<K, V> * node, const TreeMapNodeTestData<K, V> & expectedDataElement);

	TreeMapChecker() = delete;
	TreeMapChecker(const TreeMapChecker<K, V> &) = delete;
	~TreeMapChecker() = delete;
	TreeMapChecker<K, V> & operator =(const TreeMapChecker<K, V> &) = delete;
};

//===========================================================================//

}//namespace

#include "TreeMapChecker.inl"

#endif // TREE_MAP_CHECKER_H

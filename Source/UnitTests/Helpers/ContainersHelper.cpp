//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ContainersHelper.h"

namespace Bru
{

//===========================================================================//

AddingOrderedTreeMap<integer, DummyCompoundObject> ContainersHelper::CreateTestAddingOrderedMap()
{
	AddingOrderedTreeMap<integer, DummyCompoundObject> map;

	for (integer i = 0; i < AddingOrderedTreeMapElementsCount; i++)
	{
		map.Add(AddingOrderedTreeMapExpectedKeys[i], AddingOrderedTreeMapExpectedValues[i]);
	}

	return map;
}

//===========================================================================//

bool ContainersHelper::CheckTestAddingOrderedTreeMap(const AddingOrderedTreeMap<integer, DummyCompoundObject> & map)
{
	Array<TreeMapNodeTestData<integer, DummyCompoundObject> > data(7);
	data[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(0, d0, TreeMapNodeColor::Black);
	data[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(1, d1, TreeMapNodeColor::Black);
	data[2] = TreeMapNodeTestData<integer, DummyCompoundObject>(10, d10, TreeMapNodeColor::Black);
	data[3] = TreeMapNodeTestData<integer, DummyCompoundObject>(13, d13, TreeMapNodeColor::Red);
	data[4] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Red);
	data[5] = TreeMapNodeTestData<integer, DummyCompoundObject>(42, d42, TreeMapNodeColor::Black);
	data[6] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Red);

	return TreeMapChecker<integer, DummyCompoundObject>::CheckAddingOrderedTreeMap(map, data);
}

//===========================================================================//

Array<integer>::SimpleType ContainersHelper::CreateTestIntegersArray()
{
	Array<integer>::SimpleType sourceArray(TestIntegersCount);

	for (integer i = 0; i < TestIntegersCount; i++)
	{
		sourceArray[i] = TestIntegers[i];
	}

	return sourceArray;
}

//===========================================================================//

Array<float>::SimpleType ContainersHelper::CreateTestFloatsArray()
{
	Array<float>::SimpleType sourceArray(TestFloatsCount);

	for (integer i = 0; i < TestFloatsCount; i++)
	{
		sourceArray[i] = TestFloats[i];
	}

	return sourceArray;
}

//===========================================================================//

Array<DummyCompoundObject> ContainersHelper::CreateTestDummyCompoundObjectArray()
{
	Array<DummyCompoundObject> sourceArray(TestDummyesCount);

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		sourceArray[i] = TestDummyes[i];
	}

	return sourceArray;
}

//===========================================================================//

List<float> ContainersHelper::CreateTestFloatsList()
{
	List<float> sourceList;

	for (integer i = 0; i < TestFloatsCount; i++)
	{
		sourceList.Add(TestFloats[i]);
	}

	return sourceList;
}

//===========================================================================//

List<DummyCompoundObject> ContainersHelper::CreateTestDummyCompoundObjectList()
{
	List<DummyCompoundObject> sourceList;

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		sourceList.Add(TestDummyes[i]);
	}

	return sourceList;
}

//===========================================================================//

Stack<float>::SimpleType ContainersHelper::CreateTestFloatsStack()
{
	Stack<float>::SimpleType stack(TestFloatsCount);

	for (integer i = 0; i < TestFloatsCount; i++)
	{
		stack.Push(TestFloats[i]);
	}

	return stack;
}

//===========================================================================//

Stack<DummyCompoundObject> ContainersHelper::CreateTestDummyCompoundObjectStack()
{
	Stack<DummyCompoundObject> stack(TestDummyesCount);

	for (integer i = 0; i < TestDummyesCount; i++)
	{
		stack.Push(TestDummyes[i]);
	}

	return stack;
}

//===========================================================================//

TreeMap<integer, DummyCompoundObject> ContainersHelper::CreateTestTreeMap()
{
	TreeMap<integer, DummyCompoundObject> map;

	for (integer i = 0; i < TreeMapElementsCount; i++)
	{
		map.Add(TreeMapExpectedKeys[i], TreeMapExpectedValues[i]);
	}

	return map;
}

//===========================================================================//

bool ContainersHelper::CheckTestTreeMap(const TreeMap<integer, DummyCompoundObject> & map)
{
	Array<TreeMapNodeTestData<integer, DummyCompoundObject> > data(7);
	data[0] = TreeMapNodeTestData<integer, DummyCompoundObject>(0, d0, TreeMapNodeColor::Black);
	data[1] = TreeMapNodeTestData<integer, DummyCompoundObject>(1, d1, TreeMapNodeColor::Black);
	data[2] = TreeMapNodeTestData<integer, DummyCompoundObject>(10, d10, TreeMapNodeColor::Black);
	data[3] = TreeMapNodeTestData<integer, DummyCompoundObject>(13, d13, TreeMapNodeColor::Red);
	data[4] = TreeMapNodeTestData<integer, DummyCompoundObject>(41, d41, TreeMapNodeColor::Red);
	data[5] = TreeMapNodeTestData<integer, DummyCompoundObject>(42, d42, TreeMapNodeColor::Black);
	data[6] = TreeMapNodeTestData<integer, DummyCompoundObject>(54, d54, TreeMapNodeColor::Red);

	return TreeMapChecker<integer, DummyCompoundObject>::CheckTreeMap(map, data);
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ApplicationForUnitTests.h"

#include "../../ScriptSystem/ScriptSystem.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/Config/Config.h"
#include "../../Utils/PathHelper/PathHelper.h"
#include "../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"
#include "../../Utils/StringTable/StringTable.h"
#include "../../OperatingSystem/Display.h"
#include "../../Renderer/Renderer.h"
#include "../../Helpers/DomainManager/DomainManager.h"

namespace Bru
{

//===========================================================================//

const string ApplicationForUnitTests::WindowTitle = "Application for unit tests";

//===========================================================================//

ApplicationForUnitTests::ApplicationForUnitTests()
{
}

//===========================================================================//

ApplicationForUnitTests::~ApplicationForUnitTests()
{
}

//===========================================================================//

void ApplicationForUnitTests::Initialize()
{
	OSConsoleStream = new OSConsoleStreamSingleton();
	ExceptionRegistrator = new ExceptionRegistratorSingleton();
	Console = new ConsoleSingleton(MessagePriority::Debug);
	Config = new ConfigSingleton();
	ScriptSystem = new ScriptSystemSingleton();
	CoreWatcher = new CoreWatcherSingleton();
	Watcher = new WatcherSingleton();
	PerfomanceDataCollector = new PerfomanceDataCollectorSingleton(0.5f);
	ParametersFileManager = new ParametersFileManagerSingleton();
	StringTable = new StringTableSingleton(PathHelper::PathToData + "Text/StringTable");
	OperatingSystem = new OperatingSystemSingleton();
	InputSystem = new InputSystemSingleton();
	Updater = new UpdaterSingleton();
	Renderer = new RendererSingleton(400, 300, 400, 300, FullScreenMode::Off, VerticalSyncMode::Off, Colors::Black, ProjectionMode::Perspective);
	WidgetsManager = new WidgetsManagerSingleton();
	DomainManager = new DomainManagerSingleton();
}

//===========================================================================//

void ApplicationForUnitTests::MainLoop()
{
	Sleep(0);
}

//===========================================================================//

void ApplicationForUnitTests::FreeResources()
{	
	DomainManager = NullPtr;
	WidgetsManager = NullPtr;
	Renderer = NullPtr;
	Updater = NullPtr;
	InputSystem = NullPtr;
	OperatingSystem = NullPtr;
	StringTable = NullPtr;
	ParametersFileManager = NullPtr;
	PerfomanceDataCollector = NullPtr;
	Watcher = NullPtr;
	CoreWatcher = NullPtr;
	ScriptSystem = NullPtr;
	Config = NullPtr;	
	Console = NullPtr;
	ExceptionRegistrator = NullPtr;
	OSConsoleStream = NullPtr;
}

//===========================================================================//

}//namespace

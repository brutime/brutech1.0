//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef MESH_HELPER_H
#define MESH_HELPER_H

#include "../../Graphics/Mesh.h"

namespace Bru
{

//===========================================================================//

class MeshHelper
{
public:
	static SharedPtr<Mesh> CreateFirstTestMesh();
	static SharedPtr<Mesh> CreateSecondTestMesh();
	static SharedPtr<Mesh> CreateThirdTestMesh();

private:
	MeshHelper() = delete;	
	MeshHelper(const MeshHelper &) = delete;
	~MeshHelper() = delete;
	MeshHelper & operator =(const MeshHelper &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // MESH_HELPER_H

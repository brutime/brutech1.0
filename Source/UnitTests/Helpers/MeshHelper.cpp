//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MeshHelper.h"

#include "RenderingTestData.h"

namespace Bru
{

//===========================================================================//

SharedPtr<Mesh> MeshHelper::CreateFirstTestMesh()
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles, FirstTestVerticesCount, FirstTestIndicesCount);

	for (integer i = 0; i < FirstTestVerticesCount; i++)
	{
		mesh->SetVertex(i, FirstTestVertices[i].Position, FirstTestVertices[i].TextureCoord, FirstTestVertices[i].VertexColor);
	}

	for (integer i = 0; i < FirstTestIndicesCount; i++)
	{
		mesh->SetIndex(i, FirstTestIndices[i]);
	}

	return mesh;
}

//===========================================================================//

SharedPtr<Mesh> MeshHelper::CreateSecondTestMesh()
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles, SecondTestVerticesCount, SecondTestIndicesCount);

	for (integer i = 0; i < SecondTestVerticesCount; i++)
	{
		mesh->SetVertex(i, SecondTestVertices[i].Position, SecondTestVertices[i].TextureCoord, SecondTestVertices[i].VertexColor);
	}

	for (integer i = 0; i < SecondTestIndicesCount; i++)
	{
		mesh->SetIndex(i, SecondTestIndices[i]);
	}

	return mesh;
}

//===========================================================================//

SharedPtr<Mesh> MeshHelper::CreateThirdTestMesh()
{
	SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles, ThirdTestVerticesCount, ThirdTestIndicesCount);

	for (integer i = 0; i < ThirdTestVerticesCount; i++)
	{
		mesh->SetVertex(i, ThirdTestVertices[i].Position, ThirdTestVertices[i].TextureCoord, ThirdTestVertices[i].VertexColor);
	}

	for (integer i = 0; i < ThirdTestIndicesCount; i++)
	{
		mesh->SetIndex(i, ThirdTestIndices[i]);
	}

	return mesh;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StaticCommand.h"

namespace Bru
{

//===========================================================================//

StaticCommand<void>::StaticCommand(const string & name, StaticMethod staticMethod) : 
	IScriptCommand(),
	_name(name),
	_staticMethod(staticMethod)	
{
}

//===========================================================================//


StaticCommand<void>::~StaticCommand()
{
}

//===========================================================================//

IScriptCommand & StaticCommand<void>::AddParameter(integer, const string &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

IScriptCommand & StaticCommand<void>::BoundParameterValues(integer, const Array<string> &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

CommandExecutionResult StaticCommand<void>::Execute()
{
	CommandExecutionResult result;
	try
	{
		_staticMethod();
		result.ExecutedSuccessfully = true;
	}
	catch (Exception & e)
	{
		result.ResultString = e.GetDetails();
	}
	return result;
}

//===========================================================================//

const string & StaticCommand<void>::GetName() const
{
	return _name;
}

//===========================================================================//

integer StaticCommand<void>::GetArgsCount() const
{
	return 0;
}

//===========================================================================//

const Array<string> & StaticCommand<void>::GetBoundedValues(integer) const
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

} // namespace Bru
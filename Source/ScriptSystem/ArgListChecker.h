//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: утилитарные методы для проверки корректности
//              списков аргументов ArgList<...>
//

#pragma once
#ifndef ARG_LIST_CHECKER_H
#define ARG_LIST_CHECKER_H

#include <bitset>

#include "../Common/Common.h"
#include "Exceptions/InvalidFunctionArgumentException.h"

namespace Bru
{

namespace ArgListChecker
{

//===========================================================================//

/// Проверяет, все ли аргументы заданы, перед вызовом метода
template<integer argCount>
void CheckArguments(const string & commandName, const std::bitset<argCount> & args);

//===========================================================================//

/// Проверяет, не вышел ли переданный индекс за указанные границы
void CheckParameterIndex(integer index, integer maxIndex);

//===========================================================================//

}// namespace ArgListChecker

} // namespace Bru

#include "ArgListChecker.inl"

#endif // ARG_LIST_CHECKER_H

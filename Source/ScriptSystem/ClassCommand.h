//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: шаблон - обертка для метода класса, позволяющая
//              разделить регистрацию метода, задание аргументов и вызов.
//              Из-за того, что задание аргументов отделено от выполнения,
//              копии аргументов хранятся в классе
//

#pragma once
#ifndef CLASS_COMMAND_H
#define CLASS_COMMAND_H

#include <bitset>

#include "../Common/Common.h"
#include "IScriptCommand.h"
#include "ArgList.h"

namespace Bru
{

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
class ClassCommand : public IScriptCommand
{
	typedef Result (Class::*Method)(Types...);
	typedef ArgList<Types...> Args;

public:
	ClassCommand(const string & name, Class * object, Method method);
	virtual ~ClassCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	void ResetArguments();

	string _name;
	Class * _object;
	Method _method;
	Args _args;
	std::bitset<sizeof...(Types)> _argSet;
	Array<Array<string>> _argsValues;

	ClassCommand(const ClassCommand & other) = delete;
	ClassCommand & operator =(const ClassCommand & other) = delete;
};

//===========================================================================//

template<typename Class, typename Result>
class ClassCommand<Class, Result> : public IScriptCommand
{
	typedef Result (Class::*Method)();

public:
	ClassCommand(const string & name, Class * object, Method method);
	virtual ~ClassCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	string _name;
	Class * _object;
	Method _method;

	ClassCommand(const ClassCommand & other) = delete;
	ClassCommand & operator =(const ClassCommand & other) = delete;
};

//===========================================================================//

template<typename Class>
class ClassCommand<Class, void> : public IScriptCommand
{
	typedef void (Class::*Method)();

public:
	ClassCommand(const string & name, Class * object, Method method);
	virtual ~ClassCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	string _name;
	Class * _object;
	Method _method;

	ClassCommand(const ClassCommand & other) = delete;
	ClassCommand & operator =(const ClassCommand & other) = delete;
};

//===========================================================================//

} // namespace Bru

#include "ClassCommand.inl"

#endif // CLASS_COMMAND_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ScriptSystem.h"

#include "../Common/Helpers/TokenParser.h"

namespace Bru
{

//===========================================================================//

SharedPtr<ScriptSystemSingleton> ScriptSystem;

//===========================================================================//

ScriptSystemSingleton::ScriptSystemSingleton() :
	_registeredCommands(),
	_enabledCommands(),
	_disabledCommands()
{
}

//===========================================================================//

ScriptSystemSingleton::~ScriptSystemSingleton()
{
}

//===========================================================================//

void ScriptSystemSingleton::RegisterCommand(const SharedPtr<IScriptCommand> & command)
{
	string commandNameUpperCase = command->GetName().ToUpper();
	if (_registeredCommands.Contains(commandNameUpperCase))
	{
		if (_registeredCommands[commandNameUpperCase].Contains(command->GetArgsCount()))
		{
			ALREADY_REGISTERED_EXCEPTION(string::Format("Command \"{0}\" already registred", command->GetName()));
		}
		else
		{
			_registeredCommands[commandNameUpperCase].Add(command->GetArgsCount(), command);
			_enabledCommands[commandNameUpperCase].Add(command->GetArgsCount(), command);
		}
	}
	else
	{
		TreeMap<integer, SharedPtr<IScriptCommand>> commandsGroup;
		commandsGroup.Add(command->GetArgsCount(), command);
		_registeredCommands.Add(commandNameUpperCase, commandsGroup);
		_enabledCommands.Add(commandNameUpperCase, commandsGroup);
	}
}
//===========================================================================//

void ScriptSystemSingleton::UnregisterCommand(const string & commandName)
{
	string commandNameUpperCase = commandName.ToUpper();
	if (!_registeredCommands.Contains(commandNameUpperCase))
	{
		COMMAND_NOT_REGISTERED_EXCEPTION(commandName);
	}

	_registeredCommands.Remove(commandNameUpperCase);
	if (_enabledCommands.Contains(commandNameUpperCase))
	{
		_enabledCommands.Remove(commandNameUpperCase);
	}
	else
	{
		_disabledCommands.Remove(commandNameUpperCase);
	}
}

//===========================================================================//

bool ScriptSystemSingleton::ContainsCommand(const string & commandName) const
{
	return _registeredCommands.Contains(commandName.ToUpper());
}

//===========================================================================//

void ScriptSystemSingleton::EnableCommand(const string & commandName)
{
	if (!ContainsCommand(commandName))
	{
		COMMAND_NOT_REGISTERED_EXCEPTION(string::Format("Command \"{0}\" not registred", commandName));
	}
	
	string commandNameUpperCase = commandName.ToUpper();
	if (!_enabledCommands.Contains(commandNameUpperCase))
	{
		_enabledCommands.Add(commandNameUpperCase, _registeredCommands[commandNameUpperCase]);
		_disabledCommands.Remove(commandNameUpperCase);	
	}
}

//===========================================================================//

void ScriptSystemSingleton::DisableCommand(const string & commandName)
{
	if (!ContainsCommand(commandName))
	{
		COMMAND_NOT_REGISTERED_EXCEPTION(string::Format("Command \"{0}\" not registred", commandName));
	}

	string commandNameUpperCase = commandName.ToUpper();
	if (!_disabledCommands.Contains(commandNameUpperCase))
	{
		_disabledCommands.Add(commandNameUpperCase, _registeredCommands[commandNameUpperCase]);
		_enabledCommands.Remove(commandNameUpperCase);	
	}
}

//===========================================================================//

bool ScriptSystemSingleton::IsCommandEnabled(const string & commandName) const
{
	if (!ContainsCommand(commandName))
	{
		COMMAND_NOT_REGISTERED_EXCEPTION(string::Format("Command \"{0}\" not registred", commandName));
	}

	return _enabledCommands.Contains(commandName.ToUpper());
}

//===========================================================================//

const SharedPtr<IScriptCommand> & ScriptSystemSingleton::GetCommand(const string & commandName, integer agrsCount) const
{
	return _registeredCommands[commandName.ToUpper()][agrsCount];
}

//===========================================================================//

const SharedPtr<IScriptCommand> & ScriptSystemSingleton::GetCommand(const string & commandName) const
{
	integer maxArgsCount = 0;
	for (auto & commandPair : _registeredCommands[commandName.ToUpper()])
	{
		if (commandPair.GetKey() > maxArgsCount)
		{
			maxArgsCount = commandPair.GetKey();
		}
	}

	return GetCommand(commandName, maxArgsCount);
}

//===========================================================================//

CommandExecutionResult ScriptSystemSingleton::Evaluate(const string & commandText) const
{
	ParsedCommandText parsedCommandText = ParseCommandText(commandText);

	if (!IsCommandRegistered(parsedCommandText))
	{
		COMMAND_NOT_REGISTERED_EXCEPTION(string::Format("Command \"{0}\" not registred", parsedCommandText.CommandName));
	}

	if (!IsCommandArgumentsCorrect(parsedCommandText))
	{
		COMMAND_NOT_REGISTERED_EXCEPTION(string::Format("Command \"{0}\" taking {1} argument(s) not registered",
		                                                parsedCommandText.CommandName, parsedCommandText.Args.GetCount()));
	}
	
	if (!IsCommandEnabled(parsedCommandText.CommandName))
	{
		INVALID_STATE_EXCEPTION(string::Format("Command \"{0}\" is disabled", parsedCommandText.CommandName));
	}	

	string commandNameUpperCase = parsedCommandText.CommandName.ToUpper();
	const TreeMap<integer, SharedPtr<IScriptCommand>> & candidates = _registeredCommands[commandNameUpperCase];
	const SharedPtr<IScriptCommand> & command = candidates[parsedCommandText.Args.GetCount()];

	integer parameterIndex = 0;
	for (const string & parameter : parsedCommandText.Args)
	{
		command->AddParameter(parameterIndex, parameter);
		parameterIndex++;
	}

	return command->Execute();
}

//===========================================================================//

ParsedCommandText ScriptSystemSingleton::ParseCommandText(const string & commandText) const
{
	ParsedCommandText parsedCommandText;

	TokenParser parser(commandText);
	if (parser.HasMore())
	{
		parsedCommandText.CommandName = parser.GetNextToken();
	}

	while (parser.HasMore())
	{
		parsedCommandText.Args.Add(parser.GetNextToken());
	}

	return parsedCommandText;
}

//===========================================================================//

bool ScriptSystemSingleton::IsCommandRegistered(const ParsedCommandText & parsedCommandText) const
{
	string commandNameUpperCase = parsedCommandText.CommandName.ToUpper();
	return _registeredCommands.Contains(commandNameUpperCase);
}

//===========================================================================//

bool ScriptSystemSingleton::IsCommandArgumentsCorrect(const ParsedCommandText & parsedCommandText) const
{
	string commandNameUpperCase = parsedCommandText.CommandName.ToUpper();
	const TreeMap<integer, SharedPtr<IScriptCommand>> & candidates = _registeredCommands[commandNameUpperCase];
	return candidates.Contains(parsedCommandText.Args.GetCount());
}

//===========================================================================//

const TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> &ScriptSystemSingleton::GetRegisteredCommands() const
{
	return _registeredCommands;
}

//===========================================================================//

const TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> &ScriptSystemSingleton::GetEnabledCommands() const
{
	return _enabledCommands;
}

//===========================================================================//

const TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> &ScriptSystemSingleton::GetDisabledCommands() const
{
	return _disabledCommands;
}

//===========================================================================//

List<string> ScriptSystemSingleton::GetEnabledCommandsList() const
{
	List<string> commandHints;
	commandHints.Add("List of commands:");
	for (auto & commandPair : _enabledCommands)
	{
		for (auto & commandVariantPair : commandPair.GetValue())
		{
			const SharedPtr<IScriptCommand> & command = commandVariantPair.GetValue();
			commandHints.Add(string::Format(" - {0}({1})", command->GetName(), command->GetArgsCount()));
		}
	}
	return commandHints;
}

//===========================================================================//

} // namespace

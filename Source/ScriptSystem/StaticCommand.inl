//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename Result, typename ... Types>
StaticCommand<Result, Types ...>::StaticCommand(const string & name, StaticMethod staticMethod) :
	IScriptCommand(),
	_name(name),
	_staticMethod(staticMethod),
	_args(),
	_argSet(),
	_argsValues(GetArgsCount())
{
}

//===========================================================================//

template<typename Result, typename ... Types>
StaticCommand<Result, Types ...>::~StaticCommand()
{
}

//===========================================================================//

template<typename Result, typename ... Types>
IScriptCommand & StaticCommand<Result, Types ...>::AddParameter(integer index, const string & value)
{
	_args.AddParameter(index, value);
	_argSet[index] = 1;
	return *this;
}

//===========================================================================//

template<typename Result, typename ... Types>
IScriptCommand & StaticCommand<Result, Types ...>::BoundParameterValues(integer index, const Array<string> & values)
{
	_argsValues[index] = values;
	return *this;
}

//===========================================================================//

template<typename Result, typename ... Types>
CommandExecutionResult StaticCommand<Result, Types ...>::Execute()
{
	CommandExecutionResult result;
	ArgListChecker::CheckArguments<Args::length>(_name, _argSet);
	try
	{
		CommandExecutor::ExecuteStaticMethod(_staticMethod, _args, result);
		ResetArguments();
	}
	catch (Exception & e)
	{
		result.ResultString = e.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Result, typename ... Types>
const string & StaticCommand<Result, Types ...>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Result, typename ... Types>
integer StaticCommand<Result, Types ...>::GetArgsCount() const
{
	return sizeof ... (Types);
}

//===========================================================================//

template<typename Result, typename ... Types>
const Array<string> & StaticCommand<Result, Types ...>::GetBoundedValues(integer index) const
{
	return _argsValues[index];
}

//===========================================================================//

template<typename Result, typename ... Types>
void StaticCommand<Result, Types ...>::ResetArguments()
{
	_argSet.reset();
	_args.Reset();
}

//===========================================================================//

template<typename Result>
StaticCommand<Result>::StaticCommand(const string & name, StaticMethod staticMethod) :
	IScriptCommand(),
	_name(name),
	_staticMethod(staticMethod)
{
}

//===========================================================================//

template<typename Result>
StaticCommand<Result>::~StaticCommand()
{
}

//===========================================================================//

template<typename Result>
IScriptCommand & StaticCommand<Result>::AddParameter(integer, const string &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Result>
IScriptCommand & StaticCommand<Result>::BoundParameterValues(integer, const Array<string> &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Result>
CommandExecutionResult StaticCommand<Result>::Execute()
{
	CommandExecutionResult result;
	try
	{
		result.ResultString = Converter::ToString(_staticMethod());
		result.ExecutedSuccessfully = true;
		result.ContainsValue = true;
	}
	catch (Exception & e)
	{
		result.ResultString = e.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Result>
const string & StaticCommand<Result>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Result>
integer StaticCommand<Result>::GetArgsCount() const
{
	return 0;
}

//===========================================================================//

template<typename Result>
const Array<string> & StaticCommand<Result>::GetBoundedValues(integer) const
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

} // namespace Bru

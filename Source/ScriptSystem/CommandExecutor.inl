//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

namespace CommandExecutor
{

//===========================================================================//

template<typename ArgList, integer Index>
typename Get<ArgList, Index>::Type & Get<ArgList, Index>::Value(ArgList & list)
{
	return Get<typename ArgList::TailType, Index - 1>::Value(list.Tail);
}

//===========================================================================//

// специализация для завершения рекурсии
template<typename ArgList>
typename ArgList::HeadType & Get<ArgList, 0>::Value(ArgList & list)
{
	return list.Head;
}

//===========================================================================//

template<int N>
struct StaticUnrollHelper
{
	template<typename Result, typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static Result UnrollArgList(Result (*StaticMethod)(MethodArgTypes...), ArgList<ArgListTypes...> argList,
	                            ArgTypes ... args)
	{
		return StaticUnrollHelper<N - 1>::UnrollArgList(StaticMethod, argList,
		        Get<ArgList<ArgListTypes...>, N - 1>::Value(argList), args...);
	}

	template<typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static void UnrollArgList(void (*StaticMethod)(MethodArgTypes...), ArgList<ArgListTypes...> argList,
	                          ArgTypes ... args)
	{
		StaticUnrollHelper<N - 1>::UnrollArgList(StaticMethod, argList,
		        Get<ArgList<ArgListTypes...>, N - 1>::Value(argList), args...);
	}
};

//===========================================================================//

template<>
struct StaticUnrollHelper<0>
{
	template<typename Result, typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static Result UnrollArgList(Result (*StaticMethod)(MethodArgTypes...), ArgList<ArgListTypes...>, ArgTypes ... args)
	{
		return StaticMethod(args...);
	}

	template<typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static void UnrollArgList(void (*StaticMethod)(MethodArgTypes...), ArgList<ArgListTypes...>, ArgTypes ... args)
	{
		StaticMethod(args...);
	}
};

//===========================================================================//

template<typename Result, typename ... ArgTypes>
void ExecuteStaticMethod(Result (*StaticMethod)(ArgTypes...), const ArgList<ArgTypes...> & args,
                         CommandExecutionResult & result)
{
	result.ResultString = Converter::ToString(
	                          StaticUnrollHelper<sizeof...(ArgTypes)>::UnrollArgList(StaticMethod, ArgList<ArgTypes...>(args)));
	result.ExecutedSuccessfully = true;
	result.ContainsValue = true;
}

//===========================================================================//

template<typename ... ArgTypes>
void ExecuteStaticMethod(void (*StaticMethod)(ArgTypes...), const ArgList<ArgTypes...> & args,
                         CommandExecutionResult & result)
{
	StaticUnrollHelper<sizeof...(ArgTypes)>::UnrollArgList(StaticMethod, args);
	result.ExecutedSuccessfully = true;
}

//===========================================================================//

template<int N>
struct UnrollHelper
{
	template<typename Class, typename Result, typename ... MethodArgTypes, typename ... ArgListTypes,
	         typename ... ArgTypes>
	static Result UnrollArgList(Class * object, Result (Class::*Method)(MethodArgTypes...) const,
	                            ArgList<ArgListTypes...> argList, ArgTypes ... args)
	{
		return UnrollHelper<N - 1>::UnrollArgList(object, Method, argList,
		        Get<ArgList<ArgListTypes...>, N - 1>::Value(argList), args...);
	}

	template<typename Class, typename Result, typename ... MethodArgTypes, typename ... ArgListTypes,
	         typename ... ArgTypes>
	static Result UnrollArgList(Class * object, Result (Class::*Method)(MethodArgTypes...),
	                            ArgList<ArgListTypes...> argList, ArgTypes ... args)
	{
		return UnrollHelper<N - 1>::UnrollArgList(object, Method, argList,
		        Get<ArgList<ArgListTypes...>, N - 1>::Value(argList), args...);
	}

	template<typename Class, typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static void UnrollArgList(Class * object, void (Class::*Method)(MethodArgTypes...),
	                          ArgList<ArgListTypes...> argList, ArgTypes ... args)
	{
		UnrollHelper<N - 1>::UnrollArgList(object, Method, argList,
		                                   Get<ArgList<ArgListTypes...>, N - 1>::Value(argList), args...);
	}

	template<typename Class, typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static void UnrollArgList(Class * object, void (Class::*Method)(MethodArgTypes...) const,
	                          ArgList<ArgListTypes...> argList, ArgTypes ... args)
	{
		UnrollHelper<N - 1>::UnrollArgList(object, Method, argList,
		                                   Get<ArgList<ArgListTypes...>, N - 1>::Value(argList), args...);
	}
};

//===========================================================================//

template<>
struct UnrollHelper<0>
{
	template<typename Class, typename Result, typename ... MethodArgTypes, typename ... ArgListTypes,
	         typename ... ArgTypes>
	static Result UnrollArgList(Class * object, Result (Class::*Method)(MethodArgTypes...), ArgList<ArgListTypes...>,
	                            ArgTypes ... args)
	{
		return (object->*Method)(args...);
	}

	template<typename Class, typename Result, typename ... MethodArgTypes, typename ... ArgListTypes,
	         typename ... ArgTypes>
	static Result UnrollArgList(Class * object, Result (Class::*Method)(MethodArgTypes...) const,
	                            ArgList<ArgListTypes...>, ArgTypes ... args)
	{
		return (object->*Method)(args...);
	}

	template<typename Class, typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static void UnrollArgList(Class * object, void (Class::*Method)(MethodArgTypes...), ArgList<ArgListTypes...>,
	                          ArgTypes ... args)
	{
		(object->*Method)(args...);
	}

	template<typename Class, typename ... MethodArgTypes, typename ... ArgListTypes, typename ... ArgTypes>
	static void UnrollArgList(Class * object, void (Class::*Method)(MethodArgTypes...) const, ArgList<ArgListTypes...>,
	                          ArgTypes ... args)
	{
		(object->*Method)(args...);
	}
};

//===========================================================================//

template<typename Class, typename Result, typename ... ArgTypes>
void ExecuteMethod(Class * object, Result (Class::*Method)(ArgTypes...), const ArgList<ArgTypes...> & args,
                   CommandExecutionResult & result)
{
	result.ResultString = Converter::ToString(
	                          UnrollHelper<sizeof...(ArgTypes)>::UnrollArgList(object, Method, ArgList<ArgTypes...>(args)));
	result.ExecutedSuccessfully = true;
	result.ContainsValue = true;
}

//===========================================================================//

template<typename Class, typename Result, typename ... ArgTypes>
void ExecuteMethod(Class * object, Result (Class::*Method)(ArgTypes...) const, const ArgList<ArgTypes...> & args,
                   CommandExecutionResult & result)
{
	result.ResultString = Converter::ToString(
	                          UnrollHelper<sizeof...(ArgTypes)>::UnrollArgList(object, Method, ArgList<ArgTypes...>(args)));
	result.ExecutedSuccessfully = true;
	result.ContainsValue = true;
}

//===========================================================================//

template<typename Class, typename ... ArgTypes>
void ExecuteMethod(Class * object, void (Class::*Method)(ArgTypes...), const ArgList<ArgTypes...> & args,
                   CommandExecutionResult & result)
{
	UnrollHelper<sizeof...(ArgTypes)>::UnrollArgList(object, Method, args);
	result.ExecutedSuccessfully = true;
}

//===========================================================================//

template<typename Class, typename ... ArgTypes>
void ExecuteMethod(Class * object, void (Class::*Method)(ArgTypes...) const, const ArgList<ArgTypes...> & args,
                   CommandExecutionResult & result)
{
	UnrollHelper<sizeof...(ArgTypes)>::UnrollArgList(object, Method, args);
	result.ExecutedSuccessfully = true;
}

//===========================================================================//

}// namespace CommandExecutor

} // namespace Bru


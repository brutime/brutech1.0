//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
ClassCommand<Class, Result, Types ...>::ClassCommand(const string & name, Class * object, Method method) :
	IScriptCommand(),
	_name(name),
	_object(object),
	_method(method),
	_args(),
	_argSet(),
	_argsValues(GetArgsCount())
{
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
ClassCommand<Class, Result, Types ...>::~ClassCommand()
{
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
IScriptCommand & ClassCommand<Class, Result, Types ...>::AddParameter(integer index, const string & value)
{
	_args.AddParameter(index, value);
	_argSet[index] = 1;
	return *this;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
IScriptCommand & ClassCommand<Class, Result, Types ...>::BoundParameterValues(integer index, const Array<string> & values)
{
	_argsValues[index] = values;
	return *this;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
CommandExecutionResult ClassCommand<Class, Result, Types ...>::Execute()
{
	CommandExecutionResult result;
	ArgListChecker::CheckArguments<Args::length>(_name, _argSet);
	try
	{
		CommandExecutor::ExecuteMethod(_object, _method, _args, result);
		ResetArguments();
	}
	catch (Exception & exception)
	{
		result.ResultString = exception.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
const string & ClassCommand<Class, Result, Types ...>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
integer ClassCommand<Class, Result, Types ...>::GetArgsCount() const
{
	return sizeof ... (Types);
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
const Array<string> & ClassCommand<Class, Result, Types ...>::GetBoundedValues(integer index) const
{
	return _argsValues[index];
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
void ClassCommand<Class, Result, Types ...>::ResetArguments()
{
	_argSet.reset();
	_args.Reset();
}

//===========================================================================//

template<typename Class, typename Result>
ClassCommand<Class, Result>::ClassCommand(const string & name, Class * object, Method method) :
	IScriptCommand(),
	_name(name),
	_object(object),
	_method(method)
{
}

//===========================================================================//

template<typename Class, typename Result>
ClassCommand<Class, Result>::~ClassCommand()
{
}

//===========================================================================//

template<typename Class, typename Result>
IScriptCommand & ClassCommand<Class, Result>::AddParameter(integer, const string &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class, typename Result>
IScriptCommand & ClassCommand<Class, Result>::BoundParameterValues(integer, const Array<string> &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class, typename Result>
CommandExecutionResult ClassCommand<Class, Result>::Execute()
{
	CommandExecutionResult result;
	try
	{
		result.ResultString = Converter::ToString((_object->*_method)());
		result.ExecutedSuccessfully = true;
		result.ContainsValue = true;
	}
	catch (Exception & exception)
	{
		result.ResultString = exception.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Class, typename Result>
const string & ClassCommand<Class, Result>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Class, typename Result>
integer ClassCommand<Class, Result>::GetArgsCount() const
{
	return 0;
}

//===========================================================================//

template<typename Class, typename Result>
const Array<string> & ClassCommand<Class, Result>::GetBoundedValues(integer) const
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class>
ClassCommand<Class, void>::ClassCommand(const string & name, Class * object, Method method) :
	IScriptCommand(),
	_name(name),
	_object(object),
	_method(method)
{
}

//===========================================================================//

template<typename Class>
ClassCommand<Class, void>::~ClassCommand()
{
}

//===========================================================================//

template<typename Class>
IScriptCommand & ClassCommand<Class, void>::AddParameter(integer, const string &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class>
IScriptCommand & ClassCommand<Class, void>::BoundParameterValues(integer, const Array<string> &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class>
CommandExecutionResult ClassCommand<Class, void>::Execute()
{
	CommandExecutionResult result;
	try
	{
		(_object->*_method)();
		result.ExecutedSuccessfully = true;
	}
	catch (Exception & exception)
	{
		result.ResultString = exception.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Class>
const string & ClassCommand<Class, void>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Class>
integer ClassCommand<Class, void>::GetArgsCount() const
{
	return 0;
}

//===========================================================================//

template<typename Class>
const Array<string> & ClassCommand<Class, void>::GetBoundedValues(integer) const
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - ошибка выполнения команды
//

#pragma once
#ifndef INVALID_FUNCTION_ARGUMENT_EXCEPTION_H
#define INVALID_FUNCTION_ARGUMENT_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class InvalidFunctionArgumentException : public Exception
{
public:
	InvalidFunctionArgumentException(const string & defails, const string & fileName, const string & methodName,
	                                 integer fileLine);
	virtual ~InvalidFunctionArgumentException();
};

//===========================================================================//

#define INVALID_FUNCTION_ARGUMENT_EXCEPTION(defails) \
    throw InvalidFunctionArgumentException(defails, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // INVALID_FUNCTION_ARGUMENT_EXCEPTION_H

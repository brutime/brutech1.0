//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - добавляемый компонент уже зарегистрирован
//

#pragma once
#ifndef ALREADY_REGISTERED_EXCEPTION_H
#define ALREADY_REGISTERED_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class AlreadyRegisteredException : public Exception
{
public:
	AlreadyRegisteredException(const string & details, const string & fileName, const string & methodName,
	                           integer fileLine);
	virtual ~AlreadyRegisteredException();
};

//===========================================================================//

#define ALREADY_REGISTERED_EXCEPTION(details) \
    throw AlreadyRegisteredException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // ALREADY_REGISTERED_EXCEPTION_H

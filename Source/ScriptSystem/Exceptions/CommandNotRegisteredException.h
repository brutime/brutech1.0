//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - команда не зарегистрирована
//

#pragma once
#ifndef COMMAND_NOT_REGISTERED_EXCEPTION_H
#define COMMAND_NOT_REGISTERED_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class CommandNotRegisteredException : public Exception
{
public:
	CommandNotRegisteredException(const string & details, const string & fileName, const string & methodName,
	                              integer fileLine);
	virtual ~CommandNotRegisteredException();
};

//===========================================================================//

#define COMMAND_NOT_REGISTERED_EXCEPTION(details) \
    throw CommandNotRegisteredException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // COMMAND_NOT_REGISTERED_EXCEPTION_H

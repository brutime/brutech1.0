//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: шаблон - обертка для статического метода, позволяющая
//              разделить регистрацию метода, задание аргументов и вызов.
//              Из-за того, что задание аргументов отделено от выполнения,
//              копии аргументов хранятся в классе
//

#pragma once
#ifndef STATIC_COMMAND_H
#define STATIC_COMMAND_H

#include <bitset>

#include "../Common/Common.h"
#include "IScriptCommand.h"
#include "ArgList.h"

namespace Bru
{

//===========================================================================//

template<typename Result, typename ... Types>
class StaticCommand : public IScriptCommand
{
	typedef Result (* StaticMethod)(Types ...);
	typedef ArgList<Types ...> Args;

public:
	StaticCommand(const string & name, StaticMethod staticMethod);
	virtual ~StaticCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	void ResetArguments();

	string _name;
	StaticMethod _staticMethod;
	Args _args;
	std::bitset<sizeof ... (Types)> _argSet;
	Array<Array<string>> _argsValues;

	StaticCommand(const StaticCommand & other) = delete;
	StaticCommand & operator =(const StaticCommand & other) = delete;
};

//===========================================================================//

template<typename Result>
class StaticCommand<Result> : public IScriptCommand
{
	typedef Result (* StaticMethod)();
public:
	StaticCommand(const string & name, StaticMethod staticMethod);
	virtual ~StaticCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	string _name;
	StaticMethod _staticMethod;

	StaticCommand(const StaticCommand & other) = delete;
	StaticCommand & operator =(const StaticCommand & other) = delete;
};

//===========================================================================//

template<>
class StaticCommand<void> : public IScriptCommand
{
	typedef void (* StaticMethod)();
public:
	StaticCommand(const string & name, StaticMethod staticMethod);
	virtual ~StaticCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);
	
	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	string _name;
	StaticMethod _staticMethod;

	StaticCommand(const StaticCommand & other) = delete;
	StaticCommand & operator =(const StaticCommand & other) = delete;
};

//===========================================================================//

} // namespace Bru

#include "StaticCommand.inl"

#endif // STATIC_COMMAND_H

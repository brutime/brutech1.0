//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: шаблон - обертка для константного метода класса, позволяющая
//              разделить регистрацию метода, задание аргументов и вызов.
//              Из-за того, что задание аргументов отделено от выполнения,
//              копии аргументов хранятся в классе
//

#pragma once
#ifndef CLASS_CONST_COMMAND_H
#define CLASS_CONST_COMMAND_H

#include <bitset>

#include "../Common/Common.h"
#include "IScriptCommand.h"
#include "ArgList.h"

namespace Bru
{

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
class ClassConstCommand : public IScriptCommand
{
	typedef Result (Class::* Method)(Types ...) const;
	typedef ArgList<Types ...> Args;

public:
	ClassConstCommand(const string & name, Class * object, Method method);
	virtual ~ClassConstCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	void ResetArguments();

	string _name;
	Class * _object;
	Method _method;
	Args _args;
	std::bitset<sizeof ... (Types)> _argSet;
	Array<Array<string>> _argsValues;

	ClassConstCommand(const ClassConstCommand & other) = delete;
	ClassConstCommand & operator =(const ClassConstCommand & other) = delete;
};

//===========================================================================//

template<typename Class, typename Result>
class ClassConstCommand<Class, Result> : public IScriptCommand
{
	typedef Result (Class::* Method)() const;

public:
	ClassConstCommand(const string & name, Class * object, Method method);
	virtual ~ClassConstCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	string _name;
	Class * _object;
	Method _method;

	ClassConstCommand(const ClassConstCommand & other) = delete;
	ClassConstCommand & operator =(const ClassConstCommand & other) = delete;
};

//===========================================================================//

template<typename Class>
class ClassConstCommand<Class, void> : public IScriptCommand
{
	typedef void (Class::* Method)() const;

public:
	ClassConstCommand(const string & name, Class * object, Method method);
	virtual ~ClassConstCommand();

	IScriptCommand & AddParameter(integer index, const string & value);
	IScriptCommand & BoundParameterValues(integer index, const Array<string> & values);

	CommandExecutionResult Execute();

	const string & GetName() const;
	integer GetArgsCount() const;
	const Array<string> & GetBoundedValues(integer index) const;

private:
	string _name;
	Class * _object;
	Method _method;

	ClassConstCommand(const ClassConstCommand & other) = delete;
	ClassConstCommand & operator =(const ClassConstCommand & other) = delete;
};

//===========================================================================//

} // namespace Bru

#include "ClassConstCommand.inl"

#endif // CLASS_CONST_COMMAND_H

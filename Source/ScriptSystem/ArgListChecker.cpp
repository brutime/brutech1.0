//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: не-шаблонные методы, которые должны быть скомпилированы
//

#include "ArgListChecker.h"

#include "../Common/Exceptions/InvalidArgumentException.h"

namespace Bru
{

namespace ArgListChecker
{

//===========================================================================//

void CheckParameterIndex(integer index, integer maxIndex)
{
	if (index < 0 || index > maxIndex)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, maxIndex));
	}
}

//===========================================================================//

}// namespace ArgListChecker

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<class T>
struct ArgList<T>
{
	ArgList() :
		Head()
	{
	}

	ArgList(const T & head) :
		Head(head)
	{
	}

	void Reset()
	{
		Head = HeadType();
	}

	typedef T HeadType;
	typedef EmptyClass TailType;

	T Head;

	enum
	{
	    length = 1
	};

	void AddParameter(integer index, const string & value)
	{
		ArgListChecker::CheckParameterIndex(index, 0);
		Head = Convert<string, HeadType>(value);
	}

	operator T()
	{
		return Head.Value;
	}
};

//===========================================================================//

template<class T>
struct ArgList<const T &>
{
	ArgList() :
		Head()
	{
	}

	ArgList(const T & head) :
		Head(head)
	{
	}

	void Reset()
	{
		Head = HeadType();
	}

	typedef T HeadType;
	typedef EmptyClass TailType;

	T Head;

	enum
	{
	    length = 1
	};

	void AddParameter(integer index, const string & value)
	{
		ArgListChecker::CheckParameterIndex(index, 0);
		Head = Convert<string, HeadType>(value);
	}

	operator T()
	{
		return Head.Value;
	}
};

//===========================================================================//

template<class T, class ...TailTypes>
struct ArgList<T, TailTypes...>
{
	ArgList() :
		Head(),
		Tail()
	{
	}

	ArgList(T head, TailTypes ... tail) :
		Head(head),
		Tail(tail...)
	{
	}

	void Reset()
	{
		Head = HeadType();
		Tail.Reset();
	}

	typedef T HeadType;
	typedef ArgList<TailTypes...> TailType;

	enum
	{
	    length = TailType::length + 1
	};

	T Head;
	TailType Tail;

	void AddParameter(integer index, const string & value)
	{
		if (index == 0)
		{
			Head = Convert<string, HeadType>(value);
		}
		else
		{
			Tail.AddParameter(index - 1, value);
		}
	}
};

//===========================================================================//

template<class T, class ...TailTypes>
struct ArgList<const T &, TailTypes...>
{
	ArgList() :
		Head(),
		Tail()
	{
	}

	ArgList(T head, TailTypes ... tail) :
		Head(head),
		Tail(tail...)
	{
	}

	void Reset()
	{
		Head = HeadType();
		Tail.Reset();
	}

	typedef T HeadType;
	typedef ArgList<TailTypes...> TailType;

	enum
	{
	    length = TailType::length + 1
	};

	T Head;
	TailType Tail;

	void AddParameter(integer index, const string & value)
	{
		if (index == 0)
		{
			Head = Convert<string, HeadType>(value);
		}
		else
		{
			Tail.AddParameter(index - 1, value);
		}
	}
};

//===========================================================================//

}

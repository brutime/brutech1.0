//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: шаблон ArgList представляет пару (Head, Tail), где Tail - либо
//              ArgList, либо отсутствует. Таким образом можно хранить список
//              значений различных типов. Есть возможность получать во время компиляции
//              значение и его тип по индексу в списке.
//    Важно:    значения аргументов сохраняются в самом списке, поэтому
//              передавать значения по ссылке не имеет смысла
//

#pragma once
#ifndef ARG_LIST_H
#define ARG_LIST_H

#include "../Common/Common.h"
#include "EmptyClass.h"

namespace Bru
{

//===========================================================================//

template<typename ... Types>
struct ArgList;

template<typename T>
struct ArgList<T> ;

template<typename T, typename ... TailTypes>
struct ArgList<T, TailTypes...> ;

//===========================================================================//

}// namespace

#include "CommandExecutor.h"
#include "ArgListChecker.h"

#include "ArgList.inl"

#endif // ARG_LIST_H

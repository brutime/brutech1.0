//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
ClassConstCommand<Class, Result, Types ...>::ClassConstCommand(const string & name, Class * object, Method method) :
	IScriptCommand(),
	_name(name),
	_object(object),
	_method(method),
	_args(),
	_argSet(),
	_argsValues(GetArgsCount())
{
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
ClassConstCommand<Class, Result, Types ...>::~ClassConstCommand()
{
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
IScriptCommand & ClassConstCommand<Class, Result, Types ...>::AddParameter(integer index, const string & value)
{
	_args.AddParameter(index, value);
	_argSet[index] = 1;
	return *this;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
IScriptCommand & ClassConstCommand<Class, Result, Types ...>::BoundParameterValues(integer index, const Array<string> & values)
{
	_argsValues[index] = values;
	return *this;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
CommandExecutionResult ClassConstCommand<Class, Result, Types ...>::Execute()
{
	CommandExecutionResult result;
	ArgListChecker::CheckArguments<Args::length>(_name, _argSet);
	try
	{
		CommandExecutor::ExecuteMethod(_object, _method, _args, result);
		ResetArguments();
	}
	catch (Exception & e)
	{
		result.ResultString = e.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
const string & ClassConstCommand<Class, Result, Types ...>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
integer ClassConstCommand<Class, Result, Types ...>::GetArgsCount() const
{
	return sizeof ... (Types);
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
const Array<string> & ClassConstCommand<Class, Result, Types ...>::GetBoundedValues(integer index) const
{
	return _argsValues[index];
}

//===========================================================================//

template<typename Class, typename Result, typename ... Types>
void ClassConstCommand<Class, Result, Types ...>::ResetArguments()
{
	_argSet.reset();
	_args.Reset();
}

//===========================================================================//

template<typename Class, typename Result>
ClassConstCommand<Class, Result>::ClassConstCommand(const string & name, Class * object, Method method) :
	IScriptCommand(),
	_name(name),
	_object(object),
	_method(method)
{
}

//===========================================================================//

template<typename Class, typename Result>
ClassConstCommand<Class, Result>::~ClassConstCommand()
{
}

//===========================================================================//

template<typename Class, typename Result>
IScriptCommand & ClassConstCommand<Class, Result>::AddParameter(integer, const string &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class, typename Result>
IScriptCommand & ClassConstCommand<Class, Result>::BoundParameterValues(integer, const Array<string> &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class, typename Result>
CommandExecutionResult ClassConstCommand<Class, Result>::Execute()
{
	CommandExecutionResult result;
	try
	{
		result.ResultString = Converter::ToString((_object->*_method)());
		result.ExecutedSuccessfully = true;
		result.ContainsValue = true;
	}
	catch (Exception & e)
	{
		result.ResultString = e.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Class, typename Result>
const string & ClassConstCommand<Class, Result>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Class, typename Result>
integer ClassConstCommand<Class, Result>::GetArgsCount() const
{
	return 0;
}

//===========================================================================//

template<typename Class, typename Result>
const Array<string> & ClassConstCommand<Class, Result>::GetBoundedValues(integer) const
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class>
ClassConstCommand<Class, void>::ClassConstCommand(const string & name, Class * object, Method method) :
	IScriptCommand(),
	_name(name),
	_object(object),
	_method(method)
{
}

//===========================================================================//

template<typename Class>
ClassConstCommand<Class, void>::~ClassConstCommand()
{
}

//===========================================================================//

template<typename Class>
IScriptCommand & ClassConstCommand<Class, void>::AddParameter(integer, const string &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class>
IScriptCommand & ClassConstCommand<Class, void>::BoundParameterValues(integer, const Array<string> &)
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

template<typename Class>
CommandExecutionResult ClassConstCommand<Class, void>::Execute()
{
	CommandExecutionResult result;
	try
	{
		(_object->*_method)();
		result.ExecutedSuccessfully = true;
	}
	catch (Exception & e)
	{
		result.ResultString = e.GetDetails();
	}
	return result;
}

//===========================================================================//

template<typename Class>
const string & ClassConstCommand<Class, void>::GetName() const
{
	return _name;
}

//===========================================================================//

template<typename Class>
integer ClassConstCommand<Class, void>::GetArgsCount() const
{
	return 0;
}

//===========================================================================//

template<typename Class>
const Array<string> & ClassConstCommand<Class, void>::GetBoundedValues(integer) const
{
	INVALID_ARGUMENT_EXCEPTION("Expects no parameters");
}

//===========================================================================//

} // namespace Bru

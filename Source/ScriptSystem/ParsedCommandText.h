//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: распарсенный текст команды
//

#pragma once
#ifndef PARSED_COMMAND_TEXT_H
#define PARSED_COMMAND_TEXT_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

struct ParsedCommandText
{
	ParsedCommandText();

	string CommandName;
	List<string> Args;
};

//===========================================================================//

}// namespace Bru

#endif // PARSED_COMMAND_TEXT_H

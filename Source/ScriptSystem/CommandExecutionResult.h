//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: структура, содержащая результат выполнения консольной команды
//

#pragma once
#ifndef COMMAND_EXECUTION_RESULT_H
#define COMMAND_EXECUTION_RESULT_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

struct CommandExecutionResult
{
	CommandExecutionResult() :
		ExecutedSuccessfully(false),
		ContainsValue(false),
		ResultString(string::Empty)
	{
	}

	bool ExecutedSuccessfully;
	bool ContainsValue;
	string ResultString;
};

//===========================================================================//

}// namespace

#endif // COMMAND_EXECUTION_RESULT_H

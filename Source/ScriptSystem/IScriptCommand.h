//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: интерфейс команды, которую можно вызвать, передав набор параметров
//

#pragma once
#ifndef I_SCRIPT_COMMAND_H
#define I_SCRIPT_COMMAND_H

#include "../Common/Common.h"
#include "CommandExecutionResult.h"

namespace Bru
{

//===========================================================================//

class IScriptCommand
{
public:
	virtual ~IScriptCommand();
	
	virtual IScriptCommand & AddParameter(integer index, const string & value) = 0;
	virtual IScriptCommand & BoundParameterValues(integer index, const Array<string> & values) = 0;

	
	virtual CommandExecutionResult Execute() = 0;
	
	virtual const string & GetName() const = 0;	
	virtual integer GetArgsCount() const = 0;
	virtual const Array<string> & GetBoundedValues(integer index) const = 0;

protected:
	IScriptCommand();

private:
	IScriptCommand(const IScriptCommand &) = delete;
	IScriptCommand & operator =(const IScriptCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // I_SCRIPT_COMMAND_H

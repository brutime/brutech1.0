//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс занимающийся разбором текстовых команд и выполнением скриптов
//

#pragma once
#ifndef SCRIPT_SYSTEM_H
#define SCRIPT_SYSTEM_H

#include "../Common/Common.h"
#include "IScriptCommand.h"
#include "StaticCommand.h"
#include "ClassCommand.h"
#include "ClassConstCommand.h"
#include "ParsedCommandText.h"
#include "Exceptions/AlreadyRegisteredException.h"
#include "Exceptions/CommandNotRegisteredException.h"
#include "Exceptions/InvalidFunctionArgumentException.h"

namespace Bru
{

//===========================================================================//

class ScriptSystemSingleton
{
public:
	ScriptSystemSingleton();
	~ScriptSystemSingleton();

	void RegisterCommand(const SharedPtr<IScriptCommand> & IScriptCommand);
	void UnregisterCommand(const string & commandName);
	bool ContainsCommand(const string & commandName) const;
	
	void EnableCommand(const string & commandName);
	void DisableCommand(const string & commandName);	
	bool IsCommandEnabled(const string & commandName) const;
	
	const SharedPtr<IScriptCommand> & GetCommand(const string & commandName, integer agrsCount) const;
	const SharedPtr<IScriptCommand> & GetCommand(const string & commandName) const;

	CommandExecutionResult Evaluate(const string & commandText) const;

	ParsedCommandText ParseCommandText(const string & commandText) const;

	bool IsCommandRegistered(const ParsedCommandText & parsedCommandText) const;
	bool IsCommandArgumentsCorrect(const ParsedCommandText & parsedCommandText) const;
	
	const TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> & GetRegisteredCommands() const;	
	const TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> & GetEnabledCommands() const;	
	const TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> & GetDisabledCommands() const;	

	List<string> GetEnabledCommandsList() const;

private:
	TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> _registeredCommands;	
	TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> _enabledCommands;
	TreeMap<string, TreeMap<integer, SharedPtr<IScriptCommand>>> _disabledCommands;

	ScriptSystemSingleton(const ScriptSystemSingleton &) = delete;
	ScriptSystemSingleton & operator =(const ScriptSystemSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<ScriptSystemSingleton> ScriptSystem;

//===========================================================================//

}// namespace

#endif // SCRIPT_SYSTEM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

namespace ArgListChecker
{

//===========================================================================//

template<integer argCount>
void CheckArguments(const string & commandName, const std::bitset<argCount> & args)
{
	// есть нестандартный метод all(), но для переносимости его не используем
	if (args.count() == argCount)
	{
		return;
	}

	for (integer i = 0; i < argCount; i++)
	{
		if (!args[i])
		{
			INVALID_FUNCTION_ARGUMENT_EXCEPTION(string::Format("Command \"{0}\", parameter {1} is invalid", commandName, i + 1));
		}
	}
}

//===========================================================================//

}// namespace ArgListCheckers

} // namespace Bru

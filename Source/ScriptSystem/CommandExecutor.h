//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: утилитарные шаблоны для вызова команд с параметрами ArgList<...>
//

#pragma once
#ifndef COMMAND_EXECUTOR_H
#define COMMAND_EXECUTOR_H

#include "../Common/Common.h"

namespace Bru
{

namespace CommandExecutor
{

//===========================================================================//

/// Используется для получения типа и значения аргумента в списке по его индексу (во время компиляции)
template<typename ArgList, integer Index>
class Get
{
public:
	typedef typename Get<typename ArgList::TailType, Index - 1>::Type Type;

	inline static typename Get<ArgList, Index>::Type & Value(ArgList & list);

private:
	Get(const Get &) = delete;
	Get & operator =(const Get &) = delete;
};

//===========================================================================//

template<typename ArgList>
class Get<ArgList, 0>
{
public:
	typedef typename ArgList::HeadType Type;

	inline static typename ArgList::HeadType & Value(ArgList & list);

private:
	Get(const Get &) = delete;
	Get & operator =(const Get &) = delete;
};

//===========================================================================//

/// Вспомогательные структуры для разворачивания ArgList<...> в список аргументов статической функции
template<int N>
class StaticUnrollHelper;

template<>
class StaticUnrollHelper<0> ;

//===========================================================================//

template<typename Result, typename ... ArgTypes>
inline void ExecuteStaticMethod(Result (*StaticMethod)(ArgTypes...), const ArgList<ArgTypes...> & args,
                                CommandExecutionResult & result);

template<typename ... ArgTypes>
inline void ExecuteStaticMethod(void (*StaticMethod)(ArgTypes...), const ArgList<ArgTypes...> & args,
                                CommandExecutionResult & result);

//===========================================================================//

/// Вспомогательные структуры для разворачивания ArgList<...> в список аргументов метода класса
template<int N>
struct UnrollHelper;

template<>
struct UnrollHelper<0> ;

template<typename Class, typename Result, typename ... ArgTypes>
inline void ExecuteMethod(Class * object, Result (Class::*Method)(ArgTypes...), const ArgList<ArgTypes...> & args,
                          CommandExecutionResult & result);

template<typename Class, typename Result, typename ... ArgTypes>
inline void ExecuteMethod(Class * object, Result (Class::*Method)(ArgTypes...) const, const ArgList<ArgTypes...> & args,
                          CommandExecutionResult & result);

template<typename Class, typename ... ArgTypes>
inline void ExecuteMethod(Class * object, void (Class::*Method)(ArgTypes...), const ArgList<ArgTypes...> & args,
                          CommandExecutionResult & result);

template<typename Class, typename ... ArgTypes>
inline void ExecuteMethod(Class * object, void (Class::*Method)(ArgTypes...) const, const ArgList<ArgTypes...> & args,
                          CommandExecutionResult & result);

//===========================================================================//

}// namespace CommandExecutor

} // namespace Bru

#include "CommandExecutor.inl"

#endif // COMMAND_EXECUTOR_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TextFileStream.h"

#include "FileSystem.h"

namespace Bru
{

//===========================================================================//

TextFileStream::TextFileStream(const string & pathToFile, FileAccess access) :
	BaseFileStream(pathToFile, access, FileType::Text)
{
	ReadBOM();
}

//===========================================================================//

TextFileStream::~TextFileStream()
{
}

//===========================================================================//

byte TextFileStream::ReadByte()
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

integer TextFileStream::ReadInteger()
{
	return ReadValue<integer>();
}

//===========================================================================//

integer TextFileStream::ReadInteger(integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

float TextFileStream::ReadFloat()
{
	return ReadValue<float>();
}

//===========================================================================//

float TextFileStream::ReadFloat(integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

Array<byte>::SimpleType TextFileStream::ReadByteArray(integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::ReadByteArray(byte *, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::Write(byte)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::WriteLine(byte)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::Write(byte, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::WriteLine(byte, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::Write(integer someInteger)
{
	WriteValue<integer>(someInteger);
}

//===========================================================================//

void TextFileStream::WriteLine(integer someInteger)
{
	Write(someInteger);
	WriteEndOfLine();
}

//===========================================================================//

void TextFileStream::Write(integer, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::WriteLine(integer, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::Write(float someFloat)
{
	WriteValue<float>(someFloat);
}

//===========================================================================//

void TextFileStream::WriteLine(float someFloat)
{
	Write(someFloat);
	WriteEndOfLine();
}

//===========================================================================//

void TextFileStream::Write(float, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::WriteLine(float, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::Write(const string & someString)
{
	WriteValue<string>(someString);
}

//===========================================================================//

void TextFileStream::WriteLine(const string & someString)
{
	Write(someString);
	WriteEndOfLine();
}

//===========================================================================//

void TextFileStream::Write(const Array<byte>::SimpleType &)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::WriteLine(const Array<byte>::SimpleType &)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::Write(byte * const, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::WriteLine(byte * const, integer)
{
	INVALID_STATE_EXCEPTION(string::Format("Not supported in text file: {0}", _pathToFile));
}

//===========================================================================//

void TextFileStream::Reset()
{
	BaseFileStream::Reset();
	ReadBOM();
}

//===========================================================================//

//В UTF-8 файлах первые 3 байта могут содержать BOM (Byte order mark), они нам в тексте не нужны
//Поэтому считываем их, чтобы передвинуть курсор файла на его реальное начало
void TextFileStream::ReadBOM()
{
	char bomBuffer[3] = { 0, 0, 0 };

	std::fstream bomStream;
	bomStream.open(_pathToFile, GetFileOpeningMode(FileAccess::Read, FileType::Binary));
	bomStream.read(bomBuffer, 3);
	bomStream.close();

	//Если это BOM, перемещаем положение курсора
	if (bomBuffer[0] == -17 && bomBuffer[1] == -69 && bomBuffer[2] == -65)
	{
		Seek(3, SeekOrigin::Begin);
	}
}

//===========================================================================//

template<typename T>
T TextFileStream::ReadValue()
{
	CheckForRead();

	T value;
	_stream >> value;

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Reading error. Sad, but true. File: {0}", _pathToFile));
	}

	return value;
}

//===========================================================================//

template<typename T>
void TextFileStream::WriteValue(const T & value)
{
	CheckForWrite();

	_stream << value;

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Writing error. Sad, but true. File: {0}", _pathToFile));
	}
}

//===========================================================================//

} // namespace Bru

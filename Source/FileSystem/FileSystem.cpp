﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FileSystem.h"

#include <fstream>
#include <dirent.h>

#if PLATFORM == WINDOWS
#include <windows.h>
#endif

#include "../Common/Exceptions/NotImplementedException.h"
#include "Exceptions/IOException.h"

namespace Bru
{

//===========================================================================//

bool FileSystem::FileExists(const string & pathToFile)
{
	return std::fstream(pathToFile, std::fstream::in) != NullPtr;
}

//===========================================================================//

void FileSystem::RemoveFile(const string & pathToFile)
{
	std::remove(pathToFile);
}

//===========================================================================//

Array<string> FileSystem::Dir(const string & path, bool includeFiles, bool includeSubdirs)
{
	Array<string> result;

#if PLATFORM == WINDOWS
	HANDLE hFind;
	WIN32_FIND_DATA findFileData;

	if((hFind = FindFirstFile((path + "*.*").ToChars(), &findFileData)) != INVALID_HANDLE_VALUE)
	{
		do
		{
			if ( ((includeSubdirs && findFileData.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY) ||
			        (includeFiles && findFileData.dwFileAttributes != FILE_ATTRIBUTE_DIRECTORY)) &&
			        string(findFileData.cFileName)[0] != "." )
			{
				result.Add(findFileData.cFileName);
			}
		}
		while(FindNextFile(hFind, &findFileData));
		FindClose(hFind);
	}
#endif

	return result;
}

//===========================================================================//

} // namespace Bru

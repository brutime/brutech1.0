//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BinaryFileStream.h"

#include "FileSystem.h"

namespace Bru
{

//===========================================================================//

BinaryFileStream::BinaryFileStream(const string & pathToFile, FileAccess access) :
	BaseFileStream(pathToFile, access, FileType::Binary)
{
}

//===========================================================================//

BinaryFileStream::~BinaryFileStream()
{
}

//===========================================================================//

byte BinaryFileStream::ReadByte()
{
	return ReadValue<byte>();
}

//===========================================================================//

integer BinaryFileStream::ReadInteger()
{
	return ReadValue<integer>();
}

//===========================================================================//

integer BinaryFileStream::ReadInteger(integer bytesCount)
{
#ifdef DEBUGGING
	if (bytesCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("bytesCount must be >= 0, but it is {0}", bytesCount));
	}
#endif

	return BitConverter::ToInteger(ReadByteArray(bytesCount));
}

//===========================================================================//

float BinaryFileStream::ReadFloat()
{
	return ReadValue<float>();
}

//===========================================================================//

float BinaryFileStream::ReadFloat(integer bytesCount)
{
#ifdef DEBUGGING
	if (bytesCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("bytesCount must be >= 0, but it is {0}", bytesCount));
	}
#endif

	return BitConverter::ToFloat(ReadByteArray(bytesCount));
}

//===========================================================================//

Array<byte>::SimpleType BinaryFileStream::ReadByteArray(integer count)
{
	Array<byte>::SimpleType byteArray(count);
	ReadByteArray(byteArray.GetData(), count);
	return byteArray;
}

//===========================================================================//

void BinaryFileStream::ReadByteArray(byte * byteArray, integer count)
{
#ifdef DEBUGGING
	if (count < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("count must be >= 0, but it is {0}", count));
	}
#endif

	CheckForRead();

	_stream.read(reinterpret_cast<char *>(byteArray), count);

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Reading error. Sad, but true. File: {0}", _pathToFile));
	}
}

//===========================================================================//

void BinaryFileStream::Write(byte someByte)
{
	WriteValue<byte>(someByte);
}

//===========================================================================//

void BinaryFileStream::WriteLine(byte someByte)
{
	Write(someByte);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(byte someByte, integer repeatCount)
{
#ifdef DEBUGGING
	if (repeatCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("repeatCount must be >= 0, but it is {0}", repeatCount));
	}
#endif

	for (integer i = 0; i < repeatCount; i++)
	{
		WriteValue<byte>(someByte);
	}
}

//===========================================================================//

void BinaryFileStream::WriteLine(byte someByte, integer repeatCount)
{
	Write(someByte, repeatCount);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(integer someInteger)
{
	WriteValue<integer>(someInteger);
}

//===========================================================================//

void BinaryFileStream::WriteLine(integer someInteger)
{
	Write(someInteger);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(integer someInteger, integer bytesCount)
{
#ifdef DEBUGGING
	if (bytesCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("bytesCount must be >= 0, but it is {0}", bytesCount));
	}
#endif

	WriteValue<integer>(someInteger, bytesCount);
}

//===========================================================================//

void BinaryFileStream::WriteLine(integer someInteger, integer bytesCount)
{
	Write(someInteger, bytesCount);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(float someFloat)
{
	WriteValue<float>(someFloat);
}

//===========================================================================//

void BinaryFileStream::WriteLine(float someFloat)
{
	Write(someFloat);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(float someFloat, integer bytesCount)
{
#ifdef DEBUGGING
	if (bytesCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("bytesCount must be >= 0, but it is {0}", bytesCount));
	}
#endif

	WriteValue<float>(someFloat, bytesCount);
}

//===========================================================================//

void BinaryFileStream::WriteLine(float someFloat, integer bytesCount)
{
	Write(someFloat, bytesCount);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(const string & someString)
{
	WriteArray(someString.ToChars(), someString.GetLengthInBytes());
}

//===========================================================================//

void BinaryFileStream::WriteLine(const string & someString)
{
	Write(someString);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(const Array<byte>::SimpleType & byteArray)
{
	WriteArray(reinterpret_cast<const char *>(byteArray.GetData()), byteArray.GetCount());
}

//===========================================================================//

void BinaryFileStream::WriteLine(const Array<byte>::SimpleType & byteArray)
{
	Write(byteArray);
	WriteEndOfLine();
}

//===========================================================================//

void BinaryFileStream::Write(byte * const byteArray, integer count)
{
	WriteArray(reinterpret_cast<const char *>(byteArray), count);
}

//===========================================================================//

void BinaryFileStream::WriteLine(byte * const byteArray, integer count)
{
	Write(byteArray, count);
	WriteEndOfLine();	
}

//===========================================================================//

template<typename T>
T BinaryFileStream::ReadValue()
{
	CheckForRead();

	T value;
	_stream.read(reinterpret_cast<char *>(&value), sizeof(T));

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Reading error. Sad, but true. File: {0}", _pathToFile));
	}

	return value;
}

//===========================================================================//

template<typename T>
void BinaryFileStream::WriteValue(const T & value)
{
	CheckForWrite();

	_stream.write(reinterpret_cast<const char *>(&value), sizeof(T));

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Writing error. Sad, but true. File: {0}", _pathToFile));
	}
}

//===========================================================================//

template<typename T>
void BinaryFileStream::WriteValue(const T & value, integer bytesCount)
{
	CheckForWrite();

	Write(BitConverter::ToBytes(value, bytesCount));
}

//===========================================================================//

void BinaryFileStream::WriteArray(const char * array, integer bytesCount)
{
	CheckForWrite();

	_stream.write(array, bytesCount);

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Writing error. Sad, but true. File: {0}", _pathToFile));
	}
}

//===========================================================================//

} // namespace Bru

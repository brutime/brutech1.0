//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Поток для работы с файлами
//

#pragma once
#ifndef TEXT_FILE_STREAM_H
#define TEXT_FILE_STREAM_H

#include "BaseFileStream.h"

namespace Bru
{

//===========================================================================//

class TextFileStream : public BaseFileStream
{
public:
	TextFileStream(const string & pathToFile, FileAccess access);
	~TextFileStream();

	virtual byte ReadByte();
	virtual integer ReadInteger();
	///В текстовом файле, количество байт не учитывается
	virtual integer ReadInteger(integer bytesCount);
	virtual float ReadFloat();
	///В текстовом файле, количество байт не учитывается
	virtual float ReadFloat(integer bytesCount);
	virtual Array<byte>::SimpleType ReadByteArray(integer count);
	virtual void ReadByteArray(byte * byteArray, integer count);

	virtual void Write(byte someByte);
	virtual void WriteLine(byte someByte);

	virtual void Write(byte someByte, integer repeatCount);
	virtual void WriteLine(byte someByte, integer repeatCount);

	virtual void Write(integer someInteger);
	virtual void WriteLine(integer someInteger);

	///В текстовом файле, количество байт не учитывается
	virtual void Write(integer someInteger, integer bytesCount);
	virtual void WriteLine(integer someInteger, integer bytesCount);

	virtual void Write(float someFloat);
	virtual void WriteLine(float someFloat);

	///В текстовом файле, количество байт не учитывается
	virtual void Write(float someFloat, integer bytesCount);
	virtual void WriteLine(float someFloat, integer bytesCount);

	virtual void Write(const string & someString);
	virtual void WriteLine(const string & someString);

	virtual void Write(const Array<byte>::SimpleType & byteArray);
	virtual void WriteLine(const Array<byte>::SimpleType & byteArray);
	
	virtual void Write(byte * const byteArray, integer count);
	virtual void WriteLine(byte * const byteArray, integer count);	

	virtual void Reset();

private:
	template<typename T> T ReadValue();
	template<typename T> void WriteValue(const T & value);

	void ReadBOM();

	TextFileStream(const TextFileStream &) = delete;
	TextFileStream & operator =(const TextFileStream &) = delete;

	friend class TestFileStreamBinaryConstructor;
	friend class TestFileStreamTextConstructor;
};

//===========================================================================//

} // namespace Bru

#endif // TEXT_FILE_STREAM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseFileStream.h"

#include "FileSystem.h"

namespace Bru
{

//===========================================================================//

BaseFileStream::BaseFileStream(const string & pathToFile, FileAccess access, FileType type) :
	IInputOutputStream(),
	_stream(),
	_pathToFile(pathToFile),
	_access(access)
{
	if (_access == FileAccess::Read || _access == FileAccess::Append || _access == FileAccess::ReadWrite)
	{
		if (!FileSystem::FileExists(_pathToFile))
		{
			FILE_NOT_FOUND_EXCEPTION(string::Format("File not found: {0}", _pathToFile));
		}
	}

	_stream.open(_pathToFile.ToChars(), GetFileOpeningMode(_access, type));

	if (!IsOpened())
	{
		IO_EXCEPTION(string::Format("Couldn't open file: {0}", _pathToFile));
	}
}

//===========================================================================//

BaseFileStream::~BaseFileStream()
{
	if (IsOpened())
	{
		Close();
	}
}

//===========================================================================//

string BaseFileStream::ReadString(integer bytesCount)
{
#ifdef DEBUGGING
	if (bytesCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("bytesCount must be >= 0, but it is {0}", bytesCount));
	}
#endif

	CheckForRead();

	Array<char>::SimpleType charsArray(bytesCount + 1);
	_stream.read(charsArray.GetData(), bytesCount);

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Reading error. Sad, but true. File: {0}", _pathToFile));
	}

	charsArray[bytesCount] = NullTerminator;

	return string(charsArray.GetData(), charsArray.GetCount() - 1);
}

//===========================================================================//

string BaseFileStream::ReadLine()
{
	CheckForRead();

	Array<char>::SimpleType charsArray;

	if (IsEOF())
	{
		IO_EXCEPTION(string::Format("Can't read line: stream is over. File: {0}", _pathToFile));
	}

	char currentChar;
	bool isStopped = false;

	do
	{
		_stream.read(&currentChar, 1);

		if (_stream.fail() || _stream.bad())
		{
			IO_EXCEPTION(string::Format("Reading error. Sad, but true. File: {0}", _pathToFile));
		}

		switch (currentChar)
		{
		case '\n':
		case '\r':
			// no need to read pairing symbol because stream do it by himself
			isStopped = true;
			break;

		default:
			charsArray.Add(currentChar);
			break;
		}

	}
	while (!(isStopped || IsEOF()));

	if (charsArray.GetCount() == 0)
	{
		return string();
	}
	else
	{
		return string(charsArray.GetData(), charsArray.GetCount());
	}
}

//===========================================================================//

void BaseFileStream::WriteEndOfLine()
{
	CheckForWrite();

	char endOfLine = '\n';
	_stream.write(&endOfLine, 1);

	if (_stream.fail() || _stream.bad())
	{
		IO_EXCEPTION(string::Format("Writing error. Sad, but true. File: {0}", _pathToFile));
	}
}

//===========================================================================//

void BaseFileStream::Flush()
{
	_stream.flush();
}

//===========================================================================//

void BaseFileStream::Seek(integer offset, SeekOrigin seekOrigin)
{
	_stream.seekg(offset, GetConvertedSeekOrigin(seekOrigin));
}

//===========================================================================//

void BaseFileStream::Reset()
{
	Seek(0, SeekOrigin::Begin);
}

//===========================================================================//

void BaseFileStream::Close()
{
	if (!IsOpened())
	{
		INVALID_STATE_EXCEPTION("File not opened");
	}

	_stream.close();
}

//===========================================================================//

bool BaseFileStream::IsEOF() const
{
	return _stream.peek() == EOF;
}

//===========================================================================//

bool BaseFileStream::IsOpened() const
{
	return _stream.is_open();
}

//===========================================================================//

const string & BaseFileStream::GetPathToFile() const
{
	return _pathToFile;
}

//===========================================================================//

std::fstream::ios_base::openmode BaseFileStream::GetFileOpeningMode(FileAccess access, FileType type) const
{
	std::fstream::ios_base::openmode mode;
	switch (access)
	{
	case FileAccess::Read:
		mode = std::fstream::in;
		break;

	case FileAccess::Write:
		mode = std::fstream::out;
		break;

	case FileAccess::Append:
		mode = std::fstream::app | std::fstream::out;
		break;

	case FileAccess::ReadWrite:
		mode = std::fstream::in | std::fstream::out;
		break;

	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined access");
	}

	if (type == FileType::Binary)
	{
		mode |= std::fstream::binary;
	}

	return mode;
}

//===========================================================================//

void BaseFileStream::CheckForRead() const
{
	if (!IsOpened())
	{
		INVALID_STATE_EXCEPTION(string::Format("File not opened: \"{0}\"", _pathToFile));
	}

	if (_access == FileAccess::Write)
	{
		INVALID_STATE_EXCEPTION(string::Format("File opened for read: \"{0}\"", _pathToFile));
	}

	if (_access == FileAccess::Append)
	{
		INVALID_STATE_EXCEPTION(string::Format("File opened for append: \"{0}\"", _pathToFile));
	}

	if (IsEOF())
	{
		IO_EXCEPTION(string::Format("End of file: \"{0}\"", _pathToFile));
	}
}

//===========================================================================//

void BaseFileStream::CheckForWrite() const
{
	if (!IsOpened())
	{
		INVALID_STATE_EXCEPTION(string::Format("File not opened: \"{0}\"", _pathToFile));
	}

	if (_access == FileAccess::Read)
	{
		INVALID_STATE_EXCEPTION(string::Format("File opened for read: \"{0}\"", _pathToFile));
	}
}

//===========================================================================//

std::ios_base::seekdir BaseFileStream::GetConvertedSeekOrigin(SeekOrigin seekOrigin)
{
	switch (seekOrigin)
	{
	case SeekOrigin::Begin:
		return std::ios_base::beg;
	case SeekOrigin::Current:
		return std::ios_base::cur;
	case SeekOrigin::End:
		return std::ios_base::end;

	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined seekOrigin");
	}
}

//===========================================================================//

} // namespace Bru

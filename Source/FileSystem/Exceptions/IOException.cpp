//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "IOException.h"

namespace Bru
{

//===========================================================================//

IOException::IOException(const string & defails, const string & fileName, const string & methodName, integer fileLine) :
	Exception("IO exception", defails, fileName, methodName, fileLine)
{
}

//===========================================================================//

IOException::~IOException()
{
}

//===========================================================================//

}//namespace

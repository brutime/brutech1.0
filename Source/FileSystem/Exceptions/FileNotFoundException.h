//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - файл не найден
//

#pragma once
#ifndef FILE_NOT_FOUND_EXCEPTION_H
#define FILE_NOT_FOUND_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class FileNotFoundException : public Exception
{
public:
	FileNotFoundException(const string & defails, const string & fileName, const string & methodName, integer fileLine);
	virtual ~FileNotFoundException();
};

//===========================================================================//

#define FILE_NOT_FOUND_EXCEPTION(details) \
    throw FileNotFoundException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // FILE_NOT_FOUND_EXCEPTION_H

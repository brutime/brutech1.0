//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - неисправимые ошибках ввода-вывода
//

#pragma once
#ifndef IO_EXCEPTION_H
#define IO_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class IOException : public Exception
{
public:
	IOException(const string & defails, const string & fileName, const string & methodName, integer fileLine);
	virtual ~IOException();
};

//===========================================================================//

#define IO_EXCEPTION(details) \
    throw IOException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // IO_EXCEPTION_H

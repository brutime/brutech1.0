//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тип доступа к файлу
//

#pragma once
#ifndef FILE_ACCESS_H
#define FILE_ACCESS_H

namespace Bru
{

//===========================================================================//

enum class FileAccess
{
    Read,
    Write,
    Append,
    ReadWrite
};

//===========================================================================//

} // namespace Bru

#endif // FILE_ACCESS_H

﻿//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: интерфейс файловой системы
//

#pragma once
#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H

#include "../Common/Common.h"
#include "Enums/FileAccess.h"
#include "TextFileStream.h"
#include "BinaryFileStream.h"
#include "Exceptions/FileNotFoundException.h"
#include "Exceptions/IOException.h"

namespace Bru
{

//===========================================================================//

class FileSystem
{
public:
	static bool FileExists(const string & pathToFile);
	static void RemoveFile(const string & pathToFile);
	static Array<string> Dir(const string & path, bool includeFiles = true, bool includeSubdirs = true);

	static Array<string> GetFilesList(const string & path, const string & filePattern,
	                                  bool isIncludeFiles = true, bool isIncludeSubdirectories = true);
	static Array<string> GetFilesListRecursively(const string & path, const string & filePattern = "*.*");

private:
	static Array<string> GetFilesListRecursively(const string & path, const string & localPath, const string & filePattern);

	FileSystem() = delete;
	FileSystem(const FileSystem &) = delete;
	~FileSystem() = delete;
	FileSystem & operator =(const FileSystem &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // FILE_SYSTEM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Абстрактный файловый поток
//

#pragma once
#ifndef BASE_FILE_STREAM_H
#define BASE_FILE_STREAM_H

#include <fstream>
#include "../Common/Common.h"
#include "Enums/FileAccess.h"
#include "Enums/FileType.h"

namespace Bru
{

//===========================================================================//

class BaseFileStream : public IInputOutputStream
{
public:
	BaseFileStream(const string & pathToFile, FileAccess access, FileType type);
	~BaseFileStream();

	virtual string ReadString(integer bytesCount);
	virtual string ReadLine();

	virtual void WriteEndOfLine();

	virtual void Flush();

	virtual void Seek(integer offset, SeekOrigin seekOrigin = SeekOrigin::Current);
	virtual void Reset();

	virtual void Close();

	bool IsEOF() const;

	bool IsOpened() const;
	const string & GetPathToFile() const;

protected:
	std::fstream::ios_base::openmode GetFileOpeningMode(FileAccess access, FileType type) const;

	void CheckForRead() const;
	void CheckForWrite() const;

	mutable std::fstream _stream; //mutable, чтобы можно было сделать const EOF
	string _pathToFile;
	FileAccess _access;

private:
	static std::ios_base::seekdir GetConvertedSeekOrigin(SeekOrigin seekOrigin);

	BaseFileStream(const BaseFileStream &) = delete;
	BaseFileStream & operator =(const BaseFileStream &) = delete;

	friend class TestBinaryFileStreamConstructor;
	friend class TestTextFileStreamConstructor;
};

//===========================================================================//

} // namespace Bru

#endif // BASE_FILE_STREAM_H

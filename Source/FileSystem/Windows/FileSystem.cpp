//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../FileSystem.h"

#include <windows.h>

#include "../../Common/Exceptions/NotImplementedException.h"
#include "../Exceptions/IOException.h"

namespace Bru
{

//===========================================================================//

Array<string> FileSystem::GetFilesList(const string & path, const string & filePattern,
                                       bool isIncludeFiles, bool isIncludeSubdirectories)
{
	Array<string> result;

	WIN32_FIND_DATA findFileData;
	HANDLE hFind = FindFirstFile((path + filePattern).ToChars(), &findFileData);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (((isIncludeSubdirectories && (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) ||
			        (isIncludeFiles && !(findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))) &&
			        string(findFileData.cFileName)[0] != ".")
			{
				result.Add(findFileData.cFileName);
			}
		}
		while (FindNextFile(hFind, &findFileData));
		FindClose(hFind);
	}

	return result;
}

//===========================================================================//

Array<string> FileSystem::GetFilesListRecursively(const string & path, const string & filePattern)
{
	return GetFilesListRecursively(path, string::Empty, filePattern);
}

//===========================================================================//

Array<string> FileSystem::GetFilesListRecursively(const string & path, const string & localPath, const string & filePattern)
{	
	Array<string> directories = GetFilesList(path + localPath, "*.*", false, true);

	Array<string> result;
	for (const string & directory : directories)
	{
		result.Add(GetFilesListRecursively(path, localPath + directory + "/", filePattern));
	}

	result.Add(GetFilesList(path + localPath, filePattern, true, false));
	for (string & fileName : result)
	{
		fileName = localPath + fileName;
	}	

	return result;
}

//===========================================================================//

} // namespace Bru

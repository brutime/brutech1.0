//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: слой с renderable объектами
//

#pragma once
#ifndef RENDERABLE_LAYER_H
#define RENDERABLE_LAYER_H

#include "../Common/Common.h"
#include "Enums/ProjectionMode.h"
#include "Renderable.h"
#include "LayerEventArgs.h"

namespace Bru
{

//===========================================================================//

class RenderableLayer : public PtrFromThisMixIn<RenderableLayer>
{
public:
	RenderableLayer(const string & name, ProjectionMode projectionMode = ProjectionMode::Perspective);
	~RenderableLayer();

	void AddRenderable(Renderable * const renderable);
	void RemoveRenderable(Renderable * const renderable);

	const string & GetName() const;
	ProjectionMode GetProjectionMode() const;

	void SetVisible(bool isVisible);
	bool IsVisible() const;

	void Show();
	void Hide();

	const List<Renderable * const> & GetRenderables() const;

	void PrintRenderables() const;

	bool IsEmpty() const;

private:
	string GetComponentData(Renderable * const component) const;
	
	string GetVisibleString(bool isVisible) const;
	string GetProjectionModeString(ProjectionMode projectionMode) const;	

	string _name;
	ProjectionMode _projectionMode;
	bool _isVisible;
	List<Renderable * const> _renderables;

protected:
	InnerEvent<const LayerEventArgs &> _visibleChangedEvent;

public:
	Event<const LayerEventArgs &> VisibleChangedEvent;

private:
	RenderableLayer(const RenderableLayer &) = delete;
	RenderableLayer & operator =(const RenderableLayer &) = delete;
};

//===========================================================================//

} //namespace Bru

#endif // RENDERABLE_LAYER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: набор состояний, в который необходимо установить рендерер,
//              чтобы корректно нарисовать объект
//

#pragma once
#ifndef RENDERING_ATTRIBUTES_H
#define RENDERING_ATTRIBUTES_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "../Graphics/Enums/BlendFunction.h"
#include "../Graphics/Enums/DepthFunction.h"

namespace Bru
{

//===========================================================================//

class RenderingAttributes
{
	friend class Renderable;

public:
	RenderingAttributes();
	~RenderingAttributes();

	bool operator <(const RenderingAttributes & attributes) const;
	bool operator ==(const RenderingAttributes & attributes) const;
	bool operator !=(const RenderingAttributes & attributes) const;

	void UseDepthTest(bool useDepthTest);
	bool UsesDepthTest() const;

	void SetDepthFunction(DepthFunction depthFunction);
	DepthFunction GetDepthFunction() const;

	bool UsesBlending() const;

	void SetBlendFunctions(BlendFunction sourceBlendFunction, BlendFunction destinationBlendFunction);

	void SetSourceBlendFunction(BlendFunction sourceBlendFunction);
	BlendFunction GetSourceBlendFunction() const;

	void SetDestinationBlendFunction(BlendFunction destinationBlendFunction);
	BlendFunction GetDestinationBlendFunction() const;

	bool Uses2DTexture() const;

	bool UsesScissorsTest() const;

	const IntRectangle & GetScissorsRectangle() const;

protected:
	bool IsBlendFunctionsNeedsBlending() const;

	void UseBlending(bool useBlending);
	void Use2DTexture(bool use2DTexture);

	void UseScissorsTest(bool useScissorsTest);
	void SetScissorsRectangle(const IntRectangle & scissorsRectangle);

	bool _usesDepthTest;
	DepthFunction _depthFunction;

	bool _usesBlending;
	BlendFunction _sourceBlendFunction;
	BlendFunction _destinationBlendFunction;

	bool _uses2DTexture;

	bool _usesScissorsTest;
	IntRectangle _scissorsRectangle;

protected:
	InnerEvent<const EmptyEventArgs &> _blendFunctionChangedEvent;

public:
	Event<const EmptyEventArgs &> BlendFunctionChanged;

private:
	RenderingAttributes(const RenderingAttributes &) = delete;
	RenderingAttributes & operator =(const RenderingAttributes &) = delete;
};

//===========================================================================//

}//namespace

#endif // RENDERING_ATTRIBUTES_H

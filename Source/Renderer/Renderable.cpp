//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Renderable.h"

#include "../Utils/Console/Console.h"
#include "../Positioning/PositioningNode.h"
#include "../Positioning/RotationNode.h"
#include "Renderer.h"
#include "RenderableLayer.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(Renderable, EntityComponent)

//===========================================================================//

const float Renderable::DefaultLineThickness = 1.0f;

//===========================================================================//

Renderable::Renderable(const string & domainName, const string & layerName, VerticesMode verticesMode) :
	EntityComponent(domainName),
	_mesh(new Mesh(verticesMode)),
	_worldVertices(),
	_origin(),
	_color(),
	_texture(),
	_renderingAttributes(new RenderingAttributes()),
	_boundingSphere(new BoundingSphere()),
	_originFactor(),
	_layer(layerName != string::Empty ? Renderer->GetLayer(GetDomainName(), layerName) : NullPtr),
	_renderingOrder(0.0f),
	_lineThickness(0.0f),
	_isVisible(true),
	_positioningNode(),
	_transformationChangedHandler(new EventHandler<const EmptyEventArgs &>(this, &Renderable::OnTransformationChanged)),
	_blendFunctionChangedHandler(new EventHandler<const EmptyEventArgs &>(this, &Renderable::OnBlendFunctionChanged)),
	_renderingOrderChangedEvent(),
	_visibleChangedEvent(),
	_originFactorChangedEvent(),
	RenderingOrderChangedEvent(_renderingOrderChangedEvent),
	VisibleChangedEvent(_visibleChangedEvent),
	OriginFactorChangedEvent(_originFactorChangedEvent)
{
	_renderingAttributes->BlendFunctionChanged.AddHandler(_blendFunctionChangedHandler);
	if (layerName != string::Empty)
	{
		Register();
	}
}

//===========================================================================//

Renderable::~Renderable()
{
	if (_positioningNode != NullPtr)
	{
		_positioningNode->TransformationChanged.RemoveHandler(_transformationChangedHandler);
	}

	_renderingAttributes->BlendFunctionChanged.RemoveHandler(_blendFunctionChangedHandler);
	if (IsRegistred())
	{
		Unregister();
	}
}

//===========================================================================//

const SharedPtr<Mesh> & Renderable::GetMesh() const
{
	return _mesh;
}

//===========================================================================//

const Array<Vector3>::SimpleType & Renderable::GetWorldVertices() const
{
	return _worldVertices;
}

//===========================================================================//

void Renderable::SetTexture(const WeakPtr<Texture> & texture)
{
	InternalSetTexture(texture);
	UpdateMeshTextureCoords();
}

//===========================================================================//

const WeakPtr<Texture> & Renderable::GetTexture() const
{
	return _texture;
}

//===========================================================================//

void Renderable::SetColor(const Color & color)
{
	InternalSetColor(color);
	_mesh->SetVerticesColor(color);	
}

//===========================================================================//

const Color & Renderable::GetColor() const
{
	return _color;
}

//===========================================================================//

void Renderable::SetColorAlpha(float alpha)
{
	_color.SetAlpha(alpha);
	_mesh->SetVerticesColorAlpha(_color.GetAlpha());		
	UpdateUsesBlending();
}

//===========================================================================//

float Renderable::GetColorAlpha() const
{
	return _color.GetAlpha();
}

//===========================================================================//

const SharedPtr<RenderingAttributes> & Renderable::GetRenderingAttributes() const
{
	return _renderingAttributes;
}

//===========================================================================//

void Renderable::SetLayer(const string & layerName, bool forceRegistred)
{
	SharedPtr<RenderableLayer> layer = Renderer->GetLayer(GetDomainName(), layerName);
	if (layer == _layer)
	{
		return;
	}

	bool wasRegistred = false;
	if (IsRegistred())
	{
		Unregister();
		wasRegistred = true;
	}

	_layer = layer;

	if (forceRegistred || wasRegistred)
	{
		Register();
	}
}

//===========================================================================//

const SharedPtr<RenderableLayer> & Renderable::GetLayer() const
{
	return _layer;
}

//===========================================================================//

SharedPtr<BoundingSphere> Renderable::GetBoundingSphere()
{
	return _boundingSphere;
}

//===========================================================================//

void Renderable::SetOriginFactor(const Vector3 & originFactor)
{
	InternalSetOriginFactor(originFactor);
	UpdateMeshVerticesPosition();
	_originFactorChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

const Vector3 & Renderable::GetOriginFactor() const
{
	return _originFactor;
}

//===========================================================================//

const Vector3 & Renderable::GetOrigin() const
{
	return _origin;
}

//===========================================================================//

void Renderable::SetRenderingOrder(float renderingOrder)
{
	if (Math::Equals(_renderingOrder, renderingOrder))
	{
		return;
	}

	_renderingOrder = renderingOrder;
	if (IsRegistred())
	{
		Unregister();
		Register();
	}

	_renderingOrderChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

float Renderable::GetRenderingOrder() const
{
	return _renderingOrder;
}

//===========================================================================//

void Renderable::SetLineThickness(float lineThickness)
{
	_lineThickness = lineThickness;
}

//===========================================================================//

float Renderable::GetLineThickness() const
{
	return _lineThickness;
}

//===========================================================================//

void Renderable::SetPositioningNode(const WeakPtr<BasePositioningNode> & positioningNode)
{
	if (_positioningNode != NullPtr)
	{
		_positioningNode->TransformationChanged.RemoveHandler(_transformationChangedHandler);
	}

	_positioningNode = positioningNode;

	if (_positioningNode != NullPtr)
	{
		_positioningNode->TransformationChanged.AddHandler(_transformationChangedHandler);
		OnTransformationChanged(EmptyEventArgs());
	}
}

//===========================================================================//

const SharedPtr<BasePositioningNode> & Renderable::GetPositioningNode() const
{
	return _positioningNode;
}

//===========================================================================//

void Renderable::SetVisible(bool isVisible)
{
	_isVisible = isVisible;
	_visibleChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

bool Renderable::IsVisible() const
{
	return _layer->IsVisible() && _isVisible;
}

//===========================================================================//

void Renderable::Show()
{
	SetVisible(true);
}

//===========================================================================//

void Renderable::Hide()
{
	SetVisible(false);
}

//===========================================================================//

void Renderable::Register()
{
	_layer->AddRenderable(this);
	EntityComponent::Register();
}

//===========================================================================//

void Renderable::Unregister()
{
	_layer->RemoveRenderable(this);
	EntityComponent::Unregister();
}

//===========================================================================//

void Renderable::OnAddedToOwner()
{
	if (GetOwner()->ContainsComponent(RotationNode::TypeID))
	{
		SetPositioningNode(GetOwnerComponent<RotationNode>());
	}
	else
	{
		SetPositioningNode(GetOwnerComponent<PositioningNode>());
	}
}

//===========================================================================//

void Renderable::OnRemovingFromOwner()
{
	SetPositioningNode(NullPtr);
}

//===========================================================================//

void Renderable::UpdateWorldVertices()
{
	const auto & vertices = _mesh->GetVertices();
	if (_worldVertices.GetCount() != vertices.GetCount())
	{
		_worldVertices.Resize(vertices.GetCount());
	}
	if (_positioningNode != NullPtr)
	{
		const auto & matrix = _positioningNode->GetWorldTransformation().GetMatrix();
		for (integer i = 0; i < vertices.GetCount(); ++i)
		{
			_worldVertices[i] = vertices[i].Position.MultiplyByMatrix4x4AsPoint(matrix);
		}
	}
	else
	{
		for (integer i = 0; i < vertices.GetCount(); ++i)
		{
			_worldVertices[i] = vertices[i].Position;
		}
	}
}

//===========================================================================//

void Renderable::InternalSetColor(const Color & color)
{
	_color = color;
	UpdateUsesBlending();
}

//===========================================================================//

void Renderable::InternalSetOriginFactor(const Vector3 & originFactor)
{
	_originFactor.X = Math::FitInRange(originFactor.X, 0.0f, 1.0f);
	_originFactor.Y = Math::FitInRange(originFactor.Y, 0.0f, 1.0f);
	_originFactor.Z = Math::FitInRange(originFactor.Z, 0.0f, 1.0f);
}

//===========================================================================//

void Renderable::InternalSetTexture(const WeakPtr<Texture> & texture)
{
	_texture = texture;
	_renderingAttributes->Use2DTexture(texture != NullPtr);
	UpdateUsesBlending();
}

//===========================================================================//

void Renderable::UpdateUsesBlending() const
{
	bool textureWithAlphaChannel = false;
	if (_texture != NullPtr)
	{
		textureWithAlphaChannel = _texture->GetPixelFormat() == PixelFormat::MonochromeAlpha ||
		                          _texture->GetPixelFormat() == PixelFormat::RGBA ||
		                          _texture->GetPixelFormat() == PixelFormat::BGRA;
	}

	_renderingAttributes->UseBlending(textureWithAlphaChannel || _color.GetAlpha() < 1.0f ||
	                                  _renderingAttributes->IsBlendFunctionsNeedsBlending());
}

//===========================================================================//

void Renderable::EnableScissorsTest(const IntRectangle & scissorsRectangle)
{
	_renderingAttributes->UseScissorsTest(true);
	_renderingAttributes->SetScissorsRectangle(scissorsRectangle);
}

//===========================================================================//

void Renderable::DisableScissorsTest()
{
	_renderingAttributes->UseScissorsTest(false);
}

//===========================================================================//

Vector2 Renderable::GetTextureSizeFactor() const
{
	if (_texture != NullPtr)
	{
		return {_texture->GetWidthFactor(), _texture->GetHeightFactor()};
	}
	else
	{
		return {1.0f, 1.0f};
	}
}

//===========================================================================//

void Renderable::OnTransformationChanged(const EmptyEventArgs &)
{
	UpdateWorldVertices();
	if (_positioningNode != NullPtr && _renderingAttributes->UsesScissorsTest())
	{
		const auto & transformation = _positioningNode->GetWorldTransformation();
		//ToDo: нужно доработать для 3д и для Perspective проекции
		IntRectangle scissorsRectangle = _renderingAttributes->GetScissorsRectangle();
		scissorsRectangle.X = transformation.GetPositionX();
		scissorsRectangle.Y = transformation.GetPositionY();
		_renderingAttributes->SetScissorsRectangle(scissorsRectangle);
	}
}

//===========================================================================//

void Renderable::OnBlendFunctionChanged(const EmptyEventArgs &)
{
	UpdateUsesBlending();
}

//===========================================================================//

} // namespace Bru

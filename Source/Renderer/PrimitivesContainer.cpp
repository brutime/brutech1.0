//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PrimitivesContainer.h"

#include "Renderer.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(PrimitivesContainer, EntityComponent)

//===========================================================================//

const string PrimitivesContainer::PathToLayerName = "LayerName";

//===========================================================================//

PrimitivesContainer::PrimitivesContainer(const string & domainName, const string & layerName) :
	EntityComponent(domainName),
	_layer(Renderer->GetLayer(GetDomainName(), layerName)),
	_points(),
	_lines(),
	_rectangles(),
	_circles(),
	_polygons()
{
}

//===========================================================================//

PrimitivesContainer::PrimitivesContainer(const string & domainName, const SharedPtr<BaseDataProvider> & provider) : 
	PrimitivesContainer(domainName, provider->Get<string>(PathToLayerName))
{
}

//===========================================================================//

PrimitivesContainer::~PrimitivesContainer()
{
}

//===========================================================================//

void PrimitivesContainer::AddPoints(const Array<Vector3> & vertices, const Color & color, float lineThickness,
                                    const Vector3 & originFactor)
{
	_points.Add(new RenderablePoints(GetDomainName(), _layer->GetName(), vertices, color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::RemovePoints(integer pointIndex)
{
	_points.RemoveAt(pointIndex);
}

//===========================================================================//

const List<SharedPtr<RenderablePoints>> & PrimitivesContainer::GetPoints() const
{
	return _points;
}

//===========================================================================//

const SharedPtr<RenderablePoints> & PrimitivesContainer::GetPoints(integer pointIndex) const
{
	return _points[pointIndex];
}

//===========================================================================//

const SharedPtr<RenderablePoints> & PrimitivesContainer::GetLastPoints() const
{
	return _points.GetLast();
}

//===========================================================================//

integer PrimitivesContainer::GetPointsCount() const
{
	return _points.GetCount();
}

//===========================================================================//

void PrimitivesContainer::ClearPoints()
{
	_points.Clear();
}

//===========================================================================//

void PrimitivesContainer::AddLine(const Array<Vector3> & points, const Color & color, float lineThickness,
                                  const Vector3 & originFactor)
{
	_lines.Add(new RenderableLine(GetDomainName(), _layer->GetName(), points, color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::RemoveLine(integer lineIndex)
{
	_lines.RemoveAt(lineIndex);
}

//===========================================================================//

const List<SharedPtr<RenderableLine>> & PrimitivesContainer::GetLines() const
{
	return _lines;
}

//===========================================================================//

const SharedPtr<RenderableLine> & PrimitivesContainer::GetLine(integer lineIndex) const
{
	return _lines[lineIndex];
}

//===========================================================================//

const SharedPtr<RenderableLine> & PrimitivesContainer::GetLastLine() const
{
	return _lines.GetLast();
}

//===========================================================================//

integer PrimitivesContainer::GetLinesCount() const
{
	return _lines.GetCount();
}

//===========================================================================//

void PrimitivesContainer::ClearLines()
{
	_lines.Clear();
}

//===========================================================================//

void PrimitivesContainer::AddRectangle(float width, float height, bool isFilled, const Color & color,
                                       float lineThickness, const Vector3 & originFactor)
{
	_rectangles.Add(new RenderableRectangle(GetDomainName(), _layer->GetName(), width, height, isFilled,
	                                        color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::AddRectangle(const Vector3 & position, float width, float height, bool isFilled,
                                       const Color & color, float lineThickness, const Vector3 & originFactor)
{
	_rectangles.Add(new RenderableRectangle(GetDomainName(), _layer->GetName(), position, width, height, isFilled,
	                                        color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::AddRectangle(const Vector3 & leftLowerPoint, const Vector3 & rightUpperPoint, bool isFilled, const Color & color,
                                       float lineThickness, const Vector3 & originFactor)
{
	_rectangles.Add(new RenderableRectangle(GetDomainName(), _layer->GetName(), leftLowerPoint, rightUpperPoint,
	                                        isFilled, color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::RemoveRectangle(integer rectangleIndex)
{
	_rectangles.RemoveAt(rectangleIndex);
}

//===========================================================================//

const List<SharedPtr<RenderableRectangle>> & PrimitivesContainer::GetRectangles() const
{
	return _rectangles;
}

//===========================================================================//

const SharedPtr<RenderableRectangle> & PrimitivesContainer::GetRectangle(integer rectangleIndex) const
{
	return _rectangles[rectangleIndex];
}

//===========================================================================//

const SharedPtr<RenderableRectangle> & PrimitivesContainer::GetLastRectangle() const
{
	return _rectangles.GetLast();
}

//===========================================================================//

integer PrimitivesContainer::GetRectanglesCount() const
{
	return _rectangles.GetCount();
}

//===========================================================================//

void PrimitivesContainer::ClearRectangles()
{
	_rectangles.Clear();
}

//===========================================================================//

void PrimitivesContainer::AddCircle(float radius, bool isFilled, const Color & color, float lineThickness,
                                    const Vector3 & originFactor)
{
	_circles.Add(new RenderableCircle(GetDomainName(), _layer->GetName(), radius, isFilled, color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::AddCircle(const Vector3 & position, float radius, bool isFilled, const Color & color,
                                    float lineThickness, const Vector3 & originFactor)
{
	_circles.Add(new RenderableCircle(GetDomainName(), _layer->GetName(), position, radius, isFilled, color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::RemoveCircle(integer circleIndex)
{
	_circles.RemoveAt(circleIndex);
}

//===========================================================================//

const List<SharedPtr<RenderableCircle>> & PrimitivesContainer::GetCircles() const
{
	return _circles;
}

//===========================================================================//

const SharedPtr<RenderableCircle> & PrimitivesContainer::GetCircle(integer circleIndex) const
{
	return _circles[circleIndex];
}

//===========================================================================//

const SharedPtr<RenderableCircle> & PrimitivesContainer::GetLastCircle() const
{
	return _circles.GetLast();
}

//===========================================================================//

integer PrimitivesContainer::GetCirclesCount() const
{
	return _circles.GetCount();
}

//===========================================================================//

void PrimitivesContainer::ClearCircles()
{
	_circles.Clear();
}

//===========================================================================//

void PrimitivesContainer::AddPolygon(const Array<Vector3> & vertices, bool isFilled, const Color & color, float lineThickness, const Vector3 & originFactor)
{
	_polygons.Add(new RenderablePolygon(GetDomainName(), _layer->GetName(), vertices, isFilled, color, lineThickness, originFactor));
}

//===========================================================================//

void PrimitivesContainer::RemovePolygon(integer triangleIndex)
{
	_polygons.RemoveAt(triangleIndex);
}

//===========================================================================//

const List<SharedPtr<RenderablePolygon>> & PrimitivesContainer::GetPolygons() const
{
	return _polygons;
}

//===========================================================================//

const SharedPtr<RenderablePolygon> & PrimitivesContainer::GetPolygon(integer triangleIndex) const
{
	return _polygons[triangleIndex];
}

//===========================================================================//

const SharedPtr<RenderablePolygon> & PrimitivesContainer::GetLastPolygon() const
{
	return _polygons.GetLast();
}

//===========================================================================//

integer PrimitivesContainer::GetPolygonsCount() const
{
	return _polygons.GetCount();
}

//===========================================================================//

void PrimitivesContainer::ClearPolygons()
{
	_polygons.Clear();
}

//===========================================================================//

void PrimitivesContainer::Show()
{
	for (const SharedPtr<RenderablePoints> & points : _points)
	{
		points->Show();
	}
	
	for (const SharedPtr<RenderableLine> & line : _lines)
	{
		line->Show();
	}	
	
	for (const SharedPtr<RenderableRectangle> & rectangle : _rectangles)
	{
		rectangle->Show();
	}		
	
	for (const SharedPtr<RenderableCircle> & circle : _circles)
	{
		circle->Show();
	}			
	
	for (const SharedPtr<RenderablePolygon> & polygon : _polygons)
	{
		polygon->Show();
	}
}

//===========================================================================//

void PrimitivesContainer::Hide()
{
	for (const SharedPtr<RenderablePoints> & points : _points)
	{
		points->Hide();
	}
	
	for (const SharedPtr<RenderableLine> & line : _lines)
	{
		line->Hide();
	}	
	
	for (const SharedPtr<RenderableRectangle> & rectangle : _rectangles)
	{
		rectangle->Hide();
	}		
	
	for (const SharedPtr<RenderableCircle> & circle : _circles)
	{
		circle->Hide();
	}			
	
	for (const SharedPtr<RenderablePolygon> & polygon : _polygons)
	{
		polygon->Hide();
	}	
}

//===========================================================================//        

void PrimitivesContainer::Clear()
{
	ClearPoints();
	ClearLines();
	ClearRectangles();
	ClearCircles();
	ClearPolygons();
}

//===========================================================================//

} // namespace Bru

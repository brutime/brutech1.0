//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: информация о преобразованиях ковейера
//

#ifndef PIPELINE_TRANSFORMATION_H
#define PIPELINE_TRANSFORMATION_H

#include "../Common/Common.h"
#include "../Math/Math.h"

namespace Bru
{

//===========================================================================//

struct PipelineTransformation
{
	PipelineTransformation();
	PipelineTransformation(const Matrix4x4 & projectionMatrix, const Matrix4x4 & viewMatrix);
	PipelineTransformation(const PipelineTransformation & other);
	~PipelineTransformation();

	const PipelineTransformation & operator =(const PipelineTransformation & other);

	Matrix4x4 ProjectionMatrix;
	Matrix4x4 ViewMatrix;
};

//===========================================================================//

}
#endif // PIPELINE_TRANSFORMATION_H

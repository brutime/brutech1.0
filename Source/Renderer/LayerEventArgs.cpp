//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LayerEventArgs.h"

#include "RenderableLayer.h"

namespace Bru
{

//===========================================================================//

LayerEventArgs::LayerEventArgs(const WeakPtr<RenderableLayer> & layer) : 
	Layer(layer)
{
}

//===========================================================================//

LayerEventArgs::LayerEventArgs(const LayerEventArgs & other) : 
	Layer(other.Layer)
{
}

//===========================================================================//

LayerEventArgs::~LayerEventArgs()
{
}

//===========================================================================//

LayerEventArgs & LayerEventArgs::operator =(const LayerEventArgs & other)
{
	if (this == &other)
	{
		return *this;
	}

	Layer = other.Layer;

	return *this;	
}

//===========================================================================//

} // namespace Bru

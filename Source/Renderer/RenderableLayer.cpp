//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderableLayer.h"

#include "../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

RenderableLayer::RenderableLayer(const string & name, ProjectionMode projectionMode) :
	PtrFromThisMixIn<RenderableLayer>(),
	_name(name),
	_projectionMode(projectionMode),
	_isVisible(true),
	_renderables(),
	_visibleChangedEvent(),
	VisibleChangedEvent(_visibleChangedEvent)
{
	if (name.IsEmptyOrBlankCharsOnly())
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Invalid layer name: \"{0}\"", name));
	}
}

//===========================================================================//

RenderableLayer::~RenderableLayer()
{
}

//===========================================================================//

void RenderableLayer::AddRenderable(Renderable * const renderable)
{
#ifdef DEBUGGING
	if (_renderables.Contains(renderable))
	{
		INVALID_STATE_EXCEPTION(string::Format("Layer \"{0}\" already contains renderable: \"{1}\"", _name, renderable->GetName()));
	}
#endif

	integer index = 0;
	for (Renderable * const currentRenderable : _renderables)
	{
		//Чтобы слой с минимальным rendering order оказался на верху - нужно рисовать его последним
		if (renderable->GetRenderingOrder() > currentRenderable->GetRenderingOrder())
		{
			break;
		}
		index++;
	}

	_renderables.Insert(index, renderable);
}

//===========================================================================//

void RenderableLayer::RemoveRenderable(Renderable * const renderable)
{
	_renderables.Remove(renderable);
}

//===========================================================================//

const string & RenderableLayer::GetName() const
{
	return _name;
}

//===========================================================================//

ProjectionMode RenderableLayer::GetProjectionMode() const
{
	return _projectionMode;
}

//===========================================================================//

void RenderableLayer::SetVisible(bool isVisible)
{
	_isVisible = isVisible;
	_visibleChangedEvent.Fire(LayerEventArgs(GetWeakPtr()));
}

//===========================================================================//

bool RenderableLayer::IsVisible() const
{
	return _isVisible;
}

//===========================================================================//

void RenderableLayer::Show()
{
	SetVisible(true);
}

//===========================================================================//

void RenderableLayer::Hide()
{
	SetVisible(false);
}

//===========================================================================//

const List<Renderable * const> & RenderableLayer::GetRenderables() const
{
	return _renderables;
}

//===========================================================================//

void RenderableLayer::PrintRenderables() const
{
	Console->AddOffset(2);
	Console->Hint("- {0} {1} {2}:", _name, GetVisibleString(_isVisible), GetProjectionModeString(_projectionMode));

	Console->AddOffset(2);
	for (auto * const renderable : _renderables)
	{
		Console->Hint(GetComponentData(renderable));
	}
	Console->ReduceOffset(4);
}

//===========================================================================//

bool RenderableLayer::IsEmpty() const
{
	return _renderables.IsEmpty();
}

//===========================================================================//

string RenderableLayer::GetComponentData(Renderable * const component) const
{
	string componentData;
	string visibleString = GetVisibleString(component->IsVisible());
	if (component->GetOwner() != NullPtr)
	{
		componentData = string::Format("- {0}->{1}", component->GetOwner()->GetName(), component->GetName());
	}
	else
	{
		componentData = string::Format("- {0}", component->GetName());
	}
	
	componentData += string::Format(" {0}, ({1}), [{2:0.2}]", visibleString, component->GetColor().ToString(), component->GetRenderingOrder());

	if (component->GetPositioningNode() != NullPtr)
	{
		componentData += string::Format(" {0}", component->GetPositioningNode()->GetPosition().ToString());
	}

	return componentData;
}

//===========================================================================//

string RenderableLayer::GetVisibleString(bool isVisible) const
{
	return isVisible ? "visible" : "invisible";
}

//===========================================================================//

string RenderableLayer::GetProjectionModeString(ProjectionMode projectionMode) const
{
	switch (projectionMode)
	{
	case ProjectionMode::Identity: return "Identity";
	case ProjectionMode::PixelsOrtho: return "PixelsOrtho";
	case ProjectionMode::PercentsOrtho: return "PercentsOrtho";
	case ProjectionMode::Perspective: return "Perspective";
	default: INVALID_ARGUMENT_EXCEPTION("Not defined projection mode");
	}
}

//===========================================================================//

} // namespace Bru
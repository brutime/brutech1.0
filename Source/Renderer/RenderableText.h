//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Отображаемая надпись
//

#pragma once
#ifndef RENDERABLE_TEXT_H
#define RENDERABLE_TEXT_H

#include "../Utils/ParametersFile/ParametersFile.h"
#include "../Graphics/Graphics.h"
#include "Renderable.h"

namespace Bru
{

//===========================================================================//

class RenderableText : public Renderable
{
public:
	DECLARE_COMPONENT

	RenderableText(const string & domainName, const string & layerName, const string & pathToFile);
	RenderableText(const string & domainName, const string & layerName, const string & pathToFile, const string & text);
	RenderableText(const string & domainName, const string & layerName, const string & text, const WeakPtr<Font> & font,
	               Vector2 & textBounds, const Color & color = Color::Default, WordWrapMode wrapMode = WordWrapMode::Smart,
	               TextHorizontalAlignment horizontalAlignment = TextHorizontalAlignment::Left,
	               TextVerticalAlignment verticalAlignment = TextVerticalAlignment::Baseline);
	RenderableText(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~RenderableText();

	void SetKey(const string & key);
	void SetKey(const string & key, const Color & color);
	void SetText(const string & text);
	void SetText(const string & text, const Color & color);
	const string & GetText() const;

	const WeakPtr<Font> GetFont() const;

	WordWrapMode GetWrapMode() const;

	TextHorizontalAlignment GetHorizontalTextAlignment() const;
	TextVerticalAlignment GetVerticalTextAlignment() const;

	integer GetLinesCount() const;

	float GetWidth() const;
	float GetHeight() const;

	//Ширина + расстояние до следующего символа
	float GetCharFullWidth(integer index) const;

protected:
	virtual void Register();
	virtual void Unregister();

	virtual void UpdateMeshVerticesPosition();
	virtual void UpdateMeshTextureCoords();

	virtual void UpdateOrigin();

	void BuildMesh();

	void ScaleByLayer();
	void UnscaleByLayer();

private:
	static const string PathToPathToFile;
	static const string PathToIsVisible;
	static const string PathToRenderingOrder;

	static const string PathToText;
	static const string TextUsesStringTableKey;
	static const string TextStringTableKeyKey;
	static const string TextValueValue;
	static const string PathToColor;
	static const string PathToFont;
	static const string FontNameKey;
	static const string FontSizeKey;
	static const string PathToAlignment;
	static const string AlignmentHorizontalKey;
	static const string AlignmentVerticalKey;

	void LoadFromFile(const string & pathToFile, bool readTextParameters, bool updateMeshes);

	static TextHorizontalAlignment GetHorizontalAlignmentByIntegerValue(integer value);
	static TextVerticalAlignment GetVerticalAlignmentByIntegerValue(integer value);

	string _text;
	WeakPtr<Font> _font;
	TextHorizontalAlignment _horizontalAlignment;
	TextVerticalAlignment _verticalAlignment;

	float _width;
	float _height;
	integer _linesCount;
	Vector2 _textSize;
	WordWrapMode _wrapMode;

	RenderableText(const RenderableText &) = delete;
	RenderableText & operator =(const RenderableText &) = delete;
};

//===========================================================================//

} // namespace

#endif // RENDERABLE_TEXT_H

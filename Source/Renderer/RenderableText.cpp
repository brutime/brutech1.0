//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderableText.h"

#include "../Utils/Log/Log.h"
#include "../Utils/PathHelper/PathHelper.h"
#include "../Utils/StringTable/StringTable.h"
#include "../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../Utils/ParametersFile/ParametersFile.h"
#include "../Graphics/Graphics.h"
#include "Renderer.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(RenderableText, Renderable)

//===========================================================================//

const string RenderableText::PathToPathToFile = "PathToFile";
const string RenderableText::PathToIsVisible = "IsVisible";
const string RenderableText::PathToRenderingOrder = "RenderingOrder";

//===========================================================================//

const string RenderableText::PathToText = "Text";
const string RenderableText::TextUsesStringTableKey = ".UsesStringTable";
const string RenderableText::TextStringTableKeyKey = ".StringTableKey";
const string RenderableText::TextValueValue = ".Value";
const string RenderableText::PathToColor = "Color";
const string RenderableText::PathToFont = "Font";
const string RenderableText::FontNameKey = ".Name";
const string RenderableText::FontSizeKey = ".Size";
const string RenderableText::PathToAlignment = "Alignment";
const string RenderableText::AlignmentHorizontalKey = ".Horizontal";
const string RenderableText::AlignmentVerticalKey = ".Vertical";

//===========================================================================//

RenderableText::RenderableText(const string & domainName, const string & layerName, const string & pathToFile) :
	Renderable(domainName, layerName, VerticesMode::Triangles),
	_text(),
	_font(),
	_horizontalAlignment(),
	_verticalAlignment(),
	_width(0.0f),
	_height(0.0f),
	_linesCount(0),
	_textSize(),
	_wrapMode()
{
	LoadFromFile(pathToFile, true, true);
}

//===========================================================================//

RenderableText::RenderableText(const string & domainName, const string & layerName, const string & pathToFile,
                               const string & text) :
	RenderableText(domainName, layerName, pathToFile)
{
	SetText(text);
}

//===========================================================================//

RenderableText::RenderableText(const string & domainName, const string & layerName, const string & text,
                               const WeakPtr<Font> & font, Vector2 & textBounds, const Color & color, WordWrapMode wrapMode,
                               TextHorizontalAlignment horizontalAlignment, TextVerticalAlignment verticalAlignment) :
	Renderable(domainName, layerName, VerticesMode::Triangles),
	_text(text),
	_font(font),
	_horizontalAlignment(horizontalAlignment),
	_verticalAlignment(verticalAlignment),
	_width(0.0f),
	_height(0.0f),
	_linesCount(0),
	_textSize(textBounds),
	_wrapMode(wrapMode)
{
	BuildMesh();
	SetTexture(_font->GetTexture());
	SetColor(color);
	SetName(string::Format("{0}(\"{1}\")", GetTypeName(), _text));
}

//===========================================================================//

RenderableText::RenderableText(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	Renderable(domainName, string::Empty, VerticesMode::Triangles),
	_text(),
	_font(),
	_horizontalAlignment(),
	_verticalAlignment(),
	_width(0.0f),
	_height(0.0f),
	_linesCount(0),
	_textSize(),
	_wrapMode()
{
	LoadFromFile(provider->Get<string>(PathToPathToFile), true, false);
	SetVisible(provider->Get<bool>(PathToIsVisible));
	SetRenderingOrder(provider->Get<float>(PathToRenderingOrder));
}

//===========================================================================//

RenderableText::~RenderableText()
{
}

//===========================================================================//

void RenderableText::SetKey(const string & key)
{
	SetText(StringTable->GetString(key));
}

//===========================================================================//

void RenderableText::SetKey(const string & key, const Color & color)
{
	SetText(StringTable->GetString(key), color);
}

//===========================================================================//

void RenderableText::SetText(const string & text)
{
	_text = text;
	BuildMesh();
	SetName(string::Format("{0}(\"{1}\")", GetTypeName(), _text));
}

//===========================================================================//

void RenderableText::SetText(const string & text, const Color & color)
{
	_text = text;
	InternalSetColor(color);
	BuildMesh();
	SetName(string::Format("{0}(\"{1}\")", GetTypeName(), _text));
}

//===========================================================================//

const string & RenderableText::GetText() const
{
	return _text;
}

//===========================================================================//

const WeakPtr<Font> RenderableText::GetFont() const
{
	return _font;
}

//===========================================================================//

WordWrapMode RenderableText::GetWrapMode() const
{
	return _wrapMode;
}

//===========================================================================//

TextHorizontalAlignment RenderableText::GetHorizontalTextAlignment() const
{
	return _horizontalAlignment;
}

//===========================================================================//

TextVerticalAlignment RenderableText::GetVerticalTextAlignment() const
{
	return _verticalAlignment;
}

//===========================================================================//

float RenderableText::GetWidth() const
{
	return _width;
}

//===========================================================================//

float RenderableText::GetHeight() const
{
	return _height;
}

//===========================================================================//

float RenderableText::GetCharFullWidth(integer index) const
{
	if (_text[index] == "\n" || _text[index] == "\t")
	{
		return 0.0f;
	}

	float kerningCorrection = 0.0f;
	if (index < _text.GetLength() - 1)
	{
		kerningCorrection = _font->GetKerning(_text[index], _text[index + 1]);
	}
	const auto & glyph = _font->GetGlyph(_text[index]);
	float charFullWidth = kerningCorrection + glyph->AdvanceX;

	return charFullWidth * Renderer->GetPixelsToCoordsFactors(GetLayer()->GetProjectionMode()).X;
}

//===========================================================================//

integer RenderableText::GetLinesCount() const
{
	return _linesCount;
}

//===========================================================================//

void RenderableText::Register()
{
	//Нужно учитывать слой, который может изменяеться
	ScaleByLayer();
	Renderable::Register();
}

//===========================================================================//

void RenderableText::Unregister()
{
	//Нужно учитывать слой, который может изменяеться
	UnscaleByLayer();
	Renderable::Unregister();
}

//===========================================================================//

void RenderableText::UpdateMeshVerticesPosition()
{
	BuildMesh();
}

//===========================================================================//

void RenderableText::UpdateMeshTextureCoords()
{
	BuildMesh();
}

//===========================================================================//

void RenderableText::UpdateOrigin()
{
}

//===========================================================================//

void RenderableText::BuildMesh()
{
	NoticeInfo noticeInfo = NoticeBuilder::GetNoticeMesh(_text, _font, GetColor(), _textSize, _horizontalAlignment,
	                                                     _verticalAlignment, _wrapMode);
	_mesh = noticeInfo.NoticeMesh;

	_width = _textSize.X;
	_height = _textSize.Y;

	if (GetLayer() != NullPtr)
	{
		ScaleByLayer();
	}

	_linesCount = noticeInfo.LinesCount;

	// apply origin transformation
	_origin = { _width * GetOriginFactor().X, _height * GetOriginFactor().Y, 0.0f };
	for (integer i = 0; i < _mesh->GetVertices().GetCount(); i++)
	{
		_mesh->TranslateVertexPosition(i, -_origin);
	}

	UpdateWorldVertices();
}

//===========================================================================//

void RenderableText::ScaleByLayer()
{
	Vector2 meshScaleFactors = Renderer->GetPixelsToCoordsFactors(GetLayer()->GetProjectionMode());

	_mesh->ScalePosition(meshScaleFactors.X, meshScaleFactors.Y, 1.0f);
	_width *= meshScaleFactors.X;
	_height *= meshScaleFactors.Y;
}

//===========================================================================//

void RenderableText::UnscaleByLayer()
{
	Vector2 meshScaleFactors = Renderer->GetPixelsToCoordsFactors(GetLayer()->GetProjectionMode());

	meshScaleFactors.X = 1.0f / meshScaleFactors.X;
	meshScaleFactors.Y = 1.0f / meshScaleFactors.Y;

	_mesh->ScalePosition(meshScaleFactors.X, meshScaleFactors.Y, 1.0f);
	_width *= meshScaleFactors.X;
	_height *= meshScaleFactors.Y;
}

//===========================================================================//

void RenderableText::LoadFromFile(const string & pathToFile, bool readTextParameters, bool isNeedToBuildMesh)
{
	string fullPathToFile = PathHelper::GetFullPathToRenderableTextFile(pathToFile);
	WeakPtr<ParametersFile> file = ParametersFileManager->Get(fullPathToFile);

	//Добавить проверку ParametersFileValidator'ом - если файл сломан - исключение

	if (readTextParameters)
	{
		bool usesStringTable = file->Get<bool>(PathToText + TextUsesStringTableKey);

		if (usesStringTable)
		{
			string key = file->Get<string>(PathToText + TextStringTableKeyKey);
			_text = StringTable->GetString(key);
		}
		else
		{
			_text = file->Get<string>(PathToText + TextValueValue);
		}
	}

	string fontName = file->Get<string>(PathToFont + FontNameKey);
	integer fontSize = file->Get<integer>(PathToFont + FontSizeKey);

	_font = FontManager->Get(fontName, fontSize);
	SetColor(file->Get<Color>(PathToColor));

	integer horizontalAlignmentValue = file->Get<integer>(PathToAlignment + AlignmentHorizontalKey);
	_horizontalAlignment = GetHorizontalAlignmentByIntegerValue(horizontalAlignmentValue);

	integer verticalAlignmentValue = file->Get<integer>(PathToAlignment + AlignmentVerticalKey);
	_verticalAlignment = GetVerticalAlignmentByIntegerValue(verticalAlignmentValue);

	InternalSetTexture(_font->GetTexture());

	if (isNeedToBuildMesh)
	{
		BuildMesh();
	}
}

//===========================================================================//

//void RenderableText::BuildBoundingSphere()
//{
//    BoundingSphere leftLowerSphere;
//    BoundingSphere rightUpperSphere;
//
//    leftLowerSphere.position = _localMesh->GetMinVertexPosition();
//    leftLowerSphere.isEmpty = false;
//
//    rightUpperSphere.position = _localMesh->GetMaxVertexPosition();
//
//    boundingSphere->Assign(leftLowerSphere.Consume(rightUpperSphere));
//}
//
////===========================================================================//

TextHorizontalAlignment RenderableText::GetHorizontalAlignmentByIntegerValue(integer value)
{
	if (value < 0 || value >= static_cast<integer>(TextHorizontalAlignment::Count))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Unknown value {0}", value));
	}

	return static_cast<TextHorizontalAlignment>(value);
}

//===========================================================================//

TextVerticalAlignment RenderableText::GetVerticalAlignmentByIntegerValue(integer value)
{
	if (value < 0 || value >= static_cast<integer>(TextVerticalAlignment::Count))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Unknown value {0}", value));
	}

	return static_cast<TextVerticalAlignment>(value);
}

//===========================================================================//

} // namespace Bru

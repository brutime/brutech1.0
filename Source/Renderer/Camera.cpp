//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Camera.h"

#include "Renderer.h"

namespace Bru
{

//===========================================================================//

Camera::Camera() :
	_transformation(),
	_transformationChangedEvent(),
	TransformationChanged(_transformationChangedEvent)	
{
}

//===========================================================================//

Camera::~Camera()
{
}

//===========================================================================//

void Camera::LookAt(float x, float y, float z, const Vector3 & up)
{
	_transformation.LookAt(x, y, z, up);
	ApplyTransformation();
}

//===========================================================================//

void Camera::SetPosition(float x, float y, float z)
{
	_transformation.SetPosition(x, y, z);
	ApplyTransformation();
}

//===========================================================================//

void Camera::SetPosition(const Vector3 & position)
{
	_transformation.SetPosition(position);
	ApplyTransformation();
}

//===========================================================================//

void Camera::Set2DPosition(float x, float y, float z)
{
	_transformation.SetPosition(x, y, z);
	_transformation.LookAt(x, y, z - 1);
	ApplyTransformation();
}

//===========================================================================//

void Camera::Set2DPosition(const Vector3 & position)
{
	_transformation.SetPosition(position);
	_transformation.LookAt(position.X, position.Y, position.Z - 1);
	ApplyTransformation();
}

//===========================================================================//

void Camera::Translate(float dx, float dy, float dz)
{
	_transformation.Translate(dx, dy, dz);
	ApplyTransformation();
}

//===========================================================================//

void Camera::Translate(const Vector3 & deltaPosition)
{
	_transformation.Translate(deltaPosition);
	ApplyTransformation();
}

//===========================================================================//

const Vector3 & Camera::GetPosition() const
{
	return _transformation.GetPosition();
}

//===========================================================================//

void Camera::ApplyTransformation()
{
	Renderer->SetViewMatrix(ProjectionMode::Perspective, _transformation.GetMatrix().GetInversed());
	_transformationChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

const Transformation & Bru::Camera::GetTransformation() const
{
	return _transformation;
}

//===========================================================================//

} // namespace Bru
//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: линия же
//

#pragma once
#ifndef RENDERABLE_LINE_H
#define RENDERABLE_LINE_H

#include "BaseVerticesRenderablePrimitive.h"

namespace Bru
{

//===========================================================================//

class RenderableLine : public BaseVerticesRenderablePrimitive
{
public:
	DECLARE_COMPONENT

	RenderableLine(const string & domainName, const string & layerName, const Array<Vector3> & vertices,
	               const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	               const Vector3 & originFactor = {});

	virtual ~RenderableLine();

private:
	RenderableLine(const RenderableLine &) = delete;
	RenderableLine & operator =(const RenderableLine &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERABLE_LINE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderableRectangle.h"

namespace Bru
{
//===========================================================================//

IMPLEMENT_COMPONENT(RenderableRectangle, BaseRenderablePrimitive)

//===========================================================================//

RenderableRectangle::RenderableRectangle(const string & domainName, const string & layerName, float width, float height,
                                         bool isFilled, const Color & color, float lineThickness, const Vector3 & originFactor) :
	RenderableRectangle(domainName, layerName, {}, { width, height }, isFilled, color, lineThickness, originFactor)
{
}

//===========================================================================//

RenderableRectangle::RenderableRectangle(const string & domainName, const string & layerName, const Vector3 & position,
                                         float width, float height, bool isFilled, const Color & color, float lineThickness, const Vector3 & originFactor) :
	RenderableRectangle(domainName, layerName, position, {position.X + width, position.Y + height, position.Z}, isFilled, color, lineThickness, originFactor)
{
}

//===========================================================================//

RenderableRectangle::RenderableRectangle(const string & domainName, const string & layerName, const Vector3 & leftLowerPoint,
                                         const Vector3 & rightUpperPoint, bool isFilled, const Color & color, float lineThickness, const Vector3 & originFactor) :
	BaseRenderablePrimitive(domainName, layerName, GetVerticesModeByFilledFlag(isFilled), color, lineThickness, originFactor),
	_leftLowerPoint(leftLowerPoint),
	_rightUpperPoint(rightUpperPoint)
{
	BuildMesh();
}

//===========================================================================//

RenderableRectangle::~RenderableRectangle()
{
}

//===========================================================================//

void RenderableRectangle::SetCoords(const Vector3 & leftLowerPoint, const Vector3 & rightUpperPoint)
{
	_leftLowerPoint = leftLowerPoint;
	_rightUpperPoint = rightUpperPoint;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

void RenderableRectangle::SetCoords(const Vector3 & position, float width, float height)
{
	_leftLowerPoint = position;
	_rightUpperPoint = { position.X + width, position.Y + height, position.Z };
	UpdateMeshVerticesPosition();
}

//===========================================================================//

void RenderableRectangle::SetLeftLowerPoint(const Vector3 & leftLowerPoint)
{
	_leftLowerPoint = leftLowerPoint;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

const Vector3 & RenderableRectangle::GetLeftLowerPoint() const
{
	return _leftLowerPoint;
}

//===========================================================================//

void RenderableRectangle::SetRightUpperPoint(const Vector3 & rightUpperPoint)
{
	_rightUpperPoint = rightUpperPoint;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

const Vector3 & RenderableRectangle::GetRightUpperPoint() const
{
	return _rightUpperPoint;
}

//===========================================================================//

void RenderableRectangle::SetSize(float width, float height)
{
	SetWidth(width);
	SetHeight(height);
}

//===========================================================================//

void RenderableRectangle::SetSize(Size size)
{
	SetWidth(size.Width);
	SetHeight(size.Height);
}

//===========================================================================//

Size RenderableRectangle::GetSize() const
{
	return Size(GetWidth(), GetHeight());
}

//===========================================================================//

void RenderableRectangle::SetWidth(float width)
{
	_rightUpperPoint.X = _leftLowerPoint.X + width;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

float RenderableRectangle::GetWidth() const
{
	return _rightUpperPoint.X - _leftLowerPoint.X;
}

//===========================================================================//

void RenderableRectangle::SetHeight(float height)
{
	_rightUpperPoint.Y = _leftLowerPoint.Y + height;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

float RenderableRectangle::GetHeight() const
{
	return _rightUpperPoint.Y - _leftLowerPoint.Y;
}

//===========================================================================//

void RenderableRectangle::UpdateMeshVerticesPosition()
{
	UpdateOrigin();
	_mesh->SetVertexPosition(0, { -_origin.X + _leftLowerPoint.X, -_origin.Y + _leftLowerPoint.Y, -_origin.Z + _leftLowerPoint.Z });
	_mesh->SetVertexPosition(1, { -_origin.X + _rightUpperPoint.X, -_origin.Y + _leftLowerPoint.Y, -_origin.Z + _rightUpperPoint.Z});
	_mesh->SetVertexPosition(2, { -_origin.X + _rightUpperPoint.X, -_origin.Y + _rightUpperPoint.Y, -_origin.Z + _rightUpperPoint.Z});
	_mesh->SetVertexPosition(3, { -_origin.X + _leftLowerPoint.X, -_origin.Y + _rightUpperPoint.Y, -_origin.Z + _leftLowerPoint.Z });
	UpdateWorldVertices();
}

//===========================================================================//

void RenderableRectangle::UpdateOrigin()
{
	_origin =
	{
		(_rightUpperPoint.X - _leftLowerPoint.X) * GetOriginFactor().X,
		(_rightUpperPoint.Y - _leftLowerPoint.Y) * GetOriginFactor().Y,
		0.0f
	};
}

//===========================================================================//

void RenderableRectangle::BuildMesh()
{
	PrepareMesh(4);	
	
	UpdateOrigin();
	_mesh->SetVertex(0, { -_origin.X + _leftLowerPoint.X, -_origin.Y + _leftLowerPoint.Y, -_origin.Z + _leftLowerPoint.Z }, {}, GetColor());
	_mesh->SetVertex(1, { -_origin.X + _rightUpperPoint.X, -_origin.Y + _leftLowerPoint.Y, -_origin.Z + _rightUpperPoint.Z}, {}, GetColor());
	_mesh->SetVertex(2, { -_origin.X + _rightUpperPoint.X, -_origin.Y + _rightUpperPoint.Y, -_origin.Z + _rightUpperPoint.Z}, {}, GetColor());
	_mesh->SetVertex(3, { -_origin.X + _leftLowerPoint.X, -_origin.Y + _rightUpperPoint.Y, -_origin.Z + _leftLowerPoint.Z }, {}, GetColor());
	UpdateWorldVertices();	
}

//===========================================================================//

} // namespace Bru

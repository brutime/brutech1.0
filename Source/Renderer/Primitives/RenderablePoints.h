//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Набор (или одна) точек
//

#pragma once
#ifndef RENDERABLE_POINTS_H
#define RENDERABLE_POINTS_H

#include "BaseVerticesRenderablePrimitive.h"

namespace Bru
{

//===========================================================================//

class RenderablePoints : public BaseVerticesRenderablePrimitive
{
public:
	DECLARE_COMPONENT

	RenderablePoints(const string & domainName, const string & layerName, const Array<Vector3> & vertices,
	                 const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	                 const Vector3 & originFactor = {});

	virtual ~RenderablePoints();

private:
	RenderablePoints(const RenderablePoints &) = delete;
	RenderablePoints & operator =(const RenderablePoints &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERABLE_POINTS_H

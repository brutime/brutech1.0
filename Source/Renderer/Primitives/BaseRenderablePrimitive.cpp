//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseRenderablePrimitive.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(BaseRenderablePrimitive, Renderable)

//===========================================================================//

BaseRenderablePrimitive::BaseRenderablePrimitive(const string & domainName, const string & layerName,
                                                 VerticesMode verticesMode, const Color & color, float lineThickness,
                                                 const Vector3 & originFactor) :
	Renderable(domainName, layerName, verticesMode)
{
	InternalSetOriginFactor(originFactor);
	SetLineThickness(lineThickness);
	SetColor(color);
}

//===========================================================================//

BaseRenderablePrimitive::~BaseRenderablePrimitive()
{
}

//===========================================================================//

const Vector3 & BaseRenderablePrimitive::GetFirstVertex() const
{
	return _mesh->GetVertexPosition(0);
}

//===========================================================================//

const Vector3 & BaseRenderablePrimitive::GetLastVertex() const
{
	return _mesh->GetVertexPosition(_mesh->GetVertices().GetCount());
}

//===========================================================================//

void BaseRenderablePrimitive::UpdateMeshTextureCoords()
{
}

//===========================================================================//

void BaseRenderablePrimitive::PrepareMesh(integer verticesCount)
{
	//У примитивов количество вершин и индексов должны совпадать
	_mesh->Resize(verticesCount, verticesCount);
	
	for (integer i = 0; i < verticesCount; ++i)
	{
		_mesh->SetVertexColor(i, GetColor());
		_mesh->SetIndex(i, i);
	}	
}

//===========================================================================//

VerticesMode BaseRenderablePrimitive::GetVerticesModeByFilledFlag(bool isFilled)
{
	return isFilled ? VerticesMode::Polygon : VerticesMode::LinesLoop;
}

//===========================================================================//

Vector3 BaseRenderablePrimitive::GetProfile() const
{
	return _mesh->GetMaxVertexPosition() - _mesh->GetMinVertexPosition();
}

//===========================================================================//

} // namespace Bru

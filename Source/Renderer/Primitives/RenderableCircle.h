//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Окружность
//

#pragma once
#ifndef RENDERABLE_CIRCLE_H
#define RENDERABLE_CIRCLE_H

#include "BaseRenderablePrimitive.h"

namespace Bru
{

//===========================================================================//

class RenderableCircle : public BaseRenderablePrimitive
{
public:
	DECLARE_COMPONENT

	RenderableCircle(const string & domainName, const string & layerName, float radius, bool isFilled, const Color & color,
	                 float lineThickness = RenderingConstants::DefaultLineThickness, const Vector3 & originFactor = {});

	RenderableCircle(const string & domainName, const string & layerName, const Vector3 & position, float radius,
	                 bool isFilled, const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	                 const Vector3 & originFactor = {});

	virtual ~RenderableCircle();

	void SetPosition(const Vector3 & position);
	const Vector3 & GetPosition() const;

	void SetRadius(float radius);
	float GetRadius() const;

protected:
	virtual void UpdateMeshVerticesPosition();

	virtual void UpdateOrigin();
	
	void BuildMesh();	

private:
	static const integer DegreeStep;

	Vector3 _position;
	float _radius;

	RenderableCircle(const RenderableCircle &) = delete;
	RenderableCircle & operator =(const RenderableCircle &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERABLE_CIRCLE_H

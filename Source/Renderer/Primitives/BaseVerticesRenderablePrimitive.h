//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый примитив, который состоит из набора вершин
//

#pragma once
#ifndef BASE_VERTICES_RENDERABLE_PRIMITIVE_H
#define BASE_VERTICES_RENDERABLE_PRIMITIVE_H

#include "BaseRenderablePrimitive.h"

namespace Bru
{

//===========================================================================//

class BaseVerticesRenderablePrimitive : public BaseRenderablePrimitive
{
public:
	DECLARE_COMPONENT

	BaseVerticesRenderablePrimitive(const string & domainName, const string & layerName, const Array<Vector3> & vertices,
	                                VerticesMode verticesMode, const Color & color, float lineThickness, const Vector3 & originFactor);

	virtual ~BaseVerticesRenderablePrimitive();

	void SetVertices(const Array<Vector3> & vertices);
	const Array<Vector3> & GetVertices() const;

protected:
	virtual void UpdateMeshVerticesPosition();

	virtual void UpdateOrigin();

	void BuildMesh();

private:
	Array<Vector3> _vertices;

	BaseVerticesRenderablePrimitive(const BaseVerticesRenderablePrimitive &) = delete;
	BaseVerticesRenderablePrimitive & operator =(const BaseVerticesRenderablePrimitive &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // BASE_VERTICES_RENDERABLE_PRIMITIVE_H

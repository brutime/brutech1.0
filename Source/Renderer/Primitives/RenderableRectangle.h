//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Полый прямоугольник
//

#pragma once
#ifndef RENDERABLE_RECTANGLE_H
#define RENDERABLE_RECTANGLE_H

#include "BaseRenderablePrimitive.h"

namespace Bru
{
//===========================================================================//

class RenderableRectangle : public BaseRenderablePrimitive
{
public:
	DECLARE_COMPONENT

	RenderableRectangle(const string & domainName, const string & layerName, float width, float height,
	                    bool isFilled, const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	                    const Vector3 & originFactor = {}
	);

	RenderableRectangle(const string & domainName, const string & layerName, const Vector3 & position, float width,
	                    float height, bool isFilled, const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	                    const Vector3 & originFactor = {}
	);

	RenderableRectangle(const string & domainName, const string & layerName, const Vector3 & leftLowerPoint,
	                    const Vector3 & rightUpperPoint, bool isFilled, const Color & color,
	                    float lineThickness = RenderingConstants::DefaultLineThickness,
	                    const Vector3 & originFactor = {}
	);

	virtual ~RenderableRectangle();

	void SetCoords(const Vector3 & leftLowerPoint, const Vector3 & rightUpperPoint);
	void SetCoords(const Vector3 & position, float width, float height);

	void SetLeftLowerPoint(const Vector3 & leftLowerPoint);
	const Vector3 & GetLeftLowerPoint() const;

	void SetRightUpperPoint(const Vector3 & rightUpperPoint);
	const Vector3 & GetRightUpperPoint() const;

	void SetSize(float width, float height);
	void SetSize(Size size);
	Size GetSize() const;

	void SetWidth(float width);
	float GetWidth() const;

	void SetHeight(float height);
	float GetHeight() const;

protected:
	virtual void UpdateMeshVerticesPosition();

	virtual void UpdateOrigin();
	
	void BuildMesh();	

private:
	Vector3 _leftLowerPoint;
	Vector3 _rightUpperPoint;

	RenderableRectangle(const RenderableRectangle &) = delete;
	RenderableRectangle & operator =(const RenderableRectangle &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERABLE_RECTANGLE_H


//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderablePoints.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(RenderablePoints, BaseVerticesRenderablePrimitive)

//===========================================================================//

RenderablePoints::RenderablePoints(const string & domainName, const string & layerName, const Array<Vector3> & vertices,
                                   const Color & color, float lineThickness, const Vector3 & originFactor) :
	BaseVerticesRenderablePrimitive(domainName, layerName, vertices, VerticesMode::Points, color, lineThickness, originFactor)
{
}

//===========================================================================//

RenderablePoints::~RenderablePoints()
{
}

//===========================================================================//

} // namespace Bru

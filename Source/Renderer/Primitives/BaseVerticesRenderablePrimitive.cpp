//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseVerticesRenderablePrimitive.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(BaseVerticesRenderablePrimitive, BaseRenderablePrimitive)

//===========================================================================//

BaseVerticesRenderablePrimitive::BaseVerticesRenderablePrimitive(const string & domainName, const string & layerName,
                                                                 const Array<Vector3> & vertices, VerticesMode verticesMode,
                                                                 const Color & color, float lineThickness, const Vector3 & originFactor) :
	BaseRenderablePrimitive(domainName, layerName, verticesMode, color, lineThickness, originFactor),
	_vertices(vertices)
{
	BuildMesh();
}

//===========================================================================//

BaseVerticesRenderablePrimitive::~BaseVerticesRenderablePrimitive()
{
}

//===========================================================================//

void BaseVerticesRenderablePrimitive::SetVertices(const Array<Vector3> & vertices)
{
	_vertices = vertices;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

const Array<Vector3> & BaseVerticesRenderablePrimitive::GetVertices() const
{
	return _vertices;
}

//===========================================================================//

void BaseVerticesRenderablePrimitive::UpdateMeshVerticesPosition()
{
	UpdateOrigin();
	if (_mesh->GetVertices().GetCount() != _vertices.GetCount())
	{
		PrepareMesh(_vertices.GetCount());
	}
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		_mesh->SetVertexPosition(i, -_origin + _vertices[i]);
	}
	UpdateWorldVertices();	
}

//===========================================================================//

void BaseVerticesRenderablePrimitive::UpdateOrigin()
{
	Vector3 profile = GetProfile();
	_origin = {profile.X * GetOriginFactor().X, profile.Y * GetOriginFactor().Y, 0.0f};
}

//===========================================================================//

void BaseVerticesRenderablePrimitive::BuildMesh()
{
	PrepareMesh(_vertices.GetCount());
	
	UpdateOrigin();
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		_mesh->SetVertex(i, -_origin + _vertices[i], {}, GetColor());
	}
	UpdateWorldVertices();
}

//===========================================================================//

} // namespace Bru



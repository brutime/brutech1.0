//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый примитив
//

#pragma once
#ifndef BASE_RENDERABLE_PRIMITIVE_H
#define BASE_RENDERABLE_PRIMITIVE_H

#include "../Renderable.h"
#include "../RenderingConstants.h"

namespace Bru
{

//===========================================================================//

class BaseRenderablePrimitive : public Renderable
{
public:
	DECLARE_COMPONENT

	virtual ~BaseRenderablePrimitive();

	const Vector3 & GetFirstVertex() const;
	const Vector3 & GetLastVertex() const;

protected:
	BaseRenderablePrimitive(const string & domainName, const string & layerName, VerticesMode verticesMode,
	                        const Color & color, float lineThickness, const Vector3 & originFactor);

	virtual void UpdateMeshTextureCoords();

	void PrepareMesh(integer verticesCount);

	static VerticesMode GetVerticesModeByFilledFlag(bool isFilled);
	//Метод получает вектор, содержащий ширину, высоту и глубину рамки, которая строится вокруг геометрической фигуры
	Vector3 GetProfile() const;

private:
	BaseRenderablePrimitive(const BaseRenderablePrimitive &) = delete;
	BaseRenderablePrimitive & operator =(const BaseRenderablePrimitive &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // BASE_RENDERABLE_PRIMITIVE_H

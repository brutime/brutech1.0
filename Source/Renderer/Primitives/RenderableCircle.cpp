//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderableCircle.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(RenderableCircle, BaseRenderablePrimitive)

//===========================================================================//

const integer RenderableCircle::DegreeStep = 18;

//===========================================================================//

RenderableCircle::RenderableCircle(const string & domainName, const string & layerName, float radius,
                                   bool isFilled, const Color & color, float lineThickness, const Vector3 & originFactor) :
	RenderableCircle(domainName, layerName, {}, radius, isFilled, color, lineThickness, originFactor)
{
}

//===========================================================================//

RenderableCircle::RenderableCircle(const string & domainName, const string & layerName, const Vector3 & position,
                                   float radius, bool isFilled, const Color & color, float lineThickness, const Vector3 & originFactor) :
	BaseRenderablePrimitive(domainName, layerName, GetVerticesModeByFilledFlag(isFilled), color, lineThickness, originFactor),
	_position(position),
	_radius(radius)
{
	BuildMesh();
}

//===========================================================================//

RenderableCircle::~RenderableCircle()
{
}

//===========================================================================//

void RenderableCircle::SetPosition(const Vector3 & position)
{
	_position = position;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

const Vector3 & RenderableCircle::GetPosition() const
{
	return _position;
}

//===========================================================================//

void RenderableCircle::SetRadius(float radius)
{
	_radius = radius;
	UpdateMeshVerticesPosition();
}

//===========================================================================//

float RenderableCircle::GetRadius() const
{
	return _radius;
}

//===========================================================================//

void RenderableCircle::UpdateMeshVerticesPosition()
{
	UpdateOrigin();
	integer rotation = 360;
	for (integer i = 0; i < _mesh->GetVertices().GetCount(); ++i)
	{
		_mesh->SetVertexPosition(i, {-_origin.X + _position.X + Math::Sin(rotation) * _radius,
		                             -_origin.Y + _position.Y + Math::Cos(rotation) * _radius,
		                             -_origin.Z + _position.Z});
		rotation -= DegreeStep;
	}
	UpdateWorldVertices();
}

//===========================================================================//

void RenderableCircle::UpdateOrigin()
{
	_origin = { _radius * 2.0f * GetOriginFactor().X, _radius * 2.0f * GetOriginFactor().Y, 0.0f };
}

//===========================================================================//

void RenderableCircle::BuildMesh()
{
	PrepareMesh(360 / DegreeStep);

	UpdateOrigin();
	integer rotation = 360;
	for (integer i = 0; i < _mesh->GetVertices().GetCount(); ++i)
	{
		_mesh->SetVertex(i, {-_origin.X + _position.X + Math::Sin(rotation) * _radius,
		                     -_origin.Y + _position.Y + Math::Cos(rotation) * _radius,
		                     -_origin.Z + _position.Z}, {}, GetColor());
		rotation -= DegreeStep;
	}
	UpdateWorldVertices();
}

//===========================================================================//

} // namespace Bru

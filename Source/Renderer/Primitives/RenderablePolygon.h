//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Треугольник
//

#pragma once
#ifndef RENDERABLE_POLYGON_H
#define RENDERABLE_POLYGON_H

#include "BaseVerticesRenderablePrimitive.h"

namespace Bru
{

//===========================================================================//

class RenderablePolygon : public BaseVerticesRenderablePrimitive
{
public:
	DECLARE_COMPONENT

	RenderablePolygon(const string & domainName, const string & layerName, const Array<Vector3> & vertices,
	                  bool isFilled, const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	                  const Vector3 & originFactor = {});

	virtual ~RenderablePolygon();

private:
	RenderablePolygon(const RenderablePolygon &) = delete;
	RenderablePolygon & operator =(const RenderablePolygon &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERABLE_POLYGON_H

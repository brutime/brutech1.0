//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef RENDERING_CONSTANTS_H
#define RENDERING_CONSTANTS_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

class RenderingConstants
{
public:
	static const float DefaultLineThickness;
	
private:
	RenderingConstants() = delete;
	RenderingConstants(const RenderingConstants &) = delete;
	~RenderingConstants() = delete;
	RenderingConstants & operator =(const RenderingConstants &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERING_CONSTANTS_H

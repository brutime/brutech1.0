//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PipelineTransformation.h"

namespace Bru
{

//===========================================================================//

PipelineTransformation::PipelineTransformation() :
	ProjectionMatrix(Matrix4x4::Identity),
	ViewMatrix(Matrix4x4::Identity)
{
}

//===========================================================================//

PipelineTransformation::PipelineTransformation(const Matrix4x4 & projectionMatrix, const Matrix4x4 & viewMatrix) :
	ProjectionMatrix(projectionMatrix),
	ViewMatrix(viewMatrix)
{
}

//===========================================================================//

PipelineTransformation::PipelineTransformation(const PipelineTransformation & other) :
	ProjectionMatrix(other.ProjectionMatrix),
	ViewMatrix(other.ViewMatrix)
{

}

//===========================================================================//

PipelineTransformation::~PipelineTransformation()
{
}

//===========================================================================//

const PipelineTransformation & PipelineTransformation::operator =(const PipelineTransformation & other)
{
	if (this == &other)
	{
		return *this;
	}

	ProjectionMatrix = other.ProjectionMatrix;
	ViewMatrix = other.ViewMatrix;

	return *this;
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef CAMERA_H
#define CAMERA_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "../EntitySystem/EntitySystem.h"

namespace Bru
{

//===========================================================================//

class Camera
{
public:
	Camera();
	~Camera();

	void LookAt(float x, float y, float z, const Vector3 & up = Vector3(0.0f, 1.0f, 0.0f));

	void SetPosition(float x, float y, float z = 0.0f);
	void SetPosition(const Vector3 & position);

	void Set2DPosition(float x, float y, float z = 0.0f);
	void Set2DPosition(const Vector3 & position);

	void Translate(float dx, float dy, float dz);
	void Translate(const Vector3 & deltaPosition);

	const Vector3 & GetPosition() const;

	void ApplyTransformation();

	const Transformation & GetTransformation() const;

private:
	Transformation _transformation;
	
protected:
	InnerEvent<const EmptyEventArgs &> _transformationChangedEvent;

public:
	Event<const EmptyEventArgs &> TransformationChanged;	
	
private:
	Camera(const Camera &) = delete;
	Camera & operator =(const Camera &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // CAMERA_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FPSCounter.h"

namespace Bru
{

//===========================================================================//

FPSCounter::FPSCounter() :
	_timer(),
	_lastFPS(0),
	_currentFramesCount(0),
	_currentTicks(0)
{
	_timer.Reset();
}

//===========================================================================//

FPSCounter::~FPSCounter()
{
}

//===========================================================================//

integer FPSCounter::Update(int framesCount)
{
	_currentFramesCount += framesCount;

	float _deltaTime = static_cast<float>(_timer.GetTicks() - _currentTicks) / _timer.GetFrequency();
	if (_deltaTime >= 1.0f)
	{
		_lastFPS = _currentFramesCount;
		_currentFramesCount = 0;
		_currentTicks = _timer.GetTicks();
	}

	return _lastFPS;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Счетчик FPS
//

#pragma once
#ifndef FPS_COUNTER_H
#define FPS_COUNTER_H

#include "../../Common/Common.h"
#include "../../OperatingSystem/OperatingSystem.h"

namespace Bru
{

//===========================================================================//

class FPSCounter
{
public:
	FPSCounter();
	~FPSCounter();

	integer Update(integer framesCount);

private:
	Timer _timer;

	integer _lastFPS;
	integer _currentFramesCount;
	int64 _currentTicks;

	FPSCounter(const FPSCounter &) = delete;
	FPSCounter & operator =(const FPSCounter &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // FPS_COUNTER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Полноэкранный режим
//

#pragma once
#ifndef FULL_SCREEN_MODE_H
#define FULL_SCREEN_MODE_H

#include <ostream>
#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

///Полноэкранный режим
enum class FullScreenMode
{
    Off = 0,
    On
};

//===========================================================================//

extern std::ostream & operator <<(std::ostream & stream, const FullScreenMode & fullScreenMode);

//===========================================================================//

} // namespace Bru

#endif // FULL_SCREEN_MODE_H

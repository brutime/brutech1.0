//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: режим проецирования сцены на экран
//

#pragma once
#ifndef PROJECTION_MODE_H
#define PROJECTION_MODE_H

namespace Bru
{

//===========================================================================//

enum class ProjectionMode
{
    Identity = 0,
    ///Координаты в диапозоне 0..Surface->GetWidth(), 0..Surface->GetHeight()
    PixelsOrtho = 1,
    ///Координаты в диапозоне 0..100, 0..100
    PercentsOrtho = 2,
    Perspective = 3
};

//===========================================================================//

} // namespace Bru

#endif // PROJECTION_MODE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: состояние анимации
//

#pragma once
#ifndef ANIMATION_STATE_H
#define ANIMATION_STATE_H

namespace Bru
{

//===========================================================================//

///Состояние анимации
enum class AnimationState
{
    Playing,
    Paused,
    Stoped
};

//===========================================================================//

}// namespace Bru

#endif // ANIMATION_STATE_H

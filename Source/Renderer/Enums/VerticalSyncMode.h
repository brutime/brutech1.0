//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Режим вертикальной синхронизации
//

#pragma once
#ifndef VERICAL_SYNC_MODE_H
#define VERICAL_SYNC_MODE_H

#include <ostream>
#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

///Режим вертикальной синхронизации
enum class VerticalSyncMode
{
    Off = 0,
    On
};

//===========================================================================//

extern std::ostream & operator <<(std::ostream & stream, const VerticalSyncMode & verticalSyncMode);

//===========================================================================//

} // namespace Bru

#endif // VERICAL_SYNC_MODE_H

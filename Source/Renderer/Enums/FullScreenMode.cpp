//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FullScreenMode.h"

namespace Bru
{

//===========================================================================//

std::ostream & operator <<(std::ostream & stream, const FullScreenMode & fullScreenMode)
{
	switch (fullScreenMode)
	{
		case FullScreenMode::Off:
			stream << "Off";
			break;
		case FullScreenMode::On:
			stream << "On";
			break;		
			
		default:
			INVALID_ARGUMENT_EXCEPTION("Not defined fullScreenMode");
	}
	return stream;
}

//===========================================================================//

} // namespace Bru
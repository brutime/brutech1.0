//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LAYER_EVENT_ARGS_H
#define LAYER_EVENT_ARGS_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

class RenderableLayer;

//===========================================================================//

struct LayerEventArgs
{
	LayerEventArgs(const WeakPtr<RenderableLayer> & layer);
	LayerEventArgs(const LayerEventArgs & other);
	~LayerEventArgs();
	LayerEventArgs & operator =(const LayerEventArgs & other);

	WeakPtr<RenderableLayer> Layer;
};

//===========================================================================//

} // namespace Bru

#endif // LAYER_EVENT_ARGS_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ColorAlphaController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(ColorAlphaController, Updatable)

//===========================================================================//

const string ColorAlphaController::PathToAlphaEnhanceSpeed = "AlphaEnhanceSpeed";

//===========================================================================//

ColorAlphaController::ColorAlphaController(const WeakPtr<Renderable> & renderable, float alphaEnhanceSpeed, bool isEnabled) :
	Updatable(renderable->GetDomainName(), isEnabled),
	_renderable(renderable),
	_alphaEnhanceSpeed(alphaEnhanceSpeed),
	_alphaEnhanceSpeedResetValue(_alphaEnhanceSpeed)
{
}

//===========================================================================//

ColorAlphaController::ColorAlphaController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) : 
	Updatable(domainName, provider),
	_renderable(NullPtr),
	_alphaEnhanceSpeed(provider->Get<float>(PathToAlphaEnhanceSpeed)),
	_alphaEnhanceSpeedResetValue(_alphaEnhanceSpeed)
{
}

//===========================================================================//

ColorAlphaController::~ColorAlphaController()
{
}

//===========================================================================//

void ColorAlphaController::Reset()
{
	Updatable::Reset();
	_alphaEnhanceSpeed = _alphaEnhanceSpeedResetValue;
}

//===========================================================================//

void ColorAlphaController::SetAlphaEnhanceSpeed(float alphaEnhanceSpeed)
{
	_alphaEnhanceSpeed = alphaEnhanceSpeed;
}

//===========================================================================//

float ColorAlphaController::GetAlphaEnhanceSpeed() const
{
	return _alphaEnhanceSpeed;
}

//===========================================================================//

void ColorAlphaController::InverseAlphaEnhanceSpeed()
{
	_alphaEnhanceSpeed = -_alphaEnhanceSpeed;
}

//===========================================================================//

void ColorAlphaController::Update(float deltaTime)
{
	if (_renderable == NullPtr)
	{
		return;
	}
	
	float colorAlpha = _renderable->GetColorAlpha();
	if ((_alphaEnhanceSpeed > 0.0f && colorAlpha < 1.0f) || (_alphaEnhanceSpeed < 0.0f && colorAlpha > 0.0f))
	{
		_renderable->SetColorAlpha(colorAlpha + _alphaEnhanceSpeed * deltaTime);
	}
}

//===========================================================================//

void ColorAlphaController::OnAddedToOwner()
{
	_renderable = GetOwnerComponent<Renderable>();
}

//===========================================================================//

void ColorAlphaController::OnRemovingFromOwner()
{
	_renderable = NullPtr;
}

//===========================================================================//

} // namespace Bru

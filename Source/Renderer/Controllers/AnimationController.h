//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: контроллер обновляет выбранную анимацию в анимированном спрайте
//

#pragma once
#ifndef ANIMATION_CONTROLLER_H
#define ANIMATION_CONTROLLER_H

#include "../../Updater/Updatable.h"
#include "../Sprites/AnimatedSprite/AnimatedSprite.h"

namespace Bru
{

//===========================================================================//

class AnimationController : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	AnimationController(const WeakPtr<AnimatedSprite> & sprite, bool isEnabled = true);
	AnimationController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~AnimationController();

	virtual void Reset();

	virtual void Update(float deltaTime);

	void SelectAnimation(const string & animationName);
	bool IsSelected(const string & animationName) const;

	void Play();
	void Pause();
	void Stop();

	bool IsFinished() const;
	integer GetCurrentFrameIndex() const;
	integer GetFirstFrameIndex() const;
	integer GetLastFrameIndex() const;

protected:
	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

private:
	WeakPtr<AnimatedSprite> _sprite;

	AnimationController(const AnimationController &) = delete;
	AnimationController & operator =(const AnimationController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ANIMATION_UPDATE_CONTROLLER_H

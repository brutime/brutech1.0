//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Контроллер увеличивает\уменьшает альфа канал цвета объекта
//

#ifndef COLOR_ALPHA_CONTROLLER_H
#define COLOR_ALPHA_CONTROLLER_H

#include "../../Renderer/Renderable.h"
#include "../../Updater/Updatable.h"

namespace Bru
{

//===========================================================================//

class ColorAlphaController : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	ColorAlphaController(const WeakPtr<Renderable> & renderable, float alphaEnhanceSpeed, bool isEnabled = true);
	ColorAlphaController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~ColorAlphaController();

	virtual void Reset();

	void Update(float deltaTime);

	void SetAlphaEnhanceSpeed(float alphaEnhanceSpeed);
	float GetAlphaEnhanceSpeed() const;

	void InverseAlphaEnhanceSpeed();

protected:
	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

	WeakPtr<Renderable> _renderable;
	float _alphaEnhanceSpeed;
	float _alphaEnhanceSpeedResetValue;

private:
	static const string PathToAlphaEnhanceSpeed;

	ColorAlphaController(const ColorAlphaController &) = delete;
	ColorAlphaController & operator =(const ColorAlphaController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // COLOR_ALPHA_CONTROLLER_H

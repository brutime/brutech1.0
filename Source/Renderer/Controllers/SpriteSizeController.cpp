//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SpriteSizeController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(SpriteSizeController, Updatable)

//===========================================================================//

const string SpriteSizeController::PathToDeltaWidth = "DeltaWidth";
const string SpriteSizeController::PathToDeltaHeight = "DeltaHeight";

//===========================================================================//

SpriteSizeController::SpriteSizeController(const WeakPtr<BaseSprite> & sprite,
        float deltaWidth, float deltaHeight, bool isEnabled) :
	Updatable(sprite->GetDomainName(), isEnabled),
	_sprite(sprite),
	_deltaWidth(deltaWidth),
	_deltaHeight(deltaHeight)
{
}

//===========================================================================//

SpriteSizeController::SpriteSizeController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) : 
	Updatable(domainName, provider),
	_sprite(NullPtr),
	_deltaWidth(provider->Get<float>(PathToDeltaWidth)),
	_deltaHeight(provider->Get<float>(PathToDeltaHeight))
{
}

//===========================================================================//

SpriteSizeController::~SpriteSizeController()
{
}

//===========================================================================//

void SpriteSizeController::Update(float deltaTime)
{
	if (_sprite == NullPtr)
	{
		return;
	}
	
	float newWidth = _sprite->GetWidth() + _deltaWidth * deltaTime;
	float newHeight = _sprite->GetHeight() + _deltaHeight * deltaTime;

	_sprite->SetSize(newWidth, newHeight);
}

//===========================================================================//

void SpriteSizeController::SetDeltaWidth(float deltaWidth)
{
	_deltaWidth = deltaWidth;
}

//===========================================================================//

float SpriteSizeController::GetDeltaWidth() const
{
	return _deltaWidth;
}

//===========================================================================//

void SpriteSizeController::SetDeltaHeight(float deltaHeight)
{
	_deltaHeight = deltaHeight;
}

//===========================================================================//

float SpriteSizeController::GetDeltaHeight() const
{
	return _deltaHeight;
}

//===========================================================================//

void SpriteSizeController::OnAddedToOwner()
{
	_sprite = GetOwnerComponent<BaseSprite>();
}

//===========================================================================//

void SpriteSizeController::OnRemovingFromOwner()
{
	_sprite = NullPtr;
}

//===========================================================================//

} // namespace Bru

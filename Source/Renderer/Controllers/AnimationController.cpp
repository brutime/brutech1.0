//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "AnimationController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(AnimationController, Updatable)

//===========================================================================//

AnimationController::AnimationController(const WeakPtr<AnimatedSprite> & sprite,bool isEnabled) :
	Updatable(sprite->GetDomainName(), isEnabled),
	_sprite(sprite)
{
}

//===========================================================================//

AnimationController::AnimationController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) : 
	Updatable(domainName, provider),
	_sprite(NullPtr)
{
}

//===========================================================================//

AnimationController::~AnimationController()
{
}

//===========================================================================//

void AnimationController::Reset()
{
	Updatable::Reset();
	_sprite->SelectFirstAnimation();
}

//===========================================================================//

void AnimationController::Update(float deltaTime)
{
	if (_sprite == NullPtr)
	{
		return;
	}
	
	_sprite->UpdateCurrentAnimation(deltaTime);
}

//===========================================================================//

void AnimationController::SelectAnimation(const string & animationName)
{
	_sprite->SelectAnimation(animationName);
}

//===========================================================================//

bool AnimationController::IsSelected(const string & animationName) const
{
	return _sprite->IsSelected(animationName);
}

//===========================================================================//

void AnimationController::Play()
{
	_sprite->Play();
}

//===========================================================================//

void AnimationController::Pause()
{
	_sprite->Pause();
}

//===========================================================================//

void AnimationController::Stop()
{
	_sprite->Stop();
}

//===========================================================================//

bool AnimationController::IsFinished() const
{
	return _sprite->IsFinished();
}

//===========================================================================//

integer AnimationController::GetCurrentFrameIndex() const
{
	return _sprite->GetCurrentFrameIndex();
}

//===========================================================================//

integer AnimationController::GetFirstFrameIndex() const
{
	return _sprite->GetFirstFrameIndex();
}

//===========================================================================//

integer AnimationController::GetLastFrameIndex() const
{
	return _sprite->GetLastFrameIndex();
}

//===========================================================================//

void AnimationController::OnAddedToOwner()
{
	_sprite = GetOwnerComponent<AnimatedSprite>();
}

//===========================================================================//

void AnimationController::OnRemovingFromOwner()
{
	_sprite = NullPtr;
}

//===========================================================================//

} // namespace Bru

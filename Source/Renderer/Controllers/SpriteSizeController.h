//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: контроллер размера спрайта
//

#pragma once
#ifndef SPRITE_SIZE_CONTROLLER_H
#define SPRITE_SIZE_CONTROLLER_H

#include "../../Updater/Updatable.h"
#include "../Sprites/BaseSprite.h"

namespace Bru
{

//===========================================================================//

class SpriteSizeController : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	static const string PathToDeltaWidth;
	static const string PathToDeltaHeight;

	SpriteSizeController(const WeakPtr<BaseSprite> & sprite, float deltaWidth, float deltaHeight, bool isEnabled = true);
	SpriteSizeController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~SpriteSizeController();

	void Update(float deltaTime);

	void SetDeltaWidth(float deltaWidth);
	float GetDeltaWidth() const;

	void SetDeltaHeight(float deltaHeight);
	float GetDeltaHeight() const;

protected:
	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

private:
	WeakPtr<BaseSprite> _sprite;
	float _deltaWidth;
	float _deltaHeight;

	SpriteSizeController(const SpriteSizeController &) = delete;
	SpriteSizeController & operator =(const SpriteSizeController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // SPRITE_SIZE_CONTROLLER_H

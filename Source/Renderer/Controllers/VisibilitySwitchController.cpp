//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "VisibilitySwitchController.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(VisibilitySwitchController, Updatable)

//===========================================================================//

const string VisibilitySwitchController::PathToVisibleInterval = "VisibleInterval";
const string VisibilitySwitchController::PathToInvisibleInterval = "InvisibleInterval";
const string VisibilitySwitchController::PathToVisibleTime = "VisibleTime";
const string VisibilitySwitchController::PathToIsVisibleOnStart = "IsVisibleOnStart";

//===========================================================================//

VisibilitySwitchController::VisibilitySwitchController(const WeakPtr<Renderable> & renderable,
        float switchInterval, bool isVisibleOnStart, bool isEnabled) :
	Updatable(renderable->GetDomainName(), isEnabled),
	_renderable(renderable),
	_visibleTime(switchInterval),
	_invisibleTime(switchInterval),
	_remainingTime(_visibleTime),
	_isVisibleOnStart(isVisibleOnStart)
{
	_renderable->SetVisible(isVisibleOnStart);
}

//===========================================================================//

VisibilitySwitchController::VisibilitySwitchController(const WeakPtr<Renderable> & renderable,
        float visibleInterval, float invisibleInterval, bool isVisibleOnStart, bool isEnabled) :
	Updatable(renderable->GetDomainName(), isEnabled),
	_renderable(renderable),
	_visibleTime(visibleInterval),
	_invisibleTime(invisibleInterval),
	_remainingTime(_visibleTime),
	_isVisibleOnStart(isVisibleOnStart)
{
	_renderable->SetVisible(isVisibleOnStart);
}

//===========================================================================//

VisibilitySwitchController::VisibilitySwitchController(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	Updatable(domainName, provider),
	_renderable(NullPtr),
	_visibleTime(provider->Get<float>(PathToVisibleInterval)),
	_invisibleTime(provider->Get<float>(PathToInvisibleInterval)),
	_remainingTime(provider->Get<float>(PathToVisibleTime)),
	_isVisibleOnStart(provider->Get<bool>(PathToIsVisibleOnStart))
{
}

//===========================================================================//

VisibilitySwitchController::~VisibilitySwitchController()
{
}

//===========================================================================//

void VisibilitySwitchController::Reset()
{
	Updatable::Reset();
	_remainingTime = _visibleTime;
	_renderable->SetVisible(_isVisibleOnStart);
}

//===========================================================================//

void VisibilitySwitchController::Update(float deltaTime)
{
	_remainingTime -= deltaTime;
	if (_remainingTime < 0.0f)
	{
		_renderable->SetVisible(!_renderable->IsVisible());
		_remainingTime = _renderable->IsVisible() ? _visibleTime : _invisibleTime;
	}
}

//===========================================================================//

void VisibilitySwitchController::StartSwitching()
{
	Enable();
}

//===========================================================================//

void VisibilitySwitchController::StopSwitching(bool isVisibleOnStop)
{
	if (isVisibleOnStop)
	{
		_remainingTime = _visibleTime;
		_renderable->Show();
	}
	else
	{
		_remainingTime = _invisibleTime;
		_renderable->Hide();
	}

	Disable();
}

//===========================================================================//

void VisibilitySwitchController::OnAddedToOwner()
{
	_renderable = GetOwnerComponent<Renderable>();
	_renderable->SetVisible(_isVisibleOnStart);
}

//===========================================================================//

void VisibilitySwitchController::OnRemovingFromOwner()
{
	_renderable = NullPtr;
}

//===========================================================================//

} // namespace Bru

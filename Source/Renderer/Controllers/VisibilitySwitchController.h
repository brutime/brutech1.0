//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: контроллер включает/выключает видимость Renderable'а
//              через заданные промежутки времени
//

#ifndef VISIBILITY_SWITCH_CONTROLLER_H
#define VISIBILITY_SWITCH_CONTROLLER_H

#include "../../Renderer/Renderable.h"
#include "../../Updater/Updatable.h"

namespace Bru
{

//===========================================================================//

class VisibilitySwitchController : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	VisibilitySwitchController(const WeakPtr<Renderable> & renderable, float switchInterval,
	                           bool isVisibleOnStart = true, bool isEnabled = true);
	VisibilitySwitchController(const WeakPtr<Renderable> & renderable, float visibleInterval,
	                           float invisibleInterval, bool isVisibleOnStart = true, bool isEnabled = true);
	VisibilitySwitchController(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~VisibilitySwitchController();

	virtual void Reset();

	virtual void Update(float deltaTime);

	void StartSwitching();
	void StopSwitching(bool isVisibleOnStop);

protected:
	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

	WeakPtr<Renderable> _renderable;
	float _visibleTime;
	float _invisibleTime;
	float _remainingTime;
	bool _isVisibleOnStart;

private:
	static const string PathToVisibleInterval;
	static const string PathToInvisibleInterval;
	static const string PathToVisibleTime;
	static const string PathToIsVisibleOnStart;

	VisibilitySwitchController(const VisibilitySwitchController &) = delete;
	VisibilitySwitchController & operator =(const VisibilitySwitchController &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // VISIBILITY_SWITCH_CONTROLLER_H

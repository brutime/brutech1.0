//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "AnimationTextureInfo.h"

namespace Bru
{

//===========================================================================//

AnimationTextureInfo::AnimationTextureInfo(const string & textureName, integer framesByWidth, integer framesByHeight) :
	TextureName(textureName),
	FramesByWidth(framesByWidth),
	FramesByHeight(framesByHeight),
	FrameWidth(1.f / FramesByWidth),
	FrameHeight(1.f / FramesByHeight)
{
}

//===========================================================================//

AnimationTextureInfo::AnimationTextureInfo(const AnimationTextureInfo & other) :
	TextureName(other.TextureName),
	FramesByWidth(other.FramesByWidth),
	FramesByHeight(other.FramesByHeight),
	FrameWidth(other.FramesByWidth),
	FrameHeight(other.FramesByHeight)
{
}

//===========================================================================//

AnimationTextureInfo & AnimationTextureInfo::operator =(const AnimationTextureInfo & other)
{
	if (this != &other)
	{
		TextureName = other.TextureName;
		FramesByWidth = other.FramesByWidth;
		FramesByHeight = other.FramesByHeight;
		FrameWidth = other.FrameWidth;
		FrameHeight = other.FrameHeight;
	}

	return *this;
}

//===========================================================================//

AnimationTextureInfo::~AnimationTextureInfo()
{
}

//===========================================================================//

}//namespace


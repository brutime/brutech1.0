//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: анимированный спрайт
//

#pragma once
#ifndef ANIMATED_SPRITE_H
#define ANIMATED_SPRITE_H

#include "../../../Common/Common.h"
#include "../../Enums/AnimationPlayingMode.h"
#include "../../Enums/AnimationState.h"
#include "../BaseSprite.h"
#include "AnimationTextureInfo.h"
#include "Animations/Animation.h"

namespace Bru
{

//===========================================================================//

class AnimatedSprite : public BaseSprite
{
public:
	DECLARE_COMPONENT

	AnimatedSprite(const string & domainName, const string & layerName, const string & pathToFile);
	AnimatedSprite(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~AnimatedSprite();

	virtual void Reset();

	void SelectFirstAnimation();
	void SelectAnimation(const string & animationName);
	bool IsSelected(const string & animationName) const;

	void Play();
	void Pause();
	void Stop();

	bool IsFinished() const;
	integer GetCurrentFrameIndex() const;
	integer GetFirstFrameIndex() const;
	integer GetLastFrameIndex() const;

	void UpdateCurrentAnimation(float deltaTime);

	virtual void FlipHorizontal();
	virtual void UnflipHorizontal();

	virtual void FlipVertical();
	virtual void UnflipVertical();

protected:
	virtual WeakPtr<ParametersFile> LoadFromFile(const string & pathToFile);

private:
	static const string PathToPathToFile;
	static const string PathToIsVisible;
	static const string PathToRenderingOrder;
	
	static const string PathToTextures;
	static const string TexturePathKey;
	static const string TextureFramesByWidthKey;
	static const string TextureFramesByHeightKey;

	static const string PathToAnimations;
	static const string AnimationNameKey;
	static const string AnimationFirstFrameIndexKey;
	static const string AnimationLastFrameIndexKey;
	static const string AnimationFrameRateKey;
	static const string AnimationIsLoopedKey;
	static const string AnimationPlayingModeKey;

	void LoadAnimation(const WeakPtr<ParametersFile> & file, const string & pathToSection);

	void UpdateFrameTextureCoords();

	virtual void UpdateMeshTextureCoords();

	KeyValuePair<Interval, SharedPtr<AnimationTextureInfo>> GetTextureInfo(integer currentFrameIndex) const;
	AnimationPlayingMode GetPlayingModeByIntegerValue(integer value) const;

	TreeMap<Interval, SharedPtr<AnimationTextureInfo>> _textureInfos;

	TreeMap<string, SharedPtr<Animation>> _animations;
	string _currentAnimationName;
	SharedPtr<Animation> _currentAnimation;

	float _currentFrameTime;
	AnimationState _currentAnimationState;

	AnimatedSprite(const AnimatedSprite &) = delete;
	AnimatedSprite & operator =(const AnimatedSprite &) = delete;

	friend class TestAnimatedSpriteNormalSingleNormalFlow;
	friend class TestAnimatedSpriteNormalSingleFewFrames;
	friend class TestAnimatedSpriteNormalHalfSingleNormalFlow;
	friend class TestAnimatedSpriteNormalHalfSingleFewFrames;

	friend class TestAnimatedSpriteNormalRepeatNormalFlow;
	friend class TestAnimatedSpriteNormalRepeatFewFrames;
	friend class TestAnimatedSpriteNormalRepeatBigSteps;
	friend class TestAnimatedSpriteNormalRepeatSuperiorStep;
	friend class TestAnimatedSpriteNormalHalfRepeatNormalFlow;
	friend class TestAnimatedSpriteNormalHalfRepeatFewFrames;
	friend class TestAnimatedSpriteNormalHalfRepeatBigSteps;
	friend class TestAnimatedSpriteNormalHalfRepeatSuperiorStep;

	friend class TestAnimatedSpriteReverseSingleNormalFlow;
	friend class TestAnimatedSpriteReverseSingleFewFrames;
	friend class TestAnimatedSpriteReverseHalfSingleNormalFlow;
	friend class TestAnimatedSpriteReverseHalfSingleFewFrames;

	friend class TestAnimatedSpriteReverseRepeatNormalFlow;
	friend class TestAnimatedSpriteReverseRepeatFewFrames;
	friend class TestAnimatedSpriteReverseRepeatBigSteps;
	friend class TestAnimatedSpriteReverseRepeatSuperiorStep;
	friend class TestAnimatedSpriteReverseHalfRepeatNormalFlow;
	friend class TestAnimatedSpriteReverseHalfRepeatFewFrames;
	friend class TestAnimatedSpriteReverseHalfRepeatBigSteps;
	friend class TestAnimatedSpriteReverseHalfRepeatSuperiorStep;

	friend class TestAnimatedSpritePingPongSingleNormalFlow;
	friend class TestAnimatedSpritePingPongSingleFewFrames;
	friend class TestAnimatedSpritePingPongHalfSingleNormalFlow;
	friend class TestAnimatedSpritePingPongHalfSingleFewFrames;

	friend class TestAnimatedSpritePingPongRepeatNormalFlow;
	friend class TestAnimatedSpritePingPongRepeatFewFrames;
	friend class TestAnimatedSpritePingPongRepeatBigSteps;
	friend class TestAnimatedSpritePingPongRepeatSuperiorStep;
	friend class TestAnimatedSpritePingPongHalfRepeatNormalFlow;
	friend class TestAnimatedSpritePingPongHalfRepeatFewFrames;
	friend class TestAnimatedSpritePingPongHalfRepeatBigSteps;
	friend class TestAnimatedSpritePingPongHalfRepeatSuperiorStep;
};

//===========================================================================//

}// namespace Bru

#endif // ANIMATED_SPRITE_H

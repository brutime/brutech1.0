//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: данные о текстуре с анимацией
//

#pragma once
#ifndef ANIMATION_TEXTURE_INFO_H
#define ANIMATION_TEXTURE_INFO_H

#include "../../../Common/Common.h"

namespace Bru
{

//===========================================================================//

struct AnimationTextureInfo
{
public:
	AnimationTextureInfo(const string & textureName, integer framesByWidth, integer framesByHeight);
	AnimationTextureInfo(const AnimationTextureInfo & other);
	~AnimationTextureInfo();

	AnimationTextureInfo & operator =(const AnimationTextureInfo & other);

	string TextureName;
	integer FramesByWidth;
	integer FramesByHeight;
	float FrameWidth;
	float FrameHeight;
};

//===========================================================================//

}//namespace

#endif // ANIMATION_TEXTURE_INFO_H

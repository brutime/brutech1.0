//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс анимации с прямым ходом
//

#pragma once
#ifndef NORMAL_ANIMATION_H
#define NORMAL_ANIMATION_H

#include "Animation.h"

namespace Bru
{

//===========================================================================//

class NormalAnimation : public Animation
{
public:
	NormalAnimation(integer firstFrameIndex, integer lastFrameIndex, integer frameRate, bool isLooped);
	virtual ~NormalAnimation();

	virtual void Next(integer framesCount);
	virtual void Reset();
	virtual void Finish();

private:
	NormalAnimation(const NormalAnimation &) = delete;
	NormalAnimation & operator =(const NormalAnimation &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // NORMAL_ANIMATION_H

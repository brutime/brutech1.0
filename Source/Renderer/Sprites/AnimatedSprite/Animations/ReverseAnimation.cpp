//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ReverseAnimation.h"

namespace Bru
{

//===========================================================================//

ReverseAnimation::ReverseAnimation(integer firstFrameIndex, integer lastFrameIndex, integer frameRate, bool isLooped) :
	Animation(firstFrameIndex, lastFrameIndex, frameRate, isLooped)
{
}

//===========================================================================//

ReverseAnimation::~ReverseAnimation()
{
}

//===========================================================================//

void ReverseAnimation::Next(integer framesCount)
{
	if (_isFinished || framesCount <= 0)
	{
		return;
	}

	_frameAccumulator += framesCount;
	if (_frameAccumulator >= _length)
	{
		if (!_isLooped)
		{
			Finish();
			return;
		}
		else
		{
			_frameAccumulator %= _length;
		}
	}

	_currentFrameIndex = _lastFrameIndex - _frameAccumulator;
}

//===========================================================================//

void ReverseAnimation::Reset()
{
	_currentFrameIndex = _lastFrameIndex;
	_isFinished = false;
	_frameAccumulator = 0;
}

//===========================================================================//

void ReverseAnimation::Finish()
{
	_currentFrameIndex = _firstFrameIndex;
	_isFinished = true;
	_frameAccumulator = _length - 1;
}

//===========================================================================//

}// namespace Bru


//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PingPongAnimation.h"

namespace Bru
{

//===========================================================================//

PingPongAnimation::PingPongAnimation(integer firstFrameIndex, integer lastFrameIndex, integer frameRate, bool isLooped) :
	Animation(firstFrameIndex, lastFrameIndex, frameRate, isLooped)
{
}

//===========================================================================//

PingPongAnimation::~PingPongAnimation()
{
}

//===========================================================================//

void PingPongAnimation::Next(integer framesCount)
{
	if (_isFinished || framesCount <= 0)
	{
		return;
	}

	if (_length == 1)
	{
		if (!_isLooped)
		{
			Finish();
		}
		return;
	}

	_frameAccumulator += framesCount;

	if (_frameAccumulator >= _length * 2 - 1)
	{
		if (!_isLooped)
		{
			Finish();
			return;
		}
		else
		{
			_frameAccumulator %= (_length * 2 - 2);
		}
	}

	if (_frameAccumulator < _length)
	{
		_currentFrameIndex = _firstFrameIndex + _frameAccumulator;
	}
	else
	{
		_currentFrameIndex = _firstFrameIndex + ((_length * 2 - 2) - _frameAccumulator);
	}
}

//===========================================================================//

void PingPongAnimation::Reset()
{
	_currentFrameIndex = _firstFrameIndex;
	_isFinished = false;
	_frameAccumulator = 0;
}

//===========================================================================//

void PingPongAnimation::Finish()
{
	_currentFrameIndex = _firstFrameIndex;
	_isFinished = true;
	_frameAccumulator = 0;
}

//===========================================================================//

}// namespace Bru


//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс анимации с обратным ходом
//

#pragma once
#ifndef REVERSE_ANIMATION_H
#define REVERSE_ANIMATION_H

#include "Animation.h"

namespace Bru
{

//===========================================================================//

class ReverseAnimation : public Animation
{
public:
	ReverseAnimation(integer firstFrameIndex, integer lastFrameIndex, integer frameRate, bool isLooped);
	virtual ~ReverseAnimation();

	virtual void Next(integer framesCount);
	virtual void Reset();
	virtual void Finish();

private:
	ReverseAnimation(const ReverseAnimation &) = delete;
	ReverseAnimation & operator =(const ReverseAnimation &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // REVERSE_ANIMATION_H

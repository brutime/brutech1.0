//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Абстрактный класс анимации
//

#pragma once
#ifndef BASE_ANIMATION_H
#define BASE_ANIMATION_H

#include "../../../../Common/Common.h"
#include "../../../../Utils/ParametersFile/ParametersFile.h"
#include "../../../Enums/AnimationPlayingMode.h"

namespace Bru
{

//===========================================================================//

class Animation
{
public:
	Animation(integer firstFrameIndex, integer lastFrameIndex, integer frameRate, bool isLooped);
	virtual ~Animation();

	virtual void Next(integer framesCount) = 0;
	virtual void Reset() = 0;
	virtual void Finish() = 0;

	integer GetCurrentFrameIndex() const;
	integer GetFirstFrameIndex() const;
	integer GetLastFrameIndex() const;

	void SetFrameRate(integer frameRate);
	integer GetFrameRate() const;
	float GetTimePerFrame() const;

	void SetLooped(bool flag);
	bool IsLooped() const;
	void EnableLooping();
	void DisableLooping();

	bool IsFinished() const;

protected:
	integer _currentFrameIndex;
	integer _firstFrameIndex;
	integer _lastFrameIndex;

	integer _length;
	integer _frameAccumulator;

	integer _frameRate; // кадр/сек
	float _timePerFrame; // 1.0f / frameRate

	bool _isLooped;
	bool _isFinished;

private:
	Animation(const Animation &) = delete;
	Animation & operator =(const Animation &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // BASE_ANIMATION_H

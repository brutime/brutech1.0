//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Animation.h"

namespace Bru
{

//===========================================================================//

Animation::Animation(integer firstFrameIndex, integer lastFrameIndex, integer frameRate, bool isLooped) :
	_currentFrameIndex(0),
	_firstFrameIndex(firstFrameIndex),
	_lastFrameIndex(lastFrameIndex),
	_length(0),
	_frameAccumulator(0),
	_frameRate(0),
	_timePerFrame(0.0f),
	_isLooped(isLooped),
	_isFinished(false)
{
	_length = _lastFrameIndex - _firstFrameIndex + 1;
	SetFrameRate(frameRate);
}

//===========================================================================//

Animation::~Animation()
{
}

//===========================================================================//

integer Animation::GetCurrentFrameIndex() const
{
	return _currentFrameIndex;
}

//===========================================================================//

integer Animation::GetFirstFrameIndex() const
{
	return _firstFrameIndex;
}

//===========================================================================//

integer Animation::GetLastFrameIndex() const
{
	return _lastFrameIndex;
}

//===========================================================================//

integer Animation::GetFrameRate() const
{
	return _frameRate;
}

//===========================================================================//

void Animation::SetFrameRate(integer frameRate)
{
	_frameRate = frameRate;
	if (_frameRate == 0)
	{
		_timePerFrame = 0.0f;
	}
	else
	{
		_timePerFrame = 1.0f / _frameRate;
	}
}

//===========================================================================//

float Animation::GetTimePerFrame() const
{
	return _timePerFrame;
}

//===========================================================================//

void Animation::SetLooped(bool flag)
{
	_isLooped = flag;
}

//===========================================================================//

bool Animation::IsLooped() const
{
	return _isLooped;
}

//===========================================================================//

void Animation::EnableLooping()
{
	SetLooped(true);
}

//===========================================================================//

void Animation::DisableLooping()
{
	SetLooped(false);
}

//===========================================================================//

bool Animation::IsFinished() const
{
	return _isFinished;
}

//===========================================================================//

}// namespace Bru


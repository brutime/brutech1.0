//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс анимации с PingPong ходом (0-1-2-3-2-1-0)
//

#pragma once
#ifndef PING_PONG_ANIMATION_H
#define PING_PONG_ANIMATION_H

#include "Animation.h"

namespace Bru
{

//===========================================================================//

class PingPongAnimation : public Animation
{
public:
	PingPongAnimation(integer firstFrameIndex, integer lastFrameIndex, integer frameRate, bool isLooped);
	virtual ~PingPongAnimation();

	virtual void Next(integer framesCount);
	virtual void Reset();
	virtual void Finish();

private:
	PingPongAnimation(const PingPongAnimation &) = delete;
	PingPongAnimation & operator =(const PingPongAnimation &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // PING_PONG_ANIMATION_H

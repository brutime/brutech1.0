//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "AnimatedSprite.h"

#include "../../../Utils/Console/Console.h"
#include "../../../Utils/PathHelper/PathHelper.h"
#include "../../../Graphics/TextureManager/TextureManager.h"
#include "Animations/NormalAnimation.h"
#include "Animations/ReverseAnimation.h"
#include "Animations/PingPongAnimation.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(AnimatedSprite, BaseSprite)

//===========================================================================//

const string AnimatedSprite::PathToPathToFile = "PathToFile";
const string AnimatedSprite::PathToIsVisible = "IsVisible";
const string AnimatedSprite::PathToRenderingOrder = "RenderingOrder";

//===========================================================================//

const string AnimatedSprite::PathToTextures = "Textures";
const string AnimatedSprite::TexturePathKey = "Path";
const string AnimatedSprite::TextureFramesByWidthKey = "FramesByWidth";
const string AnimatedSprite::TextureFramesByHeightKey = "FramesByHeight";

const string AnimatedSprite::PathToAnimations = "Animations";
const string AnimatedSprite::AnimationNameKey = "Name";
const string AnimatedSprite::AnimationFirstFrameIndexKey = "FirstFrameIndex";
const string AnimatedSprite::AnimationLastFrameIndexKey = "LastFrameIndex";
const string AnimatedSprite::AnimationFrameRateKey = "FrameRate";
const string AnimatedSprite::AnimationIsLoopedKey = "IsLooped";
const string AnimatedSprite::AnimationPlayingModeKey = "PlayingMode";

//===========================================================================//

AnimatedSprite::AnimatedSprite(const string & domainName, const string & layerName, const string & pathToFile) :
	BaseSprite(domainName, layerName),
	_textureInfos(),
	_animations(),
	_currentAnimationName(),
	_currentAnimation(),
	_currentFrameTime(0.0f),
	_currentAnimationState(AnimationState::Playing)
{
	SetName(string::Format("{0}({1})", GetTypeName(), pathToFile));
	LoadFromFile(pathToFile);
}

//===========================================================================//

AnimatedSprite::AnimatedSprite(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	AnimatedSprite(domainName, string::Empty, provider->Get<string>(PathToPathToFile))
{
	SetVisible(provider->Get<bool>(PathToIsVisible));
	SetRenderingOrder(provider->Get<float>(PathToRenderingOrder));
}

//===========================================================================//

AnimatedSprite::~AnimatedSprite()
{
}

//===========================================================================//

void AnimatedSprite::Reset()
{
	BaseSprite::Reset();
	SelectFirstAnimation();
	Play();
}

//===========================================================================//

void AnimatedSprite::Play()
{
	if (_currentAnimationState == AnimationState::Stoped)
	{
		_currentAnimation->Reset();
		_currentFrameTime = 0.0f;
		UpdateFrameTextureCoords();
	}
	_currentAnimationState = AnimationState::Playing;
}

//===========================================================================//

void AnimatedSprite::Pause()
{
	if (_currentAnimationState == AnimationState::Playing)
	{
		_currentAnimationState = AnimationState::Paused;
	}
}

//===========================================================================//

void AnimatedSprite::Stop()
{
	_currentAnimation->Finish();
	_currentFrameTime = 0.0f;
	_currentAnimationState = AnimationState::Stoped;
	UpdateFrameTextureCoords();
}

//===========================================================================//

bool AnimatedSprite::IsFinished() const
{
	return _currentAnimation->IsFinished();
}

//===========================================================================//

integer AnimatedSprite::GetCurrentFrameIndex() const
{
	return _currentAnimation->GetCurrentFrameIndex();
}

//===========================================================================//

integer AnimatedSprite::GetFirstFrameIndex() const
{
	return _currentAnimation->GetFirstFrameIndex();
}

//===========================================================================//

integer AnimatedSprite::GetLastFrameIndex() const
{
	return _currentAnimation->GetLastFrameIndex();
}

//===========================================================================//

bool AnimatedSprite::IsSelected(const string & animationName) const
{
	return _currentAnimationName == animationName;
}

//===========================================================================//

void AnimatedSprite::SelectFirstAnimation()
{
	const KeyValuePair<string, SharedPtr<Animation >> & pair = _animations.CreateIterator().GetCurrent();
	SelectAnimation(pair.GetKey());
}

//===========================================================================//

void AnimatedSprite::SelectAnimation(const string & animationName)
{
	if (!_animations.Contains(animationName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Animation not found: \"{0}\"", animationName));
	}

	_currentAnimationName = animationName;
	_currentAnimation = _animations[animationName];
	_currentAnimation->Reset();
	_currentFrameTime = 0.0f;
	UpdateFrameTextureCoords();
}

//===========================================================================//

void AnimatedSprite::UpdateCurrentAnimation(float deltaTime)
{
	if (_currentAnimationState != AnimationState::Playing)
	{
		return;
	}

	if (Math::EqualsZero(_currentAnimation->GetTimePerFrame()))
	{
		return;
	}

	_currentFrameTime += deltaTime;
	integer framesCount = Math::Floor(_currentFrameTime / _currentAnimation->GetTimePerFrame());
	if (framesCount == 0)
	{
		return;
	}

	_currentFrameTime -= _currentAnimation->GetTimePerFrame() * framesCount;
	_currentAnimation->Next(framesCount);

	if (_currentAnimation->IsFinished())
	{
		_currentFrameTime = 0.0f;
		_currentAnimationState = AnimationState::Stoped;
	}
	UpdateFrameTextureCoords();
}

//===========================================================================//

void AnimatedSprite::FlipHorizontal()
{
	if (IsFlippedHorizontal())
	{
		return;
	}
	
	UpdateMeshTextureCoords();
	BaseSprite::FlipHorizontal();
}

//===========================================================================//

void AnimatedSprite::UnflipHorizontal()
{
	if (!IsFlippedHorizontal())
	{
		return;
	}
	
	UpdateMeshTextureCoords();
	BaseSprite::UnflipHorizontal();
}

//===========================================================================//

void AnimatedSprite::FlipVertical()
{
	if (IsFlippedVertical())
	{
		return;
	}
	
	UpdateMeshTextureCoords();
	BaseSprite::FlipVertical();
}

//===========================================================================//

void AnimatedSprite::UnflipVertical()
{
	if (!IsFlippedVertical())
	{
		return;
	}
	
	UpdateMeshTextureCoords();
	BaseSprite::UnflipVertical();
}

//===========================================================================//

WeakPtr<ParametersFile> AnimatedSprite::LoadFromFile(const string & pathToFile)
{
	string fullPathToFile = PathHelper::GetFullPathToAnimatedSpriteFile(pathToFile);
	WeakPtr<ParametersFile> file = BaseSprite::LoadFromFile(fullPathToFile);

	//#Textures
	integer firstFrameIndex = 0;
	for (integer i = 0; i < file->GetListChildren(PathToTextures); i++)
	{
		string textureSectionPath = string::Format(PathToTextures + ".{0}.", i);
		string textureName = file->Get<string>(textureSectionPath + TexturePathKey);

		integer framesByWidth = file->Get<integer>(textureSectionPath + TextureFramesByWidthKey);
		integer framesByHeight = file->Get<integer>(textureSectionPath + TextureFramesByHeightKey);

		SharedPtr<AnimationTextureInfo> textureInfo = new AnimationTextureInfo(textureName, framesByWidth, framesByHeight);
		integer lastFrameIndex = firstFrameIndex + framesByWidth * framesByHeight - 1;
		Interval animationFramesInterval(firstFrameIndex, lastFrameIndex);
		_textureInfos.Add(animationFramesInterval, textureInfo);
		firstFrameIndex = lastFrameIndex + 1;
	}

	//#Animations
	for (integer i = 0; i < file->GetListChildren(PathToAnimations); i++)
	{
		LoadAnimation(file, string::Format(PathToAnimations + ".{0}.", i));
	}

	BuildMesh();
	SelectFirstAnimation();

	return file;
}

//===========================================================================//

void AnimatedSprite::LoadAnimation(const WeakPtr<ParametersFile> & file, const string & pathToSection)
{
	SharedPtr<Animation> animation;

	string name = file->Get<string>(pathToSection + AnimationNameKey);
	integer firstFrameIndex = file->Get<integer>(pathToSection + AnimationFirstFrameIndexKey);
	integer lastFrameIndex = file->Get<integer>(pathToSection + AnimationLastFrameIndexKey);
	integer frameRate = file->Get<integer>(pathToSection + AnimationFrameRateKey);
	bool isLooped = file->Get<bool>(pathToSection + AnimationIsLoopedKey);
	integer playingModeIntegerValue = file->Get<integer>(pathToSection + AnimationPlayingModeKey);
	AnimationPlayingMode playingMode = GetPlayingModeByIntegerValue(playingModeIntegerValue);

	switch (playingMode)
	{
	case AnimationPlayingMode::Normal:
		animation = new NormalAnimation(firstFrameIndex, lastFrameIndex, frameRate, isLooped);
		break;

	case AnimationPlayingMode::Reverse:
		animation = new ReverseAnimation(firstFrameIndex, lastFrameIndex, frameRate, isLooped);
		break;

	case AnimationPlayingMode::PingPong:
		animation = new PingPongAnimation(firstFrameIndex, lastFrameIndex, frameRate, isLooped);
		break;

	default:
		INVALID_STATE_EXCEPTION("Not defined playingMode");
	}

	_animations.Add(name, animation);
}

//===========================================================================//

void AnimatedSprite::UpdateFrameTextureCoords()
{
	auto textureInfo = GetTextureInfo(_currentAnimation->GetCurrentFrameIndex());
	if (GetTexture() == NullPtr || GetTexture()->GetName() != textureInfo.GetValue()->TextureName)
	{
		SetTexture(TextureManager->Get(textureInfo.GetValue()->TextureName));
	}
	else
	{
		UpdateMeshTextureCoords();
	}
}

//===========================================================================//

void AnimatedSprite::UpdateMeshTextureCoords()
{
	auto textureInfo = GetTextureInfo(_currentAnimation->GetCurrentFrameIndex());

	integer textureFrameIndex = _currentAnimation->GetCurrentFrameIndex() - textureInfo.GetKey().From;
	integer framesByWidth = textureInfo.GetValue()->FramesByWidth;
	integer framesByHeight = textureInfo.GetValue()->FramesByHeight;
		
	Vector2 textureSizeFactor = GetTextureSizeFactor();
	
	float frameWidth = textureInfo.GetValue()->FrameWidth * textureSizeFactor.X;
	float frameHeight = textureInfo.GetValue()->FrameHeight * textureSizeFactor.Y;

	float textureFrameX = textureFrameIndex % framesByWidth * frameWidth;
	float textureFrameY = (framesByHeight - textureFrameIndex / framesByWidth - 1) * frameHeight;

	if (!IsFlippedHorizontal() && !IsFlippedVertical())
	{
		_mesh->SetVertexTextureCoord(0, {textureFrameX, textureFrameY});
		_mesh->SetVertexTextureCoord(1, {textureFrameX + frameWidth, textureFrameY});
		_mesh->SetVertexTextureCoord(2, {textureFrameX + frameWidth, textureFrameY + frameHeight});
		_mesh->SetVertexTextureCoord(3, {textureFrameX, textureFrameY + frameHeight});
	}
	else if (IsFlippedHorizontal() && !IsFlippedVertical())
	{
		_mesh->SetVertexTextureCoord(0, {textureFrameX + frameWidth, textureFrameY});
		_mesh->SetVertexTextureCoord(1, {textureFrameX, textureFrameY});
		_mesh->SetVertexTextureCoord(2, {textureFrameX, textureFrameY + frameHeight});
		_mesh->SetVertexTextureCoord(3, {textureFrameX + frameWidth, textureFrameY + frameHeight});
	}
	else if (!IsFlippedHorizontal() && IsFlippedVertical())
	{
		_mesh->SetVertexTextureCoord(0, {textureFrameX, textureFrameY + frameHeight});
		_mesh->SetVertexTextureCoord(1, {textureFrameX + frameWidth, textureFrameY + frameHeight});
		_mesh->SetVertexTextureCoord(2, {textureFrameX + frameWidth, textureFrameY});
		_mesh->SetVertexTextureCoord(3, {textureFrameX, textureFrameY});
	}
	else
	{
		_mesh->SetVertexTextureCoord(0, {textureFrameX + frameWidth, textureFrameY + frameHeight});
		_mesh->SetVertexTextureCoord(1, {textureFrameX, textureFrameY + frameHeight});
		_mesh->SetVertexTextureCoord(2, {textureFrameX, textureFrameY});
		_mesh->SetVertexTextureCoord(3, {textureFrameX + frameWidth, textureFrameY});
	}
}

//===========================================================================//

KeyValuePair<Interval, SharedPtr<AnimationTextureInfo>> AnimatedSprite::GetTextureInfo(integer currentFrameIndex) const
{
	for (const auto & textureInfoPair : _textureInfos)
	{
		if (textureInfoPair.GetKey().Contains(currentFrameIndex))
		{
			return KeyValuePair<Interval, SharedPtr<AnimationTextureInfo >> (textureInfoPair.GetKey(), textureInfoPair.GetValue());
		}
	}

	string animationInterval;
	for (auto & textureInfoPair : _textureInfos)
	{
		animationInterval += string::Format("([{0}..{1}]) ", textureInfoPair.GetKey().From, textureInfoPair.GetKey().To);
	}

	//Убраем последний(лишний) пробел
	if (animationInterval.GetLength() > 1)
	{
		animationInterval = animationInterval.Remove(animationInterval.GetLength() - 1);
	}

	INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("current frame index out of bounds: {0} ({1})", currentFrameIndex, animationInterval));
}

//===========================================================================//

AnimationPlayingMode AnimatedSprite::GetPlayingModeByIntegerValue(integer value) const
{
	switch (value)
	{
	case 0: return AnimationPlayingMode::Normal;
	case 1: return AnimationPlayingMode::Reverse;
	case 2: return AnimationPlayingMode::PingPong;

	default: INVALID_ARGUMENT_EXCEPTION(string::Format("Unknown value {0}", value));
	}
}

//===========================================================================//

} //namespace Bru

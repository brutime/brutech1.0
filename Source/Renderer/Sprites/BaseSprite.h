//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: базовый класс спрайтов
//

#pragma once
#ifndef BASE_SPRITE_H
#define BASE_SPRITE_H

#include "../../Utils/ParametersFile/ParametersFile.h"
#include "../Renderable.h"

namespace Bru
{

//===========================================================================//

class BaseSprite : public Renderable
{
public:
	DECLARE_COMPONENT

	virtual ~BaseSprite();

	virtual void Reset();

	void SetWidth(float width);
	float GetWidth() const;

	void SetHeight(float height);
	float GetHeight() const;

	void SetSize(float width, float height);
	void SetSize(const Size & size);

	const Size & GetSize() const;
	const Size & GetSizeResetValue() const;

	virtual void FlipHorizontal();
	virtual void UnflipHorizontal();
	bool IsFlippedHorizontal() const;

	virtual void FlipVertical();
	virtual void UnflipVertical();
	bool IsFlippedVertical() const;

protected:
	BaseSprite(const string & domainName, const string & layerName);

	virtual WeakPtr<ParametersFile> LoadFromFile(const string & fullPathToFile);
	
	void BuildMesh();

	virtual void UpdateMeshVerticesPosition();
	virtual void UpdateMeshTextureCoords();
	
	virtual void UpdateOrigin();

	Size _size;
	Size _sizeResetValue;

	float _u0;
	float _v0;
	float _u1;
	float _v1;

private:
	static const string WidthKey;
	static const string HeightKey;
	static const string OriginFactorKey;
	static const string ColorKey;

	bool _isFlipHorizontal;
	bool _isFlipVertical;

protected:
	InnerEvent<const EmptyEventArgs &> _sizeChangedEvent;

public:
	Event<const EmptyEventArgs &> SizeChangedEvent;

private:
	BaseSprite(const BaseSprite &) = delete;
	BaseSprite & operator =(const BaseSprite &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // BASE_SPRITE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseSprite.h"

#include "../../Utils/PathHelper/PathHelper.h"
#include "../../Utils/ParametersFileManager/ParametersFileManager.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(BaseSprite, Renderable)

//===========================================================================//

const string BaseSprite::WidthKey = "Width";
const string BaseSprite::HeightKey = "Height";
const string BaseSprite::OriginFactorKey = "OriginFactor";
const string BaseSprite::ColorKey = "Color";

//===========================================================================//

BaseSprite::BaseSprite(const string & domainName, const string & layerName) :
	Renderable(domainName, layerName, VerticesMode::Triangles),
	_size(),
	_sizeResetValue(),
	_u0(0.0f),
	_v0(0.0f),
	_u1(0.0f),
	_v1(0.0f),
	_isFlipHorizontal(false),
	_isFlipVertical(false),
	_sizeChangedEvent(),
	SizeChangedEvent(_sizeChangedEvent)
{
}

//===========================================================================//

BaseSprite::~BaseSprite()
{
}

//===========================================================================//

void BaseSprite::Reset()
{
	Renderable::Reset();
	SetSize(_sizeResetValue);
	UnflipHorizontal();
	UnflipVertical();
}

//===========================================================================//

void BaseSprite::SetWidth(float width)
{
	_size.Width = width;
	UpdateMeshVerticesPosition();
	_sizeChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

float BaseSprite::GetWidth() const
{
	return _size.Width;
}

//===========================================================================//

void BaseSprite::SetHeight(float height)
{
	_size.Height = height;
	UpdateMeshVerticesPosition();
	_sizeChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

float BaseSprite::GetHeight() const
{
	return _size.Height;
}

//===========================================================================//

void BaseSprite::SetSize(float width, float height)
{
	SetSize(Size(width, height));
}

//===========================================================================//

void BaseSprite::SetSize(const Size & size)
{
	_size = size;
	UpdateMeshVerticesPosition();
	_sizeChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

const Size & BaseSprite::GetSize() const
{
	return _size;
}

//===========================================================================//

const Size & BaseSprite::GetSizeResetValue() const
{
	return _sizeResetValue;
}

//===========================================================================//

void BaseSprite::FlipHorizontal()
{
	if (!_isFlipHorizontal)
	{
		_isFlipHorizontal = true;
	}
}

//===========================================================================//

void BaseSprite::UnflipHorizontal()
{
	if (_isFlipHorizontal)
	{
		_isFlipHorizontal = false;
	}
}

//===========================================================================//

bool BaseSprite::IsFlippedHorizontal() const
{
	return _isFlipHorizontal;
}

//===========================================================================//

void BaseSprite::FlipVertical()
{
	if (!_isFlipVertical)
	{
		_isFlipVertical = true;
	}
}

//===========================================================================//

void BaseSprite::UnflipVertical()
{
	if (_isFlipVertical)
	{
		_isFlipVertical = false;
	}
}

//===========================================================================//

bool BaseSprite::IsFlippedVertical() const
{
	return _isFlipVertical;
}

//===========================================================================//

WeakPtr<ParametersFile> BaseSprite::LoadFromFile(const string & fullPathToFile)
{
	WeakPtr<ParametersFile> file = ParametersFileManager->Get(fullPathToFile);

	_size.Width = file->Get<float>(WidthKey);
	_size.Height = file->Get<float>(HeightKey);
	_sizeResetValue = _size;
	InternalSetOriginFactor(file->Get<Vector3>(OriginFactorKey));
	InternalSetColor(file->Get<Color>(ColorKey));

	return file;
}

//===========================================================================//

void BaseSprite::BuildMesh()
{
	_mesh->Resize(4, 6);

	UpdateOrigin();

	Vector2 textureSizeFactor = GetTextureSizeFactor();

	_mesh->SetVertex(0, {-_origin.X, -_origin.Y, -_origin.Z},                              {_u0 * textureSizeFactor.X, _v0 * textureSizeFactor.Y}, GetColor());
	_mesh->SetVertex(1, {-_origin.X + _size.Width, -_origin.Y, -_origin.Z},                {_u1 * textureSizeFactor.X, _v0 * textureSizeFactor.Y}, GetColor());
	_mesh->SetVertex(2, {-_origin.X + _size.Width, -_origin.Y + _size.Height, -_origin.Z}, {_u1 * textureSizeFactor.X, _v1 * textureSizeFactor.Y}, GetColor());
	_mesh->SetVertex(3, {-_origin.X, -_origin.Y + _size.Height, -_origin.Z},               {_u0 * textureSizeFactor.X, _v1 * textureSizeFactor.Y}, GetColor());
	
	UpdateWorldVertices();

	_mesh->SetIndex(0, 0);
	_mesh->SetIndex(1, 1);
	_mesh->SetIndex(2, 2);
	_mesh->SetIndex(3, 2);
	_mesh->SetIndex(4, 3);
	_mesh->SetIndex(5, 0);
}

//===========================================================================//

void BaseSprite::UpdateMeshVerticesPosition()
{
	UpdateOrigin();
	_mesh->SetVertexPosition(0, {-_origin.X, -_origin.Y, -_origin.Z});
	_mesh->SetVertexPosition(1, {-_origin.X + _size.Width, -_origin.Y, -_origin.Z});
	_mesh->SetVertexPosition(2, {-_origin.X + _size.Width, -_origin.Y + _size.Height, -_origin.Z});
	_mesh->SetVertexPosition(3, {-_origin.X, -_origin.Y + _size.Height, -_origin.Z});
	UpdateWorldVertices();
}

//===========================================================================//

void BaseSprite::UpdateMeshTextureCoords()
{
	Vector2 textureSizeFactor = GetTextureSizeFactor();
	
	_mesh->SetVertexTextureCoord(0, {_u0 * textureSizeFactor.X, _v0 * textureSizeFactor.Y});
	_mesh->SetVertexTextureCoord(1, {_u1 * textureSizeFactor.X, _v0 * textureSizeFactor.Y});
	_mesh->SetVertexTextureCoord(2, {_u1 * textureSizeFactor.X, _v1 * textureSizeFactor.Y});
	_mesh->SetVertexTextureCoord(3, {_u0 * textureSizeFactor.X, _v1 * textureSizeFactor.Y});
}

//===========================================================================//

void BaseSprite::UpdateOrigin()
{
	_origin = { _size.Width * GetOriginFactor().X, _size.Height * GetOriginFactor().Y, 0.0f };
}

//===========================================================================//

} // namespace Bru

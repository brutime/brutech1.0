//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Sprite.h"

#include "../../../Utils/Console/Console.h"
#include "../../../Utils/PathHelper/PathHelper.h"
#include "../../../Graphics/TextureManager/TextureManager.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(Sprite, BaseSprite)

//===========================================================================//

const string Sprite::PathToPathToFile = "PathToFile";
const string Sprite::PathToIsVisible = "IsVisible";
const string Sprite::PathToRenderingOrder = "RenderingOrder";

const string Sprite::PathToTexture = "Texture";
const string Sprite::TexturePathKey = "Path";
const string Sprite::TextureCoordU0Key = "U0";
const string Sprite::TextureCoordV0Key = "V0";
const string Sprite::TextureCoordU1Key = "U1";
const string Sprite::TextureCoordV1Key = "V1";

//===========================================================================//

Sprite::Sprite(const string & domainName, const string & layerName, const string & pathToFile) :
	BaseSprite(domainName, layerName)
{
	SetName(string::Format("{0}({1})", TypeName, pathToFile));
	LoadFromFile(pathToFile);	
}

//===========================================================================//

Sprite::Sprite(const string & domainName, const string & layerName, const Size & size, const WeakPtr<Texture> & texture,
               const Vector3 & originFactor, float u0, float v0, float u1, float v1) :
	BaseSprite(domainName, layerName)
{
	SetName(string::Format("{0}({1})", TypeName, texture->GetName()));

	_size = size;
	_sizeResetValue = _size;
	InternalSetTexture(texture);
	InternalSetOriginFactor(originFactor);

	_u0 = u0;
	_v0 = v0;
	_u1 = u1;
	_v1 = v1;
		
	BuildMesh();	
}

//===========================================================================//

Sprite::Sprite(const string & domainName, const string & layerName, const Size & size, const Color & color,
               const Vector3 & originFactor) :
	BaseSprite(domainName, layerName)
{
	SetName(string::Format("{0}({1})", TypeName, color.ToString()));

	_size = size;
	_sizeResetValue = _size;
	InternalSetOriginFactor(originFactor);
	InternalSetColor(color);
	BuildMesh();
}

//===========================================================================//

Sprite::Sprite(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	Sprite(domainName, string::Empty, provider->Get<string>(PathToPathToFile))
{
	SetVisible(provider->Get<bool>(PathToIsVisible));
	SetRenderingOrder(provider->Get<float>(PathToRenderingOrder));
}

//===========================================================================//

Sprite::~Sprite()
{
}

//===========================================================================//

void Sprite::FlipHorizontal()
{
	if (IsFlippedHorizontal())
	{
		return;
	}
	
	FlipTextureCoordsHorizontal();
	BaseSprite::FlipHorizontal();
}

//===========================================================================//

void Sprite::UnflipHorizontal()
{
	if (!IsFlippedHorizontal())
	{
		return;
	}
	
	FlipTextureCoordsHorizontal();
	BaseSprite::UnflipHorizontal();
}

//===========================================================================//

void Sprite::FlipVertical()
{
	if (IsFlippedVertical())
	{
		return;
	}
	
	FlipTextureCoordsVertical();
	BaseSprite::FlipVertical();
}

//===========================================================================//

void Sprite::UnflipVertical()
{
	if (!IsFlippedVertical())
	{
		return;
	}
	
	FlipTextureCoordsVertical();
	BaseSprite::UnflipVertical();
}

//===========================================================================//

WeakPtr<ParametersFile> Sprite::LoadFromFile(const string & pathToFile)
{
	string fullPathToFile = PathHelper::GetFullPathToSpriteFile(pathToFile);
	WeakPtr<ParametersFile> file = BaseSprite::LoadFromFile(fullPathToFile);

	//#Texture
	if (file->ContainsSection(PathToTexture))
	{
		_u0 = file->Get<float>(PathToTexture + "." + TextureCoordU0Key);
		_v0 = file->Get<float>(PathToTexture + "." + TextureCoordV0Key);
		_u1 = file->Get<float>(PathToTexture + "." + TextureCoordU1Key);
		_v1 = file->Get<float>(PathToTexture + "." + TextureCoordV1Key);
		InternalSetTexture(TextureManager->Get(file->Get<string>(PathToTexture + "." + TexturePathKey)));
	}

	BuildMesh();

	return file;
}

//===========================================================================//

void Sprite::FlipTextureCoordsHorizontal()
{
	Vector2 textureCoord0 = _mesh->GetVertexTextureCoord(0);
	Vector2 textureCoord1 = _mesh->GetVertexTextureCoord(1);
	Vector2 textureCoord2 = _mesh->GetVertexTextureCoord(2);
	Vector2 textureCoord3 = _mesh->GetVertexTextureCoord(3);

	_mesh->SetVertexTextureCoord(0, textureCoord1);
	_mesh->SetVertexTextureCoord(1, textureCoord0);
	_mesh->SetVertexTextureCoord(2, textureCoord3);
	_mesh->SetVertexTextureCoord(3, textureCoord2);
}

//===========================================================================//

void Sprite::FlipTextureCoordsVertical()
{
	Vector2 textureCoord0 = _mesh->GetVertexTextureCoord(0);
	Vector2 textureCoord1 = _mesh->GetVertexTextureCoord(1);
	Vector2 textureCoord2 = _mesh->GetVertexTextureCoord(2);
	Vector2 textureCoord3 = _mesh->GetVertexTextureCoord(3);

	_mesh->SetVertexTextureCoord(0, textureCoord3);
	_mesh->SetVertexTextureCoord(1, textureCoord2);
	_mesh->SetVertexTextureCoord(2, textureCoord1);
	_mesh->SetVertexTextureCoord(3, textureCoord0);
}

//===========================================================================//

} //namespace Bru

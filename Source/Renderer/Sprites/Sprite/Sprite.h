//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: прямоугольник с текстурой
//

#pragma once
#ifndef SPRITE_H
#define SPRITE_H

#include "../BaseSprite.h"

namespace Bru
{

//===========================================================================//

class Sprite : public BaseSprite
{
public:
	DECLARE_COMPONENT

	Sprite(const string & domainName, const string & layerName, const string & pathToFile);

	Sprite(const string & domainName, const string & layerName, const Size & size, const WeakPtr<Texture> & texture,
	       const Vector3 & originFactor = {}, float u0 = 0.0f, float v0 = 0.0f, float u1 = 1.0f, float v1 = 1.0f);

	Sprite(const string & domainName, const string & layerName, const Size & size, const Color & color,
	       const Vector3 & originFactor = {});

	Sprite(const string & domainName, const SharedPtr<BaseDataProvider> & provider);

	virtual ~Sprite();

	virtual void FlipHorizontal();
	virtual void UnflipHorizontal();

	virtual void FlipVertical();
	virtual void UnflipVertical();

protected:
	virtual WeakPtr<ParametersFile> LoadFromFile(const string & pathToFile);

private:
	static const string PathToPathToFile;
	static const string PathToIsVisible;
	static const string PathToRenderingOrder;

	static const string PathToTexture;
	static const string TexturePathKey;
	static const string TextureCoordU0Key;
	static const string TextureCoordV0Key;
	static const string TextureCoordU1Key;
	static const string TextureCoordV1Key;

	void FlipTextureCoordsHorizontal();
	void FlipTextureCoordsVertical();

	Sprite(const Sprite &) = delete;
	Sprite & operator = (const Sprite &) = delete;
};

//===========================================================================//

} //namespace Bru

#endif // SPRITE_H

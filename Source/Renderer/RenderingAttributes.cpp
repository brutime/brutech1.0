//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RenderingAttributes.h"

namespace Bru
{

//===========================================================================//

RenderingAttributes::RenderingAttributes() :
	_usesDepthTest(false),
	_depthFunction(DepthFunction::Less),
	_usesBlending(false),
	_sourceBlendFunction(BlendFunction::SourceAlpha),
	_destinationBlendFunction(BlendFunction::OneMinusSourceAlpha),
	_uses2DTexture(false),
	_usesScissorsTest(false),
	_scissorsRectangle(),
	_blendFunctionChangedEvent(),
	BlendFunctionChanged(_blendFunctionChangedEvent)
{
}

//===========================================================================//

RenderingAttributes::~RenderingAttributes()
{
}

//===========================================================================//

bool RenderingAttributes::operator <(const RenderingAttributes & attributes) const
{
	return _usesDepthTest < attributes._usesDepthTest ||
	       _depthFunction < attributes._depthFunction ||
	       _usesBlending < attributes._usesBlending ||
	       _uses2DTexture < attributes._uses2DTexture ||
	       _usesScissorsTest < attributes._usesScissorsTest;
}

//===========================================================================//

bool RenderingAttributes::operator ==(const RenderingAttributes & attributes) const
{
	return _usesDepthTest == attributes._usesDepthTest &&
	       _depthFunction == attributes._depthFunction &&
	       _usesBlending == attributes._usesBlending &&
	       _sourceBlendFunction == attributes._sourceBlendFunction &&
	       _destinationBlendFunction == attributes._destinationBlendFunction &&
	       _uses2DTexture == attributes._uses2DTexture &&
	       _usesScissorsTest == attributes._usesScissorsTest &&
	       _scissorsRectangle == attributes._scissorsRectangle;
}

//===========================================================================//

bool RenderingAttributes::operator !=(const RenderingAttributes & attributes) const
{
	return !(attributes == *this);
}

//===========================================================================//

void RenderingAttributes::UseDepthTest(bool useDepthTest)
{
	_usesDepthTest = useDepthTest;
}

//===========================================================================//

bool RenderingAttributes::UsesDepthTest() const
{
	return _usesDepthTest;
}

//===========================================================================//

void RenderingAttributes::SetDepthFunction(DepthFunction depthFunction)
{
	_depthFunction = depthFunction;
}

//===========================================================================//

DepthFunction RenderingAttributes::GetDepthFunction() const
{
	return _depthFunction;
}

//===========================================================================//

bool RenderingAttributes::UsesBlending() const
{
	return _usesBlending;
}

//===========================================================================//

void RenderingAttributes::SetBlendFunctions(BlendFunction sourceBlendFunction, BlendFunction destinationBlendFunction)
{
	_sourceBlendFunction = sourceBlendFunction;
	_destinationBlendFunction = destinationBlendFunction;
	_blendFunctionChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

void RenderingAttributes::SetSourceBlendFunction(BlendFunction sourceBlendFunction)
{
	_sourceBlendFunction = sourceBlendFunction;
	_blendFunctionChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

BlendFunction RenderingAttributes::GetSourceBlendFunction() const
{
	return _sourceBlendFunction;
}

//===========================================================================//

void RenderingAttributes::SetDestinationBlendFunction(BlendFunction destinationBlendFunction)
{
	_destinationBlendFunction = destinationBlendFunction;
	_blendFunctionChangedEvent.Fire(EmptyEventArgs());
}

//===========================================================================//

BlendFunction RenderingAttributes::GetDestinationBlendFunction() const
{
	return _destinationBlendFunction;
}

//===========================================================================//

bool RenderingAttributes::Uses2DTexture() const
{
	return _uses2DTexture;
}

//===========================================================================//

bool RenderingAttributes::UsesScissorsTest() const
{
	return _usesScissorsTest;
}

//===========================================================================//

const IntRectangle & RenderingAttributes::GetScissorsRectangle() const
{
	return _scissorsRectangle;
}

//===========================================================================//

bool RenderingAttributes::IsBlendFunctionsNeedsBlending() const
{
	return _sourceBlendFunction != BlendFunction::SourceAlpha || _destinationBlendFunction != BlendFunction::OneMinusSourceAlpha;
}

//===========================================================================//

void RenderingAttributes::UseBlending(bool useBlending)
{
	_usesBlending = useBlending;
}

//===========================================================================//

void RenderingAttributes::Use2DTexture(bool use2DTexture)
{
	_uses2DTexture = use2DTexture;
}

//===========================================================================//

void RenderingAttributes::UseScissorsTest(bool useScissorsTest)
{
	_usesScissorsTest = useScissorsTest;
}

//===========================================================================//

void RenderingAttributes::SetScissorsRectangle(const IntRectangle & scissorsRectangle)
{
#ifdef DEBUGGING_STRINGS
	if (scissorsRectangle.Width < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("scissorsRectangle.Width must be >= 0, but it is {0}", scissorsRectangle.Width));
	}

	if (scissorsRectangle.Height < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("scissorsRectangle.Height must be >= 0, but it is {0}", scissorsRectangle.Height));
	}
#endif // DEBUGGING_STRINGS	

	_scissorsRectangle = scissorsRectangle;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс содержащий информацию о геометрии объекта и его атрибутах при отображении
//

#pragma once
#ifndef RENDERABLE_H
#define RENDERABLE_H

#include "../EntitySystem/EntitySystem.h"
#include "../Math/SpaceMaths/BoundingSphere.h"
#include "../Math/SpaceMaths/Transformation.h"
#include "../Graphics/Graphics.h"
#include "../Positioning/BasePositioningNode.h"
#include "RenderingAttributes.h"

namespace Bru
{

//===========================================================================//

class RenderableLayer;

//===========================================================================//

class Renderable : public EntityComponent
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	virtual ~Renderable();

	const SharedPtr<Mesh> & GetMesh() const;

	const Array<Vector3>::SimpleType & GetWorldVertices() const;

	void SetTexture(const WeakPtr<Texture> & texture);
	const WeakPtr<Texture> & GetTexture() const;

	void SetColor(const Color & color);
	const Color & GetColor() const;

	void SetColorAlpha(float alpha);
	float GetColorAlpha() const;

	const SharedPtr<RenderingAttributes> & GetRenderingAttributes() const;

	void SetLayer(const string & layerName, bool forceRegistred = false);
	const SharedPtr<RenderableLayer> & GetLayer() const;

	SharedPtr<BoundingSphere> GetBoundingSphere();

	void SetOriginFactor(const Vector3 & originFactor);
	const Vector3 & GetOriginFactor() const;

	const Vector3 & GetOrigin() const;

	//Rendering order дроное число, чтобы в top-down играх можно было использовать
	//координату Y в качестве порядка отрисовки
	void SetRenderingOrder(float renderingOrder);
	float GetRenderingOrder() const;

	//Используется при VerticesMode::Points, VerticesMode::Lines, VerticesMode::LineStrip, VerticesMode::LinesLoop
	void SetLineThickness(float lineThickness);
	float GetLineThickness() const;

	virtual void SetPositioningNode(const WeakPtr<BasePositioningNode> & positioningNode);
	const SharedPtr<BasePositioningNode> & GetPositioningNode() const;

	virtual void SetVisible(bool isVisible);
	bool IsVisible() const;

	void Show();
	void Hide();

protected:
	static const float DefaultLineThickness;

	Renderable(const string & domainName, const string & layerName, VerticesMode verticesMode);
	Renderable(const string & domainName, VerticesMode verticesMode);

	virtual void Register();
	virtual void Unregister();

	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();

	void UpdateWorldVertices();

	virtual void UpdateMeshVerticesPosition() = 0;
	virtual void UpdateMeshTextureCoords() = 0;

	virtual void UpdateOrigin() = 0;

	void InternalSetColor(const Color & color);
	void InternalSetOriginFactor(const Vector3 & originFactor);
	void InternalSetTexture(const WeakPtr<Texture> & texture);

	void UpdateUsesBlending() const;

	void EnableScissorsTest(const IntRectangle & scissorsRectangle);
	void DisableScissorsTest();

	Vector2 GetTextureSizeFactor() const;

	virtual void OnTransformationChanged(const EmptyEventArgs &);
	virtual void OnBlendFunctionChanged(const EmptyEventArgs &);

	SharedPtr<Mesh> _mesh;
	Array<Vector3>::SimpleType _worldVertices;
	Vector3 _origin;
	Color _color;
	WeakPtr<Texture> _texture;
	SharedPtr<RenderingAttributes> _renderingAttributes;
	SharedPtr<BoundingSphere> _boundingSphere;

private:
	Vector3 _originFactor;
	SharedPtr<RenderableLayer> _layer;
	float _renderingOrder;
	float _lineThickness; //Используется при VerticesMode::Lines

	bool _isVisible;

	SharedPtr<BasePositioningNode> _positioningNode;

	SharedPtr<EventHandler<const EmptyEventArgs &>> _transformationChangedHandler;
	SharedPtr<EventHandler<const EmptyEventArgs &>> _blendFunctionChangedHandler;

protected:
	InnerEvent<const EmptyEventArgs &> _renderingOrderChangedEvent;
	InnerEvent<const EmptyEventArgs &> _visibleChangedEvent;
	InnerEvent<const EmptyEventArgs &> _originFactorChangedEvent;

public:
	Event<const EmptyEventArgs &> RenderingOrderChangedEvent;
	Event<const EmptyEventArgs &> VisibleChangedEvent;
	Event<const EmptyEventArgs &> OriginFactorChangedEvent;

private:
	Renderable(const Renderable & other) = delete;
	Renderable & operator =(const Renderable & other) = delete;

	//для тестирования
	friend class TestRenderableBatchConstructor;
	friend class TestRenderableBatchAddObject;
	friend class TestRenderableBatchIIsObjectSimilarAndClimbs;
};

//===========================================================================//

} // namespace Bru

#endif // RENDERABLE_H

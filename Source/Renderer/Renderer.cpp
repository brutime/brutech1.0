//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Renderer.h"

#include "../Math/SpaceMaths/MatrixMath.h"
#include "../ScriptSystem/ScriptSystem.h"
#include "../Utils/LanguageHelper/LanguageHelper.h"
#include "../Utils/Console/Console.h"
#include "../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"
#include "../Utils/CoreWatcher/CoreWatcher.h"
#include "../Utils/Watcher/Watcher.h"
#include "../Graphics/Graphics.h"

namespace Bru
{

//===========================================================================//

const string RendererSingleton::Name = "Renderer";

//===========================================================================//

SharedPtr<RendererSingleton> Renderer;

//===========================================================================//

RendererSingleton::RendererSingleton(integer surfaceWidth, integer surfaceHeight, integer surfaceCoordsWidth,
                                     integer surfaceCoordsHeight, FullScreenMode fullScreenMode,
                                     VerticalSyncMode verticalSyncMode, const Color & clearColor, ProjectionMode projectionMode) :
	_components(),
	_currentDomainName(),
	_pipelineTransformations(),
	_surfaceCoordsWidth(surfaceCoordsWidth),
	_surfaceCoordsHeight(surfaceCoordsHeight),
	_surfaceCoordsWidthCenter(_surfaceCoordsWidth * 0.5f),
	_surfaceCoordsHeightCenter(_surfaceCoordsHeight *0.5f),
	_surfaceCoordsCenter(_surfaceCoordsWidthCenter, _surfaceCoordsHeightCenter),
	_fullScreenMode(fullScreenMode),
	_verticalSyncMode(verticalSyncMode),
	_projectionMode(ProjectionMode::Identity),                            //Устанавливать _projectionMode нужно через SetProjectionMode
	_fps(0),
	_fpsCounter(new FPSCounter()),
	_surfaceSizeChangedHandler(new EventHandler<const IntSize &>(this, &RendererSingleton::OnSurfaceSizeChanged)),
	_layerVisibleChangedHandler(new EventHandler<const LayerEventArgs &>(this, &RendererSingleton::OnLayerVisibleChanged)),
	_layerAddedEvent(),
	_layerRemovingEvent(),
	_layerVisibleChangedEvent(),
	LayerAddedEvent(_layerAddedEvent),
	LayerRemovingEvent(_layerRemovingEvent),
	LayerVisibleChangedEvent(_layerVisibleChangedEvent)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	if (_fullScreenMode == FullScreenMode::On)
	{
		Display->SelectMode(surfaceWidth, surfaceHeight, GraphicsSingleton::ColorBufferDepth);
		surfaceWidth = Display->GetXResolution();
		surfaceHeight = Display->GetYResolution();
	}

	Surface = new SurfaceSingleton(surfaceWidth, surfaceHeight, clearColor);

	Graphics = new GraphicsSingleton(clearColor);
	Graphics->ResizeViewport(Surface->GetWidth(), Surface->GetHeight());
	verticalSyncMode == VerticalSyncMode::On ? Graphics->EnableVerticalSync() : Graphics->DisableVerticalSync();

	CleanUpUnsuitableDisplayModes();
	PrintDisplayModes();

	InitializePipelineTransformations();

	SetProjectionMode(projectionMode);

	TextureManager = new TextureManagerSingleton();
	FontManager = new FontManagerSingleton();

	AddDomain(EntitySystem::GlobalDomainName);

	Surface->SizeChanged.AddHandler(_surfaceSizeChangedHandler);

	RegisterCommands();

	PerfomanceDataCollector->AddMeasurement(Name);

	CoreWatcher->RegisterVariable("Full screen", _fullScreenMode);
	CoreWatcher->RegisterVariable("Vertical sync", _verticalSyncMode);
	CoreWatcher->RegisterVariable("FPS", _fps);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

RendererSingleton::~RendererSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	CoreWatcher->UnregisterVariableOrExpression("Full screen");
	CoreWatcher->UnregisterVariableOrExpression("Vertical sync");
	CoreWatcher->UnregisterVariableOrExpression("FPS");

	PerfomanceDataCollector->RemoveMeasurement(Name);

	UnregisterCommands();

	Surface->SizeChanged.RemoveHandler(_surfaceSizeChangedHandler);

	RemoveAllDomains();

	_pipelineTransformations.Clear();

	Graphics->SetNo2DTexture();

	FontManager = NullPtr;
	TextureManager = NullPtr;

	Graphics = NullPtr;

	Surface = NullPtr;

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void RendererSingleton::AddDomain(const string & domainName)
{
#ifdef DEBUGGING
	if (!EntitySystem::IsDomainNameValid(domainName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} trying to add domain with invalid name: \"{0}\"", Name, domainName));
	}
#endif

	_components.Add(domainName, List<SharedPtr<RenderableLayer>>());

	Console->Debug(string::Format("{0} added domain: \"{1}\"", Name, domainName));
}

//===========================================================================//

void RendererSingleton::RemoveDomain(const string & domainName)
{
	//Нельзя просто вызвать Clear - нужно проверить, пусты ли слои в домене
	//А такая проверка проходит в RemoveLayer
	ListIterator<SharedPtr<RenderableLayer>> it = _components[domainName].CreateIterator();
	while (it.HasNext())
	{
		const SharedPtr<RenderableLayer> & layer = it.GetCurrent();
		it.Next();

		string layerName = layer->GetName();
		RemoveLayer(domainName, layerName);
	}

	_components.Remove(domainName);

	Console->Debug(string::Format("{0} removed domain: \"{1}\"", Name, domainName));
}

//===========================================================================//

void RendererSingleton::SelectDomain(const string & domainName)
{
	_currentDomainName = domainName;
}

//===========================================================================//

void RendererSingleton::AddLayerToFront(const string & domainName, const SharedPtr<RenderableLayer> & layer)
{
#ifdef DEBUGGING
	if (ContainsLayer(domainName, layer->GetName()))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} already contains layer \"{1}\"", Name, layer->GetName()));
	}
#endif // DEBUGGING
	_components[domainName].Add(layer);

	layer->VisibleChangedEvent.AddHandler(_layerVisibleChangedHandler);
	_layerAddedEvent.Fire(LayerEventArgs(layer));

	Console->Debug(string::Format("{0} added layer: \"{1}\"", Name, layer->GetName()));
}

//===========================================================================//

void RendererSingleton::AddLayerToBack(const string & domainName, const SharedPtr<RenderableLayer> & layer)
{
#ifdef DEBUGGING
	if (ContainsLayer(domainName, layer->GetName()))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} already contains layer \"{1}\"", Name, layer->GetName()));
	}
#endif // DEBUGGING
	_components[domainName].Insert(0, layer);

	layer->VisibleChangedEvent.AddHandler(_layerVisibleChangedHandler);
	_layerAddedEvent.Fire(LayerEventArgs(layer));

	Console->Debug(string::Format("{0} added layer: \"{1}\"", Name, layer->GetName()));
}

//===========================================================================//

void RendererSingleton::RemoveLayer(const string & domainName, const string & layerName)
{
	const SharedPtr<RenderableLayer> & layer = GetLayer(domainName, layerName);

#ifdef DEBUGGING
	if (!layer->IsEmpty())
	{
		PrintRenderables(domainName);
		INVALID_STATE_EXCEPTION(string::Format("{0} trying to remove not empty layer: \"{1}\"", Name, layerName));
	}
#endif // DEBUGGING

	layer->VisibleChangedEvent.RemoveHandler(_layerVisibleChangedHandler);
	_layerRemovingEvent.Fire(LayerEventArgs(layer));

	_components[domainName].Remove(layer);

	Console->Debug(string::Format("{0} removed layer: \"{1}\"", Name, layerName));
}

//===========================================================================//

bool RendererSingleton::ContainsLayer(const string & domainName, const string & layerName) const
{
	for (const SharedPtr<RenderableLayer> & layer : _components[domainName])
	{
		if (layer->GetName() == layerName)
		{
			return true;
		}
	}

	return false;
}

//===========================================================================//

const SharedPtr<RenderableLayer> & RendererSingleton::GetLayer(const string & domainName, const string & layerName) const
{
	for (const SharedPtr<RenderableLayer> & layer : _components[domainName])
	{
		if (layer->GetName() == layerName)
		{
			return layer;
		}
	}

	INVALID_ARGUMENT_EXCEPTION(string::Format("{0} not contains layer \"{1}\" in domain \"{2}\"", Name, layerName, domainName));
}

//===========================================================================//

const List<SharedPtr<RenderableLayer>> & RendererSingleton::GetLayers(const string &domainName) const
{
	return _components[domainName];
}

//===========================================================================//

const SharedPtr<Renderable> & RendererSingleton::WhichIsNearest(const SharedPtr<Renderable> & first,
                                                                const SharedPtr<Renderable> & second) const
{
	const auto & firstLayer = first->GetLayer();
	const auto & secondLayer = second->GetLayer();
	if (firstLayer != secondLayer)
	{
		integer firstLayerIndex = _components[first->GetDomainName()].IndexOf(firstLayer);
		integer secondLayerIndex = _components[first->GetDomainName()].IndexOf(secondLayer);
		return firstLayerIndex >= secondLayerIndex ? first : second;
	}
	else
	{
		float firstRenderingOrder = first->GetRenderingOrder();
		float secondRenderingOrder = second->GetRenderingOrder();
		if (!Math::Equals(firstRenderingOrder, secondRenderingOrder))
		{
			return firstRenderingOrder < secondRenderingOrder ? first : second;
		}
		else
		{
			integer firstIndex = firstLayer->GetRenderables().IndexOf(first);
			integer secondIndex = secondLayer->GetRenderables().IndexOf(second);
			return firstIndex >= secondIndex ? first : second;
		}
	}
}

//===========================================================================//

void RendererSingleton::Render()
{
	PerfomanceDataCollector->StartMeasurement(Name);

	Graphics->BeginFrame();

	SimpleRender(_currentDomainName);
	SimpleRender(EntitySystem::GlobalDomainName);

	Graphics->EndFrame();
	_fps = _fpsCounter->Update(1);

	PerfomanceDataCollector->EndMeasurement(Name);
}

//===========================================================================//

void RendererSingleton::SetClearColor(const Color & clearColor)
{
	Graphics->SetClearColor(clearColor);
}

//===========================================================================//

const Color & RendererSingleton::GetClearColor() const
{
	return Graphics->GetClearColor();
}

//===========================================================================//

void RendererSingleton::SetViewMatrix(ProjectionMode mode, const Matrix4x4 & matrix)
{
	_pipelineTransformations[static_cast<integer>(mode)].ViewMatrix = matrix;

	if (_projectionMode == mode)
	{
		Graphics->LoadViewMatrix(_pipelineTransformations[static_cast<integer>(mode)].ViewMatrix);
	}
	else
	{
		SetProjectionMode(mode);
	}
}

//===========================================================================//

void RendererSingleton::SetProjectionMode(ProjectionMode mode)
{
	_projectionMode = mode;
	Graphics->LoadProjectionMatrix(_pipelineTransformations[static_cast<integer>(_projectionMode)].ProjectionMatrix);
	Graphics->LoadViewMatrix(_pipelineTransformations[static_cast<integer>(_projectionMode)].ViewMatrix);
}

//===========================================================================//

ProjectionMode RendererSingleton::GetProjectionMode() const
{
	return _projectionMode;
}

//===========================================================================//

void RendererSingleton::SetFullScreenMode(FullScreenMode fullScreenMode)
{
	if ((_fullScreenMode == FullScreenMode::On && fullScreenMode == FullScreenMode::On) ||
	    (_fullScreenMode == FullScreenMode::Off && fullScreenMode == FullScreenMode::Off))
	{
		return;
	}

	if (fullScreenMode == FullScreenMode::On)
	{
		Display->SelectMode(Surface->GetWidth(), Surface->GetHeight(), GraphicsSingleton::ColorBufferDepth);
		integer surfaceWidth = Display->GetXResolution();
		integer surfaceHeight = Display->GetYResolution();
		Surface->SetSize(surfaceWidth, surfaceHeight);
		Surface->SetPosition(0, 0);
	}
	else
	{
		Display->RestoreMode();
		Surface->PutToDesktopCenter();
	}

	_fullScreenMode = fullScreenMode;
}

//===========================================================================//

FullScreenMode RendererSingleton::GetFullScreenMode() const
{
	return _fullScreenMode;
}

//===========================================================================//

void RendererSingleton::SwitchFullScreenMode()
{
	if (_fullScreenMode == FullScreenMode::On)
	{
		SetFullScreenMode(FullScreenMode::Off);
	}
	else
	{
		SetFullScreenMode(FullScreenMode::On);
	}
}

//===========================================================================//

void RendererSingleton::SetVerticalSyncMode(VerticalSyncMode verticalSyncMode)
{
	if (verticalSyncMode == VerticalSyncMode::On)
	{
		Graphics->EnableVerticalSync();
	}
	else
	{
		Graphics->DisableVerticalSync();
	}

	_verticalSyncMode = verticalSyncMode;
}

//===========================================================================//

VerticalSyncMode RendererSingleton::GetVerticalSyncMode() const
{
	return _verticalSyncMode;
}

//===========================================================================//

void RendererSingleton::EnableWireMode()
{
	if (Graphics->GetPolygonMode() == PolygonMode::Line)
	{
		return;
	}

	Graphics->SetPolygonMode(PolygonMode::Line);
	Console->Notice("Wire mode enabled");
}

//===========================================================================//

void RendererSingleton::DisableWireMode()
{
	if (Graphics->GetPolygonMode() == PolygonMode::Fill)
	{
		return;
	}

	Graphics->SetPolygonMode(PolygonMode::Fill);
	Console->Notice("Wire mode disabled");
}

//===========================================================================//

float RendererSingleton::GetMinRenderingOrder(const string & domainName, const string & layerName) const
{
	float minRenderingOrder = 0.0f;
	const auto & layer = GetLayer(domainName, layerName);
	for (const auto & renderable : layer->GetRenderables())
	{
		if (renderable->GetRenderingOrder() < minRenderingOrder)
		{
			minRenderingOrder = renderable->GetRenderingOrder();
		}
	}
	return minRenderingOrder;
}

//===========================================================================//

float RendererSingleton::GetMaxRenderingOrder(const string & domainName, const string & layerName) const
{
	float maxRenderingOrder = 0.0f;
	const auto & layer = GetLayer(domainName, layerName);
	for (const auto & renderable : layer->GetRenderables())
	{
		if (renderable->GetRenderingOrder() > maxRenderingOrder)
		{
			maxRenderingOrder = renderable->GetRenderingOrder();
		}
	}
	return maxRenderingOrder;
}

//===========================================================================//

float RendererSingleton::GetMinRenderingOrderExceptRenderable(const SharedPtr<Renderable> & exceptRenderable) const
{
	float minRenderingOrder = 0.0f;
	const auto & layer = GetLayer(exceptRenderable->GetDomainName(), exceptRenderable->GetLayer()->GetName());
	for (const auto & renderable : layer->GetRenderables())
	{
		if (renderable == exceptRenderable)
		{
			continue;
		}

		if (renderable->GetRenderingOrder() < minRenderingOrder)
		{
			minRenderingOrder = renderable->GetRenderingOrder();
		}
	}
	return minRenderingOrder;
}

//===========================================================================//

float RendererSingleton::GetMaxRenderingOrderExceptRenderable(const SharedPtr<Renderable> & exceptRenderable) const
{
	float maxRenderingOrder = 0.0f;
	const auto & layer = GetLayer(exceptRenderable->GetDomainName(), exceptRenderable->GetLayer()->GetName());
	for (const auto & renderable : layer->GetRenderables())
	{
		if (renderable == exceptRenderable)
		{
			continue;
		}

		if (renderable->GetRenderingOrder() > maxRenderingOrder)
		{
			maxRenderingOrder = renderable->GetRenderingOrder();
		}
	}
	return maxRenderingOrder;
}

//===========================================================================//

float RendererSingleton::PixelsToCoords(ProjectionMode mode, float pixels)
{
	return PixelsToCoords(mode, Vector2(pixels, pixels)).Y;
}

//===========================================================================//

Vector2 RendererSingleton::PixelsToCoords(ProjectionMode mode, const Vector2 & pixels)
{
	return pixels * GetPixelsToCoordsFactors(mode);
}

//===========================================================================//

float RendererSingleton::CoordsToPixels(ProjectionMode mode, float coords)
{
	return CoordsToPixels(mode, Vector2(coords, coords)).Y;
}

//===========================================================================//

Vector2 RendererSingleton::CoordsToPixels(ProjectionMode mode, const Vector2 & coords)
{
	return coords / GetPixelsToCoordsFactors(mode);
}

//===========================================================================//

Vector2 RendererSingleton::GetPixelsToCoordsFactors(ProjectionMode projectionMode)
{
	switch (projectionMode)
	{
	case ProjectionMode::Identity:
		INVALID_ARGUMENT_EXCEPTION("Identity projection mode is invalid");

	case ProjectionMode::PixelsOrtho:
		return Vector2(1.0f, 1.0f);

	case ProjectionMode::PercentsOrtho:
		return Vector2(100.0f / Graphics->GetViewport().Width, 100.0f / Graphics->GetViewport().Height);

	case ProjectionMode::Perspective:
		return Vector2(1.0f / (Graphics->GetViewport().Width / _surfaceCoordsWidth),
		               1.0f / (Graphics->GetViewport().Height / _surfaceCoordsHeight));

	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined projection mode");
	}
}

//===========================================================================//

Vector2 RendererSingleton::GetLineThicknessInCoords(float lineThickness, ProjectionMode projectionMode)
{
	return PixelsToCoords(projectionMode, Vector2(lineThickness, lineThickness));
}

//===========================================================================//

const string & RendererSingleton::GetCurrentDomainName() const
{
	return _currentDomainName;
}

//===========================================================================//

float RendererSingleton::GetSurfaceCoordsWidth() const
{
	return _surfaceCoordsWidth;
}

//===========================================================================//

float RendererSingleton::GetSurfaceCoordsHeight() const
{
	return _surfaceCoordsHeight;
}

//===========================================================================//

integer RendererSingleton::GetSurfaceCoordsWidthCenter() const
{
	return _surfaceCoordsWidthCenter;
}

//===========================================================================//

integer RendererSingleton::GetSurfaceCoordsHeightCenter() const
{
	return _surfaceCoordsHeightCenter;
}

//===========================================================================//

Vector2 RendererSingleton::GetSurfaceCoordsCenter() const
{
	return _surfaceCoordsCenter;
}

//===========================================================================//

integer RendererSingleton::GetComponentsCount(const string & domainName) const
{
#ifdef DEBUGGING
	if (!_components.Contains(domainName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} not contains domain \"{1}\"", Name, domainName));
	}
#endif

	return _components[domainName].GetCount();
}

//===========================================================================//

integer RendererSingleton::GetComponentsCount() const
{
	integer count = 0;
	for (auto & pair : _components)
	{
		count += pair.GetValue().GetCount();
	}
	return count;
}

//===========================================================================//

integer RendererSingleton::GetVisibleComponentsCount(const string & domainName) const
{
	integer count = 0;
	for (const SharedPtr<RenderableLayer> & layer : _components[domainName])
	{
		for (Renderable * const renderable : layer->GetRenderables())
		{
			if (renderable->IsVisible())
			{
				count++;
			}
		}
	}
	return count;
}

//===========================================================================//

integer RendererSingleton::GetVisibleComponentsCount() const
{
	return GetVisibleComponentsCount(EntitySystem::GlobalDomainName) + GetVisibleComponentsCount(_currentDomainName);
}

//===========================================================================//

integer RendererSingleton::GetFPS() const
{
	return _fps;
}

//===========================================================================//

void RendererSingleton::PrintRenderables(const string & domainName) const
{
#ifdef DEBUGGING
	if (!_components.Contains(domainName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} not contains domain \"{1}\"", Name, domainName));
	}
#endif

	Console->Hint("Renderables (\"{0}\"):", domainName);
	Console->AddOffset(2);
	for (const SharedPtr<RenderableLayer> & layer : _components[domainName])
	{
		layer->PrintRenderables();
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void RendererSingleton::PrintRenderables() const
{
	Console->Hint("Renderables:");
	Console->AddOffset(2);
	for (auto & pair : _components)
	{
		Console->Hint(string::Format("- {0}:", pair.GetKey()));
		Console->AddOffset(2);
		for (const SharedPtr<RenderableLayer> & layer : pair.GetValue())
		{
			layer->PrintRenderables();
		}
		Console->ReduceOffset(2);
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void RendererSingleton::RemoveAllDomains()
{
	//Нельзя просто вызвать Clear - нужно проверить, пусты ли слои в доменах
	//А такая проверка проходит в RemoveDomain
	TreeMapIterator<string, List<SharedPtr<RenderableLayer>>> it = _components.CreateIterator();
	while (it.HasNext())
	{
		string domainName = it.GetCurrent().GetKey();
		it.Next();

		RemoveDomain(domainName);
	}
}

//===========================================================================//

void RendererSingleton::InitializePipelineTransformations()
{
	PipelineTransformation identity(Matrix4x4::Identity, Matrix4x4::Identity);
	_pipelineTransformations.Add(identity);

	Matrix4x4 pixelOrthoMatrix = MatrixMath::Ortho2D(0.0f, Graphics->GetViewport().Width, 0.0f, Graphics->GetViewport().Height);
	PipelineTransformation pixelsOrtho(pixelOrthoMatrix, Matrix4x4::Identity);
	_pipelineTransformations.Add(pixelsOrtho);

	PipelineTransformation percentsOrtho(MatrixMath::Ortho2D(0.0f, 100.0f, 0.0f, 100.0f), Matrix4x4::Identity);
	_pipelineTransformations.Add(percentsOrtho);

	float aspect = Graphics->GetViewport().Width / static_cast<float>(Graphics->GetViewport().Height);
	PipelineTransformation perspective(MatrixMath::Perspective(90.0f, aspect, 0.1f, 1000.f), Matrix4x4::Identity);
	_pipelineTransformations.Add(perspective);
}

//===========================================================================//

void RendererSingleton::UpdatePipelineTransformations()
{
	Matrix4x4 pixelOrthoMatrix = MatrixMath::Ortho2D(0.0f, Graphics->GetViewport().Width, 0.0f, Graphics->GetViewport().Height);
	PipelineTransformation pixelsOrtho(pixelOrthoMatrix, Matrix4x4::Identity);
	_pipelineTransformations[static_cast<integer>(ProjectionMode::PixelsOrtho)] = pixelsOrtho;

	Matrix4x4 perspectiveViewMatrix = _pipelineTransformations[static_cast<integer>(ProjectionMode::Perspective)].ViewMatrix;
	float aspect = Graphics->GetViewport().Width / static_cast<float>(Graphics->GetViewport().Height);
	PipelineTransformation perspective(MatrixMath::Perspective(90.0f, aspect, 0.1f, 1000.f), perspectiveViewMatrix);
	_pipelineTransformations[static_cast<integer>(ProjectionMode::Perspective)] = perspective;
}

//===========================================================================//

void RendererSingleton::CleanUpUnsuitableDisplayModes()
{
	integer i = 0;
	while (i < Display->GetModes().GetCount())
	{
		const SharedPtr<DisplayMode> & displayMode = Display->GetModes()[i];
		if (displayMode->GetBitsPerPixel() < 16 ||
		    displayMode->GetWidth() > VideoCardCapabilities->GetMaxTextureSize() ||
		    displayMode->GetHeight() > VideoCardCapabilities->GetMaxTextureSize())
		{
			Display->RemoveUnsuitableMode(displayMode);
		}
		else
		{
			i++;
		}
	}
}

//===========================================================================//

void RendererSingleton::PrintDisplayModes() const
{
	Console->Hint("Available display modes:");
	for (const SharedPtr<DisplayMode> & mode : Display->GetModes())
	{
		Console->Hint(string::Format(" - {0}", mode->ToString()));
	}
}

//===========================================================================//

void RendererSingleton::SimpleRender(const string & domainName)
{
	if (domainName.IsEmpty())
	{
		return;
	}

#ifdef DEBUGGING
	if (!_components.Contains(domainName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} not contains domain \"{1}\"", Name, domainName));
	}
#endif

	for (const auto & layer : _components[domainName])
	{
		if (!layer->IsVisible())
		{
			continue;
		}

		SetProjectionMode(layer->GetProjectionMode());

		for (Renderable * const renderable : layer->GetRenderables())
		{
			if (!renderable->IsVisible())
			{
				continue;
			}

			ApplyRenderingAttributes(renderable->GetRenderingAttributes(), renderable->GetTexture());
			const SharedPtr<Mesh> & mesh = renderable->GetMesh();
			VerticesMode verticesMode = mesh->GetVerticesMode();
			if (Graphics->GetPolygonMode() == PolygonMode::Line)
			{
				Graphics->SetLineThickness(1.0f);
			}
			else if (IsNeededToSetLineThickness(verticesMode))
			{
				Graphics->SetLineThickness(renderable->GetLineThickness());
			}
			
			Graphics->DrawElements(verticesMode, mesh->GetVertices(), renderable->GetWorldVertices(), mesh->GetIndices());
		}
	}
}

//===========================================================================//

void RendererSingleton::ApplyRenderingAttributes(const SharedPtr<RenderingAttributes> & attributes,
                                                 const WeakPtr<Texture> & texture)
{
	if (attributes->UsesDepthTest())
	{
		Graphics->EnableDepthTest();
		Graphics->SetDepthFunction(attributes->GetDepthFunction());
	}
	else
	{
		Graphics->DisableDepthTest();
	}

	if (attributes->UsesBlending())
	{
		Graphics->EnableBlending();
		Graphics->SetBlendFunctions(attributes->GetSourceBlendFunction(), attributes->GetDestinationBlendFunction());
	}
	else
	{
		Graphics->DisableBlending();
	}

	if (attributes->Uses2DTexture() && Graphics->GetPolygonMode() == PolygonMode::Fill)
	{
		Graphics->Enable2DTexturing();
		Graphics->Set2DTexture(texture->GetID());
	}
	else
	{
		Graphics->Disable2DTexturing();
	}

	if (attributes->UsesScissorsTest())
	{
		Graphics->EnableScissorsTest();
		Graphics->SetScissorsRectangle(attributes->GetScissorsRectangle());
	}
	else
	{
		Graphics->DisableScissorsTest();
	}
}

//===========================================================================//

void RendererSingleton::SetFullScreenModeCommand(bool flag)
{
	flag ? SetFullScreenMode(FullScreenMode::On) : SetFullScreenMode(FullScreenMode::Off);
}

//===========================================================================//

void RendererSingleton::SetWireModeCommand(bool flag)
{
	flag ? EnableWireMode() : DisableWireMode();
}

//===========================================================================//

void RendererSingleton::SetVerticalSyncModeCommand(bool flag)
{
	flag ? SetVerticalSyncMode(VerticalSyncMode::On) : SetVerticalSyncMode(VerticalSyncMode::Off);
}

//===========================================================================//
bool RendererSingleton::IsNeededToSetLineThickness(VerticesMode verticesMode) const
{
	return verticesMode == VerticesMode::Points || verticesMode == VerticesMode::Line ||
	       verticesMode == VerticesMode::LineStrip || verticesMode == VerticesMode::LinesLoop;
}

//===========================================================================//

void RendererSingleton::OnSurfaceSizeChanged(const IntSize & size)
{
	Graphics->ResizeViewport(size.Width, size.Height);
	UpdatePipelineTransformations();
}

//===========================================================================//

void RendererSingleton::OnLayerVisibleChanged(const LayerEventArgs & args)
{
	_layerVisibleChangedEvent.Fire(args);
}

//===========================================================================//

void RendererSingleton::RegisterCommands()
{
	ScriptSystem->RegisterCommand(new ClassConstCommand<RendererSingleton, void>
	                                  ("DisplayModes", this, &RendererSingleton::PrintDisplayModes));
	ScriptSystem->RegisterCommand(new ClassCommand<RendererSingleton, void, bool>
	                                  ("FullScreen", this, &RendererSingleton::SetFullScreenModeCommand));
	ScriptSystem->RegisterCommand(new ClassConstCommand<RendererSingleton, void, const string &>
	                                  ("Renderables", this, &RendererSingleton::PrintRenderables));
	ScriptSystem->RegisterCommand(new ClassConstCommand<RendererSingleton, void>
	                                  ("Renderables", this, &RendererSingleton::PrintRenderables));
	ScriptSystem->RegisterCommand(new ClassCommand<RendererSingleton, void, bool>
	                                  ("VerticalSync", this, &RendererSingleton::SetVerticalSyncModeCommand));
	ScriptSystem->RegisterCommand(new ClassCommand<RendererSingleton, void, bool>
	                                  ("WireMode", this, &RendererSingleton::SetWireModeCommand));
}

//===========================================================================//

void RendererSingleton::UnregisterCommands()
{
	ScriptSystem->UnregisterCommand("DisplayModes");
	ScriptSystem->UnregisterCommand("FullScreen");
	ScriptSystem->UnregisterCommand("Renderables");
	ScriptSystem->UnregisterCommand("VerticalSync");
	ScriptSystem->UnregisterCommand("WireMode");
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef PRIMITIVES_CONTAINER_H
#define PRIMITIVES_CONTAINER_H

#include "../EntitySystem/EntitySystem.h"
#include "Primitives/RenderableCircle.h"
#include "Primitives/RenderableLine.h"
#include "Primitives/RenderablePoints.h"
#include "Primitives/RenderablePolygon.h"
#include "Primitives/RenderableRectangle.h"

namespace Bru
{

//===========================================================================//

class PrimitivesContainer : public EntityComponent
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	PrimitivesContainer(const string & domainName, const string & layerName);
	PrimitivesContainer(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~PrimitivesContainer();

	void AddPoints(const Array<Vector3> & vertices, const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	               const Vector3 & originFactor = {});

	void RemovePoints(integer pointIndex);

	const List<SharedPtr<RenderablePoints>> & GetPoints() const;
	const SharedPtr<RenderablePoints> & GetPoints(integer pointIndex) const;
	const SharedPtr<RenderablePoints> & GetLastPoints() const;
	integer GetPointsCount() const;
	void ClearPoints();

	void AddLine(const Array<Vector3> & points, const Color & color,
	             float lineThickness = RenderingConstants::DefaultLineThickness, const Vector3 & originFactor = {});

	void RemoveLine(integer lineIndex);

	const List<SharedPtr<RenderableLine>> & GetLines() const;
	const SharedPtr<RenderableLine> & GetLine(integer lineIndex) const;
	const SharedPtr<RenderableLine> & GetLastLine() const;
	integer GetLinesCount() const;
	void ClearLines();

	void AddRectangle(float width, float height, bool isFilled, const Color & color,
	                  float lineThickness = RenderingConstants::DefaultLineThickness, const Vector3 & originFactor = {});
	void AddRectangle(const Vector3 & position, float width, float height, bool isFilled, const Color & color,
	                  float lineThickness = RenderingConstants::DefaultLineThickness, const Vector3 & originFactor = {});
	void AddRectangle(const Vector3 & leftLowerPoint, const Vector3 & rightUpperPoint, bool isFilled, const Color & color,
	                  float lineThickness = RenderingConstants::DefaultLineThickness, const Vector3 & originFactor = {});

	void RemoveRectangle(integer rectangleIndex);

	const List<SharedPtr<RenderableRectangle>> & GetRectangles() const;
	const SharedPtr<RenderableRectangle> & GetRectangle(integer rectangleIndex) const;
	const SharedPtr<RenderableRectangle> & GetLastRectangle() const;
	integer GetRectanglesCount() const;
	void ClearRectangles();

	void AddCircle(float radius, bool isFilled, const Color & color, float lineThickness = RenderingConstants::DefaultLineThickness,
	               const Vector3 & originFactor = {});
	void AddCircle(const Vector3 & position, float radius, bool isFilled, const Color & color,
	               float lineThickness = RenderingConstants::DefaultLineThickness, const Vector3 & originFactor = {});

	void RemoveCircle(integer circleIndex);

	const List<SharedPtr<RenderableCircle>> & GetCircles() const;
	const SharedPtr<RenderableCircle> & GetCircle(integer circleIndex) const;
	const SharedPtr<RenderableCircle> & GetLastCircle() const;
	integer GetCirclesCount() const;
	void ClearCircles();

	void AddPolygon(const Array<Vector3> & vertices, bool isFilled, const Color & color,
	                float lineThickness = RenderingConstants::DefaultLineThickness, const Vector3 & originFactor = {});

	void RemovePolygon(integer triangleIndex);

	const List<SharedPtr<RenderablePolygon>> & GetPolygons() const;
	const SharedPtr<RenderablePolygon> & GetPolygon(integer polygonIndex) const;
	const SharedPtr<RenderablePolygon> & GetLastPolygon() const;
	integer GetPolygonsCount() const;
	void ClearPolygons();

	void Show();
	void Hide();

	void Clear();

private:
	static const string PathToLayerName;

	SharedPtr<RenderableLayer> _layer;

	List<SharedPtr<RenderablePoints>> _points;
	List<SharedPtr<RenderableLine>> _lines;
	List<SharedPtr<RenderableRectangle>> _rectangles;
	List<SharedPtr<RenderableCircle>> _circles;
	List<SharedPtr<RenderablePolygon>> _polygons;
};

//===========================================================================//

} // namespace Bru

#endif // PRIMITIVES_CONTAINER_H

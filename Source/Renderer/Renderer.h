//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Рендерер, реализует одинаковые вещи в разных графический API
//

#pragma once
#ifndef RENDERER_H
#define RENDERER_H

#include "../OperatingSystem/OperatingSystem.h"
#include "../Graphics/Graphics.h"
#include "Controllers/AnimationController.h"
#include "Controllers/ColorAlphaController.h"
#include "Controllers/SpriteSizeController.h"
#include "Controllers/VisibilitySwitchController.h"
#include "Enums/ProjectionMode.h"
#include "Enums/FullScreenMode.h"
#include "Enums/VerticalSyncMode.h"
#include "Helpers/FPSCounter.h"
#include "Camera.h"
#include "RenderableLayer.h"
#include "PipelineTransformation.h"
#include "Primitives/RenderableCircle.h"
#include "Primitives/RenderableLine.h"
#include "Primitives/RenderablePoints.h"
#include "Primitives/RenderablePolygon.h"
#include "Primitives/RenderableRectangle.h"
#include "Sprites/AnimatedSprite/AnimatedSprite.h"
#include "Sprites/Sprite/Sprite.h"
#include "PrimitivesContainer.h"
#include "RenderableText.h"
#include "LayerEventArgs.h"

namespace Bru
{

//===========================================================================//

class RendererSingleton
{
public:
	RendererSingleton(integer surfaceWidth, integer surfaceHeight, integer surfaceCoordsWidth,
	                  integer surfaceCoordsHeight, FullScreenMode fullScreenMode,
	                  VerticalSyncMode verticalSyncMode, const Color & clearColor, ProjectionMode projectionMode);
	~RendererSingleton();

	void AddDomain(const string & domainName);
	void RemoveDomain(const string & domainName);
	void SelectDomain(const string & domainName);

	void AddLayerToFront(const string & domainName, const SharedPtr<RenderableLayer> & layer);
	void AddLayerToBack(const string & domainName, const SharedPtr<RenderableLayer> & layer);
	void RemoveLayer(const string & domainName, const string & layerName);
	bool ContainsLayer(const string & domainName, const string & layerName) const;
	const SharedPtr<RenderableLayer> & GetLayer(const string & domainName, const string & layerName) const;
	const List<SharedPtr<RenderableLayer>> & GetLayers(const string & domainName) const;

	const SharedPtr<Renderable> & WhichIsNearest(const SharedPtr<Renderable> & first, const SharedPtr<Renderable> & second) const;

	void Render();

	void SetClearColor(const Color & clearColor);
	const Color & GetClearColor() const;

	void SetViewMatrix(ProjectionMode mode, const Matrix4x4 & matrix);

	void SetProjectionMode(ProjectionMode mode);
	ProjectionMode GetProjectionMode() const;

	void SetFullScreenMode(FullScreenMode fullScreenMode);
	FullScreenMode GetFullScreenMode() const;
	void SwitchFullScreenMode();

	void SetVerticalSyncMode(VerticalSyncMode verticalSyncMode);
	VerticalSyncMode GetVerticalSyncMode() const;

	void EnableWireMode();
	void DisableWireMode();
	
	float GetMinRenderingOrder(const string & domainName, const string & layerName) const;
	float GetMaxRenderingOrder(const string & domainName, const string & layerName) const;
	
	float GetMinRenderingOrderExceptRenderable(const SharedPtr<Renderable> & exceptRenderable) const;
	float GetMaxRenderingOrderExceptRenderable(const SharedPtr<Renderable> & exceptRenderable) const;
	
	float PixelsToCoords(ProjectionMode mode, float pixels);
	Vector2 PixelsToCoords(ProjectionMode mode, const Vector2 & pixels);
	
	float CoordsToPixels(ProjectionMode mode, float coords);
	Vector2 CoordsToPixels(ProjectionMode mode, const Vector2 & coords);

	Vector2 GetPixelsToCoordsFactors(ProjectionMode projectionMode);
	Vector2 GetLineThicknessInCoords(float lineThickness, ProjectionMode projectionMode);

	const string & GetCurrentDomainName() const;

	float GetSurfaceCoordsWidth() const;
	float GetSurfaceCoordsHeight() const;
	
	integer GetSurfaceCoordsWidthCenter() const;
	integer GetSurfaceCoordsHeightCenter() const;
	
	Vector2 GetSurfaceCoordsCenter() const;

	integer GetComponentsCount(const string & domainName) const;
	integer GetComponentsCount() const;
	integer GetVisibleComponentsCount(const string & domainName) const;
	integer GetVisibleComponentsCount() const;

	integer GetFPS() const;

	void PrintRenderables(const string & domainName) const;
	void PrintRenderables() const;

private:
	static const string Name;

	void RemoveAllDomains();

	void InitializePipelineTransformations();
	void UpdatePipelineTransformations();

	void CleanUpUnsuitableDisplayModes();
	void PrintDisplayModes() const;

	void SimpleRender(const string & domainName);
	void ApplyRenderingAttributes(const SharedPtr<RenderingAttributes> & attributes, const WeakPtr<Texture> & texture);

	void BuildBatches(const string & domainName);	

	void SetFullScreenModeCommand(bool flag);
	void SetWireModeCommand(bool flag);
	void SetVerticalSyncModeCommand(bool flag);

	bool IsNeededToSetLineThickness(VerticesMode verticesMode) const;
	
	void OnSurfaceSizeChanged(const IntSize & size);
	void OnLayerVisibleChanged(const LayerEventArgs & args);

	void RegisterCommands();
	void UnregisterCommands();

	TreeMap<string, List<SharedPtr<RenderableLayer>>> _components;
	string _currentDomainName;

	Array<PipelineTransformation> _pipelineTransformations;

	float _surfaceCoordsWidth; //Ширина поверхности в игровых координатах
	float _surfaceCoordsHeight;//Высота поверхности в игровых координатах
	
	integer _surfaceCoordsWidthCenter;
	integer _surfaceCoordsHeightCenter;
	
	Vector2 _surfaceCoordsCenter;

	FullScreenMode _fullScreenMode;
	VerticalSyncMode _verticalSyncMode;

	ProjectionMode _projectionMode;

	integer _fps;
	SharedPtr<FPSCounter> _fpsCounter;
	
	SharedPtr<EventHandler<const IntSize &>> _surfaceSizeChangedHandler;
	SharedPtr<EventHandler<const LayerEventArgs &>> _layerVisibleChangedHandler;

protected:
	InnerEvent<const LayerEventArgs &> _layerAddedEvent;
	InnerEvent<const LayerEventArgs &> _layerRemovingEvent;
	InnerEvent<const LayerEventArgs &> _layerVisibleChangedEvent;

public:
	Event<const LayerEventArgs &> LayerAddedEvent;
	Event<const LayerEventArgs &> LayerRemovingEvent;
	Event<const LayerEventArgs &> LayerVisibleChangedEvent;

private:

	RendererSingleton(const RendererSingleton &) = delete;
	RendererSingleton & operator =(const RendererSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<RendererSingleton> Renderer;

//===========================================================================//

} // namespace Bru

#endif // RENDERER_H

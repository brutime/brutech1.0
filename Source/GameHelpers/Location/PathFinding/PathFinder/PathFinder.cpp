//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PathFinder.h"

namespace Bru
{

//===========================================================================//

List<Vector3> PathFinder::GetAStarPath(const SharedPtr<NavigationMesh> & navigationMesh,
                                       const Vector3 & startPosition, const Vector3 & targetPosition)
{
	if (!navigationMesh->ContainsCellWithPoint(startPosition))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Can't find path: start position {0} not in navigation mesh", startPosition.ToString()));
	}

	if (!navigationMesh->ContainsCellWithPoint(targetPosition))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Can't find path: target position {0} not in navigation mesh", targetPosition.ToString()));
	}

	const SharedPtr<NavigationMeshCell> & startCell = navigationMesh->GetCellByPoint(startPosition);
	const SharedPtr<NavigationMeshCell> & targetCell = navigationMesh->GetCellByPoint(targetPosition);

	Array<SharedPtr<PathNode>> openNodes;
	Array<SharedPtr<PathNode>> closedNodes;

	SharedPtr<PathNode> startNode = new PathNode();
	startNode->Parent = NullPtr;
	startNode->Cell = startCell;
	startNode->CostFromStart = 0;
	startNode->CostToTarget = GetCellToCellDistance(startCell, targetCell, startCell, targetCell, startPosition, targetPosition);
	startNode->TotalCost = startNode->CostFromStart + startNode->CostToTarget;
	openNodes.Add(startNode);

	while (!openNodes.IsEmpty())
	{
		SharedPtr<PathNode> currentNode = NullPtr;
		float minTotalCost = MaxFloat;

		for (integer i = 0; i < openNodes.GetCount(); ++i)
		{
			const SharedPtr<PathNode> & openNode = openNodes[i];
			if (openNode->TotalCost < minTotalCost)
			{
				minTotalCost = openNode->TotalCost;
				currentNode = openNode;
			}
		}

		if (currentNode->Cell == targetCell)
		{
			return ReconstructPath(targetPosition, currentNode);
		}

		closedNodes.Add(currentNode);
		openNodes.Remove(currentNode);

		//Перебираем всех соседей
		for (const auto & region : currentNode->Cell->NeighborRegions)
		{
			const WeakPtr<NavigationMeshCell> & neighborCell = region.Cell;
			bool isCurrentNodeInClosedNodes = false;
			for (integer i = closedNodes.GetCount() - 1; i >= 0; i--)
			{
				if (neighborCell == closedNodes[i]->Cell)
				{
					isCurrentNodeInClosedNodes = true;
					break;
				}
			}

			if (isCurrentNodeInClosedNodes)
			{
				continue;
			}

			float distanceToNeighbor = GetCellToCellDistance(currentNode->Cell, neighborCell, startCell, targetCell, startPosition, targetPosition);
			float costFromStart = currentNode->CostFromStart + distanceToNeighbor;

			bool isCurrentNodeInOpenNodes = false;
			for (integer i = openNodes.GetCount() - 1; i >= 0; i--)
			{
				const SharedPtr<PathNode> & openNode = openNodes[i];
				if (neighborCell == openNode->Cell)
				{
					isCurrentNodeInOpenNodes = true;

					if (costFromStart < currentNode->CostFromStart)
					{
						openNode->Parent = currentNode;
						openNode->CostFromStart = costFromStart;
						openNode->CostToTarget = GetCellToCellDistance(openNode->Cell, targetCell, startCell, targetCell, startPosition, targetPosition);
						openNode->TotalCost = openNode->CostFromStart + openNode->CostToTarget;
					}

					break;
				}
			}

			if (!isCurrentNodeInOpenNodes)
			{
				SharedPtr<PathNode> neighborNode = new PathNode();
				neighborNode->Parent = currentNode;
				neighborNode->Cell = neighborCell;
				neighborNode->CostFromStart = costFromStart;
				neighborNode->CostToTarget = GetCellToCellDistance(neighborCell, targetCell, startCell, targetCell, startPosition, targetPosition);
				neighborNode->TotalCost = neighborNode->CostFromStart + neighborNode->CostToTarget;
				openNodes.Add(neighborNode);
			}
		}
	}

	INVALID_ARGUMENT_EXCEPTION("Can't find path");
}

//===========================================================================//

List<Vector3> PathFinder::ReconstructPath(const Vector3 & targetPosition, const SharedPtr<PathNode> & targetNode)
{
	List<Vector3> path;

	path.Add(targetPosition);

	SharedPtr<PathNode> currentNode = targetNode;
	while (currentNode != NullPtr && currentNode->Parent != NullPtr)
	{
		SharedPtr<PathNode> neighborNode = currentNode->Parent;
		for (const auto & region : currentNode->Cell->NeighborRegions)
		{
			const Vector3 & connection = region.Connection;
			if (neighborNode->Cell->NeighborRegions.Contains([&](const NavigationMeshCellNeighborRegion & reg) {return reg.Connection == connection;}))
			{
				path.Add(connection);
			}
		}
		currentNode = neighborNode;
	}

	path.Reverse();

	return path;
}

//===========================================================================//

float PathFinder::GetCellToCellDistance(const WeakPtr<NavigationMeshCell> & fromCell,
	                                   const WeakPtr<NavigationMeshCell> & toCell,
	                                   const WeakPtr<NavigationMeshCell> & startCell,
	                                   const WeakPtr<NavigationMeshCell> & targetCell,
	                                   const Vector3 & startPosition, const Vector3 & targetPosition)
{
	if (fromCell == startCell)
	{
		return startPosition.DistanceTo(toCell->GetCenterPosition());
	}

	if (toCell == targetCell)
	{
		return fromCell->GetCenterPosition().DistanceTo(targetPosition);
	}
	
	return fromCell->GetCenterPosition().DistanceTo(toCell->GetCenterPosition());
}

//===========================================================================//

} // namespace Bru

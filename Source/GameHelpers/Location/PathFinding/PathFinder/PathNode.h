//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef PATH_NODE_H
#define PATH_NODE_H

#include "../../../../Common/Common.h"
#include "../NavigationMesh/NavigationMeshCell.h"

namespace Bru
{

//===========================================================================//

struct PathNode
{
	PathNode();	
	~PathNode();

	SharedPtr<PathNode> Parent;
	WeakPtr<NavigationMeshCell> Cell;
	float CostFromStart;
	float CostToTarget;
	float TotalCost;
	
	PathNode(const PathNode &) = delete;
	PathNode& operator =(const PathNode &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PATH_NODE_H
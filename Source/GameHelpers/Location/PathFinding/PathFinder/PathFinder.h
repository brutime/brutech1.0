//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef PATH_FINDER_H
#define PATH_FINDER_H

#include "../../../../Common/Common.h"
#include "../../../../Math/Math.h"
#include "../NavigationMesh/NavigationMesh.h"
#include "PathNode.h"

namespace Bru
{

//===========================================================================//

class PathFinder
{
public:
	static List<Vector3> GetAStarPath(const SharedPtr<NavigationMesh> & navigationMesh,
	                                  const Vector3 & startPosition, const Vector3 & targetPosition);

private:
	static List<Vector3> ReconstructPath(const Vector3 & targetPosition, const SharedPtr<PathNode> & targetNode);
	static float GetCellToCellDistance(const WeakPtr<NavigationMeshCell> & fromCell,
	                                   const WeakPtr<NavigationMeshCell> & toCell,
	                                   const WeakPtr<NavigationMeshCell> & startCell,
	                                   const WeakPtr<NavigationMeshCell> & targetCell,
	                                   const Vector3 & startPosition, const Vector3 & targetPosition);

	PathFinder() = delete;
	PathFinder(const PathFinder &) = delete;
	~PathFinder() = delete;
	PathFinder & operator =(const PathFinder &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PATH_FINDER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//


#include "NavigationMeshVisualizer.h"

namespace Bru
{

//===========================================================================//

const Color NavigationMeshVisualizer::CellsColor = Color(Colors::Gray40, 0.3f);
const Color NavigationMeshVisualizer::CellsLinesColor = Colors::White;
const Color NavigationMeshVisualizer::EdgesColor = Colors::White;
const float NavigationMeshVisualizer::LineThickness = 1.0f;
const float NavigationMeshVisualizer::ConnectionLength = 0.5f;

//===========================================================================//

NavigationMeshVisualizer::NavigationMeshVisualizer(const string & domainName, const string & layerName,
        const string & groupName, const SharedPtr<NavigationMesh> & navigationMesh) :
	_domainName(domainName),
	_layerName(layerName),
	_groupName(groupName),
	_navigationMesh(navigationMesh),
	_cells(),
	_edges(),
	_cellChangedHandler(new EventHandler<const EmptyEventArgs &>(this, &NavigationMeshVisualizer::OnCellsChangedHandler))
{
	_cells = new PrimitivesContainer(_domainName, _layerName);
	_edges = new PrimitivesContainer(_domainName, _layerName);

	OnCellsChangedHandler(EmptyEventArgs());

	_navigationMesh->CellsChanged.AddHandler(_cellChangedHandler);
}

//===========================================================================//

NavigationMeshVisualizer::~NavigationMeshVisualizer()
{
	_navigationMesh->CellsChanged.RemoveHandler(_cellChangedHandler);
	_edges->Clear();
	_cells->Clear();
}

//===========================================================================//

void NavigationMeshVisualizer::OnCellsChangedHandler(const EmptyEventArgs &)
{
	_cells->Clear();

	for (const SharedPtr<NavigationMeshCell> & cell : _navigationMesh->GetCells())
	{
		_cells->AddRectangle(cell->LeftLowerPoint, cell->RightUpperPoint, true, CellsColor);
		_cells->AddRectangle(cell->LeftLowerPoint, cell->RightUpperPoint, false, CellsLinesColor, LineThickness);

		for (const auto & region : cell->NeighborRegions)
		{
			const Vector3 & connection = region.Connection;
			Vector3 startPosition;
			Vector3 endPosition;
			if (Math::Equals(connection.X, cell->LeftLowerPoint.X) || Math::Equals(connection.X, cell->RightUpperPoint.X))
			{
				startPosition = Vector3(connection.X - ConnectionLength * 0.5f, connection.Y, connection.Z);
				endPosition = Vector3(connection.X + ConnectionLength * 0.5f, connection.Y, connection.Z);
			}
			else
			{
				startPosition = Vector3(connection.X, connection.Y - ConnectionLength * 0.5f, connection.Z);
				endPosition = Vector3(connection.X, connection.Y + ConnectionLength * 0.5f, connection.Z);
			}

			_cells->AddLine( {startPosition, endPosition}, CellsLinesColor, LineThickness);
		}
	}

	_edges->Clear();
	for (const SharedPtr<NavigationMeshEdge> & edge : _navigationMesh->GetOuterEdges())
	{
		_edges->AddLine( {edge->StartPoint, edge->EndPoint}, EdgesColor, LineThickness);
	}
}

//===========================================================================//

} // namespace Bru

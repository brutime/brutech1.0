//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef NAVIGATION_MESH_CELL_NEIGHBOR_REGION_H
#define NAVIGATION_MESH_CELL_NEIGHBOR_REGION_H

#include "../../../../Common/Common.h"
#include "../../../../Math/Math.h"

namespace Bru
{

//===========================================================================//

struct NavigationMeshCell;

//===========================================================================//

struct NavigationMeshCellNeighborRegion
{
	NavigationMeshCellNeighborRegion();

	NavigationMeshCellNeighborRegion(const Vector3 & connection, const Vector3 & connectionToCorner, const WeakPtr<NavigationMeshCell> & cell);

	Vector3 Connection;
	Vector3 ConnectionToCorner;
	WeakPtr<NavigationMeshCell> Cell;
};


//===========================================================================//

} // namespace Bru

#endif // NAVIGATION_MESH_CELL_NEIGHBOR_REGION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef NAVIGATION_MESH_H
#define NAVIGATION_MESH_H

#include "../../../../Common/Common.h"
#include "../../../../Utils/ParametersFile/ParametersFile.h"

#include "NavigationMeshCell.h"

namespace Bru
{

//===========================================================================//

class NavigationMesh
{
public:
	static const string GroupName;
	
	NavigationMesh();
	NavigationMesh(const SharedPtr<ParametersFile> & file);
	~NavigationMesh();

	void SaveToFile(const SharedPtr<ParametersFile> & file) const;

	void AddCell(const Vector3 & startPoint, const Vector3 & endPoint);
	void RemoveCell(const Vector3 & startPoint, const Vector3 & endPoint);
	
	bool ContainsCellWithPoint(const Vector3 & point, float radius = 0.0f) const;
	const SharedPtr<NavigationMeshCell> & GetCellByPoint(const Vector3 & point) const;
	Vector2 GetRandomValidPoint(float safetyRadius) const;
	
	const SharedPtr<NavigationMeshCell> & GetCell(integer index) const;
	const Array<SharedPtr<NavigationMeshCell>> & GetCells() const;
	
	const Array<SharedPtr<NavigationMeshEdge>> & GetOuterEdges() const;

private:
	static const string PathToNavigationMesh;
	static const string StartPointKey;
	static const string EndPointKey;
	
	void AddConnections(const SharedPtr<NavigationMeshCell> & cell);
	void RemoveConnections(const SharedPtr<NavigationMeshCell> & cell);
	void RebuildOuterEdges();
	const SharedPtr<NavigationMeshCell> & GetCellByPoints(const Vector3 & startPoint, const Vector3 & endPoint) const;
	ValuePair<Vector3, Vector3> GetNormalizedCellPoints(const Vector3 & startPoint, const Vector3 & endPoint) const;	

	Array<SharedPtr<NavigationMeshCell>> _cells;
	Array<SharedPtr<NavigationMeshEdge>> _outerEdges;

private:
	InnerEvent<const EmptyEventArgs &> _cellsChanged;

public:
	Event<const EmptyEventArgs &> CellsChanged;

private:
	NavigationMesh(const NavigationMesh &) = delete;
	NavigationMesh & operator =(const NavigationMesh &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // NAVIGATION_MESH_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef NAVIGATION_MESH_CELL_H
#define NAVIGATION_MESH_CELL_H

#include "../../../../Common/Common.h"
#include "NavigationMeshCellNeighborRegion.h"

using Rect = Bru::Rectangle;

namespace Bru
{

//===========================================================================//

struct NavigationMeshEdge
{
	NavigationMeshEdge();
	NavigationMeshEdge(const Vector3 & startPoint, const Vector3 & endPoint);
	~NavigationMeshEdge();

	Vector3 StartPoint;
	Vector3 EndPoint;

	NavigationMeshEdge(const NavigationMeshEdge &) = delete;
	NavigationMeshEdge & operator =(const NavigationMeshEdge &) = delete;
};

//===========================================================================//

struct NavigationMeshCell
{
	NavigationMeshCell();
	~NavigationMeshCell();

	bool ContainsPoint(const Vector3 & point, float radius = 0.0f) const;
	Vector3 GetCenterPosition() const;
	static bool IsRegionCovered(const Array<Rect> & region,
	                            const Array<WeakPtr<NavigationMeshCell>> & cellsToCheck,
	                            const Array<WeakPtr<NavigationMeshCell>> & skippedCells, 
								std::function<bool(const NavigationMeshCell *)> neighborFilter);

	Vector3 LeftLowerPoint;
	Vector3 RightUpperPoint;
	Array<NavigationMeshCellNeighborRegion> NeighborRegions;

	NavigationMeshCell(const NavigationMeshCell &) = delete;
	NavigationMeshCell & operator =(const NavigationMeshCell &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // NAVIGATION_MESH_CELL_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "NavigationMeshCell.h"

namespace Bru
{

//===========================================================================//

NavigationMeshEdge::NavigationMeshEdge() :
	StartPoint(),
	EndPoint()
{
}

//===========================================================================//

NavigationMeshEdge::NavigationMeshEdge(const Vector3 & startPoint, const Vector3 & endPoint) :
	StartPoint(startPoint),
	EndPoint(endPoint)
{
}

//===========================================================================//

NavigationMeshEdge::~NavigationMeshEdge()
{
}

//===========================================================================//

NavigationMeshCell::NavigationMeshCell() :
	LeftLowerPoint(),
	RightUpperPoint(),
	NeighborRegions()
{
}

//===========================================================================//

NavigationMeshCell::~NavigationMeshCell()
{
}

//===========================================================================//

bool NavigationMeshCell::ContainsPoint(const Vector3 & point, float radius) const
{
	if (Math::EqualsZero(radius))
	{
		return LeftLowerPoint.X <= point.X && point.X <= RightUpperPoint.X &&
		       LeftLowerPoint.Y <= point.Y && point.Y <= RightUpperPoint.Y;
	}
	else
	{
		const auto & nonCoveredRegion = Rect(point.X - radius, point.Y - radius, radius * 2.0f, radius * 2.0f).
		                                SubtractRectangle(Rect(LeftLowerPoint.X, LeftLowerPoint.Y,
		                                        RightUpperPoint.X - LeftLowerPoint.X,
		                                        RightUpperPoint.Y - LeftLowerPoint.Y));
		if (nonCoveredRegion.IsEmpty())
		{
			return true;
		}
		else
		{
			Array<WeakPtr<NavigationMeshCell>> cellsToCheck;
			for (const auto & region : NeighborRegions)
			{
				cellsToCheck.Add(region.Cell);
			}
			return IsRegionCovered(nonCoveredRegion, cellsToCheck, Array<WeakPtr<NavigationMeshCell>>(),
			                       [&](const NavigationMeshCell * cell) // фильтр, определяющий какие их соседей могут покрыть нужную область
			{
				return (cell->LeftLowerPoint.X - radius < point.X &&
				        cell->LeftLowerPoint.Y - radius < point.Y &&
				        cell->RightUpperPoint.X + radius > point.X &&
				        cell->RightUpperPoint.Y + radius > point.Y);
			});
		}
	}
}

//===========================================================================//

bool NavigationMeshCell::IsRegionCovered(const Array<Rect> & region,
        const Array<WeakPtr<NavigationMeshCell>> & cellsToCheck,
        const Array<WeakPtr<NavigationMeshCell>> & skippedCells,
        std::function<bool(const NavigationMeshCell *)> neighborFilter)
{
	if (region.IsEmpty())
	{
		return true;
	}

	if (cellsToCheck.IsEmpty())
	{
		return false;
	}

	Array<Rect> newRegion;
	for (const auto & regionPiece : region)
	{
		const auto & firstCell = cellsToCheck[0];
		Rect cellRectangle(firstCell->LeftLowerPoint.X, firstCell->LeftLowerPoint.Y,
		                   firstCell->RightUpperPoint.X - firstCell->LeftLowerPoint.X,
		                   firstCell->RightUpperPoint.Y - firstCell->LeftLowerPoint.Y);

		newRegion.Add(regionPiece.SubtractRectangle(cellRectangle));
	}

	if (cellsToCheck.GetCount() == 1)
	{
		return false;
	}
	else
	{
		auto newCellsToCheck = cellsToCheck.GetArray(1, cellsToCheck.GetCount() - 1);
		for (auto neighborRegion : cellsToCheck[0]->NeighborRegions)
		{
			auto neighborCell = neighborRegion.Cell;
			if (!skippedCells.Contains(neighborCell) && neighborFilter(neighborCell))
			{
				newCellsToCheck.Add(neighborCell);
			}
		}
		return IsRegionCovered(newRegion, newCellsToCheck,
		                       Array<WeakPtr<NavigationMeshCell>>(skippedCells).Add(cellsToCheck[0]),
		                       neighborFilter);
	}
}

//===========================================================================//

Vector3 NavigationMeshCell::GetCenterPosition() const
{
	return LeftLowerPoint + Vector3((RightUpperPoint.X - LeftLowerPoint.X) * 0.5f,
	                                (RightUpperPoint.Y - LeftLowerPoint.Y) * 0.5f);
}

//===========================================================================//

} // namespace Bru

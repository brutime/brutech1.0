//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef NAVIGATION_MESH_VISUALIZER_H
#define NAVIGATION_MESH_VISUALIZER_H

#include "../../../../Common/Common.h"
#include "../../../../Renderer/Renderer.h"

#include "NavigationMesh.h"

namespace Bru
{

//===========================================================================//

class NavigationMeshVisualizer
{
public:
	NavigationMeshVisualizer(const string & domainName, const string & layerName, const string & groupName,
	                         const SharedPtr<NavigationMesh> & navigationMesh);
	virtual ~NavigationMeshVisualizer();

private:
	static const Color CellsColor;
	static const Color CellsLinesColor;
	static const Color EdgesColor;
	static const float LineThickness;
	static const float ConnectionLength;

	void OnCellsChangedHandler(const EmptyEventArgs &);

	string _domainName;
	string _layerName;
	string _groupName;

	SharedPtr<NavigationMesh> _navigationMesh;
	SharedPtr<PrimitivesContainer> _cells;
	SharedPtr<PrimitivesContainer> _edges;

	SharedPtr<EventHandler<const EmptyEventArgs &>> _cellChangedHandler;

	NavigationMeshVisualizer(const NavigationMeshVisualizer &) = delete;
	NavigationMeshVisualizer & operator =(const NavigationMeshVisualizer &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // NAVIGATION_MESH_VISUALIZER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "NavigationMesh.h"

namespace Bru
{

//===========================================================================//

const string NavigationMesh::GroupName = "NavigationMesh";

const string NavigationMesh::PathToNavigationMesh = "NavigationMesh";
const string NavigationMesh::StartPointKey = "StartPoint";
const string NavigationMesh::EndPointKey = "EndPoint";

//===========================================================================//

NavigationMesh::NavigationMesh() :
	_cells(),
	_outerEdges(),
	_cellsChanged(),
	CellsChanged(_cellsChanged)
{
}

//===========================================================================//

NavigationMesh::NavigationMesh(const SharedPtr<ParametersFile> & file) :
	NavigationMesh()
{
	if (!file->ContainsSection(PathToNavigationMesh))
	{
		return;
	}
	
	integer cellsCount = file->GetListChildren(PathToNavigationMesh);
	for (integer i = 0; i < cellsCount; ++i)
	{
		string path = string::Format("{0}.{1}", PathToNavigationMesh, i);

		Vector3 startPoint = file->Get<Vector3>(path + "." + StartPointKey);
		Vector3 endPoint = file->Get<Vector3>(path + "." + EndPointKey);

		AddCell(startPoint, endPoint);
	}
}

//===========================================================================//

NavigationMesh::~NavigationMesh()
{
}

//===========================================================================//

void NavigationMesh::SaveToFile(const SharedPtr<ParametersFile> & file) const
{
	for (integer i = 0; i < _cells.GetCount(); ++i)
	{
		string path = string::Format("{0}.{1}", PathToNavigationMesh, i);

		const SharedPtr<NavigationMeshCell> & cell = _cells[i];

		file->Set<Vector3>(path + "." + StartPointKey, cell->LeftLowerPoint);
		file->Set<Vector3>(path + "." + EndPointKey, cell->RightUpperPoint);
	}
}

//===========================================================================//

void NavigationMesh::AddCell(const Vector3 & startPoint, const Vector3 & endPoint)
{
	SharedPtr<NavigationMeshCell> cell = new NavigationMeshCell();

	ValuePair<Vector3, Vector3> normalizedCellPoints = GetNormalizedCellPoints(startPoint, endPoint);
	cell->LeftLowerPoint = normalizedCellPoints.First;
	cell->RightUpperPoint = normalizedCellPoints.Second;

	AddConnections(cell);
	_cells.Add(cell);
	RebuildOuterEdges();

	_cellsChanged.Fire(EmptyEventArgs());
}

//===========================================================================//

void NavigationMesh::RemoveCell(const Vector3 & startPoint, const Vector3 & endPoint)
{
	const SharedPtr<NavigationMeshCell> & cell = GetCellByPoints(startPoint, endPoint);
	RemoveConnections(cell);
	_cells.Remove(cell);
	RebuildOuterEdges();

	_cellsChanged.Fire(EmptyEventArgs());
}

//===========================================================================//

bool NavigationMesh::ContainsCellWithPoint(const Vector3 & point, float radius) const
{
	for (const SharedPtr<NavigationMeshCell> & cell : _cells)
	{
		if (cell->ContainsPoint(point, radius))
		{
			return true;
		}
	}

	return false;
}

//===========================================================================//

const SharedPtr<NavigationMeshCell> & NavigationMesh::GetCellByPoint(const Vector3 & point) const
{
	for (const SharedPtr<NavigationMeshCell> & cell : _cells)
	{
		if (cell->ContainsPoint(point))
		{
			return cell;
		}
	}

	INVALID_ARGUMENT_EXCEPTION(string::Format("Navigation mesh not contains cell, which contains point {0}", point.ToString()));
}

//===========================================================================//

Vector2 NavigationMesh::GetRandomValidPoint(float safetyRadius) const
{
#ifdef DEBUG
	const integer IterationsWarningThreshold = 20;
	integer iterationsCount = 0;
#endif

	while (true)
	{
		const SharedPtr<NavigationMeshCell> & cell = _cells[Math::RandomInRange(0, _cells.GetCount() - 1)];
		const Vector3 position = Vector3(Math::RandomInRange(cell->LeftLowerPoint.X, cell->RightUpperPoint.X), Math::RandomInRange(cell->LeftLowerPoint.Y, cell->RightUpperPoint.Y), 0.0f);
		if (ContainsCellWithPoint(position, safetyRadius))
		{
			return Vector2(position.X, position.Y);
		}
#ifdef DEBUG
		iterationsCount++;
		if (iterationsCount > IterationsWarningThreshold)
		{
			Console->Warning(string::Format("NavigationMesh::GetRandomValidPoint could not find valid point in {0} iterations", IterationsWarningThreshold));
			iterationsCount = 0;
		}
#endif
	}
}

//===========================================================================//

const SharedPtr<NavigationMeshCell> & NavigationMesh::GetCell(integer index) const
{
	return _cells[index];
}

//===========================================================================//

const Array<SharedPtr<NavigationMeshCell>> & NavigationMesh::GetCells() const
{
	return _cells;
}

//===========================================================================//

const Array<SharedPtr<NavigationMeshEdge>> & NavigationMesh::GetOuterEdges() const
{
	return _outerEdges;
}

//===========================================================================//

void NavigationMesh::AddConnections(const SharedPtr<NavigationMeshCell> & cell)
{
	for (const SharedPtr<NavigationMeshCell> & neighborCandidateCell : _cells)
	{
		bool isConnectionFound = false;
		Vector3 connectionPosition;
		Vector3 connectionToCorner;
		if (Math::Equals(cell->LeftLowerPoint.X, neighborCandidateCell->RightUpperPoint.X) ||
		    Math::Equals(cell->RightUpperPoint.X, neighborCandidateCell->LeftLowerPoint.X))
		{
			if (neighborCandidateCell->LeftLowerPoint.Y >= cell->RightUpperPoint.Y ||
			    neighborCandidateCell->RightUpperPoint.Y <= cell->LeftLowerPoint.Y)
			{
				continue;
			}

			if (Math::Equals(cell->LeftLowerPoint.X, neighborCandidateCell->RightUpperPoint.X))
			{
				connectionPosition.X = cell->LeftLowerPoint.X;
			}
			else
			{
				connectionPosition.X = cell->RightUpperPoint.X;
			}

			float bottomY = Math::Max(cell->LeftLowerPoint.Y, neighborCandidateCell->LeftLowerPoint.Y);
			float topY = Math::Min(cell->RightUpperPoint.Y, neighborCandidateCell->RightUpperPoint.Y);

			connectionPosition.Y = bottomY + (topY - bottomY) * 0.5f;
			connectionToCorner = {0.f, (topY - bottomY) * 0.5f, 0.f};
			isConnectionFound = true;
		}
		else if (Math::Equals(cell->LeftLowerPoint.Y, neighborCandidateCell->RightUpperPoint.Y) ||
		         Math::Equals(cell->RightUpperPoint.Y, neighborCandidateCell->LeftLowerPoint.Y))
		{
			if (neighborCandidateCell->LeftLowerPoint.X >= cell->RightUpperPoint.X ||
			    neighborCandidateCell->RightUpperPoint.X <= cell->LeftLowerPoint.X)
			{
				continue;
			}

			float leftX = Math::Max(cell->LeftLowerPoint.X, neighborCandidateCell->LeftLowerPoint.X);
			float rightX = Math::Min(cell->RightUpperPoint.X, neighborCandidateCell->RightUpperPoint.X);

			connectionPosition.X = leftX + (rightX - leftX) * 0.5f;

			if (Math::Equals(cell->LeftLowerPoint.Y, neighborCandidateCell->RightUpperPoint.Y))
			{
				connectionPosition.Y = cell->LeftLowerPoint.Y;
			}
			else
			{
				connectionPosition.Y = cell->RightUpperPoint.Y;
			}

			connectionToCorner = {(rightX - leftX) * 0.5f, 0.f, 0.f};
			isConnectionFound = true;
		}

		if (isConnectionFound)
		{
			cell->NeighborRegions.Add(NavigationMeshCellNeighborRegion(connectionPosition, connectionToCorner, neighborCandidateCell));

			neighborCandidateCell->NeighborRegions.Add(NavigationMeshCellNeighborRegion(connectionPosition, connectionToCorner, cell));
		}
	}
}

//===========================================================================//

void NavigationMesh::RemoveConnections(const SharedPtr<NavigationMeshCell> & cell)
{
	for (const auto & neighbor : cell->NeighborRegions)
	{
		neighbor.Cell->NeighborRegions.Remove([&] (const NavigationMeshCellNeighborRegion & region) { return region.Cell == cell; });
	}
}

//===========================================================================//

void NavigationMesh::RebuildOuterEdges()
{
	//	_outerEdges.Clear();
	//	for (const SharedPtr<NavigationMeshCell> & cell : _cells)
	//	{
	//		_outerEdges.Add(new NavigationMeshEdge(cell->LeftLowerPoint, Vector3(cell->LeftLowerPoint.X, cell->RightUpperPoint.Y)));
	//		_outerEdges.Add(new NavigationMeshEdge(Vector3(cell->LeftLowerPoint.X, cell->RightUpperPoint.Y), cell->RightUpperPoint));
	//		_outerEdges.Add(new NavigationMeshEdge(cell->RightUpperPoint, Vector3(cell->RightUpperPoint.X, cell->LeftLowerPoint.Y)));
	//		_outerEdges.Add(new NavigationMeshEdge(Vector3(cell->RightUpperPoint.X, cell->LeftLowerPoint.Y), cell->LeftLowerPoint));
	//	}
}

//===========================================================================//

const SharedPtr<NavigationMeshCell> & NavigationMesh::GetCellByPoints(
    const Vector3 & startPoint, const Vector3 & endPoint) const
{
	ValuePair<Vector3, Vector3> normalizedCellPoints = GetNormalizedCellPoints(startPoint, endPoint);

	for (integer i = 0; i < _cells.GetCount(); ++i)
	{
		const SharedPtr<NavigationMeshCell> & cell = _cells[i];
		if (cell->LeftLowerPoint == normalizedCellPoints.First && cell->RightUpperPoint == normalizedCellPoints.Second)
		{
			return cell;
		}
	}

	INVALID_ARGUMENT_EXCEPTION(string::Format("Navigation cell with points {0}, {1} not found",
	                                          startPoint.ToString(), endPoint.ToString()));
}

//===========================================================================//

ValuePair<Vector3, Vector3> NavigationMesh::GetNormalizedCellPoints(const Vector3 & startPoint, const Vector3 & endPoint) const
{
	return ValuePair<Vector3, Vector3>
		   {
			   Vector3(Math::Min(startPoint.X, endPoint.X), Math::Min(startPoint.Y, endPoint.Y)),
			   Vector3(Math::Max(startPoint.X, endPoint.X), Math::Max(startPoint.Y, endPoint.Y))
		   };
}

//===========================================================================//

} // namespace Bru

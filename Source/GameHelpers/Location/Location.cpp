//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Location.h"

#include "../../Utils/PathHelper/PathHelper.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/StringTable/StringTable.h"
#include "../../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../../Helpers/EntitiesHelpers/EntityManager.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

const string Location::PathToLocationNames = "LocationNames";
const string Location::DefaultName = "Noname";

const string Location::PathToNameKey = "NameKey";
const string Location::PathToWidth = "Width";
const string Location::PathToHeight = "Height";
const string Location::PathToOffset = "Offset";

const string Location::PathToTextures = "Textures";
const string Location::PathToPathToTexture = "PathToTexture";

const string Location::PathParametersFiles = "ParametersFiles";
const string Location::PathToPathParameterFiles = "PathToParametersFile";

const string Location::PathToLayers = "Layers";

const string Location::PathPurityLogicEntities = "PurityLogicEntities";
const string Location::PathToCachedEntities = "CachedEntities";
const string Location::PathToEntities = "Entities";

const string Location::PathToLayerName = "Name";
const string Location::PathToEntitiesLayerName = "LayerName";
const string Location::PathToEntityPathToEntity = "PathToEntity";
const string Location::PathToEntityPosition = "Position";
const string Location::PathToEntityCount = "Count";
const string Location::PathToEntityAngle = "Angle";
const string Location::PathToEntitySize = "Size";
const string Location::PathToEntityRenderingOrder = "RenderingOrder";
const string Location::PathToIsEntityFlippedHorizontal = "IsFlippedHorizontal";
const string Location::PathToIsEntityFlippedVertical = "IsFlippedVertical";

//===========================================================================//

Location::Location(const string & domainName, const string & pathToFile, bool isReserveEntities) :
	PtrFromThisMixIn<Location>(),
	_domainName(domainName),
	_pathToFile(pathToFile),
	_fullPathToFile(PathHelper::GetFullPathToLocationFile(pathToFile)),
	_nameKey(),
	_name(),
	_width(0.0f),
	_height(0.0f),
	_offset(),
	_navigationMesh(),
	_pathesToTextures(),
	_pathesToParametersFiles(),
	_purityLogicEntitiesInfos(),
	_cachedEntitiesInfos(),
	_layersNames()
{
	Console->Notice(string::Format("Loading location \"{0}\" ...", _fullPathToFile));
	Console->AddOffset(2);

	ParametersFileManager->Reload(_fullPathToFile);
	Load(isReserveEntities);

	Console->ReduceOffset(2);
	Console->Notice("Location loaded");
}

//===========================================================================//

Location::Location(const string & domainName, float width, float height, const Vector2 & offset,
                   const Array<string> & layersNames, const Array<string> & purityLogicEntitiesInfos) :
	PtrFromThisMixIn<Location>(),
	_domainName(domainName),
	_pathToFile(),
	_fullPathToFile(),
	_nameKey(),
	_name(),
	_width(width),
	_height(height),
	_offset(offset),
	_navigationMesh(),
	_pathesToTextures(),
	_pathesToParametersFiles(),
	_purityLogicEntitiesInfos(purityLogicEntitiesInfos),
	_cachedEntitiesInfos(),
	_layersNames()
{
	_nameKey = string::Format("{0}.{1}", PathToLocationNames, DefaultName);
	_name = DefaultName;

	Console->Notice("Creating new location \"{0}\"...", _name);
	Console->AddOffset(2);

	for (const auto & layerName : layersNames)
	{
		AddLayer(layerName);
	}

	_navigationMesh = new NavigationMesh();

	Console->ReduceOffset(2);
	Console->Notice("Location created");
}

//===========================================================================//

Location::~Location()
{
	Console->Notice(string::Format("Removing location \"{0}\" ...", _fullPathToFile));
	Console->AddOffset(2);

	Clear();

	Console->ReduceOffset(2);
	Console->Notice("Location removed");
}

//===========================================================================//

void Location::SaveToFile(const string & pathToFile)
{
	Console->Notice(string::Format("Saving location: \"{0}\"", _name));

	_pathToFile = pathToFile;
	_fullPathToFile = PathHelper::GetFullPathToLocationFile(_pathToFile);

	SharedPtr<ParametersFile> file = new ParametersFile(_fullPathToFile, FileAccess::Write);

	file->Set<string>(PathToNameKey, _nameKey);

	file->Set<float>(PathToWidth, _width);
	file->Set<float>(PathToHeight, _height);

	file->Set<Vector2>(PathToOffset, _offset);

	SaveData(file);
	SaveLayers(file);
	SavePurityLogicEntities(file);
	SaveCachedEntities(file);
	SaveEntities(file);
	_navigationMesh->SaveToFile(file);

	file->Save();
	file->Close();

	if (ParametersFileManager->Contains(_fullPathToFile))
	{
		ParametersFileManager->Reload(_fullPathToFile);
	}
	else
	{
		ParametersFileManager->Load(_fullPathToFile);
	}

	Console->Notice("Location saved");
}

//===========================================================================//

const string & Location::GetPathToFile() const
{
	return _pathToFile;
}

//===========================================================================//

const string & Location::GetName() const
{
	return _name;
}

//===========================================================================//

float Location::GetWidth() const
{
	return _width;
}

//===========================================================================//

float Location::GetHeight() const
{
	return _height;
}

//===========================================================================//

Vector2 Location::GetOffset() const
{
	return _offset;
}

//===========================================================================//

Vector2 Location::GetCenter() const
{
	return {_offset.X + _width / 2.0f, _offset.Y + _height / 2.0f};
}

//===========================================================================//

const SharedPtr<NavigationMesh> & Location::GetNavigationMesh() const
{
	return _navigationMesh;
}

//===========================================================================//

List<Vector3> Location::GetPath(const Vector3 & startPosition, const Vector3 & targetPosition) const
{
	try
	{
		return PathFinder::GetAStarPath(_navigationMesh, startPosition, targetPosition);
	}
	catch (Exception &)
	{
		return List<Vector3>();
	}
}

//===========================================================================//

void Location::Load(bool isReserveEntities)
{
	WeakPtr<ParametersFile> file = ParametersFileManager->Get(_fullPathToFile);

	_nameKey = file->Get<string>(PathToNameKey);
	_name = StringTable->GetString(_nameKey);
	Console->Notice(string::Format("Location name: \"{0}\"", _name));

	_width = file->Get<float>(PathToWidth);
	_height = file->Get<float>(PathToHeight);

	_offset = file->Get<Vector2>(PathToOffset);

	LoadData(file);
	LoadLayers(file);
	LoadPurityLogicEntities(file);
	LoadCachedEntities(file, isReserveEntities);
	LoadEntities(file);
	_navigationMesh = new NavigationMesh(file);

	Console->Notice("Location loaded");
}

//===========================================================================//

void Location::Clear()
{
	_navigationMesh = NullPtr;
	RemoveEntities();
	RemoveLayers();
	RemoveData();
}

//===========================================================================//

void Location::LoadData(const WeakPtr<ParametersFile> & file)
{
	if (file->ContainsSection(PathToTextures))
	{
		integer texturesCount = file->GetListChildren(PathToTextures);
		for (integer i = 0; i < texturesCount; ++i)
		{
			string parameterPath = string::Format(PathToTextures + ".{0}." + PathToPathToTexture, i);
			string pathToTexture = file->Get<string>(parameterPath);
			TextureManager->Load(pathToTexture);
			_pathesToTextures.Add(pathToTexture);
		}
	}

	if (file->ContainsSection(PathParametersFiles))
	{
		integer parametersFilesCount = file->GetListChildren(PathParametersFiles);
		for (integer i = 0; i < parametersFilesCount; ++i)
		{
			string parameterPath = string::Format(PathParametersFiles + ".{0}." + PathToPathParameterFiles, i);
			string pathToParametersFile = file->Get<string>(parameterPath);
			ParametersFileManager->Load(pathToParametersFile);
			_pathesToParametersFiles.Add(pathToParametersFile);
		}
	}
}

//===========================================================================//

void Location::SaveData(const SharedPtr<ParametersFile> & file) const
{
	for (integer i = 0; i < _pathesToTextures.GetCount(); ++i)
	{
		string parameterPath = string::Format(PathToTextures + ".{0}." + PathToPathToTexture, i);
		file->Set<string>(parameterPath, _pathesToTextures[i]);
	}

	for (integer i = 0; i < _pathesToParametersFiles.GetCount(); ++i)
	{
		string parameterPath = string::Format(PathParametersFiles + ".{0}." + PathToPathParameterFiles, i);
		file->Set<string>(parameterPath, _pathesToParametersFiles[i]);
	}
}

//===========================================================================//

void Location::RemoveData()
{
	for (const string & pathToTexture : _pathesToTextures)
	{
		TextureManager->Remove(pathToTexture);
	}
	_pathesToTextures.Clear();

	for (const string & pathToParametersFile : _pathesToParametersFiles)
	{
		ParametersFileManager->Remove(pathToParametersFile);
	}
	_pathesToParametersFiles.Clear();
}

//===========================================================================//

void Location::LoadLayers(const WeakPtr<ParametersFile> & file)
{
	integer layersCount = file->GetListChildren(PathToLayers);
	for (integer i = 0; i < layersCount; ++i)
	{
		AddLayer(file->Get<string>(string::Format(PathToLayers + ".{0}." + PathToLayerName, i)));
	}
}

//===========================================================================//

void Location::SaveLayers(const SharedPtr<ParametersFile> & file) const
{
	for (integer i = 0; i < _layersNames.GetCount(); ++i)
	{
		file->Set<string>(string::Format(PathToLayers + ".{0}." + PathToLayerName, i), _layersNames[i]);
	}
}

//===========================================================================//

void Location::RemoveLayers()
{
	for (const string & layerName : _layersNames)
	{
		Renderer->RemoveLayer(_domainName, layerName);
	}
}

//===========================================================================//

void Location::LoadPurityLogicEntities(const WeakPtr<ParametersFile> & file)
{
	if (!file->ContainsSection(PathPurityLogicEntities))
	{
		return;
	}

	integer entitiesCount = file->GetListChildren(PathPurityLogicEntities);
	for (integer i = 0; i < entitiesCount; ++i)
	{
		string path = string::Format("{0}.{1}.{2}", PathPurityLogicEntities, i, PathToEntityPathToEntity);
		string pathToEntity = file->Get<string>(path);

		SharedPtr<Entity> entity = EntityManager->CreateEntity(pathToEntity, _domainName, string::Empty, Vector3());

		_purityLogicEntitiesInfos.Add(pathToEntity);
	}
}

//===========================================================================//

void Location::SavePurityLogicEntities(const SharedPtr<ParametersFile> & file) const
{
	for (integer i = 0; i < _purityLogicEntitiesInfos.GetCount(); ++i)
	{
		string path = string::Format("{0}.{1}.{2}", PathPurityLogicEntities, i, PathToEntityPathToEntity);
		file->Set<string>(path, _purityLogicEntitiesInfos[i]);
	}
}

//===========================================================================//

void Location::LoadCachedEntities(const WeakPtr<ParametersFile> & file, bool isReserveEntities)
{
	if (!file->ContainsSection(PathToCachedEntities))
	{
		return;
	}

	integer cachedEntitiesCount = file->GetListChildren(PathToCachedEntities);
	for (integer i = 0; i < cachedEntitiesCount; ++i)
	{
		string path = string::Format("{0}.{1}", PathToCachedEntities, i);
		string pathToEntity = file->Get<string>(string::Format("{0}.{1}", path, PathToEntityPathToEntity));
		integer reserveCount = file->Get<integer>(string::Format("{0}.{1}", path, PathToEntityCount));

		if (isReserveEntities)
		{
			EntityManager->ReserveEntities(pathToEntity, reserveCount);
		}

		_cachedEntitiesInfos.Add(pathToEntity, reserveCount);
	}
}

//===========================================================================//

void Location::SaveCachedEntities(const WeakPtr<ParametersFile> & file) const
{
	integer infoIndex = 0;
	for (const auto & cachedEntitiesInfo : _cachedEntitiesInfos)
	{
		string path = string::Format("{0}.{1}", PathToCachedEntities, infoIndex);
		file->Set<string>(string::Format("{0}.{1}", path, PathToEntityPathToEntity), cachedEntitiesInfo.GetKey());
		file->Set<integer>(string::Format("{0}.{1}", path, PathToEntityCount), cachedEntitiesInfo.GetValue());
		infoIndex++;
	}
}

//===========================================================================//

void Location::LoadEntities(const WeakPtr<ParametersFile> & file)
{
	if (!file->ContainsSection(PathToEntities))
	{
		return;
	}

	const integer groupsCount = file->GetListChildren(PathToEntities);

	for (integer i = 0; i < groupsCount; ++i)
	{
		const string path = string::Format("{0}.{1}", PathToEntities, i);
		const string layerName = file->Get<string>(path + "." + PathToEntitiesLayerName);
		const integer entitiesCount = file->GetListChildren(path);
		for (integer j = 0; j < entitiesCount; ++j)
		{
			const string pathToEntity = file->Get<string>(string::Format(path + ".{0}." + PathToEntityPathToEntity, j));
			const Vector3 position = file->Get<Vector3>(string::Format(path + ".{0}." + PathToEntityPosition, j));
			const string pathToAngle = string::Format(path + ".{0}." + PathToEntityAngle, j);
			const float angle = file->ContainsParameter(pathToAngle) ? file->Get<float>(pathToAngle) : EntityHelper::DefaultAngle;
			const string pathToSize = string::Format(path + ".{0}." + PathToEntitySize, j);
			Size size = file->ContainsParameter(pathToSize) ? file->Get<Size>(pathToSize) : EntityHelper::DefaultSize;

			const auto & entity = EntityManager->CreateEntity(pathToEntity, _domainName, layerName, position, angle, size);

			const string pathToRenderingOrder = string::Format(path + ".{0}." + PathToEntityRenderingOrder, j);
			if (file->ContainsParameter(pathToRenderingOrder))
			{
				EntityHelper::SetEntityRenderingOrder(entity, file->Get<float>(pathToRenderingOrder));
			}

			const string pathToIsFlippedHorizontal = string::Format(path + ".{0}." + PathToIsEntityFlippedHorizontal, j);
			if (file->ContainsParameter(pathToIsFlippedHorizontal) && file->Get<bool>(pathToIsFlippedHorizontal))
			{
				EntityHelper::FlipEntityHorizontal(entity);
			}

			const string pathToIsFlippedVertical = string::Format(path + ".{0}." + PathToIsEntityFlippedVertical, j);
			if (file->ContainsParameter(pathToIsFlippedVertical) && file->Get<bool>(pathToIsFlippedVertical))
			{
				EntityHelper::FlipEntityVertical(entity);
			}
		}
	}
}

//===========================================================================//

void Location::SaveEntities(const SharedPtr<ParametersFile> & file) const
{
	for (integer i = 0; i < _layersNames.GetCount(); ++i)
	{
		const string path = string::Format("{0}.{1}", PathToEntities, i);
		file->Set(path + "." + PathToEntitiesLayerName, _layersNames[i]);

		integer entityIndex = 0;
		for (const string & groupName : EntityManager->GetSavingGroupsNames())
		{
			const auto & entities = EntityManager->GetGroup(groupName);
			for (integer j = 0; j < entities.GetCount(); ++j)
			{
				const auto & entity = entities[j];

				const auto & renderable = entity->GetComponent<Renderable>();
				if (renderable->GetLayer()->GetName() == _layersNames[i])
				{
					const string pathToEntity = EntityManager->GetPathToEntity(entity->GetType());
					file->Set(string::Format(path + ".{0}." + PathToEntityPathToEntity, entityIndex), pathToEntity);

					const auto & node = entity->GetComponent<PositioningNode>();
					file->Set(string::Format(path + ".{0}." + PathToEntityPosition, entityIndex), node->GetPosition());

					const auto & rotationNode = entity->GetComponent<RotationNode>();
					if (rotationNode != NullPtr && !Math::EqualsZero(rotationNode->GetAngleZ()))
					{
						file->Set(string::Format(path + ".{0}." + PathToEntityAngle, entityIndex), rotationNode->GetAngleZ());
					}

					const auto & baseSprite = entity->GetComponent<BaseSprite>();
					//Если размер спрайта был изменен - запишем его в данные карты
					if (baseSprite != NullPtr && baseSprite->GetSize() != baseSprite->GetSizeResetValue())
					{
						file->Set(string::Format(path + ".{0}." + PathToEntitySize, entityIndex), baseSprite->GetSize());
					}

					if (baseSprite != NullPtr && !Math::EqualsZero(baseSprite->GetRenderingOrder()))
					{
						file->Set(string::Format(path + ".{0}." + PathToEntityRenderingOrder, entityIndex), baseSprite->GetRenderingOrder());
					}

					if (EntityHelper::IsEntityFlippedHorizontal(entity))
					{
						file->Set(string::Format(path + ".{0}." + PathToIsEntityFlippedHorizontal, entityIndex), true);
					}

					if (EntityHelper::IsEntityFlippedVertical(entity))
					{
						file->Set(string::Format(path + ".{0}." + PathToIsEntityFlippedVertical, entityIndex), true);
					}

					entityIndex++;
				}
			}
		}
	}
}

//===========================================================================//

void Location::AddLayer(const string & layerName)
{
	if (_layersNames.Contains(layerName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Layer '{0}' already exists in location", layerName));
	}
	_layersNames.Add(layerName);
	Renderer->AddLayerToBack(_domainName, new RenderableLayer(layerName));
}

//===========================================================================//

const Array<string> & Location::GetLayersNames() const
{
	return _layersNames;
}

//===========================================================================//

void Location::RemoveEntities()
{
	for (const auto & groupName : EntityManager->GetGroupsNames())
	{
		EntityManager->ClearGroup(groupName);
	}
	_cachedEntitiesInfos.Clear();
}

//===========================================================================//

} // namespace Bru

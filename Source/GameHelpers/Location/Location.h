//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LOCATION_H
#define LOCATION_H

#include "../../Common/Common.h"

#include "PathFinding/NavigationMesh/NavigationMesh.h"
#include "PathFinding/NavigationMesh/NavigationMeshVisualizer.h"
#include "PathFinding/PathFinder/PathFinder.h"

namespace Bru
{

//===========================================================================//

class Location : public PtrFromThisMixIn<Location>
{
public:
	//по поводу isReserveEntities - в редакторе карт не нужен кеш - только лишнее время на загрузку
	Location(const string & domainName, const string & pathToFile, bool isReserveEntities = true);
	Location(const string & domainName, float width, float height, const Vector2 & offset,
	         const Array<string> & layersNames, const Array<string> & purityLogicEntitiesInfos);
	virtual ~Location();

	virtual void SaveToFile(const string & pathToFile);

	const string & GetPathToFile() const;

	const string & GetName() const;

	float GetWidth() const;
	float GetHeight() const;
	Vector2 GetOffset() const;

	Vector2 GetCenter() const;

	const SharedPtr<NavigationMesh> & GetNavigationMesh() const;

	List<Vector3> GetPath(const Vector3 & startPosition, const Vector3 & targetPosition) const;

	void AddLayer(const string & layerName);
	const Array<string> & GetLayersNames() const;

private:
	static const string PathToLocationNames;
	static const string DefaultName;

	static const string PathToNameKey;
	static const string PathToWidth;
	static const string PathToHeight;
	static const string PathToOffset;

	static const string PathToTextures;
	static const string PathToPathToTexture;

	static const string PathParametersFiles;
	static const string PathToPathParameterFiles;

	static const string PathToLayers;

	static const string PathPurityLogicEntities;
	static const string PathToCachedEntities;
	static const string PathToEntities;

	static const string PathToLayerName;
	static const string PathToEntitiesLayerName;
	static const string PathToEntityPathToEntity;
	static const string PathToEntityCount;
	static const string PathToEntityPosition;
	static const string PathToEntityAngle;
	static const string PathToEntitySize;
	static const string PathToEntityRenderingOrder;
	static const string PathToIsEntityFlippedHorizontal;
	static const string PathToIsEntityFlippedVertical;

	void Load(bool isReserveEntities);
	void Clear();

	void LoadData(const WeakPtr<ParametersFile> & file);
	void SaveData(const SharedPtr<ParametersFile> & file) const;
	void RemoveData();

	void LoadLayers(const WeakPtr<ParametersFile> & file);
	void SaveLayers(const SharedPtr<ParametersFile> & file) const;
	void RemoveLayers();

	void LoadPurityLogicEntities(const WeakPtr<ParametersFile> & file);
	void SavePurityLogicEntities(const SharedPtr<ParametersFile> & file) const;

	void LoadCachedEntities(const WeakPtr<ParametersFile> & file, bool isReserveEntities);
	void SaveCachedEntities(const WeakPtr<ParametersFile> & file) const;

	void LoadEntities(const WeakPtr<ParametersFile> & file);
	void SaveEntities(const SharedPtr<ParametersFile> & file) const;

	void RemoveEntities();

	string _domainName;
	string _pathToFile;
	string _fullPathToFile;

	string _nameKey;
	string _name;

	float _width;
	float _height;
	Vector2 _offset;

	SharedPtr<NavigationMesh> _navigationMesh;

	List<string> _pathesToTextures;
	List<string> _pathesToParametersFiles;

	Array<string> _purityLogicEntitiesInfos;
	TreeMap<string, integer> _cachedEntitiesInfos;

	Array<string> _layersNames;

	Location(const Location &) = delete;
	Location & operator =(const Location &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // LOCATION_H

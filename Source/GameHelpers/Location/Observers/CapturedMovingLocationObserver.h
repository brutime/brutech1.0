//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef CAPTURED_LOCATION_OBSERVER_H
#define CAPTURED_LOCATION_OBSERVER_H

#include "BaseInputLocationObserver.h"

namespace Bru
{

//===========================================================================//

class CapturedMovingLocationObserver : public BaseInputLocationObserver
{
public:
	DECLARE_COMPONENT

	CapturedMovingLocationObserver(const string & domainName, const SharedPtr<Location> & location, const SharedPtr<BaseDataProvider> & provider);
	virtual ~CapturedMovingLocationObserver();

	virtual void Update(float deltaTime);

	void FixateCameraPosition();
	void UpdateCameraPosition(const Vector2 & capturedCoords, const Vector2 & currentCoords);

private:
	Vector3 MouseCoordsToWorldCoords(const Vector2 & mouseCoords) const;
	
	Vector3 _fixatedCameraPosition;
	
	CapturedMovingLocationObserver(const CapturedMovingLocationObserver &) = delete;
	CapturedMovingLocationObserver & operator = (const CapturedMovingLocationObserver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // CAPTURED_LOCATION_OBSERVER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BASE_LOCATION_OBSERVER_H
#define BASE_LOCATION_OBSERVER_H

#include "../../../Helpers/Engine/Engine.h"
#include "../Location.h"

namespace Bru
{

//===========================================================================//

class BaseLocationObserver : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	BaseLocationObserver(const string & domainName, const WeakPtr<Location> & location, const SharedPtr<BaseDataProvider> & provider);
	virtual ~BaseLocationObserver();

	const SharedPtr<Camera> & GetCamera() const;
	
	const string & GetLayerName() const;

protected:
	WeakPtr<Location> _location;
	SharedPtr<Camera> _camera;

private:
	static const string PathToLayerName;
	
	string _layerName;

	BaseLocationObserver(const BaseLocationObserver &);
	BaseLocationObserver & operator =(const BaseLocationObserver &);
};

//===========================================================================//

} // namespace Bru

#endif // BASE_OCATION_OBSERVER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BASE_HUMAN_LOCATION_OBSERVER_H
#define BASE_HUMAN_LOCATION_OBSERVER_H

#include "BaseLocationObserver.h"

namespace Bru
{

//===========================================================================//

class BaseInputLocationObserver : public BaseLocationObserver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	BaseInputLocationObserver(const string & domainName, const WeakPtr<Location> & location, const SharedPtr<BaseDataProvider> & provider);
	virtual ~BaseInputLocationObserver();

	void Zoom(float factor);

	void SetZoomTarget(float zoomTarget);
	float GetZoomTarget() const;

protected:
	void UpdateCameraZoom(float deltaTime);
	void RestrictCameraPosition();

	void MakeZoomTargetValid();

private:
	static const string PathToDeltaZoom;
	static const string PathToZoomingSpeed;
	static const string PathToMinZoom;
	static const string PathToMaxZoom;
	static const string PathToRestrictOffset;

	float _zoomingSpeed;
	float _deltaZoom;
	float _minZoom;
	float _maxZoom;
	Vector2 _restrictOffset;
	float _zoomTarget;

	BaseInputLocationObserver(const BaseInputLocationObserver &);
	BaseInputLocationObserver & operator =(const BaseInputLocationObserver &);
};

//===========================================================================//

} // namespace Bru

#endif // BASE_HUMAN_LOCATION_OBSERVER_H

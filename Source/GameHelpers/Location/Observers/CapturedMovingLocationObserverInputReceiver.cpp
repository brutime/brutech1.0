//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "CapturedMovingLocationObserverInputReceiver.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(CapturedMovingLocationObserverInputReceiver, InputReceiver)

//===========================================================================//

CapturedMovingLocationObserverInputReceiver::CapturedMovingLocationObserverInputReceiver(const string & domainName,
                                                                                         const SharedPtr<CapturedMovingLocationObserver> & locationObserver) :
	InputReceiver(domainName),
	_locationObserver(locationObserver),
	_isCaptured(false),
	_capturedCoords()
{
}

//===========================================================================//

CapturedMovingLocationObserverInputReceiver::~CapturedMovingLocationObserverInputReceiver()
{
}

//===========================================================================//

void CapturedMovingLocationObserverInputReceiver::Disable()
{
	InputReceiver::Disable();
}

//===========================================================================//

void CapturedMovingLocationObserverInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs &)
{
}

//===========================================================================//

void CapturedMovingLocationObserverInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	if (args.Key == MouseKey::Left)
	{
		if (args.State == KeyState::Down)
		{
			_isCaptured = true;
			_capturedCoords = {args.X, args.Y};
			_locationObserver->FixateCameraPosition();
		}
		else if (args.State == KeyState::Up)
		{
			_isCaptured = false;
			_capturedCoords.Clear();
		}
	}
	else if (args.Key == MouseKey::Move)
	{
		if (_isCaptured)
		{
			_locationObserver->UpdateCameraPosition(_capturedCoords, {args.X, args.Y});
		}
	}
	else if (args.Key == MouseKey::Scroll)
	{
		integer scrollClickCount = args.State == KeyState::Up ? args.ScrollClickCount : -args.ScrollClickCount;
		_locationObserver->Zoom(scrollClickCount);
	}
}

//===========================================================================//

void CapturedMovingLocationObserverInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

} // namespace Bru

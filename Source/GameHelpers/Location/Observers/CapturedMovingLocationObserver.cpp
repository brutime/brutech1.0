//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "CapturedMovingLocationObserver.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_COMPONENT(CapturedMovingLocationObserver, BaseLocationObserver)

//===========================================================================//

CapturedMovingLocationObserver::CapturedMovingLocationObserver(const string & domainName, const SharedPtr<Location> & location,
                                                               const SharedPtr<BaseDataProvider> & provider) :
	BaseInputLocationObserver(domainName, location, provider),
	_fixatedCameraPosition()
{
	_camera = new Camera();
	_camera->Set2DPosition(location->GetCenter().X, location->GetCenter().Y, Renderer->GetSurfaceCoordsHeightCenter());
}

//===========================================================================//

CapturedMovingLocationObserver::~CapturedMovingLocationObserver()
{
}

//===========================================================================//

void CapturedMovingLocationObserver::Update(float deltaTime)
{
	UpdateCameraZoom(deltaTime);
	RestrictCameraPosition();
}

//===========================================================================//

void CapturedMovingLocationObserver::FixateCameraPosition()
{
	_fixatedCameraPosition = _camera->GetPosition();
}

//===========================================================================//

void CapturedMovingLocationObserver::UpdateCameraPosition(const Vector2 & capturedCoords, const Vector2 & currentCoords)
{
	Vector3 deltaWorldCoords = MouseCoordsToWorldCoords(capturedCoords - currentCoords);
	_camera->Set2DPosition(_fixatedCameraPosition.X, _fixatedCameraPosition.Y, _camera->GetPosition().Z);
	_camera->Translate(deltaWorldCoords.X, deltaWorldCoords.Y, 0.0f);
}

//===========================================================================//

Vector3 CapturedMovingLocationObserver::MouseCoordsToWorldCoords(const Vector2 & mouseCoords) const
{
	ProjectionMode projectionMode = Renderer->GetLayer(GetDomainName(), GetLayerName())->GetProjectionMode();
	Vector2 translatedPosition(Renderer->PixelsToCoords(projectionMode, mouseCoords));
	return CoordsHelper::GetProjection(translatedPosition, _camera->GetPosition().Z, 0.0f);
}

//===========================================================================//

} // namespace Bru

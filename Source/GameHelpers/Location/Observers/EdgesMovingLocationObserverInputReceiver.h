//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef EDGES_MOVING_LOCATION_OBSERVER_INPUT_RECEIVER_H
#define EDGES_MOVING_LOCATION_OBSERVER_INPUT_RECEIVER_H

#include "../../../Helpers/Engine/Engine.h"
#include "EdgesMovingLocationObserver.h"

namespace Bru
{

//===========================================================================//

class EdgesMovingLocationObserverInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	EdgesMovingLocationObserverInputReceiver(const string & domainName, const SharedPtr<EdgesMovingLocationObserver> & locationObserver);
	virtual ~EdgesMovingLocationObserverInputReceiver();

	virtual void Disable();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);

private:
	void UpdateMouseSurfaceCoords(integer mouseX, integer mouseY);

	SharedPtr<EdgesMovingLocationObserver> _locationObserver;

	EdgesMovingLocationObserverInputReceiver(const EdgesMovingLocationObserverInputReceiver &) = delete;
	EdgesMovingLocationObserverInputReceiver & operator = (const EdgesMovingLocationObserverInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // EDGES_MOVING_LOCATION_OBSERVER_INPUT_RECEIVER_H

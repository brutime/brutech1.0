//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EdgesMovingLocationObserver.h"

namespace Bru
{

//===========================================================================//
 
IMPLEMENT_COMPONENT(EdgesMovingLocationObserver, BaseLocationObserver)

//===========================================================================//

const string EdgesMovingLocationObserver::PathToScrollingField = "ScrollingField";
const string EdgesMovingLocationObserver::PathToSpeed = "Speed";

//===========================================================================//

EdgesMovingLocationObserver::EdgesMovingLocationObserver(const string & domainName, const SharedPtr<Location> & location,
                                                         const SharedPtr<BaseDataProvider> & provider) :
	BaseInputLocationObserver(domainName, location, provider),	
	_scrollingField(provider->Get<float>(PathToScrollingField)),
	_speed(provider->Get<float>(PathToSpeed)),
	_mouseSurfaceCoords()
{
	_camera = new Camera();
	_camera->Set2DPosition(location->GetCenter().X, location->GetCenter().Y, Renderer->GetSurfaceCoordsHeightCenter());
}

//===========================================================================//

EdgesMovingLocationObserver::~EdgesMovingLocationObserver()
{
}

//===========================================================================//

void EdgesMovingLocationObserver::Update(float deltaTime)
{
	UpdateCameraPosition(deltaTime);
	UpdateCameraZoom(deltaTime);
	RestrictCameraPosition();
}

//===========================================================================//

void EdgesMovingLocationObserver::SetMouseSurfaceCoords(const Vector2 & mouseSurfaceCoords)
{
	_mouseSurfaceCoords = mouseSurfaceCoords;
}

//===========================================================================//

void EdgesMovingLocationObserver::UpdateCameraPosition(float deltaTime)
{
	if (_mouseSurfaceCoords.X < _scrollingField)
	{
		_camera->Translate(-_speed * deltaTime, 0.0f, 0.0f);
	}

	if (_mouseSurfaceCoords.X > Renderer->GetSurfaceCoordsWidth() - _scrollingField)
	{
		_camera->Translate(_speed * deltaTime, 0.0f, 0.0f);
	}

	if (_mouseSurfaceCoords.Y < _scrollingField)
	{
		_camera->Translate(0.0f, -_speed * deltaTime, 0.0f);
	}

	if (_mouseSurfaceCoords.Y > Renderer->GetSurfaceCoordsHeight() - _scrollingField)
	{
		_camera->Translate(0.0f, _speed * deltaTime, 0.0f);
	}
}

//===========================================================================//

} // namespace Bru

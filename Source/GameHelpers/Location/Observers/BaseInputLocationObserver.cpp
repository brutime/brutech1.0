//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseInputLocationObserver.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(BaseInputLocationObserver, BaseLocationObserver)

//===========================================================================//

const string BaseInputLocationObserver::PathToZoomingSpeed = "ZoomingSpeed";
const string BaseInputLocationObserver::PathToDeltaZoom = "DeltaZoom";
const string BaseInputLocationObserver::PathToMinZoom = "MinZoom";
const string BaseInputLocationObserver::PathToMaxZoom = "MaxZoom";
const string BaseInputLocationObserver::PathToRestrictOffset = "RestrictOffset";

//===========================================================================//

BaseInputLocationObserver::BaseInputLocationObserver(const string & domainName, const WeakPtr<Location> & location, const SharedPtr<BaseDataProvider> & provider) :
	BaseLocationObserver(domainName, location, provider),
	_zoomingSpeed(provider->Get<float>(PathToZoomingSpeed)),
	_deltaZoom(provider->Get<float>(PathToDeltaZoom)),
	_minZoom(provider->Get<float>(PathToMinZoom)),
	_maxZoom(provider->Get<float>(PathToMaxZoom)),
	_restrictOffset(provider->Get<Vector2>(PathToRestrictOffset)),
	_zoomTarget(Renderer->GetSurfaceCoordsHeightCenter())	
{
}

//===========================================================================//

BaseInputLocationObserver::~BaseInputLocationObserver()
{
}

//===========================================================================//

void BaseInputLocationObserver::Zoom(float factor)
{
	_zoomTarget += -_deltaZoom * factor;
	MakeZoomTargetValid();
}

//===========================================================================//

void BaseInputLocationObserver::SetZoomTarget(float zoomTarget)
{
	_zoomTarget = zoomTarget;
	MakeZoomTargetValid();
}

//===========================================================================//

float BaseInputLocationObserver::GetZoomTarget() const
{
	return _zoomTarget;
}

//===========================================================================//

void BaseInputLocationObserver::UpdateCameraZoom(float deltaTime)
{
	if (_camera->GetPosition().Z < _zoomTarget)
	{
		_camera->Translate(0.0f, 0.0f, _zoomingSpeed * deltaTime);
		if (_camera->GetPosition().Z > _zoomTarget)
		{
			Vector3 _cameraPosition = _camera->GetPosition();
			_cameraPosition.Z = _zoomTarget;
			_camera->SetPosition(_cameraPosition);
		}
	}

	if (_camera->GetPosition().Z > _zoomTarget)
	{
		_camera->Translate(0.0f, 0.0f, -_zoomingSpeed * deltaTime);
		if (_camera->GetPosition().Z < _zoomTarget)
		{
			Vector3 _cameraPosition = _camera->GetPosition();
			_cameraPosition.Z = _zoomTarget;
			_camera->SetPosition(_cameraPosition);
		}
	}
}

//===========================================================================//

void BaseInputLocationObserver::RestrictCameraPosition()
{
	Vector2 cameraOffset(Renderer->GetSurfaceCoordsWidthCenter(),
	                     Renderer->GetSurfaceCoordsHeightCenter());
	Vector3 projectedCameraOffset = CoordsHelper::GetProjection(cameraOffset, _camera->GetPosition().Z, _camera->GetPosition().Z);

	Vector3 leftBottomCorner = {_location->GetOffset().X + projectedCameraOffset.X - _restrictOffset.X,
		                        _location->GetOffset().Y + projectedCameraOffset.Y - _restrictOffset.Y};

	float locationWidth = _location->GetWidth();
	float locationHeight = _location->GetHeight();

	Vector3 rightTopCorner = {_location->GetOffset().X + locationWidth - projectedCameraOffset.X + _restrictOffset.X,
		                      _location->GetOffset().Y + locationHeight - projectedCameraOffset.Y + _restrictOffset.Y};

	Vector2 surfaceCoords(Renderer->GetSurfaceCoordsWidth(), Renderer->GetSurfaceCoordsHeight());
	Vector3 projectedSurfaceCoords = CoordsHelper::GetProjection(surfaceCoords, _camera->GetPosition().Z, _camera->GetPosition().Z);
	if (projectedSurfaceCoords.X < locationWidth + _restrictOffset.X * 2.0f)
	{
		if (_camera->GetPosition().X < leftBottomCorner.X)
		{
			_camera->Set2DPosition(leftBottomCorner.X, _camera->GetPosition().Y, _camera->GetPosition().Z);
		}
		else if (_camera->GetPosition().X > rightTopCorner.X)
		{
			_camera->Set2DPosition(rightTopCorner.X, _camera->GetPosition().Y, _camera->GetPosition().Z);
		}
	}
	else
	{
		_camera->Set2DPosition(_location->GetCenter().X, _camera->GetPosition().Y, _camera->GetPosition().Z);
	}

	if (projectedSurfaceCoords.Y < locationHeight + _restrictOffset.Y * 2.0f)
	{
		if (_camera->GetPosition().Y < leftBottomCorner.Y)
		{
			_camera->Set2DPosition(_camera->GetPosition().X, leftBottomCorner.Y, _camera->GetPosition().Z);
		}
		else if (_camera->GetPosition().Y > rightTopCorner.Y)
		{
			_camera->Set2DPosition(_camera->GetPosition().X, rightTopCorner.Y, _camera->GetPosition().Z);
		}
	}
	else
	{
		_camera->Set2DPosition(_camera->GetPosition().X, _location->GetCenter().Y, _camera->GetPosition().Z);
	}
}

//===========================================================================//

void BaseInputLocationObserver::MakeZoomTargetValid()
{
	if (_zoomTarget > _maxZoom)
	{
		_zoomTarget = _maxZoom;
	}
	else if (_zoomTarget < _minZoom)
	{
		_zoomTarget = _minZoom;
	}
}

//===========================================================================//

} // namespace Bru

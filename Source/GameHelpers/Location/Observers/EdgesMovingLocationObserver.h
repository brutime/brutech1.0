//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef EDGES_MOVING_LOCATION_OBSERVER_H
#define EDGES_MOVING_LOCATION_OBSERVER_H

#include "BaseInputLocationObserver.h"

namespace Bru
{

//===========================================================================//

class EdgesMovingLocationObserver : public BaseInputLocationObserver
{
public:
	DECLARE_COMPONENT

	EdgesMovingLocationObserver(const string & domainName, const SharedPtr<Location> & location, const SharedPtr<BaseDataProvider> & provider);
	virtual ~EdgesMovingLocationObserver();

	virtual void Update(float deltaTime);

	void SetMouseSurfaceCoords(const Vector2 & mouseSurfaceCoords);

private:
	static const string PathToScrollingField;
	static const string PathToSpeed;

	void UpdateCameraPosition(float deltaTime);	

	float _scrollingField;
	float _speed;
	
	Vector2 _mouseSurfaceCoords;

	EdgesMovingLocationObserver(const EdgesMovingLocationObserver &);
	EdgesMovingLocationObserver & operator = (const EdgesMovingLocationObserver &);
};

//===========================================================================//

} // namespace Bru

#endif // EDGES_MOVING_LOCATION_OBSERVER_H

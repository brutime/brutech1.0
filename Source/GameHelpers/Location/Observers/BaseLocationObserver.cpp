//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BaseLocationObserver.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(BaseLocationObserver, Updatable)

//===========================================================================//

const string BaseLocationObserver::PathToLayerName = "LayerName";

//===========================================================================//

BaseLocationObserver::BaseLocationObserver(const string & domainName, const WeakPtr<Location> & location, const SharedPtr<BaseDataProvider> & provider) :
	Updatable(domainName, provider),	
	_location(location),
	_camera(),
	_layerName(provider->Get<string>(PathToLayerName))
{
}

//===========================================================================//

BaseLocationObserver::~BaseLocationObserver()
{
}

//===========================================================================//

const SharedPtr<Camera> & BaseLocationObserver::GetCamera() const
{
	return _camera;
}

//===========================================================================//

const string & BaseLocationObserver::GetLayerName() const
{
	return _layerName;
}

//===========================================================================//

} // namespace Bru

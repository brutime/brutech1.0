//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef CAPTURED_LOCATION_OBSERVER_INPUT_RECEIVER_H
#define CAPTURED_LOCATION_OBSERVER_INPUT_RECEIVER_H

#include "../../../Helpers/Engine/Engine.h"
#include "CapturedMovingLocationObserver.h"

namespace Bru
{

//===========================================================================//

class CapturedMovingLocationObserverInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	CapturedMovingLocationObserverInputReceiver(const string & domainName, const SharedPtr<CapturedMovingLocationObserver> & locationObserver);
	virtual ~CapturedMovingLocationObserverInputReceiver();

	virtual void Disable();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);

private:

	SharedPtr<CapturedMovingLocationObserver> _locationObserver;

	bool _isCaptured;
	Vector2 _capturedCoords;

	CapturedMovingLocationObserverInputReceiver(const CapturedMovingLocationObserverInputReceiver &) = delete;
	CapturedMovingLocationObserverInputReceiver & operator = (const CapturedMovingLocationObserverInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // CAPTURED_LOCATION_OBSERVER_INPUT_RECEIVER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EdgesMovingLocationObserverInputReceiver.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(EdgesMovingLocationObserverInputReceiver, InputReceiver)

//===========================================================================//

EdgesMovingLocationObserverInputReceiver::EdgesMovingLocationObserverInputReceiver(const string & domainName,
                                                                                   const SharedPtr<EdgesMovingLocationObserver> & locationObserver) :
	InputReceiver(domainName),
	_locationObserver(locationObserver)
{
	UpdateMouseSurfaceCoords(Mouse->GetX(), Mouse->GetY());
}

//===========================================================================//

EdgesMovingLocationObserverInputReceiver::~EdgesMovingLocationObserverInputReceiver()
{
}

//===========================================================================//

void EdgesMovingLocationObserverInputReceiver::Disable()
{
	InputReceiver::Disable();
}

//===========================================================================//

void EdgesMovingLocationObserverInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs &)
{
}

//===========================================================================//

void EdgesMovingLocationObserverInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	if (args.Key == MouseKey::Move)
	{
		UpdateMouseSurfaceCoords(args.X, args.Y);
	}
	else if (args.Key == MouseKey::Scroll)
	{
		integer scrollClickCount = args.State == KeyState::Up ? args.ScrollClickCount : -args.ScrollClickCount;
		_locationObserver->Zoom(scrollClickCount);
	}
}

//===========================================================================//

void EdgesMovingLocationObserverInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

void EdgesMovingLocationObserverInputReceiver::UpdateMouseSurfaceCoords(integer mouseX, integer mouseY)
{
	ProjectionMode projectionMode = Renderer->GetLayer(GetDomainName(), _locationObserver->GetLayerName())->GetProjectionMode();
	Vector2 mouseSurfaceCoords = Renderer->PixelsToCoords(projectionMode, Vector2(mouseX, mouseY));

	_locationObserver->SetMouseSurfaceCoords(mouseSurfaceCoords);
}

//===========================================================================//

} // namespace Bru

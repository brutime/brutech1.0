//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PathesVisualizer.h"

#include "../../Helpers/EntitiesHelpers/EntityManager.h"
#include "PathKeeperLogic.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(PathesVisualizer, Updatable)

//===========================================================================//

const Color PathesVisualizer::LineColor = Colors::Red;
const float PathesVisualizer::LineThickness = 1.0f;

//===========================================================================//

PathesVisualizer::PathesVisualizer(const string & domainName, const string & layerName, const List<string> & groupsNames) :
	Updatable(domainName),
	_groupsNames(groupsNames),
	_primitivesContainer(new PrimitivesContainer(domainName, layerName))
{
}

//===========================================================================//

PathesVisualizer::~PathesVisualizer()
{
}

//===========================================================================//

void PathesVisualizer::Update(float)
{
	_primitivesContainer->Clear();
	for (const string & groupName : _groupsNames)
	{
		const List<SharedPtr<Entity>> & entities = EntityManager->GetGroup(groupName);
		for (const SharedPtr<Entity> & entity : entities)
		{
			SharedPtr<PositioningNode> node = entity->GetComponent<PositioningNode>();
			SharedPtr<PathKeeperLogic> pathFollowerLogic = entity->GetComponent<PathKeeperLogic>();
			if (!pathFollowerLogic->GetPath().IsEmpty())
			{
				Array<Vector3> points(pathFollowerLogic->GetPath().ToArray());
				points.Insert(0, node->GetPosition());
				_primitivesContainer->AddLine(points, LineColor, LineThickness);
			}
		}
	}
}

//===========================================================================//

} // namespace Bru

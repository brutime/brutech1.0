//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PathKeeperLogic.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(PathKeeperLogic, EntityComponent)

//===========================================================================//

PathKeeperLogic::PathKeeperLogic(const string & domainName, const SharedPtr<BaseDataProvider> &) :
	EntityComponent(domainName),
	_path()
{
}

//===========================================================================//

PathKeeperLogic::~PathKeeperLogic()
{
}

//===========================================================================//

void PathKeeperLogic::Reset()
{
	EntityComponent::Reset();
	_path.Clear();	
}

//===========================================================================//

void PathKeeperLogic::RemoveFirstPathPoint()
{
	_path.RemoveAt(0);
}

//===========================================================================//

void PathKeeperLogic::SetPath(const List<Vector3> & path)
{
	_path = path;
}

//===========================================================================//

const List<Vector3> & PathKeeperLogic::GetPath() const
{
	return _path;
}

//===========================================================================//

} // namespace Bru

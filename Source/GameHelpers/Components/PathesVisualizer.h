//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef PATHES_VISUALIZER_H
#define PATHES_VISUALIZER_H

#include "../../Common/Common.h"
#include "../../Updater/Updater.h"
#include "../../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class PathesVisualizer : public Updatable
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	PathesVisualizer(const string & domainName, const string & layerName, const List<string> & groupsNames);
	virtual ~PathesVisualizer();

	virtual void Update(float deltaTime);

private:
	static const Color LineColor;
	static const float LineThickness;

	List<string> _groupsNames;
	SharedPtr<PrimitivesContainer> _primitivesContainer;

	PathesVisualizer(const PathesVisualizer &) = delete;
	PathesVisualizer & operator =(const PathesVisualizer &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PATHES_VISUALIZER_H

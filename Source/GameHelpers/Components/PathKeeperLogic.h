//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef PATH_KEEPER_LOGIC_H
#define PATH_KEEPER_LOGIC_H

#include "../../Common/Common.h"
#include "../../Math/Math.h"
#include "../../EntitySystem/EntitySystem.h"

namespace Bru
{

//===========================================================================//

class PathKeeperLogic : public EntityComponent
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	//потом сюда добавим радиус
	PathKeeperLogic(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~PathKeeperLogic();

	void RemoveFirstPathPoint();

	void SetPath(const List<Vector3> & path);
	const List<Vector3> & GetPath() const;

	virtual void Reset();

private:
	List<Vector3> _path;

	PathKeeperLogic(const PathKeeperLogic &) = delete;
	PathKeeperLogic & operator =(const PathKeeperLogic &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PATH_FOLLOWER_LOGIC_H

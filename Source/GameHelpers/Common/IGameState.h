//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Состояние игры
//

#ifndef I_GAME_STATE_H
#define I_GAME_STATE_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class IGameState
{
public:
    virtual ~IGameState();

    virtual void Select();
    virtual void Leave();

protected:
    IGameState();
};

//===========================================================================//

} // namespace Bru

#endif // I_GAME_STATE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef COMPONENTS_DATA_PROVIDER_H
#define COMPONENTS_DATA_PROVIDER_H

#include "../Common/Common.h"
#include "../Utils/ParametersFile/ParametersFile.h"
#include "DirectComponentDataProvider.h"

namespace Bru
{

//===========================================================================//

class ComponentDataProvider : public BaseDataProvider
{
public:
	ComponentDataProvider(const WeakPtr<ParametersFile> & file, integer componentIndex, const string & componentTypeName);
	virtual ~ComponentDataProvider();

	string GetParameter(const string & key) const;
	void SetParameter(const string & key, const string & value);

	virtual List<const WeakPtr<BaseDataProvider>> GetAnonymousChildren(const string &root) const;
	virtual integer GetAnonymousChildrenCount() const;

private:
	WeakPtr<ParametersSection> _section;
	integer _componentIndexInFile;
	SharedPtr<DirectComponentDataProvider> _directProvider;

	ComponentDataProvider(const ComponentDataProvider &) = delete;
	ComponentDataProvider & operator =(const ComponentDataProvider &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // COMPONENTS_DATA_PROVIDER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: сущность, которая содержит в себе набор компонентов
//

#pragma once
#ifndef ENTITY_H
#define ENTITY_H

#include "../Common/Common.h"

namespace Bru
{

//===========================================================================//

class EntityComponent;

//===========================================================================//

class Entity : public PtrFromThisMixIn<Entity>
{
public:
	Entity(const string & type, const string & groupName);
	virtual ~Entity();

	void AddComponent(const SharedPtr<EntityComponent> & component);
	bool ContainsComponent(integer componentTypeID);	
	template <typename T>
	SharedPtr<T> GetComponent() const;
	template <typename T>
	SharedPtr<T> GetComponent(integer componentTypeID) const;
	const TreeMap<integer, SharedPtr<EntityComponent>> & GetComponents() const;
	void RegisterAllComponents(const string & domainName);
	void RegisterAndResetAllComponents(const string & domainName);
	void UnregisterAllComponents();	
	void RemoveComponent(integer componentTypeID);	
	void RemoveAllComponents();

	const WeakPtr<Entity> & GetParent() const;

	void AddChild(const SharedPtr<Entity> & child);
	bool ContainsChild(const SharedPtr<Entity> & child);
	void RemoveChild(const SharedPtr<Entity> & child);
	void RemoveAllChildren();
	const List<SharedPtr<Entity> > & GetChildren() const;

	void ChangeDomain(const string & domainName);

	void PutToPool();
	void ReturnFromPool(const string & domainName);
	bool IsInPool() const;

	const string & GetType() const;
	int64 GetID() const;
	const string GetName() const;
	const string & GetGroupName() const;

	void PrintInfo() const;

private:
	string _type;
	int64 _id;
	string _name;
	string _groupName;

	TreeMap<integer, SharedPtr<EntityComponent>> _components;

	WeakPtr<Entity> _parent;
	List<SharedPtr<Entity>> _children;

	bool _isInPool;

	Entity(const Entity &) = delete;
	Entity & operator =(const Entity &) = delete;
};

//===========================================================================//

} // namespace Bru

#include "EntityComponent.h"

namespace Bru
{

//===========================================================================//

template <typename T>
SharedPtr<T> Entity::GetComponent() const
{
	for (const auto & componentsPair : _components)
	{
		const auto & component = componentsPair.GetValue();
		if (component->IsDerivedFromType(T::TypeID))
		{
			return static_pointer_cast<T>(component);
		}
	}
	return GetComponent<T>(T::TypeID);
}

//===========================================================================//

template <typename T>
SharedPtr<T> Entity::GetComponent(integer componentTypeID) const
{
	//Нам может придти ID базового компонента
	if (_components.Contains(componentTypeID))
	{
#ifdef DEBUGGING
		if (!_components[componentTypeID]->IsDerivedFromType(T::TypeID))
		{
			INVALID_ARGUMENT_EXCEPTION(string::Format("Component \"{0}\" not derived from component with type : \"{1}\"", _components[componentTypeID]->TypeName, T::TypeName));
		}
#endif // DEBUGGING		

		return static_pointer_cast<T>(_components[componentTypeID]);
	}
	else
	{
		//Или ID производного компонента
		for (const auto & componentsPair : _components)
		{
			if (componentsPair.GetValue()->IsDerivedFromType(componentTypeID))
			{
				return static_pointer_cast<T>(componentsPair.GetValue());
			}
		}
	}

	return NullPtr;
}

//===========================================================================//

} // namespace Bru

#endif // ENTITY_H

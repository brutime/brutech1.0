//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef DIRECT_COMPONENTS_DATA_PROVIDER_H
#define DIRECT_COMPONENTS_DATA_PROVIDER_H

#include "../Common/Common.h"
#include "../Utils/Base/BaseDataProvider.h"
#include "../Utils/ParametersFile/ParametersFile.h"

namespace Bru
{

//===========================================================================//

class DirectComponentDataProvider : public BaseDataProvider
{
public:
	DirectComponentDataProvider(const string & componentTypeName);
	virtual ~DirectComponentDataProvider();

	string GetParameter(const string & key) const;
	void SetParameter(const string & key, const string & value);

	virtual List<const WeakPtr<BaseDataProvider>> GetAnonymousChildren(const string &root) const;
	virtual integer GetAnonymousChildrenCount() const;
	
private:
	WeakPtr<ParametersFile> _file;
	string _componentTypeName;

	DirectComponentDataProvider(const DirectComponentDataProvider &) = delete;
	DirectComponentDataProvider & operator =(const DirectComponentDataProvider &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // DIRECT_COMPONENTS_DATA_PROVIDER_H

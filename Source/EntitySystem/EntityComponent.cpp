//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityComponent.h"

#include "EntitySystem.h"

namespace Bru
{

//===========================================================================//

//Тут делаем объявление типов не через макрос, чтобы работал Code Completion
const integer EntityComponent::TypeID = EntitySystem::GetComponentTypeID("EntityComponent");
const string EntityComponent::TypeName = "EntityComponent";

//===========================================================================//

EntityComponent::EntityComponent(const string & domainName) :
	_id(EntitySystem::GenerateComponentID()),
	_name(),
	_domainName(domainName),
	_owner(),
	_isRegistred(false)
{
#ifdef DEBUGGING
	if (!EntitySystem::IsDomainNameValid(_domainName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Invalid domain name: \"{0}\"", domainName));
	}
#endif
}

//===========================================================================//

EntityComponent::~EntityComponent()
{
}

//===========================================================================//

void EntityComponent::Reset()
{
}

//===========================================================================//

//Тут делаем объявление типов не через макрос, чтобы работал Code Completion
integer EntityComponent::GetTypeID() const
{
	return EntityComponent::TypeID;
}

//===========================================================================//

const string & EntityComponent::GetTypeName() const
{
	return EntityComponent::TypeName;
}

//===========================================================================//

bool EntityComponent::IsDerivedFromType(integer componentTypeID) const
{
	return componentTypeID == TypeID;
}

//===========================================================================//

integer EntityComponent::GetFamilyBaseTypeID() const
{
	return EntityComponent::TypeID;
}

//===========================================================================//

int64 EntityComponent::GetID() const
{
	return _id;
}

//===========================================================================//

const string & EntityComponent::GetDomainName() const
{
	return _domainName;
}

//===========================================================================//

WeakPtr<Entity> EntityComponent::GetOwner() const
{
	return _owner;
}

//===========================================================================//

string EntityComponent::GetName() const
{
	if (_name.IsEmpty())
	{
		SetName(GetTypeName());
	}

	return _name;
}

//===========================================================================//

void EntityComponent::Register()
{
	_isRegistred = true;
}

//===========================================================================//

void EntityComponent::Unregister()
{
	_isRegistred = false;
}

//===========================================================================//

bool EntityComponent::IsRegistred() const
{
	return _isRegistred;
}

//===========================================================================//

void EntityComponent::OnAddedToOwner()
{
}

//===========================================================================//

void EntityComponent::OnRemovingFromOwner()
{
}

//===========================================================================//

void EntityComponent::OnOwnerAddedChild(const SharedPtr<Entity> &)
{
}

//===========================================================================//

void EntityComponent::OnOwnerRemovingChild(const SharedPtr<Entity> &)
{
}

//===========================================================================//

void EntityComponent::SetName(const string & name) const
{
	_name = string::Format("{0}_{1}", name, _id);
}

//===========================================================================//

void EntityComponent::SetDomainName(const string & domainName)
{
	_domainName = domainName;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Файл подключает основные классы EntitySystem
//

#ifndef ENTITY_SYSTEM_H
#define ENTITY_SYSTEM_H

#include "EntitySystemMacroses.h"
#include "Entity.h"
#include "EntityComponent.h"
#include "ComponentDataProvider.h"
#include "DirectComponentDataProvider.h"

namespace Bru
{

//===========================================================================//

class EntitySystem
{
public:
	static const string GlobalDomainName;
	
	static int64 GenerateEntityID();
	static int64 GenerateComponentID();	
	
	static integer GetComponentTypeID(const string & componentTypeName);
	static const string & GetComponentTypeName(integer componentTypeID);

	static bool IsDomainNameValid(const string & domainName);

private:
	static int64 _lastEntityID;
	static int64 _lastComponentID;
	
	static TreeMap<string, integer> _componentTypeIDs;
	static integer _lastComponentTypeID;

	EntitySystem() = delete;
	EntitySystem(const EntitySystem &) = delete;
	~EntitySystem() = delete;
	EntitySystem & operator =(const EntitySystem &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ENTITY_SYSTEM_H

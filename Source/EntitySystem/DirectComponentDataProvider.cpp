//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DirectComponentDataProvider.h"

#include "../Utils/ParametersFileManager/ParametersFileManager.h"
#include "../Utils/PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

DirectComponentDataProvider::DirectComponentDataProvider(const string & componentTypeName) :
	BaseDataProvider(),
	_file(ParametersFileManager->Get(PathHelper::PathToComponents)),
	_componentTypeName(componentTypeName)
{
}

//===========================================================================//

DirectComponentDataProvider::~DirectComponentDataProvider()
{
}

//===========================================================================//

string DirectComponentDataProvider::GetParameter(const string & key) const
{
	//_file->GetSection(key)->GetParameter(_componentTypeName);
	return _file->GetParameter(string::Format("{0}.{1}", _componentTypeName, key));
}

//===========================================================================//

void DirectComponentDataProvider::SetParameter(const string &, const string &)
{
	INVALID_OPERATION_EXCEPTION("This method has no sence and should never be called");
}

//===========================================================================//

List<const WeakPtr<BaseDataProvider>> DirectComponentDataProvider::GetAnonymousChildren(const string &) const
{
	INVALID_OPERATION_EXCEPTION("This method has no sence and should never be called");
}

//===========================================================================//

integer DirectComponentDataProvider::GetAnonymousChildrenCount() const
{
	INVALID_OPERATION_EXCEPTION("This method has no sence and should never be called");
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ComponentDataProvider.h"

#include "../Utils/PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

ComponentDataProvider::ComponentDataProvider(const WeakPtr<ParametersFile> & file, integer componentIndex,
                                             const string & componentTypeName) :
	BaseDataProvider(),
	_section(file->GetSection(string::Format("{0}.{1}", PathHelper::PathToComponentsSection, componentIndex))),
	_componentIndexInFile(componentIndex),
	_directProvider(new DirectComponentDataProvider(componentTypeName))
{

}

//===========================================================================//

ComponentDataProvider::~ComponentDataProvider()
{
}

//===========================================================================//

string ComponentDataProvider::GetParameter(const string & key) const
{
	if (_section->ContainsParameter(key))
	{
		return _section->GetParameter(key);
	}
	else
	{
		return _directProvider->GetParameter(key);
	}
}

//===========================================================================//

void ComponentDataProvider::SetParameter(const string &, const string &)
{
	INVALID_OPERATION_EXCEPTION("This method has no sence and should never be called");
}

//===========================================================================//

List<const WeakPtr<BaseDataProvider>> ComponentDataProvider::GetAnonymousChildren(const string &) const
{
	INVALID_OPERATION_EXCEPTION("This method has no sence and should never be called");
}

//===========================================================================//

integer ComponentDataProvider::GetAnonymousChildrenCount() const
{
	INVALID_OPERATION_EXCEPTION("This method has no sence and should never be called");
}

//===========================================================================//

} // namespace Bru

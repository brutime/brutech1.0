//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntitySystem.h"

namespace Bru
{

//===========================================================================//

const string EntitySystem::GlobalDomainName = "Global";

int64 EntitySystem::_lastEntityID = 0;
int64 EntitySystem::_lastComponentID = 0;

TreeMap<string, integer> EntitySystem::_componentTypeIDs = {};
integer EntitySystem::_lastComponentTypeID = 1;

//===========================================================================//

int64 EntitySystem::GenerateEntityID()
{
	_lastEntityID++;
	return _lastEntityID;
}

//===========================================================================//

int64 EntitySystem::GenerateComponentID()
{
	_lastComponentID++;
	return _lastComponentID;
}

//===========================================================================//

integer EntitySystem::GetComponentTypeID(const string & componentTypeName)
{	
	if (!_componentTypeIDs.Contains(componentTypeName))
	{
		_componentTypeIDs.Add(componentTypeName, _lastComponentTypeID);
		_lastComponentTypeID++;
	}
	
	return _componentTypeIDs[componentTypeName];
}

//===========================================================================//

const string & EntitySystem::GetComponentTypeName(integer componentTypeID)
{
	for (const auto & componentTypeIDPair : _componentTypeIDs)
	{
		if (componentTypeIDPair.GetValue() == componentTypeID)
		{
			return componentTypeIDPair.GetKey();	
		}
	}
	
	INVALID_ARGUMENT_EXCEPTION(string::Format("Not found component type ID: {0}", componentTypeID));
}

//===========================================================================//

bool EntitySystem::IsDomainNameValid(const string & domainName)
{
	return !domainName.IsEmptyOrBlankCharsOnly();
}

//===========================================================================//

} // namespace Bru

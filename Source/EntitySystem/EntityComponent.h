//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Компонент Entity System
//

#pragma once
#ifndef ENTITY_COMPONENT_H
#define ENTITY_COMPONENT_H

#include "../Common/Common.h"
#include "EntitySystemMacroses.h"

namespace Bru
{

//===========================================================================//

class Entity;

//===========================================================================//

class EntityComponent
{
	friend class Entity;

public:
	static const integer TypeID;
	static const string TypeName;

	virtual ~EntityComponent();

	virtual void Reset();
	
	//Тут делаем объявление типов не через макрос, чтобы работал Code Completion
	virtual integer GetTypeID() const;
	virtual const string & GetTypeName() const;

	virtual bool IsDerivedFromType(integer componentTypeID) const; 
	
	virtual integer GetFamilyBaseTypeID() const;		

	template <typename T>
	SharedPtr<T> GetOwnerComponent() const;
	template <typename T>
	SharedPtr<T> GetOwnerComponent(integer componentTypeID) const;

	int64 GetID() const;
	string GetName() const;
	const string & GetDomainName() const;
	WeakPtr<Entity> GetOwner() const;

protected:
	EntityComponent(const string & domainName);	

	virtual void Register();
	virtual void Unregister();
	bool IsRegistred() const;

	virtual void OnAddedToOwner();
	virtual void OnRemovingFromOwner();
	
	virtual void OnOwnerAddedChild(const SharedPtr<Entity> & child);
	virtual void OnOwnerRemovingChild(const SharedPtr<Entity> & child);	

	void SetName(const string & name) const;
	
	void SetDomainName(const string & domainName);

private:
	int64 _id;
	mutable string _name;
	string _domainName;
	WeakPtr<Entity> _owner;
	bool _isRegistred;

	EntityComponent(const EntityComponent &) = delete;
	EntityComponent & operator =(const EntityComponent &) = delete;
};

//===========================================================================//

} // namespace Bru

#include "Entity.h"

namespace Bru
{

//===========================================================================//

template <typename T>
SharedPtr<T> EntityComponent::GetOwnerComponent() const
{
	if (_owner == NullPtr)
	{
		return NullPtr;
	}
	else
	{
		return _owner->GetComponent<T>();
	}
}

//===========================================================================//

template <typename T>
SharedPtr<T> EntityComponent::GetOwnerComponent(integer componentTypeID) const
{
	if (_owner == NullPtr)
	{
		return NullPtr;
	}
	else
	{
		return _owner->GetComponent<T>(componentTypeID);
	}	
}

//===========================================================================//

} // namespace Bru

#endif // ENTITY_COMPONENT_H

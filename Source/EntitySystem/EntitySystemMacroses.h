//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Макросы EntitySystem
//

#pragma once
#ifndef ENTITY_SYSTEM_MACROSES_H
#define ENTITY_SYSTEM_MACROSES_H

namespace Bru
{

//===========================================================================//

//Объявление обычного компонента
#define DECLARE_COMPONENT \
	static const integer TypeID; \
	static const string TypeName; \
	 \
	virtual integer GetTypeID() const; \
	virtual const string & GetTypeName() const; \
	 \
	virtual bool IsDerivedFromType(integer componentTypeID) const; \
 
//Объявление базового компонента в семействе
#define DECLARE_FAMILY_BASE_COMPONENT \
	DECLARE_COMPONENT \
	 \
	virtual integer GetFamilyBaseTypeID() const; \
	
//Реализация обычного компонента	
#define IMPLEMENT_COMPONENT(componentTypeName, parentComponentTypeName) \
	const integer componentTypeName::TypeID = EntitySystem::GetComponentTypeID(#componentTypeName); \
	const string componentTypeName::TypeName = #componentTypeName; \
	 \
	integer componentTypeName::GetTypeID() const \
	{ \
		return componentTypeName::TypeID; \
	} \
	 \
	const string & componentTypeName::GetTypeName() const \
	{ \
		return componentTypeName::TypeName;\
	} \
	 \
	bool componentTypeName::IsDerivedFromType(integer componentTypeID) const \
	{ \
		if (componentTypeID == componentTypeName::TypeID) \
		{ \
			return true; \
		} \
		return parentComponentTypeName::IsDerivedFromType(componentTypeID); \
	}

//Реализация базового компонента в семействе
#define IMPLEMENT_FAMILY_BASE_COMPONENT(componentTypeName, parentComponentTypeName) \
	IMPLEMENT_COMPONENT(componentTypeName, parentComponentTypeName) \
	 \
	integer componentTypeName::GetFamilyBaseTypeID() const \
	{ \
		return componentTypeName::TypeID; \
	}

//===========================================================================//

} // namespace Bru

#endif // ENTITY_SYSTEM_MACROSES_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Entity.h"

#include "../Utils/Console/Console.h"
#include "EntitySystem.h"

namespace Bru
{

//===========================================================================//

Entity::Entity(const string & type, const string & groupName) :
	PtrFromThisMixIn<Entity>(),
	_type(type),
	_id(EntitySystem::GenerateEntityID()),
	_name(string::Format("{0}_{1}", _type, _id)),
	_groupName(groupName),
	_components(),
	_parent(),
	_children(),
	_isInPool(false)
{
}

//===========================================================================//

Entity::~Entity()
{
	RemoveAllComponents();
	RemoveAllChildren();
}

//===========================================================================//

void Entity::AddComponent(const SharedPtr<EntityComponent> & component)
{
#ifdef DEBUGGING
	if (_components.Contains(component->GetFamilyBaseTypeID()))
	{
		string componentTypeName = EntitySystem::GetComponentTypeName(component->GetFamilyBaseTypeID());
		INVALID_ARGUMENT_EXCEPTION(string::Format("Entity already contains component with family base type: \"{0}\"", componentTypeName));
	}
#endif

	_components.Add(component->GetFamilyBaseTypeID(), component);
	component->_owner = GetWeakPtr();
	component->OnAddedToOwner();
}

//===========================================================================//

bool Entity::ContainsComponent(integer componentTypeID)
{
	//Нам может придти ID базового компонента
	if (_components.Contains(componentTypeID))
	{
		return true;
	}

	//Или ID производного компонента
	for (const auto & componentsPair : _components)
	{
		if (componentsPair.GetValue()->IsDerivedFromType(componentTypeID))
		{
			return true;
		}
	}

	return false;
}

//===========================================================================//

const TreeMap<integer, SharedPtr<EntityComponent>> & Entity::GetComponents() const
{
	return _components;
}

//===========================================================================//

void Entity::RegisterAllComponents(const string & domainName)
{
	for (auto & pair : _components)
	{
		const auto & component = pair.GetValue();
		component->SetDomainName(domainName);
		component->Register();
	}
}

//===========================================================================//

void Entity::RegisterAndResetAllComponents(const string & domainName)
{
	for (auto & pair : _components)
	{
		const auto & component = pair.GetValue();
		component->SetDomainName(domainName);
		component->Register();
		component->Reset();
	}
}

//===========================================================================//

void Entity::UnregisterAllComponents()
{
	for (auto & pair : _components)
	{
		const SharedPtr<EntityComponent> & component = pair.GetValue();
		if (component->IsRegistred())
		{
			component->Unregister();
		}
	}	
}

//===========================================================================//

void Entity::RemoveComponent(integer componentTypeID)
{
	const auto & component = GetComponent<EntityComponent>(componentTypeID);

#ifdef DEBUGGING
	if (component == NullPtr)
	{
		string componentTypeName = EntitySystem::GetComponentTypeName(componentTypeID);
		INVALID_ARGUMENT_EXCEPTION(string::Format("Entity not contains component with family base type: \"{0}\"", componentTypeName));
	}
#endif

	component->OnRemovingFromOwner();
	component->_owner = NullPtr;
	_components.Remove(component->GetFamilyBaseTypeID());
}

//===========================================================================//

void Entity::RemoveAllComponents()
{
	//Нельзя просто очистить список - нужно отвязать от owner'а
	for (auto & pair : _components)
	{
		pair.GetValue()->_owner = NullPtr;
	}
	_components.Clear();
}

//===========================================================================//

const WeakPtr<Entity> & Entity::GetParent() const
{
	return _parent;
}

//===========================================================================//

void Entity::AddChild(const SharedPtr<Entity> & child)
{
#ifdef DEBUGGING
	if (child == this)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Self adding entity \"{0}\"", GetName()));
	}

	if (child->_parent != NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Child \"{0}\" already has parent \"{1}\"", child->GetName(), child->_parent->GetName()));
	}
#endif

	_children.Add(child);
	child->_parent = GetSharedPtr();
	for (const auto & component : _components)
	{
		component.GetValue()->OnOwnerAddedChild(child);
	}
}

//===========================================================================//

bool Entity::ContainsChild(const SharedPtr<Entity> & child)
{
	return _children.Contains(child);
}

//===========================================================================//

void Entity::RemoveChild(const SharedPtr<Entity> & child)
{
#ifdef DEBUGGING
	if (!_children.Contains(child))
	{
		INVALID_ARGUMENT_EXCEPTION("Entity not contains child");
	}
#endif

	for (const auto & component : _components)
	{
		component.GetValue()->OnOwnerRemovingChild(child);
	}
	_children.Remove(child);
	child->_parent = NullPtr;
}

//===========================================================================//

void Entity::RemoveAllChildren()
{
	//Нельзя просто так взять и очистить список (с) - нужно отвязаться от parent'а еще и рекурсивно	
	//И вызвать событие удаления OnOwnerRemovingChild
	while (_children.GetCount() > 0)
	{
		SharedPtr<Entity> child = _children.GetFirst();
		child->RemoveAllChildren();
		RemoveChild(child);
	}
}

//===========================================================================//

const List<SharedPtr<Entity> > & Entity::GetChildren() const
{
	return _children;
}

//===========================================================================//

bool Entity::IsInPool() const
{
	return _isInPool;
}

//===========================================================================//

const string & Entity::GetType() const
{
	return _type;
}

//===========================================================================//

int64 Entity::GetID() const
{
	return _id;
}

//===========================================================================//

const string Entity::GetName() const
{
	return _name;
}

//===========================================================================//

const string & Entity::GetGroupName() const
{
	return _groupName;
}

//===========================================================================//

void Entity::ChangeDomain(const string & domainName)
{
	for (auto & pair : _components)
	{
		const SharedPtr<EntityComponent> & component = pair.GetValue();
		if (component->GetDomainName() == domainName)
		{
			continue;
		}

		if (component->IsRegistred())
		{
			component->Unregister();
			component->SetDomainName(domainName);
			component->Register();
		}
		else
		{
			component->SetDomainName(domainName);
		}
	}
}

//===========================================================================//

void Entity::PutToPool()
{
	UnregisterAllComponents();
	_isInPool = true;
}

//===========================================================================//

void Entity::ReturnFromPool(const string & domainName)
{
	RegisterAndResetAllComponents(domainName);
	_isInPool = false;
}

//===========================================================================//

void Entity::PrintInfo() const
{
	Console->Hint(string::Format("Entity \"{0}\" components:", GetName()));
	Console->AddOffset(2);
	for (auto & pair : _components)
	{
		Console->Hint(string::Format("- {0}", pair.GetValue()->GetName()));
	}

	if (_children.GetCount() == 0)
	{
		Console->ReduceOffset(2);
		return;
	}

	Console->AddOffset(2);
	Console->Hint("Children:");
	Console->AddOffset(2);
	for (const SharedPtr<Entity> & child : _children)
	{
		child->PrintInfo();
	}
	Console->ReduceOffset(6);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WidgetsManagerInputReceiver.h"

#include "WidgetsManager.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(WidgetsManagerInputReceiver, InputReceiver)

//===========================================================================//

WidgetsManagerInputReceiver::WidgetsManagerInputReceiver() :
	InputReceiver(EntitySystem::GlobalDomainName)
{
}

//===========================================================================//

WidgetsManagerInputReceiver::~WidgetsManagerInputReceiver()
{
}

//===========================================================================//

void WidgetsManagerInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	WidgetsManager->DispatchKeyboardEvent(args);
}

//===========================================================================//

void WidgetsManagerInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	WidgetsManager->DispatchMouseEvent(args);
}

//===========================================================================//

void WidgetsManagerInputReceiver::DispatchJoystickEvent(const JoystickEventArgs & args)
{
	WidgetsManager->DispatchJoystickEvent(args);
}

//===========================================================================//

}// namespace Bru

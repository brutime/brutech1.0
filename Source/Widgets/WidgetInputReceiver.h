//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef WIDGET_INPUT_RECEIVER_H
#define WIDGET_INPUT_RECEIVER_H

#include "../OperatingSystem/OperatingSystem.h"
#include "../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class Widget;

//===========================================================================//

class WidgetInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	WidgetInputReceiver(const string & domainName, ProjectionMode mode, Widget * const component);
	virtual ~WidgetInputReceiver();

	const MouseEventArgs & GetLastMouseEventArgs() const;

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);

private:
	ProjectionMode _mode;
	Widget * const _component;

	MouseEventArgs _lastMouseEventArgs;

	WidgetInputReceiver(const WidgetInputReceiver &) = delete;
	WidgetInputReceiver & operator =(const WidgetInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // WIDGET_INPUT_RECEIVER_H

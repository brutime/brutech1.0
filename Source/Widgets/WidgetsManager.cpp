//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WidgetsManager.h"

#include "../Utils/Config/Config.h"
#include "../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

SharedPtr<WidgetsManagerSingleton> WidgetsManager;

//===========================================================================//

const string WidgetsManagerSingleton::CursorLayerName = "Cursor";

const string WidgetsManagerSingleton::Name = "Widgets manager";

const string WidgetsManagerSingleton::DefaultCursorName = "DefaultCursor.Name";

//===========================================================================//

WidgetsManagerSingleton::WidgetsManagerSingleton() :
	_cursorLayer(),
//    _positioningNode(),
	_cursorNode(),
	_cursor(),
	_inputReceiver(new WidgetsManagerInputReceiver())
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	Renderer->AddLayerToFront(EntitySystem::GlobalDomainName, new RenderableLayer(CursorLayerName, ProjectionMode::PercentsOrtho));

	_cursorNode = new PositioningNode(EntitySystem::GlobalDomainName);

	string cursorName = Config->Get<string>(DefaultCursorName);
	SetCursor(new Cursor(EntitySystem::GlobalDomainName, CursorLayerName, cursorName));

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

WidgetsManagerSingleton::~WidgetsManagerSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	_cursor = NullPtr;
	_cursorNode = NullPtr;
	_cursorLayer = NullPtr;

	_inputReceiver = NullPtr;

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void WidgetsManagerSingleton::DispatchMouseEvent(const MouseEventArgs & args)
{
	Vector2 mouseWorldCoords = Renderer->PixelsToCoords(ProjectionMode::PercentsOrtho, {args.X, args.Y});
	mouseWorldCoords.Y = mouseWorldCoords.Y;

	UpdateCursor(mouseWorldCoords);
}

//===========================================================================//

void WidgetsManagerSingleton::UpdateCursor(const Vector2 & mouseWorldCoords)
{
	_cursorNode->SetPosition(mouseWorldCoords.X, mouseWorldCoords.Y);
}

//===========================================================================//

void WidgetsManagerSingleton::SetCursor(const SharedPtr<Cursor> & newCursor)
{
	newCursor->GetSprite()->SetPositioningNode(_cursorNode);
	_cursor = newCursor;
}

//===========================================================================//

void WidgetsManagerSingleton::ShowCursor()
{
	_cursor->Show();
}

//===========================================================================//

void WidgetsManagerSingleton::HideCursor()
{
	_cursor->Hide();
}

//===========================================================================//

}// namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TextPanelLine.h"

namespace Bru
{

//===========================================================================//

TextPanelLine::TextPanelLine() :
	Text(),
	FontColor(Color::Default)
{
}

//===========================================================================//

TextPanelLine::TextPanelLine(const string & text, const Color & fontColor) :
	Text(text),
	FontColor(fontColor)
{
}

//===========================================================================//

TextPanelLine::~TextPanelLine()
{
}

//===========================================================================//

} // namespace Bru
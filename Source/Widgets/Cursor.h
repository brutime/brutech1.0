//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: содержит данные об изображении курсора и положении его центра
//

#ifndef CURSOR_H
#define CURSOR_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "../Renderer/Sprites/BaseSprite.h"
#include "Enums/CursorOrigin.h"

namespace Bru
{

//===========================================================================//

class Cursor
{
public:
	Cursor(const string & domainName, const string & layerName, const string & pathToFile);
	~Cursor();

	void SetVisible(bool flag);
	bool IsVisible() const
	{
		return _isVisible;
	}

	void Show()
	{
		SetVisible(true);
	}
	void Hide()
	{
		SetVisible(false);
	}

	SharedPtr<BaseSprite> GetSprite()
	{
		return _sprite;
	}

private:
	static const string PathToPathToSprite;
	static const string PathToOrigin;

	static Vector3 CoordsForOriginFactor(CursorOrigin origin);
	static CursorOrigin GetCursorOriginByIntegerValue(integer value);

	SharedPtr<BaseSprite> _sprite;
	CursorOrigin _origin;
	bool _isVisible;

	Cursor(const Cursor &) = delete;
	Cursor & operator =(const Cursor &) = delete;
};

//===========================================================================//

}

#endif // CURSOR_H

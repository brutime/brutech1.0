//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef WIDGETS_MANAGER_H
#define WIDGETS_MANAGER_H

#include "Frame.h"
#include "Button.h"
#include "Cursor.h"
#include "WidgetsManagerInputReceiver.h"

namespace Bru
{

//===========================================================================//

class WidgetsManagerSingleton
{
public:
	static const string CursorLayerName;

	WidgetsManagerSingleton();
	~WidgetsManagerSingleton();

	void DispatchKeyboardEvent(const KeyboardEventArgs &)
	{
	}
	;
	void DispatchMouseEvent(const MouseEventArgs & args);
	void DispatchJoystickEvent(const JoystickEventArgs &)
	{
	}
	;

	//SharedPtr<SceneNode> GetSceneNode() { return _positioningNode; }

	void SetCursor(const SharedPtr<Cursor> & newCursor);
	void ShowCursor();
	void HideCursor();

private:
	static const string Name;
	static const string DefaultCursorName;

	void UpdateCursor(const Vector2 & mouseWorldCoords);

	SharedPtr<RenderableLayer> _cursorLayer;
//    SharedPtr<SceneNode> _positioningNode;

	SharedPtr<PositioningNode> _cursorNode;
	SharedPtr<Cursor> _cursor;

	SharedPtr<WidgetsManagerInputReceiver> _inputReceiver;

	WidgetsManagerSingleton(const WidgetsManagerSingleton &) = delete;
	WidgetsManagerSingleton & operator =(const WidgetsManagerSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<WidgetsManagerSingleton> WidgetsManager;

//===========================================================================//

}//namespace

#endif // WIDGETS_MANAGER_H

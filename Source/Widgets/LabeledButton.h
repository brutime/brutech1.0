//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LABELED_BUTTON_H
#define LABELED_BUTTON_H

#include "Button.h"
#include "Label.h"

namespace Bru
{

//===========================================================================//

class LabeledButton : public Button
{
public:
	LabeledButton(const string & domainName, const string & layerName, float width, float height,
	              const string & normalTextureName, const string & pressedTextureName, const string & hoveredTextureName,
	              const Color & normalColor = Color::Default, const Color & pressedColor = Color::Default, const Color & hoveredColor = Color::Default);

	LabeledButton(const string & domainName, const string & layerName, float width, float height,
	              const WeakPtr<Font> & font, const Color & fontColor,
	              const string & normalTextureName, const string & pressedTextureName, const string & hoveredTextureName,
	              const Color & normalColor = Color::Default, const Color & pressedColor = Color::Default, const Color & hoveredColor = Color::Default);

	virtual ~LabeledButton();

	void SetText(const string & text);
	string GetText() const;

private:
	SharedPtr<Label> _label;
};

//===========================================================================//

} // namespace Bru

#endif // LABELED_BUTTON_H

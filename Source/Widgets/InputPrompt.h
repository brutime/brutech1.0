//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: строка с приглашением для ввода текста
//

#ifndef INPUT_PROMPT_H
#define INPUT_PROMPT_H

#include "InputBox.h"

namespace Bru
{

//===========================================================================//

class InputPrompt : public InputBox
{
public:
	InputPrompt(const string & domainName, const string & layerName, const string & promptText, float x, float y,
	            InputCursorOrientation cursorOrientation, const WeakPtr<Font> & font = FontManager->GetDefaultFont(),
	            const Color & color = Color::Default);
	virtual ~InputPrompt();

	virtual void SetText(const string & text);
	virtual void AddText(const string & text);
	virtual void InsertText(integer index, const string & text);
	virtual void RemoveText(integer index, integer count = 1);

	virtual void SetVisible(bool flag);

	virtual void SetViewOrder(integer order);

	string GetWord(integer wordIndex) const;
	string GetWordAtCursor() const;
	integer GetWordIndexAtCursor() const;
	void ReplaceWord(integer wordIndex, const string & word);

	integer GetWordsCount() const;

protected:
	virtual void OnKeyDown(KeyboardKey key);
	virtual void OnKeyUp(KeyboardKey);

private:
	void PreviousHistoryCommand();
	void NextHistoryCommand();

	void UpdateSize();

	integer GetCharIndex() const;

	SharedPtr<Label> _promtLabel;

	Array<string> _commandHistory;
	integer _commandHistoryIndex;

	InputPrompt(const InputPrompt &) = delete;
	InputPrompt & operator =(const InputPrompt &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // INPUT_PROMPT_H

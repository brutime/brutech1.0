//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef BUTTON_H
#define BUTTON_H

#include "../Common/EventSystem/Event.h"
#include "../Renderer/Sprites/Sprite/Sprite.h"
#include "Widget.h"
#include "Label.h"

namespace Bru
{

//===========================================================================//

class Button : public Widget
{
public:
	Button(const string & domainName, const string & layerName, float width, float height,
	       const string & normalTextureName, const string & pressedTextureName, const string & hoveredTextureName,
	       const Color & normalColor = Colors::White, const Color & pressedColor = Colors::White, const Color & hoveredColor = Colors::White);

	Button(const string & domainName, const string & layerName, integer width, integer height,
	       const string & normalTextureName, const string & pressedTextureName, const string & hoveredTextureName,
	       const Color & normalColor = Colors::White, const Color & pressedColor = Colors::White, const Color & hoveredColor = Colors::White);

	virtual ~Button();

	virtual void SetVisible(bool flag);
	void SetViewOrder(integer order);
	
	void SetNormalTextureName(const string & normalTextureName);
	string GetNormalTextureName() const;
	
	void SetPressedTextureName(const string & pressedTextureName);
	string GetPressedTextureName() const;

	void SetHoveredTextureName(const string & hoveredTextureName);
	string GetHoveredTextureName() const;
	
	void SetNormalColor(const Color & normalColor);
	const Color & GetNormalColor() const;
	
	void SetPressedColor(const Color & pressedColor);
	const Color & GetPressedColor() const;

	void SetHoveredColor(const Color & hoveredColor);
	const Color & GetHoveredColor() const;

	virtual void Enable();
	virtual void Disable();

protected:
	void OnMouseOver(const MouseEventArgs & args);
	void OnMouseLeave(const MouseEventArgs & args);
	void OnMouseLeftDown(const MouseEventArgs & args);
	void OnMouseLeftUp(const MouseEventArgs & args);

	SharedPtr<Sprite> _sprite;

	WeakPtr<Texture> _normalTexture;
	WeakPtr<Texture> _pressedTexture;
	WeakPtr<Texture> _hoveredTexture;
	Color _normalColor;
	Color _pressedColor;
	Color _hoveredColor;

private:
	void Initialize(const string & domainName, const string & layerName, const string & normalTextureName,
	                const string & pressedTextureName, const string & hoveredTextureName);
};

//===========================================================================//

}// namespace

#endif // BUTTON_H

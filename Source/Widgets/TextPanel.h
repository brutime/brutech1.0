//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Текстовая панель
//

#ifndef TEXT_PANEL_H
#define TEXT_PANEL_H

#include "Frame.h"
#include "Label.h"
#include "Enums/TextPanelAlignment.h"
#include "TextPanelLine.h"

namespace Bru
{

//===========================================================================//

class TextPanel : public Frame
{
public:
	TextPanel(const string & domainName, const string & layerName, const WeakPtr<Font> & font,
	          TextPanelAlignment textAlignment, float horizontalOffset, float verticalOffset, 
			  float width, float height, const Color & backgroundColor);
	TextPanel(const string & domainName, const string & layerName, float width, float height,
	          const Color & backgroundColor);

	TextPanel(const string & domainName, const string & layerName, const WeakPtr<Font> & font,
	          TextPanelAlignment textAlignment, integer horizontalOffset, integer verticalOffset, 
			  integer width, integer height, const Color & backgroundColor);
	TextPanel(const string & domainName, const string & layerName, integer width, integer height,
	          const Color & backgroundColor);

	virtual ~TextPanel();

	void AddLine(const string & text, const Color & color);
	void RemoveLineAt(integer index);
	
	void SetLineText(integer index, const string & text);
	void SetLineText(integer index, const string & text, const Color & color);
	const string & GetLineText(integer index) const;
	
	integer GetLineIndexByText(const string & text) const;
	
	void SetLines(const Array<SharedPtr<TextPanelLine>> & lines);	
	const Array<SharedPtr<TextPanelLine>> & GetLines() const;
	
	void Clear();

	void SetScrollIndex(integer linesScroll);
	integer GetScrollIndex() const;
	
	void ScrollToBegin();
	void ScrollToEnd();
	
	void ScrollUp();
	void ScrollDown();
	
	void EnableUpdating();
	void DisableUpdating();

protected:
	void Initialize(const string & domainName, const string & layerName);
	void ArrangeLabels();
	void UpdateLabels();
	void IncrementScrollIndex();
	void DecrementScrollIndex();
	float GetLinesDistance(const WeakPtr<Font> & font);
	integer GetMaxVisibleLinesCount();
	integer GetVisibleLinesCount();

	Array<SharedPtr<TextPanelLine>> _lines;
	Array<SharedPtr<Label>> _labels;

	ProjectionMode _projectionMode;
	WeakPtr<Font> _font;
	TextPanelAlignment _textAlignment;
	float _horizontalOffset;
	float _verticalOffset;
	float _linesDistance;
	integer _maxVisibleLinesCount;
	integer _scrollIndex;
	
	bool _isUpdatingEnabled;

	TextPanel(const TextPanel &) = delete;
	TextPanel & operator =(const TextPanel &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // TEXT_PANEL_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LabeledButton.h"

namespace Bru
{

//===========================================================================//

LabeledButton::LabeledButton(const string & domainName, const string & layerName,
                             float width, float height, const string & normalTextureName,
                             const string & pressedTextureName, const string & hoveredTextureName,
                             const Color & normalColor, const Color & pressedColor, const Color & hoveredColor):
	LabeledButton(domainName, layerName, width, height, FontManager->GetDefaultFont(), Color::Default,
	              normalTextureName, pressedTextureName, hoveredTextureName,
	              normalColor, pressedColor, hoveredColor)
{
}

//===========================================================================//

LabeledButton::LabeledButton(const string & domainName, const string & layerName,
                             float width, float height, const WeakPtr<Font> & font,
                             const Color & fontColor, const string & normalTextureName,
                             const string & pressedTextureName, const string & hoveredTextureName,
                             const Color & normalColor, const Color & pressedColor, const Color & hoveredColor) :
	Button(domainName, layerName, width, height,
	       normalTextureName, pressedTextureName, hoveredTextureName,
	       normalColor, pressedColor, hoveredColor),
	_label(new Label(domainName, layerName, string::Empty, font, fontColor,
	                 Vector2(width, height), WordWrapMode::Smart,
	                 TextHorizontalAlignment::Center, TextVerticalAlignment::Center))
{
	AddChild(_label);
	_label->MoveTo(width * 0.5f, height * 0.5f);
}

//===========================================================================//

void LabeledButton::SetText(const string & text)
{
	string newText = text;
	_label->SetText(text);

	// грязное решение, гореть мне в аду
	while (_label->GetSize().X > GetSize().X)
	{
		newText = newText.Substring(0, newText.GetLength() - 1);
		_label->SetText(newText);
	}
}
//===========================================================================//

string LabeledButton::GetText() const
{
	return _label->GetText();
}

//===========================================================================//

LabeledButton::~LabeledButton()
{
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Абстрактное положение точки относительно заданных границ
//

#pragma once
#ifndef CURSOR_ORIGIN
#define CURSOR_ORIGIN

namespace Bru
{

//===========================================================================//

enum class CursorOrigin
{
    UpperLeft,
    UpperCenter,
    UpperRight,
    MiddleLeft,
    Center,
    MiddleRight,
    LowerLeft,
    LowerCenter,
    LowerRight,
    Count
};

//===========================================================================//

}//namespace

#endif // CURSOR_ORIGIN

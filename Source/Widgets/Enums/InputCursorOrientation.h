//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Ориентация курсора при вводе текста
//

#pragma once
#ifndef INPUT_CURSOR_ORIENTATION_H
#define INPUT_CURSOR_ORIENTATION_H

namespace Bru
{

//===========================================================================//

enum class InputCursorOrientation
{
    Vertical,
    Horizontal
};

//===========================================================================//

}//namespace

#endif // INPUT_CURSOR_ORIENTATION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Расположение текста в текстовой панели
//

#pragma once
#ifndef TEXT_PANEL_ALIGNMENT_H
#define TEXT_PANEL_ALIGNMENT_H

namespace Bru
{

//===========================================================================//

///Расположение текста в текстовой панели
enum class TextPanelAlignment
{
    Up,
    Down
};

//===========================================================================//

}//namespace

#endif // TEXT_PANEL_ALIGNMENT_H

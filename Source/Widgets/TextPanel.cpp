//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TextPanel.h"

namespace Bru
{

//===========================================================================//

TextPanel::TextPanel(const string & domainName, const string & layerName, const WeakPtr<Font> & font,
                     TextPanelAlignment textAlignment, float horizontalOffset, float verticalOffset,
                     float width, float height, const Color & backgroundColor) :
	Frame(domainName, layerName, width, height, backgroundColor),
	_lines(),
	_labels(),
	_projectionMode(Renderer->GetLayer(domainName, layerName)->GetProjectionMode()),
	_font(font),
	_textAlignment(textAlignment),
	_horizontalOffset(horizontalOffset),
	_verticalOffset(verticalOffset),
	_linesDistance(GetLinesDistance(_font)),
	_maxVisibleLinesCount(GetMaxVisibleLinesCount()),
	_scrollIndex(0),
	_isUpdatingEnabled(true)
{
	Initialize(domainName, layerName);
}

//===========================================================================//

TextPanel::TextPanel(const string & domainName, const string & layerName, float width, float height,
                     const Color & backgroundColor) :
	Frame(domainName, layerName, width, height, backgroundColor),
	_lines(),
	_labels(),
	_projectionMode(Renderer->GetLayer(domainName, layerName)->GetProjectionMode()),
	_font(FontManager->GetDefaultFont()),
	_textAlignment(TextPanelAlignment::Up),
	_horizontalOffset(0.0f),
	_verticalOffset(0.0f),
	_linesDistance(GetLinesDistance(_font)),
	_maxVisibleLinesCount(GetMaxVisibleLinesCount()),
	_scrollIndex(0),
	_isUpdatingEnabled(true)
{
	Initialize(domainName, layerName);
}

//===========================================================================//

TextPanel::TextPanel(const string & domainName, const string & layerName, const WeakPtr<Font> & font,
                     TextPanelAlignment textAlignment, integer horizontalOffset, integer verticalOffset,
                     integer width, integer height, const Color & backgroundColor) :
	Frame(domainName, layerName, width, height, backgroundColor),
	_lines(),
	_labels(),
	_projectionMode(Renderer->GetLayer(domainName, layerName)->GetProjectionMode()),
	_font(font),
	_textAlignment(textAlignment),
	_horizontalOffset(horizontalOffset * Renderer->GetPixelsToCoordsFactors(_projectionMode).X),
	_verticalOffset(verticalOffset * Renderer->GetPixelsToCoordsFactors(_projectionMode).Y),
	_linesDistance(GetLinesDistance(_font)),
	_maxVisibleLinesCount(GetMaxVisibleLinesCount()),
	_scrollIndex(0),
	_isUpdatingEnabled(true)
{
	Initialize(domainName, layerName);
}

//===========================================================================//

TextPanel::TextPanel(const string & domainName, const string & layerName, integer width, integer height,
                     const Color & backgroundColor) :
	Frame(domainName, layerName, width, height, backgroundColor),
	_lines(),
	_labels(),
	_projectionMode(Renderer->GetLayer(domainName, layerName)->GetProjectionMode()),
	_font(FontManager->GetDefaultFont()),
	_textAlignment(TextPanelAlignment::Up),
	_horizontalOffset(0.0f),
	_verticalOffset(0.0f),
	_linesDistance(GetLinesDistance(_font)),
	_maxVisibleLinesCount(GetMaxVisibleLinesCount()),
	_scrollIndex(0),
	_isUpdatingEnabled(true)
{
	Initialize(domainName, layerName);
}

//===========================================================================//

TextPanel::~TextPanel()
{
}

//===========================================================================//

void TextPanel::AddLine(const string & text, const Color & color)
{
	_lines.Add(new TextPanelLine(text, color));
	UpdateLabels();
}

//===========================================================================//

void TextPanel::RemoveLineAt(integer index)
{
	_lines.RemoveAt(index);
	UpdateLabels();
}

//===========================================================================//

void TextPanel::SetLineText(integer index, const string & text)
{
	_lines[index]->Text = text;
	UpdateLabels();
}

//===========================================================================//

void TextPanel::SetLineText(integer index, const string & text, const Color & color)
{
	_lines[index]->Text = text;
	_lines[index]->FontColor = color;
	UpdateLabels();
}

//===========================================================================//

const string & TextPanel::GetLineText(integer index) const
{
	return _lines[index]->Text;
}

//===========================================================================//

integer TextPanel::GetLineIndexByText(const string & text) const
{
	for (integer i = 0; i < _lines.GetCount(); ++i)
	{
		if (_lines[i]->Text == text)
		{
			return i;
		}
	}

	return -1;
}

//===========================================================================//

void TextPanel::SetLines(const Array<SharedPtr<TextPanelLine>> & lines)
{
	_lines = lines;
	UpdateLabels();
}

//===========================================================================//

const Array<SharedPtr<TextPanelLine>> & TextPanel::GetLines() const
{
	return _lines;
}

//===========================================================================//

void TextPanel::Clear()
{
	_lines.Clear();

	if (_isUpdatingEnabled)
	{
		for (integer i = 0; i < _labels.GetCount(); i++)
		{
			_labels[i]->SetText(string::Empty);
		}
	}
}

//===========================================================================//

void TextPanel::SetScrollIndex(integer linesScroll)
{
	//Обработку попадания индекса берет на себя класс массива
	_scrollIndex = linesScroll;
	UpdateLabels();
}

//===========================================================================//

integer TextPanel::GetScrollIndex() const
{
	return _scrollIndex;
}

//===========================================================================//

void TextPanel::ScrollUp()
{
	if (_textAlignment == TextPanelAlignment::Up)
	{
		DecrementScrollIndex();
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		IncrementScrollIndex();
	}

	UpdateLabels();
}

//===========================================================================//

void TextPanel::ScrollDown()
{
	if (_textAlignment == TextPanelAlignment::Up)
	{
		IncrementScrollIndex();
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		DecrementScrollIndex();
	}

	UpdateLabels();
}

//===========================================================================//

void TextPanel::ScrollToBegin()
{
	if (_textAlignment == TextPanelAlignment::Up)
	{
		_scrollIndex = 0;
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		_scrollIndex = _lines.GetCount() - 1;
	}

	UpdateLabels();
}

//===========================================================================//

void TextPanel::ScrollToEnd()
{
	if (_textAlignment == TextPanelAlignment::Up)
	{
		_scrollIndex = _lines.GetCount() - 1;
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		_scrollIndex = 0;
	}

	UpdateLabels();
}

//===========================================================================//

void TextPanel::EnableUpdating()
{
	if (_textAlignment == TextPanelAlignment::Up)
	{
		ScrollToBegin();
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		ScrollToEnd();
	}

	_isUpdatingEnabled = true;
	UpdateLabels();
}

//===========================================================================//

void TextPanel::DisableUpdating()
{
	_isUpdatingEnabled = false;
}

//===========================================================================//

void TextPanel::Initialize(const string & domainName, const string & layerName)
{
	_labels.Resize(GetMaxVisibleLinesCount());
	for (integer i = 0; i < _labels.GetCount(); i++)
	{
		_labels[i] = new Label(domainName, layerName, string::Empty, _font);
		AddChild(_labels[i]);
	}
	ArrangeLabels();
}

//===========================================================================//

void TextPanel::ArrangeLabels()
{
	if (_textAlignment == TextPanelAlignment::Up)
	{
		float y = GetSize().Y - _linesDistance - _verticalOffset;
		for (integer i = 0; i < _maxVisibleLinesCount; i++)
		{
			const auto & label = _labels[i];

			label->MoveTo(_horizontalOffset, y);
			y -= label->GetLinesCount() * _linesDistance;
		}
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		float y = _verticalOffset;
		for (integer i = 0; i < _maxVisibleLinesCount; i++)
		{
			const auto & label = _labels[i];

			label->MoveTo(_horizontalOffset, y);
			y += label->GetLinesCount() * _linesDistance;
		}
	}
}

//===========================================================================//

void TextPanel::UpdateLabels()
{
	if (!_isUpdatingEnabled)
	{
		return;
	}

	if (_textAlignment == TextPanelAlignment::Up)
	{
		integer lineIndex = _scrollIndex;
		for (integer i = 0; i < _maxVisibleLinesCount; i++)
		{
			const auto & label = _labels[i];
			if (lineIndex < _lines.GetCount())
			{
				label->SetText(_lines[lineIndex]->Text, _lines[lineIndex]->FontColor);
			}
			else
			{
				if (label->GetText() != string::Empty)
				{
					label->SetText(string::Empty);
				}
			}
			lineIndex++;
		}
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		integer lineIndex = _lines.GetCount() - _scrollIndex - 1;
		for (integer i = 0; i < _maxVisibleLinesCount; i++)
		{
			const auto & label = _labels[i];
			if (lineIndex >= 0)
			{
				label->SetText(_lines[lineIndex]->Text, _lines[lineIndex]->FontColor);
			}
			else
			{
				if (label->GetText() != string::Empty)
				{
					label->SetText(string::Empty);
				}
			}
			lineIndex--;
		}
	}
}

//===========================================================================//

void TextPanel::IncrementScrollIndex()
{
	_scrollIndex++;
	if (_scrollIndex >= _lines.GetCount())
	{
		_scrollIndex = _lines.GetCount() > 0 ? _lines.GetCount() - 1 : 0;
	}
}

//===========================================================================//

void TextPanel::DecrementScrollIndex()
{
	_scrollIndex--;
	if (_scrollIndex < 0)
	{
		_scrollIndex = 0;
	}
}

//===========================================================================//

float TextPanel::GetLinesDistance(const WeakPtr<Font> & font)
{
	float yFactor = Renderer->GetPixelsToCoordsFactors(_projectionMode).Y;
	return font->GetLineDistance() * yFactor;
}

//===========================================================================//

integer TextPanel::GetMaxVisibleLinesCount()
{
	if (_textAlignment == TextPanelAlignment::Up)
	{
		return Math::Floor((GetSize().Y - _verticalOffset) / _linesDistance);
	}
	else if (_textAlignment == TextPanelAlignment::Down)
	{
		return Math::Ceiling((GetSize().Y - _verticalOffset) / _linesDistance);
	}
	else
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("_textAlignment {0} not defined", static_cast<integer>(_textAlignment)));
	}
}

//===========================================================================//

integer TextPanel::GetVisibleLinesCount()
{
	return Math::Min(_labels.GetCount(), _lines.GetCount());
}

//===========================================================================//

} // namespace Bru

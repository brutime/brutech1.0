//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: файл подключает основные виджеты
//

#ifndef WIDGETS_H
#define WIDGETS_H

#include "Button.h"
#include "Frame.h"
#include "WidgetsManager.h"
#include "InputBox.h"
#include "InputPrompt.h"
#include "Label.h"
#include "LabeledButton.h"
#include "TextPanel.h"

#endif // WIDGETS_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Widget.h"

#include "../Graphics/Graphics.h"
#include "../Positioning/Positioning.h"
#include "../Utils/Log/Log.h"

namespace Bru
{

//===========================================================================//

Widget::Widget(const string & domainName, const string & layerName) :
	Widget(domainName, layerName, Vector2())
{
	Initialize(domainName);
}

//===========================================================================//

Widget::Widget(const string & domainName, const string & layerName, const Vector2 & size) :
	_viewOrder(0),
	_domainName(domainName),
	_layerName(layerName),
	_size(size),
	_isEnabled(true),
	_isVisible(true),
	_isMouseOver(false),
	_isPressed(false),
	_mode(Renderer->GetLayer(domainName, layerName)->GetProjectionMode()),
	_positioningNode(),
	_children(),
	_inputReceiver(new WidgetInputReceiver(domainName, _mode, this)),
	_tag(0),
	_clickEvent(),
	_mouseOverEvent(),
	_mouseLeaveEvent(),
	_mouseLeftDownEvent(),
	_mouseLeftUpEvent(),
	Click(_clickEvent),
	MouseOver(_mouseOverEvent),
	MouseLeave(_mouseLeaveEvent),
	MouseLeftDown(_mouseLeftDownEvent),
	MouseLeftUp(_mouseLeftUpEvent)
{
	Initialize(domainName);
}

//===========================================================================//

Widget::~Widget()
{
}

//===========================================================================//

void Widget::Move(float x, float y)
{
	_positioningNode->Translate(x, y);
}

//===========================================================================//

void Widget::Move(integer x, integer y)
{
	Vector2 delta = Renderer->PixelsToCoords(_mode, Vector2(x, y));
	Move(delta.X, delta.Y);
}

//===========================================================================//

void Widget::MoveTo(float x, float y)
{
	_positioningNode->SetPosition(x, y);
}

//===========================================================================//

void Widget::MoveTo(integer x, integer y)
{
	Vector2 delta = Renderer->PixelsToCoords(_mode, Vector2(x, y));
	MoveTo(delta.X, delta.Y);
}

//===========================================================================//

void Widget::SetSize(Vector2 size)
{
	_size = size;
	Update();
}

//===========================================================================//

void Widget::SetSize(float width, float height)
{
	_size = Vector2(width, height);
	Update();
}

//===========================================================================//

void Widget::SetSize(integer width, integer height)
{
	Vector2 delta = Renderer->PixelsToCoords(_mode, Vector2(width, height));
	SetSize(delta.X, delta.Y);
}

//===========================================================================//

Vector2 Widget::GetPosition() const
{
	return Vector2(_positioningNode->GetLocalPosition().X, _positioningNode->GetLocalPosition().Y);
}

//===========================================================================//

bool Widget::ContainsPoint(float x, float y, bool translateCoordsToLocal) const
{
	const Vector3 & position = _positioningNode->GetPosition();
	if (translateCoordsToLocal)
	{
		Vector2 coords = Renderer->PixelsToCoords(_mode, Vector2(x, y));
		return (coords.X >= position.X && coords.X <= position.X + _size.X && coords.Y >= position.Y && coords.Y <= position.Y + _size.Y);
	}
	else
	{
		return (x >= position.X && x <= position.X + _size.X && y >= position.Y && y <= position.Y + _size.Y);
	}
}

//===========================================================================//

void Widget::Enable()
{
	_isMouseOver = false;
	_isPressed = false;
	_isEnabled = true;
}

//===========================================================================//

void Widget::Disable()
{
	MouseEventArgs args = _inputReceiver->GetLastMouseEventArgs();
	_isMouseOver = ContainsPoint(args.X, args.Y);
	_isPressed = args.Key == MouseKey::Left && args.State == KeyState::Down && ContainsPoint(args.X, args.Y);
	_isEnabled = false;
}

//===========================================================================//

bool Widget::IsEnabled() const
{
	return _isEnabled;
}

//===========================================================================//

void Widget::SetVisible(bool flag)
{
	_isVisible = flag;
	for (const SharedPtr<Widget> & child : _children)
	{
		child->SetVisible(flag);
	}
}

//===========================================================================//

const SharedPtr<PositioningNode> & Widget::GetPositioningNode() const
{
	return _positioningNode;
}

//===========================================================================//

const Vector2 & Widget::GetSize() const
{
	return _size;
}

//===========================================================================//

bool Widget::IsVisible()
{
	return _isVisible;
}

//===========================================================================//

void Widget::Show()
{
	SetVisible(true);
}

//===========================================================================//

void Widget::Hide()
{
	SetVisible(false);
}

//===========================================================================//

void Widget::DispatchMouseEvent(const MouseEventArgs & args)
{
	if (!_isEnabled)
	{
		return;
	}

	switch (args.Key)
	{
	case MouseKey::Move:
		if (ContainsPoint(args.X, args.Y))
		{
			OnMouseOver(args);
		}
		else
		{
			if (_isMouseOver)
			{
				OnMouseLeave(args);
			}
		}
		break;

	case MouseKey::Left:
		if (args.State == KeyState::Down)
		{
			if (ContainsPoint(args.X, args.Y))
			{
				OnMouseLeftDown(args);
			}
		}
		else
		{
			OnMouseLeftUp(args);
		}
		break;

	default:
		break;
	}
}

//===========================================================================//

void Widget::AddChild(const SharedPtr<Widget> & child)
{
	if (_children.Contains(child))
	{
		INVALID_ARGUMENT_EXCEPTION("Child already present in children collection");
	}

	_children.Add(child);
	_positioningNode->AddChild(child->_positioningNode);
	child->SetViewOrder(_viewOrder);
}

//===========================================================================//

void Widget::RemoveChild(const SharedPtr<Widget> & child)
{
	if (!_children.Contains(child))
	{
		INVALID_ARGUMENT_EXCEPTION("Child not found in children collection");
	}
	
	_positioningNode->RemoveChild(child->_positioningNode);
	_children.Remove(child);	
}

//===========================================================================//

const SharedPtr<WidgetInputReceiver> & Widget::GetInputReceiver() const
{
	return _inputReceiver;
}

//===========================================================================//

void Widget::Update()
{
}

//===========================================================================//

void Widget::UpdateChildrenViewOrder()
{
	for (const SharedPtr<Widget> & child : _children)
	{
		child->SetViewOrder(_viewOrder);
	}
}

//===========================================================================//

ProjectionMode Widget::GetProjectionMode() const
{
	return _mode;
}

//===========================================================================//

bool Widget::IsMouseOver() const
{
	return _isMouseOver;
}

//===========================================================================//

bool Widget::IsPressed() const
{
	return _isPressed;
}

//===========================================================================//

void Widget::OnClick(const MouseEventArgs & args)
{
	if (IsVisible())
	{
		_clickEvent.Fire(this, args);
	}
}

//===========================================================================//

void Widget::OnMouseOver(const MouseEventArgs & args)
{
	_isMouseOver = true;
	if (_isEnabled)
	{
		_mouseOverEvent.Fire(this, args);
	}
}

//===========================================================================//

void Widget::OnMouseLeave(const MouseEventArgs & args)
{
	_isMouseOver = false;
	_isPressed = false;
	if (_isEnabled)
	{
		_mouseLeaveEvent.Fire(this, args);
	}
}

//===========================================================================//

void Widget::OnMouseLeftUp(const MouseEventArgs & args)
{
	if (!_isEnabled)
	{
		return;		
	}
	
	_mouseLeftUpEvent.Fire(this, args);
	if (_isPressed)
	{
		OnClick(args);
		_isPressed = false;
	}
}

//===========================================================================//

void Widget::OnMouseLeftDown(const MouseEventArgs & args)
{
	_isPressed = true;
	if (_isEnabled)
	{
		_mouseLeftDownEvent.Fire(this, args);
	}
}

//===========================================================================//

void Widget::Initialize(const string & domainName)
{
	_positioningNode = new PositioningNode(domainName);
}

//===========================================================================//

integer Widget::GetTag() const
{
	return _tag;
}

//===========================================================================//

void Widget::SetTag(integer tag)
{
	_tag = tag;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Label.h"

#include "WidgetsManager.h"
#include "../Utils/Log/Log.h"

namespace Bru
{

//===========================================================================//

Label::Label(const string & domainName, const string & layerName) :
	Label(domainName, layerName, string::Empty)
{
}

//===========================================================================//

Label::Label(const string & domainName, const string & layerName, const string & text) :
	Label(domainName, layerName, text, FontManager->GetDefaultFont(), Color::Default, Vector2(), WordWrapMode::Smart,
	      TextHorizontalAlignment::Left, TextVerticalAlignment::Baseline)
{
}

//===========================================================================//

Label::Label(const string & domainName, const string & layerName, const string & text, const WeakPtr<Font> & font,
             const Color & fontColor, Vector2 labelSize, WordWrapMode wrapMode,
             TextHorizontalAlignment textAlignment, TextVerticalAlignment verticalAlignment) :
	Widget(domainName, layerName, labelSize),
	_renderableText()
{
	Vector2 targetSize(GetSize());
	_renderableText = new RenderableText(domainName, layerName, text, font, targetSize, fontColor, wrapMode,
	                                     textAlignment, verticalAlignment);
	_renderableText->SetPositioningNode(GetPositioningNode());

	SetSize(_renderableText->GetWidth(), _renderableText->GetHeight());
}

//===========================================================================//

Label::~Label()
{
}

//===========================================================================//

void Label::SetText(const string & text)
{
	_renderableText->SetText(text);
	SetSize(_renderableText->GetWidth(), _renderableText->GetHeight());
}

//===========================================================================//

void Label::SetText(const string & text, const Color & color)
{
	_renderableText->SetText(text, color);
	SetSize(_renderableText->GetWidth(), _renderableText->GetHeight());	
}

//===========================================================================//

const string & Label::GetText() const
{
	return _renderableText->GetText();
}

//===========================================================================//

WordWrapMode Label::GetWrapMode() const
{
	return _renderableText->GetWrapMode();
}

//===========================================================================//

TextHorizontalAlignment Label::GetHorizontalTextAlignment() const
{
	return _renderableText->GetHorizontalTextAlignment();
}

//===========================================================================//

TextVerticalAlignment Label::GetVerticalTextAlignment() const
{
	return _renderableText->GetVerticalTextAlignment();
}

//===========================================================================//

integer Label::GetLinesCount() const
{
	return _renderableText->GetLinesCount();
}

//===========================================================================//

WeakPtr<Font> Label::GetFont()
{
	return _renderableText->GetFont();
}

//===========================================================================//

integer Label::GetFontSize()
{
	return _renderableText->GetFont()->GetSize();
}

//===========================================================================//

void Label::SetColor(const Color & fontColor)
{
	_renderableText->SetColor(fontColor);
}

//===========================================================================//

const Color & Label::GetColor()
{
	return _renderableText->GetColor();
}

//===========================================================================//

void Label::SetVisible(bool flag)
{
	_renderableText->SetVisible(flag);
	Widget::SetVisible(flag);
}

//===========================================================================//

void Label::SetViewOrder(integer order)
{
	_viewOrder = order;
	_renderableText->SetRenderingOrder(order);
}

//===========================================================================//

float Label::GetCharFullWidth(integer index) const
{
	return _renderableText->GetCharFullWidth(index);
}

//===========================================================================//

} // namespace Bru

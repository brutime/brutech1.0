//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WidgetInputReceiver.h"

#include "Widget.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(WidgetInputReceiver, InputReceiver)

//===========================================================================//

WidgetInputReceiver::WidgetInputReceiver(const string & domainName, ProjectionMode mode,
        Widget * const component) :
	InputReceiver(domainName),
	_mode(mode),
	_component(component),
	_lastMouseEventArgs(MouseKey::Unknown, KeyState::None, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0)
{
}

//===========================================================================//

WidgetInputReceiver::~WidgetInputReceiver()
{
}

//===========================================================================//

const MouseEventArgs & WidgetInputReceiver::GetLastMouseEventArgs() const
{
	return _lastMouseEventArgs;
}

//===========================================================================//

void WidgetInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	_component->DispatchKeyboardEvent(args);
}

//===========================================================================//

void WidgetInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	if (_mode == ProjectionMode::PercentsOrtho)
	{
		Vector2 mouseWorldCoords = Renderer->PixelsToCoords(ProjectionMode::PercentsOrtho, {args.X, args.Y});
		Vector2 displayWorldCoords = Renderer->PixelsToCoords(ProjectionMode::PercentsOrtho, {args.DisplayX, args.DisplayY});

		MouseEventArgs orthoArgs = args;
		orthoArgs.X = mouseWorldCoords.X;
		orthoArgs.Y = mouseWorldCoords.Y;
		orthoArgs.DisplayX = displayWorldCoords.X;
		orthoArgs.DisplayY = displayWorldCoords.Y;

		_component->DispatchMouseEvent(orthoArgs);
		_lastMouseEventArgs = orthoArgs;
	}
	else
	{
		_component->DispatchMouseEvent(args);
		_lastMouseEventArgs = args;
	}
}

//===========================================================================//

void WidgetInputReceiver::DispatchJoystickEvent(const JoystickEventArgs & args)
{
	_component->DispatchJoystickEvent(args);
}

//===========================================================================//

}// namespace Bru

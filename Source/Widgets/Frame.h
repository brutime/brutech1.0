//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef FRAME_H
#define FRAME_H

#include "Widget.h"

namespace Bru
{

//===========================================================================//

class Frame : public Widget
{
public:
	Frame(const string & domainName, const string & layerName, float width, float height, const Color & color);
	Frame(const string & domainName, const string & layerName, integer width, integer height, const Color & color);

	Frame(const string & domainName, const string & layerName, float width, float height, const string & textureName);
	Frame(const string & domainName, const string & layerName, integer width, integer height,
	      const string & textureName);

	virtual ~Frame();

	virtual void SetVisible(bool flag);

	void SetViewOrder(integer order);
	
	//Иногда Frame может понадобится, просто как контейнер
	void ShowBackground();
	void HideBackground();
	bool IsBackgroundVisible();	
	
	const Color & GetColor() const;

protected:
	SharedPtr<Sprite> _sprite;
	bool _isBackgroundVisible;
	
	virtual void Update();

private:
	void Initialize(const string & domainName, const string & layerName, const Color & color);
	void Initialize(const string & domainName, const string & layerName, const string & textureName);

	Frame(const Frame &) = delete;
	Frame & operator =(const Frame &) = delete;
};

//===========================================================================//

}//namespace

#endif // FRAME_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "InputBox.h"

#include "../Graphics/Graphics.h"
#include "../Graphics/Font/NoticeBuilder.h"
#include "../Utils/Console/Console.h"

#include "WidgetsManager.h"

namespace Bru
{

//===========================================================================//

const float InputBox::CursorBlinkingInterval = 0.5f;

//===========================================================================//

InputBox::InputBox(const string & domainName, const string & layerName, float x, float y,
                   InputCursorOrientation cursorOrientation, const WeakPtr<Font> & font, const Color & color) :
	Widget(domainName, layerName),
	inputSubmittedEvent(),
	InputSubmitted(inputSubmittedEvent),
	_projectionMode(Renderer->GetLayer(domainName, layerName)->GetProjectionMode()),
	_font(font),
	_color(color),
	_label(),
	_maxLength(0),
	_cursorSceneNode(),
	_cursor(),
	_cursorOrientation(cursorOrientation),
	_cursorIndex(0),
	_blinkController()
{
	_label = new Label(domainName, layerName, string::Empty, _font, _color);
	SetSize(_label->GetSize());
	AddChild(_label);

	_cursorSceneNode = new PositioningNode(domainName);

	Vector2 cursorSize;
	if (_cursorOrientation == InputCursorOrientation::Vertical)
	{
		cursorSize = Renderer->PixelsToCoords(ProjectionMode::PercentsOrtho, {2.0f, _font->GetHeight()});
	}
	else
	{
		float underlineWidth = _font->GetGlyph("_")->Width;
		cursorSize = Renderer->PixelsToCoords(ProjectionMode::PercentsOrtho, {underlineWidth, 2.0f});
	}

	_cursor = new Sprite(domainName, layerName, {cursorSize.X, cursorSize.Y}, _color);
	_cursor->SetPositioningNode(_cursorSceneNode);

	_label->GetPositioningNode()->AddChild(_cursorSceneNode);

	UpdateCursorPosition();

	_blinkController = new VisibilitySwitchController(_cursor, CursorBlinkingInterval);

	MoveTo(x, y);
}

//===========================================================================//

InputBox::~InputBox()
{
}

//===========================================================================//

void InputBox::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	switch (args.State)
	{
	case KeyState::Down:
	case KeyState::RepeatedDown:
		OnKeyDown(args.Key);
		break;

	case KeyState::Up:
		OnKeyUp(args.Key);
		break;

	default:
		break;
	}
}

//===========================================================================//

void InputBox::DispatchMouseEvent(const MouseEventArgs &)
{
}

//===========================================================================//

void InputBox::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

void InputBox::SetText(const string & text)
{
	_label->SetText(text);
	_cursorIndex = text.GetLength();
	UpdateCursorPosition();
	SetSize(_label->GetSize());
}

//===========================================================================//

void InputBox::AddText(const string & text)
{
	InsertText(_label->GetText().GetLength(), text);
}

//===========================================================================//

void InputBox::InsertText(integer index, const string & text)
{
	_label->SetText(_label->GetText().Insert(index, text));
	_cursorIndex = index + text.GetLength();
	UpdateCursorPosition();
	SetSize(_label->GetSize());
}

//===========================================================================//

void InputBox::RemoveText(integer index, integer count)
{
	_label->SetText(_label->GetText().Remove(index, count));
	_cursorIndex = Math::Max(0, _cursorIndex - count);
	UpdateCursorPosition();
	SetSize(_label->GetSize());
}

//===========================================================================//

string InputBox::GetText() const
{
	return _label->GetText();
}

//===========================================================================//

void InputBox::Clear()
{
	_label->SetText(string::Empty);
	_cursorIndex = 0;
	UpdateCursorPosition();
}

//===========================================================================//

WeakPtr<Font> InputBox::GetFont()
{
	return _label->GetFont();
}

//===========================================================================//

void InputBox::SetMaxLength(integer maxLength)
{
	if (maxLength < 0)
	{
		return;
	}

	_maxLength = maxLength;

	const string & labelText = _label->GetText();
	if (_maxLength < labelText.GetLength())
	{
		_label->SetText(labelText.Substring(0, _maxLength));
	}
}

//===========================================================================//

void InputBox::SetVisible(bool flag)
{
	Widget::SetVisible(flag);
	_label->SetVisible(flag);
	_cursor->SetVisible(flag);
	flag ? _blinkController->StartSwitching() : _blinkController->StopSwitching(false);
}

//===========================================================================//

void InputBox::SetViewOrder(integer order)
{
	_viewOrder = order;
	_cursor->SetRenderingOrder(order);
	_label->SetViewOrder(order);
}

//===========================================================================//

void InputBox::OnKeyDown(KeyboardKey key)
{
	const string & text = GetText();
	if (IsNeedToAddChar(key))
	{
		InsertText(_cursorIndex, Keyboard->GetCharForKey(key));
	}
	else
	{
		switch (key)
		{
		case KeyboardKey::Left:
			_cursorIndex = Math::Max(0, _cursorIndex - 1);
			_blinkController->StopSwitching(true);
			break;

		case KeyboardKey::Right:
			_cursorIndex = Math::Min(text.GetLength(), _cursorIndex + 1);
			_blinkController->StopSwitching(true);
			break;

		case KeyboardKey::Home:
			_cursorIndex = 0;
			_blinkController->StopSwitching(true);
			break;

		case KeyboardKey::End:
			_cursorIndex = text.GetLength();
			_blinkController->StopSwitching(true);
			break;

		case KeyboardKey::Backspace:
			if (text.GetLength() > 0 && _cursorIndex > 0)
			{
				_label->SetText(text.Remove(_cursorIndex - 1));
				_cursorIndex--;
			}
			break;

		case KeyboardKey::Delete:
			if (text.GetLength() > 0 && _cursorIndex < text.GetLength())
			{
				_label->SetText(text.Remove(_cursorIndex));
			}
			break;

		case KeyboardKey::C:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				OperatingSystem->PutTextToClipboard(text);
			}
			break;
			
		case KeyboardKey::V:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				InsertText(_cursorIndex, OperatingSystem->GetTextFromClipboard());
			}
			break;
			
		case KeyboardKey::X:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				OperatingSystem->PutTextToClipboard(text);
				SetText(string::Empty);
			}
			break;			

		case KeyboardKey::Enter:
			inputSubmittedEvent.Fire(text);
			break;

		default:
			break;
		}

		_blinkController->StartSwitching();
		UpdateCursorPosition();
	}
}

//===========================================================================//

void InputBox::OnKeyUp(KeyboardKey)
{
}

//===========================================================================//

void InputBox::UpdateCursorPosition()
{
	Vector2 factors = Renderer->GetPixelsToCoordsFactors(_projectionMode);

	Vector3 cursorPosition;
	for (integer i = 0; i < _cursorIndex; ++i)
	{
		cursorPosition.X += _label->GetCharFullWidth(i);
	}

	if (_cursorOrientation == InputCursorOrientation::Vertical)
	{
		cursorPosition.Y = _label->GetPosition().Y + _font->GetBaseLineDescender() * factors.Y;
	}
	else
	{
		cursorPosition.Y = -_cursor->GetHeight();
	}
	_cursorSceneNode->SetPosition(cursorPosition);
}

//===========================================================================//

bool InputBox::IsNeedToAddChar(KeyboardKey key) const
{
	bool isSymbol = Keyboard->IsKeyPrintableCharacter(key) || key == KeyboardKey::Space;
	bool isCombination = isSymbol && (Keyboard->IsKeyDown(KeyboardKey::Control) || Keyboard->IsKeyDown(KeyboardKey::Alt));
	bool isClimbs = _maxLength == 0 || _label->GetText().GetLength() < _maxLength;	
	return isSymbol && !isCombination && isClimbs;
}

//===========================================================================//

integer InputBox::GetCursorIndex() const
{
	return _cursorIndex;
}

//===========================================================================//

} // namespace Bru

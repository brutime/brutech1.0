//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Cursor.h"

#include "../Utils/ParametersFile/ParametersFile.h"
#include "../Utils/PathHelper/PathHelper.h"
#include "../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

const string Cursor::PathToPathToSprite = "PathToSprite";
const string Cursor::PathToOrigin = "Origin";

//===========================================================================//

Cursor::Cursor(const string & domainName, const string & layerName, const string & pathToFile) :
	_sprite(),
	_origin(CursorOrigin::UpperLeft),
	_isVisible(true)
{
	ParametersFile file(PathHelper::PathToCursors + pathToFile);

	string pathToSprite = file.Get<string>(PathToPathToSprite);
	_origin = GetCursorOriginByIntegerValue(file.Get<integer>(PathToOrigin));

	file.Close();

	_sprite = new Sprite(domainName, layerName, pathToSprite);
	_sprite->SetOriginFactor(CoordsForOriginFactor(_origin));

}

//===========================================================================//

Cursor::~Cursor()
{
}

//===========================================================================//

void Cursor::SetVisible(bool flag)
{
	_sprite->SetVisible(flag);
}

//===========================================================================//

Vector3 Cursor::CoordsForOriginFactor(CursorOrigin origin)
{
	switch (origin)
	{
	case CursorOrigin::UpperLeft:
		return Vector3(0.0f, 1.0f);

	case CursorOrigin::UpperCenter:
		return Vector3(0.5f, 1.0f);

	case CursorOrigin::UpperRight:
		return Vector3(1.0f, 1.0f);

	case CursorOrigin::MiddleLeft:
		return Vector3(0.0f, 0.5f);

	case CursorOrigin::Center:
		return Vector3(0.5f, 0.5f);

	case CursorOrigin::MiddleRight:
		return Vector3(1.0f, 0.5f);

	case CursorOrigin::LowerLeft:
		return Vector3(0.0f, 0.0f);

	case CursorOrigin::LowerCenter:
		return Vector3(0.5f, 0.0f);

	case CursorOrigin::LowerRight:
		return Vector3(1.0f, 0.0f);

	case CursorOrigin::Count:
	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("Unknown cursor origin {0}", static_cast<integer>(origin)));
	}
}

//===========================================================================//

CursorOrigin Cursor::GetCursorOriginByIntegerValue(integer value)
{
	if (value < 0 || value >= static_cast<integer>(CursorOrigin::Count))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Unknown value {0}", value));
	}

	return static_cast<CursorOrigin>(value);
}

//===========================================================================//

}// namespace

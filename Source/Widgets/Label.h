//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#ifndef LABEL_H
#define LABEL_H

#include "Widget.h"
#include "../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class Label : public Widget
{
public:
	Label(const string & domainName, const string & layerName);
	Label(const string & domainName, const string & layerName, const string & text);
	Label(const string & domainName, const string & layerName, const string & text, const WeakPtr<Font> & font,
	      const Color & fontColor = Color::Default, Vector2 labelSize = Vector2(0.0f, 0.0f),
	      WordWrapMode wrapMode = WordWrapMode::Simple, TextHorizontalAlignment textAlignment = TextHorizontalAlignment::Left,
	      TextVerticalAlignment verticalAlignment = TextVerticalAlignment::Baseline);

	virtual ~Label();

	void SetText(const string & text);
	void SetText(const string & text, const Color & color);
	const string & GetText() const;

	WordWrapMode GetWrapMode() const;

	TextHorizontalAlignment GetHorizontalTextAlignment() const;
	TextVerticalAlignment GetVerticalTextAlignment() const;

	virtual integer GetLinesCount() const;

	virtual WeakPtr<Font> GetFont();
	virtual integer GetFontSize();

	virtual void SetColor(const Color & newColor);
	virtual const Color & GetColor();

	virtual void SetVisible(bool flag);

	void SetViewOrder(integer order);
	
	//Ширина + расстояние до следующего символа
	float GetCharFullWidth(integer index) const;	

private:
	SharedPtr<RenderableText> _renderableText;

	Label(const Label &) = delete;
	Label & operator =(const Label &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // LABEL_H

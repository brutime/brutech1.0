//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Button.h"

#include "../Graphics/Graphics.h"

#include "WidgetsManager.h"

namespace Bru
{

//===========================================================================//

Button::Button(const string & domainName, const string & layerName, float width, float height,
               const string & normalTextureName, const string & pressedTextureName, const string & hoveredTextureName,
               const Color & normalColor, const Color & pressedColor, const Color & hoveredColor) :
	Widget(domainName, layerName, {width, height}),
       _sprite(),
       _normalTexture(),
       _pressedTexture(),
       _hoveredTexture(),
       _normalColor(normalColor),
       _pressedColor(pressedColor),
       _hoveredColor(hoveredColor)
{
	Initialize(domainName, layerName, normalTextureName, pressedTextureName, hoveredTextureName);
}

//===========================================================================//

Button::Button(const string & domainName, const string & layerName, integer width, integer height,
               const string & normalTextureName, const string & pressedTextureName, const string & hoveredTextureName,
               const Color & normalColor, const Color & pressedColor, const Color & hoveredColor) :
	Widget(domainName, layerName),
	_sprite(),
	_normalTexture(),
	_pressedTexture(),
	_hoveredTexture(),
	_normalColor(normalColor),
	_pressedColor(pressedColor),
	_hoveredColor(hoveredColor)
{
	SetSize(Renderer->PixelsToCoords(GetProjectionMode(), Vector2(width, height)));
	Initialize(domainName, layerName, normalTextureName, pressedTextureName, hoveredTextureName);
}

//===========================================================================//

Button::~Button()
{
}

//===========================================================================//

void Button::SetVisible(bool flag)
{
	Widget::SetVisible(flag);
	_sprite->SetVisible(flag);
}

//===========================================================================//

void Button::SetViewOrder(integer order)
{
	_viewOrder = order;
	_sprite->SetRenderingOrder(order);
}

//===========================================================================//

void Button::SetNormalTextureName(const string & normalTextureName)
{
	_normalTexture = !normalTextureName.IsEmpty() ? TextureManager->Get(normalTextureName) : NullPtr;

	if (!IsMouseOver() && !IsPressed())
	{
		_sprite->SetTexture(_normalTexture);
	}
}

//===========================================================================//

string Button::GetNormalTextureName() const
{
	return _normalTexture != NullPtr ? _normalTexture->GetName() : string::Empty;
}

//===========================================================================//

void Button::SetPressedTextureName(const string & pressedTextureName)
{
	_pressedTexture = !pressedTextureName.IsEmpty() ? TextureManager->Get(pressedTextureName) : NullPtr;

	if (IsPressed())
	{
		_sprite->SetTexture(_pressedTexture);
	}
}

//===========================================================================//

string Button::GetPressedTextureName() const
{
	return _pressedTexture != NullPtr ? _pressedTexture->GetName() : string::Empty;
}

//===========================================================================//

void Button::SetHoveredTextureName(const string & hoveredTextureName)
{
	_hoveredTexture = !hoveredTextureName.IsEmpty() ? TextureManager->Get(hoveredTextureName) : NullPtr;

	if (IsMouseOver())
	{
		_sprite->SetTexture(_hoveredTexture);
	}
}

//===========================================================================//

string Button::GetHoveredTextureName() const
{
	return _hoveredTexture != NullPtr ? _hoveredTexture->GetName() : string::Empty;
}

//===========================================================================//

void Button::SetNormalColor(const Color & normalColor)
{
	_normalColor = normalColor;	
	
	if (!IsMouseOver() && !IsPressed())
	{
		_sprite->SetColor(_normalColor);
	}	
}

//===========================================================================//

const Color & Button::GetNormalColor() const
{
	return _normalColor;
}

//===========================================================================//

void Button::SetPressedColor(const Color & pressedColor)
{
	_pressedColor = pressedColor;	
	
	if (IsPressed())
	{
		_sprite->SetColor(_pressedColor);
	}
}	

//===========================================================================//

const Color & Button::GetPressedColor() const
{
	return _pressedColor;
}

//===========================================================================//

void Button::SetHoveredColor(const Color & hoveredColor)
{
	_hoveredColor = hoveredColor;
	
	if (IsMouseOver())
	{
		_sprite->SetColor(_hoveredColor);
	}		
}

//===========================================================================//

const Color & Button::GetHoveredColor() const
{
	return _hoveredColor;
}

//===========================================================================//

void Button::OnMouseLeave(const MouseEventArgs & args)
{
	Widget::OnMouseLeave(args);
	_sprite->SetTexture(_normalTexture);
	_sprite->SetColor(_normalColor);
}

//===========================================================================//

void Button::OnMouseOver(const MouseEventArgs & args)
{
	Widget::OnMouseOver(args);
	if (IsPressed())
	{
		_sprite->SetTexture(_pressedTexture);
		_sprite->SetColor(_pressedColor);
	}
	else
	{
		_sprite->SetTexture(_hoveredTexture);
		_sprite->SetColor(_hoveredColor);
	}
}

//===========================================================================//

void Button::OnMouseLeftDown(const MouseEventArgs & args)
{
	Widget::OnMouseLeftDown(args);
	_sprite->SetTexture(_pressedTexture);
	_sprite->SetColor(_pressedColor);
}

//===========================================================================//

void Button::OnMouseLeftUp(const MouseEventArgs & args)
{
	if (IsMouseOver())
	{
		_sprite->SetTexture(_hoveredTexture);
		_sprite->SetColor(_hoveredColor);
	}
	else
	{
		_sprite->SetTexture(_normalTexture);
		_sprite->SetColor(_normalColor);
	}

	Widget::OnMouseLeftUp(args);
}

//===========================================================================//

void Button::Initialize(const string & domainName, const string & layerName, const string & normalTextureName,
                        const string & pressedTextureName, const string & hoveredTextureName)
{
	if (!normalTextureName.IsEmpty())
	{
		_normalTexture = TextureManager->Get(normalTextureName);
	}
	if (!pressedTextureName.IsEmpty())
	{
		_pressedTexture = TextureManager->Get(pressedTextureName);
	}
	if (!hoveredTextureName.IsEmpty())
	{
		_hoveredTexture = TextureManager->Get(hoveredTextureName);
	}

	if (!normalTextureName.IsEmpty())
	{
		_sprite = new Sprite(domainName, layerName, {GetSize().X, GetSize().Y}, _normalTexture);
		_sprite->SetColor(_normalColor);
	}
	else
	{
		_sprite = new Sprite(domainName, layerName, {GetSize().X, GetSize().Y}, _normalColor);
	}
	_sprite->SetPositioningNode(GetPositioningNode());
}

//===========================================================================//

void Button::Enable()
{
	_sprite->SetTexture(_normalTexture);
	_sprite->SetColor(_normalColor);
	Widget::Enable();
}

//===========================================================================//

void Button::Disable()
{
	_sprite->SetTexture(_normalTexture);
	_sprite->SetColor({_normalColor, 0.5f});
	Widget::Disable();
}

//===========================================================================//

}// namespace

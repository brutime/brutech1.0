//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "InputPrompt.h"

namespace Bru
{

//===========================================================================//

InputPrompt::InputPrompt(const string & domainName, const string & layerName, const string & promptText, float x,
                         float y, InputCursorOrientation cursorOrientation, const WeakPtr<Font> & fontInfo, const Color & color) :
	InputBox(domainName, layerName, x, y, cursorOrientation, fontInfo, color),
	_promtLabel(),
	_commandHistory(),
	_commandHistoryIndex(0)
{
	_promtLabel = new Label(domainName, layerName, promptText, _font, _color);
	AddChild(_promtLabel);
	_label->MoveTo(_promtLabel->GetPosition().X + _promtLabel->GetSize().X, _label->GetPosition().Y);
	UpdateSize();
}

//===========================================================================//

InputPrompt::~InputPrompt()
{
}

//===========================================================================//

void InputPrompt::SetText(const string & text)
{
	InputBox::SetText(text);
	UpdateSize();
}

//===========================================================================//

void InputPrompt::AddText(const string & text)
{
	InputBox::AddText(text);
	UpdateSize();
}

//===========================================================================//

void InputPrompt::InsertText(integer index, const string & text)
{
	InputBox::InsertText(index, text);
	UpdateSize();
}

//===========================================================================//

void InputPrompt::RemoveText(integer index, integer count)
{
	InputBox::RemoveText(index, count);
	UpdateSize();	
}

//===========================================================================//

void InputPrompt::SetVisible(bool flag)
{
	InputBox::SetVisible(flag);
	_promtLabel->SetVisible(flag);
}

//===========================================================================//

void InputPrompt::SetViewOrder(integer order)
{
	InputBox::SetViewOrder(order);
	_promtLabel->SetViewOrder(order);
}

//===========================================================================//

string InputPrompt::GetWord(integer wordIndex) const
{
	return TokenParser(GetText()).GetToken(wordIndex);
}
	
//===========================================================================//	
	
string InputPrompt::GetWordAtCursor() const
{
	const string & text = GetText();
	if (text.IsEmpty())
	{
		return string::Empty;
	}
	
	TokenParser parser(text);
	Nullable<string> word = parser.GetTokenAt(GetCharIndex());
	return word.HasValue() ? word.GetValue() : string::Empty;
}
	
//===========================================================================//	
	
integer InputPrompt::GetWordIndexAtCursor() const
{
	const string & text = GetText();
	if (text.IsEmpty())
	{
		return -1;
	}
	
	TokenParser parser(text);
	return parser.GetTokenIndexAt(GetCharIndex());
}

//===========================================================================//
	
void InputPrompt::ReplaceWord(integer wordIndex, const string & word)
{	
	const string & text = GetText();
	TokenParser parser(text);
	
	const string & currentWord = parser.GetToken(wordIndex);
	integer wordIndexAtText = parser.GetTokenInfo(wordIndex)->TokenStart;
	RemoveText(wordIndexAtText, currentWord.GetLength());
	InsertText(wordIndexAtText, word);
}

//===========================================================================//

integer InputPrompt::GetWordsCount() const
{
	TokenParser parser(GetText());
	return parser.GetTokensCount();
}

//===========================================================================//

void InputPrompt::OnKeyDown(KeyboardKey key)
{
	if (!IsNeedToAddChar(key))
	{
		const string & text = GetText();
		switch (key)
		{
		case KeyboardKey::Enter:
			if (!text.IsEmptyOrBlankCharsOnly())
			{
				_commandHistory.Add(text);
				_commandHistoryIndex = _commandHistory.GetCount() - 1;
			}
			break;

		case KeyboardKey::Up:
			PreviousHistoryCommand();
			break;

		case KeyboardKey::Down:
			NextHistoryCommand();
			break;

		default:
			break;
		}
	}

	InputBox::OnKeyDown(key);
}

//===========================================================================//

void InputPrompt::OnKeyUp(KeyboardKey)
{
}

//===========================================================================//

void InputPrompt::PreviousHistoryCommand()
{
	if (_commandHistory.IsEmpty())
	{
		return;
	}

	SetText(_commandHistory[_commandHistoryIndex]);

	if (_commandHistoryIndex > 0)
	{
		_commandHistoryIndex--;
	}
}

//===========================================================================//

void InputPrompt::NextHistoryCommand()
{
	if (_commandHistory.IsEmpty() || _commandHistoryIndex >= _commandHistory.GetCount() - 1)
	{
		return;
	}

	_commandHistoryIndex++;
	SetText(_commandHistory[_commandHistoryIndex]);
}

//===========================================================================//

void InputPrompt::UpdateSize()
{
	SetSize(_promtLabel->GetSize().X + _label->GetSize().X, _label->GetSize().Y);
}

//===========================================================================//

integer InputPrompt::GetCharIndex() const
{
	const string & text = GetText();
	integer index = GetCursorIndex();
	if (index == text.GetLength() || text[index] == " ")
	{
		index = Math::Max(index - 1, 0);
	}
	
	return index;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Линия в текстовой панели
//

#ifndef TEXT_PANEL_LINE_H
#define TEXT_PANEL_LINE_H

#include "../Math/Math.h"

namespace Bru
{

//===========================================================================//

struct TextPanelLine
{
	TextPanelLine();
	TextPanelLine(const string & text, const Color & fontColor);
	
	~TextPanelLine();
	
	string Text;
	Color FontColor;
};

//===========================================================================//

} // namespace Bru

#endif // TEXT_PANEL_LINE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Frame.h"

#include "../Graphics/Graphics.h"
#include "../Utils/Log/Log.h"
#include "WidgetsManager.h"

namespace Bru
{

//===========================================================================//

Frame::Frame(const string & domainName, const string & layerName, float width, float height, const Color & color) :
	Widget(domainName, layerName, { width, height }),
	_sprite(),
	_isBackgroundVisible(true)
{
	Initialize(domainName, layerName, color);
}

//===========================================================================//

Frame::Frame(const string & domainName, const string & layerName, integer width, integer height, const Color & color) :
	Widget(domainName, layerName),
	_sprite(),
	_isBackgroundVisible(true)
{
	SetSize(Renderer->PixelsToCoords(GetProjectionMode(), (Vector2(width, height))));
	Initialize(domainName, layerName, color);
}

//===========================================================================//

Frame::Frame(const string & domainName, const string & layerName, float width, float height, const string & textureName) :
	Widget(domainName, layerName, { width, height }),
	_sprite(),
	_isBackgroundVisible(true)
{
	Initialize(domainName, layerName, textureName);
}

//===========================================================================//

Frame::Frame(const string & domainName, const string & layerName, integer width, integer height,
             const string & textureName) :
	Widget(domainName, layerName),
	_sprite(),
	_isBackgroundVisible(true)
{
	SetSize(Renderer->PixelsToCoords(ProjectionMode::PercentsOrtho, Vector2(width, height)));
	Initialize(domainName, layerName, textureName);
}

//===========================================================================//

Frame::~Frame()
{
}

//===========================================================================//

void Frame::SetVisible(bool flag)
{
	if (_isBackgroundVisible)
	{
		_sprite->SetVisible(flag);	
	}
	Widget::SetVisible(flag);
}

//===========================================================================//

void Frame::SetViewOrder(integer order)
{
	_viewOrder = order;
	_sprite->SetRenderingOrder(order);
	UpdateChildrenViewOrder();
}

//===========================================================================//

void Frame::ShowBackground()
{
	_sprite->Show();
	_isBackgroundVisible = true;
}

//===========================================================================//

void Frame::HideBackground()
{
	_sprite->Hide();
	_isBackgroundVisible = false;
}

//===========================================================================//

bool Frame::IsBackgroundVisible()
{
	return _isBackgroundVisible;
}

//===========================================================================//

const Color & Frame::GetColor() const
{
	return _sprite->GetColor();
}

//===========================================================================//

void Frame::Update()
{
	if (_sprite != NullPtr)
	{
		_sprite->SetSize(GetSize().X, GetSize().Y);
	}
}

//===========================================================================//

void Frame::Initialize(const string & domainName, const string & layerName, const Color & color)
{
	_sprite = new Sprite(domainName, layerName, {GetSize().X, GetSize().Y}, color);
	_sprite->SetPositioningNode(GetPositioningNode());
}

//===========================================================================//

void Frame::Initialize(const string & domainName, const string & layerName, const string & textureName)
{
	_sprite = new Sprite(domainName, layerName, {GetSize().X, GetSize().Y}, TextureManager->Get(textureName));
	_sprite->SetPositioningNode(GetPositioningNode());
}

//===========================================================================//

} // namespace Bru

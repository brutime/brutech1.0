//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: строка для ввода текста
//

#ifndef INPUT_BOX_H
#define INPUT_BOX_H

#include "Widget.h"
#include "Label.h"
#include "Enums/InputCursorOrientation.h"
#include "../Common/EventSystem/Event.h"
#include "../Renderer/Renderer.h"

namespace Bru
{

//===========================================================================//

class InputBox : public Widget
{
public:
	InputBox(const string & domainName, const string & layerName, float x, float y,
	         InputCursorOrientation cursorOrientation, const WeakPtr<Font> & font = FontManager->GetDefaultFont(),
	         const Color & color = Color::Default);
	virtual ~InputBox();

	void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	void DispatchMouseEvent(const MouseEventArgs & args);
	void DispatchJoystickEvent(const JoystickEventArgs & args);

	virtual void SetText(const string & text);
	virtual void AddText(const string & text);
	virtual void InsertText(integer index, const string & text);
	virtual void RemoveText(integer index, integer count = 1);
	virtual string GetText() const;
	virtual void Clear();

	virtual WeakPtr<Font> GetFont();

	virtual void SetMaxLength(integer maxLength);

	virtual void SetVisible(bool flag);

	void SetViewOrder(integer order);

// внутренние события должны быть инициализированы раньше публичных
protected:
	InnerEvent<const string &> inputSubmittedEvent;

public:
	Event<const string &> InputSubmitted;

protected:
	virtual void OnKeyDown(KeyboardKey key);
	virtual void OnKeyUp(KeyboardKey);
	virtual void UpdateCursorPosition();
	virtual bool IsNeedToAddChar(KeyboardKey key) const;
	integer GetCursorIndex() const;
	
	ProjectionMode _projectionMode;
	WeakPtr<Font> _font;
	Color _color;	

	SharedPtr<Label> _label;
	integer _maxLength;	

private:
	static const float CursorBlinkingInterval;
	
	SharedPtr<PositioningNode> _cursorSceneNode;
	SharedPtr<Sprite> _cursor;
	InputCursorOrientation _cursorOrientation;
	integer _cursorIndex;
	SharedPtr<VisibilitySwitchController> _blinkController;

	InputBox(const InputBox &) = delete;
	InputBox & operator =(const InputBox &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // INPUT_BOX_H

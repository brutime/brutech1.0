//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef WIDGET_H
#define WIDGET_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "../EntitySystem/EntitySystem.h"
#include "../Renderer/Renderer.h"
#include "../Positioning/Positioning.h"
#include "WidgetInputReceiver.h"

namespace Bru
{

//===========================================================================//

class Widget : public PtrFromThisMixIn<Widget>
{
public:
	virtual ~Widget();

	virtual void Move(float x, float y);
	virtual void Move(integer x, integer y);
	virtual void MoveTo(float x, float y);
	virtual void MoveTo(integer x, integer y);

	const SharedPtr<PositioningNode> & GetPositioningNode() const;

	Vector2 GetPosition() const;
	const Vector2 & GetSize() const;

	virtual void SetSize(Vector2 size);
	virtual void SetSize(float width, float height);
	virtual void SetSize(integer width, integer height);

	bool ContainsPoint(float x, float y, bool translateCoordsToLocal = false) const;
	
	virtual void Enable();
	virtual void Disable();
	bool IsEnabled() const;	

	virtual void SetVisible(bool flag);
	bool IsVisible();

	void Show();
	void Hide();

	void AddChild(const SharedPtr<Widget> & child);
	void RemoveChild(const SharedPtr<Widget> & child);

	virtual void SetViewOrder(integer order) = 0;

	integer GetTag() const;
	void SetTag(integer tag);
	
	virtual void DispatchKeyboardEvent(const KeyboardEventArgs &) {}
	virtual void DispatchMouseEvent(const MouseEventArgs &);
	virtual void DispatchJoystickEvent(const JoystickEventArgs &) {}	

	const SharedPtr<WidgetInputReceiver> & GetInputReceiver() const;

protected:
	Widget(const string & domainName, const string & layerName);
	Widget(const string & domainName, const string & layerName, const Vector2 & size);

	virtual void Update();
	void UpdateChildrenViewOrder();

	ProjectionMode GetProjectionMode() const;

	bool IsMouseOver() const;
	bool IsPressed() const;

	virtual void OnClick(const MouseEventArgs & args);
	virtual void OnMouseOver(const MouseEventArgs & args);
	virtual void OnMouseLeave(const MouseEventArgs & args);
	virtual void OnMouseLeftDown(const MouseEventArgs & args);
	virtual void OnMouseLeftUp(const MouseEventArgs & args);

	integer _viewOrder;
	const string _domainName;
	const string _layerName;

private:
	void Initialize(const string & domainName);

	Vector2 _size;
	bool _isEnabled;
	bool _isVisible;
	bool _isMouseOver;
	bool _isPressed;
	ProjectionMode _mode;
	SharedPtr<PositioningNode> _positioningNode;
	List<SharedPtr<Widget>> _children;
	SharedPtr<WidgetInputReceiver> _inputReceiver;
	integer _tag;

protected:
	InnerEvent<const Widget *, const MouseEventArgs &> _clickEvent;
	InnerEvent<const Widget *, const MouseEventArgs &> _mouseOverEvent;
	InnerEvent<const Widget *, const MouseEventArgs &> _mouseLeaveEvent;
	InnerEvent<const Widget *, const MouseEventArgs &> _mouseLeftDownEvent;
	InnerEvent<const Widget *, const MouseEventArgs &> _mouseLeftUpEvent;

public:
	Event<const Widget *, const MouseEventArgs &> Click;
	Event<const Widget *, const MouseEventArgs &> MouseOver;
	Event<const Widget *, const MouseEventArgs &> MouseLeave;
	Event<const Widget *, const MouseEventArgs &> MouseLeftDown;
	Event<const Widget *, const MouseEventArgs &> MouseLeftUp;

private:
	Widget(const Widget &) = delete;
	Widget & operator = (const Widget &) = delete;
};

//===========================================================================//

}// namespace

#endif // WIDGET_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SOUND_SYSTEM_H
#define SOUND_SYSTEM_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "Sound.h"
#include "SoundEmitter.h"

#include <thread>
#include <mutex>
#include <atomic>

namespace Bru
{

//===========================================================================//

class SoundSystemSingleton
{
public:
	SoundSystemSingleton();
	~SoundSystemSingleton();
	
	SharedPtr<Sound> CreateSound(const string & id) const;

	void RegisterPlayingSound(Sound * sound);
	void UnregisterPlayingSound(Sound * sound);
	
	ALuint GetFreeSource() const;
	void ReturnSourceToPool(ALuint sourceId);
	
private:
	static const string Name;
	static const float UpdatePeriod;

	void StartWorker();
	void StopWorker();
	
	void DoWork();
	void UpdateStreamingSounds();
	void CheckIfSoundsStopped();
	
	void InitializeSourcesPool();
	void DestroySourcesPool();
	
	integer _maxSourceCount;
	ALuint * _sources;
	bool * _sourceAvailable;
	List<Sound *> _nonLoopingSounds;
	List<Sound *> _streamingSounds;
	float _updateTimeout;
	std::thread _worker;
	mutable std::recursive_mutex _mutex;
	std::recursive_mutex _streamingSoundsMutex;
	std::recursive_mutex _nonLoopingSoundsMutex;
	//mutable std::mutex _sourcesMutex;
	std::atomic<bool> _terminateWorker;

private:
	SoundSystemSingleton(const SoundSystemSingleton &) = delete;
	SoundSystemSingleton & operator =(const SoundSystemSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<SoundSystemSingleton> SoundSystem;

//===========================================================================//

} // namespace Bru

#endif // SOUND_SYSTEM_H

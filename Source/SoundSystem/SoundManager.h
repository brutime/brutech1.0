//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include "../Common/Common.h"
#include "Sound.h"

namespace Bru
{

//===========================================================================//

class SoundManagerSingleton
{
public:
	SoundManagerSingleton();
	~SoundManagerSingleton();

	WeakPtr<SoundInfo> GetSoundInfo(const string & id);

	void FreeResources();

private:
	TreeMap<string, SharedPtr<SoundInfo>> _infos;

private:
	SoundManagerSingleton(const SoundManagerSingleton &) = delete;
	SoundManagerSingleton & operator =(const SoundManagerSingleton &) = delete;
};

//===========================================================================//
extern SharedPtr<SoundManagerSingleton> SoundManager;
//===========================================================================//

} // namespace Bru

#endif // SOUND_MANAGER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SoundInfo.h"

#include "../Utils/PathHelper/PathHelper.h"
//#include "../FileSystem/FileSystem.h"

namespace Bru
{

//===========================================================================//

const string SoundInfo::IsLoopedKey = "IsLooped";
const string SoundInfo::UseBufferingKey = "UseBuffering";
const string SoundInfo::PitchKey = "Pitch";
const string SoundInfo::GainKey = "Gain";
const string SoundInfo::BufferSizeKey = "BufferSize";
const integer SoundInfo::DefaultBufferSize = 4096 * 4;

//===========================================================================//

SoundInfo::SoundInfo():
	_pitch(1.0f),
	_gain(1.0f),
	_isLooped(false),
	_useBuffering(false),
	_bufferSize(DefaultBufferSize),
	_fileName()
{
}

//===========================================================================//

SoundInfo::~SoundInfo()
{
}

//===========================================================================//

void SoundInfo::LoadFromFile(const WeakPtr<ParametersFile> & parameters)
{
	_fileName = PathHelper::GetPathWithoutExtension(parameters->GetPathToFile());
	if (FileSystem::FileExists(_fileName + "." + PathHelper::OGGExtension))
	{
		_fileName += string(".") + PathHelper::OGGExtension;
	}
	else
	{
		if (FileSystem::FileExists(_fileName + "." + PathHelper::WAVExtension))
		{
			_fileName += string(".") + PathHelper::WAVExtension;
		}
		else
		{
			FILE_NOT_FOUND_EXCEPTION(string::Format("No matching sound file found for {0}", _fileName));
		}
	}

	_pitch = parameters->Get<float>(PitchKey);
	_gain = parameters->Get<float>(GainKey);
	_isLooped = parameters->Get<bool>(IsLoopedKey);
	_gain = parameters->Get<float>(GainKey);
	_useBuffering = parameters->Get<bool>(UseBufferingKey);
	if (_useBuffering && parameters->ContainsParameter(BufferSizeKey))
	{
		_bufferSize = parameters->Get<integer>(BufferSizeKey);
	}
}

//===========================================================================//

void SoundInfo::SetBufferSize(const integer & bufferSize)
{
	_bufferSize = bufferSize;
}

//===========================================================================//

void SoundInfo::SetGain(const ALfloat & gain)
{
	_gain = gain;
}

//===========================================================================//

void SoundInfo::SetIsLooped(bool isLooped)
{
	_isLooped = isLooped;
}

//===========================================================================//

void SoundInfo::SetPitch(const ALfloat & pitch)
{
	_pitch = pitch;
}

//===========================================================================//

void SoundInfo::SetUseBuffering(bool useBuffering)
{
	_useBuffering = useBuffering;
}

//===========================================================================//

integer SoundInfo::GetBufferSize() const
{
	return _bufferSize;
}

//===========================================================================//

ALfloat SoundInfo::GetGain() const
{
	return _gain;
}

//===========================================================================//

bool SoundInfo::IsLooped() const
{
	return _isLooped;
}

//===========================================================================//

ALfloat SoundInfo::GetPitch() const
{
	return _pitch;
}

//===========================================================================//

bool SoundInfo::UseBuffering() const
{
	return _useBuffering;
}

//===========================================================================//

const string & SoundInfo::GetFileName() const
{
	return _fileName;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SoundManager.h"

#include "../Utils/Console/Console.h"
#include "../Utils/PathHelper/PathHelper.h"
#include "../Utils/ParametersFileManager/ParametersFileManager.h"

namespace Bru
{

//===========================================================================//
SharedPtr<SoundManagerSingleton> SoundManager;
//===========================================================================//

SoundManagerSingleton::SoundManagerSingleton():
	_infos()
{
}

//===========================================================================//

SoundManagerSingleton::~SoundManagerSingleton()
{
}

//===========================================================================//

WeakPtr<SoundInfo> SoundManagerSingleton::GetSoundInfo(const string & id)
{
	if (_infos.Contains(id))
	{
		return _infos[id];
	}
	else
	{
		string fileName = PathHelper::PathToSounds + id;
		SharedPtr<SoundInfo> info = new SoundInfo();
		try
		{
			auto parametersFile = ParametersFileManager->Get(fileName);
			info->LoadFromFile(parametersFile);
		}
		catch (Exception & ex)
		{
			Console->Error(string::Format("Error loading sound info {0}: {1}", id, ex.GetDetails()));
		}
		_infos.Add(id, info);
		return info;
	}
}

//===========================================================================//

void SoundManagerSingleton::FreeResources()
{
	_infos.Clear();
}

//===========================================================================//

} // namespace Bru

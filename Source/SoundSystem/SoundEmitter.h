//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SOUND_EMITTER_H
#define SOUND_EMITTER_H

#include "../EntitySystem/EntityComponent.h"
#include "../Utils/Base/BaseDataProvider.h"
#include "Sound.h"

namespace Bru
{

//===========================================================================//

class SoundEmitter : public EntityComponent
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	SoundEmitter(const string & domainName);
	SoundEmitter(const string & domainName, const SharedPtr<BaseDataProvider> & provider);

	~SoundEmitter();

	void Play(const string & id);
	void Pause(const string & id);
	void Stop(const string & id);

private:
	static string SoundPath;
	static string EventKey;
	static string PathKey;
	static string ChannelKey;

	bool CheckSoundID(const string & id) const;

	TreeMap<string, WeakPtr<Sound>> _channels;
	TreeMap<string, ValuePair<WeakPtr<Sound>, string>> _events;
	TreeMap<string, SharedPtr<Sound>> _sounds;

private:
	SoundEmitter(const SoundEmitter &) = delete;
	SoundEmitter & operator =(const SoundEmitter &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // SOUND_EMITTER_H

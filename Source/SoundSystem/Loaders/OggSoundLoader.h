//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef OGG_SOUND_LOADER_H
#define OGG_SOUND_LOADER_H

#include "ISoundLoader.h"

// для борьбы с предупреждениями
#define OV_EXCLUDE_STATIC_CALLBACKS
#include <vorbisfile.h>

namespace Bru
{

//===========================================================================//

class OggSoundLoader: public ISoundLoader
{
public:
	OggSoundLoader(Sound & sound);
	~OggSoundLoader();

	void LoadData();
	void Reset();
	
	bool Stream(integer bufferSize);

private:
	static const integer NumBuffers;
	static const integer SampleSizeInBytes; // {1, 2}
	
	void InitializeStreaming();
	
	bool BufferData(integer bufferSize);

	FILE * _file;
	OggVorbis_File _oggStream;
	vorbis_info * _info;
	ALuint _format;
	ALuint * _buffers;
	integer _currentReadBuffer;
	integer _currentWriteBuffer;
	integer _filledBuffersCount;

private:
	OggSoundLoader(const OggSoundLoader &) = delete;
	OggSoundLoader & operator =(const OggSoundLoader &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // OGG_SOUND_LOADER_H

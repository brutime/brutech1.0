//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef I_SOUND_LOADER_H
#define I_SOUND_LOADER_H

#include "../../Common/Common.h"
#include "../Sound.h"

namespace Bru
{

//===========================================================================//

class ISoundLoader
{
public:
	ISoundLoader(Sound & sound);

	virtual ~ISoundLoader();

	virtual void LoadData() = 0;
	virtual void Reset() = 0;

	/// возвращает false когда воспроизведение закончилось
	virtual bool Stream(integer bufferSize) = 0;

	bool IsInitialized() const;

protected:
	virtual void InitializeStreaming() = 0;

	Sound & _sound;
	string _fileName;
	bool _isStreaming;
	bool _isInitialized;

private:
	ISoundLoader(const ISoundLoader &) = delete;
	ISoundLoader & operator =(const ISoundLoader &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // I_SOUND_LOADER_H

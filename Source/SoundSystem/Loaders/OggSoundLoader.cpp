//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "OggSoundLoader.h"
#include "../../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

const integer OggSoundLoader::NumBuffers = 2;
const integer OggSoundLoader::SampleSizeInBytes = 2;

//===========================================================================//

OggSoundLoader::OggSoundLoader(Sound & sound):
	ISoundLoader(sound),
	_file(),
	_oggStream(),
	_info(),
	_format(),
	_buffers(),
	_currentReadBuffer(0),
	_currentWriteBuffer(0),
	_filledBuffersCount(0)
{
	integer result;
	if((result = ov_fopen(_fileName.ToChars(), &_oggStream)) < 0)
	{
		fclose(_file);

		Console->Warning(string::Format("Could not open Ogg stream from file {0}: {1}", _fileName, result));
		return;
	}

	_info = ov_info(&_oggStream, -1);
	_format = (_info->channels == 1) ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;

	if (_isStreaming)
	{
		_buffers = new ALuint[NumBuffers];
		alGenBuffers(NumBuffers, _buffers);
	}
	else
	{
		_buffers = new ALuint;
		alGenBuffers(1, _buffers);
	}
}

//===========================================================================//

OggSoundLoader::~OggSoundLoader()
{
	ov_clear(&_oggStream);
	if (_isStreaming)
	{
		alDeleteBuffers(NumBuffers, _buffers);
	}
	else
	{
		alDeleteBuffers(1, _buffers);
	}

	integer error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Console->Warning(string::Format("Error deleting sound buffer: {0}", error));
	}

	delete [] _buffers;
}

//===========================================================================//

void OggSoundLoader::LoadData()
{
	if (_isStreaming)
	{
		if (_filledBuffersCount != 0)
		{
			INVALID_STATE_EXCEPTION("OggSoundLoader::LoadData() called more than once");
		}

		for (integer i = 0; i < NumBuffers; i++)
		{
			BufferData(_sound.GetInfo()->GetBufferSize());
		}
	}
	else
	{
		integer uncompressedSize = ov_pcm_total(&_oggStream, -1) * _info->channels * SampleSizeInBytes;
		BufferData(uncompressedSize);
	}

	InitializeStreaming();
	
	_isInitialized = true;
}

//===========================================================================//

void OggSoundLoader::Reset()
{
	integer enquedBuffersCount;
	ALuint buffer;
	integer error;

	const auto & sourceId = _sound.GetSourceId();
	alGetSourcei(sourceId, AL_BUFFERS_QUEUED, &enquedBuffersCount);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Console->Error(string::Format("Error acquiring number of queued buffers for source {0}: {1}", sourceId, error));
	}

	for (integer i = 0; i < enquedBuffersCount; i++)
	{
		alSourceUnqueueBuffers(sourceId, 1, &buffer);
		if ((error = alGetError()) != AL_NO_ERROR)
		{
			Console->Error(string::Format("Error unqueueing buffer for source {0}: {1}", sourceId, error));
		}
	}
	
	ov_raw_seek(&_oggStream, 0);
	
	_currentReadBuffer = 0;
	_currentWriteBuffer = 0;
	
	LoadData();
}

//===========================================================================//

void OggSoundLoader::InitializeStreaming()
{
	ALuint sourceId = _sound.GetSourceId();
	for (integer i = 0; i < _filledBuffersCount; i++)
	{
		integer error;
		alSourceQueueBuffers(sourceId, 1, &_buffers[_currentReadBuffer]);
		_currentReadBuffer = (_currentReadBuffer + 1) % NumBuffers;
		if ((error = alGetError()) != AL_NO_ERROR)
		{
			Console->Error(string::Format("Error queing buffers for source {0}: {1}", sourceId, error));
			return;
		}
	}
	_filledBuffersCount = 0;
}

//===========================================================================//

bool OggSoundLoader::Stream(integer bufferSize)
{
	integer processed;
	integer error;
	ALuint sourceId = _sound.GetSourceId();

	if (alIsSource(sourceId) == AL_FALSE)
	{
		Console->Warning(string::Format("Invalid sourceId='{0}' provided to {1}", sourceId, METHOD_NAME));
		return false;
	}

	alGetSourcei(sourceId, AL_BUFFERS_PROCESSED, &processed);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Console->Error(string::Format("Error getting AL_BUFFERS_PROCESSED for source {0}: {1}", sourceId, error));
		return false;
	}
	for (integer i = 0; i < processed; i++)
	{
		ALuint buffer;

		alSourceUnqueueBuffers(sourceId, 1, &buffer);
		if ((error = alGetError()) != AL_NO_ERROR)
		{
			Console->Error(string::Format("Error unqueing buffers for source {0}: {1}", sourceId, error));
		}

		if (BufferData(bufferSize))
		{
			alSourceQueueBuffers(sourceId, 1, &_buffers[_currentReadBuffer]);
			_currentReadBuffer = (_currentReadBuffer + 1) % NumBuffers;
			if ((error = alGetError()) != AL_NO_ERROR)
			{
				Console->Error(string::Format("Error queing buffers for source {0}: {1}", sourceId, error));
			}
			_filledBuffersCount--;
		}
	}

	return true;
}

//===========================================================================//

bool OggSoundLoader::BufferData(integer bufferSize)
{
	char * data = new char[bufferSize];
	integer size = 0;
	integer section;
	integer result;

	while(size < bufferSize)
	{
		//TODO: последние параметры надо покрутить под Linux\MacOS
		result = ov_read(&_oggStream, data + size, bufferSize - size, 0, SampleSizeInBytes, 1, &section);

		if(result > 0)
		{
			size += result;
		}
		else if(result < 0)
		{
			Console->Error(string::Format("Error occured while decoding file {0}: {1}", _fileName, result));
		}
		else
		{
			if (_sound.GetInfo()->IsLooped())
			{
				ov_raw_seek(&_oggStream, 0);
			}
			else
			{
				return false;
			}
		}
	}

	if(size == 0)
	{
		return false;
	}

	alBufferData(_buffers[_currentWriteBuffer], _format, data, size, _info->rate);
	if ((result = alGetError()) != AL_NO_ERROR)
	{
		Console->Error(string::Format("Error while moving decoded data to buffer: {0}", result));
	}
	delete [] data;
	_currentWriteBuffer = (_currentWriteBuffer + 1) % NumBuffers;
	_filledBuffersCount++;
	return true;
}

//===========================================================================//

} // namespace Bru

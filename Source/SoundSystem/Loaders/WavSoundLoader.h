//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef WAV_SOUND_LOADER_H
#define WAV_SOUND_LOADER_H

#include "ISoundLoader.h"

namespace Bru
{

//===========================================================================//

class WavSoundLoader: public ISoundLoader
{
public:
	WavSoundLoader(Sound & sound);
	~WavSoundLoader();

	void LoadData();
	void Reset();
	
	void InitializeStreaming();
	bool Stream(integer bufferSize);

private:
	ALuint bufferId;

private:
	WavSoundLoader(const WavSoundLoader &) = delete;
	WavSoundLoader & operator =(const WavSoundLoader &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // WAV_SOUND_LOADER_H

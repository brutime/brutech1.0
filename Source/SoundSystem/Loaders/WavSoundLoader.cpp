//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WavSoundLoader.h"

#include <alut.h>

#include "../../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

WavSoundLoader::WavSoundLoader(Sound & sound):
	ISoundLoader(sound),
	bufferId(0)
{
}

//===========================================================================//

WavSoundLoader::~WavSoundLoader()
{
}

//===========================================================================//

void WavSoundLoader::LoadData()
{
	if (_isStreaming)
	{
		NOT_IMPLEMENTED_EXCEPTION("WavSoundLoader::LoadData() for streaming");
	}
	else
	{
		bufferId = alutCreateBufferFromFile(_fileName);
		if (bufferId == AL_NONE)
		{
			Console->Error(string::Format("Couldn't load sound {0}", _fileName));
			return;
		}
	}
	_isInitialized = true;
}

//===========================================================================//

void WavSoundLoader::Reset()
{
}

//===========================================================================//

void WavSoundLoader::InitializeStreaming()
{
	NOT_IMPLEMENTED_EXCEPTION(METHOD_NAME);
}

//===========================================================================//

bool WavSoundLoader::Stream(integer)
{
	NOT_IMPLEMENTED_EXCEPTION(METHOD_NAME);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SoundEmitter.h"

#include "SoundSystem.h"
#include "../Utils/Console/Console.h"
#include "../EntitySystem/EntitySystem.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(SoundEmitter, EntityComponent)

//===========================================================================//

string SoundEmitter::SoundPath = "Sounds";
string SoundEmitter::EventKey = "Event";
string SoundEmitter::PathKey = "Path";
string SoundEmitter::ChannelKey = "Channel";

//===========================================================================//

SoundEmitter::SoundEmitter(const string & domainName) :
	EntityComponent(domainName),
	_channels(),
	_events(),
	_sounds()
{
	GetName();
}

//===========================================================================//

SoundEmitter::SoundEmitter(const string &domainName, const SharedPtr<BaseDataProvider> &provider) :
	SoundEmitter(domainName)
{
	for (const auto & section : provider->GetAnonymousChildren(SoundPath))
	{
		string event = section->Get<string>(EventKey);
		string path = section->Get<string>(PathKey);
		string channel = section->Get<string>(ChannelKey);
		
		SharedPtr<Sound> sound;
		if (_sounds.Contains(path))
		{
			sound = _sounds[path];
		}
		else
		{
			sound = SoundSystem->CreateSound(path);		
			_sounds.Add(path, sound);
		}
		
		_channels.Add(channel, NullPtr);
		_events.Add(event, {sound, channel});
	}
}

//===========================================================================//

SoundEmitter::~SoundEmitter()
{
}

//===========================================================================//

void SoundEmitter::Play(const string & id)
{
	if (!CheckSoundID(id)) 
		return;
	
	const auto & soundChannelPair = _events[id];
	if (_channels[soundChannelPair.Second] != NullPtr)
	{
		_channels[soundChannelPair.Second]->Stop();
	}
	_channels[soundChannelPair.Second] = soundChannelPair.First;
	_channels[soundChannelPair.Second]->Play();
}

//===========================================================================//

void SoundEmitter::Pause(const string & id)
{
	if (!CheckSoundID(id))
		return;
	
	const auto & soundChannelPair = _events[id];
	if (_channels[soundChannelPair.Second] != NullPtr)
	{
		_channels[soundChannelPair.Second]->Pause();
	}
}

//===========================================================================//

void SoundEmitter::Stop(const string & id)
{
	if (!CheckSoundID(id))
		return;
	
	const auto & soundChannelPair = _events[id];
	if (_channels[soundChannelPair.Second] != NullPtr)
	{
		_channels[soundChannelPair.Second]->Stop();
	}
}

//===========================================================================//

bool SoundEmitter::CheckSoundID(const string & id) const
{
	if (!_events.Contains(id))
	{
		Console->Warning("Unknown event ID '{0}' provided", id);
		return false;
	}
	return true;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Sound.h"

#include "../Utils/Console/Console.h"
#include "../Utils/PathHelper/PathHelper.h"
#include "Loaders/WavSoundLoader.h"
#include "Loaders/OggSoundLoader.h"
#include "SoundManager.h"
#include "SoundSystem.h"

namespace Bru
{

//===========================================================================//

Sound::Sound(const string & id):
	_id(id),
	_sourceId(0),
	_info(SoundManager->GetSoundInfo(id)),
	_loader(GetLoaderForFile(_info->GetFileName())),
	_state(SoundState::NotInitialized),
	_mutex()
{
}

//===========================================================================//

Sound::~Sound()
{
	Stop();

	if (_sourceId != 0)
	{
		SoundSystem->ReturnSourceToPool(_sourceId);
	}
	
	SoundSystem->UnregisterPlayingSound(this);
	
	_loader = NullPtr;
}

//===========================================================================//

void Sound::Play()
{
	std::lock_guard<std::mutex> guard(_mutex);
	
	if (_sourceId == 0)
	{
		_sourceId = SoundSystem->GetFreeSource();
		if (_sourceId == 0)
		{
			Console->Debug(string::Format("Can't play sound {0} - no sources available", _id));
			return;
		}

		PrepareToPlay();
	}
	else
	{
		if (_state == SoundState::Playing)
		{
			alSourceStop(_sourceId);
		}
	}

	ALfloat sourcePos[] = {0, 0, 0};
	alSourcef(_sourceId, AL_PITCH, _info->GetPitch());
	alSourcef(_sourceId, AL_GAIN, _info->GetGain());
	alSourcefv(_sourceId, AL_POSITION, sourcePos);
	if (!_info->UseBuffering())
	{
		alSourcei(_sourceId, AL_LOOPING, _info->IsLooped());
	}

	alSourcePlay(_sourceId);

	integer error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Console->Warning(string::Format("Error when trying to play sound {0}: {1}", _id, error));
		return;
	}

	_state = SoundState::Playing;
	SoundSystem->RegisterPlayingSound(this);
}

//===========================================================================//

void Sound::Pause()
{
	std::lock_guard<std::mutex> guard(_mutex);
	
	if (_sourceId == 0)
	{
		Console->Error(string::Format("{0}: source is not set", METHOD_NAME));
	}

	alSourcePause(_sourceId);

	integer error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Console->Warning(string::Format("Error when trying to pause sound {0}: {1}", _id, error));
		return;
	}

	_state = SoundState::Paused;
}

//===========================================================================//

void Sound::Stop()
{
	std::lock_guard<std::mutex> guard(_mutex);

	if (_state == SoundState::Stopped || _state == SoundState::NotInitialized)
	{
		return;
	}

	if (_sourceId == 0)
	{
		Console->Error(string::Format("{0}: source is not set", METHOD_NAME));
	}

	alSourceStop(_sourceId);

	integer error;
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Console->Warning(string::Format("Error when trying to stop sound {0}: {1}", _id, error));
		return;
	}
	
	_loader->Reset();

	_state = SoundState::Stopped;
}

//===========================================================================//

void Sound::PrepareToPlay()
{
	if (_state != SoundState::NotInitialized && _state != SoundState::Stopped)
	{
		Console->Error(string::Format("{0}: invalid sound state", METHOD_NAME));
	}

	if (alIsSource(_sourceId) == AL_FALSE)
	{
		Console->Error(string::Format("Invalid source {0} provided to {1}", _sourceId, METHOD_NAME));
	}
	
	_loader->LoadData();

	_state = SoundState::Initialized;
}

//===========================================================================//

bool Sound::Stream() const
{
	if (_state != SoundState::Playing)
	{
		return false;
	}
	
	if (_info->UseBuffering())
	{
		return _loader->Stream(_info->GetBufferSize());
	}
	else
	{
		Console->Error("Sound without buffering can't be streamed");
		return false;
	}
}

//===========================================================================//

SharedPtr<ISoundLoader> Sound::GetLoaderForFile(const string & fileName)
{
	string ext = PathHelper::GetExtension(fileName).ToLower();
	string fullPath = PathHelper::PathToData + PathHelper::PathToSounds + fileName;
	if (ext == PathHelper::WAVExtension)
	{
		return new WavSoundLoader(*this);
	}

	if (ext == PathHelper::OGGExtension)
	{
		return new OggSoundLoader(*this);
	}

	INVALID_ARGUMENT_EXCEPTION(string::Format("Attempt to load file with unknown extension: {0}", fullPath));
}

//===========================================================================//

const ALuint & Sound::GetSourceId() const
{
	return _sourceId;
}

//===========================================================================//

const WeakPtr<SoundInfo> & Sound::GetInfo() const
{
	return _info;
}

//===========================================================================//

} // namespace Bru

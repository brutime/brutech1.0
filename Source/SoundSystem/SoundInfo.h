//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SOUND_INFO_H
#define SOUND_INFO_H

//для борьбы с предупреждениями
#define TARGET_OS_MAC false
#include <AL/al.h>

#include "../Common/Common.h"
#include "../Utils/ParametersFile/ParametersFile.h"

namespace Bru
{

//===========================================================================//

class SoundInfo
{
public:
	SoundInfo();
	~SoundInfo();
	
	void LoadFromFile(const WeakPtr<ParametersFile> & parameters);

	void SetBufferSize(const integer & bufferSize);
	void SetGain(const ALfloat & gain);
	void SetIsLooped(bool isLooped);
	void SetPitch(const ALfloat & pitch);
	void SetUseBuffering(bool useBuffering);

	integer GetBufferSize() const;
	ALfloat GetGain() const;
	bool IsLooped() const;
	ALfloat GetPitch() const;
	bool UseBuffering() const;
	const string & GetFileName() const;

private:
	static const string IsLoopedKey;
	static const string UseBufferingKey;
	static const string PitchKey;
	static const string GainKey;
	static const string BufferSizeKey;
	static const integer DefaultBufferSize;

	ALfloat _pitch;
	ALfloat _gain;
	bool _isLooped;
	bool _useBuffering;
	integer _bufferSize;
	string _fileName;

private:
	SoundInfo(const SoundInfo &) = delete;
	SoundInfo & operator =(const SoundInfo &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // SOUND_INFO_H

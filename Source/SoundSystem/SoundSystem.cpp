//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SoundSystem.h"

#include "../OperatingSystem/GlobalTimer.h"
#include "../ScriptSystem/ScriptSystem.h"
#include "../Utils/Console/Console.h"
#include <alut.h>
#include "SoundManager.h"

namespace Bru
{

//===========================================================================//

SharedPtr<SoundSystemSingleton> SoundSystem;
const string SoundSystemSingleton::Name = "SoundSystem";
const float SoundSystemSingleton::UpdatePeriod = 1.0 / 20;

//===========================================================================//

SoundSystemSingleton::SoundSystemSingleton():
	_maxSourceCount(0),
	_sources(NullPtr),
	_sourceAvailable(NullPtr),
	_nonLoopingSounds(),
	_streamingSounds(),
	_updateTimeout(UpdatePeriod),
	_worker(),
	_mutex(),
	_streamingSoundsMutex(),
	_nonLoopingSoundsMutex(),
	_terminateWorker(false)
{
	alutInit(NullPtr, 0);
	alGetError();
	float position[] = {0, 0, 0};
	float velocity[] = {0, 0, 0};
	float orientation[] = {0, 0, -1, 0, 1, 0};
	alListenerfv(AL_POSITION, position);
	alListenerfv(AL_VELOCITY, velocity);
	alListenerfv(AL_ORIENTATION, orientation);

	SoundManager = new SoundManagerSingleton();

	InitializeSourcesPool();
	StartWorker();
}

//===========================================================================//

SoundSystemSingleton::~SoundSystemSingleton()
{
	StopWorker();
	integer error;
	alDeleteSources(_maxSourceCount, _sources);
	if ((error = alGetError()) != AL_NO_ERROR)
	{
		Console->Warning(string::Format(Name + ": error deleting sound sources: {0}", error));
	}
	SoundManager->FreeResources();
	DestroySourcesPool();
	alutExit();
}

//===========================================================================//

SharedPtr<Sound> SoundSystemSingleton::CreateSound(const string & id) const
{
	try
	{
		SharedPtr<Sound> sound = new Sound(id);
		return sound;
	}
	catch (Exception & e)
	{
		Console->Error(string::Format("Error loading sound {0}: {1}", id, e.GetDetails()));
		return NullPtr;
	}
}

//===========================================================================//

void SoundSystemSingleton::RegisterPlayingSound(Sound * sound)
{
	std::lock_guard<std::recursive_mutex> methodGuard(_mutex);

	const auto & info = sound->GetInfo();
	if (info->UseBuffering())
	{
		_streamingSounds.Add(sound);
	}
	if (!info->IsLooped())
	{
		_nonLoopingSounds.Add(sound);
	}
}

//===========================================================================//

void SoundSystemSingleton::UnregisterPlayingSound(Sound * sound)
{
	std::lock_guard<std::recursive_mutex> methodGuard(_mutex);

	const auto & info = sound->GetInfo();
	if (info->UseBuffering())
	{
		_streamingSounds.RemoveIfExists(sound);
	}
	if (!info->IsLooped())
	{
		_nonLoopingSounds.RemoveIfExists(sound);
	}
}

//===========================================================================//

void SoundSystemSingleton::StartWorker()
{
	_worker = std::thread(&SoundSystemSingleton::DoWork, this);
}

//===========================================================================//

void SoundSystemSingleton::StopWorker()
{
	_terminateWorker = true;
	_worker.join();
}

//===========================================================================//

void SoundSystemSingleton::DoWork()
{
	try
	{
		while (!_terminateWorker)
		{
			{
				std::lock_guard<std::recursive_mutex> guard(_mutex);

				UpdateStreamingSounds();
				CheckIfSoundsStopped();
			}
			std::this_thread::sleep_for(std::chrono::duration<double>(UpdatePeriod));
		}
	}
	catch (Exception & e)
	{
		//к этому моменту исключение уже обработано в ExceptionRegistrator
	}
	catch (...)
	{
		Console->Error(string::Format("{0}: bad things are happening", METHOD_NAME));
	}
}

//===========================================================================//

void SoundSystemSingleton::UpdateStreamingSounds()
{
	List<Sound *> soundsToBeStopped;

	{
		for (const auto & sound : _streamingSounds)
		{
			if (!sound->Stream())
			{
				soundsToBeStopped.Add(sound);
			}
		}

		for (const auto & sound : soundsToBeStopped)
		{
			sound->Stop();
			UnregisterPlayingSound(sound);
		}
	}
}

//===========================================================================//

void SoundSystemSingleton::CheckIfSoundsStopped()
{
	List<Sound *> soundsToBeStopped;

	{
		for (auto sound : _nonLoopingSounds)
		{
			ALenum state;
			alGetSourcei(sound->GetSourceId(), AL_SOURCE_STATE, &state);
			if (state == AL_STOPPED)
			{
				soundsToBeStopped.Add(sound);
			}
		}

		for (const auto & sound : soundsToBeStopped)
		{
			sound->Stop();
			UnregisterPlayingSound(sound);
		}
	}
}

//===========================================================================//

void SoundSystemSingleton::InitializeSourcesPool()
{
	//нужно определять программно
	_maxSourceCount = 16;

	_sources = new ALuint[_maxSourceCount];
	_sourceAvailable = new bool[_maxSourceCount];
	for (integer i = 0; i < _maxSourceCount; i++)
	{
		_sourceAvailable[i] = true;
	}

	alGenSources(_maxSourceCount, _sources);
}

//===========================================================================//

void SoundSystemSingleton::DestroySourcesPool()
{
	alDeleteSources(_maxSourceCount, _sources);
	delete [] _sources;
	delete [] _sourceAvailable;
	_sources = NullPtr;
	_sourceAvailable = NullPtr;
}

//===========================================================================//

ALuint SoundSystemSingleton::GetFreeSource() const
{
	std::lock_guard<std::recursive_mutex> guard(_mutex);

	for (integer i = 0; i < _maxSourceCount; i++)
	{
		if (_sourceAvailable[i])
		{
			_sourceAvailable[i] = false;
			return _sources[i];
		}
	}
	return 0;
}

//===========================================================================//

void SoundSystemSingleton::ReturnSourceToPool(ALuint sourceId)
{
	std::lock_guard<std::recursive_mutex> guard(_mutex);

	alSourcei(sourceId, AL_BUFFER, 0);

	integer sourceIndex;
	for (sourceIndex = 0; sourceIndex < _maxSourceCount; sourceIndex++)
	{
		if (_sources[sourceIndex] == sourceId)
		{
			_sourceAvailable[sourceIndex] = true;
			break;
		}
	}
	if (sourceIndex == _maxSourceCount)
	{
		Console->Error(string::Format("Attempt to return invalid source {0} to pool", sourceId));
	}
}

//===========================================================================//

} // namespace Bru

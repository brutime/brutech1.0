//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SOUND_H
#define SOUND_H

#include "SoundInfo.h"
#include "Enums/SoundState.h"

#include <mutex>
#include <atomic>

namespace Bru
{

//===========================================================================//

class ISoundLoader;

//===========================================================================//
	
class Sound
{
public:
	Sound(const string & id);
	~Sound();

	void Play();
	void Pause();
	void Stop();
	
	bool Stream() const;

	const ALuint & GetSourceId() const;
	const WeakPtr<SoundInfo> & GetInfo() const;

private:
	SharedPtr<ISoundLoader> GetLoaderForFile(const string & fileName);
	void PrepareToPlay();
	
	string _id;
	ALuint _sourceId;
	WeakPtr<SoundInfo> _info;
	SharedPtr<ISoundLoader> _loader;
	std::atomic<SoundState> _state;
	mutable std::mutex _mutex;

private:
	Sound(const Sound &) = delete;
	Sound & operator =(const Sound &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // SOUND_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Updatable.h"

#include "Updater.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(Updatable, EntityComponent)

//===========================================================================//

const string Updatable::PathToIsEnabled = "IsEnabled";
const string Updatable::PathToUpdatingGroupName = "UpdatingGroupName";

//===========================================================================//

Updatable::Updatable(const string & domainName, bool isEnabled) :
	Updatable(domainName, UpdaterSingleton::DefaultUpdatingGroupName, isEnabled)
{
}

//===========================================================================//

Updatable::Updatable(const string & domainName, const string & updatingLayerName, bool isEnabled) :
	EntityComponent(domainName),
	_isEnabled(isEnabled),
	_isEnabledOnInitialize(_isEnabled),
	_isMarkerForRemove(false),
	_updatingLayerName(updatingLayerName)
{
	Register();
}

//===========================================================================//

Updatable::Updatable(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	EntityComponent(domainName),
	_isEnabled(provider->Get<bool>(PathToIsEnabled)),
	_isEnabledOnInitialize(_isEnabled),
	_isMarkerForRemove(false),
	_updatingLayerName(provider->Get<string>(PathToUpdatingGroupName))
{
	Register();
}

//===========================================================================//

Updatable::~Updatable()
{
	if (IsRegistred())
	{
		Unregister();
	}
}

//===========================================================================//

void Updatable::Reset()
{
	EntityComponent::Register();
	_isEnabledOnInitialize ? Enable() : Disable();
}

//===========================================================================//

void Updatable::Enable()
{
	_isEnabled = true;
}

//===========================================================================//

void Updatable::Disable()
{
	_isEnabled = false;
}

//===========================================================================//

bool Updatable::IsEnabled() const
{
	return _isEnabled;
}

//===========================================================================//

void Bru::Updatable::SetMarkForRemove(bool isMarkerForRemove)
{
	_isMarkerForRemove = isMarkerForRemove;
}

//===========================================================================//

bool Updatable::IsMarkedForRemove() const
{
	return _isMarkerForRemove;
}

//===========================================================================//

const string & Updatable::GetUpdatingLayerName() const
{
	return _updatingLayerName;
}

//===========================================================================//

void Updatable::Register()
{
	Updater->RegisterComponent(this);
	EntityComponent::Register();
}

//===========================================================================//

void Updatable::Unregister()
{
	Updater->UnregisterComponent(this);
	EntityComponent::Unregister();
}

//===========================================================================//

} // namespace Bru

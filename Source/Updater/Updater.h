//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: updater
//

#pragma once
#ifndef UPDATER_H
#define UPDATER_H

#include "../Common/Common.h"
#include "Updatable.h"
#include "ActionList/ActionList.h"

namespace Bru
{

//===========================================================================//

class UpdaterSingleton
{
public:
	static const string DefaultUpdatingGroupName;

	UpdaterSingleton();
	~UpdaterSingleton();

	void Update(float deltaTime);

	void AddDomain(const string & domainName);
	void RemoveDomain(const string & domainName);
	void SelectDomain(const string & domainName);

	void RegisterComponent(Updatable * const component);
	void UnregisterComponent(Updatable * const component);

	const string&GetCurrentDomainName() const;

	integer GetComponentCount(const string & domainName) const;
	integer GetComponentCount() const;
	integer GetEnabledComponentCount() const;

	///Принимает набор правил, определяющих порядок обоновления updatable-компонентов
	///Перераспределяет уже зарегистрированные компоненты в соответствии с новыми правилами
	void SetUpdatingGroups(const Array<string> & updatingGroups);

	void PrintUpdatables(const string & domainName) const;
	void PrintUpdatables() const;

	void PrintUpdatingGroups() const;

private:
	static const string Name;

	void RemoveAllDomains();

	void Update(const string & domainName, float deltaTime);

	void RemoveComponent(Updatable * const component);
	void RemoveEmptyLists();

	string GetComponentData(integer updatingOrder, Updatable * const component) const;

	void RegisterCommands();
	void UnregisterCommands();

	integer GetUpdatingOrder(const Updatable * const component) const;

	TreeMap<string, TreeMap<integer, List<Updatable * const>>> _components;
	string _currentDomainName;

	TreeMap<string, integer> _updatingGroups;
	//mutable TreeMap<integer, integer> _updatingOrderCache;
	List<Updatable * const> _componentsMarkedForRemove;

	bool _isUpdating;

	UpdaterSingleton(const UpdaterSingleton &) = delete;
	UpdaterSingleton &operator =(const UpdaterSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<UpdaterSingleton> Updater;

//===========================================================================//

} //namespace

#endif // UPDATER_H

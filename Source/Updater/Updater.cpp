//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Updater.h"

#include "../ScriptSystem/ScriptSystem.h"
#include "../Utils/Console/Console.h"
#include "../Utils/PerfomanceDataCollector/PerfomanceDataCollector.h"

namespace Bru
{

//===========================================================================//

SharedPtr<UpdaterSingleton> Updater;

//===========================================================================//

const string UpdaterSingleton::DefaultUpdatingGroupName = "Default";

//===========================================================================//

const string UpdaterSingleton::Name = "Updater";

//===========================================================================//

UpdaterSingleton::UpdaterSingleton() :
	_components(),
	_currentDomainName(),
	_updatingGroups(),
	_componentsMarkedForRemove(),
	_isUpdating(false)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	AddDomain(EntitySystem::GlobalDomainName);
	SetUpdatingGroups({});

	RegisterCommands();

	PerfomanceDataCollector->AddMeasurement(Name);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

UpdaterSingleton::~UpdaterSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

#ifdef DEBUGGING
	if (!_componentsMarkedForRemove.IsEmpty())
	{
		INVALID_STATE_EXCEPTION(string::Format("{0} not deleted components marked for remove", Name));
	}
#endif

	PerfomanceDataCollector->RemoveMeasurement(Name);

	UnregisterCommands();
	RemoveAllDomains();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void UpdaterSingleton::Update(float deltaTime)
{
	PerfomanceDataCollector->StartMeasurement(Name);

	_isUpdating = true;

	Update(_currentDomainName, deltaTime);
	Update(EntitySystem::GlobalDomainName, deltaTime);
	RemoveEmptyLists();

	_isUpdating = false;

	PerfomanceDataCollector->EndMeasurement(Name);
}

//===========================================================================//

void UpdaterSingleton::AddDomain(const string & domainName)
{
#ifdef DEBUGGING
	if (!EntitySystem::IsDomainNameValid(domainName))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} trying to add domain with invalid name: \"{0}\"", Name, domainName));
	}
#endif

	_components.Add(domainName, TreeMap<integer, List<Updatable * const >> ());

	Console->Debug(string::Format("{0} added domain: \"{1}\"", Name, domainName));
}

//===========================================================================//

void UpdaterSingleton::RemoveDomain(const string & domainName)
{
#ifdef DEBUGGING
	if (GetComponentCount(domainName) != 0)
	{
		PrintUpdatables(domainName);
		INVALID_STATE_EXCEPTION(string::Format("{0} trying to delete not empty domain: \"{1}\"", Name, domainName));
	}
#endif

	_components.Remove(domainName);

	Console->Debug(string::Format("{0} removed domain: \"{1}\"", Name, domainName));
}

//===========================================================================//

void UpdaterSingleton::SelectDomain(const string & domainName)
{
	_currentDomainName = domainName;
}

//===========================================================================//

void UpdaterSingleton::RegisterComponent(Updatable * const component)
{
	const string & domainName = component->GetDomainName();
	integer updatingOrder = GetUpdatingOrder(component);

#ifdef DEBUGGING
	if (_components[domainName].Contains(updatingOrder) && _components[domainName][updatingOrder].Contains(component))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("{0} already contains component: \"{1}\"", Name, component->GetName()));
	}
#endif

	if (!_components[domainName].Contains(updatingOrder))
	{
		_components[domainName].Add(updatingOrder, List<Updatable * const>());
	}

	_components[domainName][updatingOrder].Add(component);
}

//===========================================================================//

void UpdaterSingleton::UnregisterComponent(Updatable * const component)
{
	//Компонент удалиться сразу, если метод вызван не из Update
	if (!_isUpdating)
	{
		RemoveComponent(component);
	}
	else
	{
		component->SetMarkForRemove(true);
		_componentsMarkedForRemove.Add(component);
	}
}

//===========================================================================//

const string &UpdaterSingleton::GetCurrentDomainName() const
{
	return _currentDomainName;
}

//===========================================================================//

integer UpdaterSingleton::GetComponentCount(const string & domainName) const
{
	integer count = 0;
	for (const auto & updatablePair : _components[domainName])
	{
		count += updatablePair.GetValue().GetCount();
	}
	return count;
}

//===========================================================================//

integer UpdaterSingleton::GetComponentCount() const
{
	integer count = 0;
	for (const auto & componentPair : _components)
	{
		for (const auto & updatablePair : componentPair.GetValue())
		{
			count += updatablePair.GetValue().GetCount();
		}
	}
	return count;
}

//===========================================================================//

integer UpdaterSingleton::GetEnabledComponentCount() const
{
	integer count = 0;
	for (const auto & componentPair : _components)
	{
		for (const auto & updatablePair : componentPair.GetValue())
		{
			for (auto * const updatable : updatablePair.GetValue())
			{
				if (updatable->IsEnabled())
				{
					count++;
				}
			}
		}
	}
	return count;
}

//===========================================================================//

void UpdaterSingleton::SetUpdatingGroups(const Array<string> & updatingGroups)
{
	_updatingGroups.Clear();
	integer groupUpdatingOrder = 0;
	_updatingGroups.Add(DefaultUpdatingGroupName, groupUpdatingOrder);
	for (const auto & updatingGroup : updatingGroups)
	{
		groupUpdatingOrder++;
		_updatingGroups.Add(updatingGroup, groupUpdatingOrder);
	}

	TreeMap<string, TreeMap<integer, List<Updatable * const >>> oldComponents = _components;
	for (auto & domainPair : _components)
	{
		domainPair.GetValue().Clear();
	}

	for (const auto & domainPair : oldComponents)
	{
		for (const auto & updatingGroupPair : domainPair.GetValue())
		{
			for (const auto & component : updatingGroupPair.GetValue())
			{
				RegisterComponent(component);
			}
		}
	}
}

//===========================================================================//

void UpdaterSingleton::PrintUpdatables(const string & domainName) const
{
	Console->Hint("Updatables (\"{0}\"):", domainName);
	Console->AddOffset(2);
	for (const auto & domainPair : _components[domainName])
	{
		for (auto * const updatable : domainPair.GetValue())
		{
			Console->Hint(GetComponentData(domainPair.GetKey(), updatable));
		}
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void UpdaterSingleton::PrintUpdatables() const
{
	Console->Hint("Updatables:");
	Console->AddOffset(2);
	for (const auto & componentPair : _components)
	{
		Console->Hint(string::Format("{0}:", componentPair.GetKey()));
		Console->AddOffset(2);
		for (const auto & domainPair : componentPair.GetValue())
		{
			for (auto * const updatable : domainPair.GetValue())
			{
				Console->Hint(GetComponentData(domainPair.GetKey(), updatable));
			}
		}
		Console->ReduceOffset(2);
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void UpdaterSingleton::PrintUpdatingGroups() const
{
	TreeMap<integer, string> _updatingGroupsByOrder;
	for (const auto & updatingGroupsPair : _updatingGroups)
	{
		_updatingGroupsByOrder.Add(updatingGroupsPair.GetValue(), updatingGroupsPair.GetKey());
	}

	List<string> printedComponents;

	Console->Hint("UpdatingGroups:");
	Console->AddOffset(2);

	for (const auto & componentPair : _components)
	{
		Console->Hint(string::Format("{0}:", componentPair.GetKey()));
		Console->AddOffset(2);
		for (const auto & _updatingGroupsByOrderPair : _updatingGroupsByOrder)
		{
			Console->Hint(string::Format("- {0}, {1}:", _updatingGroupsByOrderPair.GetKey(), _updatingGroupsByOrderPair.GetValue()));
			Console->AddOffset(2);
			for (const auto & domainPair : componentPair.GetValue())
			{
				for (auto * const updatable : domainPair.GetValue())
				{
					if (updatable->GetDomainName() == componentPair.GetKey() &&
					    updatable->GetUpdatingLayerName() == _updatingGroupsByOrderPair.GetValue() &&
					    !printedComponents.Contains(updatable->GetTypeName() + updatable->GetDomainName()))
					{
						Console->Hint(string::Format("- {0} ({1})", updatable->GetTypeName(), componentPair.GetKey()));
						printedComponents.Add(updatable->GetTypeName() + updatable->GetDomainName());
					}
				}				
			}
			Console->ReduceOffset(2);		
		}
		Console->ReduceOffset(2);
	}
	Console->ReduceOffset(2);
}

//===========================================================================//

void UpdaterSingleton::RemoveAllDomains()
{
	//Нельзя просто вызвать Clear - нужно проверить, пусты ли слои в доменах
	//А такая проверка проходит в RemoveDomain
	auto it = _components.CreateIterator();
	while (it.HasNext())
	{
		string domainName = it.GetCurrent().GetKey();
		it.Next();

		RemoveDomain(domainName);
	}
}

//===========================================================================//

void UpdaterSingleton::Update(const string & domainName, float deltaTime)
{
	if (domainName.IsEmpty())
	{
		return;
	}

	for (const auto & domainPair : _components[domainName])
	{
		for (auto * const updatable : domainPair.GetValue())
		{
			if (!updatable->IsMarkedForRemove() && updatable->IsEnabled())
			{
				updatable->Update(deltaTime);
			}
		}
	}

	for (auto * const markedForRemoveUpdatable : _componentsMarkedForRemove)
	{
		RemoveComponent(markedForRemoveUpdatable);
	}

	_componentsMarkedForRemove.Clear();
}

//===========================================================================//

void UpdaterSingleton::RemoveComponent(Updatable * const component)
{
	component->SetMarkForRemove(false);

	const string & typeName = component->GetTypeName();
	integer updatingOrder = GetUpdatingOrder(component);

#ifdef DEBUGGING
	string domainName = component->GetDomainName();
	for (const auto & domainPair : _components)
	{
		for (const auto & updatingGroupPair : domainPair.GetValue())
		{
			for (const auto & currentComponent : updatingGroupPair.GetValue())
			{
				if (currentComponent == component && (domainName != domainPair.GetKey() || updatingOrder != updatingGroupPair.GetKey()))
				{
					Console->Debug(string::Format("Component with type \"{0}\" found in \"{1}\".[{2}], but expected in \"{3}\".[{4}]",
					                              typeName, domainPair.GetKey(), updatingGroupPair.GetKey(), domainName, updatingOrder));
					domainName = domainPair.GetKey();
					updatingOrder = updatingGroupPair.GetKey();
				}
			}
		}
	}
#else
	const string & domainName = component->GetDomainName();
#endif

	_components[domainName][updatingOrder].Remove(component);
}

//===========================================================================//

void UpdaterSingleton::RemoveEmptyLists()
{
	for (auto & componentPair : _components)
	{
		auto it = componentPair.GetValue().CreateIterator();
		while (it.HasNext())
		{
			integer updatingOrder = it.GetCurrent().GetKey();
			const List<Updatable * const> & list = it.GetCurrent().GetValue();
			it.Next();

			if (list.IsEmpty())
			{
				componentPair.GetValue().Remove(updatingOrder);
			}
		}
	}
}

//===========================================================================//

string UpdaterSingleton::GetComponentData(integer updatingOrder, Updatable * const component) const
{
	if (component->GetOwner() != NullPtr)
	{
		return string::Format("- {0}, {1}->{2}", updatingOrder, component->GetOwner()->GetName(), component->GetName());
	}
	else
	{
		return string::Format("- {0}, {1}", updatingOrder, component->GetName());
	}
}

//===========================================================================//

void UpdaterSingleton::RegisterCommands()
{
	ScriptSystem->RegisterCommand(new ClassConstCommand<UpdaterSingleton, void, const string &>("Updatables", this, &UpdaterSingleton::PrintUpdatables));
	ScriptSystem->RegisterCommand(new ClassConstCommand<UpdaterSingleton, void>("Updatables", this, &UpdaterSingleton::PrintUpdatables));
	ScriptSystem->RegisterCommand(new ClassConstCommand<UpdaterSingleton, void>("UpdatingGroups", this, &UpdaterSingleton::PrintUpdatingGroups));
}

//===========================================================================//

void UpdaterSingleton::UnregisterCommands()
{
	ScriptSystem->UnregisterCommand("Updatables");
	ScriptSystem->UnregisterCommand("UpdatingGroups");
}

//===========================================================================//

integer UpdaterSingleton::GetUpdatingOrder(const Updatable * const component) const
{
#ifdef DEBUGGING
	if (!_updatingGroups.Contains(component->GetUpdatingLayerName()))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Unkhown updating layer name: \"{0}\"", component->GetUpdatingLayerName()));
	}
#endif

	return _updatingGroups[component->GetUpdatingLayerName()];
}

//===========================================================================//

} // namespace Bru

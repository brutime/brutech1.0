//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef TASK_BLOCKING_STATE_H
#define TASK_BLOCKING_STATE_H

namespace Bru
{

//===========================================================================//

enum class TaskBlockingState
{
    Blocking,
    NotBlocking,
};

//===========================================================================//

} // namespace Bru

#endif // TASK_BLOCKING_STATE_H

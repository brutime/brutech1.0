//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ACTION_LIST_H
#define ACTION_LIST_H

#include "../../Updater/Updatable.h"

#include "Action.h"

namespace Bru
{

//===========================================================================//

class ActionList : public Updatable, public PtrFromThisMixIn<ActionList>
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	ActionList(const string & domainName, bool isEnabled = true);
	ActionList(const string & domainName, const SharedPtr<BaseDataProvider> & provider);
	virtual ~ActionList();

	virtual void Reset();

	virtual void Update(float deltaTime);

	void Add(const SharedPtr<Action> & action);
	void Push(const SharedPtr<Action> & action);
	void Clear();

	bool IsEmpty() const;

	const List<SharedPtr<Action>> & GetActions() const;

private:
	List<SharedPtr<Action>> _actions;

	ActionList(const ActionList &) = delete;
	ActionList & operator =(const ActionList &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ACTION_LIST_H

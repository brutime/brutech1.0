//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ACTION_H
#define ACTION_H

#include "../../Common/Common.h"
#include "../../EntitySystem/EntitySystem.h"
#include "Enums/TaskBlockingState.h"
#include "Enums/TaskInitializingState.h"

namespace Bru
{

//===========================================================================//

class ActionList;

//===========================================================================//

class Action
{
	friend class ActionList;

public:
	virtual ~Action();

	virtual void Initialize();
	virtual bool Update(float deltaTime) = 0;

	void Block();

	const WeakPtr<Entity> & GetOwner() const;
	const WeakPtr<ActionList> & GetList() const;

	const string & GetMask() const;
	TaskBlockingState GetBlockingState() const;
	TaskInitializingState GetTaskInitializingState() const;
	bool IsInitialized() const;

protected:
	Action(const string & mask, TaskBlockingState blockingState, TaskInitializingState initializingState);

private:
	void SetOwner(const SharedPtr<Entity> & owner);

	WeakPtr<Entity> _owner;
	WeakPtr<ActionList> _list;
	string _mask;
	TaskBlockingState _blockingState;
	TaskInitializingState _initializingState;
	bool _isInitialized;

	Action(const Action &) = delete;
	Action & operator =(const Action &) = delete;
};

//===========================================================================//

}// namespace Bru

#endif // ACTION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ActionList.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(ActionList, Updatable)

//===========================================================================//

ActionList::ActionList(const string & domainName, bool isEnabled) :
	Updatable(domainName, isEnabled),
	PtrFromThisMixIn<ActionList>(),
	_actions()
{
}

//===========================================================================//

ActionList::ActionList(const string & domainName, const SharedPtr<BaseDataProvider> & provider) :
	Updatable(domainName, provider),
	PtrFromThisMixIn<ActionList>(),
	_actions()	
{
}

//===========================================================================//

ActionList::~ActionList()
{
}

//===========================================================================//

void ActionList::Reset()
{
	Updatable::Reset();
	Clear();
}

//===========================================================================//

void ActionList::Update(float deltaTime)
{
	List<string> blockedMask;

	ListIterator<SharedPtr<Action>> it = _actions.CreateIterator();
	// щепотка магического порошка
	while (it.HasNext())
	{
		const SharedPtr<Action> & action = it.GetCurrent();
		it.Next();

		if (blockedMask.Contains(action->GetMask()))
		{
			action->Block();
			continue;
		}

		if (!action->IsInitialized())
		{
			action->Initialize();
		}

		bool isTaskCompleted = action->Update(deltaTime);

		if (action->GetBlockingState() == TaskBlockingState::Blocking)
		{
			blockedMask.Add(action->GetMask());
		}

		if (isTaskCompleted)
		{
			_actions.Remove(action);
		}
	}
}

//===========================================================================//

void ActionList::Add(const SharedPtr<Action> & action)
{
	action->SetOwner(GetOwner());
	action->_list = GetWeakPtr();
	_actions.Add(action);
}

//===========================================================================//

void ActionList::Push(const SharedPtr<Action> & action)
{
	action->SetOwner(GetOwner());
	action->_list = GetWeakPtr();
	_actions.Insert(0, action);
}

//===========================================================================//

void ActionList::Clear()
{
	_actions.Clear();
}

//===========================================================================//

bool ActionList::IsEmpty() const
{
	return _actions.IsEmpty();
}

//===========================================================================//

const List<SharedPtr<Action>> & ActionList::GetActions() const
{
	return _actions;
}

//===========================================================================//


} // namespace Bru

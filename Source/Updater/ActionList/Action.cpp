//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Action.h"

#include "ActionList.h"

namespace Bru
{

//===========================================================================//

Action::Action(const string & mask, TaskBlockingState blockingState, TaskInitializingState initializingState) :
	_owner(),
	_list(),
	_mask(mask),
	_blockingState(blockingState),
	_initializingState(initializingState),
	_isInitialized(false)
{
}

//===========================================================================//

Action::~Action()
{
}

//===========================================================================//

void Action::Initialize()
{
	_isInitialized = true;
}

//===========================================================================//

void Action::Block()
{
	if (_initializingState == TaskInitializingState::ReInitializing)
	{
		_isInitialized = false;
	}
}

//===========================================================================//

const WeakPtr<Entity> & Action::GetOwner() const
{
	return _owner;
}

//===========================================================================//

const WeakPtr<ActionList> & Action::GetList() const
{
	return _list;
}

//===========================================================================//

const string & Action::GetMask() const
{
	return _mask;
}

//===========================================================================//

TaskBlockingState Action::GetBlockingState() const
{
	return _blockingState;
}

//===========================================================================//

TaskInitializingState Action::GetTaskInitializingState() const
{
	return _initializingState;
}

//===========================================================================//

bool Action::IsInitialized() const
{
	return _isInitialized;
}

//===========================================================================//

void Action::SetOwner(const SharedPtr<Entity> & owner)
{
	_owner = owner;
}

//===========================================================================//

} // namespace Bru


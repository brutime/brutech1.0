//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовый класс, доля компонента, обновляемого Updater'ом
//

#pragma once
#ifndef UPDATABLE_H
#define UPDATABLE_H

#include "../EntitySystem/EntitySystem.h"

namespace Bru
{

//===========================================================================//

class Updatable : public EntityComponent
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	virtual ~Updatable();

	virtual void Reset();

	virtual void Update(float deltaTime) = 0;

	virtual void Enable();
	virtual void Disable();
	bool IsEnabled() const;

	void SetMarkForRemove(bool isMarkerForRemove);
	bool IsMarkedForRemove() const;

	const string & GetUpdatingLayerName() const;

protected:
	Updatable(const string & domainName, bool isEnabled = true);
	Updatable(const string & domainName, const string & updatingLayerName, bool isEnabled = true);
	Updatable(const string & domainName, const SharedPtr<BaseDataProvider> & provider);

	virtual void Register();
	virtual void Unregister();

private:
	static const string PathToIsEnabled;
	static const string PathToUpdatingGroupName;

	bool _isEnabled;
	bool _isEnabledOnInitialize;
	bool _isMarkerForRemove;

	string _updatingLayerName;

	Updatable(const Updatable &) = delete;
	Updatable & operator =(const Updatable &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // UPDATABLE_H

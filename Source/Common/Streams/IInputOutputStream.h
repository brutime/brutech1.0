//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс потока, преназначенного для чтения и записи
//

#pragma once
#ifndef I_INPUT_OUTPUT_STREAM_H
#define I_INPUT_OUTPUT_STREAM_H

#include "IInputStream.h"
#include "IOutputStream.h"

namespace Bru
{

//===========================================================================//

class IInputOutputStream : public IInputStream, public IOutputStream
{
public:
	virtual ~IInputOutputStream();

protected:
	IInputOutputStream();

private:
	IInputOutputStream(const IInputOutputStream &) = delete;
	IInputOutputStream & operator =(const IInputOutputStream &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // I_INPUT_OUTPUT_STREAM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс потока, преназначенного для чтения
//

#pragma once
#ifndef I_INPUT_STREAM_H
#define I_INPUT_STREAM_H

#include "../Types/BaseTypes.h"
#include "../Types/String/String.h"
#include "../Containers/Array/Array.h"
#include "Enums/SeekOrigin.h"

namespace Bru
{

//===========================================================================//

class IInputStream
{
public:
	virtual ~IInputStream();

	virtual byte ReadByte() = 0;
	virtual integer ReadInteger() = 0;
	virtual integer ReadInteger(integer bytesCount) = 0;
	virtual float ReadFloat() = 0;
	virtual float ReadFloat(integer bytesCount) = 0;
	virtual string ReadString(integer bytesCount) = 0;
	virtual string ReadLine() = 0;
	virtual Array<byte>::SimpleType ReadByteArray(integer count) = 0;
	virtual void ReadByteArray(byte * byteArray, integer count) = 0;

	virtual void Seek(integer offset, SeekOrigin seekOrigin) = 0;
	virtual void Reset() = 0;

	virtual void Close() = 0;

protected:
	IInputStream();

private:
	IInputStream(const IInputStream &) = delete;
	IInputStream & operator =(const IInputStream &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // I_INPUT_STREAM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "OSConsoleStream.h"

#include <iostream>
#include "../Types/BitConverter.h"

namespace Bru
{

//===========================================================================//

SharedPtr<OSConsoleStreamSingleton> OSConsoleStream;

//===========================================================================//

OSConsoleStreamSingleton::OSConsoleStreamSingleton()
{
}

//===========================================================================//

OSConsoleStreamSingleton::~OSConsoleStreamSingleton()
{
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(byte someByte)
{
	WriteValue<byte>(someByte);
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(byte someByte)
{
	Write(someByte);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(byte someByte, integer repeatCount)
{
#ifdef DEBUGGING
	if (repeatCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("repeatCount must be >= 0, but it is {0}", repeatCount));
	}
#endif

	for (integer i = 0; i < repeatCount; i++)
	{
		WriteValue<byte>(someByte);
	}
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(byte someByte, integer repeatCount)
{
	Write(someByte, repeatCount);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(integer someInteger)
{
	WriteValue<integer>(someInteger);
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(integer someInteger)
{
	Write(someInteger);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(integer someInteger, integer)
{
	WriteValue<integer>(someInteger);
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(integer someInteger, integer bytesCount)
{
	Write(someInteger, bytesCount);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(float someFloat)
{
	WriteValue<float>(someFloat);
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(float someFloat)
{
	Write(someFloat);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(float someFloat, integer)
{
	WriteValue<float>(someFloat);
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(float someFloat, integer bytesCount)
{
	Write(someFloat, bytesCount);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(const string & someString)
{
	WriteArray(someString.ToChars(), someString.GetLengthInBytes());
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(const string & someString)
{
	Write(someString);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(const Array<byte>::SimpleType & byteArray)
{
	WriteArray(reinterpret_cast<char *>(byteArray.GetData()), byteArray.GetCount());
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(const Array<byte>::SimpleType & byteArray)
{
	Write(byteArray);
	WriteEndOfLine();
}

//===========================================================================//

void OSConsoleStreamSingleton::Write(byte * const byteArray, integer count)
{
	WriteArray(reinterpret_cast<char *>(byteArray), count);
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteLine(byte * const byteArray, integer count)
{
	Write(byteArray, count);
	WriteEndOfLine();	
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteEndOfLine()
{
	std::cout << std::endl;
}

//===========================================================================//

void OSConsoleStreamSingleton::Flush()
{
}

//===========================================================================//

void OSConsoleStreamSingleton::Close()
{
}

//===========================================================================//

template<typename T>
void OSConsoleStreamSingleton::WriteValue(const T & value)
{
	std::cout << value;
}

//===========================================================================//

void OSConsoleStreamSingleton::WriteArray(const char * array, integer size)
{
	std::cout.write(array, size);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс потока, преназначенного для записи
//

#pragma once
#ifndef I_OUTPUT_STREAM_H
#define I_OUTPUT_STREAM_H

#include "../Types/BaseTypes.h"
#include "../Types/String/String.h"
#include "../Containers/Array/Array.h"

namespace Bru
{

//===========================================================================//

class IOutputStream
{
public:
	virtual ~IOutputStream();

	virtual void Write(byte someByte) = 0;
	virtual void WriteLine(byte someByte) = 0;

	virtual void Write(byte someByte, integer repeatCount) = 0;
	virtual void WriteLine(byte someByte, integer repeatCount) = 0;

	virtual void Write(integer someInteger) = 0;
	virtual void WriteLine(integer someInteger) = 0;

	virtual void Write(integer someInteger, integer bytesCount) = 0;
	virtual void WriteLine(integer someInteger, integer bytesCount) = 0;

	virtual void Write(float someFloat) = 0;
	virtual void WriteLine(float someFloat) = 0;

	virtual void Write(float someFloat, integer bytesCount) = 0;
	virtual void WriteLine(float someFloat, integer bytesCount) = 0;

	virtual void Write(const string & someString) = 0;
	virtual void WriteLine(const string & someString) = 0;

	virtual void Write(const Array<byte>::SimpleType & byteArray) = 0;
	virtual void WriteLine(const Array<byte>::SimpleType & byteArray) = 0;
	
	virtual void Write(byte * const byteArray, integer count) = 0;
	virtual void WriteLine(byte * const byteArray, integer count) = 0;	

	virtual void Flush() = 0;
	virtual void Close() = 0;

protected:
	IOutputStream();

private:
	IOutputStream(const IOutputStream &) = delete;
	IOutputStream & operator =(const IOutputStream &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // I_OUTPUT_STREAM_H

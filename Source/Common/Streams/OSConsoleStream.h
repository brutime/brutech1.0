//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Поток, предназначенный для вывода в консоль операционной системы
//

#pragma once
#ifndef OS_CONSOLE_STREAM_H
#define OS_CONSOLE_STREAM_H

#include "IOutputStream.h"
#include "../Types/SharedPtr.h"

namespace Bru
{

//===========================================================================//

class OSConsoleStreamSingleton : public IOutputStream
{
public:
	OSConsoleStreamSingleton();
	virtual ~OSConsoleStreamSingleton();

	virtual void Write(byte someByte);
	virtual void WriteLine(byte someByte);

	virtual void Write(byte someByte, integer repeatCount);
	virtual void WriteLine(byte someByte, integer repeatCount);

	virtual void Write(integer someInteger);
	virtual void WriteLine(integer someInteger);

	virtual void Write(integer someInteger, integer bytesCount);
	virtual void WriteLine(integer someInteger, integer bytesCount);

	virtual void Write(float someFloat);
	virtual void WriteLine(float someFloat);

	virtual void Write(float someFloat, integer bytesCount);
	virtual void WriteLine(float someFloat, integer bytesCount);

	virtual void Write(const string & someString);
	virtual void WriteLine(const string & someString);

	virtual void Write(const Array<byte>::SimpleType & byteArray);
	virtual void WriteLine(const Array<byte>::SimpleType & byteArray);
	
	virtual void Write(byte * const byteArray, integer count);
	virtual void WriteLine(byte * const byteArray, integer count);

	virtual void WriteEndOfLine();

	virtual void Flush();
	virtual void Close();

private:
	template<typename T> void WriteValue(const T & value);
	void WriteArray(const char * array, integer bytesCount);

	OSConsoleStreamSingleton(const OSConsoleStreamSingleton &) = delete;
	OSConsoleStreamSingleton & operator =(const OSConsoleStreamSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<OSConsoleStreamSingleton> OSConsoleStream;

//===========================================================================//

} // namespace Bru

#endif // OS_CONSOLE_STREAM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ��� ����������� ������� �� ������
//

#pragma once
#ifndef SEEK_ORIGIN_H
#define SEEK_ORIGIN_H

namespace Bru
{

//===========================================================================//

enum class SeekOrigin
{
    ///�������� �� ������ ������
    Begin,
    ///�������� �� ������� ������� ������� � ������
    Current,
    ///�������� �� ����� ������
    End
};

//===========================================================================//

}
#endif // SEEK_POSITION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef COMPARE_RESULT_H
#define COMPARE_RESULT_H

namespace Bru
{

//===========================================================================//

enum class ComapareResult
{
    Less = -1,
    Equals = 0,
    Greater = 1
};

//===========================================================================//

}//namespace

#endif // COMPARE_RESULT_H

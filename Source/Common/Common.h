//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Хедер подключает основные типы и классы
//

#pragma once
#ifndef COMMON_H
#define COMMON_H

#include "Types/BaseTypes.h"
#include "Types/Nullable.h"
#include "Types/ScopedPtr.h"
#include "Types/SharedPtr.h"
#include "Types/WeakPtr.h"
#include "Types/PtrFromThisMixIn.h"
#include "Types/Converter.h"
#include "Types/Convert.h"
#include "Types/BitConverter.h"
#include "Types/String/String.h"
#include "Types/String/UTF16String.h"
#include "Types/String/UTF32String.h"
#include "Types/String/StringBuilder.h"

#include "Containers/KeyValuePair.h"
#include "Containers/ValuePair.h"

#include "Containers/AddingOrderedTreeMap/AddingOrderedTreeMap.h"
#include "Containers/Array/Array.h"
#include "Containers/List/List.h"
#include "Containers/Stack/Stack.h"
#include "Containers/TreeMap/TreeMap.h"

#include "Streams/IInputStream.h"
#include "Streams/IOutputStream.h"
#include "Streams/IInputOutputStream.h"
#include "Streams/OSConsoleStream.h"

#include "EventSystem/Event.h"
#include "EventSystem/EventHandler.h"

#include "Helpers/SingletonMixIn.h"
#include "Helpers/Memory.h"
#include "Helpers/TokenParser.h"

#include "Exceptions/ExceptionRegistrator.h"
#include "Exceptions/Exception.h"
#include "Exceptions/IndexOutOfBoundsException.h"
#include "Exceptions/InitializationException.h"
#include "Exceptions/InvalidArgumentException.h"
#include "Exceptions/InvalidFormatException.h"
#include "Exceptions/InvalidOperationException.h"
#include "Exceptions/InvalidStateException.h"
#include "Exceptions/InvalidUTF16SequenceException.h"
#include "Exceptions/InvalidUTF32SequenceException.h"
#include "Exceptions/InvalidUTF8SequenceException.h"
#include "Exceptions/NotImplementedException.h"
#include "Exceptions/NotMeetMinimalRequirementsException.h"
#include "Exceptions/NullPointerException.h"
#include "Exceptions/OverflowException.h"
#include "Exceptions/ParsingException.h"
#include "Exceptions/ShutDownException.h"

#endif // COMMON_H

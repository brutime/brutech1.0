//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Итератор для списка
//

#pragma once
#ifndef LIST_ITERATOR_H
#define LIST_ITERATOR_H

#include "../../Types/BaseTypes.h"
#include "../../Types/String/String.h"
#include "../../Exceptions/IteratorOutOfBoundsException.h"
#include "ListElement.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class List;

//===========================================================================//

template<typename T>
class ListIterator
{
	friend class List<T> ;

public:
	ListIterator(const ListIterator<T> & other);
	virtual ~ListIterator();

	ListIterator<T> & operator =(const ListIterator<T> & other);

	bool operator !=(const ListIterator<T> & someIterator) const;
	const ListIterator<T> & operator ++();
	T & operator *() const;

	void First();
	bool HasNext() const;
	void Next();

	T & GetCurrent() const;

private:
	ListIterator(const List<T> * owner, ListElement<T> * cursor);

	const List<T> * _owner;
	ListElement<T> * _cursor;

	//Для тестирования
	friend class TestListIteratorConstructor;
	friend class TestListIteratorForEmpty;
	friend class TestListIteratorForOneElement;
	friend class TestListIteratorFor;
	friend class TestListIteratorRangeForEmpty;
	friend class TestListIteratorRangeForOneElement;
	friend class TestListIteratorRangeFor;
	friend class TestListIteratorExceptions;

	friend class TestListCreateIterator;
	friend class TestListBegin;
	friend class TestListEnd;
};

//===========================================================================//

}// namespace

#include "ListIterator.inl"

#endif // LIST_ITERATOR_H

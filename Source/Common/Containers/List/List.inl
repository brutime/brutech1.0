//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
List<T>::List() :
	_count(0),
	_head(NullPtr),
	_tail(NullPtr)
{
}

//===========================================================================//

template<typename T>
List<T>::List(std::initializer_list<T> list) :
	_count(0),
	_head(NullPtr),
	_tail(NullPtr)
{
	for (const T & element : list)
	{
		Add(element);
	}
}

//===========================================================================//

template<typename T>
List<T>::List(const List<T> & other) :
	_count(0),
	_head(NullPtr),
	_tail(NullPtr)
{
	DeepCopy(other);
}

//===========================================================================//

template<typename T>
List<T>::List(List<T> && other):
	_count(other._count),
	_head(other._head),
	_tail(other._tail)
{
	other._count = 0;
	other._head = NullPtr;
	other._tail = NullPtr;
}

//===========================================================================//

template<typename T>
List<T>::~List()
{
	Clear();
}

//===========================================================================//

template<typename T>
List<T> & List<T>::operator =(const List<T> & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::operator =(List<T> && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_count, other._count);
	std::swap(_head, other._head);
	std::swap(_tail, other._tail);

	return *this;
}

//===========================================================================//

template<typename T>
bool List<T>::operator ==(const List<T> & someList) const
{
	if (_count != someList._count)
	{
		return false;
	}

	ListElement<T> * thisListElement = _head;
	ListElement<T> * comparingListElement = someList._head;
	while (thisListElement != NullPtr)
	{
		if (thisListElement->Value != comparingListElement->Value)
		{
			return false;
		}
		thisListElement = thisListElement->Next;
		comparingListElement = comparingListElement->Next;
	}

	return true;
}

//===========================================================================//

template<typename T>
bool List<T>::operator !=(const List<T> & someList) const
{
	return !(*this == someList);
}

//===========================================================================//

template<typename T>
List<T> & List<T>::Add(const T & element)
{
	if (_count == 0)
	{
		_head = new ListElement<T>(element, NullPtr);
		_tail = _head;
	}
	else
	{
		ListElement<T> * newElement = new ListElement<T>(element, NullPtr);
		_tail->Next = newElement;
		_tail = newElement;
	}
	_count++;

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::Add(T && element)
{
	if (_count == 0)
	{
		_head = new ListElement<T>(std::move(element), NullPtr);
		_tail = _head;
	}
	else
	{
		ListElement<T> * newElement = new ListElement<T>(std::move(element), NullPtr);
		_tail->Next = newElement;
		_tail = newElement;
	}
	_count++;

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::Add(const List<T> & other)
{
#ifdef DEBUGGING
	if (this == &other)
	{
		INVALID_ARGUMENT_EXCEPTION("Self adding");
	}
#endif

	if (other._count == 0)
	{
		return *this;
	}

	ListElement<T> * tmp = other._head;
	while (tmp != NullPtr)
	{
		Add(tmp->Value);
		tmp = tmp->Next;
	}

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::Add(const Array<T> & array)
{
	if (array.GetCount() == 0)
	{
		return *this;
	}

	for (integer i = 0; i < array.GetCount(); ++i)
	{
		Add(array[i]);
	}

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::Insert(integer index, const T & element)
{
#ifdef DEBUGGING
	if (index < 0 || index > _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count));
	}
#endif

	if (index == _count)
	{
		Add(element);
		return *this;
	}

	ListElement<T> * tmp = _head;
	ListElement<T> * previous = NullPtr;
	ListElement<T> * newElement = new ListElement<T>(element, NullPtr);

	//newElement->value = element;
	for (integer i = 0; i < index; i++)
	{
		previous = tmp;
		tmp = tmp->Next;
	}

	if (previous != NullPtr)
	{
		previous->Next = newElement;
	}
	else
	{
		_head = newElement;
	}

	newElement->Next = tmp;
	_count++;

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::Insert(integer index, T && element)
{
#ifdef DEBUGGING
	if (index < 0 || index > _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count));
	}
#endif

	if (index == _count)
	{
		Add(std::move(element));
		return *this;
	}

	ListElement<T> * tmp = _head;
	ListElement<T> * previous = NullPtr;
	ListElement<T> * newElement = new ListElement<T>(std::move(element), NullPtr);

	//newElement->value = element;
	for (integer i = 0; i < index; i++)
	{
		previous = tmp;
		tmp = tmp->Next;
	}

	if (previous != NullPtr)
	{
		previous->Next = newElement;
	}
	else
	{
		_head = newElement;
	}

	newElement->Next = tmp;
	_count++;

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::Remove(const T & element)
{
	if (!Contains(element))
	{
		INVALID_ARGUMENT_EXCEPTION("Requested element not found");
	}

	return RemoveIfExists(element);
}

//===========================================================================//

template <typename T>
List<T> & List<T>::RemoveIfExists(const T & element)
{
	ListElement<T> * tmp = _head;
	ListElement<T> * previous = NullPtr;
	while (tmp != NullPtr)
	{
		if (tmp->Value == element)
		{
			if (previous == NullPtr)
			{
				_head = tmp->Next;
			}
			else
			{
				if (tmp == _tail)
				{
					_tail = previous;
				}
				previous->Next = tmp->Next;
			}

			delete tmp;
			_count--;
			if (_count == 0)
			{
				_head = _tail = NullPtr;
			}

			return *this;
		}

		previous = tmp;
		tmp = tmp->Next;
	}

	return *this;
}

//===========================================================================//

template <typename T>
List<T> & List<T>::Remove(std::function<bool(const T &)> predicate)
{
	ListElement<T> * tmp = _head;
	ListElement<T> * previous = NullPtr;
	while (tmp != NullPtr)
	{
		if (predicate(tmp->Value))
		{
			if (previous == NullPtr)
			{
				_head = _head->Next;
				delete tmp;
				tmp = _head;
			}
			else
			{
				if (tmp == _tail)
				{
					_tail = previous;
				}
				previous->Next = tmp->Next;
				delete tmp;
				tmp = previous->Next;
			}

			_count--;
		}
		else
		{
			previous = tmp;
			tmp = tmp->Next;
		}
	}

	if (_count == 0)
	{
		_head = _tail = NullPtr;
	}

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::RemoveAt(integer index)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	Remove(Get(index));

	return *this;
}

//===========================================================================//

template<typename T>
List<T> & List<T>::RemoveAllStartingAt(integer index)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	if (index == 0)
	{
		Clear();
	}

	ListElement<T> * tmp = _head;
	ListElement<T> * previous = NullPtr;
	for (integer count = index - 1; count > 0; count--)
	{
		tmp = tmp->Next;
	}
	_tail = tmp;

	tmp = tmp->Next;
	while (tmp != NullPtr)
	{
		previous = tmp;
		tmp = tmp->Next;
		delete previous;
	}

	_tail->Next = NullPtr;
	_count = index;

	return *this;
}

//===========================================================================//

template<typename T>
void List<T>::Reverse()
{
	ListElement<T> * current = _head;
	_tail = _head;
	_head = NullPtr;
	while (current != NullPtr)
	{
		ListElement<T> * tmp = current;
		current = current->Next;
		tmp->Next = _head;
		_head = tmp;
	}

	_tail->Next = NullPtr;
}

//===========================================================================//

template<typename T>
T & List<T>::operator [](integer index)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	ListElement<T> * tmp = _head;
	for (integer i = 0; i < index; i++)
	{
		tmp = tmp->Next;
	}

	return tmp->Value;
}

//===========================================================================//

template<typename T>
const T & List<T>::operator [](integer index) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	ListElement<T> * tmp = _head;
	for (integer i = 0; i < index; i++)
	{
		tmp = tmp->Next;
	}

	return tmp->Value;
}

//===========================================================================//

template<typename T>
T & List<T>::GetFirst() const
{
#ifdef DEBUGGING
	if (_count == 0)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION("List is empty");
	}
#endif

	return _head->Value;
}

//===========================================================================//

template<typename T>
T & List<T>::GetLast() const
{
#ifdef DEBUGGING
	if (_count == 0)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION("List is empty");
	}
#endif

	return _tail->Value;
}

//===========================================================================//

template<typename T>
bool List<T>::Contains(const T & element) const
{
	ListElement<T> * tmp = _head;
	while (tmp != NullPtr)
	{
		if (tmp->Value == element)
		{
			return true;
		}
		tmp = tmp->Next;
	}

	return false;
}

//===========================================================================//

template <typename T>
bool List<T>::Contains(std::function<bool(const T &)> predicate) const
{
	ListElement<T> * tmp = _head;
	while (tmp != NullPtr)
	{
		if (predicate(tmp->Value))
		{
			return true;
		}
		tmp = tmp->Next;
	}

	return false;
}

//===========================================================================//

template<typename T>
integer List<T>::IndexOf(const T & element) const
{
	integer currentIndex = 0;
	ListElement<T> * tmp = _head;
	while (tmp != NullPtr)
	{
		if (tmp->Value == element)
		{
			return currentIndex;
		}
		currentIndex++;
		tmp = tmp->Next;
	}

	return -1;
}

//===========================================================================//

template<typename T>
void List<T>::Sort(const IComparator<T> & sortingComparator)
{
	ListElement<T> * newHead = NullPtr;
	ListElement<T> * newTail = NullPtr;
	ListElement<T> * tmp = _head;
	ListElement<T> * previous = NullPtr;
	ListElement<T> * min;
	ListElement<T> * beforeMin;

	for (integer i = 0; i < _count; i++)
	{
		tmp = _head->Next;
		previous = _head;
		beforeMin = NullPtr;
		min = _head;

		while (tmp != NullPtr)
		{
			if (sortingComparator.Compare(tmp->Value, min->Value))
			{
				min = tmp;
				beforeMin = previous;
			}
			previous = tmp;

			tmp = tmp->Next;
		}

		if (beforeMin != NullPtr)
		{
			beforeMin->Next = min->Next;
		}
		else
		{
			_head = min->Next;
		}

		if (newHead == NullPtr)
		{
			newHead = newTail = min;
		}
		else
		{
			newTail->Next = min;
			newTail = min;
		}
	}

	_head = newHead;
	_tail = newTail;
}

//===========================================================================//

template<typename T>
void List<T>::FillFromArray(const Array<T> & SourceArray)
{
	Clear();

	for (integer i = 0; i < SourceArray.GetCount(); i++)
	{
		Add(SourceArray[i]);
	}
}

//===========================================================================//

template<typename T>
Array<T> List<T>::ToArray() const
{
	Array<T> result(_count);

	ListElement<T> * tmp = _head;
	for (integer i = 0; tmp != NullPtr; i++, tmp = tmp->Next)
	{
		result[i] = tmp->Value;
	}

	return result;
}

//===========================================================================//

template<typename T>
void List<T>::Clear()
{
	ListElement<T> * tmp;
	while (_head != NullPtr)
	{
		tmp = _head->Next;
		delete _head;
		_head = tmp;
	}

	_count = 0;
	_head = _tail = NullPtr;
}

//===========================================================================//

template<typename T>
bool List<T>::IsEmpty() const
{
	return _count == 0;
}

//===========================================================================//

template<typename T>
integer List<T>::GetCount() const
{
	return _count;
}

//===========================================================================//

template<typename T>
ListIterator<T> List<T>::CreateIterator() const
{
	return begin();
}

//===========================================================================//

template<typename T>
ListIterator<T> List<T>::begin() const
{
	return ListIterator<T>(this, _head);
}

//===========================================================================//

template<typename T>
ListIterator<T> List<T>::end() const
{
	return ListIterator<T>(this, NullPtr);
}

//===========================================================================//

template<typename T>
T & List<T>::Get(integer index)
{
	return this->operator[](index);
}

//===========================================================================//

template<typename T>
void List<T>::DeepCopy(const List<T> & other)
{
	_count = other._count;
	_head = _tail = NullPtr;

	if (_count > 0)
	{
		ListElement<T> * tmpOther = other._head->Next;
		ListElement<T> * tmpNew = NullPtr;
		_head = new ListElement<T>(other._head->Value, NullPtr);
		_tail = _head;

		while (tmpOther != NullPtr)
		{
			tmpNew = new ListElement<T>(tmpOther->Value, NullPtr);
			_tail->Next = tmpNew;
			_tail = tmpNew;
			tmpOther = tmpOther->Next;
		}
	}
}

//===========================================================================//


} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
ListElement<T>::ListElement() :
	Value(),
	Next(NullPtr)
{
}

//===========================================================================//

template<typename T>
ListElement<T>::ListElement(const T & value, ListElement<T> * next) :
	Value(value),
	Next(next)
{
}

//===========================================================================//

template<typename T>
ListElement<T>::ListElement(T && value, ListElement<T> * next):
	Value(std::move(value)),
	Next(next)
{
}

//===========================================================================//

template<typename T>
ListElement<T>::~ListElement()
{
}

//===========================================================================//

}// namespace Bru

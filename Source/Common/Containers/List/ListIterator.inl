//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
ListIterator<T>::ListIterator(const List<T> * owner, ListElement<T> * cursor) :
	_owner(owner),
	_cursor(cursor)
{
#ifdef DEBUGGING
	if (owner == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("owner == NullPtr");
	}
#endif
}

//===========================================================================//

template<typename T>
ListIterator<T>::ListIterator(const ListIterator<T> & other) :
	_owner(other._owner),
	_cursor(other._cursor)
{
}

//===========================================================================//

template<typename T>
ListIterator<T>::~ListIterator()
{
}

//===========================================================================//

template<typename T>
ListIterator<T> & ListIterator<T>::operator =(const ListIterator<T> & other)
{
	if (this == &other)
	{
		return *this;
	}

	_owner = other._owner;
	_cursor = other._cursor;

	return *this;
}

//===========================================================================//

template<typename T>
bool ListIterator<T>::operator !=(const ListIterator<T> & someIterator) const
{
	return _owner != someIterator._owner || _cursor != someIterator._cursor;
}

//===========================================================================//

template<typename T>
const ListIterator<T> & ListIterator<T>::operator ++()
{
	Next();
	return *this;
}

//===========================================================================//

template<typename T>
T & ListIterator<T>::operator *() const
{
	return GetCurrent();
}

//===========================================================================//

template<typename T>
void ListIterator<T>::First()
{
	_cursor = _owner->_head;
}

//===========================================================================//

template<typename T>
bool ListIterator<T>::HasNext() const
{
	return _cursor != NullPtr;
}

//===========================================================================//

template<typename T>
void ListIterator<T>::Next()
{
#ifdef DEBUGGING
	if (!HasNext())
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION("Can't read beyond the end of list");
	}
#endif

	_cursor = _cursor->Next;
}

//===========================================================================//

template<typename T>
T & ListIterator<T>::GetCurrent() const
{
#ifdef DEBUGGING
	if (_cursor == NullPtr)
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION("Iterator's _cursor == NullPtr");
	}
#endif

	return _cursor->Value;
}

//===========================================================================//

}// namespace

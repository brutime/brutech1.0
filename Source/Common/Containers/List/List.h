//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Односвязный списк
//

#pragma once
#ifndef LIST_H
#define LIST_H

#include "../../Types/BaseTypes.h"
#include "../../Types/String/String.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/IndexOutOfBoundsException.h"
#include "../BaseClasses/AscendingComparator.h"
#include "../BaseClasses/DescendingComparator.h"
#include "../Array/Array.h"
#include "ListElement.h"
#include "ListIterator.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class List
{
	friend class ListIterator<T>;

public:
	List();
	List(std::initializer_list<T> list);
	List(const List<T> & other);
	List(List<T> && other);
	virtual ~List();

	List<T> & operator =(const List<T> & other);
	List<T> & operator =(List<T> && other);

	bool operator ==(const List<T> & someList) const;
	bool operator !=(const List<T> & someList) const;

	///Добавление элемента в конец списка
	List<T> & Add(const T & element);
	List<T> & Add(T && element);

	///Добавление элементов другого списка
	List<T> & Add(const List<T> & other);
	
	///Добавление элементов массива
	List<T> & Add(const Array<T> & array);

	///Вставка элемента в index-ю позицию
	List<T> & Insert(integer index, const T & element);
	List<T> & Insert(integer index, T && element);

	///Удаление элемента
	List<T> & Remove(const T & element);
	List<T> & Remove(std::function<bool(const T &)> predicate);
	
	///Удаляет элемент, но не выбрасывает исключения если его нет в коллекции
	List<T> & RemoveIfExists(const T & element);

	///Удаление элемента по индексу
	List<T> & RemoveAt(integer index);

	///Удаление всех элементов после указанного индекса
	List<T> & RemoveAllStartingAt(integer index);

	void Reverse();

	///обращение к элементу по индексу (для записи)
	T & operator [](integer index);
	///обращение к элементу по индексу (для чтения)
	const T & operator [](integer index) const;

	///Получение первого элемента
	T & GetFirst() const;

	///Получение последнего элемента
	T & GetLast() const;

	///Метод возвращает истинну, если указанный элемент присутствует в списке
	bool Contains(const T & element) const;
	bool Contains(std::function<bool(const T &)> predicate) const;

	///Получение индекса элемента
	///Метод возвращает -1, если искомый элемент не найден
	integer IndexOf(const T & element) const;

	void Sort(const IComparator<T> & SortingComparator = AscendingComparator<T>());

	//Очищается список, и заполняет его данными массива
	void FillFromArray(const Array<T> & SourceArray);

	Array<T> ToArray() const;

	///Очистка списка
	void Clear();

	///Пустой-ли список
	bool IsEmpty() const;
	integer GetCount() const;

	ListIterator<T> CreateIterator() const;
	ListIterator<T> begin() const;
	ListIterator<T> end() const;

private:
	friend class Array<T>;

	T & Get(integer index);
	void DeepCopy(const List<T> & otherList);

	integer _count;
	ListElement<T> * _head;
	ListElement<T> * _tail;

	//для тестирования
	friend class TestListGetters;
	friend class TestListDefaultConstructor;
	friend class TestListRemove;
	friend class TestListRemoveAt;
	friend class TestListFillFromEmptyArray;
	friend class TestListClear;
	friend class TestListCreateIterator;
	friend class TestListBegin;
	friend class TestListEnd;

	friend class TestListIteratorConstructor;
	friend class TestListIteratorForOneElement;
	friend class TestListIteratorFor;
	friend class TestListIteratorExceptions;
};

//===========================================================================//

} // namespace Bru

#include "List.inl"

#endif // LIST_H

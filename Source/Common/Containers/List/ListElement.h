//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Элемент списка
//

#pragma once
#ifndef LIST_ELEMENT_H
#define LIST_ELEMENT_H

#include "../../Types/BaseTypes.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class ListElement
{
public:
	ListElement();
	ListElement(const T & value, ListElement<T> * next);
	ListElement(T && value, ListElement<T> * next);
	~ListElement();

	T Value;
	ListElement * Next;

private:
	ListElement(const ListElement<T> & other) = delete;
	ListElement<T> & operator =(const ListElement<T> & other) = delete;
};

//===========================================================================//

}//namespace

#include "ListElement.inl"

#endif // LIST_ELEMENT_H

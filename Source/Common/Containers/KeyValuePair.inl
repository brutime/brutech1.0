//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

//===========================================================================//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V>::KeyValuePair(const K & key, const V & value) :
	_key(key),
	_value(value)
{
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V>::KeyValuePair(K && key, const V & value) :
	_key(std::move(key)),
	_value(value)
{

}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V>::KeyValuePair(const K & key, V && value) :
	_key(key),
	_value(std::move(value))
{

}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V>::KeyValuePair(K && key, V && value) :
	_key(std::move(key)),
	_value(std::move(value))
{
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V>::KeyValuePair(const KeyValuePair<K, V> & other) :
	_key(other._key),
	_value(other._value)
{
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V>::KeyValuePair(KeyValuePair<K, V> && other) :
	_key(std::move(other._key)),
	_value(std::move(other._value))
{
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V>::~KeyValuePair()
{
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V> & KeyValuePair<K, V>::operator =(const KeyValuePair<K, V> & other)
{
	if (this == &other)
	{
		return *this;
	}

	_key = other._key;
	_value = other._value;

	return *this;
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V> & KeyValuePair<K, V>::operator =(KeyValuePair<K, V> && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_key, other._key);
	std::swap(_value, other._value);

	return *this;
}

//===========================================================================//

template<typename K, typename V>
bool KeyValuePair<K, V>::operator ==(const KeyValuePair<K, V> & keyValuePair) const
{
	return _key == keyValuePair._key;
}

//===========================================================================//

template<typename K, typename V>
bool KeyValuePair<K, V>::operator !=(const KeyValuePair<K, V> & keyValuePair) const
{
	return !(*this == keyValuePair);
}

//===========================================================================//

template<typename K, typename V>
bool KeyValuePair<K, V>::operator <(const KeyValuePair<K, V> & keyValuePair) const
{
	return _key < keyValuePair._key;
}

//===========================================================================//

template<typename K, typename V>
bool KeyValuePair<K, V>::operator >(const KeyValuePair<K, V> & keyValuePair) const
{
	return _key > keyValuePair._key;
}

//===========================================================================//

template<typename K, typename V>
const K & KeyValuePair<K, V>::GetKey() const
{
	return _key;
}

//===========================================================================//

template<typename K, typename V>
V & KeyValuePair<K, V>::GetValue()
{
	return _value;
}

//===========================================================================//

template<typename K, typename V>
const V & KeyValuePair<K, V>::GetValue() const
{
	return _value;
}

//===========================================================================//

} // namespace Bru

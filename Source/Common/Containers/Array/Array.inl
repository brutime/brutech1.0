//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm>::Array(integer count) :
	_count(0),
	_reservedCount(0),
	_dataPtr(NullPtr)
{
	if (count != 0)
	{
		Resize(count);
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm>::Array(std::initializer_list<T> list) :
	_count(0),
	_reservedCount(0),
	_dataPtr(NullPtr)
{
	if (list.size() == 0)
	{
		return;
	}

	Resize(list.size());
	integer index = 0;
	for (const T & element : list)
	{
		_dataPtr[index] = element;
		index++;
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm>::Array(const Array<T, MoveElementsAlgorithm> & other) :
	_count(0),
	_reservedCount(0),
	_dataPtr(NullPtr)
{
	DeepCopy(other);
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm>::Array(Array<T, MoveElementsAlgorithm> && other) :
	_count(other._count),
	_reservedCount(other._reservedCount),
	_dataPtr(other._dataPtr)
{
	other._count = 0;
	other._reservedCount = 0;
	other._dataPtr = NullPtr;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm>::~Array()
{
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm> & Array<T, MoveElementsAlgorithm>::operator =(
    const Array<T, MoveElementsAlgorithm> & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm> & Array<T, MoveElementsAlgorithm>::operator =(Array<T, MoveElementsAlgorithm> && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_count, other._count);
	std::swap(_reservedCount, other._reservedCount);
	std::swap(_dataPtr, other._dataPtr);

	return *this;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
bool Array<T, MoveElementsAlgorithm>::operator ==(const Array<T, MoveElementsAlgorithm> & someArray) const
{
	if (_count != someArray._count)
	{
		return false;
	}

	for (integer i = 0; i < _count; ++i)
	{
		if (_dataPtr[i] != someArray[i])
		{
			return false;
		}
	}

	return true;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
bool Array<T, MoveElementsAlgorithm>::operator !=(const Array<T, MoveElementsAlgorithm> & someArray) const
{
	return !(*this == someArray);
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Resize(integer count)
{
	if (count == 0)
	{
		Clear();
		return;
	}

	ChangeReversedCount(count);
	_count = count;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm> & Array<T, MoveElementsAlgorithm>::Add(const T & element)
{
	if (_count == _reservedCount)
	{
		integer deltaGrowCount = GetDeltaGrowCount();
		ChangeReversedCount(_reservedCount + deltaGrowCount);
	}

	_dataPtr[_count] = element;
	_count++;

	return *this;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm> & Array<T, MoveElementsAlgorithm>::Add(T && element)
{
	if (_count == _reservedCount)
	{
		integer deltaGrowCount = GetDeltaGrowCount();
		ChangeReversedCount(_reservedCount + deltaGrowCount);
	}

	_dataPtr[_count] = std::move(element);
	_count++;

	return *this;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm> & Array<T, MoveElementsAlgorithm>::Add(const Array<T, MoveElementsAlgorithm> & other)
{
#ifdef DEBUGGING
	if (this == &other)
	{
		INVALID_ARGUMENT_EXCEPTION("Self adding");
	}
#endif

	if (other._count == 0)
	{
		return *this;
	}

	if (_count + other._count > _reservedCount)
	{
		integer deltaGrowCount = GetBatchDeltaGrowCount(other._count);
		ChangeReversedCount(_reservedCount + deltaGrowCount);
	}

	MoveElementsAlgorithm<T>::Copy(_dataPtr, _count, other._dataPtr, 0, other._count);
	_count += other._count;

	return *this;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm> & Array<T, MoveElementsAlgorithm>::Add(const List<T> & list)
{
	if (list._count == 0)
	{
		return *this;
	}

	if (_count + list._count > _reservedCount)
	{
		integer deltaGrowCount = GetBatchDeltaGrowCount(list._count);
		ChangeReversedCount(_reservedCount + deltaGrowCount);
	}

	ListElement<T> * tmp = list._head;
	for (integer i = _count; i < _count + list._count; ++i)
	{
		_dataPtr[i] = tmp->Value;
		tmp = tmp->Next;
	}
	_count += list._count;

	return *this;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Insert(integer index, const T & element)
{
#ifdef DEBUGGING
	if (index < 0 || index > _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _count));
	}
#endif

	if (_count == _reservedCount)
	{
		integer deltaGrowCount = GetDeltaGrowCount();
		ChangeReversedCount(_reservedCount + deltaGrowCount);
	}

	MoveElementsAlgorithm<T>::MoveForward(_dataPtr, index, 1, _count);
	_dataPtr[index] = element;
	_count++;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Insert(integer index, T && element)
{
#ifdef DEBUGGING
	if (index < 0 || index > _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _count));
	}
#endif

	if (_count == _reservedCount)
	{
		integer deltaGrowCount = GetDeltaGrowCount();
		ChangeReversedCount(_reservedCount + deltaGrowCount);
	}

	MoveElementsAlgorithm<T>::MoveForward(_dataPtr, index, 1, _count);
	_dataPtr[index] = std::move(element);
	_count++;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Insert(integer index, const Array<T, MoveElementsAlgorithm> & other)
{
#ifdef DEBUGGING
	if (index < 0 || index > _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count));
	}
#endif

	if (other._count == 0)
	{
		return;
	}

	if (this == &other)
	{
		INVALID_ARGUMENT_EXCEPTION("Self inserting");
	}

	if (_count + other._count > _reservedCount)
	{
		integer deltaGrowCount = GetBatchDeltaGrowCount(other._count);
		ChangeReversedCount(_reservedCount + deltaGrowCount);
	}

	MoveElementsAlgorithm<T>::MoveForward(_dataPtr, index, other._count, _count);
	MoveElementsAlgorithm<T>::Copy(_dataPtr, index, other._dataPtr, 0, other._count);
	_count += other._count;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Remove(const T & element)
{
	if (!Contains(element))
	{
		INVALID_ARGUMENT_EXCEPTION("Requested element not found");
	}

	RemoveAt(IndexOf(element));
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Remove(std::function<bool(const T & element)> predicate)
{
	//решение неэффективное, надо переделать при случае
	integer i = 0;
	while (i < _count)
	{
		if (predicate(_dataPtr[i]))
		{
			RemoveAt(i);
		}
		else
		{
			i++;
		}
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::RemoveAt(integer index)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	if (_count == 1)
	{
		Clear();
		return;
	}

	MoveElementsAlgorithm<T>::MoveBackward(_dataPtr, index, 1, _count);

	_count--;

	integer decreaseCount = GetDeltaDecreaseCount();
	if (_count < decreaseCount)
	{
		ChangeReversedCount(decreaseCount);
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::RemoveAt(integer index, integer elementsCount)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}

	if (elementsCount < 0 || index + elementsCount > _count)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Count out of bounds: {0} (index: {1}) ", _count, index));
	}
#endif

	if (_count == 1)
	{
		Clear();
		return;
	}

	MoveElementsAlgorithm<T>::MoveBackward(_dataPtr, index, elementsCount, _count);

	_count -= elementsCount;

	integer decreaseCount = GetDeltaDecreaseCount();
	if (_count < decreaseCount)
	{
		ChangeReversedCount(decreaseCount);
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
T & Array<T, MoveElementsAlgorithm>::operator [](integer index)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	return _dataPtr[index];
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
const T & Array<T, MoveElementsAlgorithm>::operator [](integer index) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	return _dataPtr[index];
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
T & Array<T, MoveElementsAlgorithm>::GetFirst() const
{
#ifdef DEBUGGING
	if (_count == 0)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION("Array is empty");
	}
#endif

	return _dataPtr[0];
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
T & Array<T, MoveElementsAlgorithm>::GetLast() const
{
#ifdef DEBUGGING
	if (_count == 0)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION("Array is empty");
	}
#endif

	return _dataPtr[_count - 1];
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::SetArray(integer index, const Array<T, MoveElementsAlgorithm> & other)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}

	if (this == &other)
	{
		INVALID_ARGUMENT_EXCEPTION("Self setting");
	}

	if (index + other._count > _count)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("index + other array _count out of bound: {0} ([0..{1}])",
		                                          index + other._count, _count));
	}
#endif

	if (other._count == 0)
	{
		return;
	}

	MoveElementsAlgorithm<T>::Copy(_dataPtr, index, other._dataPtr, 0, other._count);
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
Array<T, MoveElementsAlgorithm> Array<T, MoveElementsAlgorithm>::GetArray(integer index, integer elementsCount) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}

	if (elementsCount < 0 || index + elementsCount > _count)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("elementsCount out of bounds: {0} (index: {1}) ", _count, index));
	}
#endif

	Array<T, MoveElementsAlgorithm> subArray(elementsCount);

	if (elementsCount > 0)
	{
		MoveElementsAlgorithm<T>::Copy(subArray._dataPtr, 0, _dataPtr, index, elementsCount);
	}

	return subArray;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
bool Array<T, MoveElementsAlgorithm>::Contains(const T & element) const
{
	return IndexOf(element) != -1;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
bool Array<T, MoveElementsAlgorithm>::Contains(std::function<bool(const T &)> predicate) const
{
	for (integer i = 0; i < _count; i++)
	{
		if (predicate(_dataPtr[i]))
		{
			return true;
		}
	}

	return false;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
integer Array<T, MoveElementsAlgorithm>::IndexOf(const T & element) const
{
	for (integer i = 0; i < _count; i++)
	{
		if (_dataPtr[i] == element)
		{
			return i;
		}
	}
	return -1;
}

//===========================================================================//

//Для сортировки используется алгоритм быстрой сортировки, модифицированный для
//предотвращения переполнения стека
//see: http://algolist.manual.ru/sort/quick_sort.php
template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Sort(const IComparator<T> & sortingComparator)
{
	static const integer MAXSTACK = 2048; // максимальный размер стека
	integer i, j; // указатели, участвующие в разделении
	integer lb, ub; // границы сортируемого в цикле фрагмента
	integer lbstack[MAXSTACK], ubstack[MAXSTACK]; // стек запросов
	// каждый запрос задается парой значений,
	// а именно: левой(lbstack) и правой(ubstack)
	// границами промежутка
	integer stackpos = 1; // текущая позиция стека
	integer ppos; // середина массива
	T pivot; // опорный элемент
	T tmp;

	lbstack[1] = 0;
	ubstack[1] = _count - 1;

	do
	{
		// Взять границы lb и ub текущего массива из стека.
		lb = lbstack[stackpos];
		ub = ubstack[stackpos];
		stackpos--;
		do
		{
			// Шаг 1. Разделение по элементу pivot
			ppos = (lb + ub) >> 1;
			i = lb;
			j = ub;
			pivot = _dataPtr[ppos];
			do
			{
				while (sortingComparator.Compare(_dataPtr[i], pivot))
				{
					i++;
				}
				while (sortingComparator.Compare(pivot, _dataPtr[j]))
				{
					j--;
				}

				if (i < j)
				{
					tmp = _dataPtr[i];
					_dataPtr[i] = _dataPtr[j];
					_dataPtr[j] = tmp;
					i++;
					j--;
				}
				else if (i == j)
				{
					i++;
					j--;
				}
			}
			while (i <= j);

			// Сейчас указатель i указывает на начало правого подмассива,
			// j - на конец левого (см. иллюстрацию выше), lb ? j ? i ? ub.
			// Возможен случай, когда указатель i или j выходит за границу массива
			// Шаги 2, 3. Отправляем большую часть в стек  и двигаем lb,ub
			if (i < ppos) // правая часть больше
			{
				if (i < ub) //  если в ней больше 1 элемента - нужно
				{
					stackpos++; //  сортировать, запрос в стек
					lbstack[stackpos] = i;
					ubstack[stackpos] = ub;
				}
				ub = j; //  следующая итерация разделения
				//  будет работать с левой частью
			}
			else // левая часть больше
			{
				if (j > lb)
				{
					stackpos++;
					lbstack[stackpos] = lb;
					ubstack[stackpos] = j;
				}
				lb = i;
			}
		}
		while (lb < ub); // пока в меньшей части более 1 элемента
	}
	while (stackpos != 0); // пока есть запросы в стеке
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Fill(const T & element)
{
	if (_count == 0)
	{
		return;
	}

	for (integer i = 0; i < _count; i++)
	{
		_dataPtr[i] = element;
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Fill(integer index, integer elementsCount, const T & element)
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bound: {0} ([0..{1}])", index, _count - 1));
	}

	if (elementsCount < 0 || index + elementsCount > _count)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("_count out of bounds: {0} (index: {1}) ", _count, index));
	}
#endif

	for (integer i = 0; i < elementsCount; i++)
	{
		_dataPtr[index + i] = element;
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Clear()
{
	_count = 0;
	_reservedCount = 0;
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
		_dataPtr = NullPtr;
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
List<T> Array<T, MoveElementsAlgorithm>::ToList() const
{
	List<T> result;

	for (integer i = 0; i < _count; ++i)
	{
		result.Add(_dataPtr[i]);
	}

	return result;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Reset(integer count)
{
	Clear();
	Resize(count);
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::Reset()
{
	integer sizeBeforeClear = _count;
	Clear();
	Resize(sizeBeforeClear);
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
bool Array<T, MoveElementsAlgorithm>::IsEmpty() const
{
	return _count == 0;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
integer Array<T, MoveElementsAlgorithm>::GetCount() const
{
	return _count;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
inline T * Array<T, MoveElementsAlgorithm>::GetData() const
{
	if (IsEmpty())
	{
		INVALID_STATE_EXCEPTION("Array is empty");
	}

	return _dataPtr;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
ArrayIterator<T, MoveElementsAlgorithm> Array<T, MoveElementsAlgorithm>::CreateIterator() const
{
	return begin();
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
ArrayIterator<T, MoveElementsAlgorithm> Array<T, MoveElementsAlgorithm>::begin() const
{
	return ArrayIterator<T, MoveElementsAlgorithm>(this, 0);
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
ArrayIterator<T, MoveElementsAlgorithm> Array<T, MoveElementsAlgorithm>::end() const
{
	return ArrayIterator<T, MoveElementsAlgorithm>(this, _count);
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
integer Array<T, MoveElementsAlgorithm>::GetDeltaGrowCount() const
{
	return _reservedCount > 0 ? _reservedCount : 1;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
integer Array<T, MoveElementsAlgorithm>::GetBatchDeltaGrowCount(integer batchCount) const
{
	integer batchDeltaGrowCount = GetDeltaGrowCount();
	while (_count + batchCount > _reservedCount + batchDeltaGrowCount)
	{
		batchDeltaGrowCount *= 2;
	}
	return batchDeltaGrowCount > 0 ? batchDeltaGrowCount : 1;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
integer Array<T, MoveElementsAlgorithm>::GetDeltaDecreaseCount() const
{
	return _reservedCount / 2;
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::ChangeReversedCount(integer reservedCount)
{
#ifdef DEBUGGING
	if (reservedCount <= 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("reservedCount must be > 0, but it is {0}", reservedCount));
	}
#endif

	if (reservedCount == _reservedCount)
	{
		return;
	}

	if (_dataPtr == NullPtr)
	{
		_reservedCount = reservedCount;
		_dataPtr = new T[_reservedCount];
	}
	else
	{
		T * newDataPtr = new T[reservedCount];

		integer savedDataCount = _count < reservedCount ? _count : reservedCount;
		MoveElementsAlgorithm<T>::Copy(newDataPtr, 0, _dataPtr, 0, savedDataCount);

		delete[] _dataPtr;

		_reservedCount = reservedCount;
		_dataPtr = newDataPtr;
	}
}

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm>
void Array<T, MoveElementsAlgorithm>::DeepCopy(const Array<T, MoveElementsAlgorithm> & other)
{
	if (other._count == 0)
	{
		Clear();
		return;
	}

	if (_count == other._count && _reservedCount == other._reservedCount)
	{
		MoveElementsAlgorithm<T>::Copy(_dataPtr, 0, other._dataPtr, 0, _count);
	}
	else
	{
		_count = other._count;
		_reservedCount = other._reservedCount;

		if (_dataPtr != NullPtr)
		{
			delete[] _dataPtr;
		}

		if (_reservedCount > 0)
		{
			_dataPtr = new T[_reservedCount]();
			MoveElementsAlgorithm<T>::Copy(_dataPtr, 0, other._dataPtr, 0, _count);
		}
		else
		{
			_dataPtr = NullPtr;
		}
	}
}

//===========================================================================//

} // namespace Bru

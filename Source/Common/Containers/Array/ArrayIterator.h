//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Итератор для массива
//

#pragma once
#ifndef ARRAY_ITERATOR_H
#define ARRAY_ITERATOR_H

#include "../../Types/BaseTypes.h"
#include "../../Types/String/String.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/IteratorOutOfBoundsException.h"
#include "../BaseClasses/CompositeTypeMoveElementsAlgorithm.h"
#include "../BaseClasses/SimpleTypeMoveElementsAlgorithm.h"

namespace Bru
{

//===========================================================================//

template<typename T, template<typename > class MoveElementsAlgorithm>
class Array;

//===========================================================================//

template<typename T, template<typename > class MoveElementsAlgorithm = CompositeTypeMoveElementsAlgorithm>
class ArrayIterator
{
	friend class Array<T, MoveElementsAlgorithm>;

public:
	ArrayIterator(const ArrayIterator<T, MoveElementsAlgorithm> & other);
	virtual ~ArrayIterator();

	ArrayIterator<T, MoveElementsAlgorithm> & operator =(const ArrayIterator<T, MoveElementsAlgorithm> & other);

	bool operator !=(const ArrayIterator<T, MoveElementsAlgorithm> & someIterator) const;
	const ArrayIterator<T, MoveElementsAlgorithm> & operator ++();
	T & operator *() const;

	void First();
	bool HasNext() const;
	void Next();

	T & GetCurrent() const;

	typedef ArrayIterator<T, SimpleTypeMoveElementsAlgorithm> SimpleType;

private:
	ArrayIterator(const Array<T, MoveElementsAlgorithm> * owner, integer cursor);

	const Array<T, MoveElementsAlgorithm> * _owner;
	integer _cursor;

	//Для тестирования
	friend class TestArrayIteratorConstructor;
	friend class TestArrayIteratorForEmpty;
	friend class TestArrayIteratorForOneElement;
	friend class TestArrayIteratorFor;
	friend class TestArrayIteratorRangeForEmpty;
	friend class TestArrayIteratorRangeForOneElement;
	friend class TestArrayIteratorRangeFor;
	friend class TestArrayIteratorExceptions;

	friend class TestArrayCreateIterator;
	friend class TestArrayBegin;
	friend class TestArrayEnd;
	friend class TestSimpleTypeArrayCreateIterator;
	friend class TestSimpleTypeArrayBegin;
	friend class TestSimpleTypeArrayEnd;
};

//===========================================================================//

}// namespace

#include "ArrayIterator.inl"

#endif // ARRAY_ITERATOR_H

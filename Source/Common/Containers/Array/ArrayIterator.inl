//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
ArrayIterator<T, MoveElementsAlgorithm>::ArrayIterator(const Array<T, MoveElementsAlgorithm> * owner, integer cursor) :
	_owner(owner),
	_cursor(cursor)
{
#ifdef DEBUGGING
	if (owner == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("owner == NullPtr");
	}

	if (cursor < 0 || cursor > owner->GetCount())
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION(string::Format("cursor out of bounds: {0} ([0..{1}])", cursor, _owner->GetCount()));
	}
#endif
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
ArrayIterator<T, MoveElementsAlgorithm>::ArrayIterator(const ArrayIterator<T, MoveElementsAlgorithm> & other) :
	_owner(other._owner),
	_cursor(other._cursor)
{
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
ArrayIterator<T, MoveElementsAlgorithm> & ArrayIterator<T, MoveElementsAlgorithm>::operator =(
    const ArrayIterator<T, MoveElementsAlgorithm> & other)
{
	if (this == &other)
	{
		return *this;
	}

	_owner = other._owner;
	_cursor = other._cursor;

	return *this;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
ArrayIterator<T, MoveElementsAlgorithm>::~ArrayIterator()
{
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
bool ArrayIterator<T, MoveElementsAlgorithm>::operator !=(
    const ArrayIterator<T, MoveElementsAlgorithm> & someIterator) const
{
	return _owner != someIterator._owner || _cursor != someIterator._cursor;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
const ArrayIterator<T, MoveElementsAlgorithm> & ArrayIterator<T, MoveElementsAlgorithm>::operator ++()
{
	Next();
	return *this;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
T & ArrayIterator<T, MoveElementsAlgorithm>::operator *() const
{
	return GetCurrent();
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void ArrayIterator<T, MoveElementsAlgorithm>::First()
{
	_cursor = 0;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
bool ArrayIterator<T, MoveElementsAlgorithm>::HasNext() const
{
	return _cursor < _owner->GetCount();
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void ArrayIterator<T, MoveElementsAlgorithm>::Next()
{
#ifdef DEBUGGING
	if (!HasNext())
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION("Can't read beyond the end of array");
	}
#endif

	_cursor++;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
T & ArrayIterator<T, MoveElementsAlgorithm>::GetCurrent() const
{
#ifdef DEBUGGING
	if (_cursor < 0 || _cursor >= _owner->GetCount())
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION(string::Format("Iterator's _cursor out of bounds: {0} ([0..{1}])",
		                                 _cursor, _owner->GetCount() - 1));
	}
#endif

	return _owner->_dataPtr[_cursor];
}

//===========================================================================//

}// namespace

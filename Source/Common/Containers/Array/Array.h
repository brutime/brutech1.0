//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Шаблон массива элементов
//

#pragma once
#ifndef ARRAY_H
#define ARRAY_H

#include "../../Types/BaseTypes.h"
#include "../../Types/String/String.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidStateException.h"
#include "../../Exceptions/IndexOutOfBoundsException.h"
#include "../BaseClasses/IComparator.h"
#include "../BaseClasses/AscendingComparator.h"
#include "../BaseClasses/CompositeTypeMoveElementsAlgorithm.h"
#include "../BaseClasses/SimpleTypeMoveElementsAlgorithm.h"
#include "../List/ListElement.h"
#include "ArrayIterator.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class List;

//===========================================================================//

template<typename T, template<typename> class MoveElementsAlgorithm = CompositeTypeMoveElementsAlgorithm>
class Array
{
	friend class ArrayIterator<T>;

public:
	typedef Array<T, SimpleTypeMoveElementsAlgorithm> SimpleType;

	Array(integer count = 0);
	Array(std::initializer_list<T> list);
	Array(const Array<T, MoveElementsAlgorithm> & other);
	Array(Array<T, MoveElementsAlgorithm> && other);

	virtual ~Array();

	Array<T, MoveElementsAlgorithm> & operator =(const Array<T, MoveElementsAlgorithm> & other);
	Array<T, MoveElementsAlgorithm> & operator =(Array<T, MoveElementsAlgorithm> && other);

	bool operator ==(const Array<T, MoveElementsAlgorithm> & someArray) const;
	bool operator !=(const Array<T, MoveElementsAlgorithm> & someArray) const;

	///Выделить newCount элементов
	void Resize(integer newCount);

	///Добавление нового элемента
	Array<T, MoveElementsAlgorithm> & Add(const T & element);
	Array<T, MoveElementsAlgorithm> & Add(T && element);

	///Добавление элементов другого массива
	Array<T, MoveElementsAlgorithm> & Add(const Array<T, MoveElementsAlgorithm> & other);

	///Добавление элементов списка
	Array<T, MoveElementsAlgorithm> & Add(const List<T> & list);

	///Вставка элемента
	void Insert(integer index, const T & element);
	void Insert(integer index, T && element);

	///Вставка элементов другого массива
	void Insert(integer index, const Array<T, MoveElementsAlgorithm> & other);

	///Удаление элемента
	void Remove(const T & element);
	void Remove(std::function<bool(const T & element)> predicate);

	///Удаление элемента по индексу
	void RemoveAt(integer index);

	///Удаление элементов
	void RemoveAt(integer index, integer elementsCount);

	///Обращение к элементу массива по индексу (для чтения и записи)
	T & operator [](integer index);

	///Обращение к элементу массива по индексу (только для чтения)
	const T & operator [](integer index) const;

	///Получение первого элемента
	T & GetFirst() const;

	///Получение последнего элемента
	T & GetLast() const;

	///Заместить элементы другими элементами
	void SetArray(integer index, const Array<T, MoveElementsAlgorithm> & other);

	///Получение подмассива
	Array<T, MoveElementsAlgorithm> GetArray(integer index, integer elementsCount) const;

	///Метод возвращает истинну, если указанный элемент присутствует в массиве
	bool Contains(const T & element) const;
	bool Contains(std::function<bool(const T &)> predicate) const;

	///Получение индекса элемента
	///Метод возвращает -1, если искомый элемент не найден
	integer IndexOf(const T & element) const;

	///Сортировка массива(по умолчанию - элементы сортируются по возрастанию на основании определенного
	///типа для T оператора <)
	void Sort(const IComparator<T> & SortingComparator = AscendingComparator<T>());

	///Заполнить массив элементов Т
	void Fill(const T & element);

	///Заполнить массив элементов Т c индекса index в количестве count
	void Fill(integer index, integer elementsCount, const T & element);

	List<T> ToList() const;

	///Очистка массива
	void Clear();

	///Очистка массива и пересоздание массива заданного размера
	void Reset(integer newCount);

	///Очистка массива и пересоздание массива такого же размера
	void Reset();

	///Пустой-ли массив
	bool IsEmpty() const;

	///Получить текущий размер массива
	integer GetCount() const;

	//Переименовать в GetDataPtr
	T * GetData() const;

	ArrayIterator<T, MoveElementsAlgorithm> CreateIterator() const;
	ArrayIterator<T, MoveElementsAlgorithm> begin() const;
	ArrayIterator<T, MoveElementsAlgorithm> end() const;

private:
	integer GetDeltaGrowCount() const;
	integer GetBatchDeltaGrowCount(integer batchCount) const;
	integer GetDeltaDecreaseCount() const;
	void ChangeReversedCount(integer newReservedCount);
	void DeepCopy(const Array<T, MoveElementsAlgorithm> & other);

	integer _count;     //Сколько элементов использовано
	integer _reservedCount;     //Сколько элементов использовано + сколько в запасе
	T * _dataPtr;

	friend class TestArrayGetters;
	friend class TestArrayDefaultConstructor;
	friend class TestArrayConstructor;
	friend class TestArrayInitializerListConstructor;
	friend class TestArrayStringConstructor;
	friend class TestArrayCopyConstructor;
	friend class TestArrayMoveConstructor;
	friend class TestArrayCopyOperator;
	friend class TestArrayCopyOperatorSelf;
	friend class TestArrayMoveOperator;
	friend class TestArrayResize;
	friend class TestArrayAdd;
	friend class TestArrayAddOtherArray;
	friend class TestArrayAddList;
	friend class TestArrayInsert;
	friend class TestArrayInsertOtherArray;
	friend class TestArrayRemove;
	friend class TestArrayRemoveWithPredicate;
	friend class TestArrayRemoveAt;
	friend class TestArrayRemoveAtIndexCount;
	friend class TestArrayToList;
	friend class TestArrayClear;
	friend class TestArrayReset;
	friend class TestArrayResetCount;
	friend class TestArrayOperatorSet;
	friend class TestArrayOperatorSetOverwrite;
	friend class TestArrayOperatorGet;
	friend class TestArraySetArray;
	friend class TestArrayGetArray;

	friend class TestSimpleTypeArrayGetters;
	friend class TestSimpleTypeArrayDefaultConstructor;
	friend class TestSimpleTypeArrayConstructor;
	friend class TestSimpleTypeArrayInitializerListConstructor;
	friend class TestSimpleTypeArrayCopyConstructor;
	friend class TestSimpleTypeArrayMoveConstructor;
	friend class TestSimpleTypeArrayCopyOperator;
	friend class TestSimpleTypeArrayCopyOperatorSelf;
	friend class TestSimpleTypeArrayMoveOperator;
	friend class TestSimpleTypeArrayResize;
	friend class TestSimpleTypeArrayAdd;
	friend class TestSimpleTypeArrayAddOtherArray;
	friend class TestSimpleTypeArrayInsert;
	friend class TestSimpleTypeArrayInsertOtherArray;
	friend class TestSimpleTypeArrayRemoveAt;
	friend class TestSimpleTypeArrayRemoveAtIndexCount;
	friend class TestSimpleTypeArrayRemove;
	friend class TestSimpleTypeArrayClear;
	friend class TestSimpleTypeArrayReset;
	friend class TestSimpleTypeArrayResetCount;
	friend class TestSimpleTypeArrayOperatorSet;
	friend class TestSimpleTypeArrayOperatorSetOverwrite;
	friend class TestSimpleTypeArrayOperatorGet;
	friend class TestSimpleTypeArraySetArray;
	friend class TestSimpleTypeArrayGetArray;
	friend class TestSimpleTypeArrayFill;
	friend class TestSimpleTypeArrayFillIndexCount;

	friend class TestListToArray;
};

//===========================================================================//

} // namespace Bru

#include "Array.inl"

#endif // ARRAY_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::ValuePair() :
	First(),
	Second()
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::ValuePair(const T & first, const U & second) :
	First(first),
	Second(second)
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::ValuePair(T && first, const U & second):
	First(std::move(first)),
	Second(second)
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::ValuePair(const T & first, U && second):
	First(first),
	Second(std::move(second))
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::ValuePair(T && first, U && second):
	First(std::move(first)),
	Second(std::move(second))
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::ValuePair(const ValuePair & other) :
	First(other.First),
	Second(other.Second)
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::ValuePair(ValuePair && other):
	First(std::move(other.First)),
	Second(std::move(other.Second))
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U>::~ValuePair()
{
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U> & ValuePair<T, U>::operator =(const ValuePair & other)
{
	if (this == &other)
	{
		return *this;
	}

	First = other.First;
	Second = other.Second;

	return *this;
}

//===========================================================================//

template<typename T, typename U>
ValuePair<T, U> & ValuePair<T, U>::operator =(ValuePair && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(First, other.First);
	std::swap(Second, other.Second);

	return *this;
}

//===========================================================================//

template<typename T, typename U>
bool ValuePair<T, U>::operator ==(const ValuePair & someValuePair) const
{
	return First == someValuePair.First && Second == someValuePair.Second;
}

//===========================================================================//

template<typename T, typename U>
bool ValuePair<T, U>::operator !=(const ValuePair & someValuePair) const
{
	return !(*this == someValuePair);
}

//===========================================================================//

template<typename T, typename U>
bool ValuePair<T, U>::operator <(const ValuePair & someValuePair) const
{
	return First < someValuePair.First || Second < someValuePair.Second;
}

//===========================================================================//

template<typename T, typename U>
bool ValuePair<T, U>::operator >(const ValuePair & someValuePair) const
{
	return First > someValuePair.First || Second > someValuePair.Second;
}

//===========================================================================//

}// namespace Bru

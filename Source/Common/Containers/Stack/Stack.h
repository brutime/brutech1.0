//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Стек
//

#pragma once
#ifndef STACK_H
#define STACK_H

#include "../../Types/BaseTypes.h"
#include "../../Types/String/String.h"
#include "../../Exceptions/InvalidStateException.h"
#include "../../Exceptions/IndexOutOfBoundsException.h"
#include "../BaseClasses/CompositeTypeMoveElementsAlgorithm.h"
#include "../BaseClasses/SimpleTypeMoveElementsAlgorithm.h"

namespace Bru
{

//===========================================================================//

template<typename T, template<typename > class MoveElementsAlgorithm = CompositeTypeMoveElementsAlgorithm>
class Stack
{
public:
	typedef Stack<T, SimpleTypeMoveElementsAlgorithm> SimpleType;

	///Создание пустого стека с резервированием памяти под initialCount элементов
	Stack(integer reservedCount = 0);
	Stack(std::initializer_list<T> list);
	Stack(const Stack<T, MoveElementsAlgorithm> & other);
	Stack(Stack<T, MoveElementsAlgorithm> && other);

	~Stack();

	Stack<T, MoveElementsAlgorithm> & operator =(const Stack<T, MoveElementsAlgorithm> & other);
	Stack<T, MoveElementsAlgorithm> & operator =(Stack<T, MoveElementsAlgorithm> && other);

	///Задание размера стека. Если новый размер меньше старого, то старые лишние элементы удаляются.
	///Если новый размер больше старого - старые элементы копируются, новые заполнены мусором.
	///Если новый размер = 0, то стек очищается
	void Reserve(integer reservedCount);

	///Добавление элемента в вершину стека
	void Push(const T & element);
	void Push(T && element);

	///Получение верхнего элемента стека (только для чтения)
	const T & Top() const;

	///Получение i-го элемента стека
	const T & operator[] (integer index) const;

	///Удаление верхнего элемента из стека
	void Pop();

	///Очистка стека
	void Clear();

	///Пустой-ли стек
	bool IsEmpty() const
	{
		return _count == 0;
	}

	integer GetCount() const
	{
		return _count;
	}

private:
	integer GetDeltaGrowCount() const;
	integer GetDeltaDecreaseCount() const;
	void DeepCopy(const Stack<T, MoveElementsAlgorithm> & other);

	integer _count;
	integer _reservedCount;
	T * _dataPtr;

	friend class TestStackGetters;
	friend class TestStackDefaultConstructor;
	friend class TestStackConstructor;
	friend class TestStackInitializerListConstructor;
	friend class TestStackCopyConstructor;
	friend class TestStackMoveConstructor;
	friend class TestStackCopyOperator;
	friend class TestStackCopyOperatorSelf;
	friend class TestStackMoveOperator;
	friend class TestStackReserve;
	friend class TestStackPush;
	friend class TestStackTopPop;
	friend class TestStackGet;
	friend class TestStackClear;

	friend class TestSimpleTypeStackGetters;
	friend class TestSimpleTypeStackDefaultConstructor;
	friend class TestSimpleTypeStackConstructor;
	friend class TestSimpleTypeStackInitializerListConstructor;
	friend class TestSimpleTypeStackCopyConstructor;
	friend class TestSimpleTypeStackMoveConstructor;
	friend class TestSimpleTypeStackCopyOperator;
	friend class TestSimpleTypeStackCopyOperatorSelf;
	friend class TestSimpleTypeStackMoveOperator;
	friend class TestSimpleTypeStackReserve;
	friend class TestSimpleTypeStackPush;
	friend class TestSimpleTypeStackTopPop;
	friend class TestSimpleTypeStackGet;
	friend class TestSimpleTypeStackClear;
};

//===========================================================================//

}//namespace

#include "Stack.inl"

#endif // STACK_H

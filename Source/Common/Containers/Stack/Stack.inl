//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
Stack<T, MoveElementsAlgorithm>::Stack(integer reservedCount) :
	_count(0),
	_reservedCount(0),
	_dataPtr(NullPtr)
{
	if (reservedCount != 0)
	{
		Reserve(reservedCount);
	}
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
Stack<T, MoveElementsAlgorithm>::Stack(std::initializer_list<T> list) :
	_count(0),
	_reservedCount(0),
	_dataPtr(NullPtr)
{
	if (list.size() == 0)
	{
		return;
	}

	_count = list.size();
	Reserve(_count);
	integer index = 0;
	for (const T & element : list)
	{
		_dataPtr[index] = element;
		index++;
	}
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
Stack<T, MoveElementsAlgorithm>::Stack(const Stack<T, MoveElementsAlgorithm> & other) :
	_count(0),
	_reservedCount(0),
	_dataPtr(NullPtr)
{
	DeepCopy(other);
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
Stack<T, MoveElementsAlgorithm>::Stack(Stack<T, MoveElementsAlgorithm> && other):
	_count(other._count),
	_reservedCount(other._reservedCount),
	_dataPtr(other._dataPtr)
{
	other._count = 0;
	other._reservedCount = 0;
	other._dataPtr = NullPtr;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
Stack<T, MoveElementsAlgorithm>::~Stack()
{
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
Stack<T, MoveElementsAlgorithm> & Stack<T, MoveElementsAlgorithm>::operator =(
    const Stack<T, MoveElementsAlgorithm> & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
Stack<T, MoveElementsAlgorithm> & Stack<T, MoveElementsAlgorithm>::operator =(Stack<T, MoveElementsAlgorithm> && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_count, other._count);
	std::swap(_reservedCount, other._reservedCount);
	std::swap(_dataPtr, other._dataPtr);

	return *this;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void Stack<T, MoveElementsAlgorithm>::Reserve(integer reservedCount)
{
#ifdef DEBUGGING
	if (reservedCount < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("reservedCount must be >= 0, but it is {0}", reservedCount));
	}
#endif

	if (reservedCount == 0)
	{
		Clear();
		return;
	}

	if (reservedCount == _reservedCount)
	{
		return;
	}

	if (_dataPtr == NullPtr)
	{
		_reservedCount = reservedCount;
		_dataPtr = new T[_reservedCount];
	}
	else
	{
		T * newDataPtr = new T[reservedCount];

		integer savedDataCount = _count < reservedCount ? _count : reservedCount;
		for (integer i = 0; i < savedDataCount; i++)
		{
			newDataPtr[i] = _dataPtr[i];
		}

		delete[] _dataPtr;

		if (_count > reservedCount)
		{
			_count = reservedCount;
		}
		_reservedCount = reservedCount;
		_dataPtr = newDataPtr;
	}
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void Stack<T, MoveElementsAlgorithm>::Push(const T & element)
{
	if (_count == _reservedCount)
	{
		integer deltaGrowCount = GetDeltaGrowCount();
		Reserve(_count + deltaGrowCount);
	}

	_dataPtr[_count] = element;
	_count++;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void Stack<T, MoveElementsAlgorithm>::Push(T && element)
{
	if (_count == _reservedCount)
	{
		integer deltaGrowCount = GetDeltaGrowCount();
		Reserve(_count + deltaGrowCount);
	}

	_dataPtr[_count] = std::move(element);
	_count++;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
const T & Stack<T, MoveElementsAlgorithm>::Top() const
{
#ifdef DEBUGGING
	if (_count == 0)
	{
		INVALID_STATE_EXCEPTION("Stack is empty");
	}
#endif

	return _dataPtr[_count - 1];
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
const T & Stack<T, MoveElementsAlgorithm>::operator[](integer index) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _count)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _count - 1));
	}
#endif

	return _dataPtr[index];
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void Stack<T, MoveElementsAlgorithm>::Pop()
{
#ifdef DEBUGGING
	if (_count == 0)
	{
		INVALID_STATE_EXCEPTION("Couldn't pop elements from empty stack");
	}
#endif

	if (_count == 1)
	{
		Clear();
	}
	else
	{
		_count--;

		integer decreaseCount = GetDeltaDecreaseCount();
		if (_count < decreaseCount)
		{
			Reserve(decreaseCount);
		}
	}
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void Stack<T, MoveElementsAlgorithm>::Clear()
{
	_count = 0;
	_reservedCount = 0;
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
		_dataPtr = NullPtr;
	}
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
integer Stack<T, MoveElementsAlgorithm>::GetDeltaGrowCount() const
{
	return _reservedCount > 0 ? _reservedCount : 1;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
integer Stack<T, MoveElementsAlgorithm>::GetDeltaDecreaseCount() const
{
	return _reservedCount / 2;
}

//===========================================================================//

template<typename T, template<class > class MoveElementsAlgorithm>
void Stack<T, MoveElementsAlgorithm>::DeepCopy(const Stack<T, MoveElementsAlgorithm> & other)
{
	if (other._count == 0)
	{
		Clear();
		return;
	}

	if (_count == other._count && _reservedCount == other._reservedCount)
	{
		MoveElementsAlgorithm<T>::Copy(_dataPtr, 0, other._dataPtr, 0, _count);
	}
	else
	{
		_count = other._count;
		_reservedCount = other._reservedCount;

		if (_dataPtr != NullPtr)
		{
			delete[] _dataPtr;
		}

		if (_reservedCount > 0)
		{
			_dataPtr = new T[_reservedCount]();
			MoveElementsAlgorithm<T>::Copy(_dataPtr, 0, other._dataPtr, 0, _count);
		}
		else
		{
			_dataPtr = NullPtr;
		}
	}
}

//===========================================================================//

}//namespace

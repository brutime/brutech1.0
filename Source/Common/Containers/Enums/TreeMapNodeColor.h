//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Цвет узла дерева
//

#pragma once
#ifndef TREE_MAP_NODE_COLOR_H
#define TREE_MAP_NODE_COLOR_H

namespace Bru
{

//===========================================================================//

enum class TreeMapNodeColor
{
    Red,
    Black
};

//===========================================================================//

}//namespace

#endif // TREE_MAP_NODE_COLOR_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: пара чего-нибудь :)
//

#pragma once
#ifndef VALUE_PAIR_H
#define VALUE_PAIR_H

namespace Bru
{

//===========================================================================//

template<typename T, typename U>
class ValuePair
{
public:
	ValuePair();
	ValuePair(const T & first, const U & second);
	ValuePair(T && first, const U & second);
	ValuePair(const T & first, U && second);
	ValuePair(T && first, U && second);
	ValuePair(const ValuePair & other);
	ValuePair(ValuePair && other);
	~ValuePair();

	ValuePair & operator =(const ValuePair & other);
	ValuePair & operator =(ValuePair && other);

	bool operator ==(const ValuePair & someValuePair) const;
	bool operator !=(const ValuePair & someValuePair) const;
	bool operator <(const ValuePair & someValuePair) const;
	bool operator >(const ValuePair & someValuePair) const;

	T First;
	U Second;
};

//===========================================================================//

}// namespace

#include "ValuePair.inl"

#endif // VALUE_PAIR_H

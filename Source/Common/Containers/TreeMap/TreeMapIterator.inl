//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
TreeMapIterator<K, V>::TreeMapIterator(const TreeMap<K, V> * owner, TreeMapNode<K, V> * cursor) :
	_owner(owner),
	_cursor(cursor)
{
#ifdef DEBUGGING
	if (owner == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("owner == NullPtr");
	}
#endif
}

//===========================================================================//

template<typename K, typename V>
TreeMapIterator<K, V>::TreeMapIterator(const TreeMapIterator<K, V> & other) :
	_owner(other._owner),
	_cursor(other._cursor)
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapIterator<K, V>::~TreeMapIterator()
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapIterator<K, V> & TreeMapIterator<K, V>::operator =(const TreeMapIterator & other)
{
	if (this == &other)
	{
		return *this;
	}

	_owner = other._owner;
	_cursor = other._cursor;
//    _nodesStack = other._nodesStack;

	return *this;
}

//===========================================================================//

template<typename K, typename V>
bool TreeMapIterator<K, V>::operator !=(const TreeMapIterator<K, V> & someIterator) const
{
	return _owner != someIterator._owner || _cursor != someIterator._cursor;
}

//===========================================================================//

template<typename K, typename V>
const TreeMapIterator<K, V> & TreeMapIterator<K, V>::operator ++()
{
	Next();
	return *this;
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V> & TreeMapIterator<K, V>::operator *() const
{
	return GetCurrent();
}

//===========================================================================//

template<typename K, typename V>
void TreeMapIterator<K, V>::First()
{
	_cursor = _owner->_mostLeftNode;
}

//===========================================================================//

template<typename K, typename V>
bool TreeMapIterator<K, V>::HasNext() const
{
	return _cursor != NullPtr;
}

//===========================================================================//

template<typename K, typename V>
void TreeMapIterator<K, V>::Next()
{
#ifdef DEBUGGING
	if (!HasNext())
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION("Can't read beyond the end of tree");
	}
#endif

	if (_cursor->RightNode != NullPtr)
	{
		_cursor = _cursor->RightNode;
		while (_cursor->LeftNode != NullPtr)
		{
			_cursor = _cursor->LeftNode;
		}
	}
	else if (_cursor->ParentNode == NullPtr)
	{
		_cursor = NullPtr;
	}
	else
	{
		TreeMapNode<K, V> * parentNode = _cursor->ParentNode;
		while (_cursor == parentNode->RightNode)
		{
			_cursor = parentNode;
			parentNode = parentNode->ParentNode;
			if (parentNode == NullPtr)
			{
				_cursor = NullPtr;
				return;
			}
		}
		if (_cursor->RightNode != parentNode)
		{
			_cursor = parentNode;
		}
	}
}

//===========================================================================//

template<typename K, typename V>
KeyValuePair<K, V> & TreeMapIterator<K, V>::GetCurrent() const
{
#ifdef DEBUGGING
	if (_cursor == NullPtr)
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION("Iterator's _cursor == NullPtr");
	}
#endif
	return _cursor->Pair;
}

//===========================================================================//

}// namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: словарь
//

#pragma once
#ifndef TREE_MAP_H
#define TREE_MAP_H

#include "../BaseClasses/BaseTreeMap.h"
#include "TreeMapIterator.h"

namespace Bru
{

//===========================================================================//

//для тестирования
template<typename K, typename V>
class TreeMapChecker;

//===========================================================================//

template<typename K, typename V>
class TreeMap : public BaseTreeMap<K, V>
{
	friend class TreeMapIterator<K, V> ;

public:
	TreeMap();
	TreeMap(std::initializer_list<KeyValuePair<K, V>> list);
	TreeMap(const TreeMap<K, V> & other);
	TreeMap(TreeMap<K, V> && other);

	virtual ~TreeMap();

	TreeMap<K, V> & operator =(const TreeMap<K, V> & other);
	TreeMap<K, V> & operator =(TreeMap<K, V> && other);

	///Добавление пары (ключ, значение)
	virtual void Add(const K & key, const V & value);
	virtual void Add(K && key, const V & value);
	virtual void Add(const K & key, V && value);
	virtual void Add(K && key, V && value);

	///Добавление KeyValuePair
	virtual void Add(const KeyValuePair<K, V> & keyValuePair);
	virtual void Add(KeyValuePair<K, V> && keyValuePair);

	///Удаление элемента по ключу
	virtual void Remove(const K & key);

	///Очистка дерева
	virtual void Clear();

	TreeMapIterator<K, V> CreateIterator() const;
	TreeMapIterator<K, V> begin() const;
	TreeMapIterator<K, V> end() const;

private:
	void UpdateMostLeftNode();
	void DeepCopy(const TreeMap<K, V> & other);

	TreeMapNode<K, V> * _mostLeftNode;

	friend class TreeMapChecker<K, V>;

	friend class TestTreeMapGetters;
	friend class TestTreeMapDefaultConstructor;
	friend class TestTreeListInitializerListConstructor;
	friend class TestTreeMapCopyConstructor;
	friend class TestTreeMapMoveConstructor;
	friend class TestTreeMapCopyOperator;
	friend class TestTreeMapCopyOperatorSelf;
	friend class TestTreeMapMoveOperator;
	friend class TestTreeMapAddKeyValue;
	friend class TestTreeMapAddMoveKey;
	friend class TestTreeMapAddMoveValue;
	friend class TestTreeMapAddMoveKeyValue;
	friend class TestTreeMapAddKeyValuePair;
	friend class TestTreeMapAddMoveKeyValuePair;
	friend class TestTreeMapRemove;
	friend class TestTreeMapOperatorSetIntegerKey;
	friend class TestTreeMapOperatorSetStringKey;
	friend class TestTreeMapOperatorGetIntegerKey;
	friend class TestTreeMapOperatorGetStringKey;
	friend class TestTreeMapContains;
	friend class TestTreeMapClear;
	friend class TestTreeMapLastFoundNode;
	friend class TestTreeMapCreateIterator;
	friend class TestTreeMapBegin;
	friend class TestTreeMapEnd;

	friend class TestTreeMapIteratorConstructor;
	friend class TestTreeMapIteratorForEmpty;
	friend class TestTreeMapIteratorForOnePair;
	friend class TestTreeMapIteratorFor;
	friend class TestTreeMapIteratorRangeForEmpty;
	friend class TestTreeMapIteratorRangeForOnePair;
	friend class TestTreeMapIteratorRangeFor;
	friend class TestTreeMapIteratorExceptions;
};

//===========================================================================//

}//namespace

#include "TreeMap.inl"

#endif // TREE_MAP_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: реализация с помощью красно-черного дерева
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
TreeMap<K, V>::TreeMap() :
	BaseTreeMap<K, V>(),
	_mostLeftNode(NullPtr)
{
}

//===========================================================================//

template<typename K, typename V>
TreeMap<K, V>::TreeMap(std::initializer_list<KeyValuePair<K, V>> list) :
	BaseTreeMap<K, V>(),
	_mostLeftNode(NullPtr)
{
	for (const KeyValuePair<K, V> & pair : list)
	{
		Add(pair);
	}
}

//===========================================================================//

template<typename K, typename V>
TreeMap<K, V>::TreeMap(const TreeMap<K, V> & other) :
	BaseTreeMap<K, V>(),
	_mostLeftNode(NullPtr)
{
	DeepCopy(other);
}

//===========================================================================//

template<typename K, typename V>
TreeMap<K, V>::TreeMap(TreeMap<K, V> && other): BaseTreeMap<K, V>(),
	_mostLeftNode(other._mostLeftNode)
{
	BaseTreeMap<K, V>::_rootNode = other._rootNode;
	BaseTreeMap<K, V>::_lastFoundNode = other._lastFoundNode;
	BaseTreeMap<K, V>::_count = other._count;

	other._rootNode = NullPtr;
	other._lastFoundNode = NullPtr;
	other._count = 0;
	other._mostLeftNode = NullPtr;
}

//===========================================================================//

template<typename K, typename V>
TreeMap<K, V>::~TreeMap()
{
}

//===========================================================================//

template<typename K, typename V>
TreeMap<K, V> & TreeMap<K, V>::operator =(const TreeMap<K, V> & other)
{
	if (this == &other)
	{
		return *this;
	}

	if (!this->IsEmpty())
	{
		Clear();
	}

	DeepCopy(other);

	return *this;
}

//===========================================================================//

template<typename K, typename V>
TreeMap<K, V> & TreeMap<K, V>::operator =(TreeMap<K, V> && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(BaseTreeMap<K, V>::_rootNode, other._rootNode);
	std::swap(BaseTreeMap<K, V>::_lastFoundNode, other._lastFoundNode);
	std::swap(BaseTreeMap<K, V>::_count, other._count);
	std::swap(_mostLeftNode, other._mostLeftNode);

	return *this;
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Add(const K & key, const V & value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	this->InsertNode(new TreeMapNode<K, V>(key, value));
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Add(K && key, const V & value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	this->InsertNode(new TreeMapNode<K, V>(std::move(key), value));
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Add(const K & key, V && value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	this->InsertNode(new TreeMapNode<K, V>(key, std::move(value)));
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Add(K && key, V && value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	this->InsertNode(new TreeMapNode<K, V>(std::move(key), std::move(value)));
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Add(const KeyValuePair<K, V> & keyValuePair)
{
	if (this->Contains(keyValuePair.GetKey()))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	this->InsertNode(new TreeMapNode<K, V>(keyValuePair.GetKey(), keyValuePair.GetValue()));
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Add(KeyValuePair<K, V> && keyValuePair)
{
	if (this->Contains(keyValuePair.GetKey()))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	this->InsertNode(new TreeMapNode<K, V>(std::move(keyValuePair.GetKey()), std::move(keyValuePair.GetValue())));
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Remove(const K & key)
{
	TreeMapNode<K, V> * node = this->FindNode(key);
	if (node == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("Requested element not found");
	}

	this->RemoveNode(node);
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::Clear()
{
	BaseTreeMap<K, V>::Clear();
	UpdateMostLeftNode();
}

//===========================================================================//

template<typename K, typename V>
TreeMapIterator<K, V> TreeMap<K, V>::CreateIterator() const
{
	return begin();
}

//===========================================================================//

template<typename K, typename V>
TreeMapIterator<K, V> TreeMap<K, V>::begin() const
{
	return TreeMapIterator<K, V>(this, _mostLeftNode);
}

//===========================================================================//

template<typename K, typename V>
TreeMapIterator<K, V> TreeMap<K, V>::end() const
{
	return TreeMapIterator<K, V>(this, NullPtr);
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::UpdateMostLeftNode()
{
	if (this->_rootNode == NullPtr)
	{
		_mostLeftNode = NullPtr;
		return;
	}

	_mostLeftNode = this->_rootNode;
	while (_mostLeftNode->LeftNode != NullPtr)
	{
		_mostLeftNode = _mostLeftNode->LeftNode;
	}
}

//===========================================================================//

template<typename K, typename V>
void TreeMap<K, V>::DeepCopy(const TreeMap<K, V> & other)
{
	for (auto & pair : other)
	{
		Add(pair.GetKey(), pair.GetValue());
	}
}

//===========================================================================//

}//namespace

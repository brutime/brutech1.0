//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: итератор для словаря
//

#pragma once
#ifndef TREE_MAP_ITERATOR_H
#define TREE_MAP_ITERATOR_H

#include "../../Exceptions/IteratorOutOfBoundsException.h"
#include "../Stack/Stack.h"

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
class TreeMap;

//===========================================================================//

template<typename K, typename V>
class TreeMapIterator
{
	friend class TreeMap<K, V> ;

public:
	TreeMapIterator(const TreeMapIterator<K, V> & other);
	virtual ~TreeMapIterator();

	TreeMapIterator<K, V> & operator =(const TreeMapIterator<K, V> & other);

	bool operator !=(const TreeMapIterator<K, V> & someIterator) const;
	const TreeMapIterator<K, V> & operator ++();
	KeyValuePair<K, V> & operator *() const;

	virtual void First();
	virtual bool HasNext() const;
	virtual void Next();

	KeyValuePair<K, V> & GetCurrent() const;

private:
	TreeMapIterator(const TreeMap<K, V> * owner, TreeMapNode<K, V> * cursor);

	const TreeMap<K, V> * _owner;
	TreeMapNode<K, V> * _cursor;

	friend class TestTreeMapCreateIterator;
	friend class TestTreeMapBegin;
	friend class TestTreeMapEnd;

	friend class TestTreeMapIteratorConstructor;
	friend class TestTreeMapIteratorForEmpty;
	friend class TestTreeMapIteratorForOnePair;
	friend class TestTreeMapIteratorFor;
	friend class TestTreeMapIteratorRangeForEmpty;
	friend class TestTreeMapIteratorRangeForOnePair;
	friend class TestTreeMapIteratorRangeFor;
	friend class TestTreeMapIteratorExceptions;
};

//===========================================================================//

}// namesapace

#include "TreeMapIterator.inl"

#endif // TREE_MAP_ITERATOR_H

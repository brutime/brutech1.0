//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Ключ-значение
//

#pragma once
#ifndef KEY_VALUE_PAIR_H
#define KEY_VALUE_PAIR_H

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
class KeyValuePair
{
public:
	KeyValuePair(const K & key, const V & value);
	KeyValuePair(K && key, const V & value);
	KeyValuePair(const K & key, V && value);
	KeyValuePair(K && key, V && value);
	KeyValuePair(const KeyValuePair<K, V> & other);
	KeyValuePair(KeyValuePair<K, V> && other);
	~KeyValuePair();

	KeyValuePair<K, V> & operator =(const KeyValuePair<K, V> & other);
	KeyValuePair<K, V> & operator =(KeyValuePair<K, V> && other);

	bool operator ==(const KeyValuePair<K, V> & keyValuePair) const;
	bool operator !=(const KeyValuePair<K, V> & keyValuePair) const;
	bool operator <(const KeyValuePair<K, V> & keyValuePair) const;
	bool operator >(const KeyValuePair<K, V> & keyValuePair) const;

	const K & GetKey() const;

	V & GetValue();
	const V & GetValue() const;

private:
	K _key;
	V _value;

	friend class TestKeyValuePairGetters;
};

//===========================================================================//

}// namespace

#include "KeyValuePair.inl"

#endif //KEY_VALUE_PAIR_H

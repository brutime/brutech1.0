//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: базовая реализация красно-черного дерева
//

#pragma once
#ifndef BASE_TREE_MAP_H
#define BASE_TREE_MAP_H

#include "../../Types/BaseTypes.h"
#include "../../Types/String/String.h"
#include "../../Exceptions/InvalidStateException.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/NotImplementedException.h"
#include "TreeMapNode.h"

namespace Bru
{

//===========================================================================//

//для тестирования
template<typename K, typename V>
class TreeMapChecker;

//===========================================================================//

template<typename K, typename V>
class BaseTreeMap
{
public:
	BaseTreeMap();
	virtual ~BaseTreeMap();

	///Добавление пары (ключ, значение)
	virtual void Add(const K & key, const V & value) = 0;
	virtual void Add(K && key, const V & value) = 0;
	virtual void Add(const K & key, V && value) = 0;
	virtual void Add(K && key, V && value) = 0;

	///Добавление KeyValuePair
	virtual void Add(const KeyValuePair<K, V> & keyValuePair) = 0;
	virtual void Add(KeyValuePair<K, V> && keyValuePair) = 0;

	///Удаление элемента по ключу
	virtual void Remove(const K & key) = 0;

	///Получение значения по ключу с возможностью изменять значение
	V & operator [](const K & key);

	///Получение значения по ключу
	const V & operator [](const K & key) const;

	///Метод возвращает истинну, если указанный элемент присутствует в дереве
	bool Contains(const K & key) const;

	///Очистка дерева
	virtual void Clear();

	///Пустое-ли дерево
	bool IsEmpty() const
	{
		return _count == 0;
	}

	///Получение количества элементов
	integer GetCount() const
	{
		return _count;
	}

protected:
	TreeMapNode<K, V> * InsertNode(TreeMapNode<K, V> * insertingNode);
	void RemoveNode(TreeMapNode<K, V> * removingNode);
	TreeMapNode<K, V> * FindNode(const K & key) const;
	TreeMapNode<K, V> * GetNode(const K & key) const;
	void FreeNodes(TreeMapNode<K, V> * node);
	TreeMapNode<K, V> * FindFutureParentNode(const K & key) const;

	void FixBalanceAfterInsert(TreeMapNode<K, V> * node);
	void FixBalanceAfterRemove(TreeMapNode<K, V> * node, TreeMapNode<K, V> * nodeParent);

	void LeftRotate(TreeMapNode<K, V> * node);
	void RightRotate(TreeMapNode<K, V> * node);

	void ResetLastFoundNode() const;

	integer _count;
	TreeMapNode<K, V> * _rootNode;
	mutable TreeMapNode<K, V> * _lastFoundNode; //для оптимизации

private:
	BaseTreeMap(const BaseTreeMap<K, V> &) = delete;
	BaseTreeMap<K, V> & operator =(const BaseTreeMap<K, V> &) = delete;
};

//===========================================================================//

}//namespace

#include "BaseTreeMap.inl"

#endif // BASE_TREE_MAP_H

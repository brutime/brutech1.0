//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: перемещение эллемнтов массива простого типа
//

#pragma once
#ifndef SIMPLE_TYPE_MOVE_ELEMENTS_ALGORITHM_H
#define SIMPLE_TYPE_MOVE_ELEMENTS_ALGORITHM_H

#include "../../Types/BaseTypes.h"
#include "../../Helpers/Memory.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class SimpleTypeMoveElementsAlgorithm
{
public:
	static void Copy(T * destinationDataPtr, integer destinationIndex, const T * sourceDataPtr, integer sourceIndex,
	                 integer size);

	static void MoveForward(T * dataPtr, integer index, integer destinationOffset, integer size);
	static void MoveBackward(T * dataPtr, integer index, integer offset, integer size);

private:
	SimpleTypeMoveElementsAlgorithm() = delete;
	SimpleTypeMoveElementsAlgorithm(const SimpleTypeMoveElementsAlgorithm &) = delete;
	~SimpleTypeMoveElementsAlgorithm() = delete;
	SimpleTypeMoveElementsAlgorithm & operator =(const SimpleTypeMoveElementsAlgorithm &) = delete;
};

//===========================================================================//

}//namespace

#include "SimpleTypeMoveElementsAlgorithm.inl"

#endif // SIMPLE_TYPE_MOVE_ELEMENTS_ALGORITHM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V>::TreeMapNode(const K & key, const V & value) :
	ParentNode(NullPtr),
	LeftNode(NullPtr),
	RightNode(NullPtr),
	Color(TreeMapNodeColor::Black),
	Pair(key, value)
{
	//data = new byte[4000000];
}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V>::TreeMapNode(K && key, const V & value):
	ParentNode(NullPtr),
	LeftNode(NullPtr),
	RightNode(NullPtr),
	Color(TreeMapNodeColor::Black),
	Pair(std::move(key), value)
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V>::TreeMapNode(const K & key, V && value):
	ParentNode(NullPtr),
	LeftNode(NullPtr),
	RightNode(NullPtr),
	Color(TreeMapNodeColor::Black),
	Pair(key, std::move(value))
{
}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V>::TreeMapNode(K && key, V && value):
	ParentNode(NullPtr),
	LeftNode(NullPtr),
	RightNode(NullPtr),
	Color(TreeMapNodeColor::Black),
	Pair(std::move(key), std::move(value))
{

}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V>::~TreeMapNode()
{
	//delete [] data;
}

//===========================================================================//

}//namespace

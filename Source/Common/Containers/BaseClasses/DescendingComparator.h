//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Comparator сравнивающий объекты a > b
//

#pragma once
#ifndef DESCENDING_COMPARATOR_H
#define DESCENDING_COMPARATOR_H

#include "IComparator.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class DescendingComparator : public IComparator<T>
{
public:
	DescendingComparator();
	virtual ~DescendingComparator();

	bool Compare(const T & leftObject, const T & rightObject) const;

private:
	DescendingComparator(const DescendingComparator &) = delete;
	DescendingComparator & operator =(const DescendingComparator &) = delete;
};

//===========================================================================//

}//namespace

#include "DescendingComparator.inl"

#endif // DESCENDING_COMPARATOR_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
AscendingComparator<T>::AscendingComparator() :
	IComparator<T>()
{
}

//===========================================================================//

template<typename T>
AscendingComparator<T>::~AscendingComparator()
{
}

//===========================================================================//

template<typename T>
bool AscendingComparator<T>::Compare(const T & leftObject, const T & rightObject) const
{
	return leftObject < rightObject;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс IComparator является интерфейсом для классов,
//	  выполняющих сравнение объектов одного класса
//

#pragma once
#ifndef I_COMPARATOR_H
#define I_COMPARATOR_H

namespace Bru
{

//===========================================================================//

template<typename T>
class IComparator
{
public:
	virtual ~IComparator();

	virtual bool Compare(const T & leftObject, const T & rightObject) const = 0;

protected:
	IComparator();

private:
	IComparator(const IComparator &) = delete;
	IComparator & operator =(const IComparator &) = delete;
};

//===========================================================================//

}//namespace

#include "IComparator.inl"

#endif // I_COMPARATOR_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: реализация с помощью красно-черного дерева
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
BaseTreeMap<K, V>::BaseTreeMap() :
	_count(0),
	_rootNode(NullPtr),
	_lastFoundNode(NullPtr)
{
}

//===========================================================================//

template<typename K, typename V>
BaseTreeMap<K, V>::~BaseTreeMap()
{
	if (_rootNode != NullPtr)
	{
		Clear();
	}
}

//===========================================================================//

template<typename K, typename V>
V & BaseTreeMap<K, V>::operator [](const K & key)
{
	return GetNode(key)->Pair.GetValue();
}

//===========================================================================//

template<typename K, typename V>
const V & BaseTreeMap<K, V>::operator [](const K & key) const
{
	return GetNode(key)->Pair.GetValue();
}

//===========================================================================//

template<typename K, typename V>
bool BaseTreeMap<K, V>::Contains(const K & key) const
{
	return FindNode(key) != NullPtr;
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::Clear()
{
	if (_rootNode == NullPtr)
	{
		return;
	}

	ResetLastFoundNode();
	FreeNodes(_rootNode);

	_count = 0;
	_rootNode = NullPtr;
}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V> * BaseTreeMap<K, V>::FindFutureParentNode(const K & key) const
{
	if (_rootNode == NullPtr)
	{
		return NullPtr;
	}

	TreeMapNode<K, V> * currentNode = _rootNode;
	TreeMapNode<K, V> * parentNode = NullPtr;

	while (currentNode != NullPtr)
	{
		if (key == currentNode->Pair.GetKey())
		{
			INVALID_STATE_EXCEPTION("Unique breaked");
		}

		parentNode = currentNode;

		//Если узел должен быть слева
		if (key < currentNode->Pair.GetKey())
		{
			//Смещаемся влево
			currentNode = currentNode->LeftNode;
		}
		//Если узел должен быть справа
		else // key > currentNode->Key
		{
			//Смещаемся вправо
			currentNode = currentNode->RightNode;
		}
	}

	return parentNode;
}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V> * BaseTreeMap<K, V>::InsertNode(TreeMapNode<K, V> * insertingNode)
{
	ResetLastFoundNode();

	TreeMapNode<K, V> * parentNode = FindFutureParentNode(insertingNode->Pair.GetKey());

	insertingNode->ParentNode = parentNode;
	insertingNode->LeftNode = NullPtr;
	insertingNode->RightNode = NullPtr;
	insertingNode->Color = TreeMapNodeColor::Red;

	if (_rootNode == NullPtr) //parentNode == NullPtr
	{
		_rootNode = insertingNode;
	}
	else
	{
		if (insertingNode->Pair.GetKey() == parentNode->Pair.GetKey())
		{
			INVALID_STATE_EXCEPTION("Unique breaked");
		}

		if (insertingNode->Pair.GetKey() < parentNode->Pair.GetKey())
		{
			parentNode->LeftNode = insertingNode;
		}
		else
		{
			parentNode->RightNode = insertingNode;
		}
	}

	FixBalanceAfterInsert(insertingNode);
	_count++;
	return insertingNode;
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::RemoveNode(TreeMapNode<K, V> * removingNode)
{
	if (removingNode == NullPtr)
	{
		return;
	}

	ResetLastFoundNode();

	TreeMapNode<K, V> * successorNode;
	//Нам нужен узел с null child
	if (removingNode->LeftNode == NullPtr || removingNode->RightNode == NullPtr)
	{
		successorNode = removingNode;
	}
	else
	{
		//Ищем узел с null child
		successorNode = removingNode->RightNode;
		while (successorNode->LeftNode != NullPtr)
		{
			successorNode = successorNode->LeftNode;
		}
	}

	TreeMapNode<K, V> * successorChildNode;
	if (successorNode->LeftNode != NullPtr)
	{
		successorChildNode = successorNode->LeftNode;
	}
	else
	{
		successorChildNode = successorNode->RightNode;
	}

	//Убираем successorNode узел из цепочки
	if (successorChildNode != NullPtr)
	{
		successorChildNode->ParentNode = successorNode->ParentNode;
	}
	TreeMapNode<K, V> * successorChildNodeParent = successorNode->ParentNode;

	if (successorNode->ParentNode == NullPtr)
	{
		_rootNode = successorChildNode;
	}
	else if (successorNode != _rootNode)
	{
		if (successorNode == successorNode->ParentNode->LeftNode)
		{
			successorNode->ParentNode->LeftNode = successorChildNode;
		}
		else
		{
			successorNode->ParentNode->RightNode = successorChildNode;
		}
	}

	if (successorNode != removingNode)
	{
		if (successorNode->Color == TreeMapNodeColor::Black)
		{
			FixBalanceAfterRemove(successorChildNode, successorChildNodeParent);
		}

		successorNode->ParentNode = removingNode->ParentNode;
		successorNode->LeftNode = removingNode->LeftNode;
		successorNode->RightNode = removingNode->RightNode;
		successorNode->Color = removingNode->Color;

		if (removingNode->LeftNode != NullPtr)
		{
			removingNode->LeftNode->ParentNode = successorNode;
		}

		if (removingNode->RightNode != NullPtr)
		{
			removingNode->RightNode->ParentNode = successorNode;
		}

		if (removingNode->ParentNode == NullPtr)
		{
			_rootNode = successorNode;
		}
		else
		{
			if (removingNode->ParentNode->LeftNode == removingNode)
			{
				removingNode->ParentNode->LeftNode = successorNode;
			}
			else
			{
				removingNode->ParentNode->RightNode = successorNode;
			}
		}
	}
	else
	{
		if (successorNode->Color == TreeMapNodeColor::Black)
		{
			FixBalanceAfterRemove(successorChildNode, successorChildNodeParent);
		}
	}

	delete removingNode;

	_count--;
}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V> * BaseTreeMap<K, V>::FindNode(const K & key) const
{
	if (_lastFoundNode != NullPtr && _lastFoundNode->Pair.GetKey() == key)
	{
		return _lastFoundNode;
	}

	if (_rootNode == NullPtr)
	{
		return NullPtr;
	}

	TreeMapNode<K, V> * currentNode = _rootNode;
	while (currentNode != NullPtr)
	{
		if (key == currentNode->Pair.GetKey())
		{
			_lastFoundNode = currentNode;
			return currentNode;
		}
		else
		{
			if (key < currentNode->Pair.GetKey())
			{
				currentNode = currentNode->LeftNode;
			}
			else
			{
				currentNode = currentNode->RightNode;
			}
		}
	}

	ResetLastFoundNode();
	return NullPtr;
}

//===========================================================================//

template<typename K, typename V>
TreeMapNode<K, V> * BaseTreeMap<K, V>::GetNode(const K & key) const
{
	TreeMapNode<K, V> * node = FindNode(key);
	if (node != NullPtr)
	{
		return node;
	}
	else
	{
		INVALID_ARGUMENT_EXCEPTION("Requested node not found");
	}
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::FreeNodes(TreeMapNode<K, V> * node)
{
	if (node == NullPtr)
	{
		return;
	}

	if (node->LeftNode != NullPtr)
	{
		FreeNodes(node->LeftNode);
	}

	if (node->RightNode != NullPtr)
	{
		FreeNodes(node->RightNode);
	}

	if (node->ParentNode != NullPtr)
	{
		if (node == node->ParentNode->LeftNode)
		{
			node->ParentNode->LeftNode = NullPtr;
		}
		else
		{
			node->ParentNode->RightNode = NullPtr;
		}
	}

	delete node;
	node = NullPtr;

	_count--;
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::FixBalanceAfterInsert(TreeMapNode<K, V> * node)
{
	if (node == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("node == NullPtr");
	}

	while (node != _rootNode && node->ParentNode->Color == TreeMapNodeColor::Red)
	{
		//Если parent левый узел
		if (node->ParentNode == node->ParentNode->ParentNode->LeftNode)
		{
			TreeMapNode<K, V> * rightMirrorNode = node->ParentNode->ParentNode->RightNode;
			if (rightMirrorNode != NullPtr && rightMirrorNode->Color == TreeMapNodeColor::Red)
			{
				//Перекрашиваем узлы предыдущего уровня в черный цвет
				node->ParentNode->Color = TreeMapNodeColor::Black;
				rightMirrorNode->Color = TreeMapNodeColor::Black;

				//Перекрашиваем узл на два уровня выше в красный цвет
				node->ParentNode->ParentNode->Color = TreeMapNodeColor::Red;

				//Перемещаемся на два уровня вверх
				node = node->ParentNode->ParentNode;
			}
			else
			{
				//Если node правый узел
				if (node == node->ParentNode->RightNode)
				{
					node = node->ParentNode;
					LeftRotate(node);
				}

				node->ParentNode->Color = TreeMapNodeColor::Black;
				node->ParentNode->ParentNode->Color = TreeMapNodeColor::Red;
				RightRotate(node->ParentNode->ParentNode);
			}
		}
		else
		{
			TreeMapNode<K, V> * leftMirrorNode = node->ParentNode->ParentNode->LeftNode;
			if (leftMirrorNode != NullPtr && leftMirrorNode->Color == TreeMapNodeColor::Red)
			{
				//Перекрашиваем узлы предыдущего уровня в черный цвет
				node->ParentNode->Color = TreeMapNodeColor::Black;
				leftMirrorNode->Color = TreeMapNodeColor::Black;

				//Перекрашиваем узл на два уровня выше в красный цвет
				node->ParentNode->ParentNode->Color = TreeMapNodeColor::Red;

				//Перемещаемся на два уровня вверх
				node = node->ParentNode->ParentNode;
			}
			else
			{
				//Если node левый узел
				if (node == node->ParentNode->LeftNode)
				{
					node = node->ParentNode;
					RightRotate(node);
				}

				node->ParentNode->Color = TreeMapNodeColor::Black;
				node->ParentNode->ParentNode->Color = TreeMapNodeColor::Red;
				LeftRotate(node->ParentNode->ParentNode);
			}
		}
	}
	_rootNode->Color = TreeMapNodeColor::Black;
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::FixBalanceAfterRemove(TreeMapNode<K, V> * node, TreeMapNode<K, V> * nodeParent)
{
	while (node != _rootNode && (node == NullPtr || node->Color == TreeMapNodeColor::Black))
	{
		//Если узел левый
		if (node == nodeParent->LeftNode)
		{
			TreeMapNode<K, V> * brotherNode = nodeParent->RightNode;
			if (brotherNode->Color == TreeMapNodeColor::Red)
			{
				brotherNode->Color = TreeMapNodeColor::Black;
				nodeParent->Color = TreeMapNodeColor::Red;
				LeftRotate(nodeParent);
				brotherNode = nodeParent->RightNode;
			}

			if ((brotherNode->LeftNode == NullPtr || brotherNode->LeftNode->Color == TreeMapNodeColor::Black)
			        && (brotherNode->RightNode == NullPtr || brotherNode->RightNode->Color == TreeMapNodeColor::Black))
			{
				brotherNode->Color = TreeMapNodeColor::Red;
				node = nodeParent;
				nodeParent = node->ParentNode;
			}
			else
			{
				if (brotherNode->RightNode == NullPtr || brotherNode->RightNode->Color == TreeMapNodeColor::Black)
				{
					brotherNode->LeftNode->Color = TreeMapNodeColor::Black;
					brotherNode->Color = TreeMapNodeColor::Red;
					RightRotate(brotherNode);
					brotherNode = nodeParent->RightNode;
				}

				brotherNode->Color = nodeParent->Color;
				nodeParent->Color = TreeMapNodeColor::Black;
				if (brotherNode->RightNode != NullPtr)
				{
					brotherNode->RightNode->Color = TreeMapNodeColor::Black;
				}
				LeftRotate(nodeParent);
				node = _rootNode;
				nodeParent = node->ParentNode;
			}
		}
		else // Если узел правый
		{
			TreeMapNode<K, V> * brotherNode = nodeParent->LeftNode;
			if (brotherNode->Color == TreeMapNodeColor::Red)
			{
				brotherNode->Color = TreeMapNodeColor::Black;
				nodeParent->Color = TreeMapNodeColor::Red;
				RightRotate(nodeParent);
				brotherNode = nodeParent->LeftNode;
			}

			if ((brotherNode->RightNode == NullPtr || brotherNode->RightNode->Color == TreeMapNodeColor::Black)
			        && (brotherNode->LeftNode == NullPtr || brotherNode->LeftNode->Color == TreeMapNodeColor::Black))
			{
				brotherNode->Color = TreeMapNodeColor::Red;
				node = nodeParent;
				nodeParent = node->ParentNode;
			}
			else
			{
				if (brotherNode->LeftNode == NullPtr || brotherNode->LeftNode->Color == TreeMapNodeColor::Black)
				{
					brotherNode->RightNode->Color = TreeMapNodeColor::Black;
					brotherNode->Color = TreeMapNodeColor::Red;
					LeftRotate(brotherNode);
					brotherNode = nodeParent->LeftNode;
				}

				brotherNode->Color = nodeParent->Color;
				nodeParent->Color = TreeMapNodeColor::Black;
				if (brotherNode->LeftNode != NullPtr)
				{
					brotherNode->LeftNode->Color = TreeMapNodeColor::Black;
				}
				RightRotate(nodeParent);
				node = _rootNode;
				nodeParent = node->ParentNode;
			}
		}
	}

	if (node != NullPtr)
	{
		node->Color = TreeMapNodeColor::Black;
	}
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::LeftRotate(TreeMapNode<K, V> * node)
{
	if (node == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("node == NullPtr");
	}

	TreeMapNode<K, V> * rightNode = node->RightNode;

	node->RightNode = rightNode->LeftNode;
	if (rightNode->LeftNode != NullPtr)
	{
		rightNode->LeftNode->ParentNode = node;
	}

	if (rightNode != NullPtr)
	{
		rightNode->ParentNode = node->ParentNode;
	}

	if (node->ParentNode != NullPtr)
	{
		//Если узел левый
		if (node == node->ParentNode->LeftNode)
		{
			node->ParentNode->LeftNode = rightNode;
		}
		else
		{
			node->ParentNode->RightNode = rightNode;
		}
	}
	else
	{
		_rootNode = rightNode;
	}

	rightNode->LeftNode = node;
	if (node != NullPtr)
	{
		node->ParentNode = rightNode;
	}
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::RightRotate(TreeMapNode<K, V> * node)
{
	if (node == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("node == NullPtr");
	}

	TreeMapNode<K, V> * leftNode = node->LeftNode;

	node->LeftNode = leftNode->RightNode;
	if (leftNode->RightNode != NullPtr)
	{
		leftNode->RightNode->ParentNode = node;
	}

	if (leftNode != NullPtr)
	{
		leftNode->ParentNode = node->ParentNode;
	}

	if (node->ParentNode != NullPtr)
	{
		//Если узел левый
		if (node == node->ParentNode->RightNode)
		{
			node->ParentNode->RightNode = leftNode;
		}
		else
		{
			node->ParentNode->LeftNode = leftNode;
		}
	}
	else
	{
		_rootNode = leftNode;
	}

	leftNode->RightNode = node;
	if (node != NullPtr)
	{
		node->ParentNode = leftNode;
	}
}

//===========================================================================//

template<typename K, typename V>
void BaseTreeMap<K, V>::ResetLastFoundNode() const
{
	_lastFoundNode = NullPtr;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
void SimpleTypeMoveElementsAlgorithm<T>::Copy(T * destinationDataPtr, integer destinationIndex, const T * sourceDataPtr,
        integer sourceIndex, integer size)
{
	//В sizeof передан тип, а не переменная, так как при изменения типа переменной тип изменится и в шаблоне
	Memory::Copy(destinationDataPtr + destinationIndex, sourceDataPtr + sourceIndex, size * sizeof(T));
}

//===========================================================================//

template<typename T>
void SimpleTypeMoveElementsAlgorithm<T>::MoveForward(T * dataPtr, integer index, integer destinationOffset,
        integer size)
{
	Memory::Move(dataPtr + index + destinationOffset, dataPtr + index, (size - index) * sizeof(T));
}

//===========================================================================//

template<typename T>
void SimpleTypeMoveElementsAlgorithm<T>::MoveBackward(T * dataPtr, integer index, integer offset, integer size)
{
	Memory::Move(dataPtr + index, dataPtr + index + offset, (size - index - offset) * sizeof(T));
}

//===========================================================================//

}//namespace


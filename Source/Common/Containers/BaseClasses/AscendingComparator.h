//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Comparator сравнивающий объекты a < b
//

#pragma once
#ifndef ASCENDING_COMPARATOR_H
#define ASCENDING_COMPARATOR_H

#include "IComparator.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class AscendingComparator : public IComparator<T>
{
public:
	AscendingComparator();
	virtual ~AscendingComparator();

	bool Compare(const T & leftObject, const T & rightObject) const;

private:
	AscendingComparator(const AscendingComparator &) = delete;
	AscendingComparator & operator =(const AscendingComparator &) = delete;
};

//===========================================================================//

}//namespace

#include "AscendingComparator.inl"

#endif // ASCENDING_COMPARATOR_H

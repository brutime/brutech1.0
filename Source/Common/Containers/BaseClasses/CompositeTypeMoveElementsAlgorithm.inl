//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
void CompositeTypeMoveElementsAlgorithm<T>::Copy(T * destinationDataPtr, integer destinationIndex,
        const T * sourceDataPtr, integer sourceIndex, integer size)
{
	for (integer i = 0; i < size; i++)
	{
		destinationDataPtr[i + destinationIndex] = sourceDataPtr[i + sourceIndex];
	}
}

//===========================================================================//

template<typename T>
void CompositeTypeMoveElementsAlgorithm<T>::MoveForward(T * dataPtr, integer index, integer destinationOffset,
        integer size)
{
	for (integer i = size; i > index; i--)
	{
		dataPtr[i + destinationOffset - 1] = dataPtr[i - 1];
	}
}

//===========================================================================//

template<typename T>
void CompositeTypeMoveElementsAlgorithm<T>::MoveBackward(T * dataPtr, integer index, integer offset, integer size)
{
	for (integer i = index; i < size - offset; i++)
	{
		dataPtr[i] = dataPtr[i + offset];
	}
}

//===========================================================================//

}//namespace


//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: перемещение эллемнтов массива составного типа
//

#pragma once
#ifndef COMPOSITE_TYPE_MOVE_ELEMENTS_ALGORITHM_H
#define COMPOSITE_TYPE_MOVE_ELEMENTS_ALGORITHM_H

#include "../../Types/BaseTypes.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class CompositeTypeMoveElementsAlgorithm
{
public:
	static void Copy(T * destinationDataPtr, integer destinationIndex, const T * sourceDataPtr, integer sourceIndex,
	                 integer size);

	static void MoveForward(T * dataPtr, integer index, integer destinationOffset, integer size);
	static void MoveBackward(T * dataPtr, integer index, integer offset, integer size);

private:
	CompositeTypeMoveElementsAlgorithm() = delete;
	CompositeTypeMoveElementsAlgorithm(const CompositeTypeMoveElementsAlgorithm &) = delete;
	~CompositeTypeMoveElementsAlgorithm() = delete;
	CompositeTypeMoveElementsAlgorithm & operator =(const CompositeTypeMoveElementsAlgorithm &) = delete;
};

//===========================================================================//

}//namespace

#include "CompositeTypeMoveElementsAlgorithm.inl"

#endif // COMPOSITE_TYPE_MOVE_ELEMENTS_ALGORITHM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: узел красно-черного дерева
//

#pragma once
#ifndef TREE_MAP_NODE_H
#define TREE_MAP_NODE_H

#include "../Enums/TreeMapNodeColor.h"
#include "../KeyValuePair.h"

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
class TreeMapNode
{
public:
	TreeMapNode(const K & key, const V & value);
	TreeMapNode(K && key, const V & value);
	TreeMapNode(const K & key, V && value);
	TreeMapNode(K && key, V && value);
	~TreeMapNode();

	TreeMapNode<K, V> * ParentNode;
	TreeMapNode<K, V> * LeftNode;
	TreeMapNode<K, V> * RightNode;
	TreeMapNodeColor Color;

	KeyValuePair<K, V> Pair;

private:
	TreeMapNode(const TreeMapNode &) = delete;
	TreeMapNode<K, V> & operator =(const TreeMapNode &) = delete;

	//для тестирования
	//byte * data;
};

//===========================================================================//

}//namesapce

#include "TreeMapNode.inl"

#endif // TREE_MAP_NODE_H

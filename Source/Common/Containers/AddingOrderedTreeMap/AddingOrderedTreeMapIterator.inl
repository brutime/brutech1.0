//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapIterator<K, V>::AddingOrderedTreeMapIterator(const AddingOrderedTreeMap<K, V> * owner,
        integer cursor) :
	_owner(owner),
	_cursor(cursor)
{
#ifdef DEBUGGING
	if (owner == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("owner == NullPtr");
	}
#endif
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapIterator<K, V>::AddingOrderedTreeMapIterator(const AddingOrderedTreeMapIterator<K, V> & other) :
	_owner(other._owner),
	_cursor(other._cursor)
{
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapIterator<K, V>::~AddingOrderedTreeMapIterator()
{
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapIterator<K, V> & AddingOrderedTreeMapIterator<K, V>::operator =(
    const AddingOrderedTreeMapIterator<K, V> & other)
{
	if (this == &other)
	{
		return *this;
	}

	_owner = other._owner;
	_cursor = other._cursor;

	return *this;
}

//===========================================================================//

template<typename K, typename V>
bool AddingOrderedTreeMapIterator<K, V>::operator !=(const AddingOrderedTreeMapIterator<K, V> & someIterator) const
{
	return _owner != someIterator._owner || _cursor != someIterator._cursor;
}

//===========================================================================//

template<typename K, typename V>
const AddingOrderedTreeMapIterator<K, V> & AddingOrderedTreeMapIterator<K, V>::operator ++()
{
	Next();
	return *this;
}

//===========================================================================//

template<typename K, typename V>
const KeyValuePair<K, V> & AddingOrderedTreeMapIterator<K, V>::operator *() const
{
	return GetCurrent();
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMapIterator<K, V>::First()
{
	_cursor = 0;
}

//===========================================================================//

template<typename K, typename V>
bool AddingOrderedTreeMapIterator<K, V>::HasNext() const
{
	return _cursor < _owner->_ordinalArray.GetCount();
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMapIterator<K, V>::Next()
{
#ifdef DEBUGGING
	if (!HasNext())
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION("Can't read beyond the end of tree");
	}
#endif

	_cursor++;
}

//===========================================================================//

template<typename K, typename V>
const KeyValuePair<K, V> & AddingOrderedTreeMapIterator<K, V>::GetCurrent() const
{
#ifdef DEBUGGING
	if (_cursor < 0 || _cursor >= _owner->_ordinalArray.GetCount())
	{
		ITERATOR_OUT_OF_BOUNDS_EXCEPTION(string::Format("Iterator _cursor out of bounds: {0} ([0..{1}])",
		                                 _cursor, _owner->_ordinalArray.GetCount()));
	}
#endif

	return _owner->_ordinalArray[_cursor]->Pair;
}

//===========================================================================//

}// namespace

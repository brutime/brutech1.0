//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: итератор для словаря упорядоченного, по порядку добавления
//

#pragma once
#ifndef ADDING_ORDERED_TREE_MAP_ITERATOR_H
#define ADDING_ORDERED_TREE_MAP_ITERATOR_H

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
class AddingOrderedTreeMap;

//===========================================================================//

template<typename K, typename V>
class AddingOrderedTreeMapIterator
{
	friend class AddingOrderedTreeMap<K, V> ;

public:
	AddingOrderedTreeMapIterator(const AddingOrderedTreeMapIterator<K, V> & other);
	virtual ~AddingOrderedTreeMapIterator();

	AddingOrderedTreeMapIterator<K, V> & operator =(const AddingOrderedTreeMapIterator<K, V> & other);

	bool operator !=(const AddingOrderedTreeMapIterator<K, V> & someIterator) const;
	const AddingOrderedTreeMapIterator<K, V> & operator ++();
	const KeyValuePair<K, V> & operator *() const;

	virtual void First();
	virtual bool HasNext() const;
	virtual void Next();

	const KeyValuePair<K, V> & GetCurrent() const;

private:
	AddingOrderedTreeMapIterator(const AddingOrderedTreeMap<K, V> * owner, integer cursor);

	const AddingOrderedTreeMap<K, V> * _owner;
	integer _cursor;

	friend class TestAddingOrderedTreeMapCreateIterator;
	friend class TestAddingOrderedTreeMapBegin;
	friend class TestAddingOrderedTreeMapEnd;

	friend class TestAddingOrderedTreeMapIteratorConstructor;
	friend class TestAddingOrderedTreeMapIteratorForEmpty;
	friend class TestAddingOrderedTreeMapIteratorForOnePair;
	friend class TestAddingOrderedTreeMapIteratorFor;
	friend class TestAddingOrderedTreeMapIteratorRangeForEmpty;
	friend class TestAddingOrderedTreeMapIteratorRangeForOnePair;
	friend class TestAddingOrderedTreeMapIteratorRangeFor;
	friend class TestAddingOrderedTreeMapIteratorExceptions;
};

//===========================================================================//

}// namesapace

#include "AddingOrderedTreeMapIterator.inl"

#endif // ADDING_ORDERED_TREE_MAP_ITERATOR_H

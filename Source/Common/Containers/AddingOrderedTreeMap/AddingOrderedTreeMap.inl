//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: реализация с помощью красно-черного дерева
//

namespace Bru
{

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMap<K, V>::AddingOrderedTreeMap() :
	BaseTreeMap<K, V>(),
	_ordinalArray()
{
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMap<K, V>::AddingOrderedTreeMap(std::initializer_list<KeyValuePair<K, V>> list) :
	BaseTreeMap<K, V>(),
	_ordinalArray()
{
	for (const KeyValuePair<K, V> & pair : list)
	{
		Add(pair);
	}
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMap<K, V>::AddingOrderedTreeMap(const AddingOrderedTreeMap<K, V> & other) :
	BaseTreeMap<K, V>(),
	_ordinalArray()
{
	DeepCopy(other);
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMap<K, V>::AddingOrderedTreeMap(AddingOrderedTreeMap<K, V> && other): BaseTreeMap<K, V>(),
	_ordinalArray(std::move(other._ordinalArray))
{
	BaseTreeMap<K, V>::_rootNode = other._rootNode;
	BaseTreeMap<K, V>::_lastFoundNode = other._lastFoundNode;
	BaseTreeMap<K, V>::_count = other._count;

	other._rootNode = NullPtr;
	other._lastFoundNode = NullPtr;
	other._count = 0;
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMap<K, V>::~AddingOrderedTreeMap()
{
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMap<K, V> & AddingOrderedTreeMap<K, V>::operator =(const AddingOrderedTreeMap<K, V> & other)
{
	if (this == &other)
	{
		return *this;
	}

	if (!this->IsEmpty())
	{
		Clear();
	}

	DeepCopy(other);

	return *this;
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMap<K, V> & AddingOrderedTreeMap<K, V>::operator =(AddingOrderedTreeMap<K, V> && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(BaseTreeMap<K, V>::_rootNode, other._rootNode);
	std::swap(BaseTreeMap<K, V>::_lastFoundNode, other._lastFoundNode);
	std::swap(BaseTreeMap<K, V>::_count, other._count);
	std::swap(_ordinalArray, other._ordinalArray);

	return *this;
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Add(const K & key, const V & value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	TreeMapNode<K, V> * insertedNode = this->InsertNode(new TreeMapNode<K, V>(key, value));
	_ordinalArray.Add(insertedNode);
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Add(K && key, const V & value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	TreeMapNode<K, V> * insertedNode = this->InsertNode(new TreeMapNode<K, V>(std::move(key), value));
	_ordinalArray.Add(insertedNode);
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Add(const K & key, V && value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	TreeMapNode<K, V> * insertedNode = this->InsertNode(new TreeMapNode<K, V>(key, std::move(value)));
	_ordinalArray.Add(insertedNode);
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Add(K && key, V && value)
{
	if (this->Contains(key))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	TreeMapNode<K, V> * insertedNode = this->InsertNode(new TreeMapNode<K, V>(std::move(key), std::move(value)));
	_ordinalArray.Add(insertedNode);
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Add(const KeyValuePair<K, V> & keyValuePair)
{
	if (this->Contains(keyValuePair.GetKey()))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	TreeMapNode<K, V> * insertedNode = this->InsertNode(
	                                       new TreeMapNode<K, V>(keyValuePair.GetKey(), keyValuePair.GetValue()));
	_ordinalArray.Add(insertedNode);
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Add(KeyValuePair<K, V> && keyValuePair)
{
	if (this->Contains(keyValuePair.GetKey()))
	{
		INVALID_ARGUMENT_EXCEPTION("Value with the same key has already been added");
	}

	TreeMapNode<K, V> * insertedNode = this->InsertNode(new TreeMapNode<K, V>(std::move(keyValuePair.GetKey()), std::move(keyValuePair.GetValue())));
	_ordinalArray.Add(insertedNode);
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Remove(const K & key)
{
	TreeMapNode<K, V> * node = this->FindNode(key);
	if (node != NullPtr)
	{
		_ordinalArray.Remove(node);
		this->RemoveNode(node);
	}
	else
	{
		INVALID_ARGUMENT_EXCEPTION("Requested element not found");
	}
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::Clear()
{
	_ordinalArray.Clear();
	BaseTreeMap<K, V>::Clear();
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapIterator<K, V> AddingOrderedTreeMap<K, V>::CreateIterator() const
{
	return begin();
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapIterator<K, V> AddingOrderedTreeMap<K, V>::begin() const
{
	return AddingOrderedTreeMapIterator<K, V>(this, 0);
}

//===========================================================================//

template<typename K, typename V>
AddingOrderedTreeMapIterator<K, V> AddingOrderedTreeMap<K, V>::end() const
{
	return AddingOrderedTreeMapIterator<K, V>(this, this->GetCount());
}

//===========================================================================//

template<typename K, typename V>
void AddingOrderedTreeMap<K, V>::DeepCopy(const AddingOrderedTreeMap<K, V> & other)
{
	for (auto & pair : other)
	{
		Add(pair.GetKey(), pair.GetValue());
	}
}

//===========================================================================//

}//namespace

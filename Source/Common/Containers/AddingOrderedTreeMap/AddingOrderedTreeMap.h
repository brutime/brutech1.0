//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Cловарь с сортировкой в порядке добавления
//

#pragma once
#ifndef ADDING_ORDERED_TREE_MAP_H
#define ADDING_ORDERED_TREE_MAP_H

#include "../BaseClasses/BaseTreeMap.h"
#include "../Array/Array.h"
#include "AddingOrderedTreeMapIterator.h"

namespace Bru
{

//===========================================================================//

//для тестирования
template<typename K, typename V>
class TreeMapChecker;

//===========================================================================//

template<typename K, typename V>
class AddingOrderedTreeMap : public BaseTreeMap<K, V>
{
	friend class AddingOrderedTreeMapIterator<K, V> ;

public:
	AddingOrderedTreeMap();
	AddingOrderedTreeMap(std::initializer_list<KeyValuePair<K, V>> list);
	AddingOrderedTreeMap(const AddingOrderedTreeMap<K, V> & other);
	AddingOrderedTreeMap(AddingOrderedTreeMap<K, V> && other);

	virtual ~AddingOrderedTreeMap();

	AddingOrderedTreeMap<K, V> & operator =(const AddingOrderedTreeMap<K, V> & other);
	AddingOrderedTreeMap<K, V> & operator =(AddingOrderedTreeMap<K, V> && other);

	///Добавление пары (ключ, значение)
	virtual void Add(const K & key, const V & value);
	virtual void Add(K && key, const V & value);
	virtual void Add(const K & key, V && value);
	virtual void Add(K && key, V && value);

	virtual void Add(const KeyValuePair<K, V> & keyValuePair);
	virtual void Add(KeyValuePair<K, V> && keyValuePair);

	///Удаление элемента по ключу
	virtual void Remove(const K & key);

	///Очистка дерева
	virtual void Clear();

	AddingOrderedTreeMapIterator<K, V> CreateIterator() const;
	AddingOrderedTreeMapIterator<K, V> begin() const;
	AddingOrderedTreeMapIterator<K, V> end() const;

protected:
	void DeepCopy(const AddingOrderedTreeMap<K, V> & other);

private:
	Array<TreeMapNode<K, V> *> _ordinalArray;

	//для тестирования
	friend class TreeMapChecker<K, V>;

	friend class TestAddingOrderedTreeMapGetters;
	friend class TestAddingOrderedTreeMapDefaulConstructor;
	friend class TestAddingOrderedTreeListInitializerListConstructor;
	friend class TestAddingOrderedTreeMapCopyConstructor;
	friend class TestAddingOrderedTreeMapMoveConstructor;
	friend class TestAddingOrderedTreeMapCopyOperator;
	friend class TestAddingOrderedTreeMapCopyOperatorSelf;
	friend class TestAddingOrderedTreeMapMoveOperator;
	friend class TestAddingOrderedTreeMapAddKeyValue;
	friend class TestAddingOrderedTreeMapAddMoveKey;
	friend class TestAddingOrderedTreeMapAddMoveValue;
	friend class TestAddingOrderedTreeMapAddMoveKeyValue;
	friend class TestAddingOrderedTreeMapAddKeyValuePair;
	friend class TestAddingOrderedTreeMapAddMoveKeyValuePair;
	friend class TestAddingOrderedTreeMapRemove;
	friend class TestAddingOrderedTreeMapOperatorSetIntegerKey;
	friend class TestAddingOrderedTreeMapOperatorSetStringKey;
	friend class TestAddingOrderedTreeMapOperatorGetIntegerKey;
	friend class TestAddingOrderedTreeMapOperatorGetStringKey;
	friend class TestAddingOrderedTreeMapContains;
	friend class TestAddingOrderedTreeMapClear;
	friend class TestAddingOrderedTreeMapLastFoundNode;

	friend class TestAddingOrderedTreeMapIteratorForOnePair;
	friend class TestAddingOrderedTreeMapIteratorFor;
	friend class TestAddingOrderedTreeMapIteratorRangeForEmpty;
	friend class TestAddingOrderedTreeMapIteratorRangeForOnePair;
	friend class TestAddingOrderedTreeMapIteratorRangeFor;
};

//===========================================================================//

}//namespace

#include "AddingOrderedTreeMap.inl"

#endif // TREE_MAP_H

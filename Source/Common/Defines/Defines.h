//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовые определения для препроцессора
//

#pragma once
#ifndef DEFINES_H
#define DEFINES_H

namespace Bru
{

//===========================================================================//

#define DEBUGGING
#define DEBUGGING_STRINGS
//#define DEBUGGING_POINTERS

//===========================================================================//

#define WINDOWS   1
#define LINUX     2
#define MAC       3
#define XBOX_360  4
#define SONY_PS_3 5

#define PLATFORM WINDOWS //{WINDOWS, LINUX, MAC, XBOX_360, SONY_PS_3}
//===========================================================================//

#define X32 1
#define X64	2

#define ARCHITECTURE X32 //{X32, X64}
//===========================================================================//

#define OPEN_GL   1
#define DIRECT_3D 2

#define GRAPHIC_API OPEN_GL // {OPEN_GL, DIRECT_3D}
//===========================================================================//

#define NullPtr nullptr
#define NullPtrType std::nullptr_t
#define NullTerminator '\0'
//На случай другий компиляторов
#define FILE_NAME __FILE__
#define METHOD_NAME __PRETTY_FUNCTION__
#define FILE_LINE __LINE__

//===========================================================================//

} // namespace Bru

#endif // DEFINES_H

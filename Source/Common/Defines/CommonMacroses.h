//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Макросы. Чем их меньше тем лучше
//

#pragma once
#ifndef COMMON_MACROSES_H
#define COMMON_MACROSES_H

namespace Bru
{

//===========================================================================//

#define SIZE_OF(x) static_cast<integer>(sizeof(x))
#define PREFETCH(x) __builtin_prefetch(x,0,0)

//===========================================================================//

} // namespace Bru

#endif // COMMON_MACROSES_H

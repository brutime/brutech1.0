//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Обертка для простых типов, которая дает возможность просвоить им NullPtr
//

#pragma once
#ifndef NULLABLE_H
#define NULLABLE_H

#include "../Exceptions/NullPointerException.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class Nullable
{
public:
	Nullable();
	Nullable(NullPtrType);
	Nullable(const T & value);
	Nullable(const Nullable<T> & other);
	~Nullable();

	Nullable<T> & operator =(NullPtrType);
	Nullable<T> & operator =(const T & value);
	Nullable<T> & operator =(const Nullable<T> & other);

	bool operator ==(NullPtrType) const;
	bool operator ==(const T & value) const;
	bool operator ==(const Nullable<T> & other) const;

	bool operator !=(NullPtrType) const;
	bool operator !=(const T & value) const;
	bool operator !=(const Nullable<T> & other) const;

	bool HasValue() const;
	const T & GetValue() const;

	const T & GetValueOrDefault(const T & defaultValue) const;

private:
	T _value;
	bool _isNull;

	friend class TestNullableDefaultConstructor;
	friend class TestNullableNullPtrConstructor;
	friend class TestNullableConstructor;
	friend class TestNullableCopyConstructor;
	friend class TestNullableNullPtrCopyOperator;
	friend class TestNullableValueCopyOperator;
	friend class TestNullableCopyOperator;
	friend class TestNullableNullNulableCopyOperator;
};

//===========================================================================//

template<typename T>
std::ostream & operator <<(std::ostream & stream, const Nullable<T> & nullable);

//===========================================================================//

template<typename T>
bool operator ==(NullPtrType, const Nullable<T> & nullable);

//===========================================================================//

template<typename T>
bool operator !=(NullPtrType, const Nullable<T> & nullable);

//===========================================================================//

} // namespace Bru

//===========================================================================//

#include "Nullable.inl"

#endif // NULLABLE_H

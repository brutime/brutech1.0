//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Converter.h"

#include "String/String.h"

namespace Bru
{

//===========================================================================//

const integer Converter::DefaultConvertingPrecision = 4;
const integer Converter::MaxFloatConvertingPrecision = 10;
const string Converter::NullPtrStringRepresentation = "NULLPTR";

//===========================================================================//

bool Converter::ToBool(const string & value)
{
	if (value.ToUpper() == "FALSE")
	{
		return false;
	}
	else if (value.ToUpper() == "TRUE")
	{
		return true;
	}
	else if (ToInteger(value) == 0)
	{
		return false;
	}

	return true;
}

//===========================================================================//

char Converter::ToChar(const string & value)
{
#ifdef DEBUGGING
	if (value.GetLengthInBytes() > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("value's length in bytes out of bounds: {0} ([0..1])", (value.GetLengthInBytes())));
	}
#endif // DEBUGGING
	return value.ToChars()[0];
}

//===========================================================================//

byte Converter::ToByte(const string & value)
{
	integer result = ToInteger(value);

#ifdef DEBUGGING
	if (result < 0 || result > 255)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value out of bounds: {0} ([0..255])", result));
	}
#endif // DEBUGGING
	return byte(result);
}

//===========================================================================//

ushort Converter::ToUnsignedShort(const string & value)
{
	return ToUnsignedType<ushort>(value);
}

//===========================================================================//

uint Converter::ToUnsignedInteger(const string & value)
{
	return ToUnsignedType<uint>(value);
}

//===========================================================================//

ulong Converter::ToUnsignedLong(const string & value)
{
	return ToUnsignedType<ulong>(value);
}

//===========================================================================//

integer Converter::ToInteger(const string & value, NumberBase base)
{
	return ToType<integer>(value, base);
}

//===========================================================================//

int64 Converter::ToInteger64(const string & value, NumberBase base)
{
	return ToType<int64>(value, base);
}

//===========================================================================//

float Converter::ToFloat(const string & value)
{
	return ToType<float>(value, NumberBase::Decimal);
}

//===========================================================================//

double Converter::ToDouble(const string & value)
{
	return ToType<double>(value, NumberBase::Decimal);
}

//===========================================================================//

string Converter::ToString(bool value)
{
	if (value)
	{
		return "true";
	}

	return "false";
}

//===========================================================================//

string Converter::ToString(char value)
{
	return ToStringUsingOStringStream(value);
}

//===========================================================================//

string Converter::ToString(byte value)
{
	return ToStringUsingOStringStream<integer>(value);
}

//===========================================================================//

string Converter::ToString(ushort value)
{
	return ToStringUsingOStringStream(value);
}

//===========================================================================//

string Converter::ToString(uint value)
{
	return ToStringUsingOStringStream(value);
}

//===========================================================================//

string Converter::ToString(ulong value)
{
	return ToStringUsingOStringStream(value);
}

//===========================================================================//

string Converter::ToString(integer value)
{
	return ToStringUsingOStringStream(value);
}

//===========================================================================//

string Converter::ToString(int64 value)
{
	return ToStringUsingOStringStream(value);
}

//===========================================================================//

string Converter::ToString(float value, integer precision)
{
#ifdef DEBUGGING
	if (precision < 0 || precision > MaxFloatConvertingPrecision)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("precision out of bounds: {0} ([0..{1}])", precision, MaxFloatConvertingPrecision));
	}
#endif // DEBUGGING
	std::ostringstream stream;
	stream << std::fixed;
	stream.precision(precision);
	stream << value;
	return string(stream.str().c_str());
}

//===========================================================================//

string Converter::ToString(double value, integer precision)
{
#ifdef DEBUGGING
	if (precision < 0 || precision > MaxFloatConvertingPrecision)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("precision out of bounds: {0} ([0..{1}])", precision, MaxFloatConvertingPrecision));
	}
#endif // DEBUGGING
	std::ostringstream stream;
	stream << std::fixed;
	stream.precision(precision);
	stream << value;
	return string(stream.str().c_str());
}

//===========================================================================//

string Converter::ToString(string value)
{
	return value;
}

//===========================================================================//

}//namespace

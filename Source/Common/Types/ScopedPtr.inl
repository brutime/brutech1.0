//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

//===========================================================================//

template<class T>
ScopedPtr<T>::ScopedPtr(T * someObject) :
	object(someObject)
{
}

//===========================================================================//

template<class T>
ScopedPtr<T>::~ScopedPtr()
{
	Release();
}

//===========================================================================//

template<class T>
inline T & ScopedPtr<T>::operator *() const
{
	if (object == NullPtr)
	{
		NULL_POINTER_EXCEPTION("Attempt to dereference NullPtr pointer");
	}

	return *object;
}

//===========================================================================//

template<class T>
inline T * ScopedPtr<T>::operator ->() const
{
	if (object == NullPtr)
	{
		NULL_POINTER_EXCEPTION("Attempt to dereference NullPtr pointer");
	}

	return object;
}

//===========================================================================//

template<class T>
inline void ScopedPtr<T>::Release()
{
	if (object != NullPtr)
	{
		delete object;
		object = NullPtr;
	}
}

//===========================================================================//

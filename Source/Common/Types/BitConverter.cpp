//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "BitConverter.h"

namespace Bru
{

//===========================================================================//

integer BitConverter::ToInteger(const Array<byte>::SimpleType & byteArray)
{
	if (byteArray.GetCount() > SIZE_OF(integer))
	{
		INVALID_ARGUMENT_EXCEPTION("Count of byteArray > integer size");
	}

	integer result = 0;
	Memory::Copy(&result, byteArray.GetData(), byteArray.GetCount());
	return result;
}

//===========================================================================//

Array<byte>::SimpleType BitConverter::ToBytes(integer value)
{
	integer bytesCount = SIZE_OF(value);
	return ToBytes(value, bytesCount);
}

//===========================================================================//

Array<byte>::SimpleType BitConverter::ToBytes(integer value, integer bytesCount)
{
	Array<byte>::SimpleType byteArray(bytesCount);
	Memory::Copy(byteArray.GetData(), &value, bytesCount);
	return byteArray;
}

//===========================================================================//

float BitConverter::ToFloat(const Array<byte>::SimpleType & byteArray)
{
	if (byteArray.GetCount() > SIZE_OF(float))
	{
		INVALID_ARGUMENT_EXCEPTION("Count of byteArray > float size");
	}

	float result = 0.0f;
	char * floatPtr = reinterpret_cast<char *>(&result) + SIZE_OF(float) - 1;
	for (integer i = 0; i < byteArray.GetCount(); i++)
	{
		*floatPtr = byteArray[i];
		floatPtr--;
	}
	return result;
}

//===========================================================================//

Array<byte>::SimpleType BitConverter::ToBytes(float value)
{
	integer bytesCount = SIZE_OF(value);
	return ToBytes(value, bytesCount);
}

//===========================================================================//

Array<byte>::SimpleType BitConverter::ToBytes(float value, integer bytesCount)
{
	Array<byte>::SimpleType byteArray(bytesCount);
	char * valuePtr = reinterpret_cast<char *>(&value) + SIZE_OF(value)- 1;
	for (integer i = 0; i < byteArray.GetCount(); i++)
	{
		byteArray[i] = *valuePtr;
		valuePtr--;
	}

	return byteArray;
}

//===========================================================================//

}//namespace

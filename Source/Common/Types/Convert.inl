//
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech value code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Шаблон, позволяющий статически преобразовывать типы
//

namespace Bru
{

//===========================================================================//

template<>
class Convert<string, bool>
{
public:
	Convert(const string & value) :
		_value(Converter::ToBool(value))
	{
	}

	operator bool() const
	{
		return _value;
	}

private:
	bool _value;
};

//===========================================================================//

template<>
class Convert<string, char>
{
public:
	Convert(const string & value) :
		_value(Converter::ToChar(value))
	{
	}

	operator char() const
	{
		return _value;
	}

private:
	char _value;
};

//===========================================================================//

template<>
class Convert<string, byte>
{
public:
	Convert(const string & value) :
		_value(Converter::ToByte(value))
	{
	}

	operator byte() const
	{
		return _value;
	}

private:
	byte _value;
};

//===========================================================================//

template<>
class Convert<string, ushort>
{
public:
	Convert(const string & value) :
		_value(Converter::ToUnsignedShort(value))
	{
	}

	operator ushort() const
	{
		return _value;
	}

private:
	ushort _value;
};

//===========================================================================//

template<>
class Convert<string, uint>
{
public:
	Convert(const string & value) :
		_value(Converter::ToUnsignedInteger(value))
	{
	}

	operator uint() const
	{
		return _value;
	}

private:
	uint _value;
};

//===========================================================================//

template<>
class Convert<string, ulong>
{
public:
	Convert(const string & value) :
		_value(Converter::ToUnsignedLong(value))
	{
	}

	operator ulong() const
	{
		return _value;
	}

private:
	ulong _value;
};

//===========================================================================//

template<>
class Convert<string, integer>
{
public:
	Convert(const string & value) :
		_value(Converter::ToInteger(value))
	{
	}

	operator integer() const
	{
		return _value;
	}

private:
	integer _value;
};

//===========================================================================//

template<>
class Convert<string, int64>
{
public:
	Convert(const string & value) :
		_value(Converter::ToInteger64(value))
	{
	}

	operator int64() const
	{
		return _value;
	}

private:
	int64 _value;
};

//===========================================================================//

template<>
class Convert<string, float>
{
public:
	Convert(const string & value) :
		_value(Converter::ToFloat(value))
	{
	}

	operator float() const
	{
		return _value;
	}

private:
	float _value;
};

//===========================================================================//

template<typename T>
class Convert<string, T>
{
public:
	Convert(const string & value) :
		_value(value)
	{
	}

	operator T() const
	{
		return _value;
	}

private:
	T _value;
};

//===========================================================================//

template<typename T>
class Convert<string, Nullable<T>>
{
public:
	Convert(const string & value) :
		_value(NullPtr)
	{
		if (value.ToUpper() != Converter::NullPtrStringRepresentation)
		{
			_value = value;
		}
	}

	operator Nullable<T>() const
	{
		return _value;
	}

private:
	Nullable<T> _value;
};

//===========================================================================//

template<>
class Convert<string, string>
{
public:
	Convert(const string & value) :
		_value(value)
	{
	}

	operator string() const
	{
		return _value;
	}

private:
	string _value;
};

//===========================================================================//

template<>
class Convert<void, string>
{
public:
	operator string() const
	{
		return string::Empty;
	}
};

//===========================================================================//

template<>
class Convert<NullPtrType, string>
{
public:
	Convert(NullPtrType)
	{
	}

	operator string() const
	{
		return Converter::NullPtrStringRepresentation;
	}
};

//===========================================================================//

template<typename T>
class Convert<T, string>
{
public:
	Convert(const T & value) :
		_value(Converter::ToString(value))
	{
	}

	operator string() const
	{
		return _value;
	}

private:
	string _value;
};

//===========================================================================//

}

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Эта штука позволяет создавать SharedPtr из raw указателей, 
//              которыми владеют другие SharedPtr
//

#pragma once
#ifndef PTR_FROM_THIS_MIX_IN_H
#define PTR_FROM_THIS_MIX_IN_H

#include "BaseTypes.h"
#include "SharedPtr.h"
#include "WeakPtr.h"
#include "../Exceptions/InvalidStateException.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class PtrFromThisMixIn
{
	template<typename U> friend class SharedPtr;

public:
	SharedPtr<T> GetSharedPtr() const;
	WeakPtr<T> GetWeakPtr() const;

protected:
	PtrFromThisMixIn();
	PtrFromThisMixIn(const PtrFromThisMixIn &);
	virtual ~PtrFromThisMixIn();

	PtrFromThisMixIn & operator =(const PtrFromThisMixIn &);

private:
	template<typename U>
	void SetWeakPtrOnThis(const SharedPtr<U> * sharedPtr);

	WeakPtr<T> _weakPtrOnThis;

	//для тестирования
	friend class TestPtrFromThisMixInConstructor;
	friend class TestPtrFromThisMixInCopyConstructor;
	friend class TestPtrFromThisMixInInheritanceCopyConstructor;
	friend class TestPtrFromThisMixInCopyOperator;
	friend class TestPtrFromThisMixInInheritanceCopyOperator;
	friend class TestPtrFromThisMixInGetSharedPtr;
	friend class TestPtrFromThisMixInGetInheritanceSharedPtr;
	friend class TestPtrFromThisMixInGetCastedSharedPtr;
	friend class TestPtrFromThisMixInGetWeakPtr;
	friend class TestPtrFromThisMixInGetInheritanceWeakPtr;
	friend class TestPtrFromThisMixInGetCastedWeakPtr;
};

//===========================================================================//

#include "PtrFromThisMixIn.inl"

} //namespace

#endif // PTR_FROM_THIS_MIX_IN_H

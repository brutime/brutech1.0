//
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech value code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс конвертер типов
//

#pragma once
#ifndef CONVERTER_H
#define CONVERTER_H

#include <sstream>

#include "../Enums/NumberBase.h"
#include "BaseTypes.h"
#include "String/String.h"
#include "../Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

class Converter
{
public:
	static const integer DefaultConvertingPrecision;
	static const integer MaxFloatConvertingPrecision;
	static const string NullPtrStringRepresentation;

	static bool ToBool(const string & value);
	static char ToChar(const string & value);
	static byte ToByte(const string & value);
	static ushort ToUnsignedShort(const string & value);
	static uint ToUnsignedInteger(const string & value);
	static ulong ToUnsignedLong(const string & value);
	static integer ToInteger(const string & value, NumberBase base = NumberBase::Decimal);
	static int64 ToInteger64(const string & value, NumberBase base = NumberBase::Decimal);
	static float ToFloat(const string & value);
	static double ToDouble(const string & value);

	static string ToString(bool value);
	static string ToString(char value);
	static string ToString(byte value);
	static string ToString(ushort value);
	static string ToString(uint value);
	static string ToString(ulong value);
	static string ToString(integer value);
	static string ToString(int64 value);
	static string ToString(float value, integer precision = DefaultConvertingPrecision);
	static string ToString(double value, integer precision = DefaultConvertingPrecision);
	static string ToString(string value);
	
	///специализация для enum'ов
	template<typename T, typename = typename std::enable_if<std::is_enum<T>::value>::type>
	static string ToString(const T & value);
	
	///специализация для всех типов кроме примитивов и enum'ов
	template<typename T, typename = typename std::enable_if<!std::is_enum<T>::value>::type>
	static string ToString(T value);

private:
	template<typename T>
	static T ToType(const string & value, NumberBase base);

	template<typename T>
	static T ToUnsignedType(const string & value);
	
	template<typename T>
	static string ToStringUsingOStringStream(const T & value);

	Converter() = delete;
	Converter(const Converter &) = delete;
	~Converter() = delete;
	Converter & operator =(const Converter &) = delete;
};

//===========================================================================//

}

#include "Converter.inl"

#endif // CONVERTER_H

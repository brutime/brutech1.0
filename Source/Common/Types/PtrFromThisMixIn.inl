//
//    BruTech Source Code
//    Copyright (C) 2010-201, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

//===========================================================================//

template<typename T>
PtrFromThisMixIn<T>::PtrFromThisMixIn() :
	_weakPtrOnThis()
{
}

//===========================================================================//

template<typename T>
PtrFromThisMixIn<T>::PtrFromThisMixIn(const PtrFromThisMixIn &)
{
}

//===========================================================================//

template<typename T>
PtrFromThisMixIn<T>::~PtrFromThisMixIn()
{
}

//===========================================================================//

template<typename T>
PtrFromThisMixIn<T> & PtrFromThisMixIn<T>::operator =(const PtrFromThisMixIn &)
{
	return *this;
}

//===========================================================================//

template<typename T>
SharedPtr<T> PtrFromThisMixIn<T>::GetSharedPtr() const
{
	SharedPtr<T> sharedPtrOnThis(_weakPtrOnThis);

#ifdef DEBUGGING
	if (sharedPtrOnThis != this)
	{
		INVALID_STATE_EXCEPTION("sharedPtrOnThis != this");
	}
#endif

	return sharedPtrOnThis;
}

//===========================================================================//

template<typename T>
WeakPtr<T> PtrFromThisMixIn<T>::GetWeakPtr() const
{
	return _weakPtrOnThis;
}

//===========================================================================//

template<typename T> template<class U>
void PtrFromThisMixIn<T>::SetWeakPtrOnThis(const SharedPtr<U> * sharedPtr)
{
	if (_weakPtrOnThis == NullPtr)
	{
		_weakPtrOnThis = *sharedPtr;
	}
}

//===========================================================================//

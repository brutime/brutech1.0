//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Счетчик для указателей
//

namespace Bru
{

//===========================================================================//

void RefsCounter::IncrementSharedRefsCount()
{
	_sharedRefsCount++;
}

//===========================================================================//

void RefsCounter::DecrementSharedRefsCount()
{
#ifdef DEBUGGING
	if (_sharedRefsCount <= 0)
	{
		INVALID_STATE_EXCEPTION(string::Format("sharedRefsCount must be >= 1, but it is {0}", _sharedRefsCount));
	}
#endif

	_sharedRefsCount--;
}

//===========================================================================//

void RefsCounter::IncrementWeakRefsCount()
{
	_weakRefsCount++;
}

//===========================================================================//

void RefsCounter::DecrementWeakRefsCount()
{
#ifdef DEBUGGING
	if (_weakRefsCount <= 0)
	{
		INVALID_STATE_EXCEPTION(string::Format("_weakRefsCount must be >= 1, but it is {0}", _weakRefsCount));
	}
#endif

	_weakRefsCount--;
}

//===========================================================================//

bool RefsCounter::IsNoRefs() const
{
	return _sharedRefsCount == 0 && _weakRefsCount == 0;
}

//===========================================================================//

integer RefsCounter::GetSharedRefsCount() const
{
	return _sharedRefsCount;
}

//===========================================================================//

integer RefsCounter::GetWeakRefsCount() const
{
	return _weakRefsCount;
}

//===========================================================================//

} //namespace Bru

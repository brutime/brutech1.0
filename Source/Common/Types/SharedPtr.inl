//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
SharedPtr<T>::SharedPtr() :
	_objectPtr(NullPtr),
	_refsCounter(NullPtr)
{
}

//===========================================================================//

template<typename T>
SharedPtr<T>::SharedPtr(NullPtrType) :
	_objectPtr(NullPtr),
	_refsCounter(NullPtr)
{
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T>::SharedPtr(U * objectPtr) :
	_objectPtr(objectPtr),
	_refsCounter(NullPtr)
{
	if (_objectPtr != NullPtr)
	{
		_refsCounter = new RefsCounter(1, 0);
		EnableSharedFromThis(objectPtr);
	}
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T>::SharedPtr(T * objectPtr, const SharedPtr<U> & other) :
	_objectPtr(objectPtr),
	_refsCounter(other._refsCounter)
{
	if (_objectPtr != other._objectPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("Different object pointers");
	}	
	
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementSharedRefsCount();
	}
}

//===========================================================================//

template<typename T>
SharedPtr<T>::SharedPtr(const SharedPtr<T> & other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementSharedRefsCount();
	}
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T>::SharedPtr(const SharedPtr<U> & other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementSharedRefsCount();
	}
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T>::SharedPtr(const WeakPtr<U> & weakPtr) :
	_objectPtr(weakPtr._objectPtr),
	_refsCounter(weakPtr._refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementSharedRefsCount();
	}
}

//===========================================================================//

template<typename T>
SharedPtr<T>::SharedPtr(SharedPtr<T> && other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	other._objectPtr = NullPtr;
	other._refsCounter = NullPtr;
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T>::SharedPtr(SharedPtr<U> && other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	other._objectPtr = NullPtr;
	other._refsCounter = NullPtr;
}

//===========================================================================//

template<typename T>
SharedPtr<T>::~SharedPtr()
{
	Release();
}

//===========================================================================//

template<typename T>
SharedPtr<T> & SharedPtr<T>::operator =(NullPtrType)
{
	Release();

	return *this;
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T> & SharedPtr<T>::operator =(U * objectPtr)
{
	if (_objectPtr == objectPtr)
	{
		return *this;
	}

	if (objectPtr != NullPtr)
	{
		Redirect(objectPtr, new RefsCounter(1, 0));
		EnableSharedFromThis(objectPtr);
	}
	else
	{
		Redirect(NullPtr, NullPtr);
	}

	return *this;
}

//===========================================================================//

template<typename T>
SharedPtr<T> & SharedPtr<T>::operator =(const SharedPtr<T> & other)
{
	if (this == &other)
	{
		return *this;
	}

	Redirect(other._objectPtr, other._refsCounter);

	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementSharedRefsCount();
	}

	return *this;
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T> & SharedPtr<T>::operator =(const SharedPtr<U> & other)
{
	Redirect(other._objectPtr, other._refsCounter);

	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementSharedRefsCount();
	}

	return *this;
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T> & SharedPtr<T>::operator =(const WeakPtr<U> & weakPtr)
{
	Redirect(weakPtr._objectPtr, weakPtr._refsCounter);

	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementSharedRefsCount();
	}

	return *this;
}

//===========================================================================//

template<typename T>
SharedPtr<T> & SharedPtr<T>::operator =(SharedPtr<T> && other)
{
	std::swap(_objectPtr, other._objectPtr);
	std::swap(_refsCounter, other._refsCounter);

	return *this;
}

//===========================================================================//

template<typename T> template<typename U>
SharedPtr<T> & SharedPtr<T>::operator =(SharedPtr<U> && other)
{
	SharedPtr(std::move(other)).Swap(*this);
	return *this;
}

//===========================================================================//

template<typename T>
T & SharedPtr<T>::operator *() const
{
#ifdef DEBUGGING
	if (IsNull())
	{
		NULL_POINTER_EXCEPTION("Attempt to dereference null pointer");
	}
#endif	

	return *_objectPtr;
}

//===========================================================================//

template<typename T>
T * SharedPtr<T>::operator ->() const
{
#ifdef DEBUGGING
	if (IsNull())
	{
		NULL_POINTER_EXCEPTION("Attempt to dereference null pointer");
	}
#endif		

	return _objectPtr;
}

//===========================================================================//

template<typename T>
SharedPtr<T>::operator T * () const
{
	return _objectPtr;
}

//===========================================================================//

template<typename T>
bool SharedPtr<T>::operator ==(NullPtrType) const
{
	return IsNull();
}

//===========================================================================//

template<typename T> template<typename U>
bool SharedPtr<T>::operator ==(U * objectPtr) const
{
	return _objectPtr == objectPtr;
}

//===========================================================================//

template<typename T>
bool SharedPtr<T>::operator ==(const SharedPtr<T> & other) const
{
	return _objectPtr == other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool SharedPtr<T>::operator ==(const SharedPtr<U> & other) const
{
	return _objectPtr == other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool SharedPtr<T>::operator ==(const WeakPtr<U> & weakPtr) const
{
	return _objectPtr == weakPtr._objectPtr;
}

//===========================================================================//

template<typename T>
bool SharedPtr<T>::operator !=(NullPtrType) const
{
	return !IsNull();
}

//===========================================================================//

template<typename T> template<typename U>
bool SharedPtr<T>::operator !=(U * objectPtr) const
{
	return _objectPtr != objectPtr;
}

//===========================================================================//

template<typename T>
bool SharedPtr<T>::operator !=(const SharedPtr<T> & other) const
{
	return _objectPtr != other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool SharedPtr<T>::operator !=(const SharedPtr<U> & other) const
{
	return _objectPtr != other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool SharedPtr<T>::operator !=(const WeakPtr<U> & weakPtr) const
{
	return _objectPtr != weakPtr._objectPtr;
}

//===========================================================================//

template<typename T>
void SharedPtr<T>::Swap(SharedPtr<T> & other)
{
	std::swap(_objectPtr, other._objectPtr);
	std::swap(_refsCounter, other._refsCounter);
}

//===========================================================================//

template<typename T>
T * SharedPtr<T>::Get() const
{
	return _objectPtr;
}

//===========================================================================//

template<typename T>
void SharedPtr<T>::Redirect(T * objectPtr, RefsCounter * refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		if (_refsCounter->GetSharedRefsCount() == 1)
		{
			delete _objectPtr;
		}

		_refsCounter->DecrementSharedRefsCount();

		if (_refsCounter->IsNoRefs())
		{
			delete _refsCounter;
		}
	}

	_objectPtr = objectPtr;
	_refsCounter = refsCounter;
}

//===========================================================================//

template<typename T>
void SharedPtr<T>::Release()
{
	Redirect(NullPtr, NullPtr);
}

//===========================================================================//

template<class T>
bool SharedPtr<T>::IsNull() const
{
	return _refsCounter == NullPtr || _refsCounter->GetSharedRefsCount() == 0;
}

//===========================================================================//

template<typename T> template<typename U>
inline void SharedPtr<T>::EnableSharedFromThis(PtrFromThisMixIn<U> * ptrFromThis) const
{
	ptrFromThis->SetWeakPtrOnThis(this);
}

//===========================================================================//

template<class T>
inline void SharedPtr<T>::EnableSharedFromThis(...) const
{
}

//===========================================================================//

}// namespace

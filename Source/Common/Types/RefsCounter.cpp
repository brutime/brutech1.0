//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RefsCounter.h"

namespace Bru
{

//===========================================================================//

#ifdef DEBUGGING_POINTERS
integer RefsCounter::createdCount = 0;
integer RefsCounter::deletedCount = 0;
#endif // DEBUGGING_POINTERS	

//===========================================================================//

RefsCounter::RefsCounter(integer sharedRefsCount, integer weakRefsCount) :
	_sharedRefsCount(sharedRefsCount),
	_weakRefsCount(weakRefsCount)
{
#ifdef DEBUGGING_POINTERS
	createdCount++;
#endif // DEBUGGING_POINTERS	
}

//===========================================================================//

RefsCounter::RefsCounter(const RefsCounter & other) :
	_sharedRefsCount(other._sharedRefsCount),
	_weakRefsCount(other._weakRefsCount)
{
#ifdef DEBUGGING_POINTERS
	createdCount++;
#endif // DEBUGGING_POINTERS	
}

//===========================================================================//

RefsCounter::~RefsCounter()
{
#ifdef DEBUGGING_POINTERS
	deletedCount++;
#endif // DEBUGGING_POINTERS	
}

//===========================================================================//

#ifdef DEBUGGING_POINTERS
void RefsCounter::Reset()
{
	createdCount = 0;
	deletedCount = 0;
}

//===========================================================================//

integer RefsCounter::GetCreatedCount()
{
	return createdCount;
}

//===========================================================================//

integer RefsCounter::GetDeletedCount()
{
	return deletedCount;
}
#endif // DEBUGGING_POINTERS

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
Nullable<T>::Nullable() :
	_value(),
	_isNull(true)
{
}

//===========================================================================//

template<typename T>
Nullable<T>::Nullable(NullPtrType) :
	_value(),
	_isNull(true)
{
}

//===========================================================================//

template<typename T>
Nullable<T>::Nullable(const T & value) :
	_value(value),
	_isNull(false)
{
}

//===========================================================================//

template<typename T>
Nullable<T>::Nullable(const Nullable<T> & other) :
	_value(other._value),
	_isNull(other._isNull)
{
}

//===========================================================================//

template<typename T>
Nullable<T>::~Nullable()
{
}

//===========================================================================//

template<typename T>
Nullable<T> & Nullable<T>::operator =(NullPtrType)
{
	_value = T();
	_isNull = true;

	return *this;
}

//===========================================================================//

template<typename T>
Nullable<T> & Nullable<T>::operator =(const T & value)
{
	_value = value;
	_isNull = false;

	return *this;
}

//===========================================================================//

template<typename T>
Nullable<T> & Nullable<T>::operator =(const Nullable<T> & other)
{
	if (this == &other)
	{
		return *this;
	}

	_value = other._value;
	_isNull = other._isNull;

	return *this;
}

//===========================================================================//

template<typename T>
bool Nullable<T>::operator ==(NullPtrType) const
{
	return _isNull;
}

//===========================================================================//

template<typename T>
bool Nullable<T>::operator ==(const T & value) const
{
	if (_isNull)
	{
		return false;
	}

	return _value == value;
}

//===========================================================================//

template<typename T>
bool Nullable<T>::operator ==(const Nullable<T> & other) const
{
	return _isNull == other._isNull && _value == other._value;
}

//===========================================================================//

template<typename T>
bool Nullable<T>::operator !=(NullPtrType) const
{
	return !_isNull;
}

//===========================================================================//

template<typename T>
bool Nullable<T>::operator !=(const T & value) const
{
	if (_isNull)
	{
		return true;
	}

	return _value != value;
}

//===========================================================================//

template<typename T>
bool Nullable<T>::operator !=(const Nullable<T> & other) const
{
	return _isNull != other._isNull || _value != other._value;
}

//===========================================================================//

template<typename T>
bool Nullable<T>::HasValue() const
{
	return !_isNull;
}

//===========================================================================//

template<typename T>
const T & Nullable<T>::GetValue() const
{
	if (_isNull)
	{
		NULL_POINTER_EXCEPTION("Try to get value from null nullable.");
	}

	return _value;
}

//===========================================================================//

template<typename T>
const T & Nullable<T>::GetValueOrDefault(const T & defaultValue) const
{
	return _isNull ? defaultValue : _value;
}

//===========================================================================//

template<typename T>
std::ostream & operator <<(std::ostream & stream, const Nullable<T> & nullable)
{
	if (nullable.HasValue())
	{
		return stream << nullable.GetValue();
	}
	else
	{
		return stream;
	}
}

//===========================================================================//

template<typename T>
inline bool operator ==(NullPtrType nullValue, const Nullable<T> & nullable)
{
	return nullable == nullValue;
}

//===========================================================================//

template<typename T>
inline bool operator !=(NullPtrType nullValue, const Nullable<T> & nullable)
{
	return nullable != nullValue;
}

//===========================================================================//

} // namespace Bru

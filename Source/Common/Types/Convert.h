//
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech value code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Шаблон, позволяющий статически преобразовывать типы
//

#pragma once
#ifndef CONVERT_H
#define CONVERT_H

#include "Converter.h"

namespace Bru
{

//===========================================================================//

template<typename From, typename To>
class Convert;

template<>
class Convert<string, bool>;

template<>
class Convert<string, char>;

template<>
class Convert<string, byte>;

template<>
class Convert<string, ushort>;

template<>
class Convert<string, uint>;

template<>
class Convert<string, ulong>;

template<>
class Convert<string, integer>;

template<>
class Convert<string, int64>;

template<>
class Convert<string, float>;

template<typename T>
class Convert<string, T>;

template<typename T>
class Convert<string, Nullable<T>>;

template<>
class Convert<string, string>;

template<>
class Convert<void, string>;

template<>
class Convert<NullPtrType, string>;

//===========================================================================//

} // namespace Bru

#include "Convert.inl"

#endif // CONVERT_H

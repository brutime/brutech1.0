//
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech value code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс конвертертирующий биты в числа
//

#pragma once
#ifndef BIT_CONVERTER_H
#define BIT_CONVERTER_H

#include "BaseTypes.h"
#include "String/String.h"
#include "../Containers/Array/Array.h"

namespace Bru
{

//===========================================================================//

class BitConverter
{
public:
	static integer ToInteger(const Array<byte>::SimpleType & byteArray);
	static Array<byte>::SimpleType ToBytes(integer value);
	static Array<byte>::SimpleType ToBytes(integer value, integer bytesCount);

	static float ToFloat(const Array<byte>::SimpleType & byteArray);
	static Array<byte>::SimpleType ToBytes(float value);
	static Array<byte>::SimpleType ToBytes(float value, integer bytesCount);

private:
	BitConverter() = delete;
	BitConverter(const BitConverter &);
	~BitConverter() = delete;
	BitConverter & operator =(const BitConverter &);
};

//===========================================================================//

}
#endif // BIT_CONVERTER_H

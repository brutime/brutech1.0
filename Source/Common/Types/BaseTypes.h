//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Базовые типы
//

#pragma once
#ifndef BASE_TYPES_H
#define BASE_TYPES_H

#include "../Defines/Defines.h"
#include "../Defines/CommonMacroses.h"

#include <cstddef>
#include <utility>
#include <algorithm>
#include <limits>
#include <type_traits>
#include <initializer_list>

namespace Bru
{

//===========================================================================//

//Размер данного типа зависит от разярдности платформы
typedef size_t size_type;

//Диапазон [0 .. 255]
typedef unsigned char byte;

//Диапазон [0 .. 65535]
typedef unsigned short ushort;

//Диапазон [0 .. 65535]
typedef unsigned int uint;

//Диапазон [0 .. 4 294 967 295]
typedef unsigned long ulong;

//Диапазон [-2 147 483 648 .. 2 147 483 647] для x86 (x32) архитектуры
typedef ptrdiff_t integer;

//Диапазон [-9 223 372 036 854 775 808 .. 9 223 372 036 854 775 807]
typedef long long int64;

typedef wchar_t wide_char;

//===========================================================================//

static const float MinFloat = std::numeric_limits<float>::min();
static const float MaxFloat = std::numeric_limits<float>::max();

static const float FloatInfinity = std::numeric_limits<float>::infinity();

static const integer MinInteger = std::numeric_limits<integer>::min();
static const integer MaxInteger = std::numeric_limits<integer>::max();

//===========================================================================//

} // namespace Bru

#endif // BASE_TYPES_H

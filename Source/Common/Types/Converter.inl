//
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech value code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс конвертер типов
//

namespace Bru
{

//===========================================================================//

template<typename T, typename IsEnum>
string Converter::ToString(const T & value)
{
   return ToStringUsingOStringStream(value);
}

//===========================================================================//

template<typename T, typename IsNotEnum>
string Converter::ToString(T value)
{
	return value.ToString();
}

//===========================================================================//

template<typename T>
string Converter::ToStringUsingOStringStream(const T & value)
{
	std::ostringstream stream;
	stream << value;
	return string(stream.str().c_str());
}

//===========================================================================//

template<typename T>
T Converter::ToType(const string & value, NumberBase base)
{
	std::stringstream stream;
	switch (base)
	{
	case NumberBase::Octal:
		stream << std::oct << value;
		break;

	case NumberBase::Hexadecimal:
		stream << std::hex << value;
		break;

	case NumberBase::Decimal:
	default:
		stream << value;
		break;
	}
	T result;
	stream >> result;
	return result;
}

//===========================================================================//

template<typename T>
T Converter::ToUnsignedType(const string & value)
{
	std::stringstream stream;
	stream << value;
	integer result = 0;
	stream >> result;

#ifdef DEBUGGING
	if (result < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("value must be >= 0, but it is {0}", result));
	}
#endif // DEBUGGING
	return T(result);
}

//===========================================================================//

}


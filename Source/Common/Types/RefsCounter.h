//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Счетчик для указателей
//

#ifndef REFS_COUNTER_H
#define REFS_COUNTER_H

#include "BaseTypes.h"
#include "../Exceptions/InvalidStateException.h"

namespace Bru
{

//===========================================================================//

class RefsCounter
{
public:
	RefsCounter(integer sharedRefsCount = 0, integer weakRefsCount = 0);
	RefsCounter(const RefsCounter & other);
	~RefsCounter();

	inline void IncrementSharedRefsCount();
	inline void DecrementSharedRefsCount();

	inline void IncrementWeakRefsCount();
	inline void DecrementWeakRefsCount();

	inline bool IsNoRefs() const;

	inline integer GetSharedRefsCount() const;
	inline integer GetWeakRefsCount() const;

#ifdef DEBUGGING_POINTERS
	static void Reset();
	static integer GetCreatedCount();
	static integer GetDeletedCount();
#endif // DEBUGGING_POINTERS

private:
	integer _sharedRefsCount;
	integer _weakRefsCount;

#ifdef DEBUGGING_POINTERS
	static integer createdCount;
	static integer deletedCount;
#endif // DEBUGGING_POINTERS

	RefsCounter & operator =(const RefsCounter &);
};

//===========================================================================//

} // namespace Bru

#include "RefsCounter.Inl"

#endif // REFS_COUNTER_H

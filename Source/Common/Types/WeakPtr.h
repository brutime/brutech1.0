//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Слабый указатель. Не может создавать или удалять объект.
//              Нужен (в том числе), чтобы не было циклический ссылок
//

#pragma once
#ifndef WEAK_PTR_H
#define WEAK_PTR_H

#include "BaseTypes.h"
#include "RefsCounter.h"
#include "../Exceptions/NullPointerException.h"
#include "../Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

template<typename U> class SharedPtr;

//===========================================================================//

template<typename T>
class WeakPtr
{
	template<typename U> friend class SharedPtr;
	template<typename U> friend class WeakPtr;

public:
	WeakPtr();

	WeakPtr(NullPtrType);

	WeakPtr<T>(const WeakPtr<T> & other);

	template<typename U>
	WeakPtr<T>(const WeakPtr<U> & other);

	template<typename U>
	WeakPtr(T * objectPtr, const WeakPtr<U> & other);

	template<typename U>
	WeakPtr<T>(const SharedPtr<U> & sharedPtr);

	WeakPtr(WeakPtr<T> && other);

	template <typename U>
	WeakPtr(WeakPtr<U> && other);

	~WeakPtr();

	WeakPtr<T> & operator =(NullPtrType);

	WeakPtr<T> & operator =(const WeakPtr<T> & other);

	template <typename U>
	WeakPtr<T> & operator =(const WeakPtr<U> & other);

	template <typename U>
	WeakPtr<T> & operator =(const SharedPtr<U> & sharedPtr);

	WeakPtr<T> & operator =(WeakPtr<T> && other);

	template <typename U>
	WeakPtr<T> & operator =(WeakPtr<U> && other);

	T & operator *() const;
	T * operator ->() const;

	inline operator T * () const;

	inline bool operator ==(NullPtrType) const;

	template <typename U>
	inline bool operator ==(U * objectPtr) const;

	inline bool operator ==(const WeakPtr<T> & other) const;

	template <typename U>
	inline bool operator ==(const WeakPtr<U> & other) const;

	template <typename U>
	inline bool operator ==(const SharedPtr<U> & sharedPtr) const;

	inline bool operator !=(NullPtrType) const;

	template <typename U>
	inline bool operator !=(U * objectPtr) const;

	inline bool operator !=(const WeakPtr<T> & other) const;

	template <typename U>
	inline bool operator !=(const WeakPtr<U> & other) const;

	template <typename U>
	inline bool operator !=(const SharedPtr<U> & sharedPtr) const;

	inline SharedPtr<T> Lock() const;
	
	inline T * Get() const;

private:
	inline void Redirect(T * objectPtr, RefsCounter * refsCounter);
	inline void Release();
	inline bool IsNull() const;

	T * _objectPtr;
	RefsCounter * _refsCounter;

	//для тестирования
	friend class TestWeakPtrDefaultConstructor;
	friend class TestWeakPtrNullPtrConstructor;
	friend class TestWeakPtrObjectWeakPtrConstructor;
	friend class TestWeakPtrObjectWeakPtrInheritanceConstructor;
	friend class TestWeakPtrObjectWeakPtrCastConstructor;
	friend class TestWeakPtrObjectWeakNullPtrConstructor;
	friend class TestWeakPtrObjectWeakNullPtrInheritanceConstructor;
	friend class TestWeakPtrObjectWeakNullPtrCastConstructor;
	friend class TestWeakPtrObjectWeakPtrConstructorExceptions;
	friend class TestWeakPtrObjectWeakPtrInheritanceConstructorExceptions;	
	friend class TestWeakPtrCopyConstructor;
	friend class TestWeakPtrSharedNullPtrConstructor;
	friend class TestWeakPtrSharedNullPtrInheritanceConstructor;
	friend class TestWeakPtrInheritanceCopyConstructor;
	friend class TestWeakPtrWeakNullPtrCopyConstructor;
	friend class TestWeakPtrWeakNullPtrInheritanceCopyConstructor;
	friend class TestWeakPtrSharedPtrConstructor;
	friend class TestWeakPtrSharedPtrInheritanceConstructor;
	friend class TestWeakPtrMoveConstructor;
	friend class TestWeakPtrInheritanceMoveConstructor;
	friend class TestWeakPtrWeakNullPtrMoveConstructor;
	friend class TestWeakPtrWeakNullPtrInheritanceMoveConstructor;
	friend class TestWeakPtrNullPtrCopyOperator;
	friend class TestWeakPtrCopyOperator;
	friend class TestWeakPtrInheritanceCopyOperator;
	friend class TestWeakPtrWeakNullPtrCopyOperator;
	friend class TestWeakPtrWeakNullPtrTemporarySharedPtrCopyOperator;
	friend class TestWeakPtrWeakNullPtrInheritanceCopyOperator;
	friend class TestWeakPtrWeakNullPtrTemporarySharedPtrInheritanceCopyOperator;
	friend class TestWeakPtrSharedPtrCopyOperator;
	friend class TestWeakPtrSharedPtrInheritanceCopyOperator;
	friend class TestWeakPtrSharedNullPtrCopyOperator;
	friend class TestWeakPtrSharedNullPtrInheritanceCopyOperator;
	friend class TestWeakPtrCopyOperatorSelf;
	friend class TestWeakPtrMoveOperator;
	friend class TestWeakPtrInheritanceMoveOperator;
	friend class TestWeakPtrWeakNullPtrMoveOperator;
	friend class TestWeakPtrWeakNullPtrTemporarySharedPtrMoveOperator;
	friend class TestWeakPtrWeakNullPtrTemporarySharedPtrInheritanceMoveOperator;
	friend class TestWeakPtrWeakNullPtrInheritanceMoveOperator;

	friend class TestSharedPtrWeakPtrConstructor;
	friend class TestSharedPtrWeakPtrInheritanceConstructor;
	friend class TestSharedPtrWeakPtrCopyOperator;
	friend class TestSharedPtrWeakPtrInheritanceCopyOperator;

	friend class TestPtrFromThisMixInConstructor;
	friend class TestPtrFromThisMixInCopyConstructor;
	friend class TestPtrFromThisMixInInheritanceCopyConstructor;
	friend class TestPtrFromThisMixInCopyOperator;
	friend class TestPtrFromThisMixInInheritanceCopyOperator;
	friend class TestPtrFromThisMixInGetSharedPtr;
	friend class TestPtrFromThisMixInGetInheritanceSharedPtr;
	friend class TestPtrFromThisMixInGetCastedSharedPtr;
	friend class TestPtrFromThisMixInGetWeakPtr;
	friend class TestPtrFromThisMixInGetInheritanceWeakPtr;
	friend class TestPtrFromThisMixInGetCastedWeakPtr;
};

//===========================================================================//

template<typename T, typename U>
inline WeakPtr<T> static_pointer_cast(const WeakPtr<U> & ptr)
{
	return WeakPtr<T>(static_cast<T *>(ptr.Get()), ptr);
}

//===========================================================================//

} // namespace Bru

#include "WeakPtr.inl"

#endif // WEAK_PTR_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Владеющий умный указатель с подсчетом ссылок
//

#pragma once
#ifndef SHARED_PTR_H
#define SHARED_PTR_H

#include "BaseTypes.h"
#include "Memory.h"
#include "RefsCounter.h"
#include "../Exceptions/NullPointerException.h"
#include "../Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class PtrFromThisMixIn;

template<typename T>
class WeakPtr;

//===========================================================================//

template<typename T>
class SharedPtr
{
	template<typename U> friend class SharedPtr;
	template<typename U> friend class WeakPtr;

public:
	SharedPtr();

	SharedPtr(NullPtrType);

	template<typename U>
	SharedPtr(U * objectPtr);

	template<typename U>
	SharedPtr(T * objectPtr, const SharedPtr<U> & other);

	SharedPtr(const SharedPtr<T> & other);

	template<typename U>
	SharedPtr(const SharedPtr<U> & other);

	template<typename U>
	SharedPtr(const WeakPtr<U> & weakPtr);

	SharedPtr(SharedPtr<T> && other);

	template <typename U>
	SharedPtr(SharedPtr<U> && other);

	~SharedPtr();

	SharedPtr<T> & operator =(NullPtrType);

	template <typename U>
	SharedPtr<T> & operator =(U * objectPtr);

	SharedPtr<T> & operator =(const SharedPtr<T> & other);

	template <typename U>
	SharedPtr<T> & operator =(const SharedPtr<U> & other);

	template <typename U>
	SharedPtr<T> & operator =(const WeakPtr<U> & weakPtr);

	SharedPtr<T> & operator =(SharedPtr<T> && other);

	template <typename U>
	SharedPtr<T> & operator =(SharedPtr<U> && other);

	inline T & operator *() const;
	inline T * operator ->() const;

	inline operator T * () const;

	inline bool operator ==(NullPtrType) const;

	template <typename U>
	inline bool operator ==(U * objectPtr) const;

	inline bool operator ==(const SharedPtr<T> & other) const;

	template <typename U>
	inline bool operator ==(const SharedPtr<U> & other) const;

	template <typename U>
	inline bool operator ==(const WeakPtr<U> & weakPtr) const;

	inline bool operator !=(NullPtrType) const;

	template <typename U>
	inline bool operator !=(U * objectPtr) const;

	inline bool operator !=(const SharedPtr<T> & other) const;

	template <typename U>
	inline bool operator !=(const SharedPtr<U> & other) const;

	template <typename U>
	inline bool operator !=(const WeakPtr<U> & weakPtr) const;

	inline T * Get() const;

private:
	inline void Redirect(T * objectPtr, RefsCounter * refsCounter);
	inline void Release();
	inline void Swap(SharedPtr<T> & other);
	inline bool IsNull() const;

	template<typename U>
	inline void EnableSharedFromThis(PtrFromThisMixIn<U> * ptrFromThis) const;
	//Пустой метод, чтобы разрулить классы, унаследованные и не унаследованные от PtrFromThisMixIn
	inline void EnableSharedFromThis(...) const;

	T * _objectPtr;
	RefsCounter * _refsCounter;

	//для тестировани
	friend class TestSharedPtrDefaultConstructor;
	friend class TestSharedPtrNullPtrConstructor;
	friend class TestSharedPtrNullObjectConstructor;
	friend class TestSharedPtrObjectConstructor;
	
	friend class TestSharedPtrObjectSharedPtrConstructor;
	friend class TestSharedPtrObjectSharedPtrInheritanceConstructor;
	friend class TestSharedPtrObjectSharedPtrCastConstructor;
	friend class TestSharedPtrObjectSharedNullPtrConstructor;
	friend class TestSharedPtrObjectSharedNullPtrInheritanceConstructor;
	friend class TestSharedPtrObjectSharedNullPtrCastConstructor;
	friend class TestSharedPtrObjectSharedPtrConstructorExceptions;
	friend class TestSharedPtrObjectSharedPtrInheritanceConstructorExceptions;
	
	friend class TestSharedPtrCopyConstructor;
	friend class TestSharedPtrInheritanceCopyConstructor;
	friend class TestSharedPtrSharedNullPtrCopyConstructor;
	friend class TestSharedPtrSharedNullPtrInheritanceCopyConstructor;
	friend class TestSharedPtrWeakPtrConstructor;
	friend class TestSharedPtrWeakPtrInheritanceConstructor;
	friend class TestSharedPtrWeakNullPtrConstructor;
	friend class TestSharedPtrWeakNullPtrInheritanceConstructor;
	friend class TestSharedPtrMoveConstructor;
	friend class TestSharedPtrInheritanceMoveConstructor;
	friend class TestSharedPtrSharedNullPtrMoveConstructor;
	friend class TestSharedPtrSharedNullPtrInheritanceMoveConstructor;
	friend class TestSharedPtrNullPtrCopyOperator;
	friend class TestSharedPtrObjectCopyOperator;
	friend class TestSharedPtrSameObjectCopyOperator;
	friend class TestSharedPtrCopyOperator;
	friend class TestSharedPtrInheritanceCopyOperator;
	friend class TestSharedPtrSharedNullPtrCopyOperator;
	friend class TestSharedPtrSharedNullPtrInheritanceCopyOperator;
	friend class TestSharedPtrWeakPtrCopyOperator;
	friend class TestSharedPtrWeakPtrInheritanceCopyOperator;
	friend class TestSharedPtrWeakNullPtrCopyOperator;
	friend class TestSharedPtrWeakNullPtrInheritanceCopyOperator;
	friend class TestSharedPtrCopyOperatorSelf;
	friend class TestSharedPtrMoveOperator;
	friend class TestSharedPtrInheritanceMoveOperator;
	friend class TestSharedPtrSharedNullPtrMoveOperator;
	friend class TestSharedPtrSharedNullPtrInheritanceMoveOperator;
	friend class TestSharedPtrFactory;
	friend class TestWeakPtrMoveConstructor;
	friend class TestWeakPtrInheritanceMoveConstructor;
	friend class TestWeakPtrSharedPtrConstructor;
	friend class TestWeakPtrSharedPtrInheritanceConstructor;
	friend class TestWeakPtrNullPtrCopyOperator;
	friend class TestWeakPtrCopyOperator;
	friend class TestWeakPtrInheritanceCopyOperator;
	friend class TestWeakPtrSharedPtrCopyOperator;
	friend class TestWeakPtrSharedPtrInheritanceCopyOperator;
	friend class TestWeakPtrCopyOperatorSelf;
	friend class TestWeakPtrMoveOperator;
	friend class TestWeakPtrInheritanceMoveOperator;
	friend class TestWeakPtrLock;

	friend class TestPtrFromThisMixInConstructor;
	friend class TestPtrFromThisMixInCopyConstructor;
	friend class TestPtrFromThisMixInInheritanceCopyConstructor;
	friend class TestPtrFromThisMixInCopyOperator;
	friend class TestPtrFromThisMixInInheritanceCopyOperator;
	friend class TestPtrFromThisMixInGetSharedPtr;
	friend class TestPtrFromThisMixInGetInheritanceSharedPtr;
	friend class TestPtrFromThisMixInGetCastedSharedPtr;
	friend class TestPtrFromThisMixInGetWeakPtr;
	friend class TestPtrFromThisMixInGetInheritanceWeakPtr;
	friend class TestPtrFromThisMixInGetCastedWeakPtr;
};

//===========================================================================//

template<typename T, typename U>
inline SharedPtr<T> static_pointer_cast(const SharedPtr<U> & ptr)
{
	return SharedPtr<T>(static_cast<T *>(ptr.Get()), ptr);
}

//===========================================================================//

}//namespace

#include "SharedPtr.inl"

#endif // SHARED_PTR_H

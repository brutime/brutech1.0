//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Построитель строк :)
//

#ifndef STRING_BUILDER_H
#define STRING_BUILDER_H

#include "String.h"

namespace Bru
{

//===========================================================================//

class StringBuilder
{
	friend class string;

public:
	StringBuilder();
	StringBuilder(const string & sourceString);
	StringBuilder(const string & sourceString, integer moreReservedLengthInChars);
	StringBuilder(integer reservedLengthInChars);
	~StringBuilder();

	StringBuilder & Append(const string & someString);
	StringBuilder & Append(const utf8_char & someChar);
	StringBuilder & Insert(integer index, const string & someString);
	StringBuilder & Remove(integer index, integer count = 1);
	StringBuilder & Replace(const string & oldString, const string & newString);

	void Clear();

	const char * ToChars() const
	{
		return _dataPtr;
	}
	string ToString() const;

	integer GetLength() const
	{
		return _length;
	}
	integer GetLengthInBytes() const
	{
		return _lengthInBytes;
	}

private:
	static const string Delimeter;

	void AppendFormat(const string & formatString, const string * arguments[], integer argumentsCount);

	//Алгоритм Кнута — Морриса — Пратта (КМП-алгоритм)
	static integer GetSubstringIndexInBytes(const char * chars, integer charsLengthInBytes,
	                                        const string & searchingString, integer indexInBytes);

	StringBuilder & Append(const char * chars, integer length, integer lengthInBytes);
	static string RoundFloat(const string & floatString, integer precision);
	static char GetNextChar(const string & formatString, integer & index, integer count);

	void Grow(integer stringLengthInBytes);
	void Decrease();

	integer GetDeltaDecreaseLength() const;
	void ResizeData(integer reservedLengthInBytes);

	integer _length;
	integer _lengthInBytes;
	integer _reservedLengthInBytes;
	char * _dataPtr;

	StringBuilder(const StringBuilder &);
	StringBuilder & operator =(const StringBuilder &);
};

//===========================================================================//

}//namespace

#endif // STRING_BUILDER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTF16String.h"

#include "UTF16Helper.h"
#include "UTFLength.h"
#include "../../Helpers/Memory.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/OverflowException.h"

namespace Bru
{

//===========================================================================//

const utf16_string utf16_string::Empty = NullTerminator;

//===========================================================================//

utf16_string::utf16_string(const ushort * chars) :
	_length(0),
	_lengthInWords(0),
	_dataPtr(NullPtr)
{
#ifdef DEBUGGING_STRINGS
	UTF16Helper::Check(chars);
#endif // DEBUGGING_STRINGS
	UTFLength charsLength = UTF16Helper::GetLength(chars);

#ifdef DEBUGGING_STRINGS
	if (charsLength.LengthInChars > string::MaxLength)
	{
		OVERFLOW_EXCEPTION(string::Format("Target length {0} is more than max string size", charsLength.LengthInChars));
	}

	if (charsLength.LengthInComponents > string::MaxLength * 2)
	{
		OVERFLOW_EXCEPTION(
		    string::Format("Tager length in words {0} is more than max string size", charsLength.LengthInComponents));
	}
#endif // DEBUGGING_STRINGS
	_lengthInWords = charsLength.LengthInComponents;
	_dataPtr = new ushort[_lengthInWords + 1];
	if (charsLength.LengthInComponents > 0)
	{
		Memory::Copy(_dataPtr, chars, charsLength.LengthInComponents * sizeof(_dataPtr[0]));
	}
	_dataPtr[_lengthInWords] = NullTerminator;
	_length = charsLength.LengthInChars;
}

//===========================================================================//

utf16_string::utf16_string(const utf16_string & otherString) :
	_length(0),
	_lengthInWords(0),
	_dataPtr(NullPtr)
{
	DeepCopy(otherString);
}

//===========================================================================//

utf16_string & utf16_string::operator =(const utf16_string & otherString)
{
	if (this != &otherString)
	{
		DeepCopy(otherString);
	}

	return *this;
}

//===========================================================================//

utf16_string::~utf16_string()
{
	delete[] _dataPtr;
}

//===========================================================================//

void utf16_string::DeepCopy(const utf16_string & otherString)
{
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_lengthInWords = otherString._lengthInWords;
	_dataPtr = new ushort[_lengthInWords + 1];
	//+ 1 чтобы скопировать NullTerminator
	Memory::Copy(_dataPtr, otherString._dataPtr, (_lengthInWords + 1) * sizeof(_dataPtr[0]));
	_length = otherString._length;
}

//===========================================================================//

}//namespace

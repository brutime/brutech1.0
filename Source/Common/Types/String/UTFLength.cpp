//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTFLength.h"

namespace Bru
{

//===========================================================================//

UTFLength::UTFLength() :
	LengthInChars(0),
	LengthInComponents(0)
{
}

//===========================================================================//

UTFLength::UTFLength(integer lengthInChars, integer lengthInComponents) :
	LengthInChars(lengthInChars),
	LengthInComponents(lengthInComponents)
{
}

//===========================================================================//

UTFLength::UTFLength(const UTFLength & other) :
	LengthInChars(other.LengthInChars),
	LengthInComponents(other.LengthInComponents)
{
}

//===========================================================================//

UTFLength & UTFLength::operator =(const UTFLength & other)
{
	if (this == &other)
	{
		return *this;
	}

	LengthInChars = other.LengthInChars;
	LengthInComponents = other.LengthInComponents;

	return *this;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс UTF8 символа
//

#pragma once
#ifndef UTF8_CHAR_H
#define UTF8_CHAR_H

#include "../BaseTypes.h"
#include "UTFLength.h"
#include "UTF32Char.h"

namespace Bru
{

//===========================================================================//

class utf8_char
{
	friend class string;

public:
	utf8_char();
	utf8_char(const char * someChar);
	utf8_char(const utf8_char & other);
	utf8_char(utf8_char && other);
	~utf8_char();

	utf8_char & operator =(const char * someChar);
	utf8_char & operator =(const utf8_char & other);
	utf8_char & operator =(utf8_char && other);

	bool operator ==(const char * someChar) const;
	bool operator ==(const utf8_char & someChar) const;

	bool operator !=(const char * someChar) const;
	bool operator !=(const utf8_char & someChar) const;

	bool operator <(const char * someChar) const;
	bool operator <(const utf8_char & someChar) const;

	bool operator >(const char * someChar) const;
	bool operator >(const utf8_char & someChar) const;

	integer GetLength() const
	{
		return _lengthInBytes > 0 ? 1 : 0;
	}
	integer GetLengthInBytes() const
	{
		return _lengthInBytes;
	}

	operator const char * () const
	{
		return ToChars();
	}
	const char * ToChars() const
	{
		return _dataPtr;
	}

	utf32_char ToUTF32() const;

	bool IsDigit() const;

private:
	utf8_char(const char * someChar, integer lengthInBytes);

	void Assign(const char * someChar);
	void DeepCopy(const utf8_char & otherUTF8Char);

	integer _lengthInBytes;
	char * _dataPtr;

	//для тестирования
	friend class TestUTF8CharDefaultConstructor;
	friend class TestUTF8CharConstructorFromOneByteChar;
	friend class TestUTF8CharConstructorFromTwoBytesChar;
	friend class TestUTF8CharConstructorFromThreeBytesChar;
	friend class TestUTF8CharConstructorFromFourBytesChar;
	friend class TestUTF8CharConstructorFromOtherUTF8Char;
	friend class TestUTF8CharInternalConstructor;
	friend class TestUTF8CharOperatorAssignOneByteChar;
	friend class TestUTF8CharOperatorAssignTwoBytesChar;
	friend class TestUTF8CharOperatorAssignThreeBytesChar;
	friend class TestUTF8CharOperatorAssignFourBytesChar;
	friend class TestUTF8CharOperatorAssignFromOtherUTF8Char;

};

//===========================================================================//

}//namespace

#endif // UTF8_CHAR_H

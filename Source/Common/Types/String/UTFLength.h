//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс хранящий длину строки в символах и байтах
//

#pragma once
#ifndef UTF_LENGTH_H
#define UTF_LENGTH_H

#include "../BaseTypes.h"

namespace Bru
{

//===========================================================================//

struct UTFLength
{
	UTFLength();
	UTFLength(integer lengthInChars, integer lengthInComponents);
	UTFLength(const UTFLength & other);
	UTFLength & operator =(const UTFLength & other);

	integer LengthInChars;
	integer LengthInComponents;
};

//===========================================================================//

}//namespace

#endif // UTF_LENGTH_H

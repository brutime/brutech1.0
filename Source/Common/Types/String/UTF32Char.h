//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс UTF32 символа
//

#pragma once
#ifndef UTF32_CHAR_H
#define UTF32_CHAR_H

#include "../BaseTypes.h"
#include "UTFLength.h"

namespace Bru
{

//===========================================================================//

class utf32_char
{
public:
	utf32_char();
	utf32_char(ulong someChar);
	utf32_char(const utf32_char & other);
	~utf32_char();

	utf32_char & operator =(ulong someChar);
	utf32_char & operator =(const utf32_char & other);

	bool operator ==(ulong someChar) const;
	bool operator ==(const utf32_char & someChar) const;

	bool operator !=(ulong someChar) const;
	bool operator !=(const utf32_char & someChar) const;

	bool operator <(ulong someChar) const;
	bool operator <(const utf32_char & someChar) const;

	bool operator >(ulong someChar) const;
	bool operator >(const utf32_char & someChar) const;

	integer GetLength() const
	{
		return _char > 0 ? 1 : 0;
	}
	integer GetLengthInBytes() const
	{
		return sizeof(_char);
	}

	operator uint() const
	{
		return _char;
	}

private:
	uint _char;

	//для тестирования
	friend class TestUTF32CharDefaultConstructor;
	friend class TestUTF32CharConstructorFromChar;
	friend class TestUTF32CharConstructorFromOtherUTF32Char;
	friend class TestUTF32CharOperatorAssignChar;
	friend class TestUTF32CharOperatorOtherUTF32Char;
};

//===========================================================================//

}//namespace

#endif // UTF32_CHAR_H

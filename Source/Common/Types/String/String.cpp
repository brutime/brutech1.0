//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "String.h"

#include <iostream>

#include "../../Helpers/Memory.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidStateException.h"
#include "../../Exceptions/IndexOutOfBoundsException.h"
#include "../../Exceptions/OverflowException.h"
#include "../Converter.h"
#include "UTF8Helper.h"
#include "StringConverter.h"
#include "StringBuilder.h"
#include "CaseConvertationTable.h"

namespace Bru
{

//===========================================================================//

const string string::Empty = "";
const integer string::MaxLength = 100000;

const string string::BlankChars = " \t\n\r";
const utf8_char string::Space = " ";
const integer string::FormatArgumentLength = 8;

//===========================================================================//

string::string() :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(new char[_lengthInBytes + 1]),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	_dataPtr[_lengthInBytes] = NullTerminator;
}

//===========================================================================//

string::string(bool someBool) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someBool));
}

//===========================================================================//

string::string(char someChar) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someChar));
}

//===========================================================================//

string::string(byte someByte) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someByte));
}

//===========================================================================//

string::string(ushort someUnsignedShort) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someUnsignedShort));
}

//===========================================================================//

string::string(uint someUnsegnedInteger) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someUnsegnedInteger));
}

//===========================================================================//

string::string(ulong someUnsegnedLong) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someUnsegnedLong));
}

//===========================================================================//

string::string(integer someInteger) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someInteger));
}

//===========================================================================//

string::string(int64 someInteger64) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someInteger64));
}

//===========================================================================//

string::string(float someFloat) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someFloat));
}

//===========================================================================//

string::string(double someDouble) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(Converter::ToString(someDouble));
}

//===========================================================================//

string::string(const utf8_char & someChar) :
	_length(someChar.GetLength()),
	_lengthInBytes(someChar.GetLengthInBytes()),
	_dataPtr(new char[_lengthInBytes + 1]),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	Memory::Copy(_dataPtr, someChar._dataPtr, _lengthInBytes + 1);
}

//===========================================================================//

string::string(const utf32_char & someChar) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	const ulong chars[2] = { someChar, NullTerminator };
	DeepCopy(StringConverter::UTF32ToUTF8(chars));
}

//===========================================================================//

string::string(const char * chars) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(chars);
#endif // DEBUGGING_STRINGS
	Assign(chars);
}

//===========================================================================//

string::string(const char * chars, integer lengthInBytes) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
#ifdef DEBUGGING
	if (lengthInBytes < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("lengthInBytes must be > 0, but it is {0}", lengthInBytes));
	}
#endif

#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(chars, lengthInBytes);
#endif // DEBUGGING_STRINGS
	integer lengthInChars = UTF8Helper::GetLengthInChars(chars, lengthInBytes);
	CheckForOverflow(lengthInChars, lengthInBytes);

	_length = lengthInChars;
	_lengthInBytes = lengthInBytes;
	_dataPtr = new char[_lengthInBytes + 1];
	Memory::Copy(_dataPtr, chars, _lengthInBytes);
	_dataPtr[_lengthInBytes] = NullTerminator;
}

//===========================================================================//

//+1 чтобы скопировать NullTerminator
string::string(const char * chars, integer length, integer lengthInBytes) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
#ifdef DEBUGGING
	if (length < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("length must be > 0, but it is {0}", length));
	}

	if (lengthInBytes < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("lengthInBytes must be > 0, but it is {0}", lengthInBytes));
	}
#endif

#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(chars, lengthInBytes);
#endif // DEBUGGING_STRINGS
	CheckForOverflow(length, lengthInBytes);

	_length = length;
	_lengthInBytes = lengthInBytes;
	_dataPtr = new char[_lengthInBytes + 1];
	Memory::Copy(_dataPtr, chars, _lengthInBytes);
	_dataPtr[_lengthInBytes] = NullTerminator;
}

//===========================================================================//

string::string(const utf16_string & someString) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(StringConverter::UTF16ToUTF8(someString));
}

//===========================================================================//

string::string(const utf32_string & someString) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(StringConverter::UTF32ToUTF8(someString));
}

//===========================================================================//

string::string(const string & other) :
	_length(0),
	_lengthInBytes(0),
	_dataPtr(NullPtr),
	_cashedIndex(0),
	_cashedByteIndex(0)
{
	DeepCopy(other);
}

//===========================================================================//

string::string(string && other) :
	_length(other._length),
	_lengthInBytes(other._lengthInBytes),
	_dataPtr(other._dataPtr),
	_cashedIndex(other._cashedIndex),
	_cashedByteIndex(other._cashedByteIndex)
{
	other._length = 0;
	other._lengthInBytes = 0;
	other._dataPtr = NullPtr;
	other._cashedIndex = 0;
	other._cashedByteIndex = 0;
}

//===========================================================================//

string::~string()
{
	delete[] _dataPtr;
}

//===========================================================================//

string & string::operator =(const utf8_char & someChar)
{
	delete[] _dataPtr;

	_length = someChar.GetLength();
	_lengthInBytes = someChar.GetLengthInBytes();
	_dataPtr = new char[_lengthInBytes + 1];
	Memory::Copy(_dataPtr, someChar.ToChars(), _lengthInBytes + 1); //+ 1 чтобы скопировать NullTerminator

	ResetCashedIndices();

	return *this;
}

//===========================================================================//

string & string::operator =(const char * chars)
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(chars);
#endif // DEBUGGING_STRINGS
	Assign(chars);
	ResetCashedIndices();

	return *this;
}

//===========================================================================//

string & string::operator =(const string & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

string & string::operator =(string && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(_length, other._length);
	std::swap(_lengthInBytes, other._lengthInBytes);
	std::swap(_dataPtr, other._dataPtr);
	std::swap(_cashedIndex, other._cashedIndex);
	std::swap(_cashedByteIndex, other._cashedByteIndex);

	return *this;
}

//===========================================================================//

string & string::operator +=(const utf8_char & someChar)
{
	CheckForOverflow(_length + someChar.GetLength(), _lengthInBytes + someChar.GetLengthInBytes());

	integer sourceLengthInBytes = _lengthInBytes;

	ResizeData(_lengthInBytes + someChar.GetLengthInBytes());

	if (someChar.GetLengthInBytes() > 0)
	{
		Memory::Copy(_dataPtr + sourceLengthInBytes, someChar.ToChars(), someChar.GetLengthInBytes());
	}
	_dataPtr[_lengthInBytes] = NullTerminator;
	_length += someChar.GetLength();

	return *this;
}

//===========================================================================//

string & string::operator +=(const char * chars)
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(chars);
#endif // DEBUGGING_STRINGS
	UTFLength charsLength = UTF8Helper::GetLength(chars);

	CheckForOverflow(_length + charsLength.LengthInChars, _lengthInBytes + charsLength.LengthInComponents);

	integer sourceLengthInBytes = _lengthInBytes;

	ResizeData(_lengthInBytes + charsLength.LengthInComponents);

	if (charsLength.LengthInComponents > 0)
	{
		Memory::Copy(_dataPtr + sourceLengthInBytes, chars, charsLength.LengthInComponents);
	}
	_dataPtr[_lengthInBytes] = NullTerminator;
	_length += charsLength.LengthInChars;

	return *this;
}

//===========================================================================//

string & string::operator +=(const string & someString)
{
	CheckForOverflow(_length + someString._length, _lengthInBytes + someString._lengthInBytes);

	integer sourceLengthInBytes = _lengthInBytes;

	ResizeData(_lengthInBytes + someString._lengthInBytes);

	if (someString._lengthInBytes > 0)
	{
		Memory::Copy(_dataPtr + sourceLengthInBytes, someString._dataPtr, someString._lengthInBytes);
	}
	_dataPtr[_lengthInBytes] = NullTerminator;
	_length += someString._length;

	return *this;
}

//===========================================================================//

string string::operator +(const utf8_char & someChar) const
{
	string resultString = *this;
	resultString += someChar;
	return resultString;
}

//===========================================================================//

string string::operator +(const char * chars) const
{
	string resultString = *this;
	resultString += chars;
	return resultString;
}

//===========================================================================//

string string::operator +(const string & someString) const
{
	string resultString = *this;
	resultString += someString;
	return resultString;
}

//===========================================================================//

bool string::operator ==(const utf8_char & someChar) const
{
	if (_lengthInBytes != someChar.GetLengthInBytes())
	{
		return false;
	}

	return Memory::Compare(_dataPtr, someChar.ToChars(), _lengthInBytes) == ComapareResult::Equals;
}

//===========================================================================//

bool string::operator ==(const char * chars) const
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(chars);
#endif // DEBUGGING_STRINGS
	UTFLength charsLength = UTF8Helper::GetLength(chars);
	if (_lengthInBytes != charsLength.LengthInComponents)
	{
		return false;
	}

	return Memory::Compare(_dataPtr, chars, _lengthInBytes) == ComapareResult::Equals;
}

//===========================================================================//

bool string::operator ==(const string & someString) const
{
	if (_lengthInBytes != someString._lengthInBytes)
	{
		return false;
	}

	return Memory::Compare(_dataPtr, someString._dataPtr, _lengthInBytes) == ComapareResult::Equals;
}

//===========================================================================//

bool string::operator !=(const utf8_char & someChar) const
{
	return !(*this == someChar);
}

//===========================================================================//

bool string::operator !=(const char * chars) const
{
	return !(*this == chars);
}

//===========================================================================//

bool string::operator !=(const string & someString) const
{
	return !(*this == someString);
}

//===========================================================================//

bool string::operator <(const utf8_char & someChar) const
{
	if (_lengthInBytes > someChar.GetLengthInBytes())
	{
		return Memory::Compare(_dataPtr, someChar.ToChars(), _lengthInBytes) == ComapareResult::Less;
	}
	else
	{
		return Memory::Compare(_dataPtr, someChar.ToChars(), someChar.GetLengthInBytes()) == ComapareResult::Less;
	}
}

//===========================================================================//

bool string::operator <(const char * chars) const
{
	UTFLength charsLength = UTF8Helper::GetLength(chars);

	if (_lengthInBytes > charsLength.LengthInComponents)
	{
		return Memory::Compare(_dataPtr, chars, _lengthInBytes) == ComapareResult::Less;
	}
	else
	{
		return Memory::Compare(_dataPtr, chars, charsLength.LengthInComponents) == ComapareResult::Less;
	}
}

//===========================================================================//

bool string::operator <(const string & other) const
{
	if (_lengthInBytes > other._lengthInBytes)
	{
		return Memory::Compare(_dataPtr, other._dataPtr, _lengthInBytes) == ComapareResult::Less;
	}
	else
	{
		return Memory::Compare(_dataPtr, other._dataPtr, other._lengthInBytes) == ComapareResult::Less;
	}
}

//===========================================================================//

bool string::operator >(const utf8_char & someChar) const
{
	if (_lengthInBytes > someChar.GetLengthInBytes())
	{
		return Memory::Compare(_dataPtr, someChar.ToChars(), _lengthInBytes) == ComapareResult::Greater;
	}
	else
	{
		return Memory::Compare(_dataPtr, someChar.ToChars(), someChar.GetLengthInBytes()) == ComapareResult::Greater;
	}
}

//===========================================================================//

bool string::operator >(const char * chars) const
{
	UTFLength charsLength = UTF8Helper::GetLength(chars);

	if (_lengthInBytes > charsLength.LengthInComponents)
	{
		return Memory::Compare(_dataPtr, chars, _lengthInBytes) == ComapareResult::Greater;
	}
	else
	{
		return Memory::Compare(_dataPtr, chars, charsLength.LengthInComponents) == ComapareResult::Greater;
	}
}

//===========================================================================//

bool string::operator >(const string & other) const
{
	if (_lengthInBytes > other._lengthInBytes)
	{
		return Memory::Compare(_dataPtr, other._dataPtr, _lengthInBytes) == ComapareResult::Greater;
	}
	else
	{
		return Memory::Compare(_dataPtr, other._dataPtr, other._lengthInBytes) == ComapareResult::Greater;
	}
}

//===========================================================================//

utf8_char string::operator [](integer index) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _length - 1));
	}
#endif // DEBUGGING
	integer currentByteIndex;
	if (_cashedIndex < index)
	{
		currentByteIndex = UTF8Helper::GetByteIndex(_dataPtr, _lengthInBytes, index, _cashedIndex, _cashedByteIndex);
	}
	else
	{
		currentByteIndex = UTF8Helper::GetPreviousByteIndex(_dataPtr, index, _cashedIndex, _cashedByteIndex);
	}

	_cashedIndex = index;
	_cashedByteIndex = currentByteIndex;

	integer charLengthInBytes = UTF8Helper::GetCharLengthInBytes(_dataPtr[currentByteIndex]);
	return utf8_char(_dataPtr + currentByteIndex, charLengthInBytes);
}

//===========================================================================//

utf8_char string::GetChar(integer index) const
{
	return this->operator[](index);
}

//===========================================================================//

string::operator const char * () const
{
	return ToChars();
}

//===========================================================================//

const char * string::ToChars() const
{
	return _dataPtr;
}

//===========================================================================//

utf16_string string::ToUTF16() const
{
	return StringConverter::UTF8ToUTF16(_dataPtr);
}

//===========================================================================//

utf32_string string::ToUTF32() const
{
	return StringConverter::UTF8ToUTF32(_dataPtr);
}

//===========================================================================//

bool string::IsEmpty() const
{
	return _length == 0;
}

//===========================================================================//

bool string::IsEmptyOrBlankCharsOnly() const
{
	if (IsEmpty())
	{
		return true;
	}

	for (integer i = 0; i < _length; i++)
	{
		if (!BlankChars.Contains(GetChar(i)))
		{
			return false;
		}
	}

	return true;
}

//===========================================================================//

bool string::IsBlankCharAt(integer index) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _length - 1));
	}
#endif // DEBUGGING
	return BlankChars.Contains(GetChar(index));
}

//===========================================================================//

bool string::IsNumber() const
{
	if (IsEmpty())
	{
		return false;
	}

	for (integer i = 0; i < _length; i++)
	{
		if (!GetChar(i).IsDigit())
		{
			return false;
		}
	}

	return true;
}

//===========================================================================//

string string::Insert(integer index, const string & someString) const
{
#ifdef DEBUGGING
	if (index < 0 || index > _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _length));
	}
#endif // DEBUGGING
	CheckForOverflow(_length + someString._length, _lengthInBytes + someString._lengthInBytes);

	StringBuilder builder(*this);
	builder.Insert(index, someString);
	return builder.ToString();
}

//===========================================================================//

string string::Remove(integer index, integer count) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _length - 1));
	}

	if (count < 0 || index + count > _length)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("count out of bounds: {0} (index: {1}) ", count, index));
	}
#endif // DEBUGGING
	StringBuilder builder(*this);
	builder.Remove(index, count);
	return builder.ToString();
}

//===========================================================================//

string string::Substring(integer index, integer count) const
{
#ifdef DEBUGGING
	if (index < 0 || index > _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _length - 1));
	}

	if (count < 0 || index + count > _length)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("count out of bounds: {0} (index: {1}) ", count, index));
	}
#endif // DEBUGGING

	if (index == _length || count == 0)
	{
		return string::Empty;
	}

	integer byteIndex = UTF8Helper::GetByteIndex(_dataPtr, _lengthInBytes, index);
	UTFLength substringLength = UTF8Helper::GetLength(_dataPtr, byteIndex, count);

	return string(_dataPtr + byteIndex, substringLength.LengthInChars, substringLength.LengthInComponents);
}

//===========================================================================//

string string::Substring(integer index) const
{
	integer count = _length - index;
	return Substring(index, count);
}

//===========================================================================//

string string::Replace(const string & oldString, const string & newString) const
{
	if (IsEmpty())
	{
		return Empty;
	}

	StringBuilder builder(*this);
	builder.Replace(oldString, newString);
	CheckForOverflow(builder.GetLength(), builder.GetLengthInBytes());
	return builder.ToString();
}

//===========================================================================//

string string::Reverse() const
{
	if (IsEmpty())
	{
		return Empty;
	}

	StringBuilder builder(_lengthInBytes);

	for (integer i = _length - 1; i >= 0; i--)
	{
		builder.Append(GetChar(i));
	}

	return builder.ToString();
}

//===========================================================================//

string string::TrimLeft() const
{
	if (IsEmpty())
	{
		return Empty;
	}

	integer index = 0;
	while (index < _length && BlankChars.Contains(GetChar(index)))
	{
		index++;
	}

	return (index < _length) ? Substring(index) : Empty;
}

//===========================================================================//

string string::TrimRight() const
{
	if (IsEmpty())
	{
		return Empty;
	}

	integer index = _length - 1;
	while (index >= 0 && BlankChars.Contains(GetChar(index)))
	{
		index--;
	}

	return (index >= 0) ? Substring(0, index + 1) : Empty;
}

//===========================================================================//

string string::Trim() const
{
	if (IsEmpty())
	{
		return Empty;
	}

	integer leftIndex = 0;
	while (leftIndex < _length && BlankChars.Contains(GetChar(leftIndex)))
	{
		leftIndex++;
	}

	if (leftIndex >= _length)
	{
		return Empty;
	}

	integer rightIndex = _length - 1;
	while (rightIndex >= 0 && BlankChars.Contains(GetChar(rightIndex)))
	{
		rightIndex--;
	}

	if (rightIndex < 0)
	{
		return Empty;
	}

	return Substring(leftIndex, rightIndex - leftIndex + 1);
}

//===========================================================================//

string string::CleanUp() const
{
	if (IsEmpty())
	{
		return Empty;
	}

	StringBuilder builder(_lengthInBytes);

	bool wasSpace = false;
	for (integer i = 0; i < _length; i++)
	{
		utf8_char currentChar = GetChar(i);

		if (!wasSpace)
		{
			wasSpace = currentChar == Space;
		}

		if (!BlankChars.Contains(currentChar))
		{
			if (builder.GetLength() != 0 && wasSpace)
			{
				builder.Append(Space);
				wasSpace = false;
			}

			builder.Append(currentChar);
		}
	}

	return builder.ToString();
}

//===========================================================================//

string string::ToLower() const
{
	StringBuilder builder(_lengthInBytes);
	for (integer i = 0; i < _length; i++)
	{
		builder.Append(CaseConvertationTable::GetLowerCase(GetChar(i)));
	}
	return builder.ToString();
}

//===========================================================================//

string string::ToUpper() const
{
	StringBuilder builder(_lengthInBytes);
	for (integer i = 0; i < _length; i++)
	{
		builder.Append(CaseConvertationTable::GetUpperCase(GetChar(i)));
	}
	return builder.ToString();
}

//===========================================================================//

bool string::Contains(const string & searchingString) const
{
	if (IsEmpty() || searchingString.IsEmpty())
	{
		return false;
	}

	return StringBuilder::GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, searchingString, 0) != -1;
}

//===========================================================================//

integer string::ContainsCount(const string & searchingString) const
{
	if (IsEmpty() || searchingString.IsEmpty())
	{
		return 0;
	}

	integer containsCount = 0;
	integer index = StringBuilder::GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, searchingString, 0);
	while (index != -1)
	{
		containsCount++;
		index = StringBuilder::GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, searchingString, index + 1);
	}

	return containsCount;
}

//===========================================================================//

integer string::FirstIndexOf(const string & searchingString, integer startIndex) const
{
#ifdef DEBUGGING
	if (!(IsEmpty() && startIndex == 0))
	{
		if (startIndex < 0 || startIndex >= _length)
		{
			INDEX_OUT_OF_BOUNDS_EXCEPTION(
			    string::Format("startIndex out of bounds: {0} ([0..{1}])", startIndex, _length - 1));
		}
	}
#endif // DEBUGGING
	if (IsEmpty() || searchingString.IsEmpty())
	{
		return -1;
	}

	integer startByteIndex = UTF8Helper::GetByteIndex(_dataPtr, _lengthInBytes, startIndex);
	integer byteIndex = StringBuilder::GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, searchingString,
	                    startByteIndex);
	return byteIndex != -1 ? UTF8Helper::GetCharIndex(_dataPtr, _lengthInBytes, byteIndex) : -1;
}

//===========================================================================//

integer string::FirstIndexNumberOf(const string & searchingString, integer ordinalNumber) const
{
#ifdef DEBUGGING
	if (ordinalNumber < 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("ordinalNumber must be >= 1, but it is {0}", ordinalNumber));
	}
#endif // DEBUGGING
	if (IsEmpty() || searchingString.IsEmpty())
	{
		return -1;
	}

	integer currentOrdinalNumber = 0;
	integer byteIndex = StringBuilder::GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, searchingString, 0);
	while (byteIndex != -1)
	{
		currentOrdinalNumber++;
		if (currentOrdinalNumber == ordinalNumber)
		{
			return UTF8Helper::GetCharIndex(_dataPtr, _lengthInBytes, byteIndex);
		}

		byteIndex = StringBuilder::GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, searchingString, byteIndex + 1);
	}

	return -1;
}

//===========================================================================//

integer string::LastIndexOf(const string & searchingString) const
{
	if (IsEmpty() || searchingString.IsEmpty())
	{
		return -1;
	}

	return LastIndexOf(searchingString, GetLength() - 1);
}

//===========================================================================//

integer string::LastIndexOf(const string & searchingString, integer startIndex) const
{
#ifdef DEBUGGING
	if (startIndex < 0 || startIndex >= _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(
		    string::Format("startIndex out of bounds: {0} ([0..{1}])", startIndex, _length - 1));
	}
#endif // DEBUGGING
	if (IsEmpty() || searchingString.IsEmpty())
	{
		return -1;
	}

	string cuttedString = Substring(0, startIndex + 1);

	integer lastOrdinalNumber = cuttedString.ContainsCount(searchingString);

	if (lastOrdinalNumber == 0)
	{
		return -1;
	}

	return cuttedString.FirstIndexNumberOf(searchingString, lastOrdinalNumber);
}

//===========================================================================//

integer string::LastIndexNumberOf(const string & searchingString, integer ordinalNumber) const
{
#ifdef DEBUGGING
	if (ordinalNumber < 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("ordinalNumber must be >= 1, but it is {0}", ordinalNumber));
	}
#endif // DEBUGGING
	if (IsEmpty() || searchingString.IsEmpty())
	{
		return -1;
	}

	integer ordinalNumberFromEnd = ContainsCount(searchingString) - (ordinalNumber - 1);

	if (ordinalNumberFromEnd <= 0)
	{
		return -1;
	}

	return FirstIndexNumberOf(searchingString, ordinalNumberFromEnd);
}

//===========================================================================//

integer string::FirstIndexOfNotBlankChar(integer startIndex) const
{
#ifdef DEBUGGING
	if (startIndex < 0 || startIndex >= _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(
		    string::Format("startIndex out of bounds: {0} ([0..{1}])", startIndex, _length - 1));
	}
#endif // DEBUGGING
	integer i = startIndex;
	while (i < _length)
	{
		if (!BlankChars.Contains(GetChar(i)))
		{
			return i;
		}
		i++;
	}

	return -1;
}

//===========================================================================//

integer string::GetLength() const
{
	return _length;
}

//===========================================================================//

integer string::GetLengthInBytes() const
{
	return _lengthInBytes;
}

//===========================================================================//

void string::Assign(const char * chars)
{
	UTFLength charsLength = UTF8Helper::GetLength(chars);

	CheckForOverflow(charsLength.LengthInChars, charsLength.LengthInComponents);

	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_length = charsLength.LengthInChars;
	_lengthInBytes = charsLength.LengthInComponents;
	_dataPtr = new char[_lengthInBytes + 1];
	if (_lengthInBytes > 0)
	{
		Memory::Copy(_dataPtr, chars, _lengthInBytes);
	}
	_dataPtr[_lengthInBytes] = NullTerminator;
}

//===========================================================================//

void string::ResizeData(integer newLengthInBytes)
{
	//4 - максимальное количество байт на символ
	if (newLengthInBytes < 0 || newLengthInBytes > string::MaxLength * 4)
	{
		OVERFLOW_EXCEPTION(string::Format("New length in bytes {0} overflow", newLengthInBytes));
	}

	if (_dataPtr != NullPtr)
	{
		if (newLengthInBytes == 0)
		{
			delete[] _dataPtr;

			_lengthInBytes = newLengthInBytes;
			_dataPtr = new char[newLengthInBytes + 1];
			return;
		}

		char * newData = new char[newLengthInBytes + 1];

		integer savedCharsCount = newLengthInBytes < _lengthInBytes ? newLengthInBytes : _lengthInBytes;
		Memory::Copy(newData, _dataPtr, savedCharsCount);

		delete[] _dataPtr;

		_lengthInBytes = newLengthInBytes;
		_dataPtr = newData;
	}
	else
	{
		_lengthInBytes = newLengthInBytes;
		_dataPtr = new char[_lengthInBytes + 1];
	}
}

//===========================================================================//

void string::ResetCashedIndices() const
{
	_cashedIndex = 0;
	_cashedByteIndex = 0;
}

//===========================================================================//

string string::CreateFormattedString(const string & formatString, const string * arguments[], integer argumentsCount)
{
	StringBuilder builder(formatString.GetLengthInBytes() + argumentsCount * FormatArgumentLength);
	builder.AppendFormat(formatString, arguments, argumentsCount);
	CheckForOverflow(builder.GetLength(), builder.GetLengthInBytes());
	return builder.ToString();
}

//===========================================================================//

void string::DeepCopy(const string & other)
{
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_lengthInBytes = other._lengthInBytes;
	_length = other._length;
	_dataPtr = new char[_lengthInBytes + 1];
	Memory::Copy(_dataPtr, other._dataPtr, _lengthInBytes + 1); //+ 1 чтобы скопировать NullTerminator

	_cashedIndex = other._cashedIndex;
	_cashedByteIndex = other._cashedByteIndex;
}

//===========================================================================//

void string::CheckForOverflow(integer length, integer lengthInBytes)
{
	if (length > string::MaxLength)
	{
		OVERFLOW_EXCEPTION(string::Format("Target length {0} is more than max string size", length));
	}

	if (lengthInBytes > string::MaxLength * 4)
	{
		OVERFLOW_EXCEPTION(string::Format("Tager length in bytes {0} is more than max string size", lengthInBytes));
	}
}

//===========================================================================//

bool operator ==(const char * someChars, string someString)
{
	return someString == someChars;
}

//===========================================================================//

} // namespace Bru

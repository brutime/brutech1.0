//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Вспомогательный класс для работы с UTF16 последовательностью символов
//

#pragma once
#ifndef UTF16_HELPER_H
#define UTF16_HELPER_H

#include "../BaseTypes.h"
#include "UTFLength.h"
#include "UTF8Char.h"

namespace Bru
{

//===========================================================================//

class UTF16Helper
{
public:
	static const ushort FirstCodeInUTF16Sequence;
	static const ushort LastCodeInUTF16Sequence;

	static const ushort FirstCodeInFirstSurrogateRange;
	static const ushort LastCodeInFirstSurrogateRange;

	static const ushort FirstCodeInSecondSurrogateRange;
	static const ushort LastCodeInSecondSurrogateRange;

	static const ushort FirstCodeInFirstNoncharRange;
	static const ushort LastCodeInFirstNoncharRange;

	static const ushort FirstCodeInSecondNoncharRange;
	static const ushort LastCodeInSecondNoncharRange;

	static UTFLength GetLength(const ushort * chars);

	///Метод возвращает длину символа по его первому слову
	static integer GetCharLengthInWords(const ushort firstWord);

	///Метод ищет невалидные символы в UTF16 последовательности
	static void Check(const ushort * chars);

private:
	///Проверяет 1 байт UTF16 последовательности
	static void CheckFirstWord(ushort firstWord);
	///Проверяет 2 байт UTF16 последовательности
	static void CheckSecondWord(ushort secondWord);
	///Проверяет содержит ли символ nonchar суррогаты
	static void CheckForNoncharSurrogates(const ushort * chars);

	UTF16Helper() = delete;
	UTF16Helper(const UTF16Helper &) = delete;
	~UTF16Helper() = delete;
	UTF16Helper & operator =(const UTF16Helper &) = delete;
};

//===========================================================================//

}//namespace

#endif // UTF16_HELPER_H

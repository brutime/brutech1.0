//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StringBuilder.h"

#include "../../Helpers/Memory.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidFormatException.h"
#include "../Converter.h"
#include "UTF8Helper.h"

namespace Bru
{

//===========================================================================//

const string StringBuilder::Delimeter = ".";

//===========================================================================//

StringBuilder::StringBuilder() :
	_length(0),
	_lengthInBytes(0),
	_reservedLengthInBytes(0),
	_dataPtr(new char[_reservedLengthInBytes + 1])
{
	_dataPtr[_reservedLengthInBytes] = NullTerminator;
}

//===========================================================================//

StringBuilder::StringBuilder(const string & sourceString) :
	_length(sourceString.GetLength()),
	_lengthInBytes(sourceString._lengthInBytes),
	_reservedLengthInBytes(_lengthInBytes),
	_dataPtr(new char[_reservedLengthInBytes + 1])
{
	Memory::Copy(_dataPtr, sourceString.ToChars(), _lengthInBytes + 1);
}

//===========================================================================//

StringBuilder::StringBuilder(const string & sourceString, integer moreReservedLengthInChars) :
	_length(sourceString.GetLength()),
	_lengthInBytes(sourceString._lengthInBytes),
	_reservedLengthInBytes(_lengthInBytes + moreReservedLengthInChars * 4),
	_dataPtr(NullPtr)
{
#ifdef DEBUGGING
	if (moreReservedLengthInChars < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("moreReservedLengthInChars must be > 0, but it is {0}", moreReservedLengthInChars));
	}
#endif

	_dataPtr = new char[_reservedLengthInBytes + 1];
	Memory::Copy(_dataPtr, sourceString.ToChars(), _lengthInBytes + 1);
}

//===========================================================================//

StringBuilder::StringBuilder(integer moreReservedLengthInChars) :
	_length(0),
	_lengthInBytes(0),
	_reservedLengthInBytes(moreReservedLengthInChars * 4),
	_dataPtr(NullPtr)
{
#ifdef DEBUGGING
	if (_reservedLengthInBytes < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("moreReservedLengthInChars must be > 0, but it is {0}", _reservedLengthInBytes));
	}
#endif

	_dataPtr = new char[_reservedLengthInBytes + 1];
	_dataPtr[0] = NullTerminator;
}

//===========================================================================//

StringBuilder::~StringBuilder()
{
	delete[] _dataPtr;
}

//===========================================================================//

StringBuilder & StringBuilder::Append(const string & someString)
{
	return Append(someString.ToChars(), someString.GetLength(), someString.GetLengthInBytes());
}

//===========================================================================//

StringBuilder & StringBuilder::Append(const utf8_char & someChar)
{
	return Append(someChar.ToChars(), someChar.GetLength(), someChar.GetLengthInBytes());
}

//===========================================================================//

StringBuilder & StringBuilder::Insert(integer index, const string & someString)
{
	if (someString._lengthInBytes == 0)
	{
		return *this;
	}

	if (index == _length)
	{
		Append(someString);
		return *this;
	}

	if (_lengthInBytes + someString._lengthInBytes > _reservedLengthInBytes)
	{
		Grow(someString._lengthInBytes);
	}

	integer byteIndex = UTF8Helper::GetByteIndex(_dataPtr, _lengthInBytes, index);

	Memory::Move(_dataPtr + byteIndex + someString._lengthInBytes, _dataPtr + byteIndex, _lengthInBytes - byteIndex);

	Memory::Copy(_dataPtr + byteIndex, someString._dataPtr, someString._lengthInBytes);

	_length += someString._length;
	_lengthInBytes += someString._lengthInBytes;
	_dataPtr[_lengthInBytes] = NullTerminator;
	return *this;
}

//===========================================================================//

StringBuilder & StringBuilder::Remove(integer index, integer count)
{
	if (count == 0)
	{
		return *this;
	}

	integer byteIndex = UTF8Helper::GetByteIndex(_dataPtr, _lengthInBytes, index);
	integer partLengthInBytes = UTF8Helper::GetLength(_dataPtr, byteIndex, count).LengthInComponents;

	Memory::Move(_dataPtr + byteIndex, _dataPtr + byteIndex + partLengthInBytes,
	             _lengthInBytes - byteIndex - partLengthInBytes);

	_length -= count;
	_lengthInBytes -= partLengthInBytes;
	_dataPtr[_lengthInBytes] = NullTerminator;

	if (_lengthInBytes < GetDeltaDecreaseLength())
	{
		Decrease();
	}
	return *this;
}

//===========================================================================//

StringBuilder & StringBuilder::Replace(const string & oldString, const string & newString)
{
	if (_length == 0)
	{
		return *this;
	}

	integer indexInBytes = GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, oldString, 0);
	while (indexInBytes != -1)
	{
		integer newDataPtrLengthInBytes = _lengthInBytes + newString._lengthInBytes - oldString._lengthInBytes;
		if (newDataPtrLengthInBytes > _reservedLengthInBytes)
		{
			Grow(newString._lengthInBytes - oldString._lengthInBytes);
		}

		if (newString._lengthInBytes != oldString._lengthInBytes)
		{
			Memory::Move(_dataPtr + indexInBytes + newString._lengthInBytes,
			             _dataPtr + indexInBytes + oldString._lengthInBytes,
			             _lengthInBytes - indexInBytes - oldString._lengthInBytes);
		}

		if (newString._lengthInBytes > 0)
		{
			Memory::Copy(_dataPtr + indexInBytes, newString.ToChars(), newString._lengthInBytes);
		}

		_length = _length - oldString.GetLength() + newString.GetLength();
		_lengthInBytes = newDataPtrLengthInBytes;

		indexInBytes = GetSubstringIndexInBytes(_dataPtr, _lengthInBytes, oldString,
		                                        indexInBytes + newString._lengthInBytes);
	}
	_dataPtr[_lengthInBytes] = NullTerminator;
	return *this;
}

//===========================================================================//

void StringBuilder::Clear()
{
	delete[] _dataPtr;

	_length = 0;
	_lengthInBytes = 0;
	_reservedLengthInBytes = 0;
	_dataPtr = new char[_reservedLengthInBytes + 1];
	_dataPtr[0] = NullTerminator;
}

//===========================================================================//

string StringBuilder::ToString() const
{
	return string(_dataPtr, _length, _lengthInBytes);
}

//===========================================================================//

void StringBuilder::AppendFormat(const string & formatString, const string * arguments[], integer argumentsCount)
{
	integer textStartIndex = 0;
	integer substringLength = 0;
	integer substringLengthInBytes = 0;

	integer argumentIndex = -1;

	integer index = 0;
	while (index < formatString.GetLengthInBytes())
	{
		char currentChar = formatString._dataPtr[index];
		if (currentChar == '}')
		{
			if (index + 1 < formatString.GetLengthInBytes() && formatString._dataPtr[index + 1] == '}')
			{
				if (substringLength > 0)
				{
					Append(formatString._dataPtr + textStartIndex, substringLength, substringLengthInBytes);
					substringLength = 0;
					substringLengthInBytes = 0;
				}

				index++;

				textStartIndex = index;
			}
			else
			{
				INVALID_FORMAT_EXCEPTION(string::Format("Not paired brackets in format string: \"{0}\"", formatString));
			}
		}
		else if (currentChar == '{')
		{
			if (index + 1 < formatString.GetLengthInBytes() && formatString._dataPtr[index + 1] == '{')
			{
				if (substringLength > 0)
				{
					Append(formatString._dataPtr + textStartIndex, substringLength, substringLengthInBytes);
					substringLength = 0;
					substringLengthInBytes = 0;
				}

				index++;

				textStartIndex = index;
			}
			else
			{
				if (substringLength > 0)
				{
					Append(formatString._dataPtr + textStartIndex, substringLength, substringLengthInBytes);
					substringLength = 0;
					substringLengthInBytes = 0;
				}

				currentChar = GetNextChar(formatString, index, 1);
				argumentIndex = currentChar - 48;

				if (argumentIndex < 0 || argumentIndex >= argumentsCount)
				{
					INVALID_FORMAT_EXCEPTION(string::Format("Argument index out of bounds in format string: \"{0}\"", formatString));
				}

				currentChar = GetNextChar(formatString, index, 1);

				if (currentChar == '}')
				{
					Append(*arguments[argumentIndex]);
					index++;
				}
				else if (currentChar == ':')
				{
					currentChar = GetNextChar(formatString, index, 1);

					integer prefixStartIndex = index;
					integer prefixLength = 0;
					integer prefixLengthInBytes = 0;

					while (currentChar != '}' && currentChar != '.')
					{
						integer charLengthInBytes = UTF8Helper::GetCharLengthInBytes(currentChar);

						currentChar = GetNextChar(formatString, index, charLengthInBytes);

						prefixLength++;
						prefixLengthInBytes += charLengthInBytes;
					}

					string argument = *arguments[argumentIndex];
					if (currentChar == '.')
					{
						currentChar = GetNextChar(formatString, index, 1);

						integer precisionStartIndex = index;
						integer precisionStringLength = 0;
						integer precisionStringLengthInBytes = 0;

						//Цифры однобайтовые
						while (currentChar != '}')
						{
							if (currentChar < '0' || currentChar > '9')
							{
								INVALID_FORMAT_EXCEPTION(string::Format("Invalid symbols in precision: \"{0}\"", formatString));
							}

							currentChar = GetNextChar(formatString, index, 1);
							precisionStringLength++;
							precisionStringLengthInBytes++;
						}

						string precisionString(formatString._dataPtr + precisionStartIndex,
						                       precisionStringLength, precisionStringLengthInBytes);
						integer precision = Converter::ToInteger(precisionString);
						argument = RoundFloat(argument, precision);
					}

					if (prefixLength > 0)
					{
						string argumentPrefix(formatString._dataPtr + prefixStartIndex, prefixLength, prefixLengthInBytes);
						integer notNeededCharsCount = argumentPrefix.GetLength() - argument.GetLength();
						if (notNeededCharsCount > 0)
						{
							argumentPrefix = argumentPrefix.Remove(argumentPrefix.GetLength() - argument.GetLength(), argument.GetLength());
							Append(argumentPrefix);
						}
					}

					Append(argument);

					index++;
				}
				else
				{
					INVALID_FORMAT_EXCEPTION(string::Format("Not paired brackets or bad symbols between brackets in format string: \"{0}\"", formatString));
				}

				textStartIndex = index;
				continue;
			}
		}

		integer charLengthInBytes = UTF8Helper::GetCharLengthInBytes(currentChar);

		substringLength++;
		substringLengthInBytes += charLengthInBytes;

		index += charLengthInBytes;
	}

	if (substringLength > 0)
	{
		Append(formatString._dataPtr + textStartIndex, substringLength, substringLengthInBytes);
	}
}

//===========================================================================//

integer StringBuilder::GetSubstringIndexInBytes(const char * chars, integer charsLengthInBytes,
        const string & searchingString, integer indexInBytes)
{
	integer * nextArray = new integer[searchingString._lengthInBytes];
	nextArray[0] = -1;

	integer i = 0;
	integer j = -1;

	for (i = 0; i < searchingString._lengthInBytes; i++)
	{
		nextArray[i] = j;
		if (j >= 0)
		{
			while (j >= 0 && searchingString._dataPtr[i] != searchingString._dataPtr[j])
			{
				j = nextArray[j];
			}
		}
		j++;
	}

	j = 0;

	for (i = indexInBytes; i < charsLengthInBytes && j < searchingString._lengthInBytes; i++)
	{
		if (j >= 0)
		{
			while (j >= 0 && chars[i] != searchingString._dataPtr[j])
			{
				j = nextArray[j];
			}
		}
		j++;
	}

	delete[] nextArray;

	if (j == searchingString._lengthInBytes)
	{
		return i - searchingString._lengthInBytes;
	}

	return -1;
}

//===========================================================================//

StringBuilder & StringBuilder::Append(const char * chars, integer length, integer lengthInBytes)
{
	if (_lengthInBytes + lengthInBytes > _reservedLengthInBytes)
	{
		Grow(lengthInBytes);
	}

	Memory::Copy(_dataPtr + _lengthInBytes, chars, lengthInBytes);
	_length += length;
	_lengthInBytes += lengthInBytes;
	_dataPtr[_lengthInBytes] = NullTerminator;
	return *this;
}

//===========================================================================//

string StringBuilder::RoundFloat(const string & floatString, integer precision)
{
	integer delimeterIndex = floatString.FirstIndexOf(Delimeter);
	integer currentPrecision = floatString.GetLength() - delimeterIndex - 1;
	if (currentPrecision == precision)
	{
		return floatString;
	}
	else if (currentPrecision < precision)
	{
		integer zerosCount = precision - currentPrecision;
		char * zeros = new char[zerosCount + 1];
		Memory::FillWithValue(zeros, '0', zerosCount);
		zeros[zerosCount] = NullTerminator;

		StringBuilder builder(floatString, zerosCount);
		builder.Append(zeros, zerosCount, zerosCount);
		return builder.ToString();
	}
	else
	{
		char * floatData = new char[floatString._lengthInBytes + 1];
		Memory::Copy(floatData, floatString._dataPtr, floatString._lengthInBytes + 1);

		integer index = delimeterIndex + precision + 1;
		while (floatData[index] >= '5')
		{
			floatData[index] = '0';
			index--;
			if (floatData[index] == '.')
			{
				index--;
			}

			if (index >= 0)
			{
				floatData[index]++;
				if (floatData[index] <= '9')
				{
					break;
				}
			}
			else
			{
				Memory::Move(floatData + 1, floatData, floatString._lengthInBytes);
				floatData[0] = '1';
				delimeterIndex++;
				break;
			}
		}

		integer floatDataCorrectedLength = delimeterIndex + precision + 1;
		if (floatData[floatDataCorrectedLength - 1] == '.')
		{
			floatDataCorrectedLength--;
		}
		floatData[floatDataCorrectedLength] = NullTerminator;
		string resultString(floatData, floatDataCorrectedLength, floatDataCorrectedLength);

		delete[] floatData;

		return resultString;
	}
}

//===========================================================================//

char StringBuilder::GetNextChar(const string & formatString, integer & index, integer count)
{
	index += count;
	if (index >= formatString.GetLengthInBytes())
	{
		INVALID_FORMAT_EXCEPTION(string::Format("Not expected end of line: ", formatString));
	}
	return formatString._dataPtr[index];
}

//===========================================================================//

void StringBuilder::Grow(integer stringLengthInBytes)
{
	integer deltaGrowLength = _reservedLengthInBytes > 0 ? _reservedLengthInBytes : 1;
	while (_lengthInBytes + stringLengthInBytes > _reservedLengthInBytes + deltaGrowLength)
	{
		deltaGrowLength *= 2;
	}
	deltaGrowLength = deltaGrowLength > 0 ? deltaGrowLength : 1;
	ResizeData(_reservedLengthInBytes + deltaGrowLength);
}

//===========================================================================//

void StringBuilder::Decrease()
{
	ResizeData(GetDeltaDecreaseLength());
}

//===========================================================================//

integer StringBuilder::GetDeltaDecreaseLength() const
{
	return _reservedLengthInBytes / 2;
}

//===========================================================================//

void StringBuilder::ResizeData(integer reservedLengthInBytes)
{
#ifdef DEBUGGING
	if (reservedLengthInBytes < 0)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("reservedLengthInBytes must be > 0, but it is {0}", reservedLengthInBytes));
	}
#endif

	if (reservedLengthInBytes == _reservedLengthInBytes)
	{
		return;
	}

	char * newDataPtr = new char[reservedLengthInBytes + 1];

	integer savedDataLengthInBytes = _lengthInBytes < reservedLengthInBytes ? _lengthInBytes : reservedLengthInBytes;
	Memory::Copy(newDataPtr, _dataPtr, savedDataLengthInBytes);

	delete[] _dataPtr;

	_reservedLengthInBytes = reservedLengthInBytes;
	_dataPtr = newDataPtr;
}

//===========================================================================//

}//namespace

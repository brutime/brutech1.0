//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "CaseConvertationTable.h"

#include "CasePair.h"

namespace Bru
{

//===========================================================================//

static const CasePair UpperToLowerTable[] =
{
	//Английский алфавит
	{ "A", "a" }, { "B", "b" }, { "C", "c" }, { "D", "d" }, { "E", "e" }, { "F", "f" }, { "G", "g" },
	{ "H", "h" }, { "I", "i"}, { "J", "j" }, { "K", "k" }, { "L", "l" }, { "M", "m" }, { "N", "n" },
	{ "O", "o" }, { "P", "p" }, { "Q", "q"}, { "R", "r" }, { "S", "s" }, { "T", "t" }, { "U", "u" },
	{ "V", "v" }, { "W", "w" }, { "X", "x" }, { "Y", "y" }, { "Z", "z" },

	//Русский алфавит
	{ "Ё", "ё" }, { "А", "а" }, { "Б", "б" }, { "В", "в" }, { "Г", "г" }, { "Д", "д" }, { "Е", "е" },
	{ "Ж", "ж" }, { "З", "з" }, { "И", "и" }, { "Й", "й" }, { "К", "к" }, { "Л", "л" }, { "М", "м" },
	{ "Н", "н" }, { "О", "о" }, { "П", "п" }, { "Р", "р" }, { "С", "с" }, { "Т", "т" }, { "У", "у" },
	{ "Ф", "ф" }, { "Х", "х" }, { "Ц", "ц" }, { "Ч", "ч" }, { "Ш", "ш" }, { "Щ", "щ" }, { "Ъ", "ъ" },
	{ "Ы", "ы" }, { "Ь", "ь" }, { "Э", "э" }, { "Ю", "ю" }, { "Я", "я" }
};

//===========================================================================//

const integer UpperToLowerTableCount = static_cast<integer>(sizeof(UpperToLowerTable) / sizeof(CasePair));

//===========================================================================//

static const CasePair LowerToUpperTable[] =
{
	//Английский алфавит
	{ "A", "a" }, { "B", "b" }, { "C", "c" }, { "D", "d" }, { "E", "e" }, { "F", "f" }, { "G", "g" },
	{ "H", "h" }, { "I", "i" }, { "J", "j" }, { "K", "k" }, { "L", "l" }, { "M", "m" }, { "N", "n" },
	{ "O", "o" }, { "P", "p" }, { "Q", "q" }, { "R", "r" }, { "S", "s" }, { "T", "t" }, { "U", "u" },
	{ "V", "v" }, { "W", "w" }, { "X", "x" }, { "Y", "y" }, { "Z", "z" },

	//Русский алфавит
	{ "А", "а" }, { "Б", "б" }, { "В", "в" }, { "Г", "г" }, { "Д", "д" }, { "Е", "е" }, { "Ж", "ж" },
	{ "З", "з" }, { "И", "и" }, { "Й", "й" }, { "К", "к" }, { "Л", "л" }, { "М", "м" }, { "Н", "н" },
	{ "О", "о" }, { "П", "п" }, { "Р", "р" }, { "С", "с" }, { "Т", "т" }, { "У", "у" }, { "Ф", "ф" },
	{ "Х", "х" }, { "Ц", "ц" }, { "Ч", "ч" }, { "Ш", "ш" }, { "Щ", "щ" }, { "Ъ", "ъ" }, { "Ы", "ы" },
	{ "Ь", "ь" }, { "Э", "э" }, { "Ю", "ю" }, { "Я", "я" }, { "Ё", "ё" }
};

//===========================================================================//

const integer LowerToUpperTableCount = static_cast<integer>(sizeof(LowerToUpperTable) / sizeof(CasePair));

//===========================================================================//

utf8_char CaseConvertationTable::GetLowerCase(const utf8_char & someChar)
{
	integer first = 0;
	integer last = UpperToLowerTableCount;

	if (UpperToLowerTableCount == 0 || someChar < UpperToLowerTable[0].UpperCase ||
	    someChar > UpperToLowerTable[UpperToLowerTableCount - 1].UpperCase)
	{
		return someChar;
	}

	while (first < last)
	{
		integer middle = first + (last - first) / 2;

		if (someChar > UpperToLowerTable[middle].UpperCase)
		{
			first = middle + 1;
		}
		else
		{
			last = middle;
		}
	}

	if (UpperToLowerTable[last].UpperCase == someChar)
	{
		return UpperToLowerTable[last].LowerCase;
	}

	return someChar;
}

//===========================================================================//

utf8_char CaseConvertationTable::GetUpperCase(const utf8_char & someChar)
{
	integer first = 0;
	integer last = LowerToUpperTableCount;

	if (LowerToUpperTableCount == 0 || someChar < LowerToUpperTable[0].LowerCase ||
	    someChar > LowerToUpperTable[LowerToUpperTableCount - 1].LowerCase)
	{
		return someChar;
	}

	while (first < last)
	{
		integer middle = first + (last - first) / 2;

		if (someChar > LowerToUpperTable[middle].LowerCase)
		{
			first = middle + 1;
		}
		else
		{
			last = middle;
		}
	}

	if (LowerToUpperTable[last].LowerCase == someChar)
	{
		return LowerToUpperTable[last].UpperCase;
	}

	return someChar;
}

//===========================================================================//

} //namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTF16Helper.h"

#include "String.h"
#include "../../Exceptions/OverflowException.h"
#include "../../Exceptions/NullPointerException.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidUTF16SequenceException.h"

namespace Bru
{

//===========================================================================//

//Если изменится значение констант FirstCodeInUTF16Sequence, LastCodeInUTF16Sequence,
//LastCodeInSecondNoncharRange необходимо добавить полную проверку попадания ushort
//в интервалы. Сейчас в этой проверке нет необходимости, так как тип ushort - unsigned,
//а данные константы равны границам интервала.
//P.S. Маловероятно, что данные константы изменяться - это произойдет в случае изменении
//кодировки UTF16

const ushort UTF16Helper::FirstCodeInUTF16Sequence = NullTerminator;
const ushort UTF16Helper::LastCodeInUTF16Sequence = 0xFFFF;

const ushort UTF16Helper::FirstCodeInFirstSurrogateRange = 0xD800;
const ushort UTF16Helper::LastCodeInFirstSurrogateRange = 0xDBFF;

const ushort UTF16Helper::FirstCodeInSecondSurrogateRange = 0xDC00;
const ushort UTF16Helper::LastCodeInSecondSurrogateRange = 0xDFFF;

const ushort UTF16Helper::FirstCodeInFirstNoncharRange = 0xFDD0;
const ushort UTF16Helper::LastCodeInFirstNoncharRange = 0xFDEF;

const ushort UTF16Helper::FirstCodeInSecondNoncharRange = 0xFFFE;
const ushort UTF16Helper::LastCodeInSecondNoncharRange = 0xFFFF;

//===========================================================================//

static const integer NoncharSurrogatesCharsCount = 32;

static const ushort NoncharSurrogatesChars[NoncharSurrogatesCharsCount][2] = { { 0xD83F, 0xDFFE }, { 0xD83F, 0xDFFF }, {
		0xD87F, 0xDFFE
	}, { 0xD87F, 0xDFFF }, { 0xD8BF, 0xDFFE }, { 0xD8BF, 0xDFFF }, { 0xD8FF, 0xDFFE },
	{ 0xD8FF, 0xDFFF }, { 0xD93F, 0xDFFE }, { 0xD93F, 0xDFFF }, { 0xD97F, 0xDFFE }, { 0xD97F, 0xDFFF },
	{ 0xD9BF, 0xDFFE }, { 0xD9BF, 0xDFFF }, { 0xD9FF, 0xDFFE }, { 0xD9FF, 0xDFFF }, { 0xDA3F, 0xDFFE },
	{ 0xDA3F, 0xDFFF }, { 0xDA7F, 0xDFFE }, { 0xDA7F, 0xDFFF }, { 0xDABF, 0xDFFE }, { 0xDABF, 0xDFFF },
	{ 0xDAFF, 0xDFFE }, { 0xDAFF, 0xDFFF }, { 0xDB3F, 0xDFFE }, { 0xDB3F, 0xDFFF }, { 0xDB7F, 0xDFFE },
	{ 0xDB7F, 0xDFFF }, { 0xDBBF, 0xDFFE }, { 0xDBBF, 0xDFFF }, { 0xDBFF, 0xDFFE }, { 0xDBFF, 0xDFFF },
};

//===========================================================================//

UTFLength UTF16Helper::GetLength(const ushort * chars)
{
	if (chars == NullTerminator)
	{
		return UTFLength(0, 0);
	}

	const ushort * charsCursor = chars;

	integer lengthInChars = 0;
	integer lengthInComponents = 0;

	while (*charsCursor != NullTerminator)
	{
		PREFETCH(charsCursor);

		integer charSizeInWords = GetCharLengthInWords(*charsCursor);

		lengthInChars++;
		lengthInComponents += charSizeInWords;

		charsCursor += charSizeInWords;
	}

	return UTFLength(lengthInChars, lengthInComponents);
}

//===========================================================================//

integer UTF16Helper::GetCharLengthInWords(const ushort firstWord)
{
	if (firstWord < FirstCodeInFirstSurrogateRange || firstWord > LastCodeInSecondSurrogateRange)
	{
		return 1;
	}
	else if (FirstCodeInFirstSurrogateRange <= firstWord && firstWord <= LastCodeInFirstSurrogateRange)
	{
		return 2;
	}

	INVALID_UTF16_SEQUENCE_EXCEPTION("Bad first word");
}

//===========================================================================//

void UTF16Helper::Check(const ushort * chars)
{
	if (chars == NullTerminator)
	{
		return;
	}

	const ushort * charsCursor = chars;

	integer charsLength = 0;
	bool isFirstWord = true;

	while (*charsCursor != NullTerminator)
	{
		if (isFirstWord)
		{
			CheckFirstWord(*charsCursor);
			CheckForNoncharSurrogates(charsCursor);

			charsLength++;
			isFirstWord = GetCharLengthInWords(*charsCursor) == 1;
		}
		else
		{
			CheckSecondWord(*charsCursor);

			isFirstWord = true;
		}

		charsCursor++;
	}
}

//===========================================================================//

void UTF16Helper::CheckFirstWord(ushort firstWord)
{
	if (FirstCodeInSecondSurrogateRange <= firstWord && firstWord <= LastCodeInSecondSurrogateRange)
	{
		INVALID_UTF16_SEQUENCE_EXCEPTION("Char is in first word, but must be in second word");
	}
}

//===========================================================================//

void UTF16Helper::CheckSecondWord(ushort secondWord)
{
	if (FirstCodeInFirstSurrogateRange <= secondWord && secondWord <= LastCodeInFirstSurrogateRange)
	{
		INVALID_UTF16_SEQUENCE_EXCEPTION("Char is in second word, but must be in first word");
	}
	else if (secondWord < FirstCodeInSecondSurrogateRange || secondWord > LastCodeInSecondSurrogateRange)
	{
		INVALID_UTF16_SEQUENCE_EXCEPTION("Char is not in second surrigeta range");
	}
}

//===========================================================================//

void UTF16Helper::CheckForNoncharSurrogates(const ushort * chars)
{
	if ((FirstCodeInFirstNoncharRange <= *chars && *chars <= LastCodeInFirstNoncharRange)
	        || FirstCodeInSecondNoncharRange <= *chars)
	{
		INVALID_UTF16_SEQUENCE_EXCEPTION("Char represents nonchar");
	}
	else if (FirstCodeInFirstSurrogateRange <= *chars && *chars <= LastCodeInFirstSurrogateRange)
	{
		for (integer i = 0; i < NoncharSurrogatesCharsCount; i++)
		{
			if (*chars == NoncharSurrogatesChars[i][0] && *(chars + 1) == NoncharSurrogatesChars[i][1])
			{
				INVALID_UTF16_SEQUENCE_EXCEPTION("Chars represents nonchar surrogates");
			}
		}
	}
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Регистровое представление символа
//

#pragma once
#ifndef CASE_PAIR_H
#define CASE_PAIR_H

#include "UTF8Char.h"

namespace Bru
{

//===========================================================================//

struct CasePair
{
	utf8_char UpperCase;
	utf8_char LowerCase;
};

//===========================================================================//

}//namespace

#endif // CASE_PAIR_H

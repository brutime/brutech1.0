//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Данный класс работает с UTF-8 последовательностью символов.
//    Так как первые 127 символов совпадают как в ASCII (US-ASCII), так и
//    в UTF-8 - они имеют прямое преобразование. Все что поступает в обычных
//    char'ах более 127 символов - сичтается ошибочным потому что такие
//    символы шифруются с помощью UTF-8, а это неверно - представлять один
//    и тот же символ несколькими вариантами шифрования. Кроме того в движке
//    не используется 4-ех битовая последовтельность символов
//
//    +------------+-----------+---------------------------------------------------------------------------+
//    | Диапозон 1-го байта    | Интерпритация                                                             |
//    +------------+-----------+ (символами !!! помечены варианты ошибочного использования певрого байта)  |
//    | HEX        | Char      |                                                                           |
//    +------------+-----------+---------------------------------------------------------------------------+
//    | 0x00..0x7F | 0..127    | Символы совместимые с US-ASCII (могут быть только в 1-ом байте)           |
//    | 0x80..0xBF | -128..-65 | !!! Эти символы могут быть только во 2, 3 или 4 байте                     |
//    | 0xC0..0xC1 | -64..-63  | !!! Излишние шифрование - начинается как 2-ух байтовая                    |
//    |            |           | последовательность, но шифрует симлов <= 127 (то есть достаточно 1 байта) |
//    | 0xC2..0xDF | -62..-33  | Начало 2-ух байтовой последовательности                                   |
//    | 0xE0..0xEF | -32..-17  | Начало 3-ех байтовой последовательности                                   |
//    | 0xF0..0xF4 | -16..-12  | Начало 4-ех байтовой последовательности                                   |
//    | 0xF5..0xFD | -11..-3   | !!! Огрначено RFC 3629                                                    |
//    | 0xFE..0xFF | -2..-1    | !!! Не опеределено в UTF-8 спецификации                                   |
//    +------------+-----------+---------------------------------------------------------------------------+
//

#include "UTF8Helper.h"

#include "String.h"
#include "../../Helpers/Memory.h"
#include "../../Exceptions/InvalidStateException.h"
#include "../../Exceptions/IndexOutOfBoundsException.h"
#include "../../Exceptions/InvalidUTF8SequenceException.h"

namespace Bru
{

//===========================================================================//

//Если изменится значения констант LastCodeInOneByteSequenceCode, FirstCodeInWrongFirstByteSequence
//необходимо добавить полную проверку попадания char в интервалы. Сейчас в этой проверке нет необходимости,
//так как данные константы равны границам интервала.
//P.S. Маловероятно, что данные константы изменяться - это произойдет в случае изменении
//кодировки UTF8

const char UTF8Helper::FirstCodeInOneByteSequenceCode = 0;
const char UTF8Helper::LastCodeInOneByteSequenceCode = 127;

const char UTF8Helper::FirstCodeInTwoByteSequenceCode = -62;
const char UTF8Helper::LastCodeInTwoByteSequenceCode = -33;

const char UTF8Helper::FirstCodeInThreeByteSequenceCode = -32;
const char UTF8Helper::LastCodeInThreeByteSequenceCode = -17;

const char UTF8Helper::FirstCodeInFourByteSequenceCode = -16;
const char UTF8Helper::LastCodeInFourByteSequenceCode = -12;

const char UTF8Helper::FirstCodeInWrongFirstByteSequence = -128;
const char UTF8Helper::LastCodeInWrongFirstByteSequence = -65;

const char UTF8Helper::FirstCodeInOverlongSequenceCode = -64;
const char UTF8Helper::LastCodeInOverlongSequenceCode = -63;

const char UTF8Helper::FirstCodeInRestrictedByRFC3629Code = -11;
const char UTF8Helper::LastRestrictedByRFC3629Code = -3;

const char UTF8Helper::FirstNotDefinedBySpecificationCode = -2;
const char UTF8Helper::LastNotDefinedBySpecificationCode = -1;

const char UTF8Helper::FirstCodeInUTF8Sequence = NullTerminator;
const char UTF8Helper::LastCodeInUTF8Sequence[4] = { -37, -1, -33, -1 };

//===========================================================================//

const integer UTF8Helper::CharSizes[256] =
{
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 1, 1
};

//===========================================================================//

static const integer NoncharSurrogatesCharsCount = 32;

static const char NoncharSurrogatesChars[NoncharSurrogatesCharsCount][4] = { { -16, -97, -65, -66 }, { -16, -97, -65, -65 },
	{ -16, -81, -65, -66 }, { -16, -81, -65, -65 }, { -16, -65, -65, -66 }, { -16, -65, -65, -65 }, { -15, -113, -65, -66 },
	{ -15, -113, -65, -65 }, { -15, -97, -65, -66 }, { -15, -97, -65, -65 }, { -15, -81, -65, -66 }, { -15, -81, -65, -65 },
	{ -15, -65, -65, -66 }, { -15, -65, -65, -65 }, { -14, -113, -65, -66 }, { -14, -113, -65, -65 }, { -14, -97, -65, -66 },
	{ -14, -97, -65, -65 }, { -14, -81, -65, -66 }, { -14, -81, -65, -65 }, { -14, -65, -65, -66 }, { -14, -65, -65, -65 },
	{ -13, -113, -65, -66 }, { -13, -113, -65, -65 }, { -13, -97, -65, -66 }, { -13, -97, -65, -65 }, { -13, -81, -65, -66 },
	{ -13, -81, -65, -65 }, { -13, -65, -65, -66 }, { -13, -65, -65, -65 }, { -12, -113, -65, -66 }, { -12, -113, -65, -65 }
};

//===========================================================================//

UTF8Helper::UTF8Helper()
{
}

//===========================================================================//

UTF8Helper::~UTF8Helper()
{
}

//===========================================================================//

UTFLength UTF8Helper::GetLength(const char * chars)
{
	if (chars == NullPtr || *chars == NullTerminator)
	{
		return UTFLength(0, 0);
	}

	const char * charsCursor = chars;

	integer lengthInChars = 0;
	integer lengthInComponents = 0;

	while (*charsCursor != NullTerminator)
	{
		PREFETCH(charsCursor);

		integer charSizeInBytes = GetCharLengthInBytes(*charsCursor);

		lengthInChars++;
		lengthInComponents += charSizeInBytes;

		charsCursor += charSizeInBytes;
	}

	return UTFLength(lengthInChars, lengthInComponents);
}

//===========================================================================//

UTFLength UTF8Helper::GetLength(const char * chars, integer byteIndex, integer count)
{
	if (chars == NullPtr || *chars == NullTerminator || count == 0)
	{
		return UTFLength(0, 0);
	}

	const char * charsCursor = chars;
	charsCursor += byteIndex;

	integer lengthInChars = 0;
	integer lengthInComponents = 0;

	while (*charsCursor != NullTerminator)
	{
		PREFETCH(charsCursor);

		if (count != -1 && lengthInChars == count)
		{
			break;
		}

		integer charSizeInBytes = GetCharLengthInBytes(*charsCursor);

		lengthInChars++;
		lengthInComponents += charSizeInBytes;

		charsCursor += charSizeInBytes;
	}

	return UTFLength(lengthInChars, lengthInComponents);
}

//===========================================================================//

integer UTF8Helper::GetCharIndex(const char * chars, integer charsLengthInBytes, integer byteIndex)
{
	const char * charsCursor = chars;

	integer currentIndex = 0;
	integer currentByteIndex = 0;
	while (currentByteIndex < charsLengthInBytes)
	{
		PREFETCH(charsCursor);

		if (currentByteIndex == byteIndex)
		{
			return currentIndex;
		}
		currentIndex++;

		integer charLengthInBytes = GetCharLengthInBytes(*charsCursor);
		currentByteIndex += charLengthInBytes;

		charsCursor += charLengthInBytes;
	}

	INVALID_ARGUMENT_EXCEPTION("Indexing error");
}

//===========================================================================//

integer UTF8Helper::GetByteIndex(const char * chars, integer charsLengthInBytes, integer charIndex,
                                 integer currentCharIndex, integer currentByteIndex)
{
	const char * charsCursor = chars;
	charsCursor += currentByteIndex;

	integer index = currentCharIndex;
	integer indexInBytes = currentByteIndex;
	while (indexInBytes < charsLengthInBytes)
	{
		PREFETCH(charsCursor);

		if (index == charIndex)
		{
			return indexInBytes;
		}
		index++;

		integer charLengthInBytes = GetCharLengthInBytes(*charsCursor);
		indexInBytes += charLengthInBytes;

		charsCursor += charLengthInBytes;
	}

	INVALID_ARGUMENT_EXCEPTION("Indexing error");
}

//===========================================================================//

integer UTF8Helper::GetPreviousByteIndex(const char * chars, integer charIndex, integer currentCharIndex,
        integer currentByteIndex)
{
	const char * charsCursor = chars;
	charsCursor += currentByteIndex;

	integer index = currentCharIndex;
	integer indexInBytes = currentByteIndex;
	while (indexInBytes >= 0)
	{
		PREFETCH(charsCursor);

		if (index == charIndex)
		{
			return indexInBytes;
		}

		do
		{
			indexInBytes--;
			if (indexInBytes < 0)
			{
				INVALID_ARGUMENT_EXCEPTION("Indexing error");
			}
			charsCursor--;
		}
		while (!IsFirstByte(*charsCursor));

		index--;
	}

	INVALID_ARGUMENT_EXCEPTION("Indexing error");
}

//===========================================================================//

integer UTF8Helper::GetLengthInChars(const char * chars, integer charsLengthInBytes)
{
	if (chars == NullPtr || *chars == NullTerminator || charsLengthInBytes == 0)
	{
		return 0;
	}

	const char * charsCursor = chars;

	integer lengthInChars = 0;
	integer lengthInComponents = 0;

	while (lengthInComponents < charsLengthInBytes)
	{
		PREFETCH(charsCursor);

		integer charSizeInBytes = GetCharLengthInBytes(*charsCursor);

		lengthInChars++;
		lengthInComponents += charSizeInBytes;

		charsCursor += charSizeInBytes;
	}

	return lengthInChars;
}

//===========================================================================//

integer UTF8Helper::GetCharLengthInBytes(char firstByte)
{
	return CharSizes[static_cast<byte>(firstByte)];
}

//===========================================================================//

void UTF8Helper::Check(const char * chars, integer charsLengthInBytes)
{
	if (chars == NullPtr || *chars == NullTerminator || charsLengthInBytes == 0)
	{
		return;
	}

	const char * charsCursor = chars;

	integer currentByteIndex = 0;

	while (*charsCursor != NullTerminator)
	{
		PREFETCH(charsCursor);

		CheckFirstByte(*charsCursor);
		CheckForNoncharSurrogates(charsCursor);

		integer charSizeInBytes = GetCharLengthInBytes(*charsCursor);

		charsCursor++;
		currentByteIndex++;

		for (integer i = 0; i < charSizeInBytes - 1; i++)
		{
			CheckFollowingByte(*charsCursor);
			charsCursor++;
			currentByteIndex++;
		}
		
		if (charsLengthInBytes != -1 && currentByteIndex == charsLengthInBytes)
		{
			break;
		}		
	}
}

//===========================================================================//

bool UTF8Helper::IsFirstByte(char firstByte)
{
	return ((firstByte) & 0xC0) != 0x80;
}

//===========================================================================//

void UTF8Helper::CheckFirstByte(char firstByte)
{
	if (FirstCodeInWrongFirstByteSequence < firstByte && firstByte <= LastCodeInWrongFirstByteSequence)
	{
		INVALID_UTF8_SEQUENCE_EXCEPTION("Char is in first byte, but must be second, third, "
		                                "or fourth byte of a multi-byte sequence");
	}
	//Излишние шифрование - начинается как 2-ух байтовая
	//последовательность, но шифрует символ <= 127 (то есть достаточно 1 байта)
	else if (FirstCodeInOverlongSequenceCode <= firstByte && firstByte <= LastCodeInOverlongSequenceCode)
	{
		INVALID_UTF8_SEQUENCE_EXCEPTION("Char overlong encoding - start of 2-byte sequence, "
		                                "but would encode a code point <= 127");
	}
	//Огрначено RFC 3629
	else if (FirstCodeInRestrictedByRFC3629Code <= firstByte && firstByte <= LastRestrictedByRFC3629Code)
	{
		INVALID_UTF8_SEQUENCE_EXCEPTION("Char restricted by RFC 3629");
	}
	//Не опеределено в UTF-8 спецификации
	else if (FirstNotDefinedBySpecificationCode <= firstByte && firstByte <= LastNotDefinedBySpecificationCode)
	{
		INVALID_UTF8_SEQUENCE_EXCEPTION("Char not defined by original UTF-8 specification");
	}
}

//===========================================================================//

void UTF8Helper::CheckFollowingByte(char followingByte)
{
	if (followingByte > LastCodeInWrongFirstByteSequence)
	{
		INVALID_UTF8_SEQUENCE_EXCEPTION("Following byte out of bounds");
	}
}

//===========================================================================//

void UTF8Helper::CheckForNoncharSurrogates(const char * chars)
{
	if (chars == NullPtr || *chars == NullTerminator)
	{
		return;
	}

	if (GetCharLengthInBytes(*chars) == 4)
	{
		for (integer i = 0; i < NoncharSurrogatesCharsCount; i++)
		{
			if (*chars == NoncharSurrogatesChars[i][0] && *(chars + 1) == NoncharSurrogatesChars[i][1]
			        && *(chars + 2) == NoncharSurrogatesChars[i][2] && *(chars + 3) == NoncharSurrogatesChars[i][3])
			{
				INVALID_UTF8_SEQUENCE_EXCEPTION("Char represents nonchar surrogates");
			}
		}
	}
}

//===========================================================================//

}//namespace

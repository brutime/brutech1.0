//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef UTF8_HELPER_H
#define UTF8_HELPER_H

#include "../BaseTypes.h"
#include "UTFLength.h"

namespace Bru
{

//===========================================================================//

///Класс для внутреннего использования строкой - нет проверки входящих данных (для производительности)
class UTF8Helper
{
public:
	static const char FirstCodeInOneByteSequenceCode;
	static const char LastCodeInOneByteSequenceCode;

	static const char FirstCodeInTwoByteSequenceCode;
	static const char LastCodeInTwoByteSequenceCode;

	static const char FirstCodeInThreeByteSequenceCode;
	static const char LastCodeInThreeByteSequenceCode;

	static const char FirstCodeInFourByteSequenceCode;
	static const char LastCodeInFourByteSequenceCode;

	static const char FirstCodeInWrongFirstByteSequence;
	static const char LastCodeInWrongFirstByteSequence;

	static const char FirstCodeInOverlongSequenceCode;
	static const char LastCodeInOverlongSequenceCode;

	static const char FirstCodeInRestrictedByRFC3629Code;
	static const char LastRestrictedByRFC3629Code;

	static const char FirstNotDefinedBySpecificationCode;
	static const char LastNotDefinedBySpecificationCode;

	static const char FirstCodeInUTF8Sequence;
	static const char LastCodeInUTF8Sequence[4];

	static UTFLength GetLength(const char * chars);

	///Если метод вызван с параметром count равным -1, то возвращается длина вычисляется до конца строки
	static UTFLength GetLength(const char * chars, integer byteIndex, integer count = -1);

	///Метод возвращает длину символа по его первому байту
	static integer GetCharIndex(const char * chars, integer charsLengthInBytes, integer byteIndex);

	///Метод возвращает первый байт по индексу символа в строке, дополнительно передается позиция текущего символа
	static integer GetByteIndex(const char * chars, integer charsLengthInBytes, integer charIndex,
	                            integer currentCharIndex = 0, integer currentByteIndex = 0);

	///Метод возвращает первый байт по индексу символа в строке, дополнительно передается позиция текущего символа
	static integer GetPreviousByteIndex(const char * chars, integer charIndex, integer currentCharIndex,
	                                    integer currentByteIndex);

	///Метод возвращает длину в символах
	static integer GetLengthInChars(const char * chars, integer charsLengthInBytes);

	///Метод возвращает длину символа по его первому байту
	static integer GetCharLengthInBytes(char firstByte);

	///Метод ищет невалидные символы в UTF8 последовательности
	static void Check(const char * someChars, integer charsLengthInBytes = -1);

private:
	static const integer CharSizes[256];

	static bool IsFirstByte(char firstByte);
	///Проверяет 1 байт UTF8 последовательности
	static void CheckFirstByte(char firstByte);
	///Проверяет 2, 3 и 4 байт UTF8 последовательности
	static void CheckFollowingByte(char followingByte);
	///Проверяет содержит ли символ nonchar суррогаты
	static void CheckForNoncharSurrogates(const char * chars);

	UTF8Helper();
	~UTF8Helper();

	UTF8Helper(const UTF8Helper &);
	UTF8Helper & operator =(const UTF8Helper &);

	friend class TestUTF8HelperIsFirstByte;
};

//===========================================================================//

}//namespace

#endif // UTF8_HELPER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StringConverter.h"

#include "UTF8Helper.h"
#include "UTF16Helper.h"
#include "UTF32Helper.h"
#include "UTFLength.h"

namespace Bru
{

//===========================================================================//

//"¿"
const integer StringConverter::ReplacementChar = 0x00BF;

//===========================================================================//

utf16_string StringConverter::UTF8ToUTF16(const char * sourceUTF8Chars)
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(sourceUTF8Chars);
#endif // DEBUGGING_STRINGS
	// * 2, из-за суррогатных пар
	ushort * resultChars = new ushort[string::MaxLength * sizeof(resultChars[0]) * 2];

	integer sourceCharsIndex = 0;
	integer resultCharsIndex = 0;
	ulong intermediateUTF32Char;
	while (sourceUTF8Chars[sourceCharsIndex] != NullTerminator)
	{
		UTF8CharToUTF32Char(sourceUTF8Chars, sourceCharsIndex, &intermediateUTF32Char);
		resultCharsIndex += UTF32CharToUTF16Char(intermediateUTF32Char, resultChars, resultCharsIndex);
		sourceCharsIndex += UTF8Helper::GetCharLengthInBytes(sourceUTF8Chars[sourceCharsIndex]);
	}

	resultChars[resultCharsIndex] = NullTerminator;

	//Строка сама обрежет данные до нуль терминатора
	utf16_string resultString(resultChars);

	delete[] resultChars;

	return resultString;
}

//===========================================================================//

utf32_string StringConverter::UTF8ToUTF32(const char * sourceUTF8Chars)
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(sourceUTF8Chars);
#endif // DEBUGGING_STRINGS
	ulong * resultChars = new ulong[string::MaxLength * sizeof(ulong)];

	integer sourceCharsIndex = 0;
	integer resultCharsIndex = 0;
	while (sourceUTF8Chars[sourceCharsIndex] != NullTerminator)
	{
		resultCharsIndex += UTF8CharToUTF32Char(sourceUTF8Chars, sourceCharsIndex, &resultChars[resultCharsIndex]);
		sourceCharsIndex += UTF8Helper::GetCharLengthInBytes(sourceUTF8Chars[sourceCharsIndex]);
	}

	resultChars[resultCharsIndex] = NullTerminator;

	//Строка сама обрежет данные до нуль терминатора
	utf32_string resultString(resultChars);

	delete[] resultChars;

	return resultString;
}

//===========================================================================//

string StringConverter::UTF16ToUTF8(const ushort * sourceUTF16Chars)
{
#ifdef DEBUGGING_STRINGS
	UTF16Helper::Check(sourceUTF16Chars);
#endif // DEBUGGING_STRINGS
	//* 4 - с учетом того, что каждый символ может занимать 4 байта
	char * resultChars = new char[string::MaxLength * sizeof(char) * 4];

	integer sourceCharsIndex = 0;
	integer resultCharsIndex = 0;
	ulong intermediateUTF32Char;
	while (sourceUTF16Chars[sourceCharsIndex] != NullTerminator)
	{
		UTF16CharToUTF32Char(sourceUTF16Chars, sourceCharsIndex, &intermediateUTF32Char);
		resultCharsIndex += UTF32CharToUTF8Char(intermediateUTF32Char, resultChars, resultCharsIndex);
		sourceCharsIndex += UTF16Helper::GetCharLengthInWords(sourceUTF16Chars[sourceCharsIndex]);
	}

	resultChars[resultCharsIndex] = NullTerminator;

	//Строка сама обрежет данные до нуль терминатора
	string resulString(resultChars);

	delete[] resultChars;

	return resulString;
}

//===========================================================================//

utf32_string StringConverter::UTF16ToUTF32(const ushort * sourceUTF16Chars)
{
#ifdef DEBUGGING_STRINGS
	UTF16Helper::Check(sourceUTF16Chars);
#endif // DEBUGGING_STRINGS
	ulong * resultChars = new ulong[string::MaxLength * sizeof(ulong)];

	integer sourceCharsIndex = 0;
	integer resultCharsIndex = 0;
	while (sourceUTF16Chars[sourceCharsIndex] != NullTerminator)
	{
		resultCharsIndex += UTF16CharToUTF32Char(sourceUTF16Chars, sourceCharsIndex, &resultChars[resultCharsIndex]);
		sourceCharsIndex += UTF16Helper::GetCharLengthInWords(sourceUTF16Chars[sourceCharsIndex]);
	}

	resultChars[resultCharsIndex] = NullTerminator;

	//Строка сама обрежет данные до нуль терминатора
	utf32_string resultString(resultChars);

	delete[] resultChars;

	return resultString;
}

//===========================================================================//

string StringConverter::UTF32ToUTF8(const ulong * sourceUTF32Chars)
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(sourceUTF32Chars);
#endif // DEBUGGING_STRINGS
	//* 4 - с учетом того, что каждый символ может занимать 4 байта
	char * resultChars = new char[string::MaxLength * sizeof(char) * 4];

	integer sourceCharsIndex = 0;
	integer resultCharsIndex = 0;
	while (sourceUTF32Chars[sourceCharsIndex] != NullTerminator)
	{
		resultCharsIndex += UTF32CharToUTF8Char(sourceUTF32Chars[sourceCharsIndex], resultChars, resultCharsIndex);
		sourceCharsIndex++;
	}

	resultChars[resultCharsIndex] = NullTerminator;

	//Строка сама обрежет данные до нуль терминатора
	string resultString(resultChars);

	delete[] resultChars;

	return resultString;
}

//===========================================================================//

utf16_string StringConverter::UTF32ToUTF16(const ulong * sourceUTF32Chars)
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(sourceUTF32Chars);
#endif // DEBUGGING_STRINGS
	//*2 из-за суррогатных пар
	ushort * resultChars = new ushort[string::MaxLength * sizeof(resultChars[0]) * 2];

	integer sourceCharsIndex = 0;
	integer resultCharsIndex = 0;
	while (sourceUTF32Chars[sourceCharsIndex] != NullTerminator)
	{
		resultCharsIndex += UTF32CharToUTF16Char(sourceUTF32Chars[sourceCharsIndex], resultChars, resultCharsIndex);
		sourceCharsIndex++;
	}

	resultChars[resultCharsIndex] = NullTerminator;

	//Строка сама обрежет данные до нуль терминатора
	utf16_string resultString(resultChars);

	delete[] resultChars;

	return resultString;
}

//===========================================================================//

integer StringConverter::UTF8CharToUTF32Char(const char * sourceUTF8Chars, const integer firstByteIndex,
        ulong * destionationUTF32Char)
{
	char firstByte = sourceUTF8Chars[firstByteIndex];

	if (UTF8Helper::GetCharLengthInBytes(firstByte) == 1)
	{
		*destionationUTF32Char = static_cast<ulong>(firstByte);
	}
	else if (UTF8Helper::GetCharLengthInBytes(firstByte) == 2)
	{
		char secondByte = sourceUTF8Chars[firstByteIndex + 1];
		*destionationUTF32Char = static_cast<ulong>(((firstByte & 0x1F) << 6) | (secondByte & 0x3F));
	}
	else if (UTF8Helper::GetCharLengthInBytes(firstByte) == 3)
	{
		char secondByte = sourceUTF8Chars[firstByteIndex + 1];
		char thirdByte = sourceUTF8Chars[firstByteIndex + 2];

		*destionationUTF32Char = static_cast<ulong>(((firstByte & 0xF) << 12) | ((secondByte & 0x3F) << 6)
		                         | (thirdByte & 0x3F));
	}
	else if (UTF8Helper::GetCharLengthInBytes(firstByte) == 4)
	{
		char secondByte = sourceUTF8Chars[firstByteIndex + 1];
		char thirdByte = sourceUTF8Chars[firstByteIndex + 2];
		char fourthByte = sourceUTF8Chars[firstByteIndex + 3];

		*destionationUTF32Char = static_cast<ulong>(((firstByte & 0x7) << 18) | ((secondByte & 0x3F) << 12)
		                         | ((thirdByte & 0x3F) << 6) | (fourthByte & 0x3F));
	}
	else
	{
		*destionationUTF32Char = static_cast<ulong>(ReplacementChar);
	}
	//Всегда 1 для ulong
	return 1;
}

//===========================================================================//

integer StringConverter::UTF32CharToUTF8Char(const ulong sourceUTF16Char, char * destionationUTF8Chars,
        const integer firstByteIndex)
{
	if (UTF32Helper::FirstCodeInOneByteSequenceCode <= sourceUTF16Char
	        && sourceUTF16Char <= UTF32Helper::LastCodeInOneByteSequenceCode)
	{
		destionationUTF8Chars[firstByteIndex] = static_cast<char>(sourceUTF16Char);

		return 1;
	}
	else if (UTF32Helper::FirstCodeInTwoByteSequenceCode <= sourceUTF16Char
	         && sourceUTF16Char <= UTF32Helper::LastCodeInTwoByteSequenceCode)
	{
		destionationUTF8Chars[firstByteIndex] = static_cast<char>(0xC0 | (sourceUTF16Char >> 6));
		destionationUTF8Chars[firstByteIndex + 1] = static_cast<char>(0x80 | (sourceUTF16Char & 0x3F));

		return 2;
	}
	else if (UTF32Helper::FirstCodeInThreeByteSequenceCode <= sourceUTF16Char
	         && sourceUTF16Char <= UTF32Helper::LastCodeInThreeByteSequenceCode)
	{
		destionationUTF8Chars[firstByteIndex] = static_cast<char>(0xE0 | (sourceUTF16Char >> 12));
		destionationUTF8Chars[firstByteIndex + 1] = static_cast<char>(0x80 | ((sourceUTF16Char >> 6) & 0x3F));
		destionationUTF8Chars[firstByteIndex + 2] = static_cast<char>(0x80 | (sourceUTF16Char & 0x3F));

		return 3;
	}
	else if (UTF32Helper::FirstCodeInFourByteSequenceCode <= sourceUTF16Char
	         && sourceUTF16Char <= UTF32Helper::LastCodeInFourByteSequenceCode)
	{
		destionationUTF8Chars[firstByteIndex] = static_cast<char>(0xF0 | (sourceUTF16Char >> 18));
		destionationUTF8Chars[firstByteIndex + 1] = static_cast<char>(0x80
		        | ((sourceUTF16Char >> 12) & 0x3F));
		destionationUTF8Chars[firstByteIndex + 2] =
		    static_cast<char>(0x80 | ((sourceUTF16Char >> 6) & 0x3F));
		destionationUTF8Chars[firstByteIndex + 3] = static_cast<char>(0x80 | (sourceUTF16Char & 0x3F));

		return 4;
	}
	else
	{
		destionationUTF8Chars[firstByteIndex] = static_cast<char>(ReplacementChar);
		return 1;
	}
}

//===========================================================================//

integer StringConverter::UTF16CharToUTF32Char(const ushort * sourceUTF16Chars, const integer firstWordIndex,
        ulong * destionationUTF16Char)
{
	if (UTF16Helper::GetCharLengthInWords(sourceUTF16Chars[firstWordIndex]) == 1)
	{
		*destionationUTF16Char = static_cast<ulong>(sourceUTF16Chars[firstWordIndex]);
	}
	else if (UTF16Helper::GetCharLengthInWords(sourceUTF16Chars[firstWordIndex]) == 2)
	{
		ushort firstWord = sourceUTF16Chars[firstWordIndex];
		ushort secondWord = sourceUTF16Chars[firstWordIndex + 1];
		*destionationUTF16Char =
		    static_cast<ulong>(((firstWord - 0xD800) << 10) + (secondWord - 0xDC00) + 0x0010000);
	}
	else
	{
		*destionationUTF16Char = static_cast<ulong>(ReplacementChar);
	}

	//Всегда 1 для ulong
	return 1;
}

//===========================================================================//

integer StringConverter::UTF32CharToUTF16Char(const ulong sourceUTF32Char, ushort * destionationUTF16Chars,
        const integer firstWordIndex)
{
	if (UTF32Helper::FirstCodeInBMPSequence <= sourceUTF32Char && sourceUTF32Char <= UTF32Helper::LastCodeInBMPSequence)
	{
		destionationUTF16Chars[firstWordIndex] = static_cast<ushort>(sourceUTF32Char);
		return 1;
	}
	else if (UTF32Helper::FirstCodeInNotBMPSequence <= sourceUTF32Char
	         && sourceUTF32Char <= UTF32Helper::LastCodeInNotBMPSequence)
	{
		ulong convertingChar = sourceUTF32Char - 0x10000;
		destionationUTF16Chars[firstWordIndex] = static_cast<ushort>((convertingChar >> 10) + 0xD800);
		destionationUTF16Chars[firstWordIndex + 1] = static_cast<ushort>((convertingChar & 0x3FFUL) + 0xDC00);
		return 2;
	}
	else
	{
		destionationUTF16Chars[firstWordIndex] = static_cast<ushort>(ReplacementChar);
		return 1;
	}
}

//===========================================================================//

}//namespace

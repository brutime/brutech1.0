//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTF8Char.h"

#include "../../Helpers/Memory.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidStateException.h"

#include "UTF8Helper.h"
#include "StringConverter.h"

namespace Bru
{

//===========================================================================//

utf8_char::utf8_char() :
	_lengthInBytes(0),
	_dataPtr(new char[_lengthInBytes + 1])
{
	_dataPtr[_lengthInBytes] = NullTerminator;
}

//===========================================================================//

utf8_char::utf8_char(const char * someChar) :
	_lengthInBytes(0),
	_dataPtr(NullPtr)
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(someChar);

	UTFLength charLength = UTF8Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}
#endif // DEBUGGING_STRINGS
	Assign(someChar);
}

//===========================================================================//

utf8_char::utf8_char(const char * someChar, integer lengthInBytes) :
	_lengthInBytes(lengthInBytes),
	_dataPtr(new char[_lengthInBytes + 1])
{
	if (_lengthInBytes > 0)
	{
		Memory::Copy(_dataPtr, someChar, _lengthInBytes);
	}
	_dataPtr[_lengthInBytes] = NullTerminator;
}

//===========================================================================//

utf8_char::utf8_char(const utf8_char & other) :
	_lengthInBytes(0),
	_dataPtr(NullPtr)
{
	DeepCopy(other);
}

//===========================================================================//

utf8_char::utf8_char(utf8_char && other) :
	_lengthInBytes(other._lengthInBytes),
	_dataPtr(other._dataPtr)
{
	other._lengthInBytes = 0;
	other._dataPtr = NullPtr;
}

//===========================================================================//

utf8_char::~utf8_char()
{
	delete[] _dataPtr;
}

//===========================================================================//

utf8_char & utf8_char::operator =(const char * someChar)
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(someChar);

	UTFLength charLength = UTF8Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}
#endif // DEBUGGING_STRINGS
	Assign(someChar);
	return *this;
}

//===========================================================================//

utf8_char & utf8_char::operator =(const utf8_char & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

utf8_char & utf8_char::operator =(utf8_char && other)
{
	if (this == &other)
	{
		return *this;
	}
	
	std::swap(_lengthInBytes, other._lengthInBytes);
	std::swap(_dataPtr, other._dataPtr);

	return *this;
}

//===========================================================================//

bool utf8_char::operator ==(const char * someChar) const
{
#ifdef DEBUGGING_STRINGS
	UTF8Helper::Check(someChar);
#endif // DEBUGGING_STRINGS
	UTFLength charLength = UTF8Helper::GetLength(someChar);

#ifdef DEBUGGING_STRINGS
	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}
#endif // DEBUGGING_STRINGS
	   // -1 из-за нуль-терминатора
	if (_lengthInBytes != charLength.LengthInComponents)
	{
		return false;
	}

	return Memory::Compare(_dataPtr, someChar, _lengthInBytes) == ComapareResult::Equals;
}

//===========================================================================//

bool utf8_char::operator ==(const utf8_char & someChar) const
{
	if (_lengthInBytes != someChar._lengthInBytes)
	{
		return false;
	}

	return Memory::Compare(_dataPtr, someChar._dataPtr, _lengthInBytes) == ComapareResult::Equals;
}

//===========================================================================//

bool utf8_char::operator !=(const char * someChar) const
{
	return !(*this == someChar);
}

//===========================================================================//

bool utf8_char::operator !=(const utf8_char & someChar) const
{
	return !(*this == someChar);
}

//===========================================================================//

bool utf8_char::operator <(const char * someChar) const
{
	UTFLength charLength = UTF8Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}

	if (_lengthInBytes < charLength.LengthInComponents)
	{
		return true;
	}

	return Memory::Compare(_dataPtr, someChar, _lengthInBytes) == ComapareResult::Less;
}

//===========================================================================//

bool utf8_char::operator <(const utf8_char & someChar) const
{
	if (_lengthInBytes < someChar._lengthInBytes)
	{
		return true;
	}

	return Memory::Compare(_dataPtr, someChar._dataPtr, _lengthInBytes) == ComapareResult::Less;
}

//===========================================================================//

bool utf8_char::operator >(const char * someChar) const
{
	UTFLength charLength = UTF8Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}

	if (_lengthInBytes > charLength.LengthInComponents)
	{
		return true;
	}

	return Memory::Compare(_dataPtr, someChar, _lengthInBytes) == ComapareResult::Greater;
}

//===========================================================================//

bool utf8_char::operator >(const utf8_char & someChar) const
{
	if (_lengthInBytes > someChar._lengthInBytes)
	{
		return true;
	}

	return Memory::Compare(_dataPtr, someChar._dataPtr, _lengthInBytes) == ComapareResult::Greater;
}

//===========================================================================//

bool utf8_char::IsDigit() const
{
	//Код цифр шифруется одним байтом
	return '0' <= _dataPtr[0] && _dataPtr[0] <= '9';
}

//===========================================================================//

utf32_char utf8_char::ToUTF32() const
{
	return StringConverter::UTF8ToUTF32(_dataPtr)[0];
}

//===========================================================================//

void utf8_char::Assign(const char * someChar)
{
	UTFLength charLength = UTF8Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}

	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_lengthInBytes = charLength.LengthInComponents;
	_dataPtr = new char[_lengthInBytes + 1];
	if (charLength.LengthInComponents > 0)
	{
		Memory::Copy(_dataPtr, someChar, charLength.LengthInComponents);
	}
	_dataPtr[_lengthInBytes] = NullTerminator;
}

//===========================================================================//

void utf8_char::DeepCopy(const utf8_char & other)
{
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_lengthInBytes = other._lengthInBytes;
	_dataPtr = new char[_lengthInBytes + 1];
	Memory::Copy(_dataPtr, other._dataPtr, _lengthInBytes + 1); //+ 1 чтобы скопировать NullTerminator
}

//===========================================================================//

} //namespace

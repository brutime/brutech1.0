//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс занимается конвертацией строк из одного типа юникода в другой
//

#ifndef STRING_CONVERTER_H
#define STRING_CONVERTER_H

#include "String.h"
#include "UTF8Char.h"

namespace Bru
{

//===========================================================================//

class StringConverter
{
public:
	static const integer ReplacementChar;

	static utf16_string UTF8ToUTF16(const char * sourceUTF8Chars);
	static utf32_string UTF8ToUTF32(const char * sourceUTF8Chars);

	static string UTF16ToUTF8(const ushort * sourceUTF16chars);
	static utf32_string UTF16ToUTF32(const ushort * sourceUTF16Chars);

	static string UTF32ToUTF8(const ulong * sourceUTF32chars);
	static utf16_string UTF32ToUTF16(const ulong * sourceUTF32Chars);

private:
	static integer UTF8CharToUTF32Char(const char * sourceUTF8Chars, const integer firstByteIndex,
	                                   ulong * destionationUTF32Char);
	static integer UTF32CharToUTF8Char(const ulong sourceUTF32Char, char * destionationUTF8Chars,
	                                   const integer firstByteIndex);

	static integer UTF16CharToUTF32Char(const ushort * sourceUTF16Chars, const integer firstWordIndex,
	                                    ulong * destionationUTF32Char);
	static integer UTF32CharToUTF16Char(const ulong sourceUTF32Char, ushort * destionationUTF16Chars,
	                                    const integer firstWordIndex);

	StringConverter() = delete;
	StringConverter(const StringConverter &) = delete;
	~StringConverter() = delete;
	StringConverter & operator =(const StringConverter &) = delete;
};

//===========================================================================//

}//namespace

#endif // STRING_CONVERTER_H

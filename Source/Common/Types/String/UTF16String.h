//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс предназначен для хранения UTF16 данных
//

#pragma once
#ifndef UTF16_STRING_H
#define UTF16_STRING_H

#include "UTF16Char.h"

namespace Bru
{

//===========================================================================//

class utf16_string
{
public:
	static const utf16_string Empty;

	utf16_string(const ushort * chars);
	utf16_string(const utf16_string & otherString);
	~utf16_string();

	utf16_string & operator =(const utf16_string & otherString);

	const ushort * ToChars() const
	{
		return _dataPtr;
	}
	operator const ushort * () const
	{
		return ToChars();
	}

	integer GetLength() const
	{
		return _length;
	}
	integer GetLengthInWords() const
	{
		return _lengthInWords;
	}

private:
	integer _length;
	integer _lengthInWords;
	ushort * _dataPtr;

	void DeepCopy(const utf16_string & otherString);

	friend class TestUTF16StringGetters;
	friend class TestUTF16StringClearParameters;
	friend class TestUTF16StringDeepCopyEmpty;
	friend class TestUTF16StringDeepCopyToEmpty;
	friend class TestUTF16StringDeepCopy;
};

//===========================================================================//

}//namespace

#endif // UTF16_STRING_H

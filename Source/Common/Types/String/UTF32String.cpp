//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTF32String.h"

#include "UTF32Helper.h"
#include "UTFLength.h"
#include "../../Helpers/Memory.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/IndexOutOfBoundsException.h"
#include "../../Exceptions/OverflowException.h"

namespace Bru
{

//===========================================================================//

const utf32_string utf32_string::Empty = NullTerminator;

//===========================================================================//

utf32_string::utf32_string(const ulong * chars) :
	_length(0),
	_dataPtr(NullPtr)
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(chars);
#endif // DEBUGGING_STRINGS
	UTFLength charsLength = UTF32Helper::GetLength(chars);
	CheckForOverflow(charsLength.LengthInChars);

	_length = charsLength.LengthInComponents;
	_dataPtr = new ulong[_length + 1];
	if (charsLength.LengthInComponents > 0)
	{
		Memory::Copy(_dataPtr, chars, charsLength.LengthInComponents * sizeof(_dataPtr[0]));
	}
	_dataPtr[_length] = NullTerminator;
}

//===========================================================================//

utf32_string::utf32_string(const utf32_string & otherString) :
	_length(0),
	_dataPtr(NullPtr)
{
	DeepCopy(otherString);
}

//===========================================================================//

utf32_string & utf32_string::operator =(const utf32_string & otherString)
{
	if (this != &otherString)
	{
		DeepCopy(otherString);
	}

	return *this;
}

//===========================================================================//

utf32_string::~utf32_string()
{
	delete[] _dataPtr;
}

//===========================================================================//

ulong utf32_string::operator [](integer index) const
{
#ifdef DEBUGGING
	if (index < 0 || index >= _length)
	{
		INDEX_OUT_OF_BOUNDS_EXCEPTION(string::Format("index out of bounds: {0} ([0..{1}])", index, _length - 1));
	}
#endif // DEBUGGING
	return _dataPtr[index];
}

//===========================================================================//

void utf32_string::DeepCopy(const utf32_string & otherString)
{
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_length = otherString._length;
	_dataPtr = new ulong[_length + 1];
	//+ 1 чтобы скопировать NullTerminator
	Memory::Copy(_dataPtr, otherString._dataPtr, (_length + 1) * sizeof(_dataPtr[0]));
}

//===========================================================================//

void utf32_string::CheckForOverflow(integer length)
{
	if (length > string::MaxLength)
	{
		OVERFLOW_EXCEPTION(string::Format("Target length {0} is more than max string size", length));
	}
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Конвертер регистра символов
//

#pragma once
#ifndef CASE_CONVERTATION_TABLE_H
#define CASE_CONVERTATION_TABLE_H

#include "UTF8Char.h"

namespace Bru
{

//===========================================================================//

class CaseConvertationTable
{
public:
	static utf8_char GetLowerCase(const utf8_char & someChar);
	static utf8_char GetUpperCase(const utf8_char & someChar);

private:
	CaseConvertationTable() = delete;
	CaseConvertationTable(const CaseConvertationTable &) = delete;
	~CaseConvertationTable() = delete;
	CaseConvertationTable & operator =(const CaseConvertationTable &) = delete;
};

//===========================================================================//

}//namespace

#endif // CASE_CONVERTATION_TABLE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTF32Char.h"

#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidStateException.h"

#include "UTF32Helper.h"

namespace Bru
{

//===========================================================================//

utf32_char::utf32_char() :
	_char(NullTerminator)
{
}

//===========================================================================//

utf32_char::utf32_char(ulong someChar) :
	_char(NullTerminator)
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(someChar);
#endif // DEBUGGING_STRINGS
	_char = someChar;
}

//===========================================================================//

utf32_char::utf32_char(const utf32_char & other) :
	_char(other._char)
{
}

//===========================================================================//

utf32_char & utf32_char::operator =(const utf32_char & other)
{
	if (this != &other)
	{
		_char = other._char;
	}

	return *this;
}

//===========================================================================//

utf32_char::~utf32_char()
{
}

//===========================================================================//

utf32_char & utf32_char::operator =(ulong someChar)
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(someChar);
#endif // DEBUGGING_STRINGS
	_char = someChar;
	return *this;
}

//===========================================================================//

bool utf32_char::operator ==(ulong someChar) const
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(someChar);
#endif // DEBUGGING_STRINGS
	return _char == someChar;
}

//===========================================================================//

bool utf32_char::operator ==(const utf32_char & someChar) const
{
	return _char == someChar._char;
}

//===========================================================================//

bool utf32_char::operator !=(ulong someChar) const
{
	return !(*this == someChar);
}

//===========================================================================//

bool utf32_char::operator !=(const utf32_char & someChar) const
{
	return !(*this == someChar);
}

//===========================================================================//

bool utf32_char::operator <(ulong someChar) const
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(someChar);
#endif // DEBUGGING_STRINGS
	return _char < someChar;
}

//===========================================================================//

bool utf32_char::operator <(const utf32_char & someChar) const
{
	return _char < someChar._char;
}

//===========================================================================//

bool utf32_char::operator >(ulong someChar) const
{
#ifdef DEBUGGING_STRINGS
	UTF32Helper::Check(someChar);
#endif // DEBUGGING_STRINGS
	return _char > someChar;
}

//===========================================================================//

bool utf32_char::operator >(const utf32_char & someChar) const
{
	return _char > someChar._char;
}

//===========================================================================//

}//namespace

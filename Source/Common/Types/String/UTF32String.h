//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс предназначен для хранения UTF-32 данных
//

#pragma once
#ifndef UTF32_STRING_H
#define UTF32_STRING_H

#include "UTF32Char.h"

namespace Bru
{

//===========================================================================//

class utf32_string
{
public:
	static const utf32_string Empty;

	utf32_string(const ulong * chars);
	utf32_string(const utf32_string & otherString);
	~utf32_string();

	utf32_string & operator =(const utf32_string & otherString);

	ulong operator [](integer index) const;

	const ulong * ToChars() const
	{
		return _dataPtr;
	}
	operator const ulong * () const
	{
		return ToChars();
	}

	integer GetLength() const
	{
		return _length;
	}
	integer GetLengthInChars() const
	{
		return _length;
	}

private:
	integer _length;
	ulong * _dataPtr;

	void DeepCopy(const utf32_string & otherString);

	static void CheckForOverflow(integer length);

	friend class TestUTF32StringGetters;
	friend class TestUTF32StringClearParameters;
	friend class TestUTF32StringDeepCopyEmpty;
	friend class TestUTF32StringDeepCopyToEmpty;
	friend class TestUTF32StringDeepCopy;
};

//===========================================================================//

}//namespace

#endif // UTF32_STRING_H

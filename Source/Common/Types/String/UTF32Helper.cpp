//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTF32Helper.h"

#include "String.h"
#include "../../Exceptions/OverflowException.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidUTF32SequenceException.h"

namespace Bru
{

//===========================================================================//

//Если изменится значение константы FirstCodeInUTF32Sequence необходимо добавить полную
//проверку попадания ulong в интервалы. Сейчас в этой проверке нет необходимости,
//так как тип ulong - unsigned, а данная константа равна границам интервала.
//P.S. Маловероятно, что данные константы изменяться - это произойдет в случае изменении
//кодировки UTF32

const ulong UTF32Helper::FirstCodeInFirstNoncharRange = 0x00FFFE;
const ulong UTF32Helper::LastCodeInFirstNoncharRange = 0x00FFFF;

const ulong UTF32Helper::FirstCodeInSecondNoncharRange = 0x00FDD0;
const ulong UTF32Helper::LastCodeInSecondNoncharRange = 0x00FDEF;

const ulong UTF32Helper::FirstCodeInUTF32Sequence = NullTerminator;
const ulong UTF32Helper::LastCodeInUTF32Sequence = 0x10FFFF;

const ulong UTF32Helper::FirstCodeInOneByteSequenceCode = NullTerminator;
const ulong UTF32Helper::LastCodeInOneByteSequenceCode = 0x00007F;

const ulong UTF32Helper::FirstCodeInTwoByteSequenceCode = 0x000080;
const ulong UTF32Helper::LastCodeInTwoByteSequenceCode = 0x0007FF;

const ulong UTF32Helper::FirstCodeInThreeByteSequenceCode = 0x000800;
const ulong UTF32Helper::LastCodeInThreeByteSequenceCode = 0x00FFFF;

const ulong UTF32Helper::FirstCodeInFourByteSequenceCode = 0x010000;
const ulong UTF32Helper::LastCodeInFourByteSequenceCode = 0x10FFFF;

const ulong UTF32Helper::FirstCodeInBMPSequence = NullTerminator;
const ulong UTF32Helper::LastCodeInBMPSequence = 0x00FFFF;

const ulong UTF32Helper::FirstCodeInNotBMPSequence = 0x010000;
const ulong UTF32Helper::LastCodeInNotBMPSequence = 0x10FFFF;

//===========================================================================//

const integer NoncharSurrogatesCharsCount = 32;

const ulong NoncharSurrogatesChars[NoncharSurrogatesCharsCount] = { 0x01FFFE, 0x01FFFF, 0x02FFFE, 0x02FFFF, 0x03FFFE,
        0x03FFFF, 0x04FFFE, 0x04FFFF, 0x05FFFE, 0x05FFFF, 0x06FFFE, 0x06FFFF, 0x07FFFE, 0x07FFFF, 0x08FFFE, 0x08FFFF,
        0x09FFFE, 0x09FFFF, 0x0AFFFE, 0x0AFFFF, 0x0BFFFE, 0x0BFFFF, 0x0CFFFE, 0x0CFFFF, 0x0DFFFE, 0x0DFFFF, 0x0EFFFE,
        0x0EFFFF, 0x0FFFFE, 0x0FFFFF, 0x10FFFE, 0x10FFFF,
                                                                  };

//===========================================================================//

UTFLength UTF32Helper::GetLength(const ulong * chars)
{
	if (chars == NullTerminator)
	{
		return UTFLength(0, 0);
	}

	integer length = 0;

	const ulong * charsCursor = chars;

	while (*charsCursor != NullTerminator)
	{
		PREFETCH(charsCursor);

		length++;
		charsCursor++;
	}

	return UTFLength(length, length);
}

//===========================================================================//

void UTF32Helper::Check(ulong someChar)
{
	if (someChar > LastCodeInUTF32Sequence)
	{
		INVALID_UTF32_SEQUENCE_EXCEPTION("Char out of unicode range");
	}

	if ((FirstCodeInFirstNoncharRange <= someChar && someChar <= LastCodeInFirstNoncharRange)
	        || (FirstCodeInSecondNoncharRange <= someChar && someChar <= LastCodeInSecondNoncharRange))
	{
		INVALID_UTF32_SEQUENCE_EXCEPTION("Char represents nonchar");
	}

	for (integer i = 0; i < NoncharSurrogatesCharsCount; i++)
	{
		if (someChar == NoncharSurrogatesChars[i])
		{
			INVALID_UTF32_SEQUENCE_EXCEPTION("Char represents nonchar surrogates");
		}
	}
}

//===========================================================================//

void UTF32Helper::Check(const ulong * chars)
{
	if (chars == NullTerminator)
	{
		return;
	}

	const ulong * charsCursor = chars;

	while (*charsCursor != NullTerminator)
	{
		Check(*charsCursor);
		charsCursor++;
	}
}

//===========================================================================//

}//namespace

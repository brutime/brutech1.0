//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс UTF8 символа
//

#pragma once
#ifndef UTF16_CHAR_H
#define UTF16_CHAR_H

#include "../BaseTypes.h"
#include "UTFLength.h"

namespace Bru
{

//===========================================================================//

class utf16_char
{
public:
	utf16_char();
	utf16_char(const ushort * someChar);
	utf16_char(const utf16_char & other);
	~utf16_char();

	utf16_char & operator =(const ushort * someChar);
	utf16_char & operator =(const utf16_char & other);

	bool operator ==(const ushort * someChar) const;
	bool operator ==(const utf16_char & someChar) const;

	bool operator !=(const ushort * someChar) const;
	bool operator !=(const utf16_char & someChar) const;

	integer GetLength() const
	{
		return _lengthInWords > 0 ? 1 : 0;
	}
	integer GetLengthInWords() const
	{
		return _lengthInWords;
	}

	operator const ushort * () const
	{
		return _dataPtr;
	}

private:
	void Assign(const ushort * someChar);
	void DeepCopy(const utf16_char & other);

	integer _lengthInWords;
	ushort * _dataPtr;

	//для тестирования
	friend class TestUTF16CharDefaultConstructor;
	friend class TestUTF16CharConstructorFromOneWordChar;
	friend class TestUTF16CharConstructorFromTwoWordsChar;
	friend class TestUTF16CharConstructorFromOtherUTF16Char;
	friend class TestUTF16CharAssingOneWordChar;
	friend class TestUTF16CharAssingTwoWordsChar;
	friend class TestUTF16CharOperatorOtherUTF16Char;
};

//===========================================================================//

}//namespace

#endif // UTF16_CHAR_H

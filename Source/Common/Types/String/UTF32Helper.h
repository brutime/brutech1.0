//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Вспомогательный класс для работы с UTF32 последовательностью символов
//

#pragma once
#ifndef UTF32_HELPER_H
#define UTF32_HELPER_H

#include "../BaseTypes.h"
#include "UTFLength.h"
#include "UTF8Char.h"

namespace Bru
{

//===========================================================================//

class UTF32Helper
{
public:
	static const ulong FirstCodeInFirstNoncharRange;
	static const ulong LastCodeInFirstNoncharRange;

	static const ulong FirstCodeInSecondNoncharRange;
	static const ulong LastCodeInSecondNoncharRange;

	static const ulong FirstCodeInUTF32Sequence;
	static const ulong LastCodeInUTF32Sequence;

	static const ulong FirstCodeInOneByteSequenceCode;
	static const ulong LastCodeInOneByteSequenceCode;

	static const ulong FirstCodeInTwoByteSequenceCode;
	static const ulong LastCodeInTwoByteSequenceCode;

	static const ulong FirstCodeInThreeByteSequenceCode;
	static const ulong LastCodeInThreeByteSequenceCode;

	static const ulong FirstCodeInFourByteSequenceCode;
	static const ulong LastCodeInFourByteSequenceCode;

	//BMP - Basic Multilingual Plane
	static const ulong FirstCodeInBMPSequence;
	static const ulong LastCodeInBMPSequence;

	static const ulong FirstCodeInNotBMPSequence;
	static const ulong LastCodeInNotBMPSequence;

	static UTFLength GetLength(const ulong * chars);

	///Метод проверяем символ на валидность
	static void Check(ulong someChar);

	///Метод ищет невалидные символы в UTF32 последовательности
	static void Check(const ulong * chars);

private:
	UTF32Helper() = delete;
	UTF32Helper(const UTF32Helper &) = delete;
	~UTF32Helper() = delete;
	UTF32Helper & operator =(const UTF32Helper &) = delete;
};

//===========================================================================//

}//namespace

#endif // UTF32_HELPER_H

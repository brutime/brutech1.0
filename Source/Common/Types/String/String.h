//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс строки
//

#pragma once
#ifndef STRING_H
#define STRING_H

#include "../BaseTypes.h"
#include "UTF8Char.h"
#include "UTF16String.h"
#include "UTF32String.h"

namespace Bru
{

//===========================================================================//

class StringBuilder;

//===========================================================================//

class string
{
	friend class StringBuilder;

public:
	static const string Empty;
	static const integer MaxLength; //без учера нуль-терминатора

	string();
	string(bool someBool);
	string(char someChar);
	string(byte someByte);
	string(ushort someUnsignedShort);
	string(uint someUnsegnedInteger);
	string(ulong someUnsegnedLong);
	string(integer someInteger);
	string(int64 someInteger64);
	string(float someFloat);
	string(double someDouble);
	string(const utf8_char & someChar);
	string(const utf32_char & someChar);
	string(const char * chars);
	string(const char * chars, integer lengthInBytes);
	string(const char * chars, integer length, integer lengthInBytes);
	string(const utf16_string & someString);
	string(const utf32_string & someString);
	string(const string & other);
	string(string && other);

	~string();

	string & operator =(const utf8_char & someChar);
	string & operator =(const char * chars);
	string & operator =(const string & other);
	string & operator =(string && other);

	string & operator +=(const utf8_char & someChar);
	string & operator +=(const char * chars);
	string & operator +=(const string & someString);

	string operator +(const utf8_char & someChar) const;
	string operator +(const char * chars) const;
	string operator +(const string & someString) const;

	bool operator ==(const utf8_char & someChar) const;
	bool operator ==(const char * chars) const;
	bool operator ==(const string & someString) const;

	bool operator !=(const utf8_char & someChar) const;
	bool operator !=(const char * chars) const;
	bool operator !=(const string & someString) const;

	bool operator <(const utf8_char & someChar) const;
	bool operator <(const char * chars) const;
	bool operator <(const string & other) const;

	bool operator >(const utf8_char & someChar) const;
	bool operator >(const char * chars) const;
	bool operator >(const string & other) const;

	utf8_char operator [](integer index) const;
	utf8_char GetChar(integer index) const;

	operator const char * () const;
	const char * ToChars() const;

	utf16_string ToUTF16() const;
	utf32_string ToUTF32() const;

	template <typename ... Args>
	static string Format(const string & formatString, Args ...);

	bool IsEmpty() const;
	bool IsEmptyOrBlankCharsOnly() const;
	bool IsBlankCharAt(integer index) const;
	bool IsNumber() const;

	string Insert(integer index, const string & someString) const;
	string Remove(integer index, integer count = 1) const;

	string Substring(integer index) const;
	string Substring(integer index, integer count) const;

	string Replace(const string & oldString, const string & newString) const;

	string Reverse() const;

	string TrimLeft() const;
	string TrimRight() const;
	string Trim() const;

	///Удаление blank символов и повторяющихся пробелов по всей строке
	string CleanUp() const;

	string ToLower() const;
	string ToUpper() const;

	bool Contains(const string & searchingString) const;
	integer ContainsCount(const string & searchingString) const;

	integer FirstIndexOf(const string & searchingString, integer startIndex = 0) const;
	integer FirstIndexNumberOf(const string & searchingString, integer ordinalNumber) const;

	integer LastIndexOf(const string & searchingString) const;
	integer LastIndexOf(const string & searchingString, integer startIndex) const;
	integer LastIndexNumberOf(const string & searchingString, integer ordinalNumber) const;

	integer FirstIndexOfNotBlankChar(integer startIndex = 0) const;

	integer GetLength() const;
	integer GetLengthInBytes() const;

private:
	static const string BlankChars;
	static const utf8_char Space;
	static const integer FormatArgumentLength;

	void Assign(const char * chars);

	static string CreateFormattedString(const string & formatString, const string * arguments[],
	                                    integer argumentsCount);

	void ResizeData(integer newLengthInBytes);

	void ResetCashedIndices() const;

	void DeepCopy(const string & other);

	static void CheckForOverflow(integer length, integer lengthInBytes);

	integer _length; //без учета NullTerminator
	integer _lengthInBytes; //без учета NullTerminator
	char * _dataPtr;

	mutable integer _cashedIndex;
	mutable integer _cashedByteIndex;

	friend class TestStringDefaultConstructor;
	friend class TestStringEmptyConstructor;
	friend class TestStringBoolConstructor;
	friend class TestStringCharConstructor;
	friend class TestStringByteConstructor;
	friend class TestStringUnsignedShortConstructor;
	friend class TestStringUnsegnedIntegerConstructor;
	friend class TestStringUnsegnedLongConstructor;
	friend class TestStringIntegerConstructor;
	friend class TestStringInteger64Constructor;
	friend class TestStringFloatConstructor;
	friend class TestStringUTF8CharConstructor;
	friend class TestStringUTF32CharConstructor;
	friend class TestStringCharsConstructor;
	friend class TestStringCharsAndLengthInBytesConstructor;
	friend class TestStringCharsAndLengthsConstructor;
	friend class TestStringUTF16StringConstructor;
	friend class TestStringUTF32StringConstructor;
	friend class TestStringCopyConstructor;
	friend class TestStringMaxLengthConstructor;
	friend class TestStringConstructorExceptions;
	friend class TestStringMoveConstructor;
	friend class TestStringCopyOperator;
	friend class TestStringEmptyCopyOperator;
	friend class TestStringNullTerminatorCopyOperator;
	friend class TestStringCharCopyOperator;
	friend class TestStringCharsCopyOperator;
	friend class TestStringMaxLengthCopyOperator;
	friend class TestStringCopyOperatorSelf;
	friend class TestStringMoveOperator;
	friend class TestStringAppendToEmpty;
	friend class TestStringAppendEmpty;
	friend class TestStringAppendNullTerminator;
	friend class TestStringAppendChar;
	friend class TestStringAppendChars;
	friend class TestStringAppendCharsExceptions;
	friend class TestStringAppendOtherString;
	friend class TestStringAppendMaxLength;
	friend class TestStringAppendExceptions;
	friend class TestStringPlusToEmpty;
	friend class TestStringPlusEmpty;
	friend class TestStringPlusChar;
	friend class TestStringPlusCharExceptions;
	friend class TestStringPlusChars;
	friend class TestStringPlusOtherString;
	friend class TestStringPlusMaxLength;
	friend class TestStringOperatorGetChar;
	friend class TestStringGetChar;
	friend class TestStringInsertEmpty;
	friend class TestStringInsertInEmpty;
	friend class TestStringInsertInBegin;
	friend class TestStringInsertIntervalInBegin;
	friend class TestStringInsertInMiddle;
	friend class TestStringInsertIntervalInMiddle;
	friend class TestStringInsertInEnd;
	friend class TestStringInsertInIntervalEnd;
	friend class TestStringInsertMaxLength;
	friend class TestStringInsertExceptions;
	friend class TestStringRemove;
	friend class TestStringSubstringBegin;
	friend class TestStringSubstringMiddle;
	friend class TestStringSubstringEnd;
	friend class TestStringSubstringOneChar;
	friend class TestStringEmptyReplace;
	friend class TestStringReplaceBegin;
	friend class TestStringReplaceMiddle;
	friend class TestStringReplaceEnd;
	friend class TestStringReplaceSingleCharToDoubleChats;
	friend class TestStringReplaceSameLength;
	friend class TestStringReplaceAll;
	friend class TestStringReplaceMaxLength;
};

//===========================================================================//

extern bool operator ==(const char * someChars, string someString);

//===========================================================================//

template <typename ... Args>
string string::Format(const string & formatString, Args ... args)
{
	const integer argCount = sizeof...(args);
	string arguments[] = {args...};
	const string * addresses[argCount];
	for (integer i = 0; i < argCount; i++)
	{
		addresses[i] = &arguments[i];
	}
	return CreateFormattedString(formatString, addresses, argCount);
}

//===========================================================================//

} // namespace Bru

#endif // STRING_H

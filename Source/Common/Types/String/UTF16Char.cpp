//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "UTF16Char.h"

#include "../../Helpers/Memory.h"
#include "../../Exceptions/InvalidArgumentException.h"
#include "../../Exceptions/InvalidStateException.h"
#include "UTF16Helper.h"

namespace Bru
{

//===========================================================================//

utf16_char::utf16_char() :
	_lengthInWords(0),
	_dataPtr(new ushort[_lengthInWords + 1])
{
	_dataPtr[_lengthInWords] = NullTerminator;
}

//===========================================================================//

utf16_char::utf16_char(const ushort * someChar) :
	_lengthInWords(0),
	_dataPtr(NullPtr)
{
#ifdef DEBUGGING_STRINGS
	UTF16Helper::Check(someChar);

	UTFLength charLength = UTF16Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}
#endif // DEBUGGING_STRINGS
	Assign(someChar);
}

//===========================================================================//

utf16_char::utf16_char(const utf16_char & other) :
	_lengthInWords(0),
	_dataPtr(NullPtr)
{
	DeepCopy(other);
}

//===========================================================================//

utf16_char::~utf16_char()
{
	delete[] _dataPtr;
}

//===========================================================================//

utf16_char & utf16_char::operator =(const ushort * someChar)
{
#ifdef DEBUGGING_STRINGS
	UTF16Helper::Check(someChar);

	UTFLength charLength = UTF16Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}
#endif // DEBUGGING_STRINGS
	Assign(someChar);
	return *this;
}

//===========================================================================//

utf16_char & utf16_char::operator =(const utf16_char & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

bool utf16_char::operator ==(const ushort * someChar) const
{
#ifdef DEBUGGING_STRINGS
	UTF16Helper::Check(someChar);
#endif // DEBUGGING_STRINGS
	UTFLength charLength = UTF16Helper::GetLength(someChar);

#ifdef DEBUGGING_STRINGS
	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}
#endif // DEBUGGING_STRINGS
	// -1 из-за нуль-терминатора
	if (_lengthInWords != charLength.LengthInComponents)
	{
		return false;
	}

	return Memory::Compare(_dataPtr, someChar, _lengthInWords * sizeof(_dataPtr[0])) == ComapareResult::Equals;
}

//===========================================================================//

bool utf16_char::operator ==(const utf16_char & someChar) const
{
	if (_lengthInWords != someChar._lengthInWords)
	{
		return false;
	}

	return Memory::Compare(_dataPtr, someChar._dataPtr, _lengthInWords * sizeof(_dataPtr[0])) == ComapareResult::Equals;
}

//===========================================================================//

bool utf16_char::operator !=(const ushort * someChar) const
{
	return !(*this == someChar);
}

//===========================================================================//

bool utf16_char::operator !=(const utf16_char & someChar) const
{
	return !(*this == someChar);
}

//===========================================================================//

void utf16_char::Assign(const ushort * someChar)
{
	UTFLength charLength = UTF16Helper::GetLength(someChar);

	if (charLength.LengthInChars > 1)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Char length {0} > 1", charLength.LengthInChars));
	}

	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_lengthInWords = charLength.LengthInComponents;
	_dataPtr = new ushort[_lengthInWords + 1];
	if (charLength.LengthInComponents > 0)
	{
		Memory::Copy(_dataPtr, someChar, charLength.LengthInComponents * sizeof(_dataPtr[0]));
	}
	_dataPtr[_lengthInWords] = NullTerminator;
}

//===========================================================================//

void utf16_char::DeepCopy(const utf16_char & other)
{
	if (_dataPtr != NullPtr)
	{
		delete[] _dataPtr;
	}

	_lengthInWords = other._lengthInWords;
	_dataPtr = new ushort[_lengthInWords + 1];
	Memory::Copy(_dataPtr, other._dataPtr, (_lengthInWords + 1) * sizeof(ushort)); //+ 1 чтобы скопировать NullTerminator
}

//===========================================================================//

}//namespace

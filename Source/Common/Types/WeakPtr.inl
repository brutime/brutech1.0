//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "WeakPtr.h"

namespace Bru
{

//===========================================================================//

template<typename T>
WeakPtr<T>::WeakPtr() :
	_objectPtr(NullPtr),
	_refsCounter(NullPtr)
{
}

//===========================================================================//

template<typename T>
WeakPtr<T>::WeakPtr(NullPtrType) :
	_objectPtr(NullPtr),
	_refsCounter(NullPtr)
{
}

//===========================================================================//

template<typename T>
WeakPtr<T>::WeakPtr(const WeakPtr<T> & other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementWeakRefsCount();
	}
}

//===========================================================================//

template<typename T> template<typename U>
WeakPtr<T>::WeakPtr(const WeakPtr<U> & other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementWeakRefsCount();
	}
}

//===========================================================================//

template<typename T> template<typename U>
WeakPtr<T>::WeakPtr(T * objectPtr, const WeakPtr<U> & other) : 
	_objectPtr(objectPtr),
	_refsCounter(other._refsCounter)
{
	if (_objectPtr != other._objectPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("Different object pointers");
	}		
	
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementWeakRefsCount();
	}
}

//===========================================================================//

template<typename T> template<typename U>
WeakPtr<T>::WeakPtr(const SharedPtr<U> & sharedPtr) :
	_objectPtr(sharedPtr._objectPtr),
	_refsCounter(sharedPtr._refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementWeakRefsCount();
	}
}

//===========================================================================//

template<typename T>
WeakPtr<T>::WeakPtr(WeakPtr<T> && other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	other._objectPtr = NullPtr;
	other._refsCounter = NullPtr;
}

//===========================================================================//

template<typename T> template<typename U>
WeakPtr<T>::WeakPtr(WeakPtr<U> && other) :
	_objectPtr(other._objectPtr),
	_refsCounter(other._refsCounter)
{
	other._objectPtr = NullPtr;
	other._refsCounter = NullPtr;
}

//===========================================================================//

template<typename T>
WeakPtr<T>::~WeakPtr()
{
	Release();
}

//===========================================================================//

template<typename T>
WeakPtr<T> & WeakPtr<T>::operator =(NullPtrType)
{
	Release();

	return *this;
}

//===========================================================================//

template<typename T>
WeakPtr<T> & WeakPtr<T>::operator =(const WeakPtr<T> & other)
{
	if (this == &other)
	{
		return *this;
	}

	Redirect(other._objectPtr, other._refsCounter);

	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementWeakRefsCount();
	}

	return *this;
}

//===========================================================================//

template<typename T> template<typename U>
WeakPtr<T> & WeakPtr<T>::operator =(const WeakPtr<U> & other)
{
	Redirect(other._objectPtr, other._refsCounter);

	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementWeakRefsCount();
	}

	return *this;
}

//===========================================================================//

template<typename T> template<typename U>
WeakPtr<T> & WeakPtr<T>::operator =(const SharedPtr<U> & sharedPtr)
{
	Redirect(sharedPtr._objectPtr, sharedPtr._refsCounter);

	if (_refsCounter != NullPtr)
	{
		_refsCounter->IncrementWeakRefsCount();
	}

	return *this;
}

//===========================================================================//

template<typename T>
WeakPtr<T> & WeakPtr<T>::operator =(WeakPtr<T> && other)
{
	std::swap(_objectPtr, other._objectPtr);
	std::swap(_refsCounter, other._refsCounter);

	return *this;
}

//===========================================================================//

template<typename T> template<typename U>
WeakPtr<T> & WeakPtr<T>::operator =(WeakPtr<U> && other)
{
	WeakPtr<T> weakPtr(std::move(other));

	std::swap(_objectPtr, weakPtr._objectPtr);
	std::swap(_refsCounter, weakPtr._refsCounter);

	return *this;
}

//===========================================================================//

template<typename T>
T & WeakPtr<T>::operator *() const
{
	if (IsNull())
	{
		NULL_POINTER_EXCEPTION("Attempt to dereference null pointer");
	}

	return *_objectPtr;
}

//===========================================================================//

template<typename T>
T * WeakPtr<T>::operator ->() const
{
	if (IsNull())
	{
		NULL_POINTER_EXCEPTION("Attempt to dereference null pointer");
	}

	return _objectPtr;
}

//===========================================================================//

template<typename T>
WeakPtr<T>::operator T * () const
{
	return _objectPtr;
}

//===========================================================================//

template<typename T>
bool WeakPtr<T>::operator ==(NullPtrType) const
{
	return IsNull();
}

//===========================================================================//

template<typename T> template<typename U>
bool WeakPtr<T>::operator ==(U * objectPtr) const
{
	return _objectPtr == objectPtr;
}

//===========================================================================//

template<typename T>
bool WeakPtr<T>::operator ==(const WeakPtr<T> & other) const
{
	return _objectPtr == other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool WeakPtr<T>::operator ==(const WeakPtr<U> & other) const
{
	return _objectPtr == other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool WeakPtr<T>::operator ==(const SharedPtr<U> & sharedPtr) const
{
	return _objectPtr == sharedPtr._objectPtr;
}

//===========================================================================//

template<typename T>
bool WeakPtr<T>::operator !=(NullPtrType) const
{
	return !IsNull();
}

//===========================================================================//

template<typename T> template<typename U>
bool WeakPtr<T>::operator !=(U * objectPtr) const
{
	return _objectPtr != objectPtr;
}

//===========================================================================//

template<typename T>
bool WeakPtr<T>::operator !=(const WeakPtr<T> & other) const
{
	return _objectPtr != other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool WeakPtr<T>::operator !=(const WeakPtr<U> & other) const
{
	return _objectPtr != other._objectPtr;
}

//===========================================================================//

template<typename T> template<typename U>
bool WeakPtr<T>::operator !=(const SharedPtr<U> & sharedPtr) const
{
	return _objectPtr != sharedPtr._objectPtr;
}

//===========================================================================//

template<typename T>
SharedPtr<T> WeakPtr<T>::Lock() const
{
	return SharedPtr<T>(*this);
}

//===========================================================================//

template<typename T>
T * WeakPtr<T>::Get() const
{
	return _objectPtr;
}

//===========================================================================//

template<typename T>
void WeakPtr<T>::Redirect(T * objectPtr, RefsCounter * refsCounter)
{
	if (_refsCounter != NullPtr)
	{
		_refsCounter->DecrementWeakRefsCount();

		if (_refsCounter->IsNoRefs())
		{
			delete _refsCounter;
		}
	}

	_objectPtr = objectPtr;
	_refsCounter = refsCounter;
}

//===========================================================================//

template<class T>
void WeakPtr<T>::Release()
{
	Redirect(NullPtr, NullPtr);
}

//===========================================================================//

template<class T>
bool WeakPtr<T>::IsNull() const
{
	return _refsCounter == NullPtr || _refsCounter->GetSharedRefsCount() == 0;
}

//===========================================================================//

} // namespace Bru

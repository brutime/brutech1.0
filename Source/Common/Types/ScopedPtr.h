//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Шаблон умного указателя который при выходе из области видимости удаляет объект
//              Сейчас ScopedPtr может работать с NullPtr
//

#pragma once
#ifndef SCOPED_PTR_H
#define SCOPED_PTR_H

#include "BaseTypes.h"
#include "SharedPtr.h"
#include "WeakPtr.h"
#include "../Exceptions/NullPointerException.h"
#include "../Exceptions/NotImplementedException.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class ScopedPtr
{
public:
	explicit ScopedPtr(T * someObject);
	~ScopedPtr();

	inline T & operator *() const;
	inline T * operator ->() const;

	inline void Release();

private:
	T * object;

	ScopedPtr(const ScopedPtr<T> &) = delete;
	ScopedPtr(const SharedPtr<T> &) = delete;
	ScopedPtr(const WeakPtr<T> &) = delete;

	ScopedPtr<T> & operator =(const ScopedPtr<T> &) = delete;

	//для тестирования
	friend class TestScopedPtrGetters;
	friend class TestScopedPtrConstructor;
	friend class TestScopedPtrConstructorWithNull;
	friend class TestScopedPtrRelease;
};

//===========================================================================//

#include "ScopedPtr.inl"

} //namespace

#endif // SCOPED_PTR_H

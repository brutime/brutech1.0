//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<class ... Args>
Event<Args ...>::Event(InnerEvent<Args ...> & event) :
	_innerEvent(event)
{
}

//===========================================================================//

template<class ... Args>
Event<Args ...>::~Event()
{
}

//===========================================================================//

template<class ... Args>
void Event<Args ...>::AddHandler(const SharedPtr<EventHandler<Args ...> > & handler)
{
	_innerEvent.AddHandler(handler);
}

//===========================================================================//

template<class ... Args>
void Event<Args ...>::RemoveHandler(const SharedPtr<EventHandler<Args ...> > & handler)
{
	_innerEvent.RemoveHandler(handler);
}

//===========================================================================//

}// namespace

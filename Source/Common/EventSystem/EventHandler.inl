//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LambdaEventDelegate.h"

namespace Bru
{

//===========================================================================//

template<class ... Args> template<class Class>
EventHandler<Args ...>::EventHandler(Class * object, void (Class::*method)(Args ...)) :
	_delegate(new MethodEventDelegate<Class, Args ...>(object, method))
{
}

//===========================================================================//

template<class ... Args>
EventHandler<Args ...>::EventHandler(void (*staticMethod)(Args ...)) :
	_delegate(new StaticMethodEventDelegate<Args ...>(staticMethod))
{
}

//===========================================================================//

template<class ... Args>
EventHandler<Args ...>::EventHandler(const std::function<void(Args...)> & lambda):
	_delegate(new LambdaEventDelegate<Args...>(lambda))
{
}

//===========================================================================//

template<class ... Args>
EventHandler<Args ...>::EventHandler(const EventHandler<Args ...> & other) :
	_delegate(other._delegate)
{
}

//===========================================================================//

template<class ... Args>
EventHandler<Args ...>::~EventHandler()
{
}

//===========================================================================//

template<class ... Args>
EventHandler<Args ...> & EventHandler<Args ...>::operator =(const EventHandler<Args ...> & other)
{
	if (this != &other)
	{
		_delegate = other._delegate;
	}

	return *this;
}

//===========================================================================//

template<class ... Args>
void EventHandler<Args ...>::Invoke(Args ... args) const
{
	_delegate->Invoke(args ...);
}

//===========================================================================//

}//namespace


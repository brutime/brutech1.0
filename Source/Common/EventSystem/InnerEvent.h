//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Обработчик события - подписывается на событие и срабатывает при этом событии
//

#pragma once
#ifndef INNER_EVENT_H
#define INNER_EVENT_H

#include "../Types/BaseTypes.h"
#include "../Types/ScopedPtr.h"
#include "../Containers/List/List.h"

#include "EventHandler.h"

namespace Bru
{

//===========================================================================//

template<typename ... Args>
class InnerEvent
{
public:
	InnerEvent();
	~InnerEvent();

	void AddHandler(const SharedPtr<EventHandler<Args ...> > & handler);
	void RemoveHandler(const SharedPtr<EventHandler<Args ...> > & handler);
	bool ContainsHandler(const SharedPtr<EventHandler<Args ...> > & handler) const;

	void Fire(Args ... args) const;

	void SetEnabled(bool flag)
	{
		_isEnabled = flag;
	}
	void Enable()
	{
		_isEnabled = true;
	}
	void Disable()
	{
		_isEnabled = false;
	}
	bool IsEnabled() const
	{
		return _isEnabled;
	}

private:
	void RemoveAllHandlers();

	List<SharedPtr<EventHandler<Args ...> > > _eventHandlers;
	bool _isEnabled;

	InnerEvent(const InnerEvent &) = delete;
	InnerEvent & operator =(const InnerEvent &) = delete;
};

//===========================================================================//

struct EmptyEventArgs
{
};

//===========================================================================//

template<typename Sender>
struct SenderEventArgs
{
	SenderEventArgs(const SharedPtr<Sender> & someSender) :
		sender(someSender)
	{
	}

	SharedPtr<Sender> sender;
};

//===========================================================================//

}//namespace

#include "InnerEvent.inl"

#endif // EVENT_HANDLE_H

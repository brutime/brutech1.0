//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Делегат метода события - исполняет нестатический метод класса
//

#pragma once
#ifndef METHOD_EVENT_DELEGATE_IMPL_H
#define METHOD_EVENT_DELEGATE_IMPL_H

#include "IEventDelegate.h"

namespace Bru
{

//===========================================================================//

template<typename Class, typename ... Args>
class MethodEventDelegate : public IEventDelegate<Args ...>
{
	typedef void (Class::*Method)(Args ...);

public:
	MethodEventDelegate(Class * object, Method method);
	virtual ~MethodEventDelegate();

	void Invoke(Args ... args) const;
	bool operator ==(const MethodEventDelegate<Class, Args ... > & delegate) const;

private:
	Class * _object;
	Method _method;

	MethodEventDelegate(const MethodEventDelegate<Class, Args ...> &) = delete;
	MethodEventDelegate & operator =(const MethodEventDelegate<Class, Args ...> &) = delete;
};

//===========================================================================//

}//namespace

#include "MethodEventDelegate.inl"

#endif // METHOD_EVENT_DELEGATE_IMPL_H

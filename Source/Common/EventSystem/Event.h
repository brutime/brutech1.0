//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: оболочка для InnerEvent, которая предоставляет безопасный набор
//              публичных методов
//

#pragma once
#ifndef EVENT_H
#define EVENT_H

#include "InnerEvent.h"

namespace Bru
{

//===========================================================================//

template<typename ... Args>
class Event
{
public:
	Event(InnerEvent<Args ...> & _innerEvent);
	~Event();

	void AddHandler(const SharedPtr<EventHandler<Args ...> > & handler);
	void RemoveHandler(const SharedPtr<EventHandler<Args ...> > & handler);

private:
	InnerEvent<Args ...> & _innerEvent;

	Event(const Event &) = delete;
	Event & operator =(const Event &) = delete;
};

//===========================================================================//

}// namespace

#include "Event.inl"

#endif // EVENT_H

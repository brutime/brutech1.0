//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Делегат метода события - обертка для лябда-функции
//

#pragma once
#ifndef LAMBDA_EVENT_DELEGATE_H
#define LAMBDA_EVENT_DELEGATE_H

#include "IEventDelegate.h"

namespace Bru
{

//===========================================================================//

template<typename ... Args>
class LambdaEventDelegate : public IEventDelegate<Args ...>
{
public:
	LambdaEventDelegate(const std::function<void(Args...)> & lambda): _lambda(lambda) {}
	virtual ~LambdaEventDelegate() {}

	void Invoke(Args ... args) const { _lambda(args...); }

private:
	std::function<void(Args...)> _lambda;

	LambdaEventDelegate(const LambdaEventDelegate<Args ...> &) = delete;
	LambdaEventDelegate & operator =(const LambdaEventDelegate<Args ...> &) = delete;
};

//===========================================================================//

}//namespace

#endif // LAMBDA_EVENT_DELEGATE_H

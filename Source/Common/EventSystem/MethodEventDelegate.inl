//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<class Class, class ... Args>
MethodEventDelegate<Class, Args ...>::MethodEventDelegate(Class * object, Method method) :
	IEventDelegate<Args ...>(),
	_object(NullPtr),
	_method(NullPtr)
{
	if (object == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("object is NullPtr");
	}

	if (method == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("method is NullPtr");
	}

	_object = object;
	_method = method;
}

//===========================================================================//

template<class Class, class ... Args>
MethodEventDelegate<Class, Args ...>::~MethodEventDelegate()
{
}

//===========================================================================//

template<class Class, class ... Args>
void MethodEventDelegate<Class, Args ...>::Invoke(Args ... args) const
{
	(_object->*_method)(args ...);
}

//===========================================================================//

template<class Class, class ... Args>
bool MethodEventDelegate<Class, Args ...>::operator ==(const MethodEventDelegate<Class, Args ...> & delegate) const
{
	const MethodEventDelegate castedHandler = static_cast<const MethodEventDelegate>(delegate);
	return _object == castedHandler.object && _method == castedHandler.method;
}

//===========================================================================//

}//namespace

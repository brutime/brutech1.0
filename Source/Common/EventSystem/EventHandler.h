//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Обработчик события - содержит в себе набор делегаетов,
//              которые добавляются его подписчиками, вызывает исполнение
//              всех делегатов при выстреливании события
//

#pragma once
#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H

#include "../Types/BaseTypes.h"
#include "../Types/String/String.h"
#include "../Types/SharedPtr.h"
#include "../Containers/List/List.h"

#include "IEventDelegate.h"
#include "MethodEventDelegate.h"
#include "StaticMethodEventDelegate.h"

namespace Bru
{

//===========================================================================//

template<typename ... Args>
class EventHandler
{
public:
	template<typename Class>
	EventHandler(Class * object, void (Class::*method)(Args ...));
	EventHandler(void (*staticMethod)(Args ...));
	EventHandler(const std::function<void(Args...)> & lambda);
	EventHandler(const EventHandler<Args ...> & other);
		
	~EventHandler();

	EventHandler & operator =(const EventHandler<Args ...> & other);

	void Invoke(Args ... args) const;

private:
	SharedPtr<IEventDelegate<Args ...> > _delegate;
};

//===========================================================================//

}//namespace

#include "EventHandler.inl"

#endif // EVENT_HANDLER_H

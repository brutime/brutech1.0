//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Обработчик собатия - подписывается на событие и срабатывает при этом событии
//

#pragma once
#ifndef I_EVENT_DELEGATE_H
#define I_EVENT_DELEGATE_H

#include "../Types/BaseTypes.h"
#include "../Types/String/String.h"

namespace Bru
{

//===========================================================================//

template<typename ... Args>
class IEventDelegate
{
public:
	virtual ~IEventDelegate();

	virtual void Invoke(Args ... args) const = 0;

protected:
	IEventDelegate();

private:
	IEventDelegate(const IEventDelegate &) = delete;
	IEventDelegate & operator =(const IEventDelegate &) = delete;
};

//===========================================================================//

}//namespace

#include "IEventDelegate.inl"

#endif // I_EVENT_DELEGATE_H

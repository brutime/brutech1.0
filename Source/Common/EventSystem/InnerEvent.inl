//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<class ... Args>
InnerEvent<Args ...>::InnerEvent() :
	_eventHandlers(),
	_isEnabled(true)
{
}

//===========================================================================//

template<class ... Args>
InnerEvent<Args ...>::~InnerEvent()
{
	RemoveAllHandlers();
}

//===========================================================================//

template<class ... Args>
void InnerEvent<Args ...>::AddHandler(const SharedPtr<EventHandler<Args ...> > & handler)
{
	_eventHandlers.Add(handler);
}

//===========================================================================//

template<class ... Args>
void InnerEvent<Args ...>::RemoveHandler(const SharedPtr<EventHandler<Args ...> > & handler)
{
	_eventHandlers.Remove(handler);
}

//===========================================================================//

template<class ... Args>
void InnerEvent<Args ...>::RemoveAllHandlers()
{
	_eventHandlers.Clear();
}

//===========================================================================//

template<class ... Args>
bool InnerEvent<Args ...>::ContainsHandler(const SharedPtr<EventHandler<Args ...> > & handler) const
{
	return _eventHandlers.Contains(handler);
}

//===========================================================================//

template<class ... Args>
void InnerEvent<Args ...>::Fire(Args ... args) const
{
	if (!_isEnabled)
	{
		return;
	}

	for (const SharedPtr<EventHandler<Args ...>> & handler : _eventHandlers)
	{
		handler->Invoke(args ...);
	}
}

//===========================================================================//

}//namespace


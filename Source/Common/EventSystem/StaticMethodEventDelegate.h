//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Делегат метода события - исполняет статический метод класса
//

#pragma once
#ifndef STATIC_METHOD_EVENT_DELEGATE_H
#define STATIC_METHOD_EVENT_DELEGATE_H

#include "IEventDelegate.h"

namespace Bru
{

//===========================================================================//

template<typename ... Args>
class StaticMethodEventDelegate : public IEventDelegate<Args ...>
{
	typedef void (*StaticMethod)(Args ...);

public:
	StaticMethodEventDelegate(StaticMethod method);
	virtual ~StaticMethodEventDelegate();

	bool operator ==(const StaticMethodEventDelegate & delegate) const;

	void Invoke(Args ... args) const;

private:
	StaticMethod _method;

	StaticMethodEventDelegate(const StaticMethodEventDelegate<Args ...> &) = delete;
	StaticMethodEventDelegate & operator =(const StaticMethodEventDelegate<Args ...> &) = delete;
};

//===========================================================================//

}//namespace

#include "StaticMethodEventDelegate.inl"

#endif // STATIC_METHOD_EVENT_DELEGATE_H

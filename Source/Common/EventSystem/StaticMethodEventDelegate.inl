//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<class ... Args>
StaticMethodEventDelegate<Args ...>::StaticMethodEventDelegate(StaticMethod method) :
	IEventDelegate<Args ...>(),
	_method(NullPtr)
{
	if (method == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("method is NullPtr");
	}

	_method = method;
}

//===========================================================================//

template<class ... Args>
StaticMethodEventDelegate<Args ...>::~StaticMethodEventDelegate()
{
}

//===========================================================================//

template<class ... Args>
bool StaticMethodEventDelegate<Args ...>::operator ==(const StaticMethodEventDelegate & delegate) const
{
	return _method == delegate.method;
}

//===========================================================================//

template<class ... Args>
void StaticMethodEventDelegate<Args ...>::Invoke(Args ... args) const
{
	_method(args ...);
}

//===========================================================================//

}//namespace

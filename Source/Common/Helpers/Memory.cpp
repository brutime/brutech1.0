//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Memory.h"

#include <cstring>

#include "../Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

void Memory::Copy(void * destination, const void * source, integer size)
{
#ifdef DEBUGGING

	if (destination == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("destination == NullPtr");
	}

	if (source == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("source == NullPtr");
	}

	if (destination == source)
	{
		INVALID_ARGUMENT_EXCEPTION("destination == source");
	}

#endif // DEBUGGING

	if (size == 0)
	{
		return;
	}

	memcpy(destination, source, size);
}

//===========================================================================//

void Memory::Move(void * to, const void * from, integer size)
{
#ifdef DEBUGGING

	if (to == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("to == NullPtr");
	}

	if (from == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("from == NullPtr");
	}

	if (to == from)
	{
		INVALID_ARGUMENT_EXCEPTION("to == from");
	}

#endif // DEBUGGING
	if (size == 0)
	{
		return;
	}

	memmove(to, from, size);
}

//===========================================================================//

void Memory::FillWithNull(void * destination, integer size)
{
#ifdef DEBUGGING

	if (destination == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("destination == NullPtr");
	}

	if (size == 0)
	{
		INVALID_ARGUMENT_EXCEPTION("size == 0");
	}

#endif // DEBUGGING
	memset(destination, 0, size);
}

//===========================================================================//

void Memory::FillWithValue(void * destination, byte value, integer size)
{
#ifdef DEBUGGING

	if (destination == NullPtr)
	{
		INVALID_ARGUMENT_EXCEPTION("destination == NullPtr");
	}

#endif // DEBUGGING
	if (size == 0)
	{
		INVALID_ARGUMENT_EXCEPTION("Size == 0");
	}

	memset(destination, value, size);
}

//===========================================================================//

ComapareResult Memory::Compare(const void * first, const void * second, integer size)
{
	integer result = memcmp(first, second, size);

	if (result < 0)
	{
		return ComapareResult::Less;
	}

	if (result == 0)
	{
		return ComapareResult::Equals;
	}

	return ComapareResult::Greater;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FormatTokenParser.h"

#include "../Exceptions/ParsingException.h"

namespace Bru
{

//===========================================================================//

const utf8_char FormatTokenParser::OpenBracketChar = "{";
const utf8_char FormatTokenParser::CloseBracketChar = "}";

//===========================================================================//

FormatTokenParser::FormatTokenParser(const string & source):
	_index(),
	_source(source)
{
}

//===========================================================================//

FormatTokenParser::~FormatTokenParser()
{
}

//===========================================================================//

void FormatTokenParser::Reset()
{
	_index = 0;
}

//===========================================================================//

FormatTokenPosition FormatTokenParser::NextToken()
{
	FormatTokenPosition position;

	FindToken(OpenBracketChar, CloseBracketChar);
	position.SetPosition(_index < _source.GetLength() ? _index : -1);

	++_index;
	FindToken(CloseBracketChar, OpenBracketChar);

	if (_index >= _source.GetLength() && position.GetPosition() >= 0)
	{
		PARSING_EXCEPTION(string::Format("Expected '{0}' but not found.", CloseBracketChar));
	}
	position.SetLength(_index - position.GetPosition() + 1);

	++_index;
	return position;
}

//===========================================================================//

Nullable<string> FormatTokenParser::GetString(const FormatTokenPosition & position)
{
	if (position.GetPosition() == -1)
	{
		return NullPtr;	
	}		

	return _source.Substring(position.GetPosition() + 1, position.GetLength() - 2);
}

//===========================================================================//

Nullable<string> FormatTokenParser::NextTokenString()
{
	return GetString(NextToken());
}

//===========================================================================//

void FormatTokenParser::FindToken(const utf8_char & searchChar, const utf8_char & disallowedChar)
{
	for (; _index < _source.GetLength(); ++_index)
	{
		utf8_char current = _source[_index];
		if (current == disallowedChar)
		{
			PARSING_EXCEPTION(string::Format("Expected '{0}' but found '{1}'.", searchChar, disallowedChar));
		}

		if (current == searchChar)
		{
			return;
		}
	}
}

//===========================================================================//

} //namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<class T>
SingletonMixIn<T>::SingletonMixIn()
{
}

//===========================================================================//

template<class T>
SingletonMixIn<T>::~SingletonMixIn()
{
}

//===========================================================================//

template<class T>
SharedPtr<T> & SingletonMixIn<T>::Instance()
{
	static SharedPtr<T> instance = NullPtr;

	if (instance == NullPtr)
	{
		instance = new T();
	}

	return instance;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BRU_FORMAT_TOKEN_PARSER_H
#define BRU_FORMAT_TOKEN_PARSER_H

#include "../Common.h"
#include "FormatTokenPosition.h"

namespace Bru
{

//===========================================================================//

class FormatTokenParser
{
public:
	FormatTokenParser(const string & source);
	~FormatTokenParser();

	void Reset();
	FormatTokenPosition NextToken();

	Nullable<string> GetString(const FormatTokenPosition & position);
	Nullable<string> NextTokenString();

private:
	static const utf8_char OpenBracketChar;
	static const utf8_char CloseBracketChar;

	inline void FindToken(const utf8_char & searchChar, const utf8_char & disallowedChar);

	integer _index;
	string _source;

	FormatTokenParser(const FormatTokenParser &) = delete;
	FormatTokenParser & operator =(const FormatTokenParser &) = delete;

	friend class TestFormatTokenParserConstructor;
};

//===========================================================================//

} // namespace Bru

#endif // BRU_FORMAT_TOKEN_PARSER_H
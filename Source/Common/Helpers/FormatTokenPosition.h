//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef BRU_FORMAT_TOKEN_POSITION_H
#define BRU_FORMAT_TOKEN_POSITION_H

#include "../common.h"

namespace Bru
{

//===========================================================================//

class FormatTokenPosition
{
public:
	FormatTokenPosition();
	FormatTokenPosition(const FormatTokenPosition & other);
	~FormatTokenPosition();

	FormatTokenPosition & operator =(const FormatTokenPosition & other);

	void SetPosition(integer value);
	integer GetPosition() const;

	void SetLength(integer value);
	integer GetLength() const;

private:
	integer _position;
	integer _length;
};

//===========================================================================//

} // namespace Bru

#endif // BRU_FORMAT_TOKEN_POSITION_H

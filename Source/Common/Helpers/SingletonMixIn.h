//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SINGLETON_MIX_IN_H
#define SINGLETON_MIX_IN_H

#include "../Defines/Defines.h"
#include "../Types/SharedPtr.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class SingletonMixIn
{
public:
	virtual ~SingletonMixIn();

	static SharedPtr<T> & Instance();

protected:
	SingletonMixIn();

private:
	SingletonMixIn(const SingletonMixIn &) = delete;
	SingletonMixIn & operator =(const SingletonMixIn &) = delete;
};

//===========================================================================//

}//namespace

#include "SingletonMixIn.inl"

#endif // SINGLETON_MIX_IN_H

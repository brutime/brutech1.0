//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef TOKEN_INFO
#define TOKEN_INFO

#include "../Types/BaseTypes.h"
#include "../Types/String/String.h"

namespace Bru
{

//===========================================================================//

struct TokenInfo
{
	TokenInfo(const string & token, integer tokenStart, integer tokenEnd);
	~TokenInfo();

	string Token;
	integer TokenStart;
	integer TokenEnd;

//	TokenInfo(const TokenInfo &) = delete;
//	TokenInfo & operator =(const TokenInfo &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // TOKEN_INFO
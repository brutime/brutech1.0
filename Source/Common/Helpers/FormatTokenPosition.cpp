//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FormatTokenPosition.h"

namespace Bru
{

//===========================================================================//

FormatTokenPosition::FormatTokenPosition() : _position(), _length()
{
}

//===========================================================================//

FormatTokenPosition::FormatTokenPosition(const FormatTokenPosition & other) :
	_position(other._position),
	_length(other._length)
{
}

//===========================================================================//

FormatTokenPosition::~FormatTokenPosition()
{
}

//===========================================================================//

FormatTokenPosition & FormatTokenPosition::operator =(const FormatTokenPosition & other)
{
	if (&other != this)
	{
		_position = other._position;
		_length = other._length;
	}

	return *this;
}

//===========================================================================//

void FormatTokenPosition::SetPosition(const integer value)
{
	_position = value;
}

//===========================================================================//

integer FormatTokenPosition::GetPosition() const
{
	return _position;
}

//===========================================================================//

void FormatTokenPosition::SetLength(const integer value)
{
	_length = value;
}

//===========================================================================//

integer FormatTokenPosition::GetLength() const
{
	return _length;
}

//===========================================================================//

} // namespace Bru

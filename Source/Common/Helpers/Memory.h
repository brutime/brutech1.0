//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс памяти. Отвчеает за выделение и заполнение
//

#pragma once
#ifndef MEMORY_H
#define MEMORY_H

#include "../Types/BaseTypes.h"
#include "../Enums/CompareResult.h"

namespace Bru
{

namespace Memory
{

//===========================================================================//

extern void Copy(void * destination, const void * source, integer size);
extern void Move(void * to, const void * from, integer size);
extern void FillWithNull(void * destination, integer size);
extern void FillWithValue(void * destination, byte value, integer size);
extern ComapareResult Compare(const void * first, const void * second, integer size);

//===========================================================================//

}

} //namespace

#endif // MEMORY_H

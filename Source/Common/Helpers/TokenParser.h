//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Парсер токенов строк
//

#pragma once
#ifndef TOKEN_PARSER_H
#define TOKEN_PARSER_H

#include "../Types/BaseTypes.h"
#include "../Types/String/String.h"
#include "../Types/SharedPtr.h"
#include "../Types/Nullable.h"
#include "../Containers/Array/Array.h"
#include "TokenInfo.h"

namespace Bru
{

//===========================================================================//

class TokenParser
{
public:
	static const string DefaultDelimiters;

	TokenParser(const string & sourceString, const string & delimiters = DefaultDelimiters);
	virtual ~TokenParser();

	void Reset();
	bool HasMore() const;
	const string & GetNextToken();

	const string & GetToken(integer tokenIndex);
	const SharedPtr<TokenInfo> & GetTokenInfo(integer tokenIndex);
	Nullable<string> GetTokenAt(integer charIndex);
	integer GetTokenIndexAt(integer charIndex);	
	integer GetTokensCount() const;

private:
	integer GetWordEnd(integer wordStart) const;

	Array<SharedPtr<TokenInfo>> _tokenInfos;
	integer _currentTokenIndex;

	string _sourceString;
	string _delimiters;

	TokenParser(const TokenParser &) = delete;
	TokenParser & operator =(const TokenParser &) = delete;

	friend class TestTokenParserConstructorEmpty;
	friend class TestTokenParserConstructorJustDelimiters;
	friend class TestTokenParserConstructor;
};

//===========================================================================//

} // namespace Bru

#endif // TOKEN_PARSER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TokenParser.h"

#include "../Exceptions/InvalidStateException.h"

namespace Bru
{

//===========================================================================//

const string TokenParser::DefaultDelimiters = " ,;\n";

//===========================================================================//

TokenParser::TokenParser(const string & sourceString, const string & delimiters) :
	_tokenInfos(),
	_currentTokenIndex(0),
	_sourceString(sourceString),
	_delimiters(delimiters)	
{
	integer charIndex = 0;
	while (charIndex < sourceString.GetLength())
	{
		integer wordStart = charIndex;
		while (wordStart < sourceString.GetLength())
		{
			if (!_delimiters.Contains(sourceString[wordStart]))
			{
				break;
			}
			++wordStart;
		}

		integer wordEnd = GetWordEnd(wordStart);

		if (wordStart < sourceString.GetLength())
		{
			string token = sourceString.Substring(wordStart, wordEnd - wordStart);
			_tokenInfos.Add(new TokenInfo(token, wordStart, wordEnd - 1));
		}

		charIndex = wordEnd;
	}
}

//===========================================================================//

TokenParser::~TokenParser()
{
}

//===========================================================================//

void TokenParser::Reset()
{
	_currentTokenIndex = 0;
}

//===========================================================================//

bool TokenParser::HasMore() const
{
	return _currentTokenIndex < _tokenInfos.GetCount();
}

//===========================================================================//

const string & TokenParser::GetNextToken()
{
	if (!HasMore())
	{
		INVALID_STATE_EXCEPTION("There is no next token");
	}

	return _tokenInfos[_currentTokenIndex++]->Token;
}

//===========================================================================//

const string & TokenParser::GetToken(integer tokenIndex)
{
	return _tokenInfos[tokenIndex]->Token;
}

//===========================================================================//

const SharedPtr<TokenInfo> & TokenParser::GetTokenInfo(integer tokenIndex)
{
	return _tokenInfos[tokenIndex];
}

//===========================================================================//

Nullable<string> TokenParser::GetTokenAt(integer charIndex)
{
	if (_delimiters.Contains(_sourceString[charIndex]))
	{
		return NullPtr;
	}

	//Ищем начало слова
	integer wordStart = charIndex;
	while (wordStart > 0)
	{
		if (_delimiters.Contains(_sourceString[wordStart - 1]))
		{
			break;
		}
		--wordStart;
	}

	integer wordEnd = GetWordEnd(wordStart);
	return _sourceString.Substring(wordStart, wordEnd - wordStart);
	
	return NullPtr;
}

//===========================================================================//

integer TokenParser::GetTokenIndexAt(integer charIndex)
{
	for (integer i = 0; i < _tokenInfos.GetCount(); ++i)
	{
		const auto & tokenInfo = _tokenInfos[i];
		if (tokenInfo->TokenStart <= charIndex && charIndex <= tokenInfo->TokenEnd)
		{
			return i;
		}
	}
	
	return -1;
}

//===========================================================================//

integer TokenParser::GetTokensCount() const
{
	return _tokenInfos.GetCount();
}

//===========================================================================//

integer TokenParser::GetWordEnd(integer wordStart) const
{
	integer wordEnd = wordStart + 1;
	while (wordEnd < _sourceString.GetLength())
	{
		if (_delimiters.Contains(_sourceString[wordEnd]))
		{
			break;
		}
		++wordEnd;
	}

	return wordEnd;	
}

//===========================================================================//

} // namespace Bru

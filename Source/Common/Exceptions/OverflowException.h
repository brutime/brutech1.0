//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - выход за пределы чего-либо
//

#pragma once
#ifndef OVERFLOW_EXCEPTION_H
#define OVERFLOW_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class OverflowException : public Exception
{
public:
	OverflowException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~OverflowException();
};

//===========================================================================//

#define OVERFLOW_EXCEPTION(details) \
    throw OverflowException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // OVERFLOW_EXCEPTION_H

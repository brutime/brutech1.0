//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Регистратор иключений. Генерирует событие при регистрации исключения
//

#pragma once
#ifndef EXCEPTION_REGISTRATOR_H
#define EXCEPTION_REGISTRATOR_H

#include "Exception.h"
#include "../EventSystem/Event.h"

#include <mutex>

namespace Bru
{

//===========================================================================//

class ExceptionRegistratorSingleton
{
public:
	ExceptionRegistratorSingleton();
	~ExceptionRegistratorSingleton();

	void RegisterException(const Exception & exception) const;

private:
	InnerEvent<const Exception &> _newExceptionRegistered;
	mutable std::mutex _mutex;

public:
	Event<const Exception &> NewExceptionRegistered;

private:
	ExceptionRegistratorSingleton(const ExceptionRegistratorSingleton &) = delete;
	ExceptionRegistratorSingleton & operator =(const ExceptionRegistratorSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<ExceptionRegistratorSingleton> ExceptionRegistrator;

//===========================================================================//

}// namespace Bru

#endif // EXCEPTION_REGISTRATOR_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - ошибка при парсинге
//

#pragma once
#ifndef PARSING_EXCEPTION_H
#define PARSING_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class ParsingException : public Exception
{
public:
	ParsingException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~ParsingException();
};

//===========================================================================//

#define PARSING_EXCEPTION(details) \
    throw ParsingException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // PARSING_EXCEPTION_H

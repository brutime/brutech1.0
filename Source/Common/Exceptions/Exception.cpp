//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Exception.h"

#include "ExceptionRegistrator.h"

namespace Bru
{

//===========================================================================//

Exception::Exception(const string & details, const string & fileName, const string & methodName, integer fileLine) :
	_type("Exception"),
	_details(details),
	_fileName(fileName),
	_methodName(methodName),
	_fileLine(fileLine)
{
	Register();
}

//===========================================================================//

Exception::Exception(const string & type, const string & details, const string & fileName, const string & methodName,
                     integer fileLine) :
	_type(type),
	_details(details),
	_fileName(fileName),
	_methodName(methodName),
	_fileLine(fileLine)
{
	Register();
}

//===========================================================================//

Exception::Exception(const Exception & other) :
	_type(other._type),
	_details(other._details),
	_fileName(other._fileName),
	_methodName(other._methodName),
	_fileLine(other._fileLine)
{
}

//===========================================================================//

Exception::~Exception()
{
}

//===========================================================================//

Exception & Exception::operator =(const Exception & other)
{
	if (this == &other)
	{
		return *this;
	}

	_type = other._type;
	_details = other._details;
	_fileName = other._fileName;
	_methodName = other._methodName;
	_fileLine = other._fileLine;

	return *this;
}

//===========================================================================//

const string & Exception::GetType() const
{
	return _type;
}

//===========================================================================//

const string & Exception::GetDetails() const
{
	return _details;
}

//===========================================================================//

const string & Exception::GetFileName() const
{
	return _fileName;
}

//===========================================================================//

const string & Exception::GetMethodName() const
{
	return _methodName;
}

//===========================================================================//

integer Exception::GetFileLine() const
{
	return _fileLine;
}

//===========================================================================//

void Exception::Register()
{
	if (ExceptionRegistrator != NullPtr)
	{
		ExceptionRegistrator->RegisterException(*this);
	}
}

//===========================================================================//

std::ostream & operator <<(std::ostream & stream, const Exception & exception)
{
	stream << string::Format("\n", exception.GetType());
	stream << string::Format("type:    {0}\n", exception.GetType());
	stream << string::Format("details: {0}\n", exception.GetDetails());
	stream << string::Format("file:    {0}\n", exception.GetFileName());
	stream << string::Format("method:  {0}\n", exception.GetMethodName());
	stream << string::Format("line:    {0}\n", exception.GetFileLine());
	return stream;
}

//===========================================================================//

} // namespace Bru

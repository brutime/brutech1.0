//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - указатель содержит NullPtr, хотя ДОЛЖЕН указывать на полезную информацию
//

#pragma once
#ifndef NULL_POINTER_EXCEPTION_H
#define NULL_POINTER_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class NullPointerException : public Exception
{
public:
	NullPointerException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~NullPointerException();
};

//===========================================================================//

#define NULL_POINTER_EXCEPTION(details) \
    throw NullPointerException(details, FILE_NAME, METHOD_NAME, FILE_LINE)

//===========================================================================//

}//namespace

#endif // NULL_POINTER_EXCEPTION_H

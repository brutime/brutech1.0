//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - ошибка инициализации объекта
//

#pragma once
#ifndef INITIALIZATION_EXCEPTION_H
#define INITIALIZATION_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class InitializationException : public Exception
{
public:
	InitializationException(const string & details, const string & fileName, const string & methodName,
	                        integer fileLine);
	virtual ~InitializationException();
};

//===========================================================================//

#define INITIALIZATION_EXCEPTION(details) \
    throw InitializationException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // INITIALIZATION_EXCEPTION_H

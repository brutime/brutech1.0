//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - ошибка инициализации объекта
//

#pragma once
#ifndef SHUT_DOWN_EXCEPTION_H
#define SHUT_DOWN_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class ShutDownException : public Exception
{
public:
	ShutDownException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~ShutDownException();
};

//===========================================================================//

#define SHUT_DOWN_EXCEPTION(details) \
    throw ShutDownException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // SHUT_DOWN_EXCEPTION_H

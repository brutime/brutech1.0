//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - итератор вышел за границы
//

#pragma once
#ifndef ITERATOR_OUT_OF_BOUNDS_EXCEPTION_H
#define ITERATOR_OUT_OF_BOUNDS_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class IteratorOutOfBoundsException : public Exception
{
public:
	IteratorOutOfBoundsException(const string & details, const string & fileName, const string & methodName,
	                             integer fileLine);
	virtual ~IteratorOutOfBoundsException();
};

//===========================================================================//

#define ITERATOR_OUT_OF_BOUNDS_EXCEPTION(details) \
    throw IteratorOutOfBoundsException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // ITERATOR_OUT_OF_BOUNDS_EXCEPTION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - неподходящий аргумент
//

#pragma once
#ifndef INVALID_ARGUMENT_EXCEPTION_H
#define INVALID_ARGUMENT_EXCEPTION_H

#include "../Defines/Defines.h"
#include "Exception.h"

namespace Bru
{

//===========================================================================//

class InvalidArgumentException : public Exception
{
public:
	InvalidArgumentException(const string & details, const string & fileName, const string & methodName,
	                         integer fileLine);
	virtual ~InvalidArgumentException();

protected:
	InvalidArgumentException(const string & initType, const string & details, const string & fileName,
	                         const string & methodName, integer fileLine);
};

//===========================================================================//

#define INVALID_ARGUMENT_EXCEPTION(details) \
    throw InvalidArgumentException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // INVALID_ARGUMENT_EXCEPTION_H

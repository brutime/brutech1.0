//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "InvalidUTF32SequenceException.h"

namespace Bru
{

//===========================================================================//

InvalidUTF32SequenceException::InvalidUTF32SequenceException(const string & details, const string & fileName,
        const string & methodName, integer fileLine) :
	InvalidArgumentException("Invalid UTF32 sequence exception", details, fileName, methodName, fileLine)
{
}

//===========================================================================//

InvalidUTF32SequenceException::~InvalidUTF32SequenceException()
{
}

//===========================================================================//

}//namespace

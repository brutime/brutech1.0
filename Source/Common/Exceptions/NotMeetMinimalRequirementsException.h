//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Система не удовлетворяет минимальным требованиям
//

#pragma once
#ifndef NOT_MEET_MINIMAL_REQUIREMENTS_EXCEPTION_H
#define NOT_MEET_MINIMAL_REQUIREMENTS_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class NotMeetMinimalRequirementsException : public Exception
{
public:
	NotMeetMinimalRequirementsException(const string & details, const string & fileName, const string & methodName,
	                                    integer fileLine);
	virtual ~NotMeetMinimalRequirementsException();
};

//===========================================================================//

#define NOT_MEET_MINIMAL_REQUIREMENTS_EXCEPTION(details) \
    throw NotMeetMinimalRequirementsException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // NOT_MEET_MINIMAL_REQUIREMENTS_EXCEPTION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - неверная данные в UTF32 последовательности
//

#pragma once
#ifndef INVALID_UTF32_SEQUENCE_EXCEPTION_H
#define INVALID_UTF32_SEQUENCE_EXCEPTION_H

#include "InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

class InvalidUTF32SequenceException : public InvalidArgumentException
{
public:
	InvalidUTF32SequenceException(const string & details, const string & fileName, const string & methodName,
	                              integer fileLine);
	virtual ~InvalidUTF32SequenceException();
};

//===========================================================================//

#define INVALID_UTF32_SEQUENCE_EXCEPTION(details) \
    throw InvalidUTF32SequenceException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // INVALID_UTF32_SEQUENCE_EXCEPTION_H

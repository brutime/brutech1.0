//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - индекс вышел за границы массива или недопустим для других структур данных
//

#pragma once
#ifndef INDEX_OUT_OF_BOUNDS_EXCEPTION_H
#define INDEX_OUT_OF_BOUNDS_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class IndexOutOfBoundsException : public Exception
{
public:
	IndexOutOfBoundsException(const string & details, const string & fileName, const string & methodName,
	                          integer fileLine);
	virtual ~IndexOutOfBoundsException();
};

//===========================================================================//

#define INDEX_OUT_OF_BOUNDS_EXCEPTION(details) \
    throw IndexOutOfBoundsException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // INDEX_OUT_OF_BOUNDS_EXCEPTION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ExceptionRegistrator.h"

namespace Bru
{

//===========================================================================//

SharedPtr<ExceptionRegistratorSingleton> ExceptionRegistrator;

//===========================================================================//

ExceptionRegistratorSingleton::ExceptionRegistratorSingleton() :
	_newExceptionRegistered(),
	_mutex(),
	NewExceptionRegistered(_newExceptionRegistered)	
{
}

//===========================================================================//

ExceptionRegistratorSingleton::~ExceptionRegistratorSingleton()
{
}

//===========================================================================//

void ExceptionRegistratorSingleton::RegisterException(const Exception & exception) const
{
	std::lock_guard<std::mutex> guard(_mutex);
	_newExceptionRegistered.Fire(exception);
}

//===========================================================================//

}// namespace Bru

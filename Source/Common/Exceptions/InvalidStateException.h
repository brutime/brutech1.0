//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - объект находится в недопустимом состоянии, и не может выполнить операцию правильно
//

#pragma once
#ifndef INVALID_STATE_EXCEPTION_H
#define INVALID_STATE_EXCEPTION_H

#include "Exception.h"

namespace Bru
{

//===========================================================================//

class InvalidStateException : public Exception
{
public:
	InvalidStateException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~InvalidStateException();
};

//===========================================================================//

#define INVALID_STATE_EXCEPTION(details) \
    throw InvalidStateException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // INVALID_STATE_EXCEPTION_H

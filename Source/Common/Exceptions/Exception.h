//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Неопеределенное ислкючение
//

#pragma once
#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <ostream>
#include "../Defines/Defines.h"
#include "../Types/BaseTypes.h"
#include "../Types/String/String.h"

namespace Bru
{

//===========================================================================//

class Exception
{
public:
	Exception(const string & details, const string & fileName, const string & methodName, integer fileLine);
	Exception(const Exception & other);
	virtual ~Exception();

	Exception & operator =(const Exception & other);

	const string & GetType() const;
	const string & GetDetails() const;
	const string & GetFileName() const;
	const string & GetMethodName() const;
	integer GetFileLine() const;

protected:
	Exception(const string & type, const string & details, const string & fileName, const string & methodName,
	          integer fileLine);

private:
	void Register();

	string _type;
	string _details;
	string _fileName;
	string _methodName;
	integer _fileLine;
};

//===========================================================================//

#define EXCEPTION(details) \
    throw Exception(details, FILE_NAME, METHOD_NAME, FILE_LINE)

//===========================================================================//

extern std::ostream & operator <<(std::ostream & stream, const Exception & exception);

//===========================================================================//

} //namespace Bru

#endif // EXCEPTION_H

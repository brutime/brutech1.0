//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef I_LOCATION_EDITOR_STATE_H
#define I_LOCATION_EDITOR_STATE_H

namespace Bru
{

//===========================================================================//

class ILocationEditorState
{
public:
	virtual ~ILocationEditorState();

	virtual void Select();
	virtual void Leave();
	
	virtual void EnableInput();
	virtual void DisableInput();
	
	virtual void OnFileOpened();
	virtual void OnFileClosed();

protected:
	ILocationEditorState();

private:
	ILocationEditorState(const ILocationEditorState &) = delete;
	ILocationEditorState & operator =(const ILocationEditorState &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // I_LOCATION_EDITOR_STATE_H

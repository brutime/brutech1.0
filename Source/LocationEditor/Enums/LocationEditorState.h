//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LOCATION_EDITOR_STATE_H
#define LOCATION_EDITOR_STATE_H

namespace Bru
{

//===========================================================================//

enum class LocationEditorState
{
	None,
    ArrangementEditor,
    NavigationMeshEditor,
	PreviewMode,
	GameplayMode,
	Count
};

//===========================================================================//

} // namespace Bru

#endif // LOCATION_EDITOR_STATE_H
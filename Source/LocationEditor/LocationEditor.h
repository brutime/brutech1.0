//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Редактор карт
//

#ifndef LOCATION_EDITOR_H
#define LOCATION_EDITOR_H

#include "../Helpers/Engine/Engine.h"
#include "../GameHelpers/GameHelpers.h"
#include "LocationLandmarks.h"
#include "Enums/LocationEditorState.h"
#include "ILocationEditorState.h"
#include "ArrangementEditor/ArrangementEditor.h"
#include "NavigationMeshEditor/NavigationMeshEditor.h"
#include "PreviewMode/PreviewMode.h"
#include "LocationEditorInputReceiver.h"
#include "EditorConfig.h"

namespace Bru
{

//===========================================================================//

class LocationEditorSingleton
{
public:
	static const string Name;
	static const string DomainName;

	static const string LandmarksLayerName;
	static const string NavigationMeshLayerName;
	static const string HUDLayerName;

	static const string PathToLocationEditor;
	static const string PathToState;
	static const string PathToGameplayLayerName;

	static const string PathToNewLocationDefaults;
	static const string PathToNewLocationDefaultWidth;
	static const string PathToNewLocationDefaultHeight;
	static const string PathToNewLocationDefaultOffset;
	static const string PathToNewLocationDefaultLayersNames;
	static const string PathToNewLocationDefaultPurityLogicEntities;

	LocationEditorSingleton(const SharedPtr<ILocationEditorState> & gameplayMode);
	//LocationEditorSingleton();
	virtual ~LocationEditorSingleton();

	void NewFile();
	void NewFile(float width, float height, const Vector2 & offset);
	void NewFile(float width, float height, float offsetX, float offsetY);
	void OpenFile(const string & pathToFile);
	void SaveFile();
	void SaveAsFile(const string & pathToFile);	
	void CloseFile();

	Vector3 GetMousePoint(float x, float y, float targetZ = 0.0f) const;
	float GetMouseDelta(float delta, float targetZ = 0.0f) const;

	void ShowNavigationMesh();
	void HideNavigationMesh();
	bool IsNavigationMeshVisible() const;

	void SelectState(LocationEditorState newEditorState);
	void SelectState(integer newEditorStateValue);
	LocationEditorState GetCrrentEditorState() const;

	void SwitchToLocationObserverInput();
	void SwitchToStatesInput();

	SharedPtr<ArrangementEditor> GetArrangementEditor() const;
	SharedPtr<NavigationMeshEditor> GetNavigationMeshEditor() const;
	SharedPtr<PreviewMode> GetPreviewMode() const;
	bool HasGameplayMode() const;

	const SharedPtr<Location> & GetLocation() const;
	const SharedPtr<CapturedMovingLocationObserver> & GetLocationObserver() const;
	const SharedPtr<LocationLandmarks> GetLocationLandmarks() const;

private:
	static const string AutoSavePostfix;

	void CreateLocationObserver();
	void DestroyLocationObserver();
	
	void OnEntityCreatedOrRemoved(const EntityManagerEventArgs & args);	

	Array<string> GetLocationsNames() const;

	void RegisterCommands();
	void UnregisterCommands();

	static LocationEditorState GetLocationEditorStateByIntegerValue(integer value);

	string _gameplayLayerName;

	SharedPtr<Location> _location;
	SharedPtr<CapturedMovingLocationObserver> _locationObserver;
	SharedPtr<CapturedMovingLocationObserverInputReceiver> _locationObserverInputReceiver;

	bool _isLocationDirty;

	SharedPtr<LocationLandmarks> _locationLandmarks;

	Array<SharedPtr<ILocationEditorState>> _states;
	LocationEditorState _currentEditorState;

	SharedPtr<LocationEditorInputReceiver> _inputReceiver;

	SharedPtr<NavigationMeshVisualizer> _navigationMeshVisualizer;

	SharedPtr<EventHandler<const EntityManagerEventArgs &>> _entityCreatedEventHandler;
	SharedPtr<EventHandler<const EntityManagerEventArgs &>> _entityRemovedEventHandler;

	LocationEditorSingleton(const LocationEditorSingleton &) = delete;
	LocationEditorSingleton & operator =(const LocationEditorSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<LocationEditorSingleton> LocationEditor;

//===========================================================================//

} // namespace Bru

#endif // LOCATION_EDITOR_H

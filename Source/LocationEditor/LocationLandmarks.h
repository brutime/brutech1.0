//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LOCATION_LANDMARKS_H
#define LOCATION_LANDMARKS_H

#include "../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class LocationLandmarks
{
public:
	static const string PathToLandmarks;
	static const string PathToLandmarksIsVisible;
	static const string PathToLandmarkType;
	static const string PathToLandmarkColor;
	static const string PathToLineThickness;
	static const string PathToLandmarkStartPoint;
	static const string PathToLandmarkEndPoint;
	static const string PathToTileWidth;
	static const string PathToTileHeight;
	static const string PathToTilesCountByWidth;
	static const string PathToTilesCountByHeight;

	LocationLandmarks(const string & domainName, const string & landmarksLayerName);
	virtual ~LocationLandmarks();

	void Show();
	void Hide();
	bool IsVisible() const;
	void SwapVisibility();

private:
	void CreateLandmarks();

	string _domainName;
	string _landmarksLayerName;
	List<SharedPtr<Renderable>> _landmarks;

private:
	static const string LineType;
	static const string RectangleType;
	static const string GridType;	

	LocationLandmarks(const LocationLandmarks &) = delete;
	LocationLandmarks & operator =(const LocationLandmarks &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // LOCATION_LANDMARKS_H

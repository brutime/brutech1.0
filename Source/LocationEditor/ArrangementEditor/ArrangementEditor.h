//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ARRANGEMENT_EDITOR_H
#define ARRANGEMENT_EDITOR_H

#include "../../Helpers/Engine/Engine.h"

#include "../ILocationEditorState.h"
#include "Commands/ArrangementEditorCommands.h"
#include "EntitiesBrowser.h"
#include "LayerBrowser.h"
#include "EntityVisualizationInfo.h"
#include "SelectedEntityVisualizationInfo.h"
#include "ArrangementEntitiesInputReceiver.h"

namespace Bru
{

//===========================================================================//

class ArrangementEditor : public ILocationEditorState
{
public:
	static const string UILayerName;
	static const string AddingEntityUILayerName;

	static const string PathToArrangementEditor;
	static const string PathToIsNavigationMeshVisible;
	
	static const string PathToEntitiesFrame;
	static const string PathToEntitiesFrameColor;
	static const string PathToEntitiesFrameLineThickness;
	static const string PathToSelectedEntity;
	static const string PathToSelectedEntityInfoLabelFontName;
	static const string PathToSelectedEntityInfoLabelFontSize;
	static const string PathToSelectedEntityFrameColor;
	static const string PathToSelectedEntityAnchorsColor;
	static const string PathToSelectedEntityRotationAnchorRadius;
	static const string PathToSelectedEntitySizeAnchorsSide;	

	static const string PathToCommonColors;
	static const string PathToBackgroundColor;
	static const string PathToButtonNormalColor;
	static const string PathToButtonPressedColor;
	static const string PathToButtonHoveredColor;

	static const string PathToButtonsFont;
	static const string PathToButtonsFontName;
	static const string PathToButtonsFontSize;
	static const string PathToButtonsFontColor;

	static const float EntityMovingSmallStep;
	static const float EntityMovingLargeStep;
	static const float EntityRotationSmallStep;
	static const float EntityRotationLargeStep;
	static const float EntityResizingSmallStep;
	static const float EntityResizingLargeStep;	
	
	static const float EntityZoomStep;
	static const float MinSize;

	ArrangementEditor(const string & domainName, const string & gameplayLayerName);
	virtual ~ArrangementEditor();

	virtual void Select();
	virtual void Leave();
	
	virtual void EnableInput();
	virtual void DisableInput();
	
	virtual void OnFileClosed();

	void SelectEntity(const WeakPtr<Entity> & selectedEntity);
	void UnselectEntity(bool hard = false);
	WeakPtr<Entity> GetSelectedEntity() const;

	void SelectLayer(const string & selectingLayerName);
	const string & GetSelectedLayerName() const;

	WeakPtr<Entity> GetUpperEntity(float mouseX, float mouseY) const;

	bool IsEntityRotationAnchorContainsPoint(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const;
	bool IsEntitySizeAnchorsContainsPoint(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const;
	PullingSide GetEntityPullingSide(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const;

	SharedPtr<Entity> CreateEntity(const string & pathToEntity, const Vector3 & entityPosition);
	void AddEntity(const SharedPtr<Entity> & entity);

	void RemoveEntity(const SharedPtr<Entity> & entity);

	void MoveEntityByXYAxises(const WeakPtr<Entity> & entity, float mouseX, float mouseY, const Vector2 & captureOffsetCoords) const;
	void MoveEntityByZAxis(const WeakPtr<Entity> & entity, float mouseDelta) const;
	void SetEntityPositionZ(const WeakPtr<Entity> & entity, float z) const;
	void RotateEntity(const WeakPtr<Entity> & entity, float mouseX, float mouseY, bool useAngleStep) const;
	void ResizeEntity(const WeakPtr<Entity> & entity, PullingSide pullingSide, float mouseX, float mouseY) const;
	void ResetEntity(const WeakPtr<Entity> & entity) const;
	bool IsEntityNotReseted(const WeakPtr<Entity> & entity) const;
	
	void ShowUI();
	void HideUI();
	bool IsUIVisible() const;
	bool IsUIContainsPoint(float mouseX, float mouseY) const;

	const SharedPtr<EntitiesBrowser> & GetEntitiesBrowser() const;
	const SharedPtr<LayerBrowser> & GetLayerBrowser() const;

	const SharedPtr<CommandsManager> & GetCommandsManager() const;

private:
	static const float EntityZoomFactor;

	void UpdateSelectedEntityInfoLabel();

	bool IsEntitySuitableForSelectedLayer(const WeakPtr<Entity> & entity) const;

	void ApplyLayerVisibleToVisualizationInfos(const WeakPtr<RenderableLayer> & layer);

	void AddLayers() const;
	void RemoveLayers() const;

	void UpdateNavigationMeshVisibility() const;

	void CreateLayerBrowser();
	void CreateSelectedEntityInfoLabel();

	void AddHandlersToEntity(const WeakPtr<Entity> & entity);
	void RemoveHandlersFromEntity(const WeakPtr<Entity> & entity);

	void OnLayerVisibleChanged(const LayerEventArgs & args);

	void OnEntityCreated(const EntityManagerEventArgs & args);
	void OnEntityRemoved(const EntityManagerEventArgs & args);

	void OnSelectedEntityPositioningNodeTransformationChanged(const EmptyEventArgs &);
	void OnSelectedEntityRotationNodeTransformationChanged(const EmptyEventArgs &);
	void OnSelectedEntityRenderingOrderChanged(const EmptyEventArgs &);
	void OnSelectedEntitySizeChanged(const EmptyEventArgs &);
	
	string _domainName;
	string _gameplayLayerName;

	WeakPtr<Entity> _selectedEntity;

	string _selectedLayerName;

	TreeMap<WeakPtr<Entity>, SharedPtr<EntityVisualizationInfo>> _entitiesVisualizationInfos;
	SharedPtr<SelectedEntityVisualizationInfo> _selectedEntityVisualizationInfo;

	SharedPtr<Label> _selectedEntityInfoLabel;
	SharedPtr<EntitiesBrowser> _entitiesBrowser;
	SharedPtr<LayerBrowser> _layerBrowser;

	SharedPtr<ArrangementEntitiesInputReceiver> _inputReceiver;

	SharedPtr<CommandsManager> _commandsManager;

	SharedPtr<EventHandler<const LayerEventArgs &>> _layerVisibleChangedHandler;

	SharedPtr<EventHandler<const EntityManagerEventArgs &>> _entityCreatedEventHandler;
	SharedPtr<EventHandler<const EntityManagerEventArgs &>> _entityRemovedEventHandler;

	SharedPtr<EventHandler<const EmptyEventArgs &>> _selectedEntityPositioningNodeTransformationChangedEventHandler;
	SharedPtr<EventHandler<const EmptyEventArgs &>> _selectedEntityRotationNodeTransformationChangedEventHandler;
	SharedPtr<EventHandler<const EmptyEventArgs &>> _selectedEntityRenderingOrderChangedEventHandler;
	SharedPtr<EventHandler<const EmptyEventArgs &>> _selectedEntitySizeChangedEventHandler;

protected:
	InnerEvent<const string &> _layerChanged;

public:
	Event<const string &> LayerChanged;

private:
	ArrangementEditor(const ArrangementEditor &) = delete;
	ArrangementEditor & operator =(const ArrangementEditor &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ARRANGEMENT_EDITOR_H

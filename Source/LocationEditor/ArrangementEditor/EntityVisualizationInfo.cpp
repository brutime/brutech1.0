//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntityVisualizationInfo.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

EntityVisualizationInfo::EntityVisualizationInfo(const string & domainName, const string & layerName, const WeakPtr<Entity> & targetEntity) :
	EntityVisualizationInfo(domainName, layerName, targetEntity, EditorConfig->Get<Color>(ArrangementEditor::PathToEntitiesFrameColor))
{
}

//===========================================================================//

EntityVisualizationInfo::EntityVisualizationInfo(const string & domainName, const string & layerName,
                                                 const WeakPtr<Entity> & targetEntity, const Color & frameColor) :
	_domainName(domainName),
	_layerName(layerName),
	_targetEntity(targetEntity),
	_frame(new RenderableRectangle(_domainName, _layerName, {}, {}, false, frameColor,
	                               EditorConfig->Get<float>(ArrangementEditor::PathToEntitiesFrameLineThickness))),
	_isVisible(true),
	_targetSizeChangedEventHandler(new EventHandler<const EmptyEventArgs &>(this, &EntityVisualizationInfo::OnTargetSizeChanged))
{
	const auto & sprite = _targetEntity->GetComponent<BaseSprite>();

	_frame->SetSize(sprite->GetSize());
	_frame->SetOriginFactor(sprite->GetOriginFactor());

	_frame->SetPositioningNode(GetPositioningNodeFromTargetEntity());

	SetVisible(sprite->IsVisible());

	sprite->SizeChangedEvent.AddHandler(_targetSizeChangedEventHandler);
}

//===========================================================================//

EntityVisualizationInfo::~EntityVisualizationInfo()
{
	_targetEntity->GetComponent<BaseSprite>()->SizeChangedEvent.RemoveHandler(_targetSizeChangedEventHandler);
}

//===========================================================================//

const WeakPtr<Entity> & EntityVisualizationInfo::GetTargetEntity() const
{
	return _targetEntity;
}

//===========================================================================//

void EntityVisualizationInfo::SetVisible(bool isVisible)
{
	_frame->SetVisible(isVisible);
}

//===========================================================================//

bool EntityVisualizationInfo::IsVisible() const
{
	return _isVisible;
}

//===========================================================================//

void EntityVisualizationInfo::Show()
{
	SetVisible(true);
}

//===========================================================================//

void EntityVisualizationInfo::Hide()
{
	SetVisible(false);
}

//===========================================================================//

const string & EntityVisualizationInfo::GetDomainName() const
{
	return _domainName;
}

//===========================================================================//

const string & EntityVisualizationInfo::GetLayerName() const
{
	return _layerName;
}

//===========================================================================//

SharedPtr<BasePositioningNode> EntityVisualizationInfo::GetPositioningNodeFromTargetEntity() const
{
	if (_targetEntity->ContainsComponent(RotationNode::TypeID))
	{
		return _targetEntity->GetComponent<RotationNode>();
	}
	else
	{
		return _targetEntity->GetComponent<PositioningNode>();
	}
}

//===========================================================================//

void EntityVisualizationInfo::OnTargetSizeChanged(const EmptyEventArgs &)
{
	UpdateFrameSize();
}

//===========================================================================//

void EntityVisualizationInfo::UpdateFrameSize()
{
	_frame->SetSize(_targetEntity->GetComponent<BaseSprite>()->GetSize());
}

//===========================================================================//

} //namespace Bru

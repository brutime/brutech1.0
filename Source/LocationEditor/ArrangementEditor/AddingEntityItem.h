//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ADDING_ENTITY_ITEM_H
#define ADDING_ENTITY_ITEM_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

struct AddingEntityItem
{
	AddingEntityItem(const string & domainName, const string & layerName, const string & path,
	                 const Vector3 & position, float spriteAlpha);
	~AddingEntityItem();

	string Path;
	SharedPtr<PositioningNode> Node;
	SharedPtr<BaseSprite> Sprite;

	AddingEntityItem(const AddingEntityItem &) = delete;
	AddingEntityItem & operator =(const AddingEntityItem &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ADDING_ENTITY_ITEM_H

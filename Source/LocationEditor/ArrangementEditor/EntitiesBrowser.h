//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//     Описание:
//

#pragma once
#ifndef ENTITIES_BROWSER_H
#define ENTITIES_BROWSER_H

#include "../../Helpers/Engine/Engine.h"

#include "EntitiesBrowserItem.h"

namespace Bru
{

//===========================================================================//

class EntitiesBrowser : public Frame
{
public:
	const float AddingEntitySpriteAlpha;

	EntitiesBrowser(const string & domainName, const string & layerName);
	virtual ~EntitiesBrowser();
	
	virtual void SetVisible(bool flag);

	SharedPtr<EntitiesBrowserItem> GetItem(integer mouseX, integer mouseY) const;
	
private:
	static const string PathToEntitiesBrowser;
	static const string PathToFontName;
	static const string PathToFontSize;
	static const string PathToFontColor;
	static const string PathToItemBackgroundColor;
	static const string PathToWidth;
	static const string PathToPadding;
	static const string PathToItemHeight;
	static const string PathToItemsVerticalPadding;
	static const string PathToButtonSize;
	static const string PathToAddingEntitySpriteAlpha;

	void LoadItems();
	void UpdateCurrentPage();
	void UpdateCurrentPageItems();
	void UpdateCurrentPageUI();
	
	bool IsInCurrentPage(integer itemIndex) const;

	void OnNextButtonClick(const Widget*, const MouseEventArgs &);
	void OnBackButtonClick(const Widget*, const MouseEventArgs &);
	
	void OnEntityGenerated(const EmptyEventArgs & args);

	float GetNotItemsSpaceHeight() const;

	const Color ItemBackgroundColor;
	const Vector2 Padding;
	const float SourceItemHeight;
	const float ItemsVerticalPadding;
	const Vector2 ButtonSize;

	float _itemWidth;
	float _itemHeight;
	
	Vector2 _hintSize;
	integer _itemsPerPage;
	integer _currentPageIndex;
	Array<SharedPtr<EntitiesBrowserItem>> _items;

	SharedPtr<LabeledButton> _backButton;
	SharedPtr<Label> _pageLabel;
	SharedPtr<LabeledButton> _nextButton;
	
	SharedPtr<EventHandler<const EmptyEventArgs &>> _entityGeneratedEventHandler;

private:
	EntitiesBrowser(const EntitiesBrowser &) = delete;
	EntitiesBrowser & operator =(const EntitiesBrowser &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ENTITIES_BROWSER_H

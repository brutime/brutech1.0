//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SELECTED_ENTITY_VISUALIZATION_INFO_H
#define SELECTED_ENTITY_VISUALIZATION_INFO_H

#include "../../Helpers/Engine/Engine.h"
#include "EntityVisualizationInfo.h"

namespace Bru
{

//===========================================================================//

class SelectedEntityVisualizationInfo : public EntityVisualizationInfo
{
public:
	SelectedEntityVisualizationInfo(const string & domainName, const string & layerName, const WeakPtr<Entity> & targetEntity);
	virtual ~SelectedEntityVisualizationInfo();
	
	virtual void SetVisible(bool isVisible);

	float GetRotationAnchorRadius() const;
	float GetRotationAnchorProjectRadius() const;
	const SharedPtr<RenderableCircle> & GetRotationAnchor() const;

	float GetSizeAnchorsSide() const;
	float GetSizeAnchorsProjectSide() const;
	const Array<SharedPtr<RenderableRectangle>> & GetSizeAnchors() const;

protected:
	virtual void OnTargetSizeChanged(const EmptyEventArgs & args);
	virtual void OnTargetTransformationChanged(const EmptyEventArgs &);

private:
	static const float RotationAnchorOffset;

	void UpdateSizeAnchors();
	float GetCurrentProjection(float value) const;

	float _rotationAnchorRadius;
	SharedPtr<RenderableCircle> _rotationAnchor;

	float _sizeAnchorSide;
	Array<SharedPtr<RenderableRectangle>> _sizeAnchors;

	SharedPtr<EventHandler<const EmptyEventArgs &>> _transformationChangedHandler;

	SelectedEntityVisualizationInfo(const SelectedEntityVisualizationInfo &) = delete;
	SelectedEntityVisualizationInfo & operator =(const SelectedEntityVisualizationInfo &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // SELECTED_ENTITY_VISUALIZATION_INFO_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef DECREMENT_RENDERING_ORDER_COMMAND_H
#define DECREMENT_RENDERING_ORDER_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class DecrementRenderingOrderCommand : public Command
{
public:
	DecrementRenderingOrderCommand(const SharedPtr<Entity> & entity);
	virtual ~DecrementRenderingOrderCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	static const float IncrementStep;

	SharedPtr<Entity> _entity;

	DecrementRenderingOrderCommand(const DecrementRenderingOrderCommand &) = delete;
	DecrementRenderingOrderCommand & operator =(const DecrementRenderingOrderCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // DECREMENT_RENDERING_ORDER_COMMAND_H

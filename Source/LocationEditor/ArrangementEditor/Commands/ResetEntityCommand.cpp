//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ResetEntityCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

ResetEntityCommand::ResetEntityCommand(const SharedPtr<Entity> & entity) :
	Command(true),
	_entity(entity),
	_previousEntityPositionZ(_entity->GetComponent<PositioningNode>()->GetPosition().Z),
	_previousEntitySize(_entity->GetComponent<BaseSprite>()->GetSize()),
	_previousEntityAngle(EntityHelper::GetEntityAngle(_entity))
{
}

//===========================================================================//

ResetEntityCommand::~ResetEntityCommand()
{
}

//===========================================================================//

void ResetEntityCommand::Execute()
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	editor->ResetEntity(_entity);
}

//===========================================================================//

void ResetEntityCommand::Cancel()
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	editor->SetEntityPositionZ(_entity, _previousEntityPositionZ);
	_entity->GetComponent<BaseSprite>()->SetSize(_previousEntitySize);
	EntityHelper::SetEntityAngle(_entity, _previousEntityAngle);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef RESET_ENTITY_COMMAND_H
#define RESET_ENTITY_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class ResetEntityCommand : public Command
{
public:
	ResetEntityCommand(const SharedPtr<Entity> & entity);
	virtual ~ResetEntityCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;
	float _previousEntityPositionZ;
	Size _previousEntitySize;
	float _previousEntityAngle;	

	ResetEntityCommand(const ResetEntityCommand &) = delete;
	ResetEntityCommand & operator =(const ResetEntityCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RESET_ENTITY_COMMAND_H

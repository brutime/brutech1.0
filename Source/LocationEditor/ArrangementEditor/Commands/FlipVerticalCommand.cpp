//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FlipVerticalCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

FlipVerticalCommand::FlipVerticalCommand(const SharedPtr<Entity> & entity) :
	Command(true),
	_entity(entity)
{
}

//===========================================================================//

FlipVerticalCommand::~FlipVerticalCommand()
{
}

//===========================================================================//

void FlipVerticalCommand::Execute()
{
	EntityHelper::FlipEntityVertical(_entity);
}

//===========================================================================//

void FlipVerticalCommand::Cancel()
{
	EntityHelper::UnflipEntityVertical(_entity);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SelectLayerCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

SelectLayerCommand::SelectLayerCommand(const string & layerName) :
	Command(true),
	_layerName(layerName),
	_previousLayerName()
{
}

//===========================================================================//

SelectLayerCommand::~SelectLayerCommand()
{
}

//===========================================================================//

void SelectLayerCommand::Execute()
{	
	const auto & editor = LocationEditor->GetArrangementEditor();
	if (_previousLayerName.IsEmpty())
	{
		_previousLayerName = editor->GetSelectedLayerName();	
	}	
	editor->SelectLayer(_layerName);
}

//===========================================================================//

void SelectLayerCommand::Cancel()
{
	const auto & editor = LocationEditor->GetArrangementEditor();	
	if (!_previousLayerName.IsEmpty())
	{
		editor->SelectLayer(_previousLayerName);	
	}	
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

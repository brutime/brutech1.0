//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "DecrementRenderingOrderCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const float DecrementRenderingOrderCommand::IncrementStep = 1.0f;

//===========================================================================//

DecrementRenderingOrderCommand::DecrementRenderingOrderCommand(const SharedPtr<Entity> & entity) :
	Command(true),
	_entity(entity)
{
}

//===========================================================================//

DecrementRenderingOrderCommand::~DecrementRenderingOrderCommand()
{
}

//===========================================================================//

void DecrementRenderingOrderCommand::Execute()
{
	EntityHelper::SetEntityRenderingOrder(_entity, EntityHelper::GetEntityRenderingOrder(_entity) - IncrementStep);
}

//===========================================================================//

void DecrementRenderingOrderCommand::Cancel()
{
	EntityHelper::SetEntityRenderingOrder(_entity, EntityHelper::GetEntityRenderingOrder(_entity) + IncrementStep);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

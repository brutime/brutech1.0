//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef UNFLIP_HORIZONTAL_COMMAND_H
#define UNFLIP_HORIZONTAL_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class UnflipHorizontalCommand : public Command
{
public:
	UnflipHorizontalCommand(const SharedPtr<Entity> & entity);
	virtual ~UnflipHorizontalCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;

	UnflipHorizontalCommand(const UnflipHorizontalCommand &) = delete;
	UnflipHorizontalCommand & operator =(const UnflipHorizontalCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // UNFLIP_HORIZONTAL_COMMAND_H

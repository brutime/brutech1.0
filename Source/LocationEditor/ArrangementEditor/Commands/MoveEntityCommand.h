	//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef MOVE_ENTITY_COMMAND_H
#define MOVE_ENTITY_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class MoveEntityCommand : public Command
{
public:
	MoveEntityCommand(const SharedPtr<Entity> & entity, const Vector3 & previousEntityPosition);
	virtual ~MoveEntityCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;
	Vector3 _previousEntityPosition;	
	Vector3 _entityPosition;

	MoveEntityCommand(const MoveEntityCommand &) = delete;
	MoveEntityCommand & operator =(const MoveEntityCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // MOVE_ENTITY_COMMAND_H

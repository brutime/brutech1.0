//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef REMOVE_ENTITY_COMMAND_H
#define REMOVE_ENTITY_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class RemoveEntityCommand : public Command
{
public:
	RemoveEntityCommand(const SharedPtr<Entity> & entity);
	virtual ~RemoveEntityCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;
	string _pathToEntity;
	Vector3 _entityPosition;

	RemoveEntityCommand(const RemoveEntityCommand &) = delete;
	RemoveEntityCommand & operator =(const RemoveEntityCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // REMOVE_ENTITY_COMMAND_H

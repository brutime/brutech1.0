//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "AddEntityCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

AddEntityCommand::AddEntityCommand(float mouseX, float mouseY, const string & pathToEntity) :
	Command(true),
	_entity(),
	_pathToEntity(pathToEntity),
	_entityPosition(LocationEditor->GetMousePoint(mouseX, mouseY))
{
}

//===========================================================================//

AddEntityCommand::~AddEntityCommand()
{
}

//===========================================================================//

void AddEntityCommand::Execute()
{	
	const auto & editor = LocationEditor->GetArrangementEditor();	
	if (_entity == NullPtr)
	{
		_entity = editor->CreateEntity(_pathToEntity, _entityPosition);	
	}	
	else
	{
		editor->AddEntity(_entity);
	}	
}

//===========================================================================//

void AddEntityCommand::Cancel()
{
	const auto & editor = LocationEditor->GetArrangementEditor();	
	editor->RemoveEntity(_entity);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SELECT_LAYER_COMMAND_H
#define SELECT_LAYER_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class SelectLayerCommand : public Command
{
public:
	SelectLayerCommand(const string & layerName);
	virtual ~SelectLayerCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	string _layerName;
	string _previousLayerName;

	SelectLayerCommand(const SelectLayerCommand &) = delete;
	SelectLayerCommand & operator =(const SelectLayerCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // SELECT_LAYER_COMMAND_H

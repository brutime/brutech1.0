//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "MoveEntityCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

MoveEntityCommand::MoveEntityCommand(const SharedPtr<Entity> & entity, const Vector3 & previousEntityPosition) :
	Command(true),
	_entity(entity),
	_previousEntityPosition(previousEntityPosition),
	_entityPosition(_entity->GetComponent<PositioningNode>()->GetPosition())
{
}

//===========================================================================//

MoveEntityCommand::~MoveEntityCommand()
{
}

//===========================================================================//

void MoveEntityCommand::Execute()
{
	if (_entity->GetComponent<PositioningNode>()->GetPosition() != _entityPosition)
	{
		_entity->GetComponent<PositioningNode>()->SetPosition(_entityPosition);
	}	
}

//===========================================================================//

void MoveEntityCommand::Cancel()
{
	_entity->GetComponent<PositioningNode>()->SetPosition(_previousEntityPosition);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

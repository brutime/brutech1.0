//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ADD_ENTITY_COMMAND_H
#define ADD_ENTITY_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class AddEntityCommand : public Command
{
public:
	AddEntityCommand(float mouseX, float mouseY, const string & pathToEntity);
	virtual ~AddEntityCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;
	string _pathToEntity;
	Vector3 _entityPosition;

	AddEntityCommand(const AddEntityCommand &) = delete;
	AddEntityCommand & operator =(const AddEntityCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ADD_ENTITY_COMMAND_H

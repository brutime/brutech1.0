//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SetMinRenderingOrderCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const float SetMinRenderingOrderCommand::IncrementStep = 1.0f;

//===========================================================================//

SetMinRenderingOrderCommand::SetMinRenderingOrderCommand(const SharedPtr<Entity> & entity) :
	Command(true),
	_entity(entity),
	_previousEntityRenderingOrder(EntityHelper::GetEntityRenderingOrder(entity))
{
}

//===========================================================================//

SetMinRenderingOrderCommand::~SetMinRenderingOrderCommand()
{
}

//===========================================================================//

void SetMinRenderingOrderCommand::Execute()
{
	const auto & renderable = _entity->GetComponent<Renderable>();
	float minRenderingOrder = Renderer->GetMinRenderingOrderExceptRenderable(renderable);
	EntityHelper::SetEntityRenderingOrder(_entity, minRenderingOrder - IncrementStep);
}

//===========================================================================//

void SetMinRenderingOrderCommand::Cancel()
{
	EntityHelper::SetEntityRenderingOrder(_entity, _previousEntityRenderingOrder);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RemoveEntityCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

RemoveEntityCommand::RemoveEntityCommand(const SharedPtr<Entity> & entity) :
	Command(true),
	_entity(entity),
	_pathToEntity(EntityManager->GetPathToEntity(_entity->GetType())),
	_entityPosition(_entity->GetComponent<PositioningNode>()->GetPosition())
{
}

//===========================================================================//

RemoveEntityCommand::~RemoveEntityCommand()
{
}

//===========================================================================//

void RemoveEntityCommand::Execute()
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	editor->RemoveEntity(_entity);
}

//===========================================================================//

void RemoveEntityCommand::Cancel()
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	editor->AddEntity(_entity);	
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

	//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef RESIZE_ENTITY_COMMAND_H
#define RESIZE_ENTITY_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class ResizeEntityCommand : public Command
{
public:
	ResizeEntityCommand(const SharedPtr<Entity> & entity, const Size & previousEntitySize);
	virtual ~ResizeEntityCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;
	Size _previousEntitySize;
	Size _entitySize;

	ResizeEntityCommand(const ResizeEntityCommand &) = delete;
	ResizeEntityCommand & operator =(const ResizeEntityCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // RESIZE_ENTITY_COMMAND_H

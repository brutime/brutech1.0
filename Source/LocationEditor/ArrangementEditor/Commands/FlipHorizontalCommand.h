//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef FLIP_HORIZONTAL_COMMAND_H
#define FLIP_HORIZONTAL_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class FlipHorizontalCommand : public Command
{
public:
	FlipHorizontalCommand(const SharedPtr<Entity> & entity);
	virtual ~FlipHorizontalCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;

	FlipHorizontalCommand(const FlipHorizontalCommand &) = delete;
	FlipHorizontalCommand & operator =(const FlipHorizontalCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // FLIP_HORIZONTAL_COMMAND_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ResizeEntityCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

ResizeEntityCommand::ResizeEntityCommand(const SharedPtr<Entity> & entity, const Size & previousEntitySize) :
	Command(true),
	_entity(entity),
	_previousEntitySize(previousEntitySize),
	_entitySize(_entity->GetComponent<BaseSprite>()->GetSize())
{
}

//===========================================================================//

ResizeEntityCommand::~ResizeEntityCommand()
{
}

//===========================================================================//

void ResizeEntityCommand::Execute()
{
	if (_entity->GetComponent<BaseSprite>()->GetSize() != _entitySize)
	{
		_entity->GetComponent<BaseSprite>()->SetSize(_entitySize);
	}
}

//===========================================================================//

void ResizeEntityCommand::Cancel()
{
	_entity->GetComponent<BaseSprite>()->SetSize(_previousEntitySize);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

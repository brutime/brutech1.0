//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef ARRANGEMENT_EDITOR_COMMANDS_H
#define ARRANGEMENT_EDITOR_COMMANDS_H

#include "AddEntityCommand.h"
#include "DecrementRenderingOrderCommand.h"
#include "FlipHorizontalCommand.h"
#include "FlipVerticalCommand.h"
#include "IncrementRenderingOrderCommand.h"
#include "MoveEntityCommand.h"
#include "RemoveEntityCommand.h"
#include "ResetEntityCommand.h"
#include "ResizeEntityCommand.h"
#include "RotateEntityCommand.h"
#include "SelectLayerCommand.h"
#include "SetMaxRenderingOrderCommand.h"
#include "SetMinRenderingOrderCommand.h"
#include "UnflipHorizontalCommand.h"
#include "UnflipVerticalCommand.h"

#endif // ARRANGEMENT_EDITOR_COMMANDS_H
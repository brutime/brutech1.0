//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef SET_MAX_RENDERING_ORDER_COMMAND_H
#define SET_MAX_RENDERING_ORDER_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class SetMaxRenderingOrderCommand : public Command
{
public:
	SetMaxRenderingOrderCommand(const SharedPtr<Entity> & entity);
	virtual ~SetMaxRenderingOrderCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	static const float IncrementStep;

	SharedPtr<Entity> _entity;
	float _previousEntityRenderingOrder;

	SetMaxRenderingOrderCommand(const SetMaxRenderingOrderCommand &) = delete;
	SetMaxRenderingOrderCommand & operator =(const SetMaxRenderingOrderCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // SET_MAX_RENDERING_ORDER_COMMAND_H

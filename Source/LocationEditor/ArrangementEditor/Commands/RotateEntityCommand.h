	//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ROTATE_ENTITY_COMMAND_H
#define ROTATE_ENTITY_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class RotateEntityCommand : public Command
{
public:
	RotateEntityCommand(const SharedPtr<Entity> & entity, float previousEntityAngle);
	virtual ~RotateEntityCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	SharedPtr<Entity> _entity;
	float _previousEntityAngle;
	float _entityAngle;

	RotateEntityCommand(const RotateEntityCommand &) = delete;
	RotateEntityCommand & operator =(const RotateEntityCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ROTATE_ENTITY_COMMAND_H

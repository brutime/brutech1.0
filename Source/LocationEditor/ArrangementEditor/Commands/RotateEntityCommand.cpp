//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RotateEntityCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

RotateEntityCommand::RotateEntityCommand(const SharedPtr<Entity> & entity, float previousEntityAngle) :
	Command(true),
	_entity(entity),
	_previousEntityAngle(previousEntityAngle),
	_entityAngle(EntityHelper::GetEntityAngle(_entity))
{
}

//===========================================================================//

RotateEntityCommand::~RotateEntityCommand()
{
}

//===========================================================================//

void RotateEntityCommand::Execute()
{
	if (!Math::Equals(EntityHelper::GetEntityAngle(_entity), _entityAngle))
	{
		EntityHelper::SetEntityAngle(_entity, _entityAngle);
	}
}

//===========================================================================//

void RotateEntityCommand::Cancel()
{
	EntityHelper::SetEntityAngle(_entity, _previousEntityAngle);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

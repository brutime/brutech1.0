//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntitiesBrowser.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const string EntitiesBrowser::PathToEntitiesBrowser = "ArrangementEditor.EntitiesBrowser";
const string EntitiesBrowser::PathToFontName = PathToEntitiesBrowser + ".FontName";
const string EntitiesBrowser::PathToFontSize = PathToEntitiesBrowser + ".FontSize";
const string EntitiesBrowser::PathToFontColor = PathToEntitiesBrowser + ".FontColor";
const string EntitiesBrowser::PathToItemBackgroundColor = PathToEntitiesBrowser + ".ItemBackgroundColor";
const string EntitiesBrowser::PathToWidth = PathToEntitiesBrowser + ".Width";
const string EntitiesBrowser::PathToPadding = PathToEntitiesBrowser + ".Padding";
const string EntitiesBrowser::PathToItemHeight = PathToEntitiesBrowser + ".ItemHeight";
const string EntitiesBrowser::PathToItemsVerticalPadding = PathToEntitiesBrowser + ".ItemsVerticalPadding";
const string EntitiesBrowser::PathToButtonSize = PathToEntitiesBrowser + ".ButtonSize";
const string EntitiesBrowser::PathToAddingEntitySpriteAlpha = PathToEntitiesBrowser + ".AddingEntitySpriteAlpha";

//===========================================================================//

EntitiesBrowser::EntitiesBrowser(const string & domainName, const string & layerName) :
	Frame(domainName, layerName,
	      static_cast<integer>(Graphics->GetViewport().Width * EditorConfig->Get<float>(PathToWidth)),
	      Graphics->GetViewport().Height, EditorConfig->Get<Color>(ArrangementEditor::PathToBackgroundColor)),
	AddingEntitySpriteAlpha(EditorConfig->Get<float>(PathToAddingEntitySpriteAlpha)),
	ItemBackgroundColor(EditorConfig->Get<Color>(PathToItemBackgroundColor)),
	Padding(EditorConfig->Get<Vector2>(PathToPadding)),
	SourceItemHeight(EditorConfig->Get<float>(PathToItemHeight)),
	ItemsVerticalPadding(EditorConfig->Get<float>(PathToItemsVerticalPadding)),
	ButtonSize(EditorConfig->Get<Vector2>(PathToButtonSize)),
	_itemWidth(GetSize().X - 2.0f * Padding.X),
	_itemHeight(Renderer->PixelsToCoords(Renderer->GetLayer(_domainName, _layerName)->GetProjectionMode(), SourceItemHeight)),
	_hintSize(),
	_itemsPerPage(0),
	_currentPageIndex(0),
	_items(),
	_backButton(),
	_pageLabel(),
	_nextButton(),
	_entityGeneratedEventHandler(new EventHandler<const EmptyEventArgs &>(this, &EntitiesBrowser::OnEntityGenerated))
{
	MoveTo(static_cast<integer>(Graphics->GetViewport().Width * (1 - EditorConfig->Get<float>(PathToWidth))) + 1, 0);

	const auto & font = FontManager->Get(EditorConfig->Get<string>(ArrangementEditor::PathToButtonsFontName),
	                                     EditorConfig->Get<integer>(ArrangementEditor::PathToButtonsFontSize));
	const Color fontColor = EditorConfig->Get<Color>(ArrangementEditor::PathToButtonsFontColor);

	const Color normalColor = EditorConfig->Get<Color>(ArrangementEditor::PathToButtonNormalColor);
	const Color pressedColor = EditorConfig->Get<Color>(ArrangementEditor::PathToButtonPressedColor);
	const Color hoveredColor = EditorConfig->Get<Color>(ArrangementEditor::PathToButtonHoveredColor);

	_backButton = new LabeledButton(_domainName, _layerName, ButtonSize.X, ButtonSize.Y,
	                                font, fontColor, string::Empty, string::Empty, string::Empty,
	                                normalColor, pressedColor, hoveredColor);
	_backButton->SetText("<");
	AddChild(_backButton);

	_pageLabel = new Label(_domainName, _layerName, "", FontManager->Get(EditorConfig->Get<string>(PathToFontName),
	                                                                     EditorConfig->Get<integer>(PathToFontSize)), Colors::White, Vector2(), WordWrapMode::Simple,
	                       TextHorizontalAlignment::Center, TextVerticalAlignment::Center);
	AddChild(_pageLabel);

	_nextButton = new LabeledButton(_domainName, _layerName, ButtonSize.X, ButtonSize.Y,
	                                font, fontColor, string::Empty, string::Empty, string::Empty,
	                                normalColor, pressedColor, hoveredColor);
	_nextButton->SetText(">");
	AddChild(_nextButton);

	_backButton->MoveTo(Padding.X, Padding.Y);
	_pageLabel->MoveTo(GetSize().X * 0.5f, Padding.Y + _backButton->GetSize().Y * 0.5f);
	_nextButton->MoveTo(GetSize().X - _nextButton->GetSize().X - Padding.X, Padding.Y);

	_backButton->Click.AddHandler(new EventHandler<const Widget *, const MouseEventArgs &>(this, &EntitiesBrowser::OnBackButtonClick));
	_nextButton->Click.AddHandler(new EventHandler<const Widget *, const MouseEventArgs &>(this, &EntitiesBrowser::OnNextButtonClick));

	_itemsPerPage = Math::Floor((GetSize().Y - GetNotItemsSpaceHeight()) / (_itemHeight + ItemsVerticalPadding));

	_hintSize = {GetSize().X - 2 * Padding.X, 0.0f};
	_hintSize /= Renderer->GetPixelsToCoordsFactors(Renderer->GetLayer(_domainName, _layerName)->GetProjectionMode());

	LoadItems();
	UpdateCurrentPage();

	EntityGenerator->EntityGenerated.AddHandler(_entityGeneratedEventHandler);
}

//===========================================================================//

EntitiesBrowser::~EntitiesBrowser()
{
	EntityGenerator->EntityGenerated.RemoveHandler(_entityGeneratedEventHandler);
}

//===========================================================================//

void EntitiesBrowser::SetVisible(bool flag)
{
	Frame::SetVisible(flag);
	
	for (integer i = 0; i < _items.GetCount(); ++i)
	{
		const auto & item = _items[i];
		if (IsInCurrentPage(i))
		{
			item->SetVisible(flag);
		}
		else
		{
			item->Hide();
		}
	}	
}

//===========================================================================//

SharedPtr<EntitiesBrowserItem> EntitiesBrowser::GetItem(integer mouseX, integer mouseY) const
{
	integer itemsCount = Math::Min(_itemsPerPage, _items.GetCount() - _itemsPerPage * _currentPageIndex);
	for (integer i = 0; i < itemsCount; ++i)
	{
		const auto & item = _items[_currentPageIndex * _itemsPerPage + i];
		if (item->GetBackground()->ContainsPoint(mouseX, mouseY, true))
		{
			return item;
		}
	}

	return NullPtr;
}

//===========================================================================//

void EntitiesBrowser::LoadItems()
{
	Array<string> filter = { "Decorations", "Units" };
	Array<string> entityNames = EntityHelper::GetEntitiesFilesPathes(filter);

	const auto & font = FontManager->Get(EditorConfig->Get<string>(PathToFontName), EditorConfig->Get<integer>(PathToFontSize));
	const Color fontColor = EditorConfig->Get<Color>(PathToFontColor);

	for (const string & entityName : entityNames)
	{
		try
		{
			string path = entityName.Substring(0, entityName.LastIndexOf("."));
			SharedPtr<EntitiesBrowserItem> item = new EntitiesBrowserItem(_domainName, _layerName, path,
			                                                              _itemWidth, _itemHeight, _hintSize,
			                                                              font, fontColor, ItemBackgroundColor);
			AddChild(item->GetBackground());
			_items.Add(item);
		}
		catch (Exception & exception)
		{
			Console->Debug(string::Format("Error loading entity {0}: {1}", entityName, exception.GetDetails()));
		}
	}
}

//===========================================================================//

void EntitiesBrowser::UpdateCurrentPage()
{
	UpdateCurrentPageItems();
	UpdateCurrentPageUI();
}

//===========================================================================//

void EntitiesBrowser::UpdateCurrentPageItems()
{
	float currentItemPosition = GetSize().Y - Padding.Y - _itemHeight;
	for (integer i = 0; i < _items.GetCount(); ++i)
	{
		const auto & item = _items[i];
		if (IsInCurrentPage(i))
		{
			item->GetBackground()->MoveTo(Padding.X, currentItemPosition);
			item->Show();
			currentItemPosition -= _itemHeight + ItemsVerticalPadding;
		}
		else
		{
			item->Hide();
		}
	}
}

//===========================================================================//

void EntitiesBrowser::UpdateCurrentPageUI()
{
	integer pagesCount = static_cast<integer>(Math::Ceiling(static_cast<float>(_items.GetCount()) / _itemsPerPage));
	integer currentPageNumber = static_cast<integer>(Math::Min(_currentPageIndex + 1, pagesCount));
	_pageLabel->SetText(string::Format("{0}/{1}", currentPageNumber, pagesCount));
	if (_currentPageIndex == 0)
	{
		_backButton->Disable();
	}
	else
	{
		_backButton->Enable();
	}

	if (_items.GetCount() > _itemsPerPage * (_currentPageIndex + 1))
	{
		_nextButton->Enable();
	}
	else
	{
		_nextButton->Disable();
	}
}

//===========================================================================//

bool EntitiesBrowser::IsInCurrentPage(integer itemIndex) const
{
	integer pageStartIndex = _currentPageIndex * _itemsPerPage;
	integer pageEndIndex = Math::Min((_currentPageIndex + 1) * _itemsPerPage, _items.GetCount()) - 1;
	return pageStartIndex <= itemIndex && itemIndex <= pageEndIndex;
}

//===========================================================================//

void EntitiesBrowser::OnNextButtonClick(const Widget *, const MouseEventArgs &)
{
	_currentPageIndex++;
	UpdateCurrentPage();
}

//===========================================================================//

void EntitiesBrowser::OnBackButtonClick(const Widget *, const MouseEventArgs &)
{
	_currentPageIndex--;
	UpdateCurrentPage();
}

//===========================================================================//

void EntitiesBrowser::OnEntityGenerated(const EmptyEventArgs &)
{
	_items.Clear();
	LoadItems();
	UpdateCurrentPage();
}

//===========================================================================//

float EntitiesBrowser::GetNotItemsSpaceHeight() const
{
	return _nextButton->GetSize().Y + Padding.Y * 2.0f;
}

//===========================================================================//

}         // namespace Bru

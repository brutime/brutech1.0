//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LayerBrowserItem.h"

#include "../EditorConfig.h"
#include "ArrangementEditor.h"

namespace Bru
{

//===========================================================================//

LayerBrowserItem::LayerBrowserItem(const string & domainName, const string & layerName, float width, float height,
                                   const Vector2 & padding, const WeakPtr<Font> & font, const Color & fontColor,
                                   const Color & backgroundColor, const string & caption, integer tag) :
	Frame(domainName, layerName, width, height, backgroundColor),
	_normalColor(EditorConfig->Get<Color>(ArrangementEditor::PathToButtonNormalColor)),
	_pressedColor(EditorConfig->Get<Color>(ArrangementEditor::PathToButtonPressedColor)),
	_hoveredColor(EditorConfig->Get<Color>(ArrangementEditor::PathToButtonHoveredColor)),
	_visibilityButton(),
	_layerButton(),
	_isVisibilityEnabled(true),
	_isSelected(false)
{
	//Делаем кнопку квадратной
	const float visibleButtonHeight = height - padding.Y * 2.0f;
	const float heightFactor = Surface->GetHeight() / static_cast<float>(Surface->GetWidth());
	const float visibleButtonWidth = visibleButtonHeight * heightFactor;
	const string pathToVisibilityButtonIcon = "Editor/VisibilityButtonIcon";
	_visibilityButton = new Button(domainName, layerName, visibleButtonWidth, visibleButtonHeight,
	                               pathToVisibilityButtonIcon, pathToVisibilityButtonIcon, pathToVisibilityButtonIcon,
	                               _normalColor, _pressedColor, _hoveredColor);
	_visibilityButton->MoveTo(padding.X, padding.Y);
	_visibilityButton->SetTag(tag);
	AddChild(_visibilityButton);
	
	const float layerButtonWidth = width - visibleButtonWidth - padding.X * 3.0f;
	const float layerButtonHeight = height - padding.Y * 2.0f;	
	_layerButton = new LabeledButton(domainName, layerName, layerButtonWidth, layerButtonHeight,
	                                 font, fontColor, string::Empty, string::Empty, string::Empty,
	                                 backgroundColor, backgroundColor, _hoveredColor);
	_layerButton->SetText(caption);
	_layerButton->MoveTo(visibleButtonWidth + padding.X * 2.0f, padding.Y);
	_layerButton->SetTag(tag);
	AddChild(_layerButton);

	SetTag(tag);
}

//===========================================================================//

LayerBrowserItem::~LayerBrowserItem()
{
}

//===========================================================================//

void LayerBrowserItem::Select()
{	
	_layerButton->SetNormalColor(_normalColor);
	_layerButton->SetPressedColor(_pressedColor);
	_layerButton->SetHoveredColor(_hoveredColor);	

	_isSelected = true;
}

//===========================================================================//

void LayerBrowserItem::UnSelect()
{
	const auto & backgroundColor = GetColor();
	_layerButton->SetNormalColor(backgroundColor);
	_layerButton->SetPressedColor(backgroundColor);
//	_layerButton->SetHoveredColor(_hoveredColor);
	_isSelected = false;
}

//===========================================================================//

bool LayerBrowserItem::IsSelected() const
{
	return _isSelected;
}

//===========================================================================//

void LayerBrowserItem::EnableVisibility()
{
	_visibilityButton->SetNormalColor(_normalColor);
	_visibilityButton->SetPressedColor(_pressedColor);
	_visibilityButton->SetHoveredColor(_hoveredColor);
	_isVisibilityEnabled = true;
}

//===========================================================================//

void LayerBrowserItem::DisableVisibility()
{
	const auto & backgroundColor = GetColor();	
	_visibilityButton->SetNormalColor(backgroundColor);
	_visibilityButton->SetPressedColor(backgroundColor);
	_visibilityButton->SetHoveredColor(backgroundColor);	
	_isVisibilityEnabled = false;
}

//===========================================================================//

bool LayerBrowserItem::IsVisibilityEnabled() const
{
	return _isVisibilityEnabled;
}

//===========================================================================//

const SharedPtr<Button> & LayerBrowserItem::GetVisibilityButton() const
{
	return _visibilityButton;
}

//===========================================================================//

const SharedPtr<LabeledButton> & LayerBrowserItem::GetLayerButton() const
{
	return _layerButton;
}

//===========================================================================//

} // namespace Bru

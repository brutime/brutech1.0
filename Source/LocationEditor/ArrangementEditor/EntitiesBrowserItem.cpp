//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "EntitiesBrowserItem.h"

namespace Bru
{

//===========================================================================//

EntitiesBrowserItem::EntitiesBrowserItem(const string & domainName, const string & layerName, const string & path,
                                         float width, float height, const Vector2 & hintSize,
                                         const WeakPtr<Font> & font, const Color & fontColor, const Color & backgroundColor) :
	_path(path),
	_background(),
	_iconNode(),
	_icon(),
	_hintNode(),
	_hint()
{
	_background = new Frame(domainName, layerName, width, height, backgroundColor);

	_icon = EntityManager->CreateEntityBaseSprite(_path, domainName, layerName);
	if (HasIcon())
	{
		float widthFactor = Renderer->GetSurfaceCoordsHeight() / Renderer->GetSurfaceCoordsWidth();
		_icon->SetWidth(_icon->GetWidth() * widthFactor);
		float sizeFactor = 1.0f;
		if (_icon->GetWidth() / _icon->GetHeight() > _background->GetSize().X / _background->GetSize().Y)
		{
			sizeFactor = _background->GetSize().X / _icon->GetWidth();
		}
		else
		{
			sizeFactor = _background->GetSize().Y / _icon->GetHeight();
		}

		_icon->SetSize(_icon->GetWidth() * sizeFactor, _icon->GetHeight() * sizeFactor);

		_iconNode = new PositioningNode(domainName);
		_background->GetPositioningNode()->AddChild(_iconNode);
		_iconNode->SetPosition(_background->GetSize().X * 0.5f, _background->GetSize().Y * 0.5f, 0.0f);
		_icon->SetPositioningNode(_iconNode);
	}

	Vector2 hintBounds = hintSize;
	_hint = new RenderableText(domainName, layerName, _path, font, hintBounds, fontColor,
	                           WordWrapMode::Any, TextHorizontalAlignment::Center, TextVerticalAlignment::Top);
	_hintNode = new PositioningNode(domainName);
	_background->GetPositioningNode()->AddChild(_hintNode);
	_hintNode->SetPosition(_background->GetSize().X * 0.5f, -0.5f, 0.0f);
	_hint->SetPositioningNode(_hintNode);

	Hide();
}

//===========================================================================//

EntitiesBrowserItem::~EntitiesBrowserItem()
{
}

//===========================================================================//

bool EntitiesBrowserItem::HasIcon() const
{
	return _icon != NullPtr;
}

//===========================================================================//

void EntitiesBrowserItem::Show()
{
	SetVisible(true);
}

//===========================================================================//

void EntitiesBrowserItem::Hide()
{
	SetVisible(false);
}

//===========================================================================//

void EntitiesBrowserItem::SetVisible(bool flag)
{
	_background->SetVisible(flag);
	if (HasIcon())
	{
		_icon->SetVisible(flag);
	}
	_hint->SetVisible(flag);
}

//===========================================================================//

bool EntitiesBrowserItem::IsVisible() const
{
	return _background->IsVisible();
}

//===========================================================================//

const string & EntitiesBrowserItem::GetPath() const
{
	return _path;
}

//===========================================================================//

const SharedPtr<Frame> & EntitiesBrowserItem::GetBackground() const
{
	return _background;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "SelectedEntityVisualizationInfo.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const float SelectedEntityVisualizationInfo::RotationAnchorOffset = 1.5f;

//===========================================================================//

SelectedEntityVisualizationInfo::SelectedEntityVisualizationInfo(const string & domainName, const string & layerName,
                                                                 const WeakPtr<Entity> & targetEntity) :
	EntityVisualizationInfo(domainName, layerName, targetEntity, EditorConfig->Get<Color>(ArrangementEditor::PathToSelectedEntityFrameColor)),
	_rotationAnchorRadius(),
	_rotationAnchor(),
	_sizeAnchorSide(),
	_sizeAnchors(),
	_transformationChangedHandler(new EventHandler<const EmptyEventArgs &>(this, &SelectedEntityVisualizationInfo::OnTargetTransformationChanged))
{
	const ProjectionMode projectionMode = Renderer->GetLayer(GetDomainName(), GetLayerName())->GetProjectionMode();
	const float rotationAnchorRadiusInPixels = EditorConfig->Get<float>(ArrangementEditor::PathToSelectedEntityRotationAnchorRadius);
	_rotationAnchorRadius = Renderer->PixelsToCoords(projectionMode, rotationAnchorRadiusInPixels);

	const float sizeAnchorSide = EditorConfig->Get<float>(ArrangementEditor::PathToSelectedEntitySizeAnchorsSide);
	_sizeAnchorSide = Renderer->PixelsToCoords(projectionMode, sizeAnchorSide);

	const auto & node = GetPositioningNodeFromTargetEntity();

	Color anchorsColor = EditorConfig->Get<Color>(ArrangementEditor::PathToSelectedEntityAnchorsColor);
	if (node->GetTypeID() == RotationNode::TypeID)
	{
		_rotationAnchor = new RenderableCircle(GetDomainName(), GetLayerName(), {}, _rotationAnchorRadius, true, anchorsColor);
		_rotationAnchor->SetPositioningNode(node);
	}

	_sizeAnchors.Resize(8);
	for (integer i = 0; i < _sizeAnchors.GetCount(); ++i)
	{
		_sizeAnchors[i] = new RenderableRectangle(GetDomainName(), GetLayerName(), {}, {}, true, anchorsColor);
		_sizeAnchors[i]->SetPositioningNode(node);
		_sizeAnchors[i]->SetOriginFactor({0.5f, 0.5f});
	}
	UpdateSizeAnchors();

	node->TransformationChanged.AddHandler(_transformationChangedHandler);
	LocationEditor->GetLocationObserver()->GetCamera()->TransformationChanged.AddHandler(_transformationChangedHandler);
}

//===========================================================================//

SelectedEntityVisualizationInfo::~SelectedEntityVisualizationInfo()
{
	LocationEditor->GetLocationObserver()->GetCamera()->TransformationChanged.RemoveHandler(_transformationChangedHandler);
	GetPositioningNodeFromTargetEntity()->TransformationChanged.RemoveHandler(_transformationChangedHandler);
}

//===========================================================================//

void SelectedEntityVisualizationInfo::SetVisible(bool isVisible)
{
	for (const auto & sizeAnchor : _sizeAnchors)
	{
		sizeAnchor->SetVisible(isVisible);
	}
	_rotationAnchor->SetVisible(isVisible);
	EntityVisualizationInfo::SetVisible(isVisible);
}

//===========================================================================//

float SelectedEntityVisualizationInfo::GetRotationAnchorRadius() const
{
	return _rotationAnchorRadius;
}

//===========================================================================//

float SelectedEntityVisualizationInfo::GetRotationAnchorProjectRadius() const
{
	return GetCurrentProjection(_rotationAnchorRadius);
}

//===========================================================================//

const SharedPtr<RenderableCircle> & SelectedEntityVisualizationInfo::GetRotationAnchor() const
{
	return _rotationAnchor;
}

//===========================================================================//

float SelectedEntityVisualizationInfo::GetSizeAnchorsSide() const
{
	return _sizeAnchorSide;
}

//===========================================================================//

float SelectedEntityVisualizationInfo::GetSizeAnchorsProjectSide() const
{
	return GetCurrentProjection(_sizeAnchorSide);
}

//===========================================================================//

const Array<SharedPtr<RenderableRectangle>> & SelectedEntityVisualizationInfo::GetSizeAnchors() const
{
	return _sizeAnchors;
}

//===========================================================================//

void SelectedEntityVisualizationInfo::OnTargetSizeChanged(const EmptyEventArgs & args)
{
	UpdateSizeAnchors();
	EntityVisualizationInfo::OnTargetSizeChanged(args);
}

//===========================================================================//

void SelectedEntityVisualizationInfo::OnTargetTransformationChanged(const EmptyEventArgs &)
{
	UpdateSizeAnchors();
}

//===========================================================================//

void SelectedEntityVisualizationInfo::UpdateSizeAnchors()
{
	const auto & sprite = GetTargetEntity()->GetComponent<BaseSprite>();

	if (_rotationAnchor != NullPtr)
	{
		const float rotationAnchorProjectRadius = GetRotationAnchorProjectRadius();
		_rotationAnchor->SetPosition({0.0f, sprite->GetHeight() * 0.5f + RotationAnchorOffset});
		_rotationAnchor->SetRadius(rotationAnchorProjectRadius);
	}

	const Vector3 position = -sprite->GetOrigin();
	const float sizeAnchorsProjectSide = GetSizeAnchorsProjectSide();
	const Vector3 anchorsSides = {sizeAnchorsProjectSide, sizeAnchorsProjectSide};

	const Array<Vector3> coords =
	{
		{},
		{sprite->GetWidth() * 0.5f, 0.0f},
		{sprite->GetWidth(), 0.0f},
		{sprite->GetWidth(), sprite->GetHeight() * 0.5f},
		{sprite->GetWidth(), sprite->GetHeight()},
		{sprite->GetWidth() * 0.5f, sprite->GetHeight()},
		{0.0f, sprite->GetHeight()},
		{0.0f, sprite->GetHeight() * 0.5f}
	};

	for (integer i = 0; i < _sizeAnchors.GetCount(); ++i)
	{
		_sizeAnchors[i]->SetCoords(position + coords[i], position + coords[i] + anchorsSides);
	}
}

//===========================================================================//

float SelectedEntityVisualizationInfo::GetCurrentProjection(float value) const
{
	const auto & node = GetPositioningNodeFromTargetEntity();
	const float distance = LocationEditor->GetLocationObserver()->GetCamera()->GetPosition().Z - node->GetPositionZ();
	return CoordsHelper::GetProjection({value, value}, distance, node->GetPositionZ()).Y;
}

//===========================================================================//

} // namespace Bru

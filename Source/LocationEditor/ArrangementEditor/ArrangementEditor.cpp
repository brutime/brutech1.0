//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ArrangementEditor.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const string ArrangementEditor::UILayerName = "UI";
const string ArrangementEditor::AddingEntityUILayerName = "AddingEntityUI";

const string ArrangementEditor::PathToArrangementEditor = "ArrangementEditor";
const string ArrangementEditor::PathToIsNavigationMeshVisible = PathToArrangementEditor + ".IsNavigationMeshVisible";

const string ArrangementEditor::PathToEntitiesFrame = PathToArrangementEditor + ".EntitiesFrame";
const string ArrangementEditor::PathToEntitiesFrameColor = PathToEntitiesFrame + ".Color";
const string ArrangementEditor::PathToEntitiesFrameLineThickness = PathToEntitiesFrame + ".LineThickness";
const string ArrangementEditor::PathToSelectedEntity = PathToArrangementEditor + ".SelectedEntity";
const string ArrangementEditor::PathToSelectedEntityInfoLabelFontName = PathToSelectedEntity + ".InfoLabelFontName";
const string ArrangementEditor::PathToSelectedEntityInfoLabelFontSize = PathToSelectedEntity + ".InfoLabelFontSize";
const string ArrangementEditor::PathToSelectedEntityFrameColor = PathToSelectedEntity + ".FrameColor";
const string ArrangementEditor::PathToSelectedEntityAnchorsColor = PathToSelectedEntity + ".AnchorsColor";
const string ArrangementEditor::PathToSelectedEntityRotationAnchorRadius = PathToSelectedEntity + ".RotationAnchorRadius";
const string ArrangementEditor::PathToSelectedEntitySizeAnchorsSide = PathToSelectedEntity + ".SizeAnchorsSide";

const string ArrangementEditor::PathToCommonColors = PathToArrangementEditor + ".CommonColors";
const string ArrangementEditor::PathToBackgroundColor = PathToCommonColors + ".Background";
const string ArrangementEditor::PathToButtonNormalColor = PathToCommonColors + ".ButtonNormal";
const string ArrangementEditor::PathToButtonPressedColor = PathToCommonColors + ".ButtonPressed";
const string ArrangementEditor::PathToButtonHoveredColor = PathToCommonColors + ".ButtonHovered";

const string ArrangementEditor::PathToButtonsFont = PathToArrangementEditor + ".ButtonsFont";
const string ArrangementEditor::PathToButtonsFontName = PathToButtonsFont + ".Name";
const string ArrangementEditor::PathToButtonsFontSize = PathToButtonsFont + ".Size";
const string ArrangementEditor::PathToButtonsFontColor = PathToButtonsFont + ".Color";

const float ArrangementEditor::EntityMovingSmallStep = 0.1f;
const float ArrangementEditor::EntityMovingLargeStep = 1.0f;
const float ArrangementEditor::EntityRotationSmallStep = 1.0f;
const float ArrangementEditor::EntityRotationLargeStep = 15.0f;
const float ArrangementEditor::EntityResizingSmallStep = 0.1f;
const float ArrangementEditor::EntityResizingLargeStep = 1.0f;

const float ArrangementEditor::EntityZoomStep = 2.0f;
const float ArrangementEditor::MinSize = 0.3f;

const float ArrangementEditor::EntityZoomFactor = 1.5f;

//===========================================================================//

ArrangementEditor::ArrangementEditor(const string & domainName, const string & gameplayLayerName) : ILocationEditorState(),
	_domainName(domainName),
	_gameplayLayerName(gameplayLayerName),
	_selectedEntity(),
	_selectedLayerName(),
	_entitiesVisualizationInfos(),
	_selectedEntityVisualizationInfo(),
	_selectedEntityInfoLabel(),
	_entitiesBrowser(),
	_layerBrowser(),
	_inputReceiver(),
	_commandsManager(new CommandsManager(500)),
	_layerVisibleChangedHandler(new EventHandler<const LayerEventArgs &>(this, &ArrangementEditor::OnLayerVisibleChanged)),
	_entityCreatedEventHandler(new EventHandler<const EntityManagerEventArgs &>(this, &ArrangementEditor::OnEntityCreated)),
	_entityRemovedEventHandler(new EventHandler<const EntityManagerEventArgs &>(this, &ArrangementEditor::OnEntityRemoved)),
	_selectedEntityPositioningNodeTransformationChangedEventHandler(new EventHandler<const EmptyEventArgs &>(this, &ArrangementEditor::OnSelectedEntityPositioningNodeTransformationChanged)),
	_selectedEntityRotationNodeTransformationChangedEventHandler(new EventHandler<const EmptyEventArgs &>(this, &ArrangementEditor::OnSelectedEntityRotationNodeTransformationChanged)),
	_selectedEntityRenderingOrderChangedEventHandler(new EventHandler<const EmptyEventArgs &>(this, &ArrangementEditor::OnSelectedEntityRenderingOrderChanged)),
	_selectedEntitySizeChangedEventHandler(new EventHandler<const EmptyEventArgs &>(this, &ArrangementEditor::OnSelectedEntitySizeChanged)),
	_layerChanged(),
	LayerChanged(_layerChanged)
{
}

//===========================================================================//

ArrangementEditor::~ArrangementEditor()
{
	_commandsManager = NullPtr;
}

//===========================================================================//

void ArrangementEditor::Select()
{
	AddLayers();

	UpdateNavigationMeshVisibility();

	CreateSelectedEntityInfoLabel();
	_entitiesBrowser = new EntitiesBrowser(_domainName, ArrangementEditor::UILayerName);
	CreateLayerBrowser();

	Renderer->LayerVisibleChangedEvent.AddHandler(_layerVisibleChangedHandler);

	EntityManager->EntityCreatedEvent.AddHandler(_entityCreatedEventHandler);
	EntityManager->EntityRemovedEvent.AddHandler(_entityRemovedEventHandler);

	_inputReceiver = new ArrangementEntitiesInputReceiver(_domainName);
}

//===========================================================================//

void ArrangementEditor::Leave()
{
	_inputReceiver = NullPtr;

	Renderer->LayerVisibleChangedEvent.RemoveHandler(_layerVisibleChangedHandler);

	EntityManager->EntityCreatedEvent.RemoveHandler(_entityCreatedEventHandler);
	EntityManager->EntityRemovedEvent.RemoveHandler(_entityRemovedEventHandler);
	
	EditorConfig->Set<bool>(PathToIsNavigationMeshVisible, LocationEditor->IsNavigationMeshVisible());	

	UnselectEntity();
	_selectedLayerName = string::Empty;
	_entitiesVisualizationInfos.Clear();

	_layerBrowser = NullPtr;
	_entitiesBrowser = NullPtr;
	_selectedEntityInfoLabel = NullPtr;

	LocationEditor->HideNavigationMesh();

	RemoveLayers();
}

//===========================================================================//

void ArrangementEditor::EnableInput()
{
	_inputReceiver->Enable();
}

//===========================================================================//

void ArrangementEditor::DisableInput()
{
	_inputReceiver->Disable();
}

//===========================================================================//

void ArrangementEditor::OnFileClosed()
{
	_commandsManager->Clear();
}

//===========================================================================//

void ArrangementEditor::SelectEntity(const WeakPtr<Entity> & selectedEntity)
{
	if (!selectedEntity->ContainsComponent(Renderable::TypeID) || !IsEntitySuitableForSelectedLayer(selectedEntity))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Trying to select not valid entity: {0}", selectedEntity->GetName()));
	}

	if (selectedEntity == _selectedEntity)
	{
		return;
	}

	if (_selectedEntity != NullPtr)
	{
		RemoveHandlersFromEntity(_selectedEntity);
		_entitiesVisualizationInfos.Add(_selectedEntity, new EntityVisualizationInfo(
		                                    _domainName, LocationEditorSingleton::HUDLayerName, _selectedEntity));
	}

	_selectedEntity = selectedEntity;

	_entitiesVisualizationInfos.Remove(_selectedEntity);
	_selectedEntityVisualizationInfo = new SelectedEntityVisualizationInfo(
	    _domainName, LocationEditorSingleton::HUDLayerName, _selectedEntity);
	AddHandlersToEntity(_selectedEntity);
	UpdateSelectedEntityInfoLabel();
}

//===========================================================================//

void ArrangementEditor::UnselectEntity(bool hard)
{
	if (_selectedEntity != NullPtr)
	{
		RemoveHandlersFromEntity(_selectedEntity);
		if (!hard)
		{
			_entitiesVisualizationInfos.Add(_selectedEntity, new EntityVisualizationInfo(
			                                    _domainName, LocationEditorSingleton::HUDLayerName, _selectedEntity));
		}
	}

	_selectedEntityVisualizationInfo = NullPtr;
	_selectedEntity = NullPtr;
	UpdateSelectedEntityInfoLabel();
}

//===========================================================================//

WeakPtr<Entity> ArrangementEditor::GetSelectedEntity() const
{
	return _selectedEntity;
}

//===========================================================================//

void ArrangementEditor::SelectLayer(const string & selectingLayerName)
{
	if (selectingLayerName == _selectedLayerName)
	{
		return;
	}

	UnselectEntity();

	if (!_selectedLayerName.IsEmpty())
	{
		_entitiesVisualizationInfos.Clear();
	}

	for (const string & groupName : EntityManager->GetSavingGroupsNames())
	{
		const List<SharedPtr<Entity>> & entities = EntityManager->GetGroup(groupName);
		for (const auto & entity : entities)
		{
			if (EntityHelper::GetEntityLayerName(entity) == selectingLayerName)
			{
				SharedPtr<EntityVisualizationInfo> entityVisualizationInfo = new EntityVisualizationInfo(
				    _domainName, LocationEditorSingleton::HUDLayerName, entity);
				_entitiesVisualizationInfos.Add(entity, entityVisualizationInfo);
			}
		}
	}

	_selectedLayerName = selectingLayerName;
	_layerChanged.Fire(_selectedLayerName);
}

//===========================================================================//

const string & ArrangementEditor::GetSelectedLayerName() const
{
	return _selectedLayerName;
}

//===========================================================================//

WeakPtr<Entity> ArrangementEditor::GetUpperEntity(float mouseX, float mouseY) const
{
	WeakPtr<Entity> upperEntity;

	for (const string & groupName : EntityManager->GetSavingGroupsNames())
	{
		const auto & entities = EntityManager->GetGroup(groupName);
		for (const auto & entity : entities)
		{
			//Пропускаем entity, если у нее нет компонента визуализации
			if (!entity->ContainsComponent(Renderable::TypeID))
			{
				continue;
			}

			//Пропускаем entity, если она не в выбранном слое, или не видим
			if (EntityHelper::GetEntityLayerName(entity) != _selectedLayerName ||
			    !EntityHelper::IsEntityVisible(entity))
			{
				continue;
			}

			//Пропускаем entity, если она "не попадает в объектив"
			float cameraZ = LocationEditor->GetLocationObserver()->GetCamera()->GetPosition().Z;
			const auto & node = entity->GetComponent<PositioningNode>();
			if (node->GetPosition().Z > cameraZ)
			{
				continue;
			}

			Vector3 mousePoint = LocationEditor->GetMousePoint(mouseX, mouseY, node->GetPositionZ());

			if (EntityHelper::IsEntityContainsPoint(entity, mousePoint))
			{
				if (upperEntity == NullPtr)
				{
					upperEntity = entity;
				}
				else
				{
					const auto & renderable = entity->GetComponent<Renderable>();
					const auto & currentRenderable = upperEntity->GetComponent<Renderable>();
					if (Renderer->WhichIsNearest(renderable, currentRenderable) == renderable)
					{
						upperEntity = entity;
					}
				}
			}
		}
	}

	return upperEntity;
}

//===========================================================================//

bool ArrangementEditor::IsEntityRotationAnchorContainsPoint(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const
{
	if (entity == NullPtr || _selectedEntityVisualizationInfo->GetRotationAnchor() == NullPtr)
	{
		return false;
	}

	const auto & node = entity->GetComponent<PositioningNode>();
	const auto & rotationAnchor = _selectedEntityVisualizationInfo->GetRotationAnchor();

	Vector3 mousePoint = LocationEditor->GetMousePoint(mouseX, mouseY, node->GetPositionZ());
	Vector3 adaptedPoint = CoordsHelper::GetProjectedOnEntityPoint(entity, mousePoint);

	Vector3 rotationAnchorWorldPosition = node->GetPosition() + rotationAnchor->GetPosition();

	return adaptedPoint.DistanceTo(rotationAnchorWorldPosition) < rotationAnchor->GetRadius();
}

//===========================================================================//

bool ArrangementEditor::IsEntitySizeAnchorsContainsPoint(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const
{
	return GetEntityPullingSide(entity, mouseX, mouseY) != PullingSide::None;
}

//===========================================================================//

PullingSide ArrangementEditor::GetEntityPullingSide(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const
{
	if (entity == NullPtr)
	{
		return PullingSide::None;
	}

	const auto & node = entity->GetComponent<PositioningNode>();

	Vector3 mousePoint = LocationEditor->GetMousePoint(mouseX, mouseY, node->GetPositionZ());
	Vector3 adaptedPoint = CoordsHelper::GetProjectedOnEntityPoint(entity, mousePoint);

	const float sizeAnchorsHalfSize = _selectedEntityVisualizationInfo->GetSizeAnchorsProjectSide() / 2.0f;

	const auto & sizeAnchors = _selectedEntityVisualizationInfo->GetSizeAnchors();

	for (integer i = 0; i < sizeAnchors.GetCount(); ++i)
	{
		Vector3 sizeAnchorWorldPosition = node->GetPosition() + sizeAnchors[i]->GetLeftLowerPoint();

		if (adaptedPoint.Equals(sizeAnchorWorldPosition, sizeAnchorsHalfSize))
		{
			return static_cast<PullingSide>(i + 1);
		}
	}

	return PullingSide::None;
}

//===========================================================================//

SharedPtr<Entity> ArrangementEditor::CreateEntity(const string & pathToEntity, const Vector3 & entityPosition)
{
	if (_selectedLayerName.IsEmpty())
	{
		INVALID_STATE_EXCEPTION("No layer selected");
	}

	SharedPtr<Entity> entity = EntityManager->CreateEntity(pathToEntity, _domainName,
	                                                       _selectedLayerName, entityPosition);
	
	EntityHelper::DisableEntityDerivedComponents<Updatable>(entity);
	EntityHelper::DisableEntityDerivedComponents<InputReceiver>(entity);

	SelectEntity(entity);

	return entity;
}

//===========================================================================//

void ArrangementEditor::AddEntity(const SharedPtr<Entity> & entity)
{
	if (_selectedLayerName.IsEmpty())
	{
		INVALID_STATE_EXCEPTION("No layer selected");
	}

	EntityManager->AddEntity(entity, _domainName);
	SelectEntity(entity);
}

//===========================================================================//

void ArrangementEditor::RemoveEntity(const SharedPtr<Entity> & entity)
{
	if (entity == _selectedEntity)
	{
		UnselectEntity(true);
	}
	EntityManager->RemoveEntityWithoutPooling(entity);
}

//===========================================================================//

void ArrangementEditor::MoveEntityByXYAxises(const WeakPtr<Entity> & entity, float mouseX, float mouseY, const Vector2 & captureOffsetCoords) const
{
	const auto & node = entity->GetComponent<PositioningNode>();
	const Vector3 mousePoint = LocationEditor->GetMousePoint(mouseX, mouseY, node->GetPositionZ());
	node->SetPositionX(mousePoint.X - captureOffsetCoords.X);
	node->SetPositionY(mousePoint.Y - captureOffsetCoords.Y);
}

//===========================================================================//

void ArrangementEditor::MoveEntityByZAxis(const WeakPtr<Entity> & entity, float mouseDelta) const
{
	const auto & node = entity->GetComponent<PositioningNode>();
	const float deltaZ = LocationEditor->GetMouseDelta(mouseDelta, node->GetPositionZ());
	SetEntityPositionZ(entity, node->GetPositionZ() + deltaZ * EntityZoomFactor);
}

//===========================================================================//

void ArrangementEditor::SetEntityPositionZ(const WeakPtr<Entity> & entity, float z) const
{
	const auto & node = entity->GetComponent<PositioningNode>();
	Vector3 cameraPosition = LocationEditor->GetLocationObserver()->GetCamera()->GetPosition();
	ProjectionMode projectionMode = Renderer->GetLayer(_domainName, _gameplayLayerName)->GetProjectionMode();
	Vector2 screenCoords = CoordsHelper::WorldToSurface(node->GetPosition(), cameraPosition, projectionMode);

	node->SetPositionZ(z);

	const Vector3 worldCoords = LocationEditor->GetMousePoint(screenCoords.X, screenCoords.Y, node->GetPositionZ());
	node->SetPositionX(worldCoords.X);
	node->SetPositionY(worldCoords.Y);
}

//===========================================================================//

void ArrangementEditor::RotateEntity(const WeakPtr<Entity> & entity, float mouseX, float mouseY, bool useAngleStep) const
{
	const auto & node = entity->GetComponent<PositioningNode>();
	const auto & rotationNode = entity->GetComponent<RotationNode>();

	const Vector3 mousePoint = LocationEditor->GetMousePoint(mouseX, mouseY, node->GetPositionZ());

	Angle angle = Math::ArcTan(mousePoint.X - node->GetPosition().X, mousePoint.Y - node->GetPosition().Y);
	if (useAngleStep)
	{
		angle = Math::Round(angle, EntityRotationLargeStep);
	}

	rotationNode->SetAngleZ(angle);
}

//===========================================================================//

void ArrangementEditor::ResizeEntity(const WeakPtr<Entity> & entity, PullingSide pullingSide, float mouseX, float mouseY) const
{
	const auto & node = entity->GetComponent<PositioningNode>();
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	const Vector3 mousePoint = LocationEditor->GetMousePoint(mouseX, mouseY, node->GetPositionZ());
	const Vector3 adaptedPoint = CoordsHelper::GetProjectedOnEntityPoint(entity, mousePoint);

	Size newSize =
	{
		(node->GetPosition().X - adaptedPoint.X) * 2.0f,
		(node->GetPosition().Y - adaptedPoint.Y) * 2.0f
	};

	Size size = baseSprite->GetSize();

	const float sizeRatioFactor = size.Width / size.Height;

	switch (pullingSide)
	{
	case PullingSide::LeftBottom:
	case PullingSide::RightBottom:
		size.Height = Math::Max(newSize.Height, MinSize);
		size.Width = sizeRatioFactor * size.Height;
		break;

	case PullingSide::LeftTop:
	case PullingSide::RightTop:
		size.Height = Math::Max(-newSize.Height, MinSize);
		size.Width = sizeRatioFactor * size.Height;
		break;

	case PullingSide::Bottom:
		size.Height = Math::Max(newSize.Height, MinSize);
		break;

	case PullingSide::Right:
		size.Width = Math::Max(-newSize.Width, MinSize);
		break;

	case PullingSide::Top:
		size.Height = Math::Max(-newSize.Height, MinSize);
		break;

	case PullingSide::Left:
		size.Width = Math::Max(newSize.Width, MinSize);
		break;

	default:
		break;
	}

	baseSprite->SetSize(size);
}

//===========================================================================//

void ArrangementEditor::ResetEntity(const WeakPtr<Entity> & entity) const
{
	SetEntityPositionZ(entity, 0.0f);
	const auto & positioningNode = entity->GetComponent<PositioningNode>();
	const auto & rotationNode = entity->GetComponent<RotationNode>();
	rotationNode != NullPtr ? rotationNode->SetAngleZ(0.0f) : positioningNode->SetAngleZ(0.0f);
	const auto & renderable = entity->GetComponent<Renderable>();
	renderable->Reset();
}

//===========================================================================//

void ArrangementEditor::ShowUI()
{
	_entitiesBrowser->Show();
	_layerBrowser->Show();
	_selectedEntityInfoLabel->Show();
}

//===========================================================================//

void ArrangementEditor::HideUI()
{
	_entitiesBrowser->Hide();
	_layerBrowser->Hide();
	_selectedEntityInfoLabel->Hide();
}

//===========================================================================//

bool ArrangementEditor::IsUIVisible() const
{
	return _entitiesBrowser->IsVisible() && _layerBrowser->IsVisible();
}

//===========================================================================//

bool ArrangementEditor::IsUIContainsPoint(float mouseX, float mouseY) const
{
	return _entitiesBrowser->ContainsPoint(mouseX, mouseY, true) ||
	       _layerBrowser->ContainsPoint(mouseX, mouseY, true);
}

//===========================================================================//

const SharedPtr<EntitiesBrowser> & ArrangementEditor::GetEntitiesBrowser() const
{
	return _entitiesBrowser;
}

//===========================================================================//

const SharedPtr<LayerBrowser> & ArrangementEditor::GetLayerBrowser() const
{
	return _layerBrowser;
}

//===========================================================================//

const SharedPtr<CommandsManager> & ArrangementEditor::GetCommandsManager() const
{
	return _commandsManager;
}

//===========================================================================//

bool ArrangementEditor::IsEntitySuitableForSelectedLayer(const WeakPtr<Entity> & entity) const
{
	return EntityManager->GetSavingGroupsNames().Contains(entity->GetGroupName()) &&
	       EntityHelper::GetEntityLayerName(entity) == _selectedLayerName;
}

//===========================================================================//

void ArrangementEditor::UpdateSelectedEntityInfoLabel()
{
	if (_selectedEntity == NullPtr)
	{
		_selectedEntityInfoLabel->SetText(string::Empty);
		return;
	}

	const string nameText = string::Format("Type = {0}\n", _selectedEntity->GetType());
	const string idText = string::Format("Id = {0}\n", _selectedEntity->GetID());
	const auto & positioningNode = _selectedEntity->GetComponent<PositioningNode>();
	const string positionText = string::Format("X = {0}\nY = {1}\nZ = {2}\n",
	                                           positioningNode->GetPositionX(), positioningNode->GetPositionY(),
	                                           positioningNode->GetPositionZ());
	const auto & rotationNode = _selectedEntity->GetComponent<RotationNode>();
	const float angleZ = rotationNode != NullPtr ? rotationNode->GetAngleZ() : positioningNode->GetAngleZ();
	const string angleText = string::Format("A = {0}\n", angleZ);
	const auto & baseSprite = _selectedEntity->GetComponent<BaseSprite>();
	const string sizeText = string::Format("W = {0}\nH = {1}\n", baseSprite->GetWidth(), baseSprite->GetHeight());
	const string renderingOrderText = string::Format("O = {0}\n", baseSprite->GetRenderingOrder());

	_selectedEntityInfoLabel->SetText(nameText + idText + positionText + angleText + sizeText + renderingOrderText);
}

//===========================================================================//

void ArrangementEditor::ApplyLayerVisibleToVisualizationInfos(const WeakPtr<RenderableLayer> & layer)
{
	for (const auto & entitiesVisualizationInfo : _entitiesVisualizationInfos)
	{
		entitiesVisualizationInfo.GetValue()->SetVisible(layer->IsVisible());
	}

	if (_selectedEntityVisualizationInfo != NullPtr)
	{
		_selectedEntityVisualizationInfo->SetVisible(layer->IsVisible());
	}
}

//===========================================================================//

void ArrangementEditor::AddLayers() const
{
	Renderer->AddLayerToFront(_domainName, new RenderableLayer(UILayerName, ProjectionMode::PercentsOrtho));
	Renderer->AddLayerToFront(_domainName, new RenderableLayer(AddingEntityUILayerName));
}

//===========================================================================//

void ArrangementEditor::RemoveLayers() const
{
	Renderer->RemoveLayer(_domainName, AddingEntityUILayerName);
	Renderer->RemoveLayer(_domainName, UILayerName);
}

//===========================================================================//

void ArrangementEditor::UpdateNavigationMeshVisibility() const
{
	if (EditorConfig->Get<bool>(PathToIsNavigationMeshVisible))
	{
		LocationEditor->ShowNavigationMesh();
	}
	else
	{
		LocationEditor->HideNavigationMesh();
	}
}

//===========================================================================//

void ArrangementEditor::CreateLayerBrowser()
{
	_layerBrowser = new LayerBrowser(_domainName, ArrangementEditor::UILayerName);
	_layerBrowser->MoveTo(_entitiesBrowser->GetPosition().X - _layerBrowser->GetSize().X,
	                      _entitiesBrowser->GetPosition().Y);
	_layerBrowser->Move(-1, 0);
}

//===========================================================================//

void ArrangementEditor::CreateSelectedEntityInfoLabel()
{
	const string fontName = EditorConfig->Get<string>(PathToSelectedEntityInfoLabelFontName);
	const integer fontSize = EditorConfig->Get<integer>(PathToSelectedEntityInfoLabelFontSize);
	_selectedEntityInfoLabel = new Label(_domainName, UILayerName, string::Empty,
	                                     FontManager->Get(fontName, fontSize),
	                                     Colors::White, Vector2(), WordWrapMode::Simple,
	                                     TextHorizontalAlignment::Left, TextVerticalAlignment::Bottom);
	_selectedEntityInfoLabel->GetPositioningNode()->SetPosition(0.5f, 0.5f);
}

//===========================================================================//

void ArrangementEditor::AddHandlersToEntity(const WeakPtr<Entity> & entity)
{
	const auto & node = entity->GetComponent<PositioningNode>();
	node->TransformationChanged.AddHandler(_selectedEntityPositioningNodeTransformationChangedEventHandler);
	if (entity->ContainsComponent(RotationNode::TypeID))
	{
		const auto & rotationNode = entity->GetComponent<RotationNode>();
		rotationNode->TransformationChanged.AddHandler(_selectedEntityRotationNodeTransformationChangedEventHandler);
	}
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	baseSprite->RenderingOrderChangedEvent.AddHandler(_selectedEntityRenderingOrderChangedEventHandler);
	baseSprite->SizeChangedEvent.AddHandler(_selectedEntitySizeChangedEventHandler);
}

//===========================================================================//

void ArrangementEditor::RemoveHandlersFromEntity(const WeakPtr<Entity> & entity)
{
	const auto & node = entity->GetComponent<PositioningNode>();
	node->TransformationChanged.RemoveHandler(_selectedEntityPositioningNodeTransformationChangedEventHandler);
	if (entity->ContainsComponent(RotationNode::TypeID))
	{
		const auto & rotationNode = entity->GetComponent<RotationNode>();
		rotationNode->TransformationChanged.RemoveHandler(_selectedEntityRotationNodeTransformationChangedEventHandler);
	}
	const auto & baseSprite = entity->GetComponent<BaseSprite>();
	baseSprite->RenderingOrderChangedEvent.RemoveHandler(_selectedEntityRenderingOrderChangedEventHandler);
	baseSprite->SizeChangedEvent.RemoveHandler(_selectedEntitySizeChangedEventHandler);
}

//===========================================================================//

void ArrangementEditor::OnLayerVisibleChanged(const LayerEventArgs & args)
{
	if (args.Layer->GetName() == _selectedLayerName)
	{
		ApplyLayerVisibleToVisualizationInfos(args.Layer);
	}
}

//===========================================================================//

void ArrangementEditor::OnEntityCreated(const EntityManagerEventArgs & args)
{
	const auto & entity = args.ChangedEntity;
	if (!IsEntitySuitableForSelectedLayer(entity))
	{
		return;
	}

	SharedPtr<EntityVisualizationInfo> entityVisualizationInfo = new EntityVisualizationInfo(
	    _domainName, LocationEditorSingleton::HUDLayerName, entity);
	_entitiesVisualizationInfos.Add(entity, entityVisualizationInfo);
}

//===========================================================================//

void ArrangementEditor::OnEntityRemoved(const EntityManagerEventArgs & args)
{
	const auto & entity = args.ChangedEntity;
	if (_entitiesVisualizationInfos.Contains(entity))
	{
		_entitiesVisualizationInfos.Remove(entity);
	}
	else if (_selectedEntity == entity)
	{
		UnselectEntity();
	}
}

//===========================================================================//

void ArrangementEditor::OnSelectedEntityPositioningNodeTransformationChanged(const EmptyEventArgs &)
{
	UpdateSelectedEntityInfoLabel();
}

//===========================================================================//

void ArrangementEditor::OnSelectedEntityRotationNodeTransformationChanged(const EmptyEventArgs &)
{
	UpdateSelectedEntityInfoLabel();
}

//===========================================================================//

void ArrangementEditor::OnSelectedEntityRenderingOrderChanged(const EmptyEventArgs &)
{
	UpdateSelectedEntityInfoLabel();
}

//===========================================================================//

void ArrangementEditor::OnSelectedEntitySizeChanged(const EmptyEventArgs &)
{
	UpdateSelectedEntityInfoLabel();
}

//===========================================================================//

} // namespace Bru

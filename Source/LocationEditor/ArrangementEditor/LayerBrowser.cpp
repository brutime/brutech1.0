//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LayerBrowser.h"

#include "../LocationEditor.h"
#include "ArrangementEditor.h"

namespace Bru
{

//===========================================================================//

const string LayerBrowser::PathToLayerBrowser = "ArrangementEditor.LayerBrowser";
const string LayerBrowser::PathToFontName = PathToLayerBrowser + ".FontName";
const string LayerBrowser::PathToFontSize = PathToLayerBrowser + ".FontSize";
const string LayerBrowser::PathToFontColor = PathToLayerBrowser + ".FontColor";
const string LayerBrowser::PathToWidth = PathToLayerBrowser + ".Width";
const string LayerBrowser::PathToPadding = PathToLayerBrowser + ".Padding";
const string LayerBrowser::PathToLayerItemHeight = PathToLayerBrowser + ".LayerItemHeight";

//===========================================================================//

LayerBrowser::LayerBrowser(const string & domainName, const string & layerName) :
	Frame(domainName, layerName,
	      static_cast<integer>(Graphics->GetViewport().Width * EditorConfig->Get<float>(PathToWidth)),
	      Graphics->GetViewport().Height, EditorConfig->Get<Color>(ArrangementEditor::PathToBackgroundColor)),
	Padding(EditorConfig->Get<Vector2>(PathToPadding)),
	LayerItemHeight(EditorConfig->Get<float>(PathToLayerItemHeight)),
	BackgroundColor(EditorConfig->Get<Color>(ArrangementEditor::PathToBackgroundColor)),
	_layersItems(),
	_selectedLayerItemIndex(-1),
	_visibilityButtonClickHandler(new EventHandler<const Widget *, const MouseEventArgs &>(this, &LayerBrowser::OnVisibilityButtonClick)),
	_layerButtonClickHandler(new EventHandler<const Widget *, const MouseEventArgs &>(this, &LayerBrowser::OnLayerButtonClick)),
	_layerChangedEventHandler(new EventHandler<const string &>(this, &LayerBrowser::OnLayerChanged))
{
	UpdateUI();
	LocationEditor->GetArrangementEditor()->LayerChanged.AddHandler(_layerChangedEventHandler);
	if (!_layersItems.IsEmpty())
	{
		const auto & layersNames = LocationEditor->GetLocation()->GetLayersNames();
		SelectLayer(layersNames[0]);
	}
}

//===========================================================================//

LayerBrowser::~LayerBrowser()
{
	LocationEditor->GetArrangementEditor()->LayerChanged.RemoveHandler(_layerChangedEventHandler);
}

//===========================================================================//

void LayerBrowser::UpdateUI()
{
	const auto & font = FontManager->Get(EditorConfig->Get<string>(ArrangementEditor::PathToButtonsFontName),
	                                     EditorConfig->Get<integer>(ArrangementEditor::PathToButtonsFontSize));
	const Color fontColor = EditorConfig->Get<Color>(ArrangementEditor::PathToButtonsFontColor);

	if (LocationEditor->GetLocation() != NullPtr)
	{
		const auto & layersNames = LocationEditor->GetLocation()->GetLayersNames();
		float verticalOffset = 0.0f;
		for (integer i = 0; i < layersNames.GetCount(); ++i)
		{
			SharedPtr<LayerBrowserItem> item = new LayerBrowserItem(_domainName, _layerName, GetSize().X, LayerItemHeight,
			                                                        {Padding.X, Padding.Y}, font, fontColor,
			                                                        BackgroundColor, layersNames[i], i);
			item->MoveTo(0.0f, verticalOffset);
			item->GetVisibilityButton()->Click.AddHandler(_visibilityButtonClickHandler);
			item->GetLayerButton()->Click.AddHandler(_layerButtonClickHandler);
			AddChild(item);
			_layersItems.Add(item);
			verticalOffset += LayerItemHeight;
		}
	}

	SetSize(GetSize().X, LayerItemHeight * _layersItems.GetCount());
}

//===========================================================================//

void LayerBrowser::SelectLayer(const string & layerName)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	editor->GetCommandsManager()->Execute(new SelectLayerCommand(layerName));
}

//===========================================================================//

void LayerBrowser::OnLayerButtonClick(const Widget * sender, const MouseEventArgs &)
{
	const auto & layersNames = LocationEditor->GetLocation()->GetLayersNames();
	SelectLayer(layersNames[sender->GetTag()]);
}

//===========================================================================//

void LayerBrowser::OnVisibilityButtonClick(const Widget * sender, const MouseEventArgs &)
{
	const integer layerItemIndex = sender->GetTag();
	const auto & layerItem = _layersItems[layerItemIndex];
	const auto & layersNames = LocationEditor->GetLocation()->GetLayersNames();
	const auto & layer = Renderer->GetLayer(_domainName, layersNames[layerItemIndex]);
	if (layerItem->IsVisibilityEnabled())
	{
		layerItem->DisableVisibility();
		layer->Hide();
	}
	else
	{
		layerItem->EnableVisibility();
		layer->Show();
	}
}

//===========================================================================//

void LayerBrowser::OnLayerChanged(const string & layerName)
{
	const auto & layersNames = LocationEditor->GetLocation()->GetLayersNames();
	integer selectedLayerItemIndex = layersNames.IndexOf(layerName);
	if (selectedLayerItemIndex == _selectedLayerItemIndex)
	{
		return;
	}

	if (_selectedLayerItemIndex != -1)
	{
		_layersItems[_selectedLayerItemIndex]->UnSelect();
	}

	_selectedLayerItemIndex = selectedLayerItemIndex;
	_layersItems[_selectedLayerItemIndex]->Select();
}

//===========================================================================//

} // namespace Bru

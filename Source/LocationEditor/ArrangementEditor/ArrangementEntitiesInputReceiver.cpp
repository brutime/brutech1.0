//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ArrangementEntitiesInputReceiver.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(ArrangementEntitiesInputReceiver, InputReceiver)

//===========================================================================//

ArrangementEntitiesInputReceiver::ArrangementEntitiesInputReceiver(const string & domainName) :
	InputReceiver(domainName),
	_addingEntityItem(),
	_entityArrangingMode(EntityArrangingMode::None),
	_executeLastCommand(false),
	_entityMovingPosition(),
	_captureOffsetCoords(),
	_entityRotatingAngle(0.0f),
	_entityPullingSide(PullingSide::None),
	_entityResizingSize()
{
}

//===========================================================================//

ArrangementEntitiesInputReceiver::~ArrangementEntitiesInputReceiver()
{
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	if (args.State == KeyState::Down || args.State == KeyState::RepeatedDown)
	{
		DispatchKeyboardUndoRedo(args);

		auto selectedEntity = LocationEditor->GetArrangementEditor()->GetSelectedEntity();
		if (selectedEntity != NullPtr && EntityHelper::IsEntityVisible(selectedEntity))
		{
			DispatchKeyboardSelectedEntityActions(args);
		}

		DispatchKeyboardDownUI(args);
	}
	else
	{
		DispatchKeyboardUpUI(args);
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	if (args.Key == MouseKey::Move)
	{
		OnMouseMove(args);
	}

	if (args.State == KeyState::Down)
	{
		OnMouseKeyDown(args);
	}
	else if (args.State == KeyState::Up)
	{
		OnMouseKeyUp(args);
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::LostFocus()
{
	DestroyAddingEntityVisualization();
	UnselectEntity();
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardUndoRedo(const KeyboardEventArgs & args) const
{
	const auto & editor = LocationEditor->GetArrangementEditor();

	switch (args.Key)
	{
	case KeyboardKey::Z:
		if (Keyboard->IsKeyDown(KeyboardKey::Control))
		{
			editor->GetCommandsManager()->Undo();
		}
		break;

	case KeyboardKey::Y:
		if (Keyboard->IsKeyDown(KeyboardKey::Control))
		{
			editor->GetCommandsManager()->Redo();
		}
		break;

	default:
		break;
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardSelectedEntityActions(const KeyboardEventArgs & args)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	if (selectedEntity->ContainsComponent(PositioningNode::TypeID) &&
	    !Keyboard->IsKeyDown(KeyboardKey::Control) &&
	    !Keyboard->IsKeyDown(KeyboardKey::Alt))
	{
		DispatchKeyboardMovingEntity(args);
	}
	else if (selectedEntity->ContainsComponent(RotationNode::TypeID) &&
	         !Keyboard->IsKeyDown(KeyboardKey::Control) &&
	         Keyboard->IsKeyDown(KeyboardKey::Alt))
	{
		DispatchKeyboardRotatingEntity(args);
	}
	else if (selectedEntity->ContainsComponent(BaseSprite::TypeID) &&
	         Keyboard->IsKeyDown(KeyboardKey::Control) &&
	         !Keyboard->IsKeyDown(KeyboardKey::Alt))
	{
		DispatchKeyboardResizingEntity(args);
	}

	float entityMoveByZAxisDelta = 0.0f;

	if (Keyboard->IsKeyDown(KeyboardKey::Control))
	{
		return;
	}

	switch (args.Key)
	{
	case KeyboardKey::Plus:
		entityMoveByZAxisDelta = ArrangementEditor::EntityZoomStep;
		break;

	case KeyboardKey::Minus:
		entityMoveByZAxisDelta = -ArrangementEditor::EntityZoomStep;
		break;

	case KeyboardKey::D:
		if (EntityHelper::IsEntityFlippedHorizontal(selectedEntity))
		{
			editor->GetCommandsManager()->Execute(new UnflipHorizontalCommand(selectedEntity));
		}
		break;
		

	case KeyboardKey::A:
		if (!EntityHelper::IsEntityFlippedHorizontal(selectedEntity))
		{
			editor->GetCommandsManager()->Execute(new FlipHorizontalCommand(selectedEntity));
		}
		break;		

	case KeyboardKey::W:
		if (EntityHelper::IsEntityFlippedVertical(selectedEntity))
		{
			editor->GetCommandsManager()->Execute(new UnflipVerticalCommand(selectedEntity));
		}
		break;

	case KeyboardKey::S:
		if (!EntityHelper::IsEntityFlippedVertical(selectedEntity))
		{
			editor->GetCommandsManager()->Execute(new FlipVerticalCommand(selectedEntity));
		}
		break;
		
	case KeyboardKey::T:
		editor->GetCommandsManager()->Execute(new IncrementRenderingOrderCommand(selectedEntity));
		break;
		
	case KeyboardKey::G:
		editor->GetCommandsManager()->Execute(new DecrementRenderingOrderCommand(selectedEntity));
		break;
		
	case KeyboardKey::Y:
		editor->GetCommandsManager()->Execute(new SetMaxRenderingOrderCommand(selectedEntity));
		break;
		
	case KeyboardKey::H:
		editor->GetCommandsManager()->Execute(new SetMinRenderingOrderCommand(selectedEntity));
		break;

	case KeyboardKey::R:
		editor->GetCommandsManager()->Execute(new ResetEntityCommand(selectedEntity));
		break;

	case KeyboardKey::Delete:
		RemoveEntity(selectedEntity);
		break;

	default:
		break;
	}

	if (!Math::EqualsZero(entityMoveByZAxisDelta))
	{
		editor->MoveEntityByZAxis(selectedEntity, entityMoveByZAxisDelta);
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardMovingEntity(const KeyboardEventArgs & args)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	const auto & node = selectedEntity->GetComponent<PositioningNode>();
	const float movingStep = Keyboard->IsKeyDown(KeyboardKey::Shift) ? ArrangementEditor::EntityMovingLargeStep : ArrangementEditor::EntityMovingSmallStep;
	_entityMovingPosition = node->GetPosition();

	switch (args.Key)
	{
	case KeyboardKey::Left:
		node->SetPositionX(Math::Round(node->GetPosition().X - movingStep, movingStep));
		break;

	case KeyboardKey::Right:
		node->SetPositionX(Math::Round(node->GetPosition().X + movingStep, movingStep));
		break;

	case KeyboardKey::Up:
		node->SetPositionY(Math::Round(node->GetPosition().Y + movingStep, movingStep));
		break;

	case KeyboardKey::Down:
		node->SetPositionY(Math::Round(node->GetPosition().Y - movingStep, movingStep));
		break;

	default:
		break;
	}

	if (node->GetPosition() != _entityMovingPosition)
	{
		editor->GetCommandsManager()->Execute(new MoveEntityCommand(selectedEntity, _entityMovingPosition));
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardRotatingEntity(const KeyboardEventArgs & args)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	const auto & rotationNode = selectedEntity->GetComponent<RotationNode>();
	const float rotationStep = Keyboard->IsKeyDown(KeyboardKey::Shift) ? ArrangementEditor::EntityRotationLargeStep : ArrangementEditor::EntityRotationSmallStep;
	_entityRotatingAngle = rotationNode->GetAngleZ();

	switch (args.Key)
	{
	case KeyboardKey::Left:
		rotationNode->Rotate2D(-rotationStep);
		break;

	case KeyboardKey::Right:
		rotationNode->Rotate2D(rotationStep);
		break;

	default:
		break;
	}

	if (!Math::Equals(rotationNode->GetAngleZ(), _entityRotatingAngle))
	{
		rotationNode->SetAngleZ(Math::Round(rotationNode->GetAngleZ(), rotationStep));
		editor->GetCommandsManager()->Execute(new RotateEntityCommand(selectedEntity, _entityRotatingAngle));
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardResizingEntity(const KeyboardEventArgs & args)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	const auto & baseSprite = selectedEntity->GetComponent<BaseSprite>();
	const float sizeStep = Keyboard->IsKeyDown(KeyboardKey::Shift) ? ArrangementEditor::EntityResizingLargeStep : ArrangementEditor::EntityResizingSmallStep;
	_entityResizingSize = baseSprite->GetSize();
	Size newSize = _entityResizingSize;

	switch (args.Key)
	{
	case KeyboardKey::Up:
		newSize.Width = Math::Max(newSize.Width + sizeStep, ArrangementEditor::MinSize);
		newSize.Height = Math::Max(newSize.Height + sizeStep, ArrangementEditor::MinSize);
		break;

	case KeyboardKey::Down:
		newSize.Width = Math::Max(newSize.Width - sizeStep, ArrangementEditor::MinSize);
		newSize.Height = Math::Max(newSize.Height - sizeStep, ArrangementEditor::MinSize);
		break;

	default:
		break;
	}

	if (newSize != _entityResizingSize)
	{
		baseSprite->SetSize(newSize);
		editor->GetCommandsManager()->Execute(new ResizeEntityCommand(selectedEntity, _entityResizingSize));
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardDownUI(const KeyboardEventArgs & args)
{
	const auto & editor = LocationEditor->GetArrangementEditor();

	switch (args.Key)
	{
	case KeyboardKey::Escape:
		UnselectEntity();
		break;

	case KeyboardKey::I:
		editor->IsUIVisible() ? editor->HideUI() : editor->ShowUI();
		break;

	case KeyboardKey::N:
		LocationEditor->IsNavigationMeshVisible() ? LocationEditor->HideNavigationMesh() : LocationEditor->ShowNavigationMesh();
		break;

	default:
		break;
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DispatchKeyboardUpUI(const KeyboardEventArgs & args)
{
	switch (args.Key)
	{
	case KeyboardKey::Shift:
		if (_addingEntityItem != NullPtr)
		{
			DestroyAddingEntityVisualization();
		}
		break;

	default:
		break;
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::OnMouseMove(const MouseEventArgs & args)
{
	const auto & locationEditor = LocationEditor;
	const auto & editor = locationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	if (_addingEntityItem != NullPtr)
	{
		_addingEntityItem->Node->SetPosition(locationEditor->GetMousePoint(args.X, args.Y));
	}
	else if (selectedEntity != NullPtr && EntityHelper::IsEntityVisible(selectedEntity))
	{
		switch (_entityArrangingMode)
		{
		case EntityArrangingMode::Moving:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				editor->MoveEntityByZAxis(selectedEntity, -args.DeltaY);
			}
			else
			{
				editor->MoveEntityByXYAxises(selectedEntity, args.X, args.Y, _captureOffsetCoords);
			}
			_executeLastCommand = true;
			break;

		case EntityArrangingMode::Rotating:
			editor->RotateEntity(selectedEntity, args.X, args.Y, Keyboard->IsKeyDown(KeyboardKey::Shift));
			_executeLastCommand = true;
			break;

		case EntityArrangingMode::Resizing:
			editor->ResizeEntity(selectedEntity, _entityPullingSide, args.X, args.Y);
			_executeLastCommand = true;
			break;

		case EntityArrangingMode::None:
			break;

		default:
			INVALID_ARGUMENT_EXCEPTION("_entityArrangingMode not defined");
		}
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::OnMouseKeyDown(const MouseEventArgs & args)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	if (args.Key == MouseKey::Left)
	{
		//Если не добавляем очередную entity на карту и не кликаем по UI
		if (_addingEntityItem == NullPtr && (!editor->IsUIContainsPoint(args.X, args.Y) || !editor->IsUIVisible()))
		{
			if (editor->IsEntityRotationAnchorContainsPoint(selectedEntity, args.X, args.Y))
			{
				SelectEntityArrangingMode(EntityArrangingMode::Rotating, args.X, args.Y);
			}
			else if (editor->IsEntitySizeAnchorsContainsPoint(selectedEntity, args.X, args.Y))
			{
				SelectEntityArrangingMode(EntityArrangingMode::Resizing, args.X, args.Y);
			}
			else
			{
				auto upperEntity = editor->GetUpperEntity(args.X, args.Y);
				if (upperEntity != NullPtr)
				{
					editor->SelectEntity(upperEntity);
					SelectEntityArrangingMode(EntityArrangingMode::Moving, args.X, args.Y);
				}
				else
				{
					UnselectEntity();
				}
			}
		}
		else
		{
			//Если мы выбрали что-нибудь в entitiesBrowser
			auto selectedItem = editor->GetEntitiesBrowser()->GetItem(args.X, args.Y);
			if (selectedItem != NullPtr)
			{
				//Создаем отображение для этого Entity
				CreateAddingEntityVisualization(selectedItem, args.X, args.Y);
			}
		}
	}
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::OnMouseKeyUp(const MouseEventArgs & args)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	if (_addingEntityItem == NullPtr)
	{
		if (selectedEntity != NullPtr)
		{
			if (_executeLastCommand)
			{
				switch (_entityArrangingMode)
				{
				case EntityArrangingMode::Moving:
					editor->GetCommandsManager()->Execute(new MoveEntityCommand(selectedEntity, _entityMovingPosition));
					break;

				case EntityArrangingMode::Rotating:
					editor->GetCommandsManager()->Execute(new RotateEntityCommand(selectedEntity, _entityRotatingAngle));
					break;

				case EntityArrangingMode::Resizing:
					editor->GetCommandsManager()->Execute(new ResizeEntityCommand(selectedEntity, _entityResizingSize));
					break;

				case EntityArrangingMode::None:
					break;

				default:
					INVALID_ARGUMENT_EXCEPTION("entityArrangingMode not defined");
				}

				_executeLastCommand = false;
			}

			SelectEntityArrangingMode(EntityArrangingMode::None);
		}

		if (args.Key == MouseKey::Right)
		{
			RemoveEntity(editor->GetUpperEntity(args.X, args.Y));
		}
	}
	//Если мы тащили отображение добавляемой Entity
	else if (args.Key == MouseKey::Left)
	{
		//И бросили ее на карту, а не на UI
		if (!editor->IsUIContainsPoint(args.X, args.Y) || !editor->IsUIVisible())
		{
			//Добавляем новую Entity на карту
			editor->GetCommandsManager()->Execute(new AddEntityCommand(args.X, args.Y, _addingEntityItem->Path));
		}
		if (!Keyboard->IsKeyDown(KeyboardKey::Shift))
		{
			DestroyAddingEntityVisualization();
		}
	}

}

//===========================================================================//

void ArrangementEntitiesInputReceiver::SelectEntityArrangingMode(EntityArrangingMode entityArrangingMode, float mouseX, float mouseY)
{
	const auto & editor = LocationEditor->GetArrangementEditor();
	auto selectedEntity = editor->GetSelectedEntity();

	switch (entityArrangingMode)
	{
	case EntityArrangingMode::Moving:
		_entityMovingPosition = selectedEntity->GetComponent<PositioningNode>()->GetPosition();
		_captureOffsetCoords = GetCaptureOffsetCoords(selectedEntity, mouseX, mouseY);
		_entityRotatingAngle = 0.0f;
		_entityPullingSide = PullingSide::None;
		_entityResizingSize.Clear();
		break;

	case EntityArrangingMode::Rotating:
		_entityMovingPosition.Clear();
		_captureOffsetCoords.Clear();
		_entityRotatingAngle = EntityHelper::GetEntityAngle(selectedEntity);
		_entityPullingSide = PullingSide::None;
		_entityResizingSize.Clear();
		break;

	case EntityArrangingMode::Resizing:
		_entityMovingPosition.Clear();
		_captureOffsetCoords.Clear();
		_entityRotatingAngle = 0.0f;
		_entityPullingSide = editor->GetEntityPullingSide(selectedEntity, mouseX, mouseY);
		_entityResizingSize = selectedEntity->GetComponent<BaseSprite>()->GetSize();
		break;

	case EntityArrangingMode::None:
		_entityMovingPosition.Clear();
		_captureOffsetCoords.Clear();
		_entityRotatingAngle = 0.0f;
		_entityPullingSide = PullingSide::None;
		_entityResizingSize.Clear();
		break;

	default:
		INVALID_ARGUMENT_EXCEPTION("entityArrangingMode not defined");
	}

	_entityArrangingMode = entityArrangingMode;
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::CreateAddingEntityVisualization(const SharedPtr<EntitiesBrowserItem> & selectedItem,
                                                                       float mouseX, float mouseY)
{
	const auto & locationEditor = LocationEditor;
	const auto & entitiesBrowser = locationEditor->GetArrangementEditor()->GetEntitiesBrowser();

	_addingEntityItem = new AddingEntityItem(GetDomainName(), ArrangementEditor::AddingEntityUILayerName,
	                                         selectedItem->GetPath(), locationEditor->GetMousePoint(mouseX, mouseY), entitiesBrowser->AddingEntitySpriteAlpha);
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::DestroyAddingEntityVisualization()
{
	_addingEntityItem = NullPtr;
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::UnselectEntity()
{
	LocationEditor->GetArrangementEditor()->UnselectEntity();
	SelectEntityArrangingMode(EntityArrangingMode::None);
}

//===========================================================================//

void ArrangementEntitiesInputReceiver::RemoveEntity(const WeakPtr<Entity> & entity)
{
	if (entity == NullPtr)
	{
		return;
	}

	const auto & editor = LocationEditor->GetArrangementEditor();
	editor->GetCommandsManager()->Execute(new RemoveEntityCommand(entity));
	SelectEntityArrangingMode(EntityArrangingMode::None);
}

//===========================================================================//

Vector2 ArrangementEntitiesInputReceiver::GetCaptureOffsetCoords(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const
{
	const auto & locationEditor = LocationEditor;
	const auto & node = entity->GetComponent<PositioningNode>();
	const Vector3 mousePoint = locationEditor->GetMousePoint(mouseX, mouseY, node->GetPositionZ());
	return { mousePoint.X - node->GetPosition().X, mousePoint.Y - node->GetPosition().Y };
}

//===========================================================================//

} // namespace Bru

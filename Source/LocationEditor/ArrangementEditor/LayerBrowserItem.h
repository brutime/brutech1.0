//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LAYER_BROWSER_ITEM_H
#define LAYER_BROWSER_ITEM_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class LayerBrowserItem : public Frame
{
public:
	LayerBrowserItem(const string & domainName, const string & layerName, float width, float height,
	                 const Vector2 & padding, const WeakPtr<Font> & font, const Color & fontColor,
	                 const Color & backgroundColor, const string & caption, integer tag);
	virtual ~LayerBrowserItem();

	void Select();
	void UnSelect();
	bool IsSelected() const;

	void EnableVisibility();
	void DisableVisibility();
	bool IsVisibilityEnabled() const;

	const SharedPtr<Button> & GetVisibilityButton() const;
	const SharedPtr<LabeledButton> & GetLayerButton() const;

private:
	const Color _normalColor;
	const Color _pressedColor;
	const Color _hoveredColor;

	SharedPtr<Button> _visibilityButton;
	SharedPtr<LabeledButton> _layerButton;

	bool _isVisibilityEnabled;
	bool _isSelected;

private:
	LayerBrowserItem(const LayerBrowserItem &) = delete;
	LayerBrowserItem & operator =(const LayerBrowserItem &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // LAYER_BROWSER_ITEM_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENTITIES_BROWSER_ITEM_H
#define ENTITIES_BROWSER_ITEM_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class EntitiesBrowserItem
{
public:
	EntitiesBrowserItem(const string & domainName, const string & layerName, const string & path,
	                    float width, float height, const Vector2 & hintSize, 
						const WeakPtr<Font> & font, const Color & fontColor, const Color & backgroundColor);
	~EntitiesBrowserItem();

	bool HasIcon() const;

	void Show();
	void Hide();
	void SetVisible(bool flag);
	bool IsVisible() const;

	const string & GetPath() const;
	const SharedPtr<Frame> & GetBackground() const;

private:
	string _path;
	SharedPtr<Frame> _background;
	SharedPtr<PositioningNode> _iconNode;
	SharedPtr<BaseSprite> _icon;
	SharedPtr<PositioningNode> _hintNode;
	SharedPtr<RenderableText> _hint;

	EntitiesBrowserItem(const EntitiesBrowserItem &) = delete;
	EntitiesBrowserItem & operator =(const EntitiesBrowserItem &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ENTITIES_BROWSER_ITEM_H

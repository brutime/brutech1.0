//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LAYER_BROWSER_H
#define LAYER_BROWSER_H

#include "../../Helpers/Engine/Engine.h"
#include "LayerBrowserItem.h"

namespace Bru
{

//===========================================================================//

class LayerBrowser : public Frame
{
public:
	LayerBrowser(const string & domainName, const string & layerName);
	virtual ~LayerBrowser();

private:
	static const string PathToLayerBrowser;
	static const string PathToFontName;
	static const string PathToFontSize;
	static const string PathToFontColor;
	static const string PathToWidth;
	static const string PathToPadding;
	static const string PathToLayerItemHeight;

	void UpdateUI();
	void SelectLayer(const string & layerName);

	void OnLayerButtonClick(const Widget * sender, const MouseEventArgs & args);
	void OnVisibilityButtonClick(const Widget * sender, const MouseEventArgs & args);
	
	void OnLayerChanged(const string & layerName);

	const Vector2 Padding;	
	const float LayerItemHeight;
	const Color BackgroundColor;
	
	Array<SharedPtr<LayerBrowserItem>> _layersItems;
	integer _selectedLayerItemIndex;
	
	SharedPtr<EventHandler<const Widget *, const MouseEventArgs &>> _visibilityButtonClickHandler;
	SharedPtr<EventHandler<const Widget *, const MouseEventArgs &>> _layerButtonClickHandler;
	
	SharedPtr<EventHandler<const string &>> _layerChangedEventHandler;

private:
	LayerBrowser(const LayerBrowser &) = delete;
	LayerBrowser & operator =(const LayerBrowser &) = delete;
};


//===========================================================================//

} // namespace Bru

#endif // LAYER_BROWSER_H

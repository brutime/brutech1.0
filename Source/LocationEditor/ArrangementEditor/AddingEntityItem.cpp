//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "AddingEntityItem.h"

namespace Bru
{

//===========================================================================//

AddingEntityItem::AddingEntityItem(const string & domainName, const string & layerName, const string & path,
                                   const Vector3 & position, float spriteAlpha) :
	Path(path),
	Node(new PositioningNode(domainName)),
	Sprite(EntityManager->CreateEntityBaseSprite(Path, domainName, layerName))
{
	Node->SetPosition(position);
	Sprite->SetColorAlpha(spriteAlpha);
	Sprite->SetPositioningNode(Node);	
}

//===========================================================================//

AddingEntityItem::~AddingEntityItem()
{
}

//===========================================================================//

} // namespace Bru

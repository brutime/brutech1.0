//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ENTITY_VISUALIZATION_INFO_H
#define ENTITY_VISUALIZATION_INFO_H

#include "../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class EntityVisualizationInfo
{
public:
	EntityVisualizationInfo(const string & domainName, const string & layerName, const WeakPtr<Entity> & targetEntity);
	EntityVisualizationInfo(const string & domainName, const string & layerName, const WeakPtr<Entity> & targetEntity, const Color & frameColor);
	virtual ~EntityVisualizationInfo();

	const WeakPtr<Entity> & GetTargetEntity() const;

	virtual void SetVisible(bool isVisible);
	bool IsVisible() const;

	void Show();
	void Hide();
	
	const string & GetDomainName() const;
	const string & GetLayerName() const;

protected:
	SharedPtr<BasePositioningNode> GetPositioningNodeFromTargetEntity() const;

	virtual void OnTargetSizeChanged(const EmptyEventArgs &);

private:
	void UpdateFrameSize();
	
	string _domainName;
	string _layerName;

	WeakPtr<Entity> _targetEntity;
	SharedPtr<RenderableRectangle> _frame;
	
	bool _isVisible;

	SharedPtr<EventHandler<const EmptyEventArgs &>> _targetSizeChangedEventHandler;

	EntityVisualizationInfo(const EntityVisualizationInfo &) = delete;
	EntityVisualizationInfo & operator =(const EntityVisualizationInfo &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ENTITY_VISUALIZATION_INFO_H

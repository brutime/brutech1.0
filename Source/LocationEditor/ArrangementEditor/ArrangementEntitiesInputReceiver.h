//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef ARRANGEMENT_ENTITIES_INPUT_RECEIVER_H
#define ARRANGEMENT_ENTITIES_INPUT_RECEIVER_H

#include "../../Helpers/Engine/Engine.h"

#include "../Enums/PullingSide.h"
#include "Enums/EntityArrangingMode.h"
#include "EntitiesBrowser.h"
#include "AddingEntityItem.h"

namespace Bru
{

//===========================================================================//

class ArrangementEntitiesInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	ArrangementEntitiesInputReceiver(const string & domainName);
	virtual ~ArrangementEntitiesInputReceiver();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);

	virtual void LostFocus();

private:
	void DispatchKeyboardUndoRedo(const KeyboardEventArgs & args) const;
	void DispatchKeyboardSelectedEntityActions(const KeyboardEventArgs & args);
	void DispatchKeyboardMovingEntity(const KeyboardEventArgs & args);
	void DispatchKeyboardRotatingEntity(const KeyboardEventArgs & args);
	void DispatchKeyboardResizingEntity(const KeyboardEventArgs & args);
	void DispatchKeyboardDownUI(const KeyboardEventArgs & args);
	void DispatchKeyboardUpUI(const KeyboardEventArgs & args);

	void OnMouseMove(const MouseEventArgs & args);
	void OnMouseKeyDown(const MouseEventArgs & args);
	void OnMouseKeyUp(const MouseEventArgs & args);

	void SelectEntityArrangingMode(EntityArrangingMode entityArrangingMode, float mouseX = 0.0f, float mouseY = 0.0f);	

	void UnselectEntity();
	void RemoveEntity(const WeakPtr<Entity> & entity);

	Vector2 GetCaptureOffsetCoords(const WeakPtr<Entity> & entity, float mouseX, float mouseY) const;

	void CreateAddingEntityVisualization(const SharedPtr<EntitiesBrowserItem> & selectedItem, float mouseX, float mouseY);
	void DestroyAddingEntityVisualization();

	SharedPtr<AddingEntityItem> _addingEntityItem;

	EntityArrangingMode _entityArrangingMode;
	bool _executeLastCommand;

	Vector3 _entityMovingPosition;
	Vector2 _captureOffsetCoords;

	float _entityRotatingAngle;

	PullingSide _entityPullingSide;
	Size _entityResizingSize;

	ArrangementEntitiesInputReceiver(const ArrangementEntitiesInputReceiver &) = delete;
	ArrangementEntitiesInputReceiver & operator =(const ArrangementEntitiesInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // ARRANGEMENT_ENTITIES_INPUT_RECEIVER_H


//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LOCATION_EDITOR_INPUT_RECEIVER_H
#define LOCATION_EDITOR_INPUT_RECEIVER_H

#include "../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class LocationEditorInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	LocationEditorInputReceiver(const string & domainName);
	virtual ~LocationEditorInputReceiver();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);
	
	virtual void LostFocus();

private:
	LocationEditorInputReceiver(const LocationEditorInputReceiver &) = delete;
	LocationEditorInputReceiver & operator =(const LocationEditorInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // LOCATION_EDITOR_INPUT_RECEIVER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LocationLandmarks.h"

#include "EditorConfig.h"

namespace Bru
{

//===========================================================================//

const string LocationLandmarks::PathToLandmarks = "Landmarks";
const string LocationLandmarks::PathToLandmarksIsVisible = PathToLandmarks + ".IsVisible";
const string LocationLandmarks::PathToLandmarkType = "Type";
const string LocationLandmarks::PathToLandmarkColor = "Color";
const string LocationLandmarks::PathToLineThickness = "LineThickness";
const string LocationLandmarks::PathToLandmarkStartPoint = "StartPoint";
const string LocationLandmarks::PathToLandmarkEndPoint = "EndPoint";
const string LocationLandmarks::PathToTileWidth = "TileWidth";
const string LocationLandmarks::PathToTileHeight = "TileHeight";
const string LocationLandmarks::PathToTilesCountByWidth = "TilesCountByWidth";
const string LocationLandmarks::PathToTilesCountByHeight = "TilesCountByHeight";

const string LocationLandmarks::LineType = "Line";
const string LocationLandmarks::RectangleType = "Rectangle";
const string LocationLandmarks::GridType = "Grid";

//===========================================================================//

LocationLandmarks::LocationLandmarks(const string & domainName, const string & landmarksLayerName) :
	_domainName(domainName),
	_landmarksLayerName(landmarksLayerName),
	_landmarks()
{
	CreateLandmarks();
	EditorConfig->Get<bool>(PathToLandmarksIsVisible) ? Show() : Hide();
}

//===========================================================================//

LocationLandmarks::~LocationLandmarks()
{
	_landmarks.Clear();
}

//===========================================================================//

void LocationLandmarks::Show()
{
	for (const auto & landmark : _landmarks)
	{
		landmark->Show();
	}
}

//===========================================================================//

void LocationLandmarks::Hide()
{
	for (const auto & landmark : _landmarks)
	{
		landmark->Hide();
	}
}

//===========================================================================//

bool LocationLandmarks::IsVisible() const
{
	if (_landmarks.IsEmpty())
	{
		return false;
	}

	return _landmarks.GetFirst()->IsVisible();
}

//===========================================================================//

void LocationLandmarks::SwapVisibility()
{
	IsVisible() ? Hide() : Show();
}

//===========================================================================//

void LocationLandmarks::CreateLandmarks()
{
	if (!EditorConfig->ContainsSection(PathToLandmarks))
	{
		return;
	}

	integer landmarksCount = EditorConfig->GetListChildren(PathToLandmarks);
	for (integer i = 0; i < landmarksCount; ++i)
	{
		const string landmarkType = EditorConfig->Get<string>(string::Format(PathToLandmarks + ".{0}." + PathToLandmarkType, i));
		const Color color = EditorConfig->Get<Color>(string::Format(PathToLandmarks + ".{0}." + PathToLandmarkColor, i));
		const float lineThickness = EditorConfig->Get<float>(string::Format(PathToLandmarks + ".{0}." + PathToLineThickness, i));

		if (landmarkType == LineType)
		{
			const Vector3 startPoint = EditorConfig->Get<Vector3>(string::Format(PathToLandmarks + ".{0}." + PathToLandmarkStartPoint, i));
			const Vector3 endPoint = EditorConfig->Get<Vector3>(string::Format(PathToLandmarks + ".{0}." + PathToLandmarkEndPoint, i));
			_landmarks.Add(new RenderableLine(_domainName, _landmarksLayerName, {startPoint, endPoint}, color, lineThickness));
		}
		else if (landmarkType == RectangleType)
		{
			const Vector3 startPoint = EditorConfig->Get<Vector3>(string::Format(PathToLandmarks + ".{0}." + PathToLandmarkStartPoint, i));
			const Vector3 endPoint = EditorConfig->Get<Vector3>(string::Format(PathToLandmarks + ".{0}." + PathToLandmarkEndPoint, i));
			_landmarks.Add(new RenderableRectangle(_domainName, _landmarksLayerName, startPoint, endPoint, false, color, lineThickness));
		}
		else if (landmarkType == GridType)
		{
			const Vector3 startPoint = EditorConfig->Get<Vector3>(string::Format(PathToLandmarks + ".{0}." + PathToLandmarkStartPoint, i));
			const float tileWidth = EditorConfig->Get<float>(string::Format(PathToLandmarks + ".{0}." + PathToTileWidth, i));
			const float tileHeight = EditorConfig->Get<float>(string::Format(PathToLandmarks + ".{0}." + PathToTileHeight, i));
			const integer tilesCountByWidth = EditorConfig->Get<integer>(string::Format(PathToLandmarks + ".{0}." + PathToTilesCountByWidth, i));
			const integer tilesCountByHeight = EditorConfig->Get<integer>(string::Format(PathToLandmarks + ".{0}." + PathToTilesCountByHeight, i));

			const float totalWidth = tileWidth * tilesCountByWidth;
			const float totalHeight = tileHeight * tilesCountByHeight;

			//Горизонтальные линии линии
			float y = 0.0f;
			for (integer yIndex = 0; yIndex <= tilesCountByHeight; ++yIndex)
			{
				const Vector3 lineStartPoint(startPoint.X, startPoint.Y + y, startPoint.Z);
				const Vector3 lineEndPoint(lineStartPoint.X + totalWidth, lineStartPoint.Y, lineStartPoint.Z);
				_landmarks.Add(new RenderableLine(_domainName, _landmarksLayerName, {lineStartPoint, lineEndPoint}, color, lineThickness));
				y += tileHeight;
			}

			//Вертикальные линии
			float x = 0.0f;
			for (integer xIndex = 0; xIndex <= tilesCountByWidth; ++xIndex)
			{
				const Vector3 lineStartPoint(startPoint.X + x, startPoint.Y, startPoint.Z);
				const Vector3 lineEndPoint(lineStartPoint.X, lineStartPoint.Y + totalHeight, lineStartPoint.Z);
				_landmarks.Add(new RenderableLine(_domainName, _landmarksLayerName, {lineStartPoint, lineEndPoint}, color, lineThickness));
				x += tileWidth;
			}
		}
		else
		{
			INVALID_ARGUMENT_EXCEPTION(string::Format("Unkhown landmark type: \"{0}\"", landmarkType));
		}
	}
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, предоставляющий удобный доступ к настройкам редактора
//

#pragma once
#ifndef EDITOR_CONFIG_H
#define EDITOR_CONFIG_H

#include "../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class EditorConfigSingleton : public ParametersFile
{
public:
	EditorConfigSingleton();
	~EditorConfigSingleton();

private:
	static const string PathToEditorConfig;

	EditorConfigSingleton(const EditorConfigSingleton &) = delete;
	EditorConfigSingleton & operator =(const EditorConfigSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<EditorConfigSingleton> EditorConfig;

//===========================================================================//

} // namespace Bru

#endif // EDITOR_CONFIG_H

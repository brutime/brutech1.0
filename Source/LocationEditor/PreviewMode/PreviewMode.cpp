//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PreviewMode.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const Array<integer> PreviewMode::ControllersForPreviewMode = {AnimationController::TypeID};

//===========================================================================//

PreviewMode::PreviewMode() : ILocationEditorState()
{
}

//===========================================================================//

PreviewMode::~PreviewMode()
{
}

//===========================================================================//

void PreviewMode::Select()
{
	for (const string & groupName : EntityManager->GetSavingGroupsNames())
	{
		EntityManager->EnableGroupComponents<Updatable>(groupName, ControllersForPreviewMode);
	}
}

//===========================================================================//

void PreviewMode::Leave()
{
	for (const string & groupName : EntityManager->GetSavingGroupsNames())
	{
		EntityManager->ResetGroupComponents(groupName, ControllersForPreviewMode);
		EntityManager->DisableGroupComponents<Updatable>(groupName, ControllersForPreviewMode);
	}
}

//===========================================================================//

void PreviewMode::OnFileOpened()
{
	if (LocationEditor->GetCrrentEditorState() == LocationEditorState::PreviewMode)
	{
		Select();	
	}	
}

//===========================================================================//

} // namespace Bru

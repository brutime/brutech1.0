//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef PREVIEW_MODE_H
#define PREVIEW_MODE_H

#include "../../Helpers/Engine/Engine.h"

#include "../ILocationEditorState.h"

namespace Bru
{

//===========================================================================//

class PreviewMode : public ILocationEditorState
{
public:
	PreviewMode();
	virtual ~PreviewMode();

	virtual void Select();
	virtual void Leave();
	
	virtual void OnFileOpened();

private:
	static const Array<integer> ControllersForPreviewMode;

	PreviewMode(const PreviewMode &) = delete;
	PreviewMode & operator =(const PreviewMode &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PREVIEW_MODE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LocationEditor.h"

namespace Bru
{

//===========================================================================//

SharedPtr<LocationEditorSingleton> LocationEditor;

//===========================================================================//

const string LocationEditorSingleton::Name = "Location editor state";
const string LocationEditorSingleton::DomainName = "LocationEditor";

const string LocationEditorSingleton::LandmarksLayerName = "Landmarks";
const string LocationEditorSingleton::NavigationMeshLayerName = "NavigationMesh";
const string LocationEditorSingleton::HUDLayerName = "HUD";

const string LocationEditorSingleton::PathToLocationEditor = "LocationEditor";
const string LocationEditorSingleton::PathToState = "State";
const string LocationEditorSingleton::PathToGameplayLayerName = "GameplayLayerName";

const string LocationEditorSingleton::PathToNewLocationDefaults = "NewLocationDefaults";
const string LocationEditorSingleton::PathToNewLocationDefaultWidth = PathToNewLocationDefaults + ".Width";
const string LocationEditorSingleton::PathToNewLocationDefaultHeight = PathToNewLocationDefaults + ".Height";
const string LocationEditorSingleton::PathToNewLocationDefaultOffset = PathToNewLocationDefaults + ".Offset";
const string LocationEditorSingleton::PathToNewLocationDefaultLayersNames = PathToNewLocationDefaults + ".LayersNames";
const string LocationEditorSingleton::PathToNewLocationDefaultPurityLogicEntities = PathToNewLocationDefaults + ".PurityLogicEntities";

const string LocationEditorSingleton::AutoSavePostfix = "AutoSave";

//===========================================================================//

LocationEditorSingleton::LocationEditorSingleton(const SharedPtr<ILocationEditorState> & gameplayMode) :
	_gameplayLayerName(),
	_location(),
	_locationObserver(),
	_locationObserverInputReceiver(),
	_isLocationDirty(false),
	_locationLandmarks(),
	_states(),
	_currentEditorState(),
	_inputReceiver(),
	_navigationMeshVisualizer(),
	_entityCreatedEventHandler(new EventHandler<const EntityManagerEventArgs &>(this, &LocationEditorSingleton::OnEntityCreatedOrRemoved)),
	_entityRemovedEventHandler(new EventHandler<const EntityManagerEventArgs &>(this, &LocationEditorSingleton::OnEntityCreatedOrRemoved))
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	EditorConfig = new EditorConfigSingleton();

	_gameplayLayerName = EditorConfig->Get<string>(LocationEditorSingleton::PathToGameplayLayerName);

	DomainManager->Add(DomainName);

	Renderer->AddLayerToFront(DomainName, new RenderableLayer(LandmarksLayerName));
	Renderer->AddLayerToFront(DomainName, new RenderableLayer(NavigationMeshLayerName));
	Renderer->AddLayerToFront(DomainName, new RenderableLayer(HUDLayerName));

	//EntityManager->AddGroups(EntityManager->GetGroupsNames());
	EntityManager->AddGroup(NavigationMesh::GroupName);

	_locationLandmarks = new LocationLandmarks(DomainName, LandmarksLayerName);

	_states.Resize(static_cast<integer>(LocationEditorState::Count));
	_states[static_cast<integer>(LocationEditorState::ArrangementEditor)] = new ArrangementEditor(DomainName, _gameplayLayerName);
	_states[static_cast<integer>(LocationEditorState::NavigationMeshEditor)] = new NavigationMeshEditor(DomainName);
	_states[static_cast<integer>(LocationEditorState::PreviewMode)] = new PreviewMode();
	//_states[static_cast<integer>(LocationEditorState::GameplayMode)] = new GameplayMode();
	_states[static_cast<integer>(LocationEditorState::GameplayMode)] = gameplayMode;

	DomainManager->Select(DomainName);

	EntityManager->EntityCreatedEvent.AddHandler(_entityCreatedEventHandler);
	EntityManager->EntityRemovedEvent.AddHandler(_entityRemovedEventHandler);

	RegisterCommands();

	_inputReceiver = new LocationEditorInputReceiver(EntitySystem::GlobalDomainName);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

LocationEditorSingleton::~LocationEditorSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	_inputReceiver = NullPtr;

	EntityManager->EntityCreatedEvent.RemoveHandler(_entityCreatedEventHandler);
	EntityManager->EntityRemovedEvent.RemoveHandler(_entityRemovedEventHandler);

	UnregisterCommands();

	EditorConfig->Set<integer>(PathToState, static_cast<integer>(_currentEditorState));
	EditorConfig->Set<bool>(LocationLandmarks::PathToLandmarksIsVisible, _locationLandmarks->IsVisible());

	integer currentEditorStateIndex = static_cast<integer>(_currentEditorState);
	if (_states[currentEditorStateIndex] != NullPtr)
	{
		_states[currentEditorStateIndex]->Leave();
	}
	
	CloseFile();	

	for (integer i = static_cast<integer>(LocationEditorState::Count) - 1; i > static_cast<integer>(LocationEditorState::None); --i)
	{
		_states[i] = NullPtr;
	}
	_states.Clear();	
	_currentEditorState = LocationEditorState::None;

	_locationLandmarks = NullPtr;

	//EntityManager->RemoveGroups(EntityManager->GetGroupsNames());
	EntityManager->RemoveGroup(NavigationMesh::GroupName);

	Renderer->RemoveLayer(DomainName, LandmarksLayerName);
	Renderer->RemoveLayer(DomainName, NavigationMeshLayerName);
	Renderer->RemoveLayer(DomainName, HUDLayerName);

	DomainManager->Remove(DomainName);

	EditorConfig->Save();
	EditorConfig->Close();
	EditorConfig = NullPtr;

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void LocationEditorSingleton::NewFile()
{
	float width = EditorConfig->Get<float>(PathToNewLocationDefaultWidth);
	float height = EditorConfig->Get<float>(PathToNewLocationDefaultHeight);
	Vector2 offset = EditorConfig->Get<Vector2>(PathToNewLocationDefaultOffset);
	NewFile(width, height, offset);
}

//===========================================================================//

void LocationEditorSingleton::NewFile(float width, float height, const Vector2 & offset)
{
	if (_location != NullPtr)
	{
		CloseFile();
	}

	Array<string> layersNames = EditorConfig->GetArray<string>(PathToNewLocationDefaultLayersNames);
	Array<string> purityLogicEntitiesInfos = EditorConfig->GetArray<string>(PathToNewLocationDefaultPurityLogicEntities);
	_location = new Location(DomainName, width, height, offset, layersNames, purityLogicEntitiesInfos);

	CreateLocationObserver();
	Engine->ResetTiming();
}

//===========================================================================//

void LocationEditorSingleton::NewFile(float width, float height, float offsetX, float offsetY)
{
	NewFile(width, height, {offsetX, offsetY});
}

//===========================================================================//

void LocationEditorSingleton::OpenFile(const string & pathToFile)
{
	if (!FileSystem::FileExists(PathHelper::PathToData + PathHelper::GetFullPathToLocationFile(pathToFile) + "." + PathHelper::ParametersFileExtension))
	{
		Console->Error(string::Format("Location \"{0}\" not found", pathToFile));
		return;
	}

	if (_location != NullPtr)
	{
		CloseFile();
	}

	_location = new Location(DomainName, pathToFile, false);

	CreateLocationObserver();
	SwitchToStatesInput();

	for (const string & groupName : EntityManager->GetGroupsNames())
	{
		EntityManager->DisableGroupDerivedComponents<Updatable>(groupName);
		EntityManager->DisableGroupDerivedComponents<InputReceiver>(groupName);
	}

	_isLocationDirty = false;

	for (integer i = static_cast<integer>(LocationEditorState::None) + 1; i < static_cast<integer>(LocationEditorState::Count); ++i)
	{
		_states[i]->OnFileOpened();
	}

	Engine->ResetTiming();
}

//===========================================================================//

void LocationEditorSingleton::SaveFile()
{
	if (_location->GetPathToFile().IsEmpty())
	{
		Console->Error("Use SaveAs command first");
		ConsoleView->Open();
		return;
	}

	SaveAsFile(_location->GetPathToFile());
}

//===========================================================================//

void LocationEditorSingleton::SaveAsFile(const string & pathToFile)
{
	_location->SaveToFile(pathToFile);

	_isLocationDirty = false;

	auto locationsNames = GetLocationsNames();

	ScriptSystem->GetCommand("Open")->BoundParameterValues(0, locationsNames);
	ScriptSystem->GetCommand("SaveAs")->BoundParameterValues(0, locationsNames);
}

//===========================================================================//

void LocationEditorSingleton::CloseFile()
{
	if (_navigationMeshVisualizer != NullPtr)
	{
		HideNavigationMesh();
	}

	DestroyLocationObserver();

	if (_isLocationDirty)
	{
		string pathToFile;
		if (_location->GetPathToFile().IsEmpty())
		{
			pathToFile = DateTimeHelper::GetCurrentLocalDateAndTime().Replace(".", "-").Replace(":", "-").Replace(" ", "_");
		}
		else
		{
			pathToFile = _location->GetPathToFile();
		}

		if (!pathToFile.Contains(AutoSavePostfix))
		{
			pathToFile += string(".") + AutoSavePostfix;
		}

		_location->SaveToFile(pathToFile);
	}

	_location = NullPtr;
	_isLocationDirty = false;

	for (integer i = static_cast<integer>(LocationEditorState::None) + 1; i < static_cast<integer>(LocationEditorState::Count); ++i)
	{
		_states[i]->OnFileClosed();
	}
}

//===========================================================================//

Vector3 LocationEditorSingleton::GetMousePoint(float x, float y, float targetZ) const
{
	Vector3 cameraPosition = _locationObserver->GetCamera()->GetPosition();
	ProjectionMode projectionMode = Renderer->GetLayer(DomainName, _gameplayLayerName)->GetProjectionMode();
	return CoordsHelper::SurfaceToWorld({x, y}, targetZ, cameraPosition, projectionMode);
}

//===========================================================================//

float LocationEditorSingleton::GetMouseDelta(float delta, float targetZ) const
{
	ProjectionMode projectionMode = Renderer->GetLayer(DomainName, _gameplayLayerName)->GetProjectionMode();
	Vector2 translatedPosition(Renderer->PixelsToCoords(projectionMode, Vector2(delta, delta)));
	Vector3 cameraPosition = LocationEditor->GetLocationObserver()->GetCamera()->GetPosition();
	return CoordsHelper::GetProjection(translatedPosition, cameraPosition.Z - targetZ, targetZ).Y;
}

//===========================================================================//

void LocationEditorSingleton::ShowNavigationMesh()
{
	_navigationMeshVisualizer = new NavigationMeshVisualizer(DomainName, NavigationMeshLayerName,
	                                                         NavigationMesh::GroupName, _location->GetNavigationMesh());
}

//===========================================================================//

void LocationEditorSingleton::HideNavigationMesh()
{
	_navigationMeshVisualizer = NullPtr;
}

//===========================================================================//

bool LocationEditorSingleton::IsNavigationMeshVisible() const
{
	return _navigationMeshVisualizer != NullPtr;
}

//===========================================================================//

void LocationEditorSingleton::SelectState(LocationEditorState newEditorState)
{
	if (_currentEditorState == newEditorState)
	{
		return;
	}

	integer currentEditorStateIndex = static_cast<integer>(_currentEditorState);
	if (_currentEditorState != LocationEditorState::None)
	{
		_states[currentEditorStateIndex]->Leave();
	}

	_currentEditorState = newEditorState;

	integer newCurrentEditorStateIndex = static_cast<integer>(_currentEditorState);
	if (_currentEditorState != LocationEditorState::None)
	{
		_states[newCurrentEditorStateIndex]->Select();
	}

	SwitchToStatesInput();
}

//===========================================================================//

void LocationEditorSingleton::SelectState(integer newEditorStateValue)
{
	SelectState(GetLocationEditorStateByIntegerValue(newEditorStateValue));
}

//===========================================================================//

LocationEditorState LocationEditorSingleton::GetCrrentEditorState() const
{
	return _currentEditorState;
}

//===========================================================================//

void LocationEditorSingleton::SwitchToLocationObserverInput()
{
	_states[static_cast<integer>(_currentEditorState)]->DisableInput();

	_locationObserverInputReceiver->Enable();
}

//===========================================================================//

void LocationEditorSingleton::SwitchToStatesInput()
{
	_states[static_cast<integer>(_currentEditorState)]->EnableInput();

	_locationObserverInputReceiver->Disable();
}

//===========================================================================//

SharedPtr<ArrangementEditor> LocationEditorSingleton::GetArrangementEditor() const
{
	integer arrangementEditorIndex = static_cast<integer>(LocationEditorState::ArrangementEditor);
	return static_pointer_cast<ArrangementEditor>(_states[arrangementEditorIndex]);
}

//===========================================================================//

SharedPtr<NavigationMeshEditor> LocationEditorSingleton::GetNavigationMeshEditor() const
{
	integer navigationMeshEditorIndex = static_cast<integer>(LocationEditorState::NavigationMeshEditor);
	return static_pointer_cast<NavigationMeshEditor>(_states[navigationMeshEditorIndex]);
}

//===========================================================================//

SharedPtr<PreviewMode> LocationEditorSingleton::GetPreviewMode() const
{
	integer previewModeIndex = static_cast<integer>(LocationEditorState::PreviewMode);
	return static_pointer_cast<PreviewMode>(_states[previewModeIndex]);
}

//===========================================================================//

bool LocationEditorSingleton::HasGameplayMode() const
{
	return _states[static_cast<integer>(LocationEditorState::GameplayMode)] != NullPtr;
}

//===========================================================================//

const SharedPtr<Location> & LocationEditorSingleton::GetLocation() const
{
	return _location;
}

//===========================================================================//

const SharedPtr<CapturedMovingLocationObserver> & LocationEditorSingleton::GetLocationObserver() const
{
	return _locationObserver;
}

//===========================================================================//

const SharedPtr<LocationLandmarks> LocationEditorSingleton::GetLocationLandmarks() const
{
	return _locationLandmarks;
}

//===========================================================================//

void LocationEditorSingleton::CreateLocationObserver()
{
	_locationObserver = new CapturedMovingLocationObserver(DomainName, _location, new DirectComponentDataProvider("CapturedMovingLocationObserver"));
	_locationObserverInputReceiver = new CapturedMovingLocationObserverInputReceiver(DomainName, _locationObserver);

	Vector3 cameraPosition = _locationObserver->GetCamera()->GetPosition();
	cameraPosition.Z = 16.85f;

	_locationObserver->GetCamera()->Set2DPosition(cameraPosition);
	_locationObserver->SetZoomTarget(cameraPosition.Z);
}

//===========================================================================//

void LocationEditorSingleton::DestroyLocationObserver()
{	
	_locationObserverInputReceiver = NullPtr;
	_locationObserver = NullPtr;
}

//===========================================================================//

void LocationEditorSingleton::OnEntityCreatedOrRemoved(const EntityManagerEventArgs &)
{
	_isLocationDirty = true;
}

//===========================================================================//

Array<string> LocationEditorSingleton::GetLocationsNames() const
{
	Array<string> locationsNames = FileSystem::GetFilesListRecursively(PathHelper::PathToData + PathHelper::PathToLocations);
	locationsNames.Remove(PathHelper::LocationsListFileName + "." + PathHelper::ParametersFileExtension);
	for (auto & locationName : locationsNames)
	{
		locationName = locationName.Replace(string(".") + PathHelper::ParametersFileExtension, string::Empty);
	}
	return locationsNames;
}

//===========================================================================//

void LocationEditorSingleton::RegisterCommands()
{
	//New
	ScriptSystem->RegisterCommand(new ClassCommand<LocationEditorSingleton, void>("New", this, &LocationEditorSingleton::NewFile));
	SharedPtr<IScriptCommand> newLocationCommand = new ClassCommand<LocationEditorSingleton, void, float, float, float, float>(
	    "New", this, &LocationEditorSingleton::NewFile);
	newLocationCommand->BoundParameterValues(0, {EditorConfig->Get<float>(PathToNewLocationDefaultWidth)});
	newLocationCommand->BoundParameterValues(1, {EditorConfig->Get<float>(PathToNewLocationDefaultHeight)});
	newLocationCommand->BoundParameterValues(2, {EditorConfig->Get<Vector2>(PathToNewLocationDefaultOffset).X});
	newLocationCommand->BoundParameterValues(3, {EditorConfig->Get<Vector2>(PathToNewLocationDefaultOffset).Y});
	ScriptSystem->RegisterCommand(newLocationCommand);

	auto locationsNames = GetLocationsNames();

	//Open
	SharedPtr<IScriptCommand> openLocationCommand = new ClassCommand<LocationEditorSingleton, void, const string &>(
	    "Open", this, &LocationEditorSingleton::OpenFile);
	openLocationCommand->BoundParameterValues(0, locationsNames);
	ScriptSystem->RegisterCommand(openLocationCommand);

	//Save
	ScriptSystem->RegisterCommand(new ClassCommand<LocationEditorSingleton, void>("Save", this, &LocationEditorSingleton::SaveFile));

	//SaveAs
	SharedPtr<IScriptCommand> saveAsLocationCommand = new ClassCommand<LocationEditorSingleton, void, const string &>(
	    "SaveAs", this, &LocationEditorSingleton::SaveAsFile);
	saveAsLocationCommand->BoundParameterValues(0, locationsNames);
	ScriptSystem->RegisterCommand(saveAsLocationCommand);
}

//===========================================================================//

void LocationEditorSingleton::UnregisterCommands()
{
	ScriptSystem->UnregisterCommand("New");
	ScriptSystem->UnregisterCommand("Open");
	ScriptSystem->UnregisterCommand("Save");
	ScriptSystem->UnregisterCommand("SaveAs");
}

//===========================================================================//

LocationEditorState LocationEditorSingleton::GetLocationEditorStateByIntegerValue(integer value)
{
	switch (value)
	{
	case 0: return LocationEditorState::None;
	case 1: return LocationEditorState::ArrangementEditor;
	case 2: return LocationEditorState::NavigationMeshEditor;
	case 3: return LocationEditorState::PreviewMode;
	case 4: return LocationEditorState::GameplayMode;

	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("value {0} not defined", static_cast<integer>(value)));
	}
}

//===========================================================================//

} // namespace Bru

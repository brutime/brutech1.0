//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LocationEditorInputReceiver.h"

#include "LocationEditor.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(LocationEditorInputReceiver, InputReceiver)

//===========================================================================//

LocationEditorInputReceiver::LocationEditorInputReceiver(const string & domainName) :
	InputReceiver(domainName)
{
}

//===========================================================================//

LocationEditorInputReceiver::~LocationEditorInputReceiver()
{
}

//===========================================================================//

void LocationEditorInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	if (ConsoleView->IsActive())
	{
		return;
	}
	
	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::Digit1:
			LocationEditor->SelectState(LocationEditorState::ArrangementEditor);
			break;

		case KeyboardKey::Digit2:
			LocationEditor->SelectState(LocationEditorState::NavigationMeshEditor);
			break;

		case KeyboardKey::Digit3:
			LocationEditor->SelectState(LocationEditorState::PreviewMode);
			break;
			
		case KeyboardKey::Digit4:
			if (LocationEditor->HasGameplayMode())
			{
				LocationEditor->SelectState(LocationEditorState::GameplayMode);	
			}			
			break;

		case KeyboardKey::Space:
			LocationEditor->SwitchToLocationObserverInput();
			break;
			
		case KeyboardKey::S:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				LocationEditor->SaveFile();	
			}			
			break;			

		case KeyboardKey::L:
			LocationEditor->GetLocationLandmarks()->SwapVisibility();
			break;

		default:
			break;
		}
	}
	else if (args.State == KeyState::Up)
	{
		switch (args.Key)
		{
		case KeyboardKey::Space:
			LocationEditor->SwitchToStatesInput();
			break;

		default:
			break;
		}
	}
}

//===========================================================================//

void LocationEditorInputReceiver::DispatchMouseEvent(const MouseEventArgs &)
{
	if (ConsoleView->IsActive())
	{
		return;
	}	
}

//===========================================================================//

void LocationEditorInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
	if (ConsoleView->IsActive())
	{
		return;
	}	
}

//===========================================================================//

void LocationEditorInputReceiver::LostFocus()
{
	LocationEditor->SwitchToStatesInput();
}

//===========================================================================//

} // namespace Bru

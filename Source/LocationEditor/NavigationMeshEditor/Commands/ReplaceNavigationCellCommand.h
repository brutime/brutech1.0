//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef REPLACE_NAVIGATION_CELL_COMMAND_H
#define REPLACE_NAVIGATION_CELL_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class ReplaceNavigationCellCommand : public Command
{
public:
	ReplaceNavigationCellCommand(const Vector3 & removingStartPoint, const Vector3 & removingEndPoint,
	                             const Vector3 & addingStartPoint, const Vector3 & addingEndPoint);
	virtual ~ReplaceNavigationCellCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	Vector3 _removingStartPoint;
	Vector3 _removingEndPoint;
	Vector3 _addingStartPoint;
	Vector3 _addingEndPoint;

	ReplaceNavigationCellCommand(const ReplaceNavigationCellCommand &) = delete;
	ReplaceNavigationCellCommand & operator =(const ReplaceNavigationCellCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // REPLACE_NAVIGATION_CELL_COMMAND_H

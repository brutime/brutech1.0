//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ReplaceNavigationCellCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

ReplaceNavigationCellCommand::ReplaceNavigationCellCommand(const Vector3 & removingStartPoint,
        const Vector3 & removingEndPoint, const Vector3 & addingStartPoint, const Vector3 & addingEndPoint) :
	Command(true),
	_removingStartPoint(removingStartPoint),
	_removingEndPoint(removingEndPoint),
	_addingStartPoint(addingStartPoint),
	_addingEndPoint(addingEndPoint)
{
}

//===========================================================================//

ReplaceNavigationCellCommand::~ReplaceNavigationCellCommand()
{
}

//===========================================================================//

void ReplaceNavigationCellCommand::Execute()
{
	const SharedPtr<NavigationMesh> & navigationMesh = LocationEditor->GetLocation()->GetNavigationMesh();
	navigationMesh->RemoveCell(_removingStartPoint, _removingEndPoint);
	navigationMesh->AddCell(_addingStartPoint, _addingEndPoint);
}

//===========================================================================//

void ReplaceNavigationCellCommand::Cancel()
{
	const SharedPtr<NavigationMesh> & navigationMesh = LocationEditor->GetLocation()->GetNavigationMesh();
	navigationMesh->RemoveCell(_addingStartPoint, _addingEndPoint);
	navigationMesh->AddCell(_removingStartPoint, _removingEndPoint);
}

//===========================================================================//

} // namespace Bru

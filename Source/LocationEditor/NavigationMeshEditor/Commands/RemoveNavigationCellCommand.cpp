//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "RemoveNavigationCellCommand.h"

#include "../../LocationEditor.h"

namespace Bru
{

//===========================================================================//

RemoveNavigationCellCommand::RemoveNavigationCellCommand(const Vector3 & startPoint, const Vector3 & endPoint) :
	Command(true),
	_startPoint(startPoint),
	_endPoint(endPoint)
{
}

//===========================================================================//

RemoveNavigationCellCommand::~RemoveNavigationCellCommand()
{
}

//===========================================================================//

void RemoveNavigationCellCommand::Execute()
{
	const SharedPtr<NavigationMesh> & navigationMesh = LocationEditor->GetLocation()->GetNavigationMesh();
	navigationMesh->RemoveCell(_startPoint, _endPoint);
}

//===========================================================================//

void RemoveNavigationCellCommand::Cancel()
{
	const SharedPtr<NavigationMesh> & navigationMesh = LocationEditor->GetLocation()->GetNavigationMesh();
	navigationMesh->AddCell(_startPoint, _endPoint);
	Command::Cancel();
}

//===========================================================================//

} // namespace Bru

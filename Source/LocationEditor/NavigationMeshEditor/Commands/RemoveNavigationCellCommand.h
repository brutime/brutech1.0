//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef REMOVE_NAVIGATION_CELL_COMMAND_H
#define REMOVE_NAVIGATION_CELL_COMMAND_H

#include "../../../Helpers/Engine/Engine.h"

namespace Bru
{

//===========================================================================//

class RemoveNavigationCellCommand : public Command
{
public:
	RemoveNavigationCellCommand(const Vector3 & startPoint, const Vector3 & endPoint);
	virtual ~RemoveNavigationCellCommand();

protected:
	virtual void Execute();
	virtual void Cancel();

private:
	Vector3 _startPoint;
	Vector3 _endPoint;

	RemoveNavigationCellCommand(const RemoveNavigationCellCommand &) = delete;
	RemoveNavigationCellCommand & operator =(const RemoveNavigationCellCommand &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // REMOVE_NAVIGATION_CELL_COMMAND_H

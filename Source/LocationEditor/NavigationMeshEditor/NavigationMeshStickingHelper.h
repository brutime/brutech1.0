//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef NAVIGATION_MESH_STICKING_HELPER_H
#define NAVIGATION_MESH_STICKING_HELPER_H

#include "../../Helpers/Engine/Engine.h"
#include "../../GameHelpers/GameHelpers.h"

namespace Bru
{

//===========================================================================//

class NavigationMeshStickingHelper
{
public:
	NavigationMeshStickingHelper(const SharedPtr<NavigationMesh> & navigationMesh);
	~NavigationMeshStickingHelper();

	//Если точка ни к чему ни прилипает - возвращает ее саму
	Vector3 GetAnyStickingPoint(const Vector3 & currentPoint) const;

private:
	static const float PointStickRadius;
	static const float PointRayStep;
	
	SharedPtr<NavigationMeshCell> GetCenterCell(const Vector3 & currentPoint) const;
	SharedPtr<NavigationMeshCell> GetLeftCell(const Vector3 & currentPoint) const;
	SharedPtr<NavigationMeshCell> GetRightCell(const Vector3 & currentPoint) const;
	SharedPtr<NavigationMeshCell> GetUpCell(const Vector3 & currentPoint) const;
	SharedPtr<NavigationMeshCell> GetDownCell(const Vector3 & currentPoint) const;

	SharedPtr<NavigationMesh> _navigationMesh;

	NavigationMeshStickingHelper(const NavigationMeshStickingHelper &) = delete;
	NavigationMeshStickingHelper & operator =(const NavigationMeshStickingHelper &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // NAVIGATION_MESH_STICKING_HELPER_H

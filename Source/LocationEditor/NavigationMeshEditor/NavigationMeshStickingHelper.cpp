//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "NavigationMeshStickingHelper.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const float NavigationMeshStickingHelper::PointStickRadius = 0.2f;
const float NavigationMeshStickingHelper::PointRayStep = 0.08f;

//===========================================================================//

NavigationMeshStickingHelper::NavigationMeshStickingHelper(const SharedPtr<NavigationMesh> & navigationMesh) :
	_navigationMesh(navigationMesh)
{
}

//===========================================================================//

NavigationMeshStickingHelper::~NavigationMeshStickingHelper()
{
}

//===========================================================================//

Vector3 NavigationMeshStickingHelper::GetAnyStickingPoint(const Vector3 & currentPoint) const
{
	List<SharedPtr<NavigationMeshCell>> cells;
	
	const auto & centerCell = GetCenterCell(currentPoint);
	if (centerCell != NullPtr)
	{
		cells.Add(centerCell);
	}	

	const auto & leftCell = GetLeftCell(currentPoint);
	if (leftCell != NullPtr)
	{
		cells.Add(leftCell);
	}

	const auto & rightCell = GetRightCell(currentPoint);
	if (rightCell != NullPtr)
	{
		cells.Add(rightCell);
	}

	const auto & upCell = GetUpCell(currentPoint);
	if (upCell != NullPtr)
	{
		cells.Add(upCell);
	}

	const auto & downCell = GetDownCell(currentPoint);
	if (downCell != NullPtr)
	{
		cells.Add(downCell);
	}

	Vector3 resultVector = currentPoint;

	for (const auto & cell : cells)
	{
		if (Math::Abs(cell->LeftLowerPoint.Y - currentPoint.Y) < PointStickRadius)
		{
			resultVector.Y = cell->LeftLowerPoint.Y;
		}
		else if (Math::Abs(cell->RightUpperPoint.Y - currentPoint.Y) < PointStickRadius)
		{
			resultVector.Y = cell->RightUpperPoint.Y;
		}

		if (Math::Abs(cell->LeftLowerPoint.X - currentPoint.X) < PointStickRadius)
		{
			resultVector.X = cell->LeftLowerPoint.X;
		}
		else if (Math::Abs(cell->RightUpperPoint.X - currentPoint.X) < PointStickRadius)
		{
			resultVector.X = cell->RightUpperPoint.X;
		}
	}

	return resultVector;
}

//===========================================================================//

SharedPtr<NavigationMeshCell> NavigationMeshStickingHelper::GetCenterCell(const Vector3 & currentPoint) const
{
	const SharedPtr<LocationEditorSingleton> & editor = LocationEditor;
	for (const SharedPtr<NavigationMeshCell> & cell : editor->GetLocation()->GetNavigationMesh()->GetCells())
	{
		if (cell->ContainsPoint(currentPoint))
		{
			return cell;
		}
	}

	return NullPtr;
}

//===========================================================================//

SharedPtr<NavigationMeshCell> NavigationMeshStickingHelper::GetLeftCell(const Vector3 & currentPoint) const
{
	float beamX = currentPoint.X;
	while (beamX >= 0.0f)
	{
		for (const SharedPtr<NavigationMeshCell> & cell : LocationEditor->GetLocation()->GetNavigationMesh()->GetCells())
		{
			if (cell->LeftLowerPoint.X <= beamX && beamX <= cell->RightUpperPoint.X &&
			        cell->LeftLowerPoint.Y - PointStickRadius <= currentPoint.Y &&
			        currentPoint.Y <= cell->RightUpperPoint.Y + PointStickRadius &&
			        !cell->ContainsPoint(currentPoint))
			{
				return cell;
			}
		}
		beamX -= PointRayStep;
	}

	return NullPtr;
}

//===========================================================================//

SharedPtr<NavigationMeshCell> NavigationMeshStickingHelper::GetRightCell(const Vector3 & currentPoint) const
{
	const SharedPtr<LocationEditorSingleton> & editor = LocationEditor;
	float beamX = currentPoint.X;
	while (beamX <= editor->GetLocation()->GetWidth())
	{
		for (const SharedPtr<NavigationMeshCell> & cell : editor->GetLocation()->GetNavigationMesh()->GetCells())
		{
			if (cell->LeftLowerPoint.X <= beamX && beamX <= cell->RightUpperPoint.X &&
			        cell->LeftLowerPoint.Y - PointStickRadius <= currentPoint.Y &&
			        currentPoint.Y <= cell->RightUpperPoint.Y + PointStickRadius &&
					!cell->ContainsPoint(currentPoint))
			{
				return cell;
			}
		}
		beamX += PointRayStep;
	}

	return NullPtr;
}

//===========================================================================//

SharedPtr<NavigationMeshCell> NavigationMeshStickingHelper::GetUpCell(const Vector3 & currentPoint) const
{
	const SharedPtr<LocationEditorSingleton> & editor = LocationEditor;

	float beamY = currentPoint.Y;
	while (beamY <= editor->GetLocation()->GetHeight())
	{
		for (const SharedPtr<NavigationMeshCell> & cell : editor->GetLocation()->GetNavigationMesh()->GetCells())
		{
			if (cell->LeftLowerPoint.X - PointStickRadius <= currentPoint.X &&
			        currentPoint.X <= cell->RightUpperPoint.X + PointStickRadius &&
			        cell->LeftLowerPoint.Y <= beamY && beamY <= cell->RightUpperPoint.Y &&
					!cell->ContainsPoint(currentPoint))
			{
				return cell;
			}
		}
		beamY += PointRayStep;
	}

	return NullPtr;
}

//===========================================================================//

SharedPtr<NavigationMeshCell> NavigationMeshStickingHelper::GetDownCell(const Vector3 & currentPoint) const
{
	const SharedPtr<LocationEditorSingleton> & editor = LocationEditor;
	float beamY = currentPoint.Y;
	while (beamY >= 0.0f)
	{
		for (const SharedPtr<NavigationMeshCell> & cell : editor->GetLocation()->GetNavigationMesh()->GetCells())
		{
			if (cell->LeftLowerPoint.X - PointStickRadius <= currentPoint.X &&
			        currentPoint.X <= cell->RightUpperPoint.X + PointStickRadius &&
			        cell->LeftLowerPoint.Y <= beamY && beamY <= cell->RightUpperPoint.Y && 
					!cell->ContainsPoint(currentPoint))
			{
				return cell;
			}
		}
		beamY -= PointRayStep;
	}

	return NullPtr;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef NAVIGATION_MESH_EDITOR_INPUT_RECEIVER_H
#define NAVIGATION_MESH_EDITOR_INPUT_RECEIVER_H

#include "../../Helpers/Engine/Engine.h"
#include "../../GameHelpers/GameHelpers.h"

#include "../Enums/PullingSide.h"

namespace Bru
{

//===========================================================================//

class NavigationMeshEditorInputReceiver : public InputReceiver
{
public:
	DECLARE_FAMILY_BASE_COMPONENT

	NavigationMeshEditorInputReceiver(const string & domainName);
	virtual ~NavigationMeshEditorInputReceiver();

	virtual void DispatchKeyboardEvent(const KeyboardEventArgs & args);
	virtual void DispatchMouseEvent(const MouseEventArgs & args);
	virtual void DispatchJoystickEvent(const JoystickEventArgs & args);

private:
	void DispatchMouseEventForAddingCells(const MouseEventArgs & args);
	void DispatchMouseEventForSizingCells(const MouseEventArgs & args);
	
	Vector3 GetAnyStickingPoint(const MouseEventArgs & args) const;

	bool HasCellStartPosition() const;
	void ClearNewCellStartPosition();

	void UpdateSizingRectangle(const Vector3 & point);
	PullingSide GetPullingSide(const SharedPtr<NavigationMeshCell> & cell, const Vector3 & point) const;

	bool _isSizing;

	Vector3 _addingCellStartPosition;

	PullingSide _pullingSide;
	SharedPtr<NavigationMeshCell> _sizingCell;

	Vector3 _sizingCellStartPosition;
	Vector3 _sizingCellEndPosition;

	NavigationMeshEditorInputReceiver(const NavigationMeshEditorInputReceiver &) = delete;
	NavigationMeshEditorInputReceiver & operator =(const NavigationMeshEditorInputReceiver &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // NAVIGATION_MESH_EDITOR_INPUT_RECEIVER_H

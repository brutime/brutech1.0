//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "NavigationMeshEditorInputReceiver.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

IMPLEMENT_FAMILY_BASE_COMPONENT(NavigationMeshEditorInputReceiver, InputReceiver)

//===========================================================================//

NavigationMeshEditorInputReceiver::NavigationMeshEditorInputReceiver(const string & domainName) :
	InputReceiver(domainName),
	_isSizing(false),
	_addingCellStartPosition(),
	_pullingSide(),
	_sizingCell(),
	_sizingCellStartPosition(),
	_sizingCellEndPosition()
{
	ClearNewCellStartPosition();
	LocationEditor->GetNavigationMeshEditor()->SetEditCursorPosition(LocationEditor->GetMousePoint(Mouse->GetX(), Mouse->GetY()));	
}

//===========================================================================//

NavigationMeshEditorInputReceiver::~NavigationMeshEditorInputReceiver()
{
	ClearNewCellStartPosition();
	_sizingCellStartPosition = Vector3();
	_sizingCellEndPosition = Vector3();
	LocationEditor->GetNavigationMeshEditor()->HideCellPreviewRectangle();
}

//===========================================================================//

void NavigationMeshEditorInputReceiver::DispatchKeyboardEvent(const KeyboardEventArgs & args)
{
	const auto & editor = LocationEditor->GetNavigationMeshEditor();

	if (args.State == KeyState::Down || args.State == KeyState::RepeatedDown)
	{
		switch (args.Key)
		{
		case KeyboardKey::Z:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				editor->GetCommandsManager()->Undo();
			}
			break;

		case KeyboardKey::Y:
			if (Keyboard->IsKeyDown(KeyboardKey::Control))
			{
				editor->GetCommandsManager()->Redo();
			}
			break;

		default:
			break;
		}
	}

	if (args.State == KeyState::Down)
	{
		switch (args.Key)
		{
		case KeyboardKey::Escape:
			if (HasCellStartPosition())
			{
				ClearNewCellStartPosition();
				editor->HideCellPreviewRectangle();
			}
			break;

		case KeyboardKey::Shift:
			if (!HasCellStartPosition())
			{
				_isSizing = true;
			}
			break;

		default:
			break;
		}
	}

	if (args.State == KeyState::Up)
	{
		switch (args.Key)
		{
		case KeyboardKey::Shift:
			_isSizing = false;
			break;

		default:
			break;
		}
	}
}

//===========================================================================//

void NavigationMeshEditorInputReceiver::DispatchMouseEvent(const MouseEventArgs & args)
{
	if (_isSizing)
	{
		DispatchMouseEventForSizingCells(args);
	}
	else
	{
		DispatchMouseEventForAddingCells(args);
	}
}

//===========================================================================//

void NavigationMeshEditorInputReceiver::DispatchJoystickEvent(const JoystickEventArgs &)
{
}

//===========================================================================//

void NavigationMeshEditorInputReceiver::DispatchMouseEventForAddingCells(const MouseEventArgs & args)
{
	const auto & editor = LocationEditor->GetNavigationMeshEditor();
	Vector3 point = GetAnyStickingPoint(args);

	if (args.Key == MouseKey::Move)
	{
		editor->SetEditCursorPosition(point);

		if (HasCellStartPosition())
		{
			editor->SetCellPreviewRectangleEndPosition(point);
		}
	}

	if (args.State == KeyState::Down)
	{
		if (args.Key == MouseKey::Left)
		{
			if (!HasCellStartPosition())
			{
				_addingCellStartPosition = point;
				editor->SetCellPreviewRectangleStartPosition(point);
				editor->SetCellPreviewRectangleEndPosition(point);
				editor->ShowCellPreviewRectangle();
			}
			else
			{
				editor->GetCommandsManager()->Execute(new AddNavigationCellCommand(_addingCellStartPosition, point));
				ClearNewCellStartPosition();
				editor->HideCellPreviewRectangle();
			}
		}

		if (args.Key == MouseKey::Right)
		{
			SharedPtr<NavigationMeshCell> cell = editor->GetNavigationMeshCellByPoint(point, true);
			if (cell != NullPtr)
			{
				editor->GetCommandsManager()->Execute(new RemoveNavigationCellCommand(cell->LeftLowerPoint, cell->RightUpperPoint));
			}
		}
	}
}

//===========================================================================//

void NavigationMeshEditorInputReceiver::DispatchMouseEventForSizingCells(const MouseEventArgs & args)
{
	const auto & editor = LocationEditor->GetNavigationMeshEditor();
	Vector3 point = GetAnyStickingPoint(args);

	if (args.Key == MouseKey::Move)
	{
		editor->SetEditCursorPosition(point);
		UpdateSizingRectangle(point);
	}

	if (args.State == KeyState::Down)
	{
		if (args.Key == MouseKey::Left)
		{
			const SharedPtr<NavigationMesh> & navigationMesh = LocationEditor->GetLocation()->GetNavigationMesh();
			for (const SharedPtr<NavigationMeshCell> & cell : navigationMesh->GetCells())
			{
				_pullingSide = GetPullingSide(cell, point);

				if (_pullingSide != PullingSide::None)
				{
					_sizingCellStartPosition = cell->LeftLowerPoint;
					_sizingCellEndPosition = cell->RightUpperPoint;
					editor->SetCellPreviewRectangleStartPosition(_sizingCellStartPosition);
					editor->SetCellPreviewRectangleEndPosition(_sizingCellEndPosition);
					editor->ShowCellPreviewRectangle();
					_sizingCell = cell;
					break;
				}
			}
		}
	}
	else if (args.State == KeyState::Up)
	{
		if (_pullingSide != PullingSide::None)
		{
			editor->GetCommandsManager()->Execute(new ReplaceNavigationCellCommand(
			                                          _sizingCell->LeftLowerPoint, _sizingCell->RightUpperPoint, _sizingCellStartPosition, _sizingCellEndPosition));
			editor->HideCellPreviewRectangle();
			_sizingCell = NullPtr;
			_pullingSide = PullingSide::None;
		}
	}
}

//===========================================================================//

Vector3 NavigationMeshEditorInputReceiver::GetAnyStickingPoint(const MouseEventArgs & args) const
{
	const auto & editor = LocationEditor->GetNavigationMeshEditor();
	return editor->GetStickingHelper()->GetAnyStickingPoint(LocationEditor->GetMousePoint(args.X, args.Y));
}

//===========================================================================//

bool NavigationMeshEditorInputReceiver::HasCellStartPosition() const
{
	return _addingCellStartPosition != Vector3(-1.0f, -1.0f, -1.0f);
}

//===========================================================================//

void NavigationMeshEditorInputReceiver::ClearNewCellStartPosition()
{
	_addingCellStartPosition = Vector3(-1.0f, -1.0f, -1.0f);
}

//===========================================================================//

void NavigationMeshEditorInputReceiver::UpdateSizingRectangle(const Vector3 & point)
{
	switch (_pullingSide)
	{
	case PullingSide::LeftBottom:
		_sizingCellStartPosition = point;
		break;

	case PullingSide::Bottom:
		_sizingCellStartPosition.Y = point.Y;
		break;

	case PullingSide::RightBottom:
		_sizingCellStartPosition.Y = point.Y;
		_sizingCellEndPosition.X = point.X;
		break;

	case PullingSide::Right:
		_sizingCellEndPosition.X = point.X;
		break;

	case PullingSide::RightTop:
		_sizingCellEndPosition = point;
		break;

	case PullingSide::Top:
		_sizingCellEndPosition.Y = point.Y;
		break;

	case PullingSide::LeftTop:
		_sizingCellStartPosition.X = point.X;
		_sizingCellEndPosition.Y = point.Y;
		break;

	case PullingSide::Left:
		_sizingCellStartPosition.X = point.X;
		break;

	case PullingSide::None:
	default:
		break;
	}

	const auto & editor = LocationEditor->GetNavigationMeshEditor();

	editor->SetCellPreviewRectangleStartPosition(_sizingCellStartPosition);
	editor->SetCellPreviewRectangleEndPosition(_sizingCellEndPosition);
}

//===========================================================================//

PullingSide NavigationMeshEditorInputReceiver::GetPullingSide(const SharedPtr<NavigationMeshCell> & cell,
                                                              const Vector3 & point) const
{
	if (point == cell->LeftLowerPoint)
	{
		return PullingSide::LeftBottom;
	}
	else if (point == Vector3(cell->LeftLowerPoint.X, cell->RightUpperPoint.Y))
	{
		return PullingSide::LeftTop;
	}
	else if (point == cell->RightUpperPoint)
	{
		return PullingSide::RightTop;
	}
	else if (point == Vector3(cell->RightUpperPoint.X, cell->LeftLowerPoint.Y))
	{
		return PullingSide::RightBottom;
	}
	else if (cell->LeftLowerPoint.X <= point.X && point.X <= cell->RightUpperPoint.X && Math::Equals(point.Y, cell->LeftLowerPoint.Y))
	{
		return PullingSide::Bottom;
	}
	else if (cell->LeftLowerPoint.X <= point.X && point.X <= cell->RightUpperPoint.X && Math::Equals(point.Y, cell->RightUpperPoint.Y))
	{
		return PullingSide::Top;
	}
	else if (cell->LeftLowerPoint.Y <= point.Y && point.Y <= cell->RightUpperPoint.Y && Math::Equals(point.X, cell->LeftLowerPoint.X))
	{
		return PullingSide::Left;
	}
	else if (cell->LeftLowerPoint.Y <= point.Y && point.Y <= cell->RightUpperPoint.Y && Math::Equals(point.X, cell->RightUpperPoint.X))
	{
		return PullingSide::Right;
	}

	return PullingSide::None;
}

//===========================================================================//

} // namespace Bru

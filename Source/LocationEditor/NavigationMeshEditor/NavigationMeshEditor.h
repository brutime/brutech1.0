//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef NAVIGATION_MESH_EDITOR_H
#define NAVIGATION_MESH_EDITOR_H

#include "../../Helpers/Engine/Engine.h"

#include "../ILocationEditorState.h"
#include "Commands/NavigationMeshCommands.h"
#include "NavigationMeshStickingHelper.h"
#include "NavigationMeshEditorInputReceiver.h"

namespace Bru
{

//===========================================================================//

class NavigationMeshEditor : public ILocationEditorState
{
public:
	NavigationMeshEditor(const string & domainName);
	virtual ~NavigationMeshEditor();

	virtual void Select();
	virtual void Leave();
	
	virtual void EnableInput();
	virtual void DisableInput();	
	
	virtual void OnFileOpened();
	virtual void OnFileClosed();

	SharedPtr<NavigationMeshCell> GetNavigationMeshCellByPoint(const Vector3 & point, bool isStrictly) const;
	
	void SetEditCursorPosition(const Vector3 & position);

	void ShowCellPreviewRectangle();
	void HideCellPreviewRectangle();

	void SetCellPreviewRectangleStartPosition(const Vector3 & position);
	void SetCellPreviewRectangleEndPosition(const Vector3 & position);

	const SharedPtr<CommandsManager> & GetCommandsManager() const;
	const SharedPtr<NavigationMeshStickingHelper> & GetStickingHelper() const;

private:
	static const float PointsLineThickness;
	static const Color PreviewGeometryColor;
	static const float PreviewGeometryLineThickness;
	
	string _domainName;
	
	SharedPtr<RenderablePoints> _editCursor;
	SharedPtr<RenderableRectangle> _cellPreviewRectangle;
	SharedPtr<NavigationMeshStickingHelper> _stickingHelper;

	SharedPtr<NavigationMeshEditorInputReceiver> _inputReceiver;
	
	SharedPtr<CommandsManager> _commandsManager;

	NavigationMeshEditor(const NavigationMeshEditor &) = delete;
	NavigationMeshEditor & operator =(const NavigationMeshEditor &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // NAVIGATION_MESH_EDITOR_H

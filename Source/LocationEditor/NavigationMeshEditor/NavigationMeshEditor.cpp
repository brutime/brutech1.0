//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "NavigationMeshEditor.h"

#include "../LocationEditor.h"

namespace Bru
{

//===========================================================================//

const float NavigationMeshEditor::PointsLineThickness = 5.0f;
const Color NavigationMeshEditor::PreviewGeometryColor = Colors::Red;
const float NavigationMeshEditor::PreviewGeometryLineThickness = 1.0f;

//===========================================================================//

NavigationMeshEditor::NavigationMeshEditor(const string & domainName) : ILocationEditorState(),
	_domainName(domainName),
	_editCursor(),
	_cellPreviewRectangle(),
	_stickingHelper(),
	_inputReceiver(),
	_commandsManager(new CommandsManager(500))
{
	_editCursor = new RenderablePoints(_domainName, LocationEditorSingleton::HUDLayerName, {},
	                                   PreviewGeometryColor, PointsLineThickness);
	_editCursor->Hide();

	_cellPreviewRectangle = new RenderableRectangle(_domainName, LocationEditorSingleton::HUDLayerName, {}, {},
	                                                false, PreviewGeometryColor, PreviewGeometryLineThickness);
	_cellPreviewRectangle->Hide();
}

//===========================================================================//

NavigationMeshEditor::~NavigationMeshEditor()
{
	_cellPreviewRectangle = NullPtr;
	_editCursor = NullPtr;
	_commandsManager = NullPtr;
}

//===========================================================================//

void NavigationMeshEditor::Select()
{
	_stickingHelper = new NavigationMeshStickingHelper(LocationEditor->GetLocation()->GetNavigationMesh());

	LocationEditor->ShowNavigationMesh();
	_editCursor->Show();

	_inputReceiver = new NavigationMeshEditorInputReceiver(_domainName);
}

//===========================================================================//

void NavigationMeshEditor::Leave()
{
	_inputReceiver = NullPtr;

	LocationEditor->HideNavigationMesh();
	_editCursor->Hide();

	_stickingHelper = NullPtr;
}

//===========================================================================//

void NavigationMeshEditor::EnableInput()
{
	_inputReceiver->Enable();
}

//===========================================================================//

void NavigationMeshEditor::DisableInput()
{
	_inputReceiver->Disable();
}

//===========================================================================//

void NavigationMeshEditor::OnFileOpened()
{
	if (LocationEditor->GetCrrentEditorState() == LocationEditorState::NavigationMeshEditor)
	{
		LocationEditor->ShowNavigationMesh();
	}
}

//===========================================================================//

void NavigationMeshEditor::OnFileClosed()
{
	_commandsManager->Clear();
}

//===========================================================================//

SharedPtr<NavigationMeshCell> NavigationMeshEditor::GetNavigationMeshCellByPoint(const Vector3 & point, bool isStrictly) const
{
	SharedPtr<NavigationMeshCell> resultCell = NullPtr;

	const SharedPtr<NavigationMesh> & navigationMesh = LocationEditor->GetLocation()->GetNavigationMesh();
	for (const SharedPtr<NavigationMeshCell> & cell : navigationMesh->GetCells())
	{
		bool isCellInludedPoint;
		if (isStrictly)
		{
			isCellInludedPoint = cell->LeftLowerPoint.X < point.X && point.X < cell->RightUpperPoint.X &&
			                     cell->LeftLowerPoint.Y < point.Y && point.Y < cell->RightUpperPoint.Y;
		}
		else
		{
			isCellInludedPoint = cell->LeftLowerPoint.X <= point.X && point.X <= cell->RightUpperPoint.X &&
			                     cell->LeftLowerPoint.Y <= point.Y && point.Y <= cell->RightUpperPoint.Y;
		}
		if (isCellInludedPoint && (resultCell == NullPtr || cell->NeighborRegions.GetCount() < resultCell->NeighborRegions.GetCount()))
		{
			resultCell = cell;
		}
	}

	return resultCell;
}

//===========================================================================//

void NavigationMeshEditor::SetEditCursorPosition(const Vector3 & position)
{
	_editCursor->SetVertices({position});
}

//===========================================================================//

void NavigationMeshEditor::ShowCellPreviewRectangle()
{
	_cellPreviewRectangle->Show();
}

//===========================================================================//

void NavigationMeshEditor::HideCellPreviewRectangle()
{
	_cellPreviewRectangle->Hide();
}

//===========================================================================//

void NavigationMeshEditor::SetCellPreviewRectangleStartPosition(const Vector3 & position)
{
	_cellPreviewRectangle->SetLeftLowerPoint(position);
}

//===========================================================================//

void NavigationMeshEditor::SetCellPreviewRectangleEndPosition(const Vector3 & position)
{
	_cellPreviewRectangle->SetRightUpperPoint(position);
}

//===========================================================================//

const SharedPtr<CommandsManager> & NavigationMeshEditor::GetCommandsManager() const
{
	return _commandsManager;
}

//===========================================================================//

const SharedPtr<NavigationMeshStickingHelper> & NavigationMeshEditor::GetStickingHelper() const
{
	return _stickingHelper;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef BRU_STRING_TABLE_RECURSIVE_EXCEPTION_H
#define BRU_STRING_TABLE_RECURSIVE_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class StringTableRecursiveEntryException : public Exception
{
public:
	StringTableRecursiveEntryException(const string & details, const string & filename, const string & methodName, integer fileLine);
	virtual ~StringTableRecursiveEntryException();
};

//===========================================================================//

#define STRING_TABLE_RECURSIVE_ENTRY_EXCEPTION(details) \
    throw StringTableRecursiveEntryException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

} //namespace Bru

#endif // BRU_STRING_TABLE_RECURSIVE_EXCEPTION_H
//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - проблемы с логом. Возможно не открыты файлы для
//    записи, возможно лог забыли закрыть перед удалением
//

#pragma once
#ifndef LOG_EXCEPTION_H
#define LOG_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class LogException : public Exception
{
public:
	LogException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~LogException();
};

//===========================================================================//

#define LOG_EXCEPTION(details) \
    throw LogException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // LOG_EXCEPTION_H

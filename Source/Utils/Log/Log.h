//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Лог файл. Выводит в файл сообщения определенного
//    приоритета. Также если это необходимо дублирует сообщения об ошибках
//    и замечаниях в отдельный файл. Применен шаблон Singleron.
//

#pragma once
#ifndef LOG_H
#define LOG_H

#include "../../Common/Common.h"
#include "../../FileSystem/FileSystem.h"
#include "../Enums/MessagePriority.h"
#include "Enums/LogType.h"
#include "LogWriter.h"

namespace Bru
{

//===========================================================================//

class LogSingleton
{
public:
	static const string ShortHeaderTitle;
	static const string HeaderTitle;
	static const string FooterTitle;

	static const string DebugPriorityTitle;
	static const string NoticePriorityTitle;
	static const string ConsoleHintPriorityTitle;
	static const string ConsoleCommandPriorityTitle;
	static const string WarningPriorityTitle;
	static const string ErrorPriorityTitle;
	static const string FatalPriorityTitle;

	LogSingleton(const string & pathToFile, const LogType logType);
	~LogSingleton();

	///Метод выводит сообщение в лог и переходит на новую строку,
	///поэтому нет необходимости в \n в конце предложения
	void WriteMessage(const MessagePriority messagePriority, const string & message);

private:
	SharedPtr<LogWriter> CreateWriter(const string & pathToFile, const LogType logType);

	SharedPtr<LogWriter> _writer;

	LogSingleton(const LogSingleton &) = delete;
	LogSingleton & operator =(const LogSingleton &) = delete;

	//Для тестирования
	friend class TestTextLogOpenClose;
	friend class TestTextLogSimple;
	friend class TestTextLogFailures;
	friend class TestTextLogShortMethods;

	friend class TestHTMLLogOpenClose;
	friend class TestHTMLLogSimple;
	friend class TestHTMLLogFailures;
	friend class TestHTMLLogShortMethods;
};

//===========================================================================//

//Временное название. После появления консоли с ее переменной нужно переимновать в лог
extern SharedPtr<LogSingleton> Log;

//===========================================================================//

}//namespace

#endif // LOG_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "HTMLLogWriter.h"

#include "Log.h"

namespace Bru
{

//===========================================================================//

HTMLLogWriter::HTMLLogWriter(const string & pathToFile) :
	LogWriter(pathToFile)
{
}

//===========================================================================//

HTMLLogWriter::~HTMLLogWriter()
{
}

//===========================================================================//

void HTMLLogWriter::WriteHeader()
{
	_logFile->WriteLine("<html>");
	_logFile->WriteLine("<head>");
	_logFile->WriteLine("<meta http-equiv = \"Content-Type\" content = \"text/html; charset = utf-8\">");
	_logFile->WriteLine(string::Format("<title>{0}</title>", LogSingleton::ShortHeaderTitle));
	_logFile->WriteLine("</head>");
	_logFile->WriteLine("<body bgcolor = \"#101010\" text = \"#FFFFFF\">");
	_logFile->WriteLine("<font size = \"2\" face = \"Courier New\">");
	_logFile->WriteLine("<p style = \"word-wrap: break-word; width: 100%; left: 0\">");
	_logFile->WriteLine(string::Format("{0}<br>\n<br>", LogSingleton::HeaderTitle));

	_logFile->Flush();
}

//===========================================================================//

void HTMLLogWriter::WriteFooter()
{
	_logFile->WriteLine("<br>");
	_logFile->WriteLine(string::Format("{0}<br>", LogSingleton::FooterTitle));
	_logFile->WriteLine("</p>");
	_logFile->WriteLine("</font>");
	_logFile->WriteLine("</body>");
	_logFile->WriteLine("</html>");

	_logFile->Flush();
}

//===========================================================================//

void HTMLLogWriter::WriteLine(const string & text)
{
	string convertedText = ConvertTextSymbolsToHTML(text);
	_logFile->WriteLine(convertedText);
	_logFile->Flush();
}

//===========================================================================//

void HTMLLogWriter::WriteLine(const MessagePriority messagePriority, const string & message)
{
	string convertedMessage = ConvertTextSymbolsToHTML(message);
	string convertedPriorityTitle = ConvertTextSymbolsToHTML(GetMessagePriorityTitle(messagePriority));
	_logFile->WriteLine(
	    string::Format("<font color = {0}>{1}{2}</font><br>", GetMessageColorByPriority(messagePriority),
	                   convertedPriorityTitle, convertedMessage));
	_logFile->Flush();
}

//===========================================================================//

string HTMLLogWriter::GetMessageColorByPriority(const MessagePriority priority)
{
	switch (priority)
	{
	case MessagePriority::Debug:
		return "#B3B3B3";
	case MessagePriority::Notice:
		return "#FFFFFF";
	case MessagePriority::ConsoleHint:
		return "#8E8EBC";
	case MessagePriority::ConsoleCommand:
		return "#8EBC8E";
	case MessagePriority::Warning:
		return "#CC7F32";
	case MessagePriority::Error:
		return "#FF3333";

	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("priority {0} not defined priority", static_cast<integer>(priority)));
	}
}

//===========================================================================//

string HTMLLogWriter::ConvertTextSymbolsToHTML(const string & sourceText)
{
	string resultString = sourceText;

	resultString = resultString.Replace("&", "&#38;");
	resultString = resultString.Replace("<", "&#60;");
	resultString = resultString.Replace(">", "&#62;");
	resultString = resultString.Replace("|", "&#166;");
	resultString = resultString.Replace("\n", "<br>\n");
	resultString = resultString.Replace("\t", "&#09;");
	resultString = resultString.Replace(" ", "&nbsp;");

	return resultString;
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тип формата лога
//

#pragma once
#ifndef LOG_TYPE_H
#define LOG_TYPE_H

namespace Bru
{

//===========================================================================//

enum class LogType
{
    Text,
    HTML
};

//===========================================================================//

}//namespace

#endif // LOG_TYPE_H

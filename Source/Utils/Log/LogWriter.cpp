//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс, с помощью которого осуществляется вывод в лог
//

#include "LogWriter.h"

#include "Log.h"

namespace Bru
{

//===========================================================================//

LogWriter::LogWriter(const string & pathToFile) :
	_logFile(new TextFileStream(pathToFile, FileAccess::Write))
{
}

//===========================================================================//

LogWriter::~LogWriter()
{
	_logFile->Close();
}

//===========================================================================//

string LogWriter::GetMessagePriorityTitle(const MessagePriority messagePriority) const
{
	switch (messagePriority)
	{
	case MessagePriority::Debug:
		return LogSingleton::DebugPriorityTitle;
	case MessagePriority::Notice:
		return LogSingleton::NoticePriorityTitle;
	case MessagePriority::ConsoleHint:
		return LogSingleton::ConsoleHintPriorityTitle;		
	case MessagePriority::ConsoleCommand:
		return LogSingleton::ConsoleCommandPriorityTitle;
	case MessagePriority::Warning:
		return LogSingleton::WarningPriorityTitle;
	case MessagePriority::Error:
		return LogSingleton::ErrorPriorityTitle;

	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined messagePriority");
	}
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс, с помощью которого осуществляется вывод в лог
//

#pragma once
#ifndef LOG_WRITER_H
#define LOG_WRITER_H

#include "../../Common/Types/SharedPtr.h"
#include "../../FileSystem/FileSystem.h"
#include "../Enums/MessagePriority.h"

namespace Bru
{

//===========================================================================//

class LogWriter
{
public:
	virtual ~LogWriter();

	virtual void WriteHeader() = 0;
	virtual void WriteFooter() = 0;

	virtual void WriteLine(const string & text) = 0;
	virtual void WriteLine(const MessagePriority messagePriority, const string & message) = 0;

protected:
	LogWriter(const string & pathToFile);

	string GetMessagePriorityTitle(const MessagePriority messagePriority) const;

	SharedPtr<TextFileStream> _logFile;

private:
	LogWriter(const LogWriter &) = delete;
	LogWriter & operator =(const LogWriter &) = delete;
};

//===========================================================================//

}//namespace

#endif // LOG_WRITER_H

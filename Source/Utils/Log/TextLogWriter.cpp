//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, с помощью которого осуществляется вывод в текстовый лог
//

#include "TextLogWriter.h"

#include "Log.h"

namespace Bru
{

//===========================================================================//

TextLogWriter::TextLogWriter(const string & pathToFile) :
	LogWriter(pathToFile)
{
}

//===========================================================================//

TextLogWriter::~TextLogWriter()
{
}

//===========================================================================//

void TextLogWriter::WriteHeader()
{
	_logFile->WriteLine(string::Format("{0}\n", LogSingleton::HeaderTitle));
	_logFile->Flush();
}

//===========================================================================//

void TextLogWriter::WriteFooter()
{
	_logFile->WriteLine(string::Format("\n{0}", LogSingleton::FooterTitle));
	_logFile->Flush();
}

//===========================================================================//

void TextLogWriter::WriteLine(const string & text)
{
	_logFile->WriteLine(text);
	_logFile->Flush();
}

//===========================================================================//

void TextLogWriter::WriteLine(const MessagePriority messagePriority, const string & message)
{
	_logFile->WriteLine(string::Format("{0}{1}", GetMessagePriorityTitle(messagePriority), message));
	_logFile->Flush();
}

//===========================================================================//

}//namespace

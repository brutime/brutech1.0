//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, с помощью которого осуществляется вывод в HTML лог
//

#pragma once
#ifndef HTML_LOG_WRITER_H
#define HTML_LOG_WRITER_H

#include "LogWriter.h"

namespace Bru
{

//===========================================================================//

class HTMLLogWriter : public LogWriter
{
public:
	HTMLLogWriter(const string & pathToFile);
	virtual ~HTMLLogWriter();

	virtual void WriteHeader();
	virtual void WriteFooter();

	virtual void WriteLine(const string & text);
	virtual void WriteLine(const MessagePriority messagePriority, const string & message);

	static string ConvertTextSymbolsToHTML(const string & sourceText);

private:
	static string GetMessageColorByPriority(const MessagePriority messagePriority);

	HTMLLogWriter(const HTMLLogWriter &) = delete;
	HTMLLogWriter & operator =(const HTMLLogWriter &) = delete;
};

//===========================================================================//

}//namespace

#endif // HTML_LOG_WRITER_H

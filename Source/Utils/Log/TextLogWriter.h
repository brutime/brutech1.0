//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, с помощью которого осуществляется вывод в текстовый лог
//

#pragma once
#ifndef TEXT_LOG_WRITER_H
#define TEXT_LOG_WRITER_H

#include "LogWriter.h"

namespace Bru
{

//===========================================================================//

class TextLogWriter : public LogWriter
{
public:
	TextLogWriter(const string & pathToFile);
	virtual ~TextLogWriter();

	virtual void WriteHeader();
	virtual void WriteFooter();

	virtual void WriteLine(const string & text);
	virtual void WriteLine(const MessagePriority messagePriority, const string & message);

private:
	TextLogWriter(const TextLogWriter &) = delete;
	TextLogWriter & operator =(const TextLogWriter &) = delete;
};

//===========================================================================//

}//namespace

#endif // TEXT_LOG_WRITER_H

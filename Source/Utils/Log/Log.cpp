//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Log.h"

#include "TextLogWriter.h"
#include "HTMLLogWriter.h"

#include "../Exceptions/LogException.h"
#include "../../Common/Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

SharedPtr<LogSingleton> Log;

//===========================================================================//

const string LogSingleton::ShortHeaderTitle = "Bru Tech Log File";
const string LogSingleton::HeaderTitle = "======================== Bru Tech Log File ========================";
const string LogSingleton::FooterTitle = "============================= Log End =============================";

const string LogSingleton::DebugPriorityTitle = "[Debug]:   ";
const string LogSingleton::NoticePriorityTitle = "[Notice]:  ";
const string LogSingleton::ConsoleHintPriorityTitle = "[Hint]:    ";
const string LogSingleton::ConsoleCommandPriorityTitle = "[Command]: ";
const string LogSingleton::WarningPriorityTitle = "[WARNING]: ";
const string LogSingleton::ErrorPriorityTitle = "[ERROR!]:  ";
const string LogSingleton::FatalPriorityTitle = "[FATAL!!]: ";

//===========================================================================//

LogSingleton::LogSingleton(const string & pathToFile, const LogType logType) :
	_writer()
{
	_writer = CreateWriter(pathToFile, logType);
	_writer->WriteHeader();
}

//===========================================================================//

LogSingleton::~LogSingleton()
{
	_writer->WriteFooter();
}

//===========================================================================//

void LogSingleton::WriteMessage(const MessagePriority messagePriority, const string & message)
{
	_writer->WriteLine(messagePriority, message);
}

//===========================================================================//

SharedPtr<LogWriter> LogSingleton::CreateWriter(const string & pathToFile, const LogType logType)
{
	switch (logType)
	{
	case LogType::Text:
		return new TextLogWriter(pathToFile);
	case LogType::HTML:
		return new HTMLLogWriter(pathToFile);

	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined logType");
	}
}

//===========================================================================//

}//namespace

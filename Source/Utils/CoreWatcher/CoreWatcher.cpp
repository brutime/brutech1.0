//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "CoreWatcher.h"

namespace Bru
{

//===========================================================================//

SharedPtr<CoreWatcherSingleton> CoreWatcher;

//===========================================================================//

const string CoreWatcherSingleton::Name = "Core watcher";

//===========================================================================//

CoreWatcherSingleton::CoreWatcherSingleton() : ExpressionWatcher()

{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	ScriptSystem->RegisterCommand(new ClassConstCommand<ExpressionWatcher, string, const string &>(
	                                  "CoreWatch", this, & ExpressionWatcher::Watch));

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

CoreWatcherSingleton::~CoreWatcherSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	ScriptSystem->UnregisterCommand("CoreWatch");

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

} // namespace Bru

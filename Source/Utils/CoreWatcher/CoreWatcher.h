//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Хранит ссылки на именованные переменные ядра
//

#pragma once
#ifndef CORE_WATCHER_H
#define CORE_WATCHER_H

#include "../../Common/Common.h"
#include "../ExpressionWatcher/ExpressionWatcher.h"

namespace Bru
{

//===========================================================================//

class CoreWatcherSingleton : public ExpressionWatcher
{
public:
	CoreWatcherSingleton();
	virtual ~CoreWatcherSingleton();

private:
	static const string Name;

	CoreWatcherSingleton(const CoreWatcherSingleton &) = delete;
	CoreWatcherSingleton & operator =(const CoreWatcherSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<CoreWatcherSingleton> CoreWatcher;

//===========================================================================//

} // namespace Bru

#endif // CORE_WATCHER_H

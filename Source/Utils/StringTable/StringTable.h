//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef STRING_TABLE_H
#define STRING_TABLE_H

#include "../../Common/Common.h"
#include "../ParametersFile/ParametersFile.h"
#include "../Exceptions/StringTableRecursiveEntryException.h"
#include "Enums/EntryStatus.h"

namespace Bru
{

//===========================================================================//

class StringTableSingleton
{
public:
	StringTableSingleton(const string & pathToFile);
	~StringTableSingleton();

	string GetString(const string & key);

private:
	static const string Name;
	static const string SectionsSeparator;
	static const string GlobalPathPrefix;

	SharedPtr<ParametersFile> _file;
	SharedPtr<TreeMap<string, EntryStatus>> _entriesStatus;
	
	void BuildString(const string & key, StringBuilder & result, TreeMap<string, EntryStatus> & marks);
	static string ResolveKey(const string & otherKey, const string & key);
	
	StringTableSingleton(const StringTableSingleton &) = delete;
	StringTableSingleton & operator =(const StringTableSingleton &) = delete;	
};

//===========================================================================//

extern SharedPtr<StringTableSingleton> StringTable;

//===========================================================================//

} // namespace Bru

#endif // STRING_TABLE_H

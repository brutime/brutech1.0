//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "StringTable.h"

#include "../Console/Console.h"
#include "../PathHelper/PathHelper.h"
#include "../../Common/Helpers/FormatTokenParser.h"
#include "../../Common/Helpers/FormatTokenPosition.h"
#include "../Exceptions/StringTableRecursiveEntryException.h"

namespace Bru
{


//===========================================================================//

SharedPtr<StringTableSingleton> StringTable;

//===========================================================================//

const string StringTableSingleton::Name = "String table";
const string StringTableSingleton::SectionsSeparator = ".";
const string StringTableSingleton::GlobalPathPrefix = "::";

//===========================================================================//

StringTableSingleton::StringTableSingleton(const string & pathToFile) :
	_file(),
	_entriesStatus()
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	_file = new ParametersFile(PathHelper::PathToText + pathToFile);
	_entriesStatus = new TreeMap<string, EntryStatus>();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

StringTableSingleton::~StringTableSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	_file->Close();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

string StringTableSingleton::GetString(const string & key)
{
	StringBuilder result;
	TreeMap<string, EntryStatus> marks;

	BuildString(key, result, marks);

	return result.ToString();
}

//===========================================================================//

void StringTableSingleton::BuildString(const string & key, StringBuilder & result, TreeMap<string, EntryStatus> & marks)
{
	if (!marks.Contains(key))
	{
		marks.Add(key, EntryStatus::Checking);
	}
	else if (marks[key] == EntryStatus::Checking)
	{
		STRING_TABLE_RECURSIVE_ENTRY_EXCEPTION("Recursive reference found in string table");
	}

	string value = _file->Get<string>(key);

	FormatTokenParser parser(value);
	integer position = 0;

	for (FormatTokenPosition p = parser.NextToken(); p.GetPosition() >= 0; p = parser.NextToken())
	{
		result.Append(value.Substring(position, p.GetPosition() - position));
		BuildString(ResolveKey(key, parser.GetString(p).GetValue()), result, marks);

		position = p.GetPosition() + p.GetLength();
	}

	result.Append(value.Substring(position, value.GetLength() - position));

	marks[key] = EntryStatus::Safe;
}

//===========================================================================//

string StringTableSingleton::ResolveKey(const string & otherKey, const string & key)
{
	// Проверяем префик. FirstIndex вместо Substring чтобы не созадавать копию субстрингом
	if (key.FirstIndexOf(GlobalPathPrefix) == 0)
	{
		return key.Substring(2);
	}

	string basePath = otherKey.Substring(0, otherKey.LastIndexOf(SectionsSeparator) + 1);

	return basePath + key;
}

//===========================================================================//

} // namespace Bru

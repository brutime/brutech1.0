//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, предоставляющий удобный доступ к настройкам
//

#pragma once
#ifndef CONFIG_H
#define CONFIG_H

#include "../PathHelper/PathHelper.h"
#include "../ParametersFile/ParametersFile.h"

namespace Bru
{

//===========================================================================//

class ConfigSingleton : public ParametersFile
{
public:
	ConfigSingleton();
	~ConfigSingleton();

private:
	ConfigSingleton(const ConfigSingleton &) = delete;
	ConfigSingleton & operator =(const ConfigSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<ConfigSingleton> Config;

//===========================================================================//

} // namespace Bru

#endif // CONFIG_H

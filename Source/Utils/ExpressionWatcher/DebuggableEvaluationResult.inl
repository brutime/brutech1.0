//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
#include "../../ScriptSystem/ScriptSystem.h"

namespace Bru
{

//===========================================================================//

DebuggableEvaluationResult::DebuggableEvaluationResult(const string & expression) :
	_expression(expression)
{
}

//===========================================================================//

DebuggableEvaluationResult::~DebuggableEvaluationResult()
{
}

//===========================================================================//

string DebuggableEvaluationResult::GetValue() const
{
	return (ScriptSystem->Evaluate(_expression)).ResultString;
}

//===========================================================================//

} // namespace Bru

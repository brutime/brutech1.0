//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ExpressionWatcher.h"

#include "DebuggableEvaluationResult.h"

namespace Bru
{

//===========================================================================//

ExpressionWatcher::ExpressionWatcher() :
	_registeredExpressions(),
	_expressionsRegistered(),
	_expressionsUnregistered(),
	ExpressionsRegistered(_expressionsRegistered),
	ExpressionsUnregistered(_expressionsUnregistered)
{
}

//===========================================================================//

ExpressionWatcher::~ExpressionWatcher()
{
}

//===========================================================================//

void ExpressionWatcher::RegisterExpression(const string & variableName, const string & expression)
{
	if (_registeredExpressions.Contains(variableName))
	{
		Console->Warning(string::Format("Expression '{0}' already registered. Skipping registration", variableName));
	}
	else
	{
		_registeredExpressions.Add(variableName, SharedPtr<IDebuggableExpression>(new DebuggableEvaluationResult(expression)));
		_expressionsRegistered.Fire(variableName);
	}
}

//===========================================================================//

void ExpressionWatcher::UnregisterVariableOrExpression(const string & variableName)
{
	if (_registeredExpressions.Contains(variableName))
	{
		_registeredExpressions.Remove(variableName);
		_expressionsUnregistered.Fire(variableName);
	}
	else
	{
		Console->Warning(string::Format("\"{0}\" is not a registered variable or expression", variableName));
	}
}

//===========================================================================//

string ExpressionWatcher::Watch(const string & variableName) const
{
	if (_registeredExpressions.Contains(variableName))
	{
		return _registeredExpressions[variableName]->GetValue();
	}
	else
	{
		return string::Format("\"{0}\" is not a registered variable or expression", variableName);
	}
}

//===========================================================================//

bool ExpressionWatcher::ContainsVariable(const string & variableName) const
{
	return _registeredExpressions.Contains(variableName);
}

//===========================================================================//

List<string> ExpressionWatcher::GetVariablesNames() const
{
	List<string> variablesNames;
	
	for (const auto & expression : _registeredExpressions)
	{
		variablesNames.Add(expression.GetKey());
	}	
	
	return variablesNames;
}

//===========================================================================//

AddingOrderedTreeMap<string, string> ExpressionWatcher::GetVariables() const
{
	AddingOrderedTreeMap<string, string> variablesList;

	for (const auto & expression : _registeredExpressions)
	{
		variablesList.Add(expression.GetKey(), Watch(expression.GetKey()));
	}

	return variablesList;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

namespace Bru
{

//===========================================================================//

template<typename T>
void ExpressionWatcher::RegisterVariable(const string & variableName, const T & variableAddress)
{
	if (_registeredExpressions.Contains(variableName))
	{
		Console->Warning(string::Format("Variable \"{0}\" already registered. Skipping registration", variableName));
	}
	else
	{
		_registeredExpressions.Add(variableName, SharedPtr<IDebuggableExpression>(new DebuggableVariable<T>(variableAddress)));
		_expressionsRegistered.Fire(variableName);
	}
}

//===========================================================================//

} // namespace Bru

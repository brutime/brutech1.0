//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: интерфейс обертки для выражения, доступного для просмотра в отладчике
//

#pragma once
#ifndef I_DEBUGGABLE_EXPRESSION_H
#define I_DEBUGGABLE_EXPRESSION_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class IDebuggableExpression
{
public:
	IDebuggableExpression();
	virtual ~IDebuggableExpression();

	virtual string GetValue() const = 0;

private:
	IDebuggableExpression(const IDebuggableExpression &) = delete;
	IDebuggableExpression & operator =(const IDebuggableExpression &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // I_DEBUGGABLE_EXPRESSION_H

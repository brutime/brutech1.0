//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Хранит ссылки на именованные переменные и выражения для отладки
//

#pragma once
#ifndef EXPRESSION_WATCHER_H
#define EXPRESSION_WATCHER_H

#include "../../Common/Common.h"
#include "../../ScriptSystem/ScriptSystem.h"
#include "../Console/Console.h"
#include "IDebuggableExpression.h"
#include "DebuggableVariable.h"

namespace Bru
{

//===========================================================================//

class ExpressionWatcher
{
public:
	ExpressionWatcher();
	virtual ~ExpressionWatcher();

	template<typename T>
	void RegisterVariable(const string & variableName, const T & variableAddress);
	void RegisterExpression(const string & variableName, const string & expression);

	void UnregisterVariableOrExpression(const string & variableName);

	string Watch(const string & variableName) const;

	bool ContainsVariable(const string & variableName) const;

	List<string> GetVariablesNames() const;
	AddingOrderedTreeMap<string, string> GetVariables() const;

private:
	AddingOrderedTreeMap<string, SharedPtr<IDebuggableExpression>> _registeredExpressions;

private:
	InnerEvent<const string &> _expressionsRegistered;
	InnerEvent<const string &> _expressionsUnregistered;

public:
	Event<const string &> ExpressionsRegistered;
	Event<const string &> ExpressionsUnregistered;

private:
	ExpressionWatcher(const ExpressionWatcher &) = delete;
	ExpressionWatcher & operator =(const ExpressionWatcher &) = delete;
};

//===========================================================================//

} // namespace Bru

#include "ExpressionWatcher.inl"

#endif // EXPRESSION_WATCHER_H

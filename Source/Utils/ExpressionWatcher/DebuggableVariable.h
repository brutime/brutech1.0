//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: обертка для переменной, доступной для просмотра в отладчике
//

#pragma once
#ifndef DEBUGGABLE_VARIABLE_H
#define DEBUGGABLE_VARIABLE_H

#include "IDebuggableExpression.h"

namespace Bru
{

//===========================================================================//

template<typename T>
class DebuggableVariable : public IDebuggableExpression
{
public:
	DebuggableVariable(const T & variableAddress);
	~DebuggableVariable();

	string GetValue() const;

private:
	const T & _variableAddress;
	
	DebuggableVariable(const DebuggableVariable &) = delete;
	DebuggableVariable & operator =(const DebuggableVariable &) = delete;	
};

//===========================================================================//

} // namespace Bru

#include "DebuggableVariable.inl"

#endif // DEBUGGABLE_VARIABLE_H

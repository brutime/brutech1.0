//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
#include "../../Common/Types/Converter.h"

namespace Bru
{

//===========================================================================//

template<typename T>
DebuggableVariable<T>::DebuggableVariable(const T & variableAddress) :
	_variableAddress(variableAddress)
{
}

//===========================================================================//

template<typename T>
DebuggableVariable<T>::~DebuggableVariable()
{
}

//===========================================================================//

template<typename T>
string DebuggableVariable<T>::GetValue() const
{
	return Converter::ToString(_variableAddress);
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: обертка для результата выполнения скриптовой команды, доступной для просмотра в отладчике
//

#pragma once
#ifndef DEBUGGABLE_EVALUATION_RESULT_H
#define DEBUGGABLE_EVALUATION_RESULT_H

#include "IDebuggableExpression.h"

namespace Bru
{

//===========================================================================//

class DebuggableEvaluationResult : public IDebuggableExpression
{
public:
	DebuggableEvaluationResult(const string & expression);
	~DebuggableEvaluationResult();

	string GetValue() const;

private:
	string _expression;

	DebuggableEvaluationResult(const DebuggableEvaluationResult &) = delete;
	DebuggableEvaluationResult & operator =(const DebuggableEvaluationResult &) = delete;
};

//===========================================================================//

} // namespace Bru

#include "DebuggableEvaluationResult.inl"

#endif // DEBUGGABLE_EVALUATION_RESULT_H

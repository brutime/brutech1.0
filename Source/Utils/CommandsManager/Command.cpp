//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Command.h"

#include "CommandsManager.h"

namespace Bru
{

//===========================================================================//

Command::Command(bool isCancelable):
	_isCancelable(isCancelable)
{
}

//===========================================================================//

Command::~Command()
{
}

//===========================================================================//

void Command::Cancel()
{	
	if (!_isCancelable)
	{
		INVALID_STATE_EXCEPTION("Can't cancel not cancelable command");
	}
}

//===========================================================================//

bool Command::IsCancelable() const
{
	return _isCancelable;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef COMMAND_H
#define COMMAND_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class CommandsManager;

//===========================================================================//

class Command
{
	friend class CommandsManager;

public:
	Command(bool isHistorable);
	virtual ~Command();

protected:
	virtual void Execute() = 0;
	virtual void Cancel();

	bool IsCancelable() const;

private:
	bool _isCancelable; //Попадает в список undo\rero

	Command(const Command &) = delete;
	Command & operator =(const Command &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // COMMAND_H

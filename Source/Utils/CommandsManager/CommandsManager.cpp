//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "CommandsManager.h"

namespace Bru
{

//===========================================================================//

CommandsManager::CommandsManager(integer historyCommandsCount) :
	_commandsHistory(historyCommandsCount),
	_historyCursor(-1),
	_lastExecutedIndex(-1)
{
}

//===========================================================================//

CommandsManager::~CommandsManager()
{
}

//===========================================================================//

void CommandsManager::Execute(const SharedPtr<Command> & command)
{
	command->Execute();
	if (!command->IsCancelable())
	{
		return;
	}
		
	if (_historyCursor == _commandsHistory.GetCount() - 1)
	{
		for (integer i = 0; i < _commandsHistory.GetCount() - 1; i++)
		{
			_commandsHistory[i] = _commandsHistory[i + 1];
		}
	}
	else
	{
		_historyCursor++;
	}
	
	_commandsHistory[_historyCursor] = command;
	_lastExecutedIndex = _historyCursor;
}

//===========================================================================//

void CommandsManager::Undo()
{
	if (_historyCursor == -1)
	{
		return;
	}
	
	_commandsHistory[_historyCursor]->Cancel();
	_historyCursor--;
}

//===========================================================================//

void CommandsManager::Redo()
{
	if (_historyCursor >= _lastExecutedIndex)
	{
		return;
	}
	
	_historyCursor++;
	_commandsHistory[_historyCursor]->Execute();
}

//===========================================================================//

void CommandsManager::Clear()
{
	integer historyCommandsCount = _commandsHistory.GetCount();
	_commandsHistory.Clear();
	_commandsHistory.Resize(historyCommandsCount);
	_historyCursor = -1;
	_lastExecutedIndex = -1;
}

//===========================================================================//

} // namespace Bru

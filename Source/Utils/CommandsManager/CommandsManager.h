//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef COMMANDS_MANAGER_H
#define COMMANDS_MANAGER_H

#include "../../Common/Common.h"
#include "Command.h"

namespace Bru
{

//===========================================================================//

class CommandsManager
{
public:
	CommandsManager(integer historyCommandsCount);
	~CommandsManager();

	void Execute(const SharedPtr<Command> & command);
	void Undo();
	void Redo();
	
	void Clear();

private:
	Array<SharedPtr<Command>> _commandsHistory;
	integer _historyCursor;
	integer _lastExecutedIndex;

	CommandsManager(const CommandsManager &) = delete;
	CommandsManager & operator =(const CommandsManager &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // COMMANDS_MANAGER_H

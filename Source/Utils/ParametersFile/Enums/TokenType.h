//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: тип токена
//

#pragma once
#ifndef PARAMETERS_FILE_PARSING_TOKEN_TYPE_H
#define PARAMETERS_FILE_PARSING_TOKEN_TYPE_H

namespace Bru
{

namespace ParametersFileParsing
{

//===========================================================================//

enum class TokenType
{
    StreamBegin = 0,
    SectionNamePrefix,
    SectionName,
    SectionBegin,
    SectionEnd,
    Key,
    Assign,
    Value,
    Count
};

//===========================================================================//

}//namespace ParametersFileParsing

} //namespace Bru

#endif // PARAMETERS_FILE_PARSING_TOKEN_TYPE_H

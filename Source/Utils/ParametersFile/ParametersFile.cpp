//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ParametersFile.h"

#include "../Console/Console.h"
#include "../PathHelper/PathHelper.h"
namespace Bru

{

//===========================================================================//

const string ParametersFile::RootSection = "RootSection";

const utf8_char ParametersFile::SectionNamePrefixSymbol = "#";
const utf8_char ParametersFile::SectionBeginSymbol = "{";
const utf8_char ParametersFile::SectionEndSymbol = "}";
const utf8_char ParametersFile::AssignSymbol = "=";
const utf8_char ParametersFile::QuoteSymbol = "\"";

//===========================================================================//

ParametersFile::ParametersFile(const string & pathToFile, FileAccess access) :
	BaseDataProvider(),
	_rootSection(new ParametersSection(RootSection, NullPtr)),
	_pathToFile(PathHelper::PathToData + pathToFile + "." + PathHelper::ParametersFileExtension),
	_access(access),
	_isOpened(false)
{
	if (access == FileAccess::Read || access == FileAccess::Append || _access == FileAccess::ReadWrite)
	{
		if (!FileSystem::FileExists(_pathToFile))
		{
			FILE_NOT_FOUND_EXCEPTION(string::Format("File not found: \"{0}\"", _pathToFile));
		}
	}

	if (access != FileAccess::Write)
	{
		ParseFile();
	}

	_isOpened = true;
	Console->Debug(string::Format("Parameters file opened: \"{0}\"", _pathToFile));
}

//===========================================================================//

ParametersFile::~ParametersFile()
{
	if (_isOpened)
	{
		Close();
	}
}

//===========================================================================//

integer ParametersFile::GetListChildren(const string & pathToSection) const
{
	CheckForOpen();

	SharedPtr<ParametersSection> section = GetSection(pathToSection, false);
	return section->GetAnonymousChildrenCount();
}

//===========================================================================//

bool ParametersFile::ContainsSection(const string & pathToSection) const
{
	CheckForOpen();
	CheckForEmptyPath(pathToSection);

	return _rootSection->ContainsSection(pathToSection);
}

//===========================================================================//

bool ParametersFile::ContainsParameter(const string & path) const
{
	CheckForOpen();
	CheckForEmptyPath(path);

	return _rootSection->ContainsParameter(path);
}

//===========================================================================//

void ParametersFile::Save()
{
	SaveAs(_pathToFile);
}

//===========================================================================//

void ParametersFile::SaveAs(const string & pathToFile)
{
	SharedPtr<TextFileStream> file = new TextFileStream(pathToFile, FileAccess::Write);
	_rootSection->WriteParameters(file);
	file->Close();

	if (_pathToFile == pathToFile)
	{
		Console->Debug(string::Format("Parameters file \"{0}\" saved", _pathToFile));
	}
	else
	{
		Console->Debug(string::Format("Parameters file \"{0}\" saved as \"{1}\"", _pathToFile, pathToFile));
	}
}

//===========================================================================//

void ParametersFile::Print()
{
	_rootSection->WriteParameters(OSConsoleStream);
}

//===========================================================================//

void ParametersFile::Close()
{
	CheckForOpen();

	_rootSection->Clear();
	_isOpened = false;

	Console->Debug(string::Format("Parameters file closed: \"{0}\"", _pathToFile));
}

//===========================================================================//

bool ParametersFile::IsOpened() const
{
	return _isOpened;
}

//===========================================================================//

WeakPtr<ParametersSection> ParametersFile::GetSection(const string & pathToSection, bool returnNullIfNotExist) const
{
	try
	{
		return _rootSection->GetSection(pathToSection, returnNullIfNotExist);
	}
	catch (InvalidArgumentException & exception)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("File: \"{0}\", details: \"{1}\"",
		                                          _pathToFile, exception.GetDetails()));
	}
}

//===========================================================================//

WeakPtr<ParametersSection> ParametersFile::GetSection(const string & pathToSection, bool createIfNotExist, bool returnNullIfNotExist)
{
	try
	{
		return _rootSection->GetSection(pathToSection, createIfNotExist, returnNullIfNotExist);
	}
	catch (InvalidArgumentException & exception)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("File: \"{0}\", details: \"{1}\"",
		                                          _pathToFile, exception.GetDetails()));
	}
}

//===========================================================================//

void ParametersFile::ParseFile()
{
	SharedPtr<TextFileStream> file = new TextFileStream(_pathToFile, FileAccess::Read);

	SharedPtr<ParametersSection> currentSection = _rootSection;
	string currentSectionName;
	string currentKey;

	integer openedBracketsCount = 0;
	integer closedBracketsCount = 0;
	integer lineNumber = 1;
	ParametersFileParsing::TokenType previousToken = ParametersFileParsing::TokenType::StreamBegin;
	while (!file->IsEOF())
	{
		string line = file->ReadLine();
		line = RemoveComments(line);
		line = line.Trim();

		if (line.IsEmptyOrBlankCharsOnly())
		{
			lineNumber++;
			continue;
		}

		integer cursor = 0;
		while (cursor < line.GetLength())
		{
			ParametersFileParsing::TokenType token = GetToken(previousToken, line[cursor], lineNumber);

			if (token == ParametersFileParsing::TokenType::SectionName)
			{
				currentSectionName = ReadString(line, cursor, lineNumber);
			}
			else if (token == ParametersFileParsing::TokenType::SectionBegin)
			{
				if (previousToken == ParametersFileParsing::TokenType::SectionName)
				{
					currentSection->AddChild(currentSectionName, new ParametersSection(currentSectionName, currentSection));
					currentSection = currentSection->GetChild(currentSectionName);
					currentSectionName = string::Empty;
				}
				else
				{
					string sectionName = Converter::ToString(currentSection->GetAnonymousChildrenCount());
					currentSection->AddChild(sectionName, new ParametersSection(sectionName, currentSection));
					currentSection = currentSection->GetChild(sectionName);
				}

				openedBracketsCount++;
			}
			else if (token == ParametersFileParsing::TokenType::SectionEnd)
			{
				currentSection = currentSection->GetParent();

				closedBracketsCount++;
				if (closedBracketsCount > openedBracketsCount)
				{
					PARSING_EXCEPTION(string::Format("Incorrect order of brackets. File: \"{0}\", line: {1}", _pathToFile, lineNumber));
				}
			}
			else if (token == ParametersFileParsing::TokenType::Key)
			{
				currentKey = ReadString(line, cursor, lineNumber);
			}
			else if (token == ParametersFileParsing::TokenType::Value)
			{
				string currentValue = ReadString(line, cursor, lineNumber, true);
				currentSection->AddParameter(currentKey, currentValue);
			}

			previousToken = token;
			cursor++;
		}

		lineNumber++;
	}

	if (openedBracketsCount != closedBracketsCount)
	{
		PARSING_EXCEPTION(string::Format("Incorrect order of brackets. File \"{0}\"", _pathToFile));
	}

	file->Close();
}

//===========================================================================//

ParametersFileParsing::TokenType ParametersFile::GetToken(ParametersFileParsing::TokenType previousToken,
                                                          const utf8_char & tokenSymbol, integer lineNumber) const
{
	ParametersFileParsing::TokenType token;
	if (tokenSymbol == SectionNamePrefixSymbol)
	{
		token = ParametersFileParsing::TokenType::SectionNamePrefix;
	}
	else if (tokenSymbol == SectionBeginSymbol)
	{
		token = ParametersFileParsing::TokenType::SectionBegin;
	}
	else if (tokenSymbol == SectionEndSymbol)
	{
		token = ParametersFileParsing::TokenType::SectionEnd;
	}
	else if (tokenSymbol == AssignSymbol)
	{
		token = ParametersFileParsing::TokenType::Assign;
	}
	else if (previousToken == ParametersFileParsing::TokenType::SectionNamePrefix)
	{
		token = ParametersFileParsing::TokenType::SectionName;
	}
	else if (previousToken == ParametersFileParsing::TokenType::StreamBegin ||
	         previousToken == ParametersFileParsing::TokenType::Value ||
	         previousToken == ParametersFileParsing::TokenType::SectionBegin ||
	         previousToken == ParametersFileParsing::TokenType::SectionEnd)
	{
		token = ParametersFileParsing::TokenType::Key;
	}
	else if (previousToken == ParametersFileParsing::TokenType::Assign)
	{
		token = ParametersFileParsing::TokenType::Value;
	}
	else
	{
		PARSING_EXCEPTION(string::Format("Unknown token symbol \"{0}\". File: \"{1}\", line: {2}", tokenSymbol, _pathToFile, lineNumber));
	}

	CheckTokensOrder(previousToken, token, tokenSymbol, lineNumber);

	return token;
}

//===========================================================================//

void ParametersFile::CheckTokensOrder(ParametersFileParsing::TokenType previousToken,
                                      ParametersFileParsing::TokenType token, const utf8_char & tokenSymbol, integer lineNumber) const
{
	const integer tokenTokensCount = static_cast<integer>(ParametersFileParsing::TokenType::Count);
	static byte expectedTokens[tokenTokensCount][tokenTokensCount] =
	{
		//                         0  1  2  3  4  5  6  7
		/* 0 StreamBegin       */ { 0, 1, 0, 0, 0, 1, 0, 0 },
		/* 1 SectionNamePrefix */ { 0, 0, 1, 0, 0, 0, 0, 0 },
		/* 2 SectionName,      */ { 0, 0, 0, 1, 0, 0, 0, 0 },
		/* 3 SectionBegin,     */ { 0, 1, 0, 1, 1, 1, 0, 0 },
		/* 4 SectionEnd,       */ { 0, 1, 0, 1, 1, 1, 0, 0 },
		/* 5 Key,              */ { 0, 0, 0, 0, 0, 0, 1, 0 },
		/* 6 Assign,           */ { 0, 0, 0, 0, 0, 0, 0, 1 },
		/* 7 Value,            */ { 0, 1, 0, 1, 1, 1, 0, 0 }
	};

	if (!expectedTokens[static_cast<integer>(previousToken)][static_cast<integer>(token)])
	{
		PARSING_EXCEPTION(string::Format("Unexpected token symbol \"{0}\". File: \"{1}\", line: {2}", tokenSymbol, _pathToFile, lineNumber));
	}
}

//===========================================================================//

string ParametersFile::ReadString(const string & line, integer & cursor, integer lineNumber, bool acceptBlankStrings) const
{
	string resultString;

	integer notBlankCharIndex = line.FirstIndexOfNotBlankChar(cursor);
	if (notBlankCharIndex != -1 && line[notBlankCharIndex] == QuoteSymbol)
	{
		integer lastQuoteIndex = line.LastIndexOf(QuoteSymbol);
		if (notBlankCharIndex == lastQuoteIndex)
		{
			resultString = QuoteSymbol;
		}
		else
		{
			resultString = line.Substring(notBlankCharIndex + 1, lastQuoteIndex - notBlankCharIndex - 1);
		}

		if (lastQuoteIndex == line.GetLength() - 1)
		{
			cursor = line.GetLength() - 1;
		}
		else
		{
			notBlankCharIndex = line.FirstIndexOfNotBlankChar(lastQuoteIndex + 1);
			if (notBlankCharIndex != -1)
			{
				cursor = notBlankCharIndex - 1;
			}
			else
			{
				cursor = line.GetLength() - 1;
			}
		}

	}
	else
	{
		StringBuilder builder(50);
		while (cursor < line.GetLength())
		{
			utf8_char tokenSymbol = line[cursor];
			if (IsReservedSymbol(tokenSymbol))
			{
				cursor--;
				break;
			}

			builder.Append(tokenSymbol);
			cursor++;
		}

		resultString = builder.ToString().Trim();
	}

	if (!acceptBlankStrings && resultString.IsEmptyOrBlankCharsOnly())
	{
		PARSING_EXCEPTION(string::Format("Empty string. File: \"{0}\", line {1}", _pathToFile, lineNumber));
	}

	return resultString;
}

//===========================================================================//

string ParametersFile::RemoveComments(const string & sourceString) const
{
	integer commentPosition = sourceString.FirstIndexOf("//");
	if (commentPosition == -1)
	{
		return sourceString;
	}

	return sourceString.Remove(commentPosition, sourceString.GetLength() - commentPosition);
}

//===========================================================================//

bool ParametersFile::IsReservedSymbol(const utf8_char & tokenSymbol) const
{
	if (tokenSymbol == SectionNamePrefixSymbol || tokenSymbol == SectionBeginSymbol ||
	    tokenSymbol == SectionEndSymbol || tokenSymbol == AssignSymbol)
	{
		return true;
	}

	return false;
}

//===========================================================================//

void ParametersFile::CheckForOpen() const
{
	if (!IsOpened())
	{
		INVALID_STATE_EXCEPTION(string::Format("File not opened: \"{0}\"", _pathToFile));
	}
}

//===========================================================================//

void ParametersFile::CheckForEmptyPath(const string & path) const
{
	if (path.IsEmptyOrBlankCharsOnly())
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("path \"{0}\" is empty or blank chars only", path));
	}
}

//===========================================================================//

void ParametersFile::CheckForRead() const
{
	CheckForOpen();

	if (_access == FileAccess::Write)
	{
		INVALID_STATE_EXCEPTION(string::Format("File opened for read: \"{0}\"", _pathToFile));
	}

	if (_access == FileAccess::Append)
	{
		INVALID_STATE_EXCEPTION(string::Format("File opened for append: \"{0}\"", _pathToFile));
	}
}

//===========================================================================//

void ParametersFile::CheckForWrite() const
{
	CheckForOpen();

	if (_access == FileAccess::Read)
	{
		INVALID_STATE_EXCEPTION(string::Format("File opened for read: \"{0}\"", _pathToFile));
	}
}

//===========================================================================//

const string & ParametersFile::GetPathToFile() const
{
	return _pathToFile;
}

//===========================================================================//

string ParametersFile::GetParameter(const string & path) const
{
	CheckForRead();
	CheckForEmptyPath(path);

	try
	{
		return _rootSection->GetParameter(path);
	}
	catch (InvalidArgumentException & e)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("File: {0}  path: {1}", _pathToFile, path));
	}
}

//===========================================================================//

void ParametersFile::SetParameter(const string & path, const string & value)
{
	CheckForWrite();
	CheckForEmptyPath(path);

	try
	{
		_rootSection->SetParameter(path, value);
	}
	catch (InvalidArgumentException & e)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("File: {0} path: {1}", _pathToFile, path));
	}
}

//===========================================================================//

List<const WeakPtr<BaseDataProvider>> ParametersFile::GetAnonymousChildren(const string &root) const
{
	return _rootSection->GetAnonymousChildren(root);
}

//===========================================================================//

integer ParametersFile::GetAnonymousChildrenCount() const
{
	return _rootSection->GetAnonymousChildrenCount();
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: секция с набором параметров
//

#pragma once
#ifndef PARAMETERS_FILE_SECTION_H
#define PARAMETERS_FILE_SECTION_H

#include "../../Common/Common.h"
#include "../Base/BaseDataProvider.h"

namespace Bru
{

//===========================================================================//

class ParametersSection : public BaseDataProvider, public PtrFromThisMixIn<ParametersSection>
{
public:
	static const string UndefinedSectionName;
	static const string PathDelimiter;
	static const integer TabIndentsCount;

	ParametersSection();
	ParametersSection(const ParametersSection && other);
	ParametersSection(const string & name, const WeakPtr<ParametersSection> & parent);
	virtual ~ParametersSection();

	ParametersSection & operator =(const ParametersSection && other);

	virtual void SetParameter(const string & path, const string & value);
	virtual string GetParameter(const string & path) const;

	virtual void AddParameter(const string & key, const string & value);
	virtual bool ContainsParameter(const string & key) const;

	virtual WeakPtr<ParametersSection> AddChild(const string & name, const SharedPtr<ParametersSection> & section);
	virtual bool ContainsChild(const string & name) const;

	virtual WeakPtr<ParametersSection> GetSection(const string & pathToSection, bool returnNullIfNotExist) const;
	///метод не-const, т.к. во время вызова возможно создание новой секции
	virtual WeakPtr<ParametersSection> GetSection(const string & pathToSection, bool createIfNotExist, bool returnNullIfNotExist);
	virtual bool ContainsSection(const string & pathToSection) const;
	
	virtual SharedPtr<ParametersSection> GetChild(const string & name) const;
	virtual List<const WeakPtr<BaseDataProvider>> GetAnonymousChildren(const string &root) const;
	virtual integer GetAnonymousChildrenCount() const;

	virtual void WriteParameters(const SharedPtr<IOutputStream> & stream, integer offset = 0);
	virtual void Clear();

	const string GetName();
	WeakPtr<ParametersSection> GetParent() const;

private:
	static void SplitPath(const string & path, string & headPart, string & tailPart);	
	
	string _name;
	AddingOrderedTreeMap<string, string> _parameters;
	WeakPtr<ParametersSection> _parent;
	AddingOrderedTreeMap<string, SharedPtr<ParametersSection>> _children;
	AddingOrderedTreeMap<string, SharedPtr<ParametersSection>> _listChildren;

private:
	ParametersSection(const ParametersSection & other) = delete;
	ParametersSection & operator =(const ParametersSection & other) = delete;
};

//===========================================================================//

} //namespace Bru

#endif // PARAMETERS_FILE_SECTION_H

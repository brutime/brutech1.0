//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: файл с набором параметров
//

#pragma once
#ifndef PARAMETERS_FILE_H
#define PARAMETERS_FILE_H

#include "../../Common/Common.h"
#include "../../FileSystem/FileSystem.h"
#include "Enums/TokenType.h"
#include "ParametersSection.h"

namespace Bru
{

//===========================================================================//

class ParametersFile : public BaseDataProvider
{
public:
	static const string RootSection;

	static const utf8_char SectionNamePrefixSymbol;
	static const utf8_char SectionBeginSymbol;
	static const utf8_char SectionEndSymbol;
	static const utf8_char AssignSymbol;
	static const utf8_char QuoteSymbol;

	ParametersFile(const string & pathToFile, FileAccess access = FileAccess::Read);
	virtual ~ParametersFile();

	integer GetListChildren(const string & pathToSection) const;

	bool ContainsSection(const string & pathToSection) const;
	bool ContainsParameter(const string & path) const;

	void Save();
	void SaveAs(const string & pathToFile);
	void Print();

	void Close();

	bool IsOpened() const;

	const string & GetPathToFile() const;

	WeakPtr<ParametersSection> GetSection(const string & pathToSection, bool returnNullIfNotExist = true) const;
	WeakPtr<ParametersSection> GetSection(const string & pathToSection, bool createIfNotExist = false, bool returnNullIfNotExist = true);

	virtual string GetParameter(const string & path) const;
	virtual void SetParameter(const string & path, const string & value);

	virtual List<const WeakPtr<BaseDataProvider>> GetAnonymousChildren(const string &root) const;
	virtual integer GetAnonymousChildrenCount() const;

private:
	void ParseFile();

	ParametersFileParsing::TokenType GetToken(ParametersFileParsing::TokenType previousToken,
	                                          const utf8_char & tokenSymbol, integer lineNumber) const;
	void CheckTokensOrder(ParametersFileParsing::TokenType previousToken, ParametersFileParsing::TokenType token,
	                      const utf8_char & tokenSymbol, integer lineNumber) const;

	//Внимание! В этом методе изменятеся cursor
	string ReadString(const string & line, integer & cursor, integer lineNumber, bool acceptBlankStrings = false) const;
	string RemoveComments(const string & sourceString) const;

	bool IsReservedSymbol(const utf8_char & tokenSymbol) const;

	void CheckForOpen() const;
	void CheckForEmptyPath(const string & path) const;
	void CheckForRead() const;
	void CheckForWrite() const;

	SharedPtr<ParametersSection> _rootSection;
	string _pathToFile; //Это поле необходимо, чтобы выводить путь до файла в исключениях
	FileAccess _access;
	bool _isOpened;

	ParametersFile(const ParametersFile &) = delete;
	ParametersFile & operator =(const ParametersFile &) = delete;

	friend class TestParametersFileConstructor;
};

//===========================================================================//

} // namespace Bru

#endif // PARAMETERS_FILE_H

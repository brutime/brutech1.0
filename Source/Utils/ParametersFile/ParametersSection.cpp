//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ParametersSection.h"

namespace Bru
{

//===========================================================================//

const string ParametersSection::UndefinedSectionName = "Undefined";
const string ParametersSection::PathDelimiter = ".";
const integer ParametersSection::TabIndentsCount = 1;

//===========================================================================//

ParametersSection::ParametersSection() :
	BaseDataProvider(),
	PtrFromThisMixIn<ParametersSection>(),	
	_name(UndefinedSectionName),
	_parameters(),
	_parent(),
	_children(),
	_listChildren()
{
}

//===========================================================================//

ParametersSection::ParametersSection(const ParametersSection && other) :
	BaseDataProvider(),
	PtrFromThisMixIn<ParametersSection>(),	
	_name(other._name),
	_parameters(other._parameters),
	_parent(other._parent),
	_children(other._children),
	_listChildren(other._listChildren)
{
}

//===========================================================================//

ParametersSection::ParametersSection(const string & name, const WeakPtr<ParametersSection> & parent) :
	_name(name),
	_parameters(),
	_parent(parent),
	_children(),
	_listChildren()
{
	if (name.IsEmptyOrBlankCharsOnly())
	{
		INVALID_ARGUMENT_EXCEPTION("name is empty or blanks chars only");
	}
}

//===========================================================================//

ParametersSection::~ParametersSection()
{
}

//===========================================================================//

ParametersSection & ParametersSection::operator =(const ParametersSection && other)
{
	if (this == &other)
	{
		return *this;
	}

	_name = other._name;
	_parameters = other._parameters;
	_parent = other._parent;
	_children = other._children;
	_listChildren = other._listChildren;

	return *this;
}

//===========================================================================//

void ParametersSection::AddParameter(const string & key, const string & value)
{
	_parameters.Add(key, value);
}

//===========================================================================//

WeakPtr<ParametersSection> ParametersSection::GetSection(const string & pathToSection, bool returnNullIfNotExist) const
{
	string head, tail;
	SplitPath(pathToSection, head, tail);

	if (head.IsEmpty())
	{
		return GetWeakPtr();
	}
	else
	{
		if (!ContainsChild(head))
		{
			if (returnNullIfNotExist)
			{
				return NullPtr;
			}
			else
			{
				INVALID_ARGUMENT_EXCEPTION(string::Format("Section \"{0}\" not found.", pathToSection));
			}
		}
		return GetChild(head)->GetSection(tail, returnNullIfNotExist);
	}
}

//===========================================================================//

WeakPtr<ParametersSection> ParametersSection::GetSection(const string & pathToSection, bool createIfNotExist, bool returnNullIfNotExist)
{
#ifdef DEBUGGING
	if (createIfNotExist && returnNullIfNotExist)
	{
		INVALID_ARGUMENT_EXCEPTION("Mutually exclusive parameters provided for case when path doesn't exist");
	}
#endif // DEBUGGING

	string head, tail;
	SplitPath(pathToSection, head, tail);

	SharedPtr<ParametersSection> result;
	if (head.IsEmpty())
	{
		return GetWeakPtr();
	}
	else
	{
		if (!ContainsChild(head))
		{
			if (returnNullIfNotExist)
			{
				return NullPtr;
			}
			else if (createIfNotExist)
			{
				return AddChild(head, new ParametersSection(head, GetWeakPtr()))
				       ->GetSection(tail, createIfNotExist, returnNullIfNotExist);
			}
			else
			{
				INVALID_ARGUMENT_EXCEPTION(string::Format("Section \"{0}\" not found.", pathToSection));
			}
		}
		return GetChild(head)->GetSection(tail, createIfNotExist, returnNullIfNotExist);
	}
}

//===========================================================================//

bool ParametersSection::ContainsSection(const string & pathToSection) const
{
	return GetSection(pathToSection, true) != NullPtr;
}

//===========================================================================//

void ParametersSection::SetParameter(const string & path, const string & value)
{
	string head, tail;
	SplitPath(path, head, tail);

	if (tail.IsEmpty())
	{
		if (_parameters.Contains(head))
		{
			_parameters[head] = value;
		}
		else
		{
			_parameters.Add(head, value);
		}
	}
	else
	{
		GetSection(head, true, false)->SetParameter(tail, value);
	}
}

//===========================================================================//

string ParametersSection::GetParameter(const string & path) const
{
	string head, tail;
	SplitPath(path, head, tail);

	if (tail.IsEmpty())
	{
		if (!_parameters.Contains(head))
		{
			INVALID_ARGUMENT_EXCEPTION(string::Format("Parameter \"{0}\" not found.", path));
		}
		return _parameters[head];
	}
	return GetChild(head)->GetParameter(tail);
}

//===========================================================================//

bool ParametersSection::ContainsParameter(const string & key) const
{
	string head, tail;
	SplitPath(key, head, tail);
	if (tail.IsEmpty())
	{
		return _parameters.Contains(head);
	}
	else
	{
		return GetChild(head)->ContainsParameter(tail);
	}
}

//===========================================================================//

WeakPtr<ParametersSection> ParametersSection::AddChild(const string & name, const SharedPtr<ParametersSection> & section)
{
	if (name.IsEmptyOrBlankCharsOnly())
	{
		INVALID_ARGUMENT_EXCEPTION("name is empty or blanks chars only");
	}

	if (name.IsNumber())
	{
		_listChildren.Add(name, section);
		return _listChildren[name];
	}

	_children.Add(name, section);
	return _children[name];
}

//===========================================================================//

bool ParametersSection::ContainsChild(const string & name) const
{
	if (name.IsNumber())
	{
		return _listChildren.Contains(name);
	}

	return _children.Contains(name);
}

//===========================================================================//

SharedPtr<ParametersSection> ParametersSection::GetChild(const string & name) const
{
	if (name.IsNumber())
	{
		return _listChildren[name];
	}

	return _children[name];
}

//===========================================================================//

List<const WeakPtr<BaseDataProvider>> ParametersSection::GetAnonymousChildren(const string & root) const
{
	List<const WeakPtr<BaseDataProvider>> result;
	for (const auto & item:_children[root]->_listChildren)
	{
		if (string::Empty != item.GetKey() && !item.GetKey().IsNumber())
		{
			INVALID_STATE_EXCEPTION(string::Format("List of anonymous sections contains named section {0}", item.GetKey()));
		}
		result.Add(item.GetValue());
	}
	return result;
}

//===========================================================================//

integer ParametersSection::GetAnonymousChildrenCount() const
{
	return _listChildren.GetCount();
}

//===========================================================================//

void ParametersSection::Clear()
{
	for (auto & childPair:_children)
	{
		childPair.GetValue()->Clear();
	}
	_children.Clear();

	for (auto & listChildPair:_listChildren)
	{
		listChildPair.GetValue()->Clear();
	}
	_listChildren.Clear();

	_parameters.Clear();
}

//===========================================================================//

void ParametersSection::WriteParameters(const SharedPtr<IOutputStream> & stream, integer offset)
{
	string indentString;
	for (integer i = 0; i < offset; i++)
	{
		indentString += "\t";
	}

	for (auto & parameterPair:_parameters)
	{
		stream->WriteLine(string::Format("{0}{1} = {2}", indentString, parameterPair.GetKey(), parameterPair.GetValue()));
	}

	bool isFirstChild = true;
	for (auto & childPair:_children)
	{
		if (!isFirstChild || _parameters.GetCount() > 0)
		{
			stream->WriteLine(string::Empty);
		}
		else
		{
			isFirstChild = false;
		}

		stream->WriteLine(string::Format("{0}#{1}", indentString, childPair.GetValue()->GetName()));

		stream->WriteLine(string::Format("{0}{{", indentString));
		childPair.GetValue()->WriteParameters(stream, offset + TabIndentsCount);
		stream->WriteLine(string::Format("{0}}}", indentString));
	}

	for (auto & listChildPair:_listChildren)
	{
		stream->WriteLine(string::Format("{0}{{", indentString));
		listChildPair.GetValue()->WriteParameters(stream, offset + TabIndentsCount);
		stream->WriteLine(string::Format("{0}}}", indentString));
	}
}

//===========================================================================//

const string ParametersSection::GetName()
{
	return _name;
}

//===========================================================================//

WeakPtr<ParametersSection> ParametersSection::GetParent() const
{
	return _parent;
}

//===========================================================================//

void ParametersSection::SplitPath(const string & originalPath, string & headPart, string & tailPart)
{
	string path = originalPath;
	integer delimiterIndex = path.FirstIndexOf(PathDelimiter);

	if (delimiterIndex != -1)
	{
		headPart = path.Substring(0, delimiterIndex);

		integer searchPathSubstringLength = path.GetLength() - delimiterIndex - PathDelimiter.GetLength();
		tailPart = path.Substring(delimiterIndex + PathDelimiter.GetLength(), searchPathSubstringLength);
	}
	else
	{
		headPart = path;
		tailPart = string::Empty;
	}
}

//===========================================================================//

} // namespace Bru

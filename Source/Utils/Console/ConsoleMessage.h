//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: сообщение с приоритетом
//

#pragma once
#ifndef CONSOLE_MESSAGE_H
#define CONSOLE_MESSAGE_H

#include "../../Common/Common.h"
#include "../../Utils/Enums/MessagePriority.h"

namespace Bru
{

//===========================================================================//

class ConsoleMessage
{
public:
	ConsoleMessage(MessagePriority priority, const string & text);
	~ConsoleMessage();

	MessagePriority GetPriority() const;
	const string & GetText() const;

private:
	MessagePriority _priority;
	string _text;
};

//===========================================================================//

} // namespace Bru

#endif // CONSOLE_MESSAGE_H

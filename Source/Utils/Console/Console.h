//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс представляющий внутреннее устройство командной консоли
//

#pragma once
#ifndef CONSOLE_H
#define CONSOLE_H

#include "../../Common/EventSystem/InnerEvent.h"
#include "../../Common/EventSystem/Event.h"
#include "../../Utils/Enums/MessagePriority.h"
#include "ConsoleMessage.h"

#include <mutex>

namespace Bru
{

//===========================================================================//

class ConsoleSingleton
{
public:
	ConsoleSingleton(MessagePriority minimalPriority);
	~ConsoleSingleton();

	///Метод выводит сообщение в консоль и переходит на новую строку,
	///поэтому нет необходимости в \n в конце предложения
	void WriteMessage(MessagePriority priority, const string & message);
	template<typename ... Args>
	void WriteMessage(MessagePriority priority, const string & messageWithFormat, Args ...);

	template<typename ... Args>
	void Debug(string const & messageWithFormat, Args ...);
	template<typename ... Args>
	void Notice(const string & messageWithFormat, Args ...);
	template<typename ... Args>
	void Hint(const string & messageWithFormat, Args ...);
	template<typename ... Args>
	void Command(const string & messageWithFormat, Args ...);
	template<typename ... Args>
	void Warning(const string & messageWithFormat, Args ...);
	template<typename ... Args>
	void Error(const string & messageWithFormat, Args ...);

	void SetOffset(integer newOffser);
	void AddOffset(integer deltaOffset);
	void ReduceOffset(integer deltaOffset);
	void ResetOffset();

	integer GetOffset();

	bool Evaluate(const string & commandText);

	const List<ConsoleMessage> & GetMessages() const;

private:
	InnerEvent<const ConsoleMessage &> _newMessage;

public:
	Event<const ConsoleMessage &> NewMessage;

private:
	static const string Name;

	void AddException(const Exception & exception);

	MessagePriority _minimalPriority;

	List<ConsoleMessage> _messages;
	integer _offset;

	SharedPtr<EventHandler<const Exception &>> _newExceptionRegisteredHandler;

	std::recursive_mutex _mutex;
};

//===========================================================================//

extern SharedPtr<ConsoleSingleton> Console;

//===========================================================================//

template<typename ... Args>
void ConsoleSingleton::WriteMessage(MessagePriority priority, const string & messageWithFormat, Args ... args)
{
	string finalMessage = string::Format(messageWithFormat, args ...);
	WriteMessage(priority, finalMessage);
}

//===========================================================================//

template<typename ... Args>
void ConsoleSingleton::Debug(string const & messageWithFormat, Args ... args)
{
	WriteMessage(MessagePriority::Debug, messageWithFormat, args ...);
}

//===========================================================================//

template<typename ... Args>
void ConsoleSingleton::Notice(string const & messageWithFormat, Args ... args)
{
	WriteMessage(MessagePriority::Notice, messageWithFormat, args ...);
}

//===========================================================================//

template<typename ... Args>
void ConsoleSingleton::Hint(string const & messageWithFormat, Args ... args)
{
	WriteMessage(MessagePriority::ConsoleHint, messageWithFormat, args ...);
}

//===========================================================================//

template<typename ... Args>
void ConsoleSingleton::Command(string const & messageWithFormat, Args ... args)
{
	WriteMessage(MessagePriority::ConsoleCommand, messageWithFormat, args ...);
}

//===========================================================================//

template<typename ... Args>
void ConsoleSingleton::Warning(string const & messageWithFormat, Args ... args)
{
	WriteMessage(MessagePriority::Warning, messageWithFormat, args ...);
}

//===========================================================================//

template<typename ... Args>
void ConsoleSingleton::Error(string const & messageWithFormat, Args ... args)
{
	WriteMessage(MessagePriority::Error, messageWithFormat, args ...);
}

//===========================================================================//

} // namespace Bru

#endif // CONSOLE_H

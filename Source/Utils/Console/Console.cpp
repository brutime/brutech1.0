//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Console.h"

#include "../../ScriptSystem/ScriptSystem.h"
#include "../Log/Log.h"
#include "../PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

SharedPtr<ConsoleSingleton> Console;

//===========================================================================//

const string ConsoleSingleton::Name = "Console";

//===========================================================================//

ConsoleSingleton::ConsoleSingleton(MessagePriority minimalPriority) :
	_newMessage(),
	NewMessage(_newMessage),
	_minimalPriority(minimalPriority),
	_messages(),
	_offset(0),
	_newExceptionRegisteredHandler(new EventHandler<const Exception &>(this, &ConsoleSingleton::AddException)),
	_mutex()
{
	Log = new LogSingleton("Log.html", LogType::HTML);

	ExceptionRegistrator->NewExceptionRegistered.AddHandler(_newExceptionRegisteredHandler);
}

//===========================================================================//

ConsoleSingleton::~ConsoleSingleton()
{
	ExceptionRegistrator->NewExceptionRegistered.RemoveHandler(_newExceptionRegisteredHandler);

	Log = NullPtr;
}

//===========================================================================//

void ConsoleSingleton::WriteMessage(MessagePriority priority, const string & message)
{
	if (priority < _minimalPriority)
	{
		return;
	}
	
	std::lock_guard<std::recursive_mutex> guard(_mutex);
	
	string offsetedMessage;
	for (integer i = 0; i < _offset; i++)
	{
		offsetedMessage += " ";
	}
	offsetedMessage += message;

	_messages.Add(ConsoleMessage(priority, offsetedMessage));
	Log->WriteMessage(priority, offsetedMessage);
	_newMessage.Fire(ConsoleMessage(priority, offsetedMessage));
}

//===========================================================================//

void ConsoleSingleton::SetOffset(integer newOffser)
{
	_offset = newOffser;
}

//===========================================================================//

void ConsoleSingleton::AddOffset(integer deltaOffset)
{
	_offset += deltaOffset;
}

//===========================================================================//

void ConsoleSingleton::ReduceOffset(integer deltaOffset)
{
	_offset -= deltaOffset;
}

//===========================================================================//

void ConsoleSingleton::ResetOffset()
{
	_offset = 0;
}

//===========================================================================//

integer ConsoleSingleton::GetOffset()
{
	return _offset;
}

//===========================================================================//

const List<ConsoleMessage> & ConsoleSingleton::GetMessages() const
{
	return _messages;
}

//===========================================================================//

bool ConsoleSingleton::Evaluate(const string & commandText)
{
	std::lock_guard<std::recursive_mutex> guard(_mutex);
	
	ParsedCommandText parsedCommandText = ScriptSystem->ParseCommandText(commandText);
	if (!ScriptSystem->IsCommandRegistered(parsedCommandText))
	{
		Error(string::Format("Command \"{0}\" not registred. Use \"Help\" command to see a list of available commands",
		                     parsedCommandText.CommandName));
		return false;
	}

	if (!ScriptSystem->IsCommandArgumentsCorrect(parsedCommandText))
	{
		Error(string::Format("Command \"{0}\" taking {1} argument(s) not registered",
		                     parsedCommandText.CommandName, parsedCommandText.Args.GetCount()));
		return false;
	}

	Command(commandText);

	CommandExecutionResult result = ScriptSystem->Evaluate(commandText);
	if (result.ExecutedSuccessfully && result.ContainsValue)
	{
		Command(result.ResultString);		
	}
	
	return result.ExecutedSuccessfully;
}

//===========================================================================//

void ConsoleSingleton::AddException(const Exception & exception)
{
	std::lock_guard<std::recursive_mutex> guard(_mutex);
	
	Error("Exception occurred:");
	Error(string::Format("type:    {0}", exception.GetType()));
	Error(string::Format("details: {0}", exception.GetDetails()));
	Error(string::Format("file:    {0}", PathHelper::GetFileName(exception.GetFileName(), 2)));
	Error(string::Format("method:  {0}", exception.GetMethodName()));
	Error(string::Format("line:    {0}", exception.GetFileLine()));
}

//===========================================================================//

} // namespace Bru

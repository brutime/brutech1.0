//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ConsoleMessage.h"

namespace Bru
{

//===========================================================================//

ConsoleMessage::ConsoleMessage(MessagePriority priority, const string & text) :
	_priority(priority),
	_text(text)
{
}

//===========================================================================//

ConsoleMessage::~ConsoleMessage()
{
}

//===========================================================================//

MessagePriority ConsoleMessage::GetPriority() const
{
	return _priority;
}

//===========================================================================//

const string & ConsoleMessage::GetText() const
{
	return _text;
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PathHelper.h"

#include "../Log/Log.h"
#include "../../Common/Exceptions/InvalidArgumentException.h"

namespace Bru
{

//===========================================================================//

const string PathHelper::ParametersFileExtension = "pf";

const string PathHelper::JPGExtension = "jpg";
const string PathHelper::OGGExtension = "ogg";
const string PathHelper::PNGExtension = "png";
const string PathHelper::TGAExtension = "tga";
const string PathHelper::TTFExtension = "ttf";
const string PathHelper::WAVExtension = "wav";

const string PathHelper::PathToData = "../../Data/";
const string PathHelper::PathToScreenshots = "../../Screenshots/";
const string PathHelper::PathToAnimatedSprites = "AnimatedSprites/";
const string PathHelper::PathToComponents = "Components/Components";
const string PathHelper::PathToCursors = "Cursors/";
const string PathHelper::PathToFonts = PathToData + "Fonts/";
const string PathHelper::PathToLocations = "Locations/";
const string PathHelper::PathToEntities = "Entities/";
const string PathHelper::PathToRenderableTexts = "RenderableTexts/";
const string PathHelper::PathToSkeletalMeshes = "SkeletalMeshes/";
const string PathHelper::PathToSkeletons = "Skeletons/";
const string PathHelper::PathToSounds = "Sounds/";
const string PathHelper::PathToSprites = "Sprites/";
const string PathHelper::PathToText = "Text/";
const string PathHelper::PathToTextures = "Textures/";

const string PathHelper::LocationsListFileName = "Locations";

const string PathHelper::PathToAlphabets = PathToText + "Alphabets";
const string PathHelper::PathToConfig = "Config";

const string PathHelper::PathToComponentsSection = "Components";
const string PathHelper::PathToChildrenSection = "Children";

static const utf8_char ForwardSlashSeparartor = "/";
static const utf8_char BackslashSeparartor = "\\";
static const utf8_char ExtensionSeparator = ".";

//===========================================================================//

string PathHelper::GetFullPathToJPGImageFile(const string & pathToFile)
{
	return PathToData + PathToTextures + pathToFile + "." + JPGExtension;
}

//===========================================================================//

string PathHelper::GetFullPathToPNGImageFile(const string & pathToFile)
{
	return PathToData + PathToTextures + pathToFile + "." + PNGExtension;
}

//===========================================================================//

string PathHelper::GetFullPathToTGAImageFile(const string & pathToFile)
{
	return PathToData + PathToTextures + pathToFile + "." + TGAExtension;
}

//===========================================================================//

string PathHelper::GetFullPathToAnimatedSpriteFile(const string & pathToFile)
{
	return PathToAnimatedSprites + pathToFile;
}

//===========================================================================//

string PathHelper::GetFullPathToFontFile(const string & pathToFile)
{
	return PathToFonts + pathToFile + "." + TTFExtension;
}

//===========================================================================//

string PathHelper::GetFullPathToLocationFile(const string & pathToFile)
{
	return PathToLocations + pathToFile;
}

//===========================================================================//

string PathHelper::GetFullPathToEntityFile(const string & pathToFile)
{
	return PathToEntities + pathToFile;
}

//===========================================================================//

string PathHelper::GetFullPathToRenderableTextFile(const string & pathToFile)
{
	return PathToRenderableTexts + pathToFile;
}

//===========================================================================//

string PathHelper::GetFullPathToSpriteFile(const string & pathToFile)
{
	return PathToSprites + pathToFile;
}

//===========================================================================//

string PathHelper::GetFullPathToTextureFile(const string & pathToFile)
{
	return PathToTextures + pathToFile;
}

//===========================================================================//

string PathHelper::GetPathWithoutExtension(const string & path)
{
	if (path.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("Path is empty");
	}

	string correctedPath = path.Replace(BackslashSeparartor, ForwardSlashSeparartor);

	if (!path.Contains(ExtensionSeparator))
	{
		return correctedPath;
	}

	integer index = correctedPath.LastIndexOf(ExtensionSeparator);

	if (index == 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Path has only extension: \"{0}\"", path));
	}

	return correctedPath.Substring(0, index);
}

//===========================================================================//

string PathHelper::GetFileName(const string & path, const integer nestingCount)
{
	if (path.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("Path is empty");
	}

	if (nestingCount <= 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("nestingCount must be >= 0, but it is {0}", nestingCount));
	}

	string correctedPath = path.Replace(BackslashSeparartor, ForwardSlashSeparartor);
	integer index = correctedPath.LastIndexNumberOf(ForwardSlashSeparartor, nestingCount);

	//Путь может начиниться не со слешей
	if (index == -1 && correctedPath.ContainsCount(ForwardSlashSeparartor) < nestingCount - 1)
	{
		integer separatorsCount = correctedPath.ContainsCount(ForwardSlashSeparartor);
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("nestingCount {0} > max nesting level {1} in path \"{2}\"", nestingCount, separatorsCount, path));
	}

	return correctedPath.Substring(index + 1);
}

//===========================================================================//

string PathHelper::GetFileNameWithoutExtension(const string & path)
{
	if (path.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("Path is empty");
	}

	if (!path.Contains(ExtensionSeparator))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Path not contains extension separator: \"{0}\"", path));
	}

	string fileName = GetFileName(path, 1);
	integer i = fileName.LastIndexOf(ExtensionSeparator);
	return fileName.Substring(0, i);
}

//===========================================================================//

string PathHelper::GetExtension(const string & path)
{
	if (path.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("Path is empty");
	}

	if (!path.Contains(ExtensionSeparator))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Path not contains extension separator: \"{0}\"", path));
	}

	string fileName = GetFileName(path, 1);
	integer i = fileName.LastIndexOf(ExtensionSeparator);
	return fileName.Substring(i + 1, fileName.GetLength() - i - 1);
}

//===========================================================================//

bool PathHelper::IsSupportedImageExtension(const string & path)
{
	if (path.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("Path is empty");
	}

	string extension = GetExtension(path);
	return extension == JPGExtension || extension == PNGExtension || extension == TGAExtension;
}

//===========================================================================//

string PathHelper::GetResourceNameByPathToFile(const string & path)
{
	if (path.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("Path is empty");
	}

	string resourceName = PathHelper::GetPathWithoutExtension(path);

	//Не смотря на то, что путь неверен - он содержит какие-то символы,
	//и к такому имени можно привязать ресурс
	if (resourceName.IsEmpty())
	{
		return path;
	}

	return resourceName;
}

//===========================================================================//

}//namespace

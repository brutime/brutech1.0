//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс возвращает пути к ресурсам и извлекает из строки расширение,
//    имя файла с учетом его вложенности
//

#pragma once
#ifndef PATH_HELPER_H
#define PATH_HELPER_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class PathHelper
{
public:
	static const string ParametersFileExtension;
	static const string JPGExtension;
	static const string OGGExtension;
	static const string PNGExtension;
	static const string TGAExtension;
	static const string TTFExtension;
	static const string WAVExtension;

	static const string PathToData;
	static const string PathToScreenshots;

	static const string PathToAnimatedSprites;
	static const string PathToComponents;
	static const string PathToCursors;
	static const string PathToFonts;
	static const string PathToLocations;
	static const string PathToEntities;
	static const string PathToRenderableTexts;
	static const string PathToSkeletons;
	static const string PathToSkeletalMeshes;
	static const string PathToSounds;
	static const string PathToSprites;
	static const string PathToText;
	static const string PathToTextures;
	
	static const string LocationsListFileName;

	static const string PathToAlphabets;
	static const string PathToConfig;

	static const string PathToComponentsSection;
	static const string PathToChildrenSection;

	static string GetFullPathToJPGImageFile(const string & pathToFile);
	static string GetFullPathToPNGImageFile(const string & pathToFile);
	static string GetFullPathToTGAImageFile(const string & pathToFile);

	static string GetFullPathToAnimatedSpriteFile(const string & pathToFile);
	static string GetFullPathToFontFile(const string & pathToFile);
	static string GetFullPathToLocationFile(const string & pathToFile);
	static string GetFullPathToEntityFile(const string & pathToFile);
	static string GetFullPathToRenderableTextFile(const string & pathToFile);
	static string GetFullPathToSpriteFile(const string & pathToFile);
	static string GetFullPathToTextureFile(const string & pathToFile);

	///Возвращает путь без раcширения
	static string GetPathWithoutExtension(const string & path);
	///Возвращает имя файла с укзанной вложенностью по его пути
	static string GetFileName(const string & path, const integer nestingCount = 1);
	///Возвращает имя файла по его пути без раcширения
	static string GetFileNameWithoutExtension(const string & path);
	///Возвращает расширение файла по его пути
	static string GetExtension(const string & path);
	///Метод возвращает истину, если путь содержит не поддерживаемое разрешение изображения
	static bool IsSupportedImageExtension(const string & path);
	///Метод возвращает имя ресурса по его пути
	static string GetResourceNameByPathToFile(const string & path);

private:
	PathHelper() = delete;
	PathHelper(const PathHelper &) = delete;
	~PathHelper() = delete;
	PathHelper & operator =(const PathHelper &) = delete;
};

//===========================================================================//

}//namespace

#endif // PATH_HELPER_H

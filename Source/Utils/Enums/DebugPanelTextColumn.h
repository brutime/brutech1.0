//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Перечисление колонок, в которые может быть добавлен текст в DebugPanel
//

#ifndef DEBUG_PANEL_TEXT_COLUMN_H
#define DEBUG_PANEL_TEXT_COLUMN_H

namespace Bru
{

//===========================================================================//

enum class DebugPanelTextColumn
{
    Left,
    Center,
    Right
};

//===========================================================================//

}
#endif // DEBUG_PANEL_TEXT_COLUMN_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Приоритет сообщения в лог файле
//

#pragma once
#ifndef MESSAGE_PRIORITY_H
#define MESSAGE_PRIORITY_H

namespace Bru
{

//===========================================================================//

enum class MessagePriority
{
    Debug,
    Notice,
	ConsoleHint,
    ConsoleCommand,
    Warning,
    Error
};

//===========================================================================//

}//namespace

#endif // MESSAGE_PRIORITY_H

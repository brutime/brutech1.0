//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Watcher.h"

namespace Bru
{

//===========================================================================//

SharedPtr<WatcherSingleton> Watcher;

//===========================================================================//

const string WatcherSingleton::Name = "Watcher";

//===========================================================================//

WatcherSingleton::WatcherSingleton() : ExpressionWatcher()

{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);
	
	ScriptSystem->RegisterCommand(new ClassConstCommand<ExpressionWatcher, string, const string &>(
	                                  "Watch", this, &ExpressionWatcher::Watch));	

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

WatcherSingleton::~WatcherSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	ScriptSystem->UnregisterCommand("Watch");

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

} // namespace Bru

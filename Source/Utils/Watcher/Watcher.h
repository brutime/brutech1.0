//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Хранит ссылки на именованные переменные для отладки
//

#pragma once
#ifndef WATCHER_H
#define WATCHER_H

#include "../../Common/Common.h"
#include "../ExpressionWatcher/ExpressionWatcher.h"

namespace Bru
{

//===========================================================================//

class WatcherSingleton : public ExpressionWatcher
{
public:
	WatcherSingleton();
	virtual ~WatcherSingleton();

private:
	static const string Name;

	WatcherSingleton(const WatcherSingleton &) = delete;
	WatcherSingleton & operator =(const WatcherSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<WatcherSingleton> Watcher;

//===========================================================================//

} // namespace Bru

#endif // WATCHER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PercentageMeasurement.h"

namespace Bru
{

//===========================================================================//

PercentageMeasurement::PercentageMeasurement(const string & name, float time, float percent) :
	Name(name),
	Time(time),
	Percent(percent)
{
}

//===========================================================================//

PercentageMeasurement::~PercentageMeasurement()
{
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: 
//

#pragma once
#ifndef PERCENTAGE_MEASUREMENT_H
#define PERCENTAGE_MEASUREMENT_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

struct PercentageMeasurement
{
	PercentageMeasurement(const string & name, float time, float percent);
	~PercentageMeasurement();
	
	string Name;
	float Time;
	float Percent;
    
	PercentageMeasurement(const PercentageMeasurement &) = delete;
	PercentageMeasurement& operator =(const PercentageMeasurement &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PERCENTAGE_MEASUREMENT_H
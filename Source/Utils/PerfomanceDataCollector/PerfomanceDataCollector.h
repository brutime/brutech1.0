//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef PERFOMANCE_DATA_COLLECTOR_H
#define PERFOMANCE_DATA_COLLECTOR_H

#include "../../Common/Common.h"
#include "../../OperatingSystem/OperatingSystem.h"
#include "../ExpressionWatcher/ExpressionWatcher.h"
#include "PercentageMeasurement.h"

namespace Bru
{

//===========================================================================//

class PerfomanceDataCollectorSingleton
{
public:
	PerfomanceDataCollectorSingleton(float updatingIntervalTime);
	~PerfomanceDataCollectorSingleton();

	void AddMeasurement(const string & measurementName);
	void RemoveMeasurement(const string & measurementName);

	void StartMeasurement(const string & measurementName);
	void EndMeasurement(const string & measurementName);

	void AddSubtraction(const string & subtractionName);
	void RemoveSubtraction(const string & subtractionName);
	
	void StartSubtraction(const string & subtractionName);
	void EndSubtraction(const string & subtractionName);

	void Update();

	const SharedPtr<PercentageMeasurement> & GetPercentageMeasurement(const string & measurementName) const;
	const AddingOrderedTreeMap<string, SharedPtr<PercentageMeasurement>> & GetPercentageMeasurements() const;

private:
	Timer _timer;
	int64 _currentTicks;
	float _updatingIntervalTime;

	AddingOrderedTreeMap<string, ValuePair<float, float>> _measurements;
	AddingOrderedTreeMap<string, SharedPtr<PercentageMeasurement>> _percentageMeasurements;
	
	TreeMap<string, ValuePair<float, float>> _subtractions;

private:
	InnerEvent<const string &> _measurementAdded;
	InnerEvent<const string &> _measurementRemoved;

public:
	Event<const string &> MeasurementAdded;
	Event<const string &> MeasurementRemoved;

private:
	PerfomanceDataCollectorSingleton(const PerfomanceDataCollectorSingleton &) = delete;
	PerfomanceDataCollectorSingleton & operator =(const PerfomanceDataCollectorSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<PerfomanceDataCollectorSingleton> PerfomanceDataCollector;

//===========================================================================//

} // namespace Bru

#endif // PERFOMANCE_DATA_COLLECTOR_H

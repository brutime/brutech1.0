//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PerfomanceDataCollector.h"

#include "../../OperatingSystem/OperatingSystem.h"
#include "../Console/Console.h"

namespace Bru
{

//===========================================================================//

SharedPtr<PerfomanceDataCollectorSingleton> PerfomanceDataCollector;

//===========================================================================//

PerfomanceDataCollectorSingleton::PerfomanceDataCollectorSingleton(float updatingIntervalTime) :
	_timer(),
	_currentTicks(0),
	_updatingIntervalTime(updatingIntervalTime),
	_measurements(),
	_percentageMeasurements(),
	_subtractions(),
	_measurementAdded(),
	_measurementRemoved(),
	MeasurementAdded(_measurementAdded),
	MeasurementRemoved(_measurementRemoved)
{
	AddMeasurement("Other");
}

//===========================================================================//

PerfomanceDataCollectorSingleton::~PerfomanceDataCollectorSingleton()
{
	RemoveMeasurement("Other");
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::AddMeasurement(const string & measurementName)
{
	_measurements.Add(measurementName, ValuePair<float, float>(0.0f, 0.0f));
	_percentageMeasurements.Add(measurementName, new PercentageMeasurement(measurementName, 0.0f, 0.0f));
	_measurementAdded.Fire(measurementName);
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::RemoveMeasurement(const string & measurementName)
{
	_measurements.Remove(measurementName);
	_percentageMeasurements.Remove(measurementName);
	_measurementRemoved.Fire(measurementName);
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::StartMeasurement(const string & measurementName)
{
	_measurements[measurementName].Second = _timer.GetSeconds();
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::EndMeasurement(const string & measurementName)
{
	_measurements[measurementName].First += _timer.GetSeconds() - _measurements[measurementName].Second;
	_measurements[measurementName].Second = 0.0f;
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::AddSubtraction(const string & subtractionName)
{
	_subtractions.Add(subtractionName, ValuePair<float, float>(0.0f, 0.0f));
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::RemoveSubtraction(const string & subtractionName)
{
	_subtractions.Remove(subtractionName);
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::StartSubtraction(const string & subtractionName)
{
	_subtractions[subtractionName].Second = _timer.GetSeconds();
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::EndSubtraction(const string & subtractionName)
{
	_subtractions[subtractionName].First += _timer.GetSeconds() - _subtractions[subtractionName].Second;
	_subtractions[subtractionName].Second = 0.0f;
}

//===========================================================================//

void PerfomanceDataCollectorSingleton::Update()
{
	float _deltaTime = static_cast<float>(_timer.GetTicks() - _currentTicks) / _timer.GetFrequency();
	if (_deltaTime < _updatingIntervalTime)
	{
		return;
	}

	for (const auto & subtraction : _subtractions)
	{
		_deltaTime -= subtraction.GetValue().First;

		_subtractions[subtraction.GetKey()].First = 0.0f;
		_subtractions[subtraction.GetKey()].Second = 0.0f;
	}

	for (const auto & measurement : _measurements)
	{
		float time = measurement.GetValue().First;
		float percent = time / _deltaTime * 100.0f;
		_percentageMeasurements[measurement.GetKey()] = new PercentageMeasurement(measurement.GetKey(), time, percent);

		_measurements[measurement.GetKey()].First = 0.0f;
		_measurements[measurement.GetKey()].Second = 0.0f;
	}

	float summaryMeasurementTime = 0.0f;
	for (const auto & percentageMeasurement : _percentageMeasurements)
	{
		summaryMeasurementTime += percentageMeasurement.GetValue()->Time;
	}

	float otherTime = _deltaTime - summaryMeasurementTime;
	otherTime = Math::Max(otherTime, 0.0f);
	float otherPercent = otherTime / _deltaTime * 100.0f;
	_percentageMeasurements["Other"] = new PercentageMeasurement("Other", otherTime, otherPercent);

	_currentTicks = _timer.GetTicks();
}

//===========================================================================//

const SharedPtr<PercentageMeasurement> & PerfomanceDataCollectorSingleton::GetPercentageMeasurement(const string & measurementName) const
{
	return _percentageMeasurements[measurementName];
}

//===========================================================================//

const AddingOrderedTreeMap<string, SharedPtr<PercentageMeasurement>> & PerfomanceDataCollectorSingleton::GetPercentageMeasurements() const
{
	return _percentageMeasurements;
}

//===========================================================================//

} // namespace Bru

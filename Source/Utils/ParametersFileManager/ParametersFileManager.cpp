//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "ParametersFileManager.h"

#include "../../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

SharedPtr<ParametersFileManagerSingleton> ParametersFileManager;

//===========================================================================//

const string ParametersFileManagerSingleton::Name = "Parameters file manager";

//===========================================================================//

ParametersFileManagerSingleton::ParametersFileManagerSingleton() :
	_parametersFiles()
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

ParametersFileManagerSingleton::~ParametersFileManagerSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	Clear();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

WeakPtr<ParametersFile> ParametersFileManagerSingleton::Load(const string & pathToFile)
{
	if (_parametersFiles.Contains(pathToFile))
	{
		Console->Warning(string::Format("ParametersFileManager: File already : \"{0}\"", pathToFile));
		return Get(pathToFile);
	}

	SharedPtr<ParametersFile> parametersFile = new ParametersFile(pathToFile);
	_parametersFiles.Add(pathToFile, parametersFile);
	return parametersFile;
}

//===========================================================================//

WeakPtr<ParametersFile> ParametersFileManagerSingleton::Reload(const string & pathToFile)
{
	if (!_parametersFiles.Contains(pathToFile))
	{
		Console->Warning(string::Format("ParametersFileManager: File not found : \"{0}\"", pathToFile));
	}
	else
	{
		Remove(pathToFile);
	}
	
	return Load(pathToFile);
}

//===========================================================================//

WeakPtr<ParametersFile> ParametersFileManagerSingleton::Get(const string & pathToFile)
{
	if (!_parametersFiles.Contains(pathToFile))
	{
		Console->Warning(string::Format("ParametersFileManager: File not found in manager: \"{0}\", attempt to load", pathToFile));
		return Load(pathToFile);
	}

	return _parametersFiles[pathToFile];
}

//===========================================================================//

bool ParametersFileManagerSingleton::Contains(const string & pathToFile) const
{
	return _parametersFiles.Contains(pathToFile);
}

//===========================================================================//

void ParametersFileManagerSingleton::Remove(const string & pathToFile)
{
	if (!_parametersFiles.Contains(pathToFile))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("File not found in manager: \"{0}\"", pathToFile));
	}

	return _parametersFiles.Remove(pathToFile);
}

//===========================================================================//

void ParametersFileManagerSingleton::Clear()
{
	_parametersFiles.Clear();
}

//===========================================================================//

List<string> ParametersFileManagerSingleton::GetParametersFileList()
{
	List<string> parametersFilesList;

	for (auto & parameterFilePair : _parametersFiles)
	{
		parametersFilesList.Add(parameterFilePair.GetKey());
	}

	return parametersFilesList;
}

//===========================================================================//

}// namespace

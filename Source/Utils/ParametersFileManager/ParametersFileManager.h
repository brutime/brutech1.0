//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Менеджер файлов параметров
//

#pragma once
#ifndef PARAMETERS_FILE_MANAGER_H
#define PARAMETERS_FILE_MANAGER_H

#include "../ParametersFile/ParametersFile.h"

namespace Bru
{

//===========================================================================//

class ParametersFileManagerSingleton
{
public:
	ParametersFileManagerSingleton();
	~ParametersFileManagerSingleton();

	WeakPtr<ParametersFile> Load(const string & pathToFile);
	WeakPtr<ParametersFile> Reload(const string & pathToFile);
	WeakPtr<ParametersFile> Get(const string & pathToFile);
	void Remove(const string & pathToFile);
	
	bool Contains(const string & pathToFile) const;

	///Удаляются все файлы
	void Clear();

	List<string> GetParametersFileList();

private:
	static const string Name;

	TreeMap<string, SharedPtr<ParametersFile> > _parametersFiles;

	ParametersFileManagerSingleton(const ParametersFileManagerSingleton &) = delete;
	ParametersFileManagerSingleton & operator =(const ParametersFileManagerSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<ParametersFileManagerSingleton> ParametersFileManager;

//===========================================================================//

}// namespace

#endif // PARAMETERS_FILE_MANAGER_H

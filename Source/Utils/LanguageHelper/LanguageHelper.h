//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef LANGUAGE_HELPER_H
#define LANGUAGE_HELPER_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class LanguageHelperSingleton
{
public:
	LanguageHelperSingleton();
	~LanguageHelperSingleton();

	string GetCurrentAlphabet()
	{
		return currentAlphabet;
	}

private:
	static const string Name;

	static const string CurrentAlphabetPath;
	static const string AlphabetPath;
	static const string SystemAlphabetPath;

	string currentAlphabet;

	LanguageHelperSingleton(const LanguageHelperSingleton &) = delete;
	LanguageHelperSingleton & operator =(const LanguageHelperSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<LanguageHelperSingleton> LanguageHelper;

//===========================================================================//

}//namespace

#endif // LANGUAGE_HELPER_H

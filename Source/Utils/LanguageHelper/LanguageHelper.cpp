//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "LanguageHelper.h"

#include "../ParametersFile/ParametersFile.h"
#include "../Config/Config.h"
#include "../Console/Console.h"
#include "../PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

SharedPtr<LanguageHelperSingleton> LanguageHelper;

//===========================================================================//

const string LanguageHelperSingleton::Name = "Language helper";

const string LanguageHelperSingleton::CurrentAlphabetPath = "Language.Current";
const string LanguageHelperSingleton::AlphabetPath = "Alphabet";
const string LanguageHelperSingleton::SystemAlphabetPath = "English";

//===========================================================================//

LanguageHelperSingleton::LanguageHelperSingleton() :
	currentAlphabet()
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	string currentAlphabetName = Config->Get<string>(CurrentAlphabetPath);

	if (currentAlphabetName != SystemAlphabetPath)
	{
		ParametersFile alphabetsFile(PathHelper::PathToAlphabets);
		string pathToAlphabet = currentAlphabetName + "." + AlphabetPath;
		currentAlphabet = alphabetsFile.Get<string>(pathToAlphabet);
	}
	else
	{
		currentAlphabet = string::Empty;
	}

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

LanguageHelperSingleton::~LanguageHelperSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#include "BaseDataProvider.h"

namespace Bru
{

//===========================================================================//

BaseDataProvider::BaseDataProvider()
{
}

//===========================================================================//

BaseDataProvider::~BaseDataProvider()
{
}

//===========================================================================//

template<>
void BaseDataProvider::Set<string>(const string & path, const string & value)
{
	SetParameter(path, string::Format("\"{0}\"", value));
}

//===========================================================================//

}

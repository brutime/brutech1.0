//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: интерфейс класса-поставщика данных
//

#pragma once
#ifndef I_DATA_PROVIDER_H
#define I_DATA_PROVIDER_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class BaseDataProvider
{
public:
	BaseDataProvider();
	virtual ~BaseDataProvider();

	template<typename T>
	T Get(const string & key) const;

	template<typename T>
	void Set(const string & path, const T & value);
	
	template<typename T>
	Array<T> GetArray(const string & key) const;
	
	virtual List<const WeakPtr<BaseDataProvider>> GetAnonymousChildren(const string &root) const = 0;
	virtual integer GetAnonymousChildrenCount() const = 0;

	virtual string GetParameter(const string & key) const = 0;
	virtual void SetParameter(const string & key, const string & value) = 0;

private:
	BaseDataProvider(const BaseDataProvider &) = delete;
	BaseDataProvider & operator =(const BaseDataProvider &) = delete;
};

//===========================================================================//

template<typename T>
T BaseDataProvider::Get(const string & path) const
{
	return Convert<string, T>(GetParameter(path));
}

//===========================================================================//

template<typename T>
void BaseDataProvider::Set(const string & path, const T & value)
{
	SetParameter(path, Convert<T, string>(value));
}

//===========================================================================//

template<>
void BaseDataProvider::Set<string>(const string & path, const string & value);

//===========================================================================//

template<typename T>
Array<T> BaseDataProvider::GetArray(const string & path) const
{
	Array<T> array;

	TokenParser parser(Get<string>(path));
	if (parser.GetTokensCount() == 0)
	{
		return array;
	}

	array.Resize(parser.GetTokensCount());
	for (integer i = 0; i < parser.GetTokensCount(); ++i)
	{
		array[i] = parser.GetToken(i);
	}

	return array;
}

//===========================================================================//

} // namespace Bru

#endif // I_DATA_PROVIDER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: vertex switch
//

#pragma once
#ifndef VERTEX_SWITCH_H
#define VERTEX_SWITCH_H

#include "../Common/Common.h"

#if GRAPHIC_API == OPEN_GL
#include "OpenGL/Vertex.h"
#elif GRAPHIC_API == DIRECT_3D
#include "Direct3D/Vertex.h"
#else
#error Graphic API not defined
#endif

#endif // VERTEX_SWITCH_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Mesh.h"

namespace Bru
{

//===========================================================================//

Mesh::Mesh(const Mesh & other) : 
	_vertices(other._vertices),
	_verticesMode(other._verticesMode),
	_indices(other._indices),
	_vertexPerFace(other._vertexPerFace),
	_maxIndex(other._maxIndex)
{	
}

//===========================================================================//

Mesh::Mesh(VerticesMode verticesMode) :
	_vertices(),
	_verticesMode(verticesMode),
	_indices(),
	_vertexPerFace(GetVertexPerFaceByVerticesMode(_verticesMode)),
	_maxIndex(0)
{
}

//===========================================================================//

Mesh::Mesh(VerticesMode verticesMode, integer reserveVertiesCount, integer reserveIndicesCount) :
	_vertices(),
	_verticesMode(verticesMode),
	_indices(),
	_vertexPerFace(GetVertexPerFaceByVerticesMode(_verticesMode)),
	_maxIndex(0)
{
	Resize(reserveVertiesCount, reserveIndicesCount);
}

//===========================================================================//

Mesh::~Mesh()
{
}

//===========================================================================//

Mesh & Mesh::operator =(const Mesh & other)
{
	if (this == &other)	
	{
		return *this;
	}
	
	_vertices = other._vertices;
	_verticesMode = other._verticesMode;
	_indices = other._indices;
	_vertexPerFace = other._vertexPerFace;
	_maxIndex = other._maxIndex;
	
	return *this;
}

//===========================================================================//

void Mesh::Resize(integer vertiesCount, integer indicesCount)
{
	_vertices.Resize(vertiesCount);
	_indices.Resize(indicesCount);
}

//===========================================================================//

void Mesh::Add(const Mesh & mesh)
{
	if (mesh._verticesMode != _verticesMode)
	{
		INVALID_ARGUMENT_EXCEPTION("Adding mesh's vertex mode != mesh's vertex mode");
	}

	_vertices.Add(mesh._vertices);

	integer newVerticesMaxIndex = _indices.GetCount() == 0 ? 0 : _maxIndex + 1;	
	for (integer i = 0; i < mesh._indices.GetCount(); ++i)
	{
		integer newIndex = newVerticesMaxIndex + mesh._indices[i];
		_indices.Add(newIndex);
		_maxIndex = Math::Max(newIndex, _maxIndex);
	}
}

//===========================================================================//

void Mesh::AddVertex(const Vector3 & position, const Vector2 & textureCoord, const Color & color)
{
	_vertices.Add(Vertex(position, textureCoord, color));
}

//===========================================================================//

void Mesh::SetVertex(integer vertexIndex, const Vector3 & position, const Vector2 & textureCoord, const Color & color)
{
	_vertices[vertexIndex].Position = position;
	_vertices[vertexIndex].TextureCoord = textureCoord;
	_vertices[vertexIndex].VertexColor = color;
}

//===========================================================================//

void Mesh::ClearVertices()
{
	_vertices.Clear();
}

//===========================================================================//

void Mesh::SetVertexPosition(integer vertexIndex, const Vector3 & vertexPosition)
{
	_vertices[vertexIndex].Position = vertexPosition;
}

//===========================================================================//

void Mesh::TranslateVertexPosition(integer vertexIndex, const Vector3 & deltaVertexPosition)
{
	_vertices[vertexIndex].Position += deltaVertexPosition;
}

//===========================================================================//

const Vector3 & Mesh::GetVertexPosition(integer vertexIndex) const
{
	return _vertices[vertexIndex].Position;
}

//===========================================================================//

void Mesh::ScalePosition(float xFactor, float yFactor, float zFactor)
{
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		_vertices[i].Position.X *= xFactor;
		_vertices[i].Position.Y *= yFactor;
		_vertices[i].Position.Z *= zFactor;
	}
}

//===========================================================================//

Vector3 Mesh::GetMinVertexPosition() const
{
	if (_vertices.GetCount() == 0)
	{
		return {};
	}

	Vector3 minVector = _vertices[0].Position;
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		minVector.X = Math::Min(minVector.X, _vertices[i].Position.X);
		minVector.Y = Math::Min(minVector.Y, _vertices[i].Position.Y);
		minVector.Z = Math::Min(minVector.Z, _vertices[i].Position.Z);
	}

	return minVector;
}

//===========================================================================//

Vector3 Mesh::GetMaxVertexPosition() const
{
	if (_vertices.GetCount() == 0)
	{
		return {};
	}

	Vector3 maxVector = _vertices[0].Position;
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		maxVector.X = Math::Max(maxVector.X, _vertices[i].Position.X);
		maxVector.Y = Math::Max(maxVector.Y, _vertices[i].Position.Y);
		maxVector.Z = Math::Max(maxVector.Z, _vertices[i].Position.Z);
	}

	return maxVector;
}

//===========================================================================//

void Mesh::SetVertexTextureCoord(integer vertexIndex, const Vector2 & vertexTextureCoord)
{
	_vertices[vertexIndex].TextureCoord = vertexTextureCoord;
}

//===========================================================================//

const Vector2 & Mesh::GetVertexTextureCoord(integer vertexIndex) const
{
	return _vertices[vertexIndex].TextureCoord;
}

//===========================================================================//

void Mesh::SetVertexColor(integer vertexIndex, const Color & color)
{
	_vertices[vertexIndex].VertexColor = color;
}

//===========================================================================//

const Color & Mesh::GetVertexColor(integer vertexIndex) const
{
	return _vertices[vertexIndex].VertexColor;
}

//===========================================================================//

void Mesh::SetVerticesColor(const Color & color)
{
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		_vertices[i].VertexColor = color;
	}
}

//===========================================================================//

void Mesh::SetVerticesColorAlpha(float alpha)
{
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		_vertices[i].VertexColor.SetAlpha(alpha);
	}
}

//===========================================================================//

const Array<Vertex>::SimpleType & Mesh::GetVertices() const
{
	return _vertices;
}

//===========================================================================//

VerticesMode Mesh::GetVerticesMode() const
{
	return _verticesMode;
}

//===========================================================================//

void Mesh::AddIndex(integer index)
{
	_indices.Add(index);
	_maxIndex = Math::Max(_maxIndex, index);
}

//===========================================================================//

void Mesh::SetIndex(integer indexIndex, integer index)
{
	_indices[indexIndex] = index;
	_maxIndex = Math::Max(_maxIndex, index);
}

//===========================================================================//

integer Bru::Mesh::GetMaxIndex() const
{
	return _maxIndex;
}

//===========================================================================//

const Array<uint>::SimpleType & Bru::Mesh::GetIndices() const
{
	return _indices;
}

//===========================================================================//

void Mesh::CopyVertexPositionFromOtherMesh(const Mesh & other)
{
	if (this == &other)
	{
		return;
	}

	if (_vertices.GetCount() != other._vertices.GetCount())
	{
		INVALID_ARGUMENT_EXCEPTION("Vertices count != other mesh's vertices count");
	}

	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		_vertices[i].Position = other._vertices[i].Position;
	}
}

//===========================================================================//

void Mesh::CopyTextureCoordFromOtherMesh(const Mesh & other)
{
	if (this == &other)
	{
		return;
	}

	if (_vertices.GetCount() != other._vertices.GetCount())
	{
		INVALID_ARGUMENT_EXCEPTION("Vertices count != other mesh's vertices count");
	}

	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		_vertices[i].TextureCoord = other._vertices[i].TextureCoord;
	}
}

//===========================================================================//

void Mesh::CopyIndicesFromOtherMesh(const Mesh & other)
{
	if (this == &other)
	{
		return;
	}

	_indices.Clear();
	_indices.Add(other._indices);
	_maxIndex = other._maxIndex;
}

//===========================================================================//

void Mesh::Clear()
{
	_vertices.Clear();
	_indices.Clear();
	_maxIndex = 0;
}

//===========================================================================//

integer Bru::Mesh::GetVertexPerFace() const
{
	return _vertexPerFace;
}

//===========================================================================//

string Mesh::ToString() const
{
	string meshString = string::Format("Mesh: vertices count: {0}; indices count: {1}\n", _vertices.GetCount(), _indices.GetCount());

	meshString += "Vertices:\n";
	for (integer i = 0; i < _vertices.GetCount(); ++i)
	{
		meshString += string::Format("{0:0.2} {1:0.2} {2:0.2}; {3:0.6} {4:0.6}\n",
		                             _vertices[i].Position.X, _vertices[i].Position.Y, _vertices[i].Position.Z,
		                             _vertices[i].TextureCoord.X,  _vertices[i].TextureCoord.Y);
	}

	meshString += "Indices:\n";
	for (integer i = 0; i < _indices.GetCount(); ++i)
	{
		meshString += string::Format("{0}\n", (integer) _indices[i]);
	}

	return meshString;
}

//===========================================================================//

integer Mesh::GetVertexPerFaceByVerticesMode(VerticesMode verticesMode)
{
	switch (verticesMode)
	{
	case VerticesMode::Points: return 1;
	case VerticesMode::Line: return 2;
	case VerticesMode::LineStrip: return 1;
	case VerticesMode::LinesLoop: return 1;
	case VerticesMode::Triangles: return 3;
	case VerticesMode::TriangleStrip: return 3;
	case VerticesMode::Polygon: return 1;

	default: INVALID_ARGUMENT_EXCEPTION("verticesMode not defined");
	}
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../VideoCardCapabilities.h"

#include "BruGL.h"

#include "../../Math/Math.h"
#include "../../OperatingSystem/OperatingSystem.h"

namespace Bru
{

//===========================================================================//

void VideoCardCapabilitiesSingleton::FindOutCapabilities()
{
	_vendorName = (const char *) glGetString(GL_VENDOR);
	_vendor = GetVendorByName(_vendorName);
	_modelName = (const char *) glGetString(GL_RENDERER);
	_hardwareVersionSupported = (const char *) glGetString(GL_VERSION);

    string extensions = (const char *)glGetString(GL_EXTENSIONS);
	
#if PLATFORM == WINDOWS

	_isVerticalSyncSupported = OperatingSystem->GetFuctionAddress("wglSwapIntervalEXT") != NullPtr;

#elif PLATFORM == LINUX
#elif PLATFORM == MAC
#else
#error Platform not defined
#endif	

	float version = Converter::ToFloat(_hardwareVersionSupported.Replace(".", ","));
	_isDrawElementsSupported = version > 1.1f || Math::Equals(version, 1.1f);

	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &_maxTextureSize);
}

//===========================================================================//

}//namespace

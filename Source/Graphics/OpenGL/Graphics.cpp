//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Graphics.h"

#include "../../Utils/Console/Console.h"
#include "../VideoCardCapabilities.h"
#include "../Exceptions/GraphicsException.h"

namespace Bru
{

//===========================================================================//

SharedPtr<GraphicsSingleton> Graphics;

//===========================================================================//

const integer GraphicsSingleton::ColorBufferDepth = 32;
const integer GraphicsSingleton::DepthBufferDepth = 16;

const string GraphicsSingleton::Name = "Graphics";

//===========================================================================//

GraphicsSingleton::GraphicsSingleton(const Color & clearColor) :
	_viewport(),
	_deviceContext(),
	_renderingContext(),
	_projectionMatrix(Matrix4x4::Identity),
	_modelMatrix(Matrix4x4::Identity),
	_viewMatrix(Matrix4x4::Identity),
	_clearColor(clearColor),
	_clearDepth(0.0f),
	_matrixMode(-1),
	_depthFunction(DepthFunction::Less),
	_sourceBlendFunction(BlendFunction::SourceAlpha),
	_destinationBlendFunction(BlendFunction::OneMinusSourceAlpha),
	_textureID(Texture::DefaultID),
	_scissorsRectangle(),
	_polygonMode(PolygonMode::Fill),
	_lineThickness(0.0f),
	_isDepthTestEnabled(false),
	_isLightingEnabled(false),
	_isBlendingEnabled(false),
	_is2DTexturingEnabled(false),
	_isScissorsTestEnabled(false),
	_isVerticalSyncEnabled(true)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	_deviceContext = OperatingSystem->CreateDeviceContext(Surface->GetHandle());
	OperatingSystem->SetPixelFormat(_deviceContext, ColorBufferDepth, DepthBufferDepth);
	_renderingContext = OperatingSystem->CreateRenderingContext(_deviceContext);
	OperatingSystem->SetRenderingContextAsCurrent(_deviceContext, _renderingContext);

	VideoCardCapabilities = new VideoCardCapabilitiesSingleton();
	VideoCardCapabilities->OutputVideoCardInfo();

	InitExtensions();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_SCISSOR_TEST);
	glDisable(GL_POINT_SMOOTH);

	SetClearColor(clearColor);

	SetShadeMode(ShadeMode::Flat);
	SetCullMode(CullMode::ClockWise);

	glBlendFunc(static_cast<uint>(_sourceBlendFunction), static_cast<uint>(_destinationBlendFunction));
	glDepthFunc(static_cast<uint>(_depthFunction));

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glDepthMask(GL_TRUE);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (VideoCardCapabilities->IsDrawElementsSupported())
	{
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_VERTEX_ARRAY);
	}

	glGetFloatv(GL_LINE_WIDTH, &_lineThickness);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

GraphicsSingleton::~GraphicsSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	if (VideoCardCapabilities->IsDrawElementsSupported())
	{
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
	}

	OperatingSystem->ClearCurrentRenderingContext();
	OperatingSystem->DestroyRenderingContext(_renderingContext);
	OperatingSystem->DeleteDeviceContext(Surface->GetHandle(), _deviceContext);

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

void GraphicsSingleton::ResizeViewport(integer width, integer height)
{
	glViewport(0, 0, width, height);
	_viewport = IntRectangle(0, 0, width, height);
#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

const IntRectangle & GraphicsSingleton::GetViewport() const
{
	return _viewport;
}

//===========================================================================//

void GraphicsSingleton::LoadProjectionMatrix(const Matrix4x4 & newProjectionMatrix)
{
	if (_matrixMode != GL_PROJECTION)
	{
		glMatrixMode(GL_PROJECTION);
		_matrixMode = GL_PROJECTION;
	}
	_projectionMatrix = newProjectionMatrix;
	glLoadMatrixf(_projectionMatrix);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::LoadModelMatrix(const Matrix4x4 & newModelMatrix)
{
	if (_matrixMode != GL_MODELVIEW)
	{
		glMatrixMode(GL_MODELVIEW);
		_matrixMode = GL_MODELVIEW;
	}

	_modelMatrix = newModelMatrix;
	glLoadMatrixf(_modelMatrix * _viewMatrix);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::LoadViewMatrix(const Matrix4x4 & newViewMatrix)
{
	if (_matrixMode != GL_MODELVIEW)
	{
		glMatrixMode(GL_MODELVIEW);
		_matrixMode = GL_MODELVIEW;
	}

	_viewMatrix = newViewMatrix;
	glLoadMatrixf(_modelMatrix * _viewMatrix);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::SetClearColor(const Color & color)
{
	glClearColor(color.GetRed(), color.GetGreen(), color.GetBlue(), color.GetAlpha());

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

const Color & GraphicsSingleton::GetClearColor() const
{
	return _clearColor;
}

//===========================================================================//

void GraphicsSingleton::SetClearDepth(float depth)
{
	glClearDepth(depth);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

float GraphicsSingleton::GetClearDepth() const
{
	return _clearDepth;
}

//===========================================================================//

void GraphicsSingleton::BeginFrame()
{
	//Производители видеокарт настоятельно рекумендуют очищать буферы каждый кадр
	if (_isScissorsTestEnabled)
	{
		glDisable(GL_SCISSOR_TEST);
	}

	GLbitfield clearFlags = IsDepthTestEnabled() ? GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT : GL_COLOR_BUFFER_BIT;
	glClear(clearFlags);

	if (_isScissorsTestEnabled)
	{
		glEnable(GL_SCISSOR_TEST);
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::EndFrame()
{
	glFlush();
	OperatingSystem->SwapBuffers(_deviceContext);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::DrawElements(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
                                     const Array<uint>::SimpleType & indices)
{
	if (vertices.GetCount() == 0 || indices.GetCount() == 0)
	{
		return;
	}

	//Простая проверка должна быть дешевле виртуального вызова
	if (VideoCardCapabilities->IsDrawElementsSupported())
	{
		glColorPointer(4, GL_FLOAT, sizeof(Vertex), &vertices.GetData()[0].VertexColor);

		if (_is2DTexturingEnabled)
		{
			glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &vertices.GetData()[0].TextureCoord);
		}

		glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &vertices.GetData()[0].Position);

		glDrawElements(static_cast<uint>(verticesMode), indices.GetCount(), GL_UNSIGNED_INT, indices.GetData());
	}
	else
	{
		DrawElementsPerVertex(verticesMode, vertices, indices);
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::DrawElements(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
                                     const Array<uint>::SimpleType & indices, const Matrix4x4 & matrix)
{
	if (vertices.GetCount() == 0 || indices.GetCount() == 0)
	{
		return;
	}
	
	glLoadMatrixf((_viewMatrix * matrix).GetData());
	DrawElements(verticesMode, vertices, indices);
}

//===========================================================================//

void GraphicsSingleton::DrawElements(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
                                     const Array<Vector3>::SimpleType & worldVertices, const Array<uint>::SimpleType & indices)
{
	if (vertices.GetCount() == 0 || indices.GetCount() == 0)
	{
		return;
	}

	//Простая проверка должна быть дешевле виртуального вызова
	if (VideoCardCapabilities->IsDrawElementsSupported())
	{
		glColorPointer(4, GL_FLOAT, sizeof(Vertex), &vertices.GetData()[0].VertexColor);

		if (_is2DTexturingEnabled)
		{
			glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &vertices.GetData()[0].TextureCoord);
		}

		glVertexPointer(3, GL_FLOAT, sizeof(Vector3), worldVertices.GetData());

		glDrawElements(static_cast<uint>(verticesMode), indices.GetCount(), GL_UNSIGNED_INT, indices.GetData());
	}
	else
	{
		DrawElementsPerVertex(verticesMode, vertices, worldVertices, indices);
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::SetShadeMode(const ShadeMode shadeMode)
{
	glShadeModel(static_cast<uint>(shadeMode));

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::SetCullMode(const CullMode cullMode)
{
	if (cullMode == CullMode::None)
	{
		glDisable(GL_CULL_FACE);
		return;
	}

	glEnable(GL_CULL_FACE);
	glCullFace(static_cast<uint>(cullMode));

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::SetDepthFunction(DepthFunction depthFunction)
{
	if (depthFunction != _depthFunction)
	{
		glDepthFunc(static_cast<uint>(depthFunction));
		_depthFunction = depthFunction;
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::SetBlendFunctions(BlendFunction sourceBlendFunction, BlendFunction destinationBlendFunction)
{
	if (sourceBlendFunction != _sourceBlendFunction || destinationBlendFunction != _destinationBlendFunction)
	{
		glBlendFunc(static_cast<uint>(sourceBlendFunction), static_cast<uint>(destinationBlendFunction));
		_sourceBlendFunction = sourceBlendFunction;
		_destinationBlendFunction = destinationBlendFunction;
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::Set2DTexture(integer textureID)
{
	//Надеюсь эта оптимизиация не делает хуже)
	//Проверить на быстродейтсвие
	if (_textureID != textureID)
	{
		glBindTexture(GL_TEXTURE_2D, textureID);
		_textureID = textureID;
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::SetNo2DTexture()
{
	glBindTexture(GL_TEXTURE_2D, 0);
	_textureID = 0;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

integer GraphicsSingleton::GetTextureID() const
{
	return _textureID;
}

//===========================================================================//

const Matrix4x4 & GraphicsSingleton::GetProjectionMatrix() const
{
	return _projectionMatrix;
}

//===========================================================================//

const Matrix4x4 & GraphicsSingleton::GetModelMatrix() const
{
	return _modelMatrix;
}

//===========================================================================//

const Matrix4x4 & GraphicsSingleton::GetViewMatrix() const
{
	return _viewMatrix;
}

//===========================================================================//

void GraphicsSingleton::SetScissorsRectangle(const IntRectangle & scissorsRectangle)
{
	//Надеюсь эта оптимизиация не делает хуже)
	//Проверить на быстродейтсвие
	if (_scissorsRectangle != scissorsRectangle)
	{
		glScissor(scissorsRectangle.X, scissorsRectangle.Y, scissorsRectangle.Width, scissorsRectangle.Height);
		_scissorsRectangle = scissorsRectangle;
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

const IntRectangle & GraphicsSingleton::GetScissorsRectangle() const
{
	return _scissorsRectangle;
}

//===========================================================================//

void GraphicsSingleton::SetPolygonMode(PolygonMode polygonMode)
{
	glPolygonMode(GL_FRONT_AND_BACK, static_cast<uint>(polygonMode));
	_polygonMode = polygonMode;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

PolygonMode GraphicsSingleton::GetPolygonMode() const
{
	return _polygonMode;
}

//===========================================================================//

void GraphicsSingleton::SetLineThickness(float lineThickness)
{
	if (!Math::Equals(_lineThickness, lineThickness))
	{
		_lineThickness = lineThickness;
		glLineWidth(_lineThickness);
		glPointSize(_lineThickness);
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

float GraphicsSingleton::GetLineThickness() const
{
	return _lineThickness;
}

//===========================================================================//

void GraphicsSingleton::EnableDepthTest()
{
	if (_isDepthTestEnabled)
	{
		return;
	}

	glEnable(GL_DEPTH_TEST);
	_isDepthTestEnabled = true;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::DisableDepthTest()
{
	if (!_isDepthTestEnabled)
	{
		return;
	}

	glDisable(GL_DEPTH_TEST);
	_isDepthTestEnabled = false;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

bool GraphicsSingleton::IsDepthTestEnabled() const
{
	return _isDepthTestEnabled;
}

//===========================================================================//

void GraphicsSingleton::EnableLighting()
{
	if (_isLightingEnabled)
	{
		return;
	}

	glEnable(GL_LIGHTING);
	_isLightingEnabled = true;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::DisableLighting()
{
	if (!_isLightingEnabled)
	{
		return;
	}

	glDisable(GL_LIGHTING);
	_isLightingEnabled = false;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

bool GraphicsSingleton::IsLightingEnabled() const
{
	return _isLightingEnabled;
}

//===========================================================================//

void GraphicsSingleton::EnableBlending()
{
	if (_isBlendingEnabled)
	{
		return;
	}

	glEnable(GL_BLEND);
	_isBlendingEnabled = true;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::DisableBlending()
{
	if (!_isBlendingEnabled)
	{
		return;
	}

	glDisable(GL_BLEND);
	_isBlendingEnabled = false;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

bool GraphicsSingleton::IsBlendingEnabled() const
{
	return _isBlendingEnabled;
}

//===========================================================================//

void GraphicsSingleton::Enable2DTexturing()
{
	if (_is2DTexturingEnabled)
	{
		return;
	}

	glEnable(GL_TEXTURE_2D);

	if (VideoCardCapabilities->IsDrawElementsSupported())
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	_is2DTexturingEnabled = true;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::Disable2DTexturing()
{
	if (!_is2DTexturingEnabled)
	{
		return;
	}

	glDisable(GL_TEXTURE_2D);

	if (VideoCardCapabilities->IsDrawElementsSupported())
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	_is2DTexturingEnabled = false;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

bool GraphicsSingleton::Is2DTexturingEnabled() const
{
	return _is2DTexturingEnabled;
}

//===========================================================================//

void GraphicsSingleton::EnableScissorsTest()
{
	if (_isScissorsTestEnabled)
	{
		return;
	}

	glEnable(GL_SCISSOR_TEST);
	_isScissorsTestEnabled = true;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::DisableScissorsTest()
{
	if (!_isScissorsTestEnabled)
	{
		return;
	}

	glDisable(GL_SCISSOR_TEST);
	_isScissorsTestEnabled = false;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

bool GraphicsSingleton::IsScissorsTestEnabled() const
{
	return _isScissorsTestEnabled;
}

//===========================================================================//

void GraphicsSingleton::EnableVerticalSync()
{
	if (!VideoCardCapabilities->IsVerticalSyncSupported() || _isVerticalSyncEnabled)
	{
		return;
	}

	wglSwapIntervalEXT(1);
	_isVerticalSyncEnabled = true;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void GraphicsSingleton::DisableVerticalSync()
{
	if (!VideoCardCapabilities->IsVerticalSyncSupported() || !_isVerticalSyncEnabled)
	{
		return;
	}

	wglSwapIntervalEXT(0);
	_isVerticalSyncEnabled = false;

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

bool GraphicsSingleton::IsVerticalSyncEnabled() const
{
	return _isVerticalSyncEnabled;
}

//===========================================================================//

void GraphicsSingleton::CheckForErrors(const string & fileName, string methodName, integer fileLine)
{
	GLenum error;

	do
	{
		//Возможно вместо эксепшенов стоит писать в консоль
		error = glGetError();
		switch (error)
		{
		case GL_NO_ERROR:
			break;

		case GL_INVALID_VALUE:
			throw GraphicsException("Invalid value", fileName, methodName, fileLine);

		case GL_INVALID_ENUM:
			throw GraphicsException("Invalid enum", fileName, methodName, fileLine);

		case GL_INVALID_OPERATION:
			throw GraphicsException("Invalid operation", fileName, methodName, fileLine);

		case GL_STACK_OVERFLOW:
			throw GraphicsException("Stack overflow", fileName, methodName, fileLine);

		case GL_STACK_UNDERFLOW:
			throw GraphicsException("Stack underflow", fileName, methodName, fileLine);

		case GL_OUT_OF_MEMORY:
			throw GraphicsException("Out of memory", fileName, methodName, fileLine);

		default:
			throw GraphicsException("Unknown error", fileName, methodName, fileLine);
		}
	}
	while (error != GL_NO_ERROR);
}

//===========================================================================//

void GraphicsSingleton::DrawElementsPerVertex(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
                                              const Array<Vector3>::SimpleType & worldVertices, const Array<uint>::SimpleType & indices)
{
	glBegin(static_cast<uint>(verticesMode));

	for (integer i = 0; i < indices.GetCount(); i++)
	{
		integer currentIndex = indices[i];

		glColor4f(vertices[currentIndex].VertexColor.GetRed(), vertices[currentIndex].VertexColor.GetGreen(),
		          vertices[currentIndex].VertexColor.GetBlue(), vertices[currentIndex].VertexColor.GetAlpha());

		if (_is2DTexturingEnabled)
		{
			glTexCoord2f(vertices[currentIndex].TextureCoord.X, vertices[currentIndex].TextureCoord.Y);
		}

		glVertex3f(worldVertices[currentIndex].X, worldVertices[currentIndex].Y,
		           worldVertices[currentIndex].Z);
	}

	glEnd();
}

//===========================================================================//

void GraphicsSingleton::DrawElementsPerVertex(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
                                              const Array<uint>::SimpleType & indices)
{
	glBegin(static_cast<uint>(verticesMode));

	for (integer i = 0; i < indices.GetCount(); i++)
	{
		integer currentIndex = indices[i];

		glColor4f(vertices[currentIndex].VertexColor.GetRed(), vertices[currentIndex].VertexColor.GetGreen(),
		          vertices[currentIndex].VertexColor.GetBlue(), vertices[currentIndex].VertexColor.GetAlpha());

		if (_is2DTexturingEnabled)
		{
			glTexCoord2f(vertices[currentIndex].TextureCoord.X, vertices[currentIndex].TextureCoord.Y);
		}

		glVertex3f(vertices[currentIndex].Position.X, vertices[currentIndex].Position.Y,
		           vertices[currentIndex].Position.Z);
	}

	glEnd();
}

//===========================================================================//

void GraphicsSingleton::InitExtensions()
{
	wglSwapIntervalEXT = (wglSwapIntervalEXTPROC) OperatingSystem->GetFuctionAddress("wglSwapIntervalEXT");

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

} //namespace

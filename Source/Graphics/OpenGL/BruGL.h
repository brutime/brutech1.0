//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: описание gl функций и констант
//

#pragma once
#ifndef BRU_GL_H
#define BRU_GL_H

#include "../../../Libs/OpenGL/GL/gl.h"
#include "../../../Libs/OpenGL/GL/glu.h"

//===========================================================================//

#define GL_CLAMP_TO_EDGE 0x812F

#define GL_BGR 0x80E0
#define GL_BGRA 0x80E1

//===========================================================================//

typedef GLboolean (APIENTRY * wglSwapIntervalEXTPROC)(int interval);

//===========================================================================//

extern wglSwapIntervalEXTPROC wglSwapIntervalEXT;

//===========================================================================//

#endif // BRU_GL_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Vertex.h"

namespace Bru
{

//===========================================================================//

Vertex::Vertex() :
	Position(),
	TextureCoord(),
	VertexColor()
{
}

//===========================================================================//

Vertex::Vertex(const Vector3 & position, const Vector2 & textureCoord, const Color & vertexColor) :
	Position(position),
	TextureCoord(textureCoord),
	VertexColor(vertexColor)
{
}

//===========================================================================//

Vertex::Vertex(const Vertex & other) :
	Position(other.Position),
	TextureCoord(other.TextureCoord),
	VertexColor(other.VertexColor)
{
}

//===========================================================================//

Vertex::Vertex(Vertex && other) :
	Position(std::move(other.Position)),
	TextureCoord(std::move(other.TextureCoord)),
	VertexColor(std::move(other.VertexColor))
{
	other.Position.Clear();
	other.TextureCoord.Clear();
	other.VertexColor.Clear();
}

//===========================================================================//

Vertex::~Vertex()
{
}

//===========================================================================//

Vertex & Vertex::operator =(const Vertex & other)
{
	if (this == &other)
	{
		return *this;
	}

	Position = other.Position;
	TextureCoord = other.TextureCoord;
	VertexColor = other.VertexColor;

	return *this;
}

//===========================================================================//

Vertex & Vertex::operator =(Vertex && other)
{
	if (this == &other)
	{
		return *this;
	}

	std::swap(Position, other.Position);
	std::swap(TextureCoord, other.TextureCoord);
	std::swap(VertexColor, other.VertexColor);

	return *this;	
}

//===========================================================================//

}//namespace

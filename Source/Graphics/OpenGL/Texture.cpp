//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "../Texture.h"

#include "../../Math/Math.h"
#include "../../Utils/Log/Log.h"
#include "../Graphics.h"
#include "../VideoCardCapabilities.h"

namespace Bru
{

//===========================================================================//

void Texture::Initialize(Image & image)
{
	integer sourceWidth = image.GetWidth();
	integer sourceHeight = image.GetHeight();

	CheckSize(sourceWidth, sourceHeight);

	//Если размеры image не кратны 2-ум - создаем
	if (!Math::IsPowerOfTwo(image.GetWidth()) || !Math::IsPowerOfTwo(image.GetHeight()))
	{
		Image::ExpandDataForPowerOfTwo(image);
	}

	_width = image.GetWidth();
	_height = image.GetHeight();
	_pixelFormat = image.GetPixelFormat();

	_widthFactor = sourceWidth / static_cast<float>(image.GetWidth());
	_heightFactor = sourceHeight / static_cast<float>(image.GetHeight());

	//Хорошо бы обработать случай, когда нельзя выделить более текстур
	glGenTextures(1, reinterpret_cast<GLuint *>(&_id));

	Graphics->Set2DTexture(_id);
	Upload(image);
	InitializeColorBlending();
	InitializeSmooth();
	InitializeBorderType();
	Graphics->SetNo2DTexture();
}

//===========================================================================//

void Texture::InitializeColorBlending()
{
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void Texture::InitializeSmooth()
{
	if (_isSmooth)
	{
		if (_isMipMap)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
	else
	{
		if (_isMipMap)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		}
		else
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void Texture::InitializeBorderType()
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, static_cast<uint>(_borderType));
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, static_cast<uint>(_borderType));

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void Texture::Upload(const Image & image)
{
	if (_isMipMap)
	{
		gluBuild2DMipmaps(GL_TEXTURE_2D, image.GetPixelSize(), image.GetWidth(), image.GetHeight(),
		                  static_cast<uint>(image.GetPixelFormat()), GL_UNSIGNED_BYTE, image.GetData().GetData());

		//Проверить, возможно ATI строить мип мапы всегда-всегда
		if (VideoCardCapabilities->GetVendor() == VideoCardVendor::ATI)
		{
			ATIBuildMipMaps();
		}
	}
	else
	{
		glTexImage2D(GL_TEXTURE_2D, 0, image.GetPixelSize(), image.GetWidth(), image.GetHeight(), 0,
		             static_cast<uint>(image.GetPixelFormat()), GL_UNSIGNED_BYTE, image.GetData().GetData());
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

//Забавная история с ATI - их карточки строят MipMap для текстур при первом вызове текстуры
//Данный метод - это дурацкое решение проблемы конечно, но работает
//Как только будет найдено лучшее решение - следует незамедлительно его применить
void Texture::ATIBuildMipMaps()
{
	glEnable(GL_TEXTURE_2D);

	glBegin(GL_TRIANGLE_STRIP);
	{
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(0.0f, 0.0f);

		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(1.0f, 0.0f);

		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(0.0f, 1.0f);

		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(1.0f, 1.0f);
	}
	glEnd();

	glDisable(GL_TEXTURE_2D);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void Texture::FreeTexture()
{
	if (_id != DefaultID)
	{
		if (Graphics->GetTextureID() == _id)
		{
			Graphics->SetNo2DTexture();
		}
		glDeleteTextures(1, reinterpret_cast<GLuint *>(&_id));
		_id = DefaultID;
	}

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

void Texture::DeepCopy(const Texture & other)
{
	FreeTexture();

	_name = other._name + CopyedTextureNamePostfix;
	_isMipMap = other._isMipMap;
	_isSmooth = other._isSmooth;
	_borderType = other._borderType;

	integer pixelSize = GraphicsHelper::GetBytesPerPixel(other._pixelFormat);
	Array<byte>::SimpleType otherData(other._width * other._height * pixelSize);

	Graphics->Set2DTexture(other._id);
	glGetTexImage(GL_TEXTURE_2D, 0, static_cast<uint>(other._pixelFormat), GL_UNSIGNED_BYTE, otherData.GetData());
	Graphics->SetNo2DTexture();

	Image image(other._name, other._width, other._height, other._pixelFormat, otherData);
	Initialize(image);

#ifdef DEBUGGING_GRAPHICS
	CHECK_FOR_ERRORS();
#endif
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: структура вершины
//

#pragma once
#ifndef VERTEX_H
#define VERTEX_H

#include "../../Math/Math.h"

namespace Bru
{

//===========================================================================//

struct Vertex
{
	Vertex();
	Vertex(const Vector3 & position, const Vector2 & textureCoord, const Color & vertexColor);
	Vertex(const Vertex & other);
	Vertex(Vertex && other);
	~Vertex();

	Vertex & operator =(const Vertex & other);
	Vertex & operator =(Vertex && other);

	Vector3 Position;
	Vector2 TextureCoord;
	Color VertexColor;
};

//===========================================================================//

}//namespace

#endif // VERTEX_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Глиф
//

#pragma once
#ifndef GLYPH_H
#define GLYPH_H

#include "../../Common/Common.h"
#include "../Mesh.h"

namespace Bru
{

//===========================================================================//

struct Glyph
{
	Glyph();

	float X;
	float Y;
	float BitmapWidth;
	float BitmapHeight;

	float Width;
	float Height;

	float AdvanceX; // расстояние до след.символа

	float BearingX; // гориз. расстояние от начала до левого края символа
	float BearingY; // верт. расстояние от базовой линии до верхнего края символа

	SharedPtr<Mesh> GlyphMesh;
};

//===========================================================================//

}// namespace

#endif // GLYPH_H

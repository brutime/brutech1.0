//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Font.h"

#include "../../Math/Math.h"
#include "../../FileSystem/FileSystem.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/Log/Log.h"
#include "../../Utils/PathHelper/PathHelper.h"
#include "../../OperatingSystem/OSCapabilities.h"
#include "../../Graphics/TextureManager/TextureManager.h"

namespace Bru
{

//===========================================================================//

const integer Font::GlyphBorder = 2;
const utf8_char Font::BadChar = "?";

//===========================================================================//

Font::Font(const string & pathToFile, integer size, FontType type, const string & alphabet, const FT_Library & library) :
	_face(),
	_lastError(),
	_name(pathToFile),
	_size(size),
	_type(type),
	_hasKerning(false),
	_lineDistance(0),
	_height(0),
	_baseLineAscender(0),
	_baseLineDescender(0),
	_boundingBoxWidth(0),
	_alphabetGlyphs(),
	_kerningCache(),
	_texture(),
	_textMeshesCache(),
	_isFaceInitialized(false)
{
	InitializeFace(pathToFile, library);
	SetSize(_size);

	BuildFontData(alphabet);

	_hasKerning = FT_HAS_KERNING(_face);
	_lineDistance = _face->size->metrics.height / 64.0f;
	_baseLineDescender = _face->size->metrics.descender / 64.0f;
	_baseLineAscender = _face->size->metrics.ascender / 64.0f;
	_height = _baseLineAscender - _baseLineDescender;
	_boundingBoxWidth = _face->size->metrics.max_advance / 64.0f;

	Console->Notice(string::Format("Font loaded: {0}", ToString()));
}

//===========================================================================//

Font::~Font()
{
	if (_isFaceInitialized)
	{
		FT_Done_Face(_face);
		_isFaceInitialized = false;
	}

	Console->Notice(string::Format("Font destroyed: {0}", ToString()));
}

//===========================================================================//

const string & Font::GetName() const
{
	return _name;
}

//===========================================================================//

integer Font::GetSize() const
{
	return _size;
}

//===========================================================================//

FontType Font::GetType() const
{
	return _type;
}

//===========================================================================//

SharedPtr<Glyph> Font::GetGlyph(const utf8_char & someChar)
{
	if (_alphabetGlyphs.Contains(someChar))
	{
		return _alphabetGlyphs[someChar];
	}

	Console->Warning("Couldn't find glyph for some char");
	//Чтобы не вызывать рекурсии в консоли - дописываем символ только в лог
	Log->WriteMessage(MessagePriority::Warning, string::Format("Char: \"{0}\"", someChar));
	return _alphabetGlyphs[BadChar];
}

//===========================================================================//

float Font::GetKerning(utf8_char firstChar, utf8_char secondChar)
{
	if (!_hasKerning || firstChar == NullTerminator)
	{
		return 0.0f;
	}

	ValuePair<utf8_char, utf8_char> charsPair(firstChar, secondChar);

	if (_kerningCache.Contains(charsPair))
	{
		return _kerningCache[charsPair];
	}

	FT_UInt firstIndex = FT_Get_Char_Index(_face, firstChar.ToUTF32());
	FT_UInt secondIndex = FT_Get_Char_Index(_face, secondChar.ToUTF32());

	FT_Vector kerningVector;
	_lastError = FT_Get_Kerning(_face, firstIndex, secondIndex, FT_KERNING_DEFAULT, & kerningVector);
	if (_lastError)
	{
		Console->Warning(string::Format("Font::GetKerning(): can't load kerning information for "
		                                "pair of chars \"{0}\"-\"{1}\". Font name \"{2}\"", _name, firstChar, secondChar));
		return 0.0f;
	}

	float kerning = kerningVector.x / 64.f;
	_kerningCache.Add(charsPair, kerning);
	return kerning;
}

//===========================================================================//

float Font::GetLineDistance() const
{
	return _lineDistance;
}

//===========================================================================//

float Font::GetHeight() const
{
	return _height;
}

//===========================================================================//

float Font::GetBaseLineAscender() const
{
	return _baseLineAscender;
}

//===========================================================================//

float Font::GetBaseLineDescender() const
{
	return _baseLineDescender;
}

//===========================================================================//

float Font::GetBoundingBoxWidth() const
{
	return _boundingBoxWidth;
}

//===========================================================================//

const WeakPtr<Texture> & Font::GetTexture()
{
	return _texture;
}

//===========================================================================//

void Font::PutToTextMeshesCache(const ValuePair<string, WordWrapMode> & key, const SharedPtr<Mesh> & mesh, const List<ValuePair<float, integer>> & stringWidths)
{
	_textMeshesCache.Add(key, ValuePair<SharedPtr<Mesh>, List<ValuePair<float, integer>>>(new Mesh(*mesh), stringWidths));
}

//===========================================================================//

const ValuePair<SharedPtr<Mesh>, List<ValuePair<float, integer>>> & Font::GetFromTextMeshesCache(const ValuePair<string, WordWrapMode> & key)
{
	return _textMeshesCache[key];
}

//===========================================================================//

bool Font::ContainsInTextMeshesCache(const ValuePair<string, WordWrapMode> & key)
{
	return _textMeshesCache.Contains(key);
}

//===========================================================================//

string Font::ConvertTypeToString(FontType someType)
{
	switch (someType)
	{
	case FontType::Monochrome: return "Monochrome";
	case FontType::AntiAliased: return "Antialiased";
	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined font type");
	}
}

//===========================================================================//

string Font::ToString() const
{
	return string::Format("\"{0}\" ({1}, {2})", _name, _size, Font::ConvertTypeToString(_type));
}

//===========================================================================//

void Font::InitializeFace(const string & pathToFile, const FT_Library & library)
{
	string fullPathToFile = PathHelper::GetFullPathToFontFile(pathToFile);

	_lastError = FT_New_Face(library, fullPathToFile.ToChars(), 0, & _face);
	if (_lastError == FT_Err_Unknown_File_Format)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Unknown file format. Path to file: \"{0}\"", pathToFile));
	}
	else if (_lastError)
	{
		if (!FileSystem::FileExists(pathToFile))
		{
			FILE_NOT_FOUND_EXCEPTION(string::Format("File not found: \"{0}\"", pathToFile));
		}
		else
		{
			IO_EXCEPTION(string::Format("Couldn't read file: \"{0}\"", pathToFile));
		}
	}

	_isFaceInitialized = true;
}

//===========================================================================//

void Font::SetSize(integer points)
{
	_lastError = FT_Set_Char_Size(_face, 0, points * 64, OSCapabilities::GetXDPI(), OSCapabilities::GetYDPI());
	if (_lastError)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Invalid points value {0}", points));
	}
}

//===========================================================================//

void Font::BuildFontData(const string & alphabet)
{
	integer glyphMaxBitmapWidth = 0;
	integer glyphMaxBitmapHeight = 0;

	TreeMap<SharedPtr<Image>, SharedPtr<Glyph>> glyphsImages;

	utf32_string utf32Alphabet = alphabet.ToUTF32();

	for (integer i = 0, count = utf32Alphabet.GetLength(); i < count; i++)
	{
		FT_GlyphSlot slot;
		try
		{
			slot = GetSlot(utf32Alphabet[i]);
		}
		catch (InvalidArgumentException &)
		{
			slot = GetSlot(BadChar.ToUTF32());
		}

		SharedPtr<Image> glyphImage = CreateGlyphImage(slot);
		SharedPtr<Glyph> glyph = CreateGlyph(glyphImage->GetWidth(), glyphImage->GetHeight(), slot);

		_alphabetGlyphs.Add(alphabet[i], glyph);
		glyphsImages.Add(glyphImage, glyph);

		if (glyph->BitmapWidth > glyphMaxBitmapWidth)
		{
			glyphMaxBitmapWidth = glyph->BitmapWidth;
		}

		if (glyph->BitmapHeight > glyphMaxBitmapHeight)
		{
			glyphMaxBitmapHeight = glyph->BitmapHeight;
		}
	}

	SharedPtr<Image> fontImage = GetFontEmptyImage(glyphMaxBitmapWidth, glyphMaxBitmapHeight, utf32Alphabet.GetLength());

	integer imageX = 0;
	integer imageY = 0;

	for (auto & glyphImagePair : glyphsImages)
	{
		if (imageX + glyphMaxBitmapWidth + GlyphBorder > fontImage->GetWidth())
		{
			imageY += glyphMaxBitmapHeight + GlyphBorder;
			imageX = 0;
		}

		fontImage->SubData(imageX + GlyphBorder, imageY + GlyphBorder, *glyphImagePair.GetKey());

		glyphImagePair.GetValue()->X = imageX + GlyphBorder;
		glyphImagePair.GetValue()->Y = imageY + GlyphBorder;

		imageX += glyphMaxBitmapWidth + GlyphBorder;
	}

	_texture = TextureManager->Create(GetFullName(), *fontImage, false, false, TextureBorderType::ClampToEdge);

	const Vector2 textureCoordsScale(1.0f / _texture->GetWidth(), 1.0f / _texture->GetHeight());
	CreateGlyphsMeshes(textureCoordsScale);
}

//===========================================================================//

FT_GlyphSlot Font::GetSlot(utf32_char someChar)
{
	_lastError = FT_Load_Char(_face, someChar, static_cast<integer>(_type));
	if (_lastError != 0)
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Can't load char {0}", someChar));
	}

	return _face->glyph;
}

//===========================================================================//

SharedPtr<Image> Font::CreateGlyphImage(const FT_GlyphSlot & slot)
{
	FT_Pixel_Mode pm = static_cast<FT_Pixel_Mode>(slot->bitmap.pixel_mode);

	Array<byte>::SimpleType glyphImageData;

	integer dataSize = slot->bitmap.width * slot->bitmap.rows * 2;
	glyphImageData.Resize(dataSize);

	if (pm == FT_PIXEL_MODE_MONO)
	{
		GetMonochromeGlyphImageData(glyphImageData, slot);
	}
	else
	{
		GetGrayscaleGlyphImageData(glyphImageData, slot);
	}

	string imageName = _name + "Glyph";
	SharedPtr<Image> charImage = new Image(imageName, slot->bitmap.width, slot->bitmap.rows,
	                                       PixelFormat::MonochromeAlpha, glyphImageData);
	charImage->FlipHorizontal();
	return charImage;
}

//===========================================================================//

void Font::GetMonochromeGlyphImageData(Array<byte>::SimpleType & glyphImageData, const FT_GlyphSlot & slot)
{
	byte * startBytePtr = slot->bitmap.buffer;
	byte * currentBytePtr = startBytePtr;
	byte shift = 7;

	integer bitCount = 0;

	for (integer i = 0, count = glyphImageData.GetCount(); i < count; i += 2)
	{
		glyphImageData[i] = 255;

		if (*currentBytePtr & (1 << shift))
		{
			glyphImageData[i + 1] = 255;
		}
		else
		{
			glyphImageData[i + 1] = 0;
		}

		bitCount++;

		if (bitCount % slot->bitmap.width == 0)
		{
			startBytePtr += slot->bitmap.pitch;
			currentBytePtr = startBytePtr;
			shift = 7;
		}
		else if (shift == 0)
		{
			currentBytePtr++;
			shift = 7;
		}
		else
		{
			shift--;
		}
	}
}

//===========================================================================//

void Font::GetGrayscaleGlyphImageData(Array<byte>::SimpleType & glyphImageData, const FT_GlyphSlot & slot)
{
	byte * srcPtr = slot->bitmap.buffer;
	for (integer j = 0; j < slot->bitmap.rows; j++)
	{
		for (integer i = 0; i < slot->bitmap.width; i++)
		{
			glyphImageData[(j * slot->bitmap.width + i) * 2] = 255;
			glyphImageData[(j * slot->bitmap.width + i) * 2 + 1] = srcPtr[i];
		}
		srcPtr += slot->bitmap.pitch;
	}
}

//===========================================================================//

SharedPtr<Glyph> Font::CreateGlyph(integer glyphBitmapWidth, integer glyphBitmapHeight, const FT_GlyphSlot & slot)
{
	SharedPtr<Glyph> glyph = new Glyph();

	glyph->BitmapWidth = glyphBitmapWidth;
	glyph->BitmapHeight = glyphBitmapHeight;

	glyph->Width = slot->metrics.width / 64.f;
	glyph->Height = slot->metrics.height / 64.f;

	glyph->AdvanceX = slot->metrics.horiAdvance / 64.f;

	glyph->BearingX = slot->metrics.horiBearingX / 64.f;
	glyph->BearingY = slot->metrics.horiBearingY / 64.f;

	return glyph;
}

//===========================================================================//

void Font::CreateGlyphsMeshes(const Vector2 & textureCoordsFactor)
{
	//ScopedPtr<TreeMapIterator<utf8_char, SharedPtr<Glyph> > > glyphIt(_alphabetGlyphs.CreateIterator());
	//for (glyphIt->First(); glyphIt->HasNext(); glyphIt->Next())
	for (auto & alphabetGlyphPair : _alphabetGlyphs)
	{
		const SharedPtr<Glyph> & glyph = alphabetGlyphPair.GetValue();

		float x0 = glyph->BearingX;
		float y0 = glyph->BearingY - glyph->Height;
		float x1 = x0 + glyph->Width;
		float y1 = y0 + glyph->Height;

		float u0 = glyph->X * textureCoordsFactor.X;
		float v0 = glyph->Y * textureCoordsFactor.Y;
		float u1 = (glyph->X + glyph->BitmapWidth) * textureCoordsFactor.X;
		float v1 = (glyph->Y + glyph->BitmapHeight) * textureCoordsFactor.Y;

		SharedPtr<Mesh> mesh = new Mesh(VerticesMode::Triangles, 4, 6);

		mesh->SetVertex(0, {x0, y0, 0.0f}, {u0, v0}, Colors::White);
		mesh->SetVertex(1, {x1, y0, 0.0f}, {u1, v0}, Colors::White);
		mesh->SetVertex(2, {x1, y1, 0.0f}, {u1, v1}, Colors::White);
		mesh->SetVertex(3, {x0, y1, 0.0f}, {u0, v1}, Colors::White);

		mesh->SetIndex(0, 0);
		mesh->SetIndex(1, 1);
		mesh->SetIndex(2, 2);
		mesh->SetIndex(3, 2);
		mesh->SetIndex(4, 3);
		mesh->SetIndex(5, 0);

		glyph->GlyphMesh = mesh;
	}
}

//===========================================================================//

SharedPtr<Image> Font::GetFontEmptyImage(integer glyphMaxBitmapWidth, integer glyphMaxBitmapHeight,
                                         integer alphabetLength) const
{
	integer dataSize = (glyphMaxBitmapWidth + GlyphBorder) * (glyphMaxBitmapHeight + GlyphBorder) * alphabetLength;
	integer sideSize = Math::Sqrt(dataSize);

	//Из-за того что алгоритм примерный, запасаемся по одному символу по краям
	sideSize += Math::Max(glyphMaxBitmapWidth + GlyphBorder, glyphMaxBitmapHeight + GlyphBorder);

	integer powerOfTwoSideSize = Math::GetNearestPowerOfTwo(sideSize);

	integer finalWidth = powerOfTwoSideSize;
	integer finalHeight;
	if (Math::Sqr(sideSize) < Math::Sqr(powerOfTwoSideSize) / 2)
	{
		finalHeight = powerOfTwoSideSize / 2;
	}
	else
	{
		finalHeight = powerOfTwoSideSize;
	}

	Array<byte>::SimpleType emptyData(finalWidth * finalHeight * 2);
	for (integer i = 0, count = emptyData.GetCount(); i < count; i += 2)
	{
		emptyData[i] = 255;
		emptyData[i + 1] = 0;
	}

	return new Image(GetFullName(), finalWidth, finalHeight, PixelFormat::MonochromeAlpha, emptyData);
}

//===========================================================================//

string Font::GetFullName() const
{
	return _name + Converter::ToString(_size) + ConvertTypeToString(_type);
}

//===========================================================================//

} //namespace

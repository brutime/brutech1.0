//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Выравнивание текста по горизонтали
//

#pragma once
#ifndef TEXT_HORIZONTAL_ALIGNMENT_H
#define TEXT_HORIZONTAL_ALIGNMENT_H

namespace Bru
{

//===========================================================================//

enum class TextHorizontalAlignment
{
    Left,
    Center,
    Right,
    Count
};

//===========================================================================//

}//namespace

#endif // TEXT_HORIZONTAL_ALIGNMENT_H

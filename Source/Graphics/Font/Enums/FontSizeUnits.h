//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Единицы измерения размера шрифта
//

#pragma once
#ifndef FONT_SIZE_UNITS_H
#define FONT_SIZE_UNITS_H

namespace Bru
{

//===========================================================================//

enum class FontSizeUnits
{
    Points,
    Pixels
};

//===========================================================================//

}//namespace

#endif // FONT_SIZE_UNITS_H

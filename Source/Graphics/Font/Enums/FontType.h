//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Способ вывода шрифта
//

#pragma once
#ifndef FONT_TYPE_H
#define FONT_TYPE_H

#include <ft2build.h>
#include FT_FREETYPE_H

namespace Bru
{

//===========================================================================//

enum class FontType
{
    Monochrome = FT_LOAD_RENDER | FT_LOAD_MONOCHROME | FT_LOAD_TARGET_MONO,
    AntiAliased = FT_LOAD_RENDER
};

//===========================================================================//

}//namespace Bru

#endif // FONT_TYPE_H

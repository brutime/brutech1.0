//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Режим переноса текста
//

#pragma once
#ifndef WORD_WRAP_H
#define WORD_WRAP_H

namespace Bru
{

//===========================================================================//

enum class WordWrapMode
{
    /// Текст выводится как есть без переноса. Символ \n вызывает перенос на след. строку
    Simple,
    /// Текст выводится в одну строку. Символы \n игнорируются
    SingleLine,
    /// Строка текста может быть разорвана для переноса на любом символе, в том числе в середине слова
    Any,
    /// Строка текста разрывается на ближайшей границе слов до границы области. Знак препинания, следующий за словом, не переносится.
    Smart
};

//===========================================================================//

}//namespace

#endif // WORD_WRAP_H

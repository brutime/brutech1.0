//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Шрифт - умеет загружать себя из файла, строить текстуру, и расчитывать кернинг.
//              Хранит тип, размер, положение глифов
//

#pragma once
#ifndef FONT_H
#define FONT_H

#include "../../Common/Common.h"
#include "../Texture.h"
#include "Enums/FontType.h"
#include "Enums/WordWrapMode.h"
#include "Glyph.h"

#include <ft2build.h>
#include FT_FREETYPE_H

namespace Bru
{

//===========================================================================//

class Font
{
	friend class FontManagerSingleton;

public:
	virtual ~Font();

	const string & GetName() const;
	integer GetSize() const;
	FontType GetType() const;

	SharedPtr<Glyph> GetGlyph(const utf8_char & someChar);
	// Возвращает значение в пикселях
	float GetKerning(utf8_char firstChar, utf8_char secondChar);
	float GetLineDistance() const;
	float GetHeight() const;
	float GetBaseLineAscender() const;
	float GetBaseLineDescender() const;
	float GetBoundingBoxWidth() const;
	
	const WeakPtr<Texture> & GetTexture();
	
	void PutToTextMeshesCache(const ValuePair<string, WordWrapMode> & key, const SharedPtr<Mesh> & mesh, const List<ValuePair<float, integer>> & stringWidths);
	const ValuePair<SharedPtr<Mesh>, List<ValuePair<float, integer>>> & GetFromTextMeshesCache(const ValuePair<string, WordWrapMode> & key);
	bool ContainsInTextMeshesCache(const ValuePair<string, WordWrapMode> & key);

	static string ConvertTypeToString(FontType someType);

	string ToString() const;

protected:
	static const integer GlyphBorder;
	static const utf8_char BadChar;

	Font(const string & pathToFile, integer size, FontType type, const string & alphabet, const FT_Library & library);

	void InitializeFace(const string & pathToFile, const FT_Library & library);
	void SetSize(integer points);

	void BuildFontData(const string & alphabet);

	FT_GlyphSlot GetSlot(utf32_char someChar);

	SharedPtr<Image> CreateGlyphImage(const FT_GlyphSlot & slot);
	void GetMonochromeGlyphImageData(Array<byte>::SimpleType & glyphImageData, const FT_GlyphSlot & slot);
	void GetGrayscaleGlyphImageData(Array<byte>::SimpleType & glyphImageData, const FT_GlyphSlot & slot);
	SharedPtr<Glyph> CreateGlyph(integer glyphBitmapWidth, integer glyphBitmapHeight, const FT_GlyphSlot & slot);
	void CreateGlyphsMeshes(const Vector2 & textureCoordsFactor);

	SharedPtr<Image> GetFontEmptyImage(integer glyphMaxBitmapWidth, integer glyphMaxBitmapHeight,
	                                   integer alphabetLength) const;

	string GetFullName() const;

	FT_Face _face;
	FT_Error _lastError;

	string _name;
	integer _size;
	FontType _type;
	bool _hasKerning;
	float _lineDistance; // В пикселях
	float _height; // В пикселях
	float _baseLineAscender; // В пикселях
	float _baseLineDescender; // В пикселях
	float _boundingBoxWidth; // В пикселях

	TreeMap<utf8_char, SharedPtr<Glyph>> _alphabetGlyphs;
	TreeMap<ValuePair<utf8_char, utf8_char>, float> _kerningCache;

	WeakPtr<Texture> _texture;
	
	//Может не самое подходящее место для хранения, но все же :)
	TreeMap<ValuePair<string, WordWrapMode>, ValuePair<SharedPtr<Mesh>, List<ValuePair<float, integer>>>> _textMeshesCache;

	bool _isFaceInitialized;

	Font(const Font &) = delete;
	Font & operator =(const Font &) = delete;
};

//===========================================================================//

}// namespace

#endif // FONT_H

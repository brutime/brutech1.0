//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Структура для хранения данных о построенной надписи
//

#ifndef NOTICE_INFO_H
#define NOTICE_INFO_H

#include "../../Graphics/Mesh.h"

namespace Bru
{

//===========================================================================//

struct NoticeInfo
{
	NoticeInfo(const SharedPtr<Mesh> & noticeMesh, integer linesCount);

	SharedPtr<Mesh> NoticeMesh;
	integer LinesCount;
};

//===========================================================================//

}
#endif // NOTICE_INFO_H

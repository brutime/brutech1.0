//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "NoticeBuilder.h"

#include "../Graphics.h"
#include "../../Utils/Log/Log.h"

namespace Bru
{

//===========================================================================//

NoticeInfo NoticeBuilder::GetNoticeMesh(const string & text, const WeakPtr<Font> & font, const Color & color,
                                        Vector2 & textSize, TextHorizontalAlignment horizontalAlignment, TextVerticalAlignment verticalAlignment,
                                        WordWrapMode wrapMode)
{
	List<ValuePair<float, integer>> stringWidths;

	SharedPtr<Mesh> resultMesh;
	ValuePair<string, WordWrapMode> key(text, wrapMode);
	if (font->ContainsInTextMeshesCache(key))
	{
		auto cachedData = font->GetFromTextMeshesCache(key);
		resultMesh = new Mesh(*cachedData.First);
		stringWidths = cachedData.Second;
	}
	else
	{
		resultMesh = BuildTextMesh(text, textSize, font, wrapMode, stringWidths);
		font->PutToTextMeshesCache(key, resultMesh, stringWidths);
	}
	
	resultMesh->SetVerticesColor(color);
	CorrectMeshAlignment(resultMesh, font, stringWidths, horizontalAlignment, verticalAlignment);

	float textMaxWidth = 0.0f;
	for (const auto & pair : stringWidths)
	{
		if (pair.First > textMaxWidth)
		{
			textMaxWidth = pair.First;
		}
	}

	textSize.X = textMaxWidth;
	textSize.Y = stringWidths.GetCount() * font->GetLineDistance();

	return NoticeInfo(resultMesh, stringWidths.GetCount());
}

//===========================================================================//

SharedPtr<Mesh> NoticeBuilder::BuildTextMesh(const string & text, Vector2 & textSize, const WeakPtr<Font> & font,
                                             WordWrapMode wrapMode, List<ValuePair<float, integer>> & stringWidths)
{
	SharedPtr<Mesh> resultMesh = new Mesh(VerticesMode::Triangles);
	Vector2 position;
	utf8_char previousChar = 0;

	float currentLineWidth = 0.0f;
	float potentialLineBreakPosition = 0.0f;
	float spaceWidth = 0.0f;

	integer stringGlyphCount = 0;
	integer potentialLineBreakSymbolIndexInCurrentString = 0;
	integer potentialLineBreakMeshIndex = 0;
	integer meshIndex = 0;

	bool ignoreBoundingRect = textSize == Vector2(0.0f, 0.0f);

	for (integer i = 0; i < text.GetLength(); i++)
	{
		SharedPtr<Glyph> glyph;
		utf8_char currentChar = text[i];
		float kerningCorrection = font->GetKerning(previousChar, currentChar);

		if (wrapMode == WordWrapMode::SingleLine)
		{
			glyph = font->GetGlyph(currentChar);
			if (currentChar == " ")
			{
				spaceWidth = glyph->AdvanceX;
				position.X += spaceWidth;
				previousChar = 0;
				continue;
			}
			else if (currentChar == "\n")
			{
				previousChar = 0;
				continue;
			}
		}
		else
		{
			if (currentChar == "\n")
			{
				stringWidths.Add(ValuePair<float, integer>(currentLineWidth, stringGlyphCount));
				position.X = 0;
				position.Y -= font->GetLineDistance();
				previousChar = 0;
				stringGlyphCount = 0;
				currentLineWidth = 0;

				continue;
			}
			else
			{
				glyph = font->GetGlyph(currentChar);
				if (currentChar == " ") // (добавить символы пунктуации)
				{
					spaceWidth = glyph->AdvanceX;
					potentialLineBreakPosition = currentLineWidth + spaceWidth;
					potentialLineBreakSymbolIndexInCurrentString = stringGlyphCount;
					potentialLineBreakMeshIndex = meshIndex;
					position.X += spaceWidth;
					previousChar = 0;
					continue;
				}
			}

			if (!ignoreBoundingRect && ((position.X + kerningCorrection + glyph->BearingX + glyph->Width) > textSize.X))
			{
				if (wrapMode == WordWrapMode::Any)
				{
					stringWidths.Add(ValuePair<float, integer>(currentLineWidth, stringGlyphCount));
					position.X = 0;
					position.Y -= font->GetLineDistance();
					currentLineWidth = 0;
					stringGlyphCount = 0;
					previousChar = 0;
				}
				else if (wrapMode == WordWrapMode::Smart)
				{
					if (stringGlyphCount == 0)
					{
						position.X = 0.0f; //переноса нет, т.к. в строке еще нет ни одного символа(возможно, только пробел). нужно расширить boundingRect
					}
					else
					{
						if (potentialLineBreakPosition > 0.0f) //currentLineWidth * StringFillFactorForSmartWordWrap)
						{
							//перенос всех символов, начиная с последнего пробела, на след. строку
							Vector3 toNewLineTranslation(-potentialLineBreakPosition, -font->GetLineDistance(), 0.0f);
							for (integer j = potentialLineBreakMeshIndex; j < meshIndex; ++j)
							{
								resultMesh->TranslateVertexPosition(j, toNewLineTranslation);
							}

							stringWidths.Add(ValuePair<float, integer>(potentialLineBreakPosition - spaceWidth,
							                                           potentialLineBreakSymbolIndexInCurrentString));
							currentLineWidth -= potentialLineBreakPosition;
							stringGlyphCount -= potentialLineBreakSymbolIndexInCurrentString;
							position.X = currentLineWidth;
							position.Y -= font->GetLineDistance();
							potentialLineBreakPosition = 0.f;
							potentialLineBreakSymbolIndexInCurrentString = 0;
						}
					}
				}
			}
		}

		resultMesh->Add(*glyph->GlyphMesh);
		resultMesh->TranslateVertexPosition(meshIndex + 0, position);
		resultMesh->TranslateVertexPosition(meshIndex + 1, position);
		resultMesh->TranslateVertexPosition(meshIndex + 2, position);
		resultMesh->TranslateVertexPosition(meshIndex + 3, position);

		meshIndex += 4;

		currentLineWidth = position.X + kerningCorrection + glyph->BearingX + glyph->Width;
		position.X += glyph->AdvanceX;

		previousChar = currentChar;
		stringGlyphCount++;
	}

	currentLineWidth += spaceWidth * (text.GetLength() - text.TrimRight().GetLength());
	stringWidths.Add(ValuePair<float, integer>(currentLineWidth, stringGlyphCount));	

	return resultMesh;
}

//===========================================================================//

void NoticeBuilder::CorrectMeshAlignment(const SharedPtr<Mesh> & mesh, const WeakPtr<Font> & font, List<ValuePair<float, integer>> & stringWidths,
                                         TextHorizontalAlignment horizontalAlignment, TextVerticalAlignment verticalAlignment)
{
	float textHeight = (stringWidths.GetCount()-1) * font->GetLineDistance() + font->GetHeight();
	Vector2 offset;

	switch (verticalAlignment)
	{
	case TextVerticalAlignment::Baseline:
		offset.Y = 0;
		break;
	case TextVerticalAlignment::Top:
		offset.Y = -font->GetBaseLineAscender();
		break;
	case TextVerticalAlignment::Bottom:
		offset.Y = (stringWidths.GetCount()-1) * font->GetLineDistance() - font->GetBaseLineDescender();
		break;
	case TextVerticalAlignment::Center:
		offset.Y = -Math::Round(textHeight * 0.5f + font->GetBaseLineDescender());
		break;
	case TextVerticalAlignment::Count:
	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined vertical aligment");
	}

	integer glyphIndex = 0;
	for (auto & line : stringWidths)
	{
		switch (horizontalAlignment)
		{
		case TextHorizontalAlignment::Left :
			offset.X = 0;
			break;
		case TextHorizontalAlignment::Right:
			offset.X = -line.First;
			break;
		case TextHorizontalAlignment::Center:
			offset.X = Math::Round(line.First * -0.5f);
			break;

		case TextHorizontalAlignment::Count:
		default:
			INVALID_ARGUMENT_EXCEPTION("Not defined horizontal aligment");
		}

		for (integer i = 0; i < line.Second; i++, glyphIndex++)
		{
			mesh->TranslateVertexPosition(glyphIndex * 4 + 0, offset);
			mesh->TranslateVertexPosition(glyphIndex * 4 + 1, offset);
			mesh->TranslateVertexPosition(glyphIndex * 4 + 2, offset);
			mesh->TranslateVertexPosition(glyphIndex * 4 + 3, offset);
		}
	}
}

//===========================================================================//

} //namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Glyph.h"

namespace Bru
{

//===========================================================================//

Glyph::Glyph() :
	X(0.0f),
	Y(0.0f),
	BitmapWidth(0.0f),
	BitmapHeight(0.0f),
	Width(0.0f),
	Height(0.0f),
	AdvanceX(0.0f),
	BearingX(0.0f),
	BearingY(0.0f),
	GlyphMesh()
{
}

//===========================================================================//

}

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Строит Mesh надписи по тексту, шрифту, параметрам расположения текста
//              Размеры Mesh в пикселях
//

#pragma once
#ifndef NOTICE_BUILDER_H
#define NOTICE_BUILDER_H

#include "../../Common/Common.h"
#include "../../Math/Math.h"
#include "Enums/TextHorizontalAlignment.h"
#include "Enums/TextVerticalAlignment.h"
#include "Font.h"
#include "Enums/WordWrapMode.h"
#include "NoticeInfo.h"

namespace Bru
{

//===========================================================================//

class NoticeBuilder
{
public:
	static NoticeInfo GetNoticeMesh(const string & text, const WeakPtr<Font> & font, const Color & color,
	                                Vector2 & textSize, TextHorizontalAlignment horizontalAlignment, TextVerticalAlignment verticalAlignment,
	                                WordWrapMode wordWrapMode = WordWrapMode::Simple);

private:
	static SharedPtr<Mesh> BuildTextMesh(const string & text, Vector2 & textSize, const WeakPtr<Font> & font,
	                                     WordWrapMode wrapMode, List<ValuePair<float, integer>> & stringWidths);

	static void CorrectMeshAlignment(const SharedPtr<Mesh> & mesh, const WeakPtr<Font> & font, List<ValuePair<float, integer> > & stringWidths,
	                                 TextHorizontalAlignment horizontalAlignment, TextVerticalAlignment verticalAlignment);

	NoticeBuilder() = delete;
	NoticeBuilder(const NoticeBuilder &) = delete;
	NoticeBuilder & operator =(const NoticeBuilder &) = delete;
	~NoticeBuilder() = delete;
};

//===========================================================================//

}// namespace

#endif // NOTICE_BUILDER_H

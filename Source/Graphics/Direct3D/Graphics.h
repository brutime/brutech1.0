//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ����� ���������� ������ ������� � ������� Direct 3D
//

#pragma once
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "../Graphics.h"

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

class SingletonGraphics : public AbstractGraphics, public SingletonMixIn<SingletonGraphics>
{
	friend class SingletonMixIn<SingletonGraphics>;

public:

	static const float DefaultDepth;

	virtual ~SingletonGraphics() {};

	void CreateDevice(const SharedPtr<Window> & someWindow);
	void DeleteDevice();
	void RestartDevice();

	void ResizeViewport(const integer width, const integer height);

	void LoadMatrix(const Matrix4x4 & matrix);

	void Clear(const integer frameBuffers, const Color & color);
	void Clear(const integer frameBuffers, const Color & color, const float depth);

	void BeginFrame();
	void EndFrame();

	void SetShadeMode(const ShadeMode shadeMode);
	void SetCullMode(const CullMode cullMode);
	void SetDepthFunction(const DepthFunction depthFunction);

	void SetPerspectiveCorrectionHint(const HintType hintType);

	void EnableDepthTest();
	void DisableDepthTest();

	void EnableDepthBufferWrite();
	void DisableDepthBufferWrite();

	void EnableLighting();
	void DisableLighting();

protected:

	SingletonGraphics();

private:

	LPDIRECT3D9         direct3d;
	LPDIRECT3DDEVICE9   device;

	static integer ConvertShadeMode(const ShadeMode shadeMode);
};

//===========================================================================//

#define Graphics SingletonGraphics::Instance()

//===========================================================================//

} //namespace

#endif // GRAPHICS_H

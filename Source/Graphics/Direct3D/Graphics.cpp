//
//    BruTech Source Code
//    Copyright (C) 2010-2012, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Graphics.h"

#include "../../Common/Exceptions/NotImplementatedException.h"
#include "../../Common/Helpers/Memory.h"
#include "../../Math/Math.h"
#include "../../Math/Matrices/Matrix4x4.h"

namespace Bru
{

//===========================================================================//

const float SingletonGraphics::DefaultDepth = 1.0f;

//===========================================================================//

SingletonGraphics::SingletonGraphics() : AbstractGraphics()
{
}

//===========================================================================//

void SingletonGraphics::CreateDevice(const SharedPtr<Window> & someWindow)
{
	SetRenderingWindow(someWindow);

	direct3d = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS direct3dParameters;

	Memory::FillWithNull(&direct3dParameters, sizeof(direct3dParameters));
	direct3dParameters.Windowed = TRUE;
	direct3dParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	direct3dParameters.hDeviceWindow = GetRenderingWindow()->GetHandle();
	direct3dParameters.BackBufferFormat = D3DFMT_X8R8G8B8;
	direct3dParameters.BackBufferWidth = GetRenderingWindow()->GetWidth();
	direct3dParameters.BackBufferHeight = GetRenderingWindow()->GetHeight();
	direct3dParameters.EnableAutoDepthStencil = TRUE;
	direct3dParameters.AutoDepthStencilFormat = D3DFMT_D16;

	direct3d->CreateDevice(D3DADAPTER_DEFAULT,
	                       D3DDEVTYPE_HAL,
	                       GetRenderingWindow()->GetHandle(),
	                       D3DCREATE_SOFTWARE_VERTEXPROCESSING,
	                       &direct3dParameters,
	                       &device);
}

//===========================================================================//

void SingletonGraphics::DeleteDevice()
{
	device->Release();
	direct3d->Release();
	ClearRenderingWindow();
}

//===========================================================================//

void SingletonGraphics::RestartDevice()
{
	NOT_IMPLEMENTATED_EXCEPTION("RestartDevice()");
}

//===========================================================================//

void SingletonGraphics::ResizeViewport(const integer width, const integer height)
{
//    D3DVIEWPORT9 viewport;
//
//    viewport.X = 0;
//    viewport.Y = 0;
//    viewport.Width = width;
//    viewport.Height = height;
//    viewport.MinZ = 0.1f;
//    viewport.MaxZ = 100.0f;
//
//    device->SetViewport(&viewport);
//
//    D3DPRESENT_PARAMETERS direct3dParameters;
//    direct3dParameters.BackBufferWidth = width;
//    direct3dParameters.BackBufferHeight = height;
//    device->Reset(&direct3dParameters);

	D3DXMATRIX identityMatrix;
	D3DXMatrixIdentity(&identityMatrix);
	device->SetTransform(D3DTS_PROJECTION, &identityMatrix);

	Matrix4x4 perspectiveMatrix;
	perspectiveMatrix.Perspective(45.0f, static_cast<float>(width) / static_cast<float>(height), 0.1f, 100.0f);
	D3DXMATRIX projectionMatrix(perspectiveMatrix.PtrToBegin());
	device->SetTransform(D3DTS_PROJECTION, &projectionMatrix);
}

//===========================================================================//

void SingletonGraphics::LoadMatrix(const Matrix4x4 & matrix)
{
	NOT_IMPLEMENTATED_EXCEPTION("LoadMatrix()");
}

//===========================================================================//

void SingletonGraphics::Clear(const integer frameBuffers, const Color & color)
{
	Clear(frameBuffers, color, DefaultDepth);
}

//===========================================================================//

void SingletonGraphics::Clear(const integer frameBuffers, const Color & color, const float depth)
{
	DWORD flags = ConvertFrameBuffers(frameBuffers);
	device->Clear(0,
	              NullPtr,
	              flags,
	              D3DCOLOR_ARGB(color.GetAlphaByte(), color.GetRedByte(), color.GetGreenByte(), color.GetBlueByte()),
	              depth,
	              0.0f);
}

//===========================================================================//

void SingletonGraphics::BeginFrame()
{
	device->BeginScene();
}

//===========================================================================//

void SingletonGraphics::EndFrame()
{
	device->EndScene();
	device->Present(NULL, NULL, NULL, NULL);
}

//===========================================================================//

//Можно было бы сделать через ToFixedArray(), но тогда будет двойной проход -
//первый чтобы построить FixedArray, а второй собстенно добавляет вершины из этого fixed array
void SingletonGraphics::DrawVertices(const AddingVerticesMode addingVerticesMode,
                                     const SharedPtr< Array<Vertex> > & vertices)
{
	struct CustomVertex
	{
		FLOAT X, Y, Z;
		DWORD Color;
	};
#define CUSTOMFVF (D3DFVF_XYZ | D3DFVF_DIFFUSE)

	CustomVertex customVertices[vertices->GetSize()];

	for (integer i = 0; i < vertices->GetSize(); i++)
	{
		customVertices[i].X = vertices->GetElement(i).GetX();
		customVertices[i].Y = vertices->GetElement(i).GetY();
		customVertices[i].Z = vertices->GetElement(i).GetZ();
		customVertices[i].Color = D3DCOLOR_ARGB(vertices->GetElement(i).GetColor().GetAlphaByte(),
		                                        vertices->GetElement(i).GetColor().GetRedByte(),
		                                        vertices->GetElement(i).GetColor().GetGreenByte(),
		                                        vertices->GetElement(i).GetColor().GetBlueByte());
	}

	LPDIRECT3DVERTEXBUFFER9 v_buffer = NULL;

	device->CreateVertexBuffer(vertices->GetSize() * sizeof(CustomVertex),
	                           0,
	                           CUSTOMFVF,
	                           D3DPOOL_MANAGED,
	                           &v_buffer,
	                           NULL);

	VOID * pVoid;

	v_buffer->Lock(0, 0, (void **)&pVoid, 0);
	memcpy(pVoid, customVertices, sizeof(customVertices));
	v_buffer->Unlock();

	device->SetFVF(CUSTOMFVF);
	device->SetStreamSource(0, v_buffer, 0, sizeof(CustomVertex));
	device->DrawPrimitive(D3DPT_TRIANGLELIST, 0, vertices->GetSize() / 3);

	v_buffer->Release();
}

//===========================================================================//

void SingletonGraphics::SetShadeMode(const ShadeMode shadeMode)
{
	device->SetRenderState(D3DRS_SHADEMODE, ConvertShadeMode(shadeMode));
}

//===========================================================================//

void SingletonGraphics::SetCullMode(const CullMode cullMode)
{
	device->SetRenderState(D3DRS_CULLMODE, ConvertCullMode(cullMode));
}

//===========================================================================//

void SingletonGraphics::SetDepthFunction(const DepthFunction depthFunction)
{
	device->SetRenderState(D3DRS_ZFUNC, ConvertDepthFunction(depthFunction));
}

//===========================================================================//

void SingletonGraphics::SetPerspectiveCorrectionHint(const HintType hintType)
{
	//В Direct 3D нет такого рода настроек
}

//===========================================================================//

void SingletonGraphics::EnableDepthTest()
{
	AbstractGraphics::EnableDepthTest();
	device->SetRenderState(D3DRS_ZENABLE, TRUE);
}

//===========================================================================//

void SingletonGraphics::DisableDepthTest()
{
	AbstractGraphics::DisableDepthTest();
	device->SetRenderState(D3DRS_ZENABLE, FALSE);
}

//===========================================================================//

void SingletonGraphics::EnableDepthBufferWrite()
{
	AbstractGraphics::EnableDepthBufferWrite();
	NOT_IMPLEMENTATED_EXCEPTION("EnableDepthBufferWrite()");
}

//===========================================================================//

void SingletonGraphics::DisableDepthBufferWrite()
{
	AbstractGraphics::DisableDepthBufferWrite();
	NOT_IMPLEMENTATED_EXCEPTION("DisableDepthBufferWrite()");
}

//===========================================================================//

void SingletonGraphics::EnableLighting()
{
	AbstractGraphics::EnableLighting();
	device->SetRenderState(D3DRS_LIGHTING, TRUE);
}

//===========================================================================//

void SingletonGraphics::DisableLighting()
{
	AbstractGraphics::DisableLighting();
	device->SetRenderState(D3DRS_LIGHTING, FALSE);
}

//===========================================================================//

} //namespace

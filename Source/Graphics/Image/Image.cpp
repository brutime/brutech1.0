//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Image.h"

#include "../../Utils/Log/Log.h"
#include "../../Utils/PathHelper/PathHelper.h"
#include "../../Math/Math.h"
#include "../../Common/Exceptions/OverflowException.h"
#include "../Exceptions/ImageCreatingException.h"
#include "../Exceptions/FormatMismatchException.h"
#include "../Graphics.h"

namespace Bru
{

//===========================================================================//

const string Image::CopyedImageNamePostfix = "Copy";

//===========================================================================//

Image::Image() :
	_name(),
	_width(0),
	_height(0),
	_pixelFormat(PixelFormat::Unknown),
	_data()
{
}

//===========================================================================//

Image::Image(const string & pathToFile) :
	_name(),
	_width(0),
	_height(0),
	_pixelFormat(PixelFormat::Unknown),
	_data()
{
	string fullPathToFile;

	string fullPathToJPGFile = PathHelper::GetFullPathToJPGImageFile(pathToFile);
	string fullPathToPNGFile = PathHelper::GetFullPathToPNGImageFile(pathToFile);
	string fullPathToTGAFile = PathHelper::GetFullPathToTGAImageFile(pathToFile);

	if (FileSystem::FileExists(fullPathToJPGFile))
	{
		fullPathToFile = fullPathToJPGFile;
	}
	else if (FileSystem::FileExists(fullPathToPNGFile))
	{
		fullPathToFile = fullPathToPNGFile;
	}
	else if (FileSystem::FileExists(fullPathToTGAFile))
	{
		fullPathToFile = fullPathToTGAFile;
	}
	else
	{
		FILE_NOT_FOUND_EXCEPTION(string::Format("Files not found: \"{0}\", \"{1}\", \"{2}\"",
		                                        fullPathToTGAFile, fullPathToPNGFile, fullPathToJPGFile));
	}

	_name = pathToFile;

	SharedPtr<IImageLoader> loader = GetImageLoader(fullPathToFile);
	loader->LoadFromFile(fullPathToFile, *this);

#ifdef DEBUG_MESSAGES
	Console->Debug(string::Format("Image loaded: {0}", ToString()));
#endif
}

//===========================================================================//

Image::Image(const string & name, integer width, integer height, PixelFormat pixelFormat,
             const Array<byte>::SimpleType & data) :
	_name(),
	_width(0),
	_height(0),
	_pixelFormat(PixelFormat::Unknown),
	_data()
{
	if (name.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("name is empty");
	}

	_name = name;

	integer expectedDataSize = width * height * GraphicsHelper::GetBytesPerPixel(pixelFormat);
	if (data.GetCount() != expectedDataSize)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("Data size {0} != expected data size {1}", data.GetCount(), expectedDataSize));
	}

	_width = width;
	_height = height;
	_pixelFormat = pixelFormat;

	if (expectedDataSize != 0)
	{
		_data = data;
	}

#ifdef DEBUG_MESSAGES
	Console->Debug(string::Format("Image created: {0}", ToString()));
#endif
}

//===========================================================================//

Image::Image(const Image & other) :
	_name(other._name + CopyedImageNamePostfix),
	_width(other._width),
	_height(other._height),
	_pixelFormat(other._pixelFormat),
	_data(other._data)
{
#ifdef DEBUG_MESSAGES
	Console->Debug(string::Format("Image created: {0}", ToString()));
#endif
}

//===========================================================================//

Image & Image::operator =(const Image & other)
{
	if (this == &other)
	{
		return *this;
	}

	_name = other._name + CopyedImageNamePostfix;
	_width = other._width;
	_height = other._height;
	_pixelFormat = other._pixelFormat;
	_data = other._data;

	return *this;
}

//===========================================================================//

Image::~Image()
{

#ifdef DEBUG_MESSAGES
	Console->Debug(string::Format("Image destroyed: \"{0}\"", _name));
#endif
}

//===========================================================================//

void Image::SaveToFile(const string & pathToFile) const
{
	if (!PathHelper::IsSupportedImageExtension(pathToFile))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Unknown file extension: \"{0}\"", pathToFile));
	}

	SharedPtr<IImageLoader> loader = GetImageLoader(pathToFile);
	loader->SaveToFile(PathHelper::PathToData + PathHelper::PathToTextures + pathToFile, *this);
	loader = NullPtr;

#ifdef DEBUG_MESSAGES
	Console->Debug(string::Format("Image saved: {0}", ToString()));
#endif
}

//===========================================================================//

void Image::SubData(integer offsetX, integer offsetY, const Image & subImage)
{
	if ((offsetX + subImage._width > _width) || (offsetY + subImage._height > _height))
	{
		OVERFLOW_EXCEPTION(string::Format("Sub image data doesn't fit into image data "
		                                  "(offsetX: {0}, offsetY: {1}, sum image name: \"{2}\")", offsetX, offsetY, subImage._name));
	}

	if (subImage._pixelFormat != _pixelFormat)
	{
		FORMAT_MISMATCH_EXCEPTION("Source and destination pixel formats don't match.");
	}

	integer pixelSize = GetPixelSize();
	for (integer i = 0; i < subImage._height; i++)
	{
//        data.Set((width * (offsetY + i) + offsetX) * pixelSize,
//                 subImage.data.Get((subImage.width) * i * pixelSize),
//                 subImage.width * pixelSize;)
		Memory::Copy(&_data[(_width * (offsetY + i) + offsetX) * pixelSize],
		             &subImage._data[(subImage._width) * i * pixelSize], subImage._width * pixelSize);
	}
}

//===========================================================================//

void Image::FlipHorizontal()
{
	integer lineLength = _width * GetPixelSize();
	Array<byte>::SimpleType lineData(lineLength);

	for (integer i = 0; i < _height / 2; i++)
	{
		lineData.SetArray(0, _data.GetArray(i * lineLength, lineLength));
		_data.SetArray(i * lineLength, _data.GetArray((_height - i - 1) * lineLength, lineLength));
		_data.SetArray((_height - i - 1) * lineLength, lineData);
	}
}

//===========================================================================//

void Image::ExpandDataForPowerOfTwo(Image & image)
{
	if (Math::IsPowerOfTwo(image._width) && Math::IsPowerOfTwo(image._height))
	{
		return;
	}

	integer powerOfTwoWidth = Math::GetNearestPowerOfTwo(image._width);
	integer powerOfTwoHeight = Math::GetNearestPowerOfTwo(image._height);
	integer pixelSize = image.GetPixelSize();

	Array<byte>::SimpleType expandedData(powerOfTwoWidth * powerOfTwoHeight * pixelSize);

	for (integer i = 0; i < image._height; i++)
	{
		//копируем строку
		Array<byte>::SimpleType lineData = image._data.GetArray(i * image._width * pixelSize, image._width * pixelSize);
		expandedData.SetArray(i * powerOfTwoWidth * pixelSize, lineData);
		//добавляем один пиксель крайнего пикселяи "добиваем" нулями, если ширина рамки больше 1 пикселя
		const integer rightBorderWidth = powerOfTwoWidth - image._width;
		if (rightBorderWidth > 0)
		{
			Array<byte>::SimpleType lastPixelData = lineData.GetArray(image._width * pixelSize - pixelSize, pixelSize);
			integer lastPixelIndex = (i * powerOfTwoWidth + image._width) * pixelSize;
			expandedData.SetArray(lastPixelIndex, lastPixelData);

			byte * lineOffsetPtr = &expandedData[lastPixelIndex + pixelSize];
			if (rightBorderWidth > 1)
			{
				Memory::FillWithNull(lineOffsetPtr, rightBorderWidth * pixelSize - pixelSize);
			}
		}
	}

	//и заливаем весь низ одним блоком и выходим
	const integer bottomBorderWidth = powerOfTwoHeight - image._height;
	if (bottomBorderWidth > 0)
	{
		Array<byte>::SimpleType lastLineData = expandedData.GetArray(powerOfTwoWidth * (image._height - 1) * pixelSize,
		                                       powerOfTwoWidth * pixelSize);

		integer lastLineIndex = powerOfTwoWidth * image._height * pixelSize;
		expandedData.SetArray(lastLineIndex, lastLineData);

		if (bottomBorderWidth > 1)
		{
			byte * blockStartPtr = &expandedData[powerOfTwoWidth * (image._height + 1) * pixelSize];
			Memory::FillWithNull(blockStartPtr, powerOfTwoWidth * (powerOfTwoHeight - (image._height + 1)) * pixelSize);
		}
	}

	image._width = powerOfTwoWidth;
	image._height = powerOfTwoHeight;
	image._data.Resize(expandedData.GetCount());
	Memory::Copy(image._data.GetData(), expandedData.GetData(), expandedData.GetCount());
}

//===========================================================================//

SharedPtr<IImageLoader> Image::GetImageLoader(const string & pathToFile) const
{
	string extension = PathHelper::GetExtension(pathToFile);
	extension = extension.ToLower();

	if (extension == PathHelper::JPGExtension)
	{
		return new JPGImageLoader();
	}
	
	if (extension == PathHelper::PNGExtension)
	{
		return new PNGImageLoader();
	}	

	if (extension == PathHelper::TGAExtension)
	{
		return new TGAImageLoader();
	}

	IMAGE_CREATING_EXCEPTION(string::Format("Unknown image file extension: \"{1}\"", pathToFile));
}

//===========================================================================//

string Image::ToString() const
{
	return string::Format("\"{0}\" ({1}x{2}x{3})", _name, _width, _height,
	                      GraphicsHelper::GetBitsPerPixel(_pixelFormat));
}

//===========================================================================//

}//namespace

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//		Описание: Перечисление типов упаковки данных формата TGA
//

#ifndef TGA_DATA_TYPE_H
#define TGA_DATA_TYPE_H
#pragma once

//===========================================================================//

enum class TGADataType
{
    ColorUnpacked = 2,
    MonochromeUnpacked = 3,
    ColorPacked = 10,
    MonochromePacked = 11
};

//===========================================================================//

#endif // TGA_DATA_TYPE_H

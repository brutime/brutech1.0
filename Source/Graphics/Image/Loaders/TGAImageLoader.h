//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Загрузчик TGA формата
//

#pragma once
#ifndef TGA_IMAGE_LOADER_H
#define TGA_IMAGE_LOADER_H

#include "IImageLoader.h"

#include "../../../FileSystem/FileSystem.h"
#include "TGAHeader.h"
#include "Enums/TGADataType.h"

namespace Bru
{

//===========================================================================//

class TGAImageLoader : public IImageLoader
{
public:
	TGAImageLoader();
	virtual ~TGAImageLoader();

	void LoadFromFile(const string & pathToFile, Image & image) const;
	void SaveToFile(const string & pathToFile, const Image & image) const;

private:
	TGAHeader ReadHeader(const SharedPtr<BinaryFileStream> & file) const;
	void ReadData(const SharedPtr<BinaryFileStream> & file, Image & image, const TGADataType dataType) const;
	void ReadPackedData(const SharedPtr<BinaryFileStream> & file, Image & image) const;

	void WriteHeader(const SharedPtr<BinaryFileStream> & file, TGAHeader & header) const;

	void SwapRedAndBlueColors(Image & image) const;

	bool IsBlockPacked(const byte blockInfo) const;
	integer GetPixelsCount(const byte blockInfo) const;

	TGAImageLoader(const TGAImageLoader &) = delete;
	TGAImageLoader & operator =(const TGAImageLoader &) = delete;
};

//===========================================================================//

}//namespace

#endif // TGA_IMAGE_LOADER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Формат заголовка
//    +--------+-------------+--------------+--------------------------------------------------------------+
//    | Кол-во | Тип         | Название     | Описание                                                     |
//    | байт   | переменной  |              |                                                              |
//    +--------+-------------+--------------+--------------------------------------------------------------+
//    | 1      | char        | IdLeight     | Длина текстовой информации после первого                     |
//    |        |             |              | 18-ти байтового блока. Может быть использована для описания  |
//    |        |             |              | файла                                                        |
//    | 1      | char        | ColorMap     | Идентификатор наличия цветовой карты - устарел               |
//    | 1      | char        | DataType     | Тип данных - запакованные или нет                            |
//    | 5      | char[5]     | ColorMapInfo | Информация о цветовой карте - устарело                       |
//    | 2      | int         | XOrigin      | Начало изображения по оси X                                  |
//    | 2      | int         | YOrigin      | Начало изображения по оси Y                                  |
//    | 2      | int         | Width        | Ширина изображения                                           |
//    | 2      | int         | Height       | Высота изображения                                           |
//    | 1      | char        | BitPerPixel  | Кол-во бит на пиксель                                        |
//    | 1      | char        | Description  | Описание                                                     |
//    +--------+-------------+--------------+--------------------------------------------------------------+
//    После этого до данных еще IdLeight байт
//

#include "TGAImageLoader.h"

#include "../../../Common/Common.h"
#include "../../../Math/Math.h"
#include "../../../Utils/PathHelper/PathHelper.h"
#include "../../Exceptions/ImageCreatingException.h"
#include "../Image.h"

namespace Bru
{

//===========================================================================//

TGAImageLoader::TGAImageLoader() :
	IImageLoader()
{
}

//===========================================================================//

TGAImageLoader::~TGAImageLoader()
{
}

//===========================================================================//

void TGAImageLoader::LoadFromFile(const string & pathToFile, Image & image) const
{
	SharedPtr<BinaryFileStream> file = new BinaryFileStream(pathToFile, FileAccess::Read);

	TGAHeader header = ReadHeader(file);

	image._width = header.Width;
	image._height = header.Height;
	image._pixelFormat = GraphicsHelper::GetPixelFormatByBitsPerPixel(header.BitsPerPixel);

	integer dataSize = header.Width * header.Height * GraphicsHelper::GetBytesPerPixel(header.BitsPerPixel);
	image._data.Resize(dataSize);

	ReadData(file, image, static_cast<TGADataType>(header.DataType));

	if (header.DataType == static_cast<byte>(TGADataType::ColorUnpacked)
	        || header.DataType == static_cast<byte>(TGADataType::ColorPacked))
	{
		image._pixelFormat = image._pixelFormat == PixelFormat::RGB ? PixelFormat::BGR : PixelFormat::BGRA;
	}

	file->Close();
}

//===========================================================================//

void TGAImageLoader::SaveToFile(const string & pathToFile, const Image & image) const
{
	SharedPtr<BinaryFileStream> file = new BinaryFileStream(pathToFile, FileAccess::Write);

	//Перед сохранением изображению необходимо поменять местами красный и синий цвет
	//поэтому создается не const копия
	Image savingImage = image;

	TGAHeader header;

	header.IdLength = 0;
	header.DataType = static_cast<byte>(TGADataType::ColorUnpacked);
	header.Width = savingImage._width;
	header.Height = savingImage._height;
	header.BitsPerPixel = GraphicsHelper::GetBitsPerPixel(savingImage._pixelFormat);

	WriteHeader(file, header);

	if (image._pixelFormat == PixelFormat::RGB || image._pixelFormat == PixelFormat::RGBA)
	{
		SwapRedAndBlueColors(savingImage);
	}

	file->Write(savingImage._data);

	file->Close();
}

//===========================================================================//

TGAHeader TGAImageLoader::ReadHeader(const SharedPtr<BinaryFileStream> & file) const
{
	TGAHeader header;

	header.IdLength = file->ReadByte();
	file->Seek(1);
	header.DataType = file->ReadByte();
	file->Seek(9);
	header.Width = file->ReadInteger(2);
	header.Height = file->ReadInteger(2);
	header.BitsPerPixel = file->ReadByte();
	file->Seek(1);
	file->Seek(header.IdLength);

	return header;
}

//===========================================================================//

void TGAImageLoader::ReadData(const SharedPtr<BinaryFileStream> & file, Image & image, const TGADataType dataType) const
{
	switch (dataType)
	{
	case TGADataType::ColorUnpacked:
	case TGADataType::MonochromeUnpacked:
		image._data = file->ReadByteArray(image.GetDataSize());
		break;

	case TGADataType::ColorPacked:
	case TGADataType::MonochromePacked:
		ReadPackedData(file, image);
		break;

	default:
		IMAGE_CREATING_EXCEPTION(string::Format("Data type {0} out of range. Coulnd't load file: \"{1}\"",
		                                        static_cast<byte>(dataType), file->GetPathToFile()));
	}
}

//===========================================================================//

void TGAImageLoader::ReadPackedData(const SharedPtr<BinaryFileStream> & file, Image & image) const
{
	integer pixelSize = image.GetPixelSize();
	integer readedBytesCount = 0;
	while (readedBytesCount < image.GetDataSize())
	{
		byte blockInfo = file->ReadByte();
		bool isBlockPacked = IsBlockPacked(blockInfo);
		integer pixelsCount = GetPixelsCount(blockInfo);

		if (isBlockPacked)
		{
			Array<byte>::SimpleType pixelData = file->ReadByteArray(pixelSize);
			for (long i = 0; i < pixelsCount; i++)
			{
				image._data.SetArray(readedBytesCount, pixelData);
				readedBytesCount += pixelData.GetCount();
			}
		}
		else
		{
			Array<byte>::SimpleType blockData = file->ReadByteArray(pixelsCount * pixelSize);
			image._data.SetArray(readedBytesCount, blockData);
			readedBytesCount += blockData.GetCount();
		}
	}
}

//===========================================================================//

void TGAImageLoader::WriteHeader(const SharedPtr<BinaryFileStream> & file, TGAHeader & header) const
{
	byte nullByte = 0;

	file->Write(header.IdLength);
	file->Write(nullByte, 1);
	file->Write(header.DataType);
	file->Write(nullByte, 9);
	file->Write(header.Width, 2);
	file->Write(header.Height, 2);
	file->Write(header.BitsPerPixel);
	file->Write(nullByte);
	file->Write(nullByte, header.IdLength);
}

//===========================================================================//

void TGAImageLoader::SwapRedAndBlueColors(Image & image) const
{
	integer pixelSize = image.GetPixelSize();
	for (integer i = 0; i < image.GetDataSize(); i += pixelSize)
	{
		integer blueColorValue = image._data[i];
		image._data[i] = image._data[i + 2];
		image._data[i + 2] = blueColorValue;
	}
}

//===========================================================================//

bool TGAImageLoader::IsBlockPacked(const byte blockInfo) const
{
	return ((blockInfo & 128) == 128) ? true : false;
}

//===========================================================================//

integer TGAImageLoader::GetPixelsCount(const byte blockInfo) const
{
	return (blockInfo & 127) + 1;
}

//===========================================================================//

} // namespace Bru

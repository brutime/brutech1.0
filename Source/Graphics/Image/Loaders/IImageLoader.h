//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Интерфейс загрузчика данных изображения
//

#pragma once
#ifndef I_IMAGE_LOADER_H
#define I_IMAGE_LOADER_H

#include "../../../Common/Common.h"
#include "../../Enums/PixelFormat.h"

namespace Bru
{

//===========================================================================//

class Image;

//===========================================================================//

class IImageLoader
{
public:
	virtual ~IImageLoader();

	virtual void LoadFromFile(const string & pathToFile, Image & someImage) const = 0;
	virtual void SaveToFile(const string & pathToFile, const Image & someImage) const = 0;

protected:
	IImageLoader();

private:
	IImageLoader(const IImageLoader &) = delete;
	IImageLoader & operator =(const IImageLoader &) = delete;
};

//===========================================================================//

}//namespace

#endif // I_IMAGE_LOADER_H

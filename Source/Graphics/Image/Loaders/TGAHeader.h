//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Структура заголовока TGA файла
//

#pragma once
#ifndef TGA_HEADER_H
#define TGA_HEADER_H

#include "../../../Common/Common.h"

namespace Bru
{

//===========================================================================//

struct TGAHeader
{
	TGAHeader();

	byte IdLength;
	byte DataType;
	integer Width;
	integer Height;
	byte BitsPerPixel;
};

//===========================================================================//

}//namespace

#endif // TGA_HEADER_H

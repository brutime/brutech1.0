//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "PNGImageLoader.h"

#include "../../../../Libs/LibPNG/png.h"
#include "../../../FileSystem/FileSystem.h"
#include "../../Exceptions/ImageCreatingException.h"
#include "../Image.h"

namespace Bru
{

//===========================================================================//

void PNGReadFunction(png_structp pngPtr, png_bytep data, png_size_t bytesCount)
{
	SharedPtr<BinaryFileStream> * stream = (SharedPtr<BinaryFileStream> *)png_get_io_ptr(pngPtr);
	stream->Get()->ReadByteArray(data, bytesCount);
}

//===========================================================================//

void PNGWriteFunction(png_structp pngPtr, png_bytep data, png_size_t bytesCount)
{
	SharedPtr<BinaryFileStream> * stream = (SharedPtr<BinaryFileStream> *)png_get_io_ptr(pngPtr);
	stream->Get()->Write(data, bytesCount);
}

//===========================================================================//

void PNGFlushFunction(png_structp pngPtr)
{
	SharedPtr<BinaryFileStream> * stream = (SharedPtr<BinaryFileStream> *)png_get_io_ptr(pngPtr);
	stream->Get()->Flush();
}

//===========================================================================//

PNGImageLoader::PNGImageLoader() :
	IImageLoader()
{
}

//===========================================================================//

PNGImageLoader::~PNGImageLoader()
{
}

//===========================================================================//

void PNGImageLoader::LoadFromFile(const string & pathToFile, Image & image) const
{
	SharedPtr<BinaryFileStream> file = new BinaryFileStream(pathToFile, FileAccess::Read);

	const integer signatureBytesCount = 8;

	Array<byte>::SimpleType signature = file->ReadByteArray(signatureBytesCount);
	if (!png_check_sig(signature.GetData(), signatureBytesCount))
	{
		IMAGE_CREATING_EXCEPTION(string::Format("File isn't recognized as a PNG file. Coulnd't load file: \"{0}\"", file->GetPathToFile()));
	}

	png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	if (pngPtr == NullPtr)
	{
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't create png struct. Coulnd't load file: \"{0}\"", file->GetPathToFile()));
	}

	png_infop pngInfo = png_create_info_struct(pngPtr);
	if (pngInfo == NullPtr)
	{
		png_destroy_read_struct(&pngPtr, 0, 0);
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't create png info. Coulnd't load file: \"{0}\"", file->GetPathToFile()));
	}

	png_set_read_fn(pngPtr, static_cast<void *>(&file), PNGReadFunction);

	png_set_sig_bytes(pngPtr, signatureBytesCount);

	png_read_info(pngPtr, pngInfo);

	png_uint_32 width = 0;
	png_uint_32 height = 0;
	int colorType = 0;
	int bitDepth = 0;
	png_get_IHDR(pngPtr, pngInfo, &width, &height, &bitDepth, &colorType, 0, 0, 0);

	png_set_expand(pngPtr);
	png_read_update_info(pngPtr, pngInfo);

	png_get_IHDR(pngPtr, pngInfo, &width, &height, &bitDepth, &colorType, 0, 0, 0);

	png_byte bitsPerPixel = png_get_channels(pngPtr, pngInfo);

	image._width = width;
	image._height = height;
	image._pixelFormat = GraphicsHelper::GetPixelFormatByBytesPerPixel(bitsPerPixel);

	png_uint_32 bytesInRow = png_get_rowbytes(pngPtr, pngInfo);

 	image._data.Resize(bytesInRow * height);

	Array<byte*> rowPointers(height);
	for (png_uint_32 i = 0; i < height; ++i)
	{
		rowPointers[height - i - 1] = image._data.GetData() + i * bytesInRow;
	}

	png_read_image(pngPtr, static_cast<byte **>(rowPointers.GetData()));
	png_read_end(pngPtr, 0);

	png_destroy_read_struct(&pngPtr, &pngInfo, 0);

	file->Close();
}

//===========================================================================//

void PNGImageLoader::SaveToFile(const string & pathToFile, const Image & image) const
{
	SharedPtr<BinaryFileStream> file = new BinaryFileStream(pathToFile, FileAccess::Write);

	png_structp pngPtr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (pngPtr == NullPtr)
	{
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't create png struct. Coulnd't load file: \"{0}\"", file->GetPathToFile()));
	}

	png_infop pngInfo = png_create_info_struct(pngPtr);
	if (pngInfo == NullPtr)
	{
		png_destroy_write_struct(&pngPtr, 0);
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't create png info. Coulnd't load file: \"{0}\"", file->GetPathToFile()));
	}

	png_set_write_fn(pngPtr, static_cast<void *>(&file), PNGWriteFunction, PNGFlushFunction);

	const int bitDepth = 8;
	int colorType = GetColorType(image._pixelFormat);

	png_set_IHDR(pngPtr, pngInfo, image._width, image._height, bitDepth, colorType,
	             PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);	
	
	png_uint_32 bytesInRow = png_get_rowbytes(pngPtr, pngInfo);
	
	Array<byte*> rowPointers(image._height);	
	for (integer i = 0; i < image._height; ++i)
	{
		rowPointers[image._height - i - 1] = image._data.GetData() + i * bytesInRow;
	}	
	
	png_write_info(pngPtr, pngInfo);
	png_write_image(pngPtr, const_cast<unsigned char **>(rowPointers.GetData()));
	png_write_end(pngPtr, pngInfo);
	png_destroy_write_struct(&pngPtr, &pngInfo);	

	file->Close();
}

//===========================================================================//

int PNGImageLoader::GetColorType(PixelFormat pixelFormat) const
{
	switch (pixelFormat)
	{
	case PixelFormat::Monochrome: return PNG_COLOR_TYPE_GRAY;
	case PixelFormat::MonochromeAlpha: return PNG_COLOR_TYPE_GRAY_ALPHA;
	case PixelFormat::RGB: return PNG_COLOR_TYPE_RGB;
	case PixelFormat::RGBA: return PNG_COLOR_TYPE_RGBA;
	
	case PixelFormat::BGR:
	case PixelFormat::BGRA:
		INVALID_ARGUMENT_EXCEPTION("Not supported pixelFormat");

	case PixelFormat::Unknown:
	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined pixelFormat");
	}
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Загрузчик JPG формата
//

#pragma once
#ifndef JPG_IMAGE_LOADER_H
#define JPG_IMAGE_LOADER_H

#include "IImageLoader.h"

#include "../../../../Libs/IJL/ijl.h"

namespace Bru
{

//===========================================================================//

class JPGImageLoader : public IImageLoader
{
public:
	JPGImageLoader();
	virtual ~JPGImageLoader();

	void LoadFromFile(const string & pathToFile, Image & image) const;
	void SaveToFile(const string & pathToFile, const Image & image) const;

private:
	void SetupPropertiesForWrite(const string & pathToFile, const Image & image,
	                             JPEG_CORE_PROPERTIES & jpegProperties) const;
	void FreeProperties(const string & pathToFile, JPEG_CORE_PROPERTIES & jpegProperties) const;

	JPGImageLoader(const JPGImageLoader &) = delete;
	JPGImageLoader & operator =(const JPGImageLoader &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // JPG_IMAGE_LOADER_H

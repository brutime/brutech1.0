//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "JPGImageLoader.h"

#include "../../../Math/Math.h"
#include "../../../FileSystem/FileSystem.h"
#include "../../../Utils/PathHelper/PathHelper.h"
#include "../../Exceptions/ImageCreatingException.h"
#include "../Image.h"

namespace Bru
{

//===========================================================================//

JPGImageLoader::JPGImageLoader() :
	IImageLoader()
{
}

//===========================================================================//

JPGImageLoader::~JPGImageLoader()
{
}

//===========================================================================//

void JPGImageLoader::LoadFromFile(const string & pathToFile, Image & image) const
{
	JPEG_CORE_PROPERTIES jpegProperties;
	Memory::FillWithNull(&jpegProperties, sizeof(JPEG_CORE_PROPERTIES));

	IJLERR lastError;

	lastError = ijlInit(&jpegProperties);
	if (lastError != IJL_OK)
	{
		FreeProperties(pathToFile, jpegProperties);
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't initialize jpeg properties. Couldn't load file: \"{0}\"", pathToFile));
	}

	//Считываем заголовок
	jpegProperties.JPGFile = pathToFile.ToChars();
	lastError = ijlRead(&jpegProperties, IJL_JFILE_READPARAMS);
	if (lastError != IJL_OK)
	{
		FreeProperties(pathToFile, jpegProperties);
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't read jpeg properties. Couldn't load file: \"{0}\"", pathToFile));
	}

	image._width = jpegProperties.JPGWidth;
	image._height = jpegProperties.JPGHeight;
	image._pixelFormat = PixelFormat::BGR;

	integer dataSize = jpegProperties.JPGWidth * jpegProperties.JPGHeight * GraphicsHelper::GetBytesPerPixel(image._pixelFormat);
	image._data.Resize(dataSize);

	jpegProperties.DIBBytes = image._data.GetData();
	jpegProperties.DIBColor = IJL_BGR;
	jpegProperties.DIBWidth = jpegProperties.JPGWidth;
	jpegProperties.DIBHeight = jpegProperties.JPGHeight;
	jpegProperties.DIBChannels = 3;

	//Считываем данные
	lastError = ijlRead(&jpegProperties, IJL_JFILE_READWHOLEIMAGE);
	if (lastError != IJL_OK)
	{
		FreeProperties(pathToFile, jpegProperties);
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't read jpeg data. Couldn't load file: \"{0}\"", pathToFile));
	}

	image.FlipHorizontal();

	FreeProperties(pathToFile, jpegProperties);
}

//===========================================================================//

void JPGImageLoader::SaveToFile(const string & pathToFile, const Image & image) const
{
	if (image.GetPixelFormat() != PixelFormat::RGB && image.GetPixelFormat() != PixelFormat::BGR)
	{
		INVALID_ARGUMENT_EXCEPTION("Unsupported pixel format. JPG image must be in RGB or BGR format");
	}

	JPEG_CORE_PROPERTIES jpegProperties;
	Memory::FillWithNull(&jpegProperties, sizeof(JPEG_CORE_PROPERTIES));

	IJLERR lastError;

	lastError = ijlInit(&jpegProperties);
	if (lastError != IJL_OK)
	{
		FreeProperties(pathToFile, jpegProperties);
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't initialize jpeg properties. Couldn't load file: \"{0}\"", pathToFile));
	}

	//Перед сохранением изображению необходимо перевернуть по горизонтали
	//поэтому создается не const копия
	Image savingImage = image;
	savingImage.FlipHorizontal();

	SetupPropertiesForWrite(pathToFile, savingImage, jpegProperties);

	lastError = ijlWrite(&jpegProperties, IJL_JFILE_WRITEWHOLEIMAGE);
	if (lastError != IJL_OK)
	{
		FreeProperties(pathToFile, jpegProperties);
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't write jpeg data. Couldn't save file: \"{0}\"", pathToFile));
	}

	FreeProperties(pathToFile, jpegProperties);
}

//===========================================================================//

void JPGImageLoader::SetupPropertiesForWrite(const string & pathToFile, const Image & image,
        JPEG_CORE_PROPERTIES & jpegProperties) const
{
	jpegProperties.jquality = 100; //100%
	jpegProperties.JPGFile = pathToFile.ToChars();
	jpegProperties.JPGWidth = image._width;
	jpegProperties.JPGHeight = image._height;
	jpegProperties.JPGColor = IJL_YCBCR;
	jpegProperties.JPGChannels = 3;
	jpegProperties.JPGSubsampling = IJL_NONE;

	jpegProperties.DIBBytes = image._data.GetData();
	jpegProperties.DIBWidth = image._width;
	jpegProperties.DIBHeight = image._height;
	jpegProperties.DIBColor = image._pixelFormat == PixelFormat::RGB ? IJL_RGB : IJL_BGR;
	jpegProperties.DIBChannels = 3;
	jpegProperties.DIBPadBytes = IJL_DIB_PAD_BYTES(jpegProperties.DIBWidth, 3);
}

//===========================================================================//

void JPGImageLoader::FreeProperties(const string & pathToFile, JPEG_CORE_PROPERTIES & jpegProperties) const
{
	IJLERR result = ijlFree(&jpegProperties);
	if (result != IJL_OK)
	{
		IMAGE_CREATING_EXCEPTION(string::Format("Couldn't free jpeg core properties. Couldn't file: \"{0}\"", pathToFile));
	}
}

//===========================================================================//

} // namespace Bru

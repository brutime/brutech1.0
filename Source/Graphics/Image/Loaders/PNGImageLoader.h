//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Загрузчик PNG формата
//

#pragma once
#ifndef PNG_IMAGE_LOADER_H
#define PNG_IMAGE_LOADER_H

#include "IImageLoader.h"

namespace Bru
{

//===========================================================================//

class PNGImageLoader : public IImageLoader
{
public:
	PNGImageLoader();
	virtual ~PNGImageLoader();

	void LoadFromFile(const string & pathToFile, Image & image) const;
	void SaveToFile(const string & pathToFile, const Image & image) const;

private:
	int GetColorType(PixelFormat pixelFormat) const;

	PNGImageLoader(const PNGImageLoader &) = delete;
	PNGImageLoader & operator =(const PNGImageLoader &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // PNG_IMAGE_LOADER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс данных изображения
//

#pragma once
#ifndef IMAGE_H
#define IMAGE_H

#include "../../Common/Common.h"
#include "../../Math/Math.h"
#include "../Enums/PixelFormat.h"
#include "../GraphicsHelper.h"
#include "Loaders/IImageLoader.h"
#include "Loaders/PNGImageLoader.h"
#include "Loaders/JPGImageLoader.h"
#include "Loaders/TGAImageLoader.h"

namespace Bru
{

//===========================================================================//

//#define DEBUG_MESSAGES

//===========================================================================//

class Image
{
	friend class JPGImageLoader;
	friend class PNGImageLoader;
	friend class TGAImageLoader;	

public:
	Image(const string & pathToFile);
	//Иногда при генерации изображения не нужно постоянно писать это в лог, например при создании
	//отдельных избражений для букв шрифта
	Image(const string & name, integer width, integer height, PixelFormat pixelFormat,
	      const Array<byte>::SimpleType & data);
	Image(const Image & other);
	Image & operator =(const Image & other);

	~Image();

	void SaveToFile(const string & pathToFile) const;

	void SubData(integer offsetX, integer offsetY, const Image & subImage);

	void FlipHorizontal();

	static void ExpandDataForPowerOfTwo(Image & sourceImage);

	string GetName() const
	{
		return _name;
	}
	integer GetWidth() const
	{
		return _width;
	}
	integer GetHeight() const
	{
		return _height;
	}
	PixelFormat GetPixelFormat() const
	{
		return _pixelFormat;
	}
	integer GetPixelSize() const
	{
		return GraphicsHelper::GetBytesPerPixel(_pixelFormat);
	}

	const Array<byte>::SimpleType & GetData() const
	{
		return _data;
	}
	integer GetDataSize() const
	{
		return _data.GetCount();
	}

	string ToString() const;

private:
	static const string CopyedImageNamePostfix;

	Image();

	SharedPtr<IImageLoader> GetImageLoader(const string & pathToFile) const;

	void DeepCopy(const Image & other);

	string _name;
	integer _width;
	integer _height;
	PixelFormat _pixelFormat;
	Array<byte>::SimpleType _data;
};

//===========================================================================//

}//namespace

#endif // IMAGE_DATA_H

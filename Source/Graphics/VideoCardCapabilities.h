//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс, собирающий и хранящий в себе возможности видео карты,
//              такие как поддержка VBO, шейдеров, максимальный размер текстуры и т. д.
//

#pragma once
#ifndef VIDEO_CARD_CAPABILITIES_H
#define VIDEO_CARD_CAPABILITIES_H

#include "../Common/Common.h"
#include "Enums/VideoCardVendor.h"

namespace Bru
{

//===========================================================================//

class VideoCardCapabilitiesSingleton
{
public:
	VideoCardCapabilitiesSingleton();
	~VideoCardCapabilitiesSingleton();

	void OutputVideoCardInfo();

	VideoCardVendor GetVendor() const;
	bool IsVerticalSyncSupported() const;

#if GRAPHIC_API == OPEN_GL

	bool IsDrawElementsSupported() const;

#elif GRAPHIC_API == DIRECT_3D

#else
#error Graphic API not defined
#endif

	integer GetMinTextureSize() const;
	integer GetMaxTextureSize() const;

private:
	void FindOutCapabilities();

	VideoCardVendor GetVendorByName(const string & someVendorName);
	string GetSupportedText(bool flag);

	VideoCardVendor _vendor;
	string _vendorName;
	string _modelName;
	string _hardwareVersionSupported;

	bool _isVerticalSyncSupported;

#if GRAPHIC_API == OPEN_GL

	bool _isDrawElementsSupported;

#elif GRAPHIC_API == DIRECT_3D

#else
#error Graphic API not defined
#endif

	integer _maxTextureSize;

	VideoCardCapabilitiesSingleton(const VideoCardCapabilitiesSingleton &) = delete;
	VideoCardCapabilitiesSingleton & operator =(const VideoCardCapabilitiesSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<VideoCardCapabilitiesSingleton> VideoCardCapabilities;

//===========================================================================//

}//namespace

#endif // VIDEO_CARD_CAPABILITIES_H

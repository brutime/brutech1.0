//
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef FORMAT_MISMATC_HEXCEPTION_H
#define FORMAT_MISMATC_HEXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class FormatMismatchException : public Exception
{
public:
	FormatMismatchException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~FormatMismatchException();
};

//===========================================================================//

#define FORMAT_MISMATCH_EXCEPTION(details) \
    throw FormatMismatchException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // FORMAT_MISMATCH_EXCEPTION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Исключение - невозможно создать изображение
//

#pragma once
#ifndef IMAGE_CREATING_EXCEPTION_H
#define IMAGE_CREATING_EXCEPTION_H

#include "../../Common/Exceptions/Exception.h"

namespace Bru
{

//===========================================================================//

class ImageCreatingException : public Exception
{
public:
	ImageCreatingException(const string & details, const string & fileName, const string & methodName, integer fileLine);
	virtual ~ImageCreatingException();
};

//===========================================================================//

#define IMAGE_CREATING_EXCEPTION(details) \
    throw ImageCreatingException(details, FILE_NAME, METHOD_NAME , FILE_LINE)

//===========================================================================//

}//namespace

#endif // IMAGE_CREATING_EXCEPTION_H

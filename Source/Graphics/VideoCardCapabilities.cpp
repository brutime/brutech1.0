//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "VideoCardCapabilities.h"

#include "../Utils/Console/Console.h"

namespace Bru
{

//===========================================================================//

SharedPtr<VideoCardCapabilitiesSingleton> VideoCardCapabilities;

//===========================================================================//

VideoCardCapabilitiesSingleton::VideoCardCapabilitiesSingleton() :
	_vendor(VideoCardVendor::Unknown),
	_vendorName(),
	_modelName(),
	_hardwareVersionSupported(),
	_isVerticalSyncSupported(false),
	_isDrawElementsSupported(false),
	_maxTextureSize(0)
{
	FindOutCapabilities();
}

//===========================================================================//

VideoCardCapabilitiesSingleton::~VideoCardCapabilitiesSingleton()
{
}

//===========================================================================//

void VideoCardCapabilitiesSingleton::OutputVideoCardInfo()
{
	Console->Notice("Video card info:");

	Console->Notice(string::Format(" - Vendor: \"{0}\"", _vendorName));
	Console->Notice(string::Format(" - Model: \"{0}\"", _modelName));
	Console->Notice(string::Format(" - Hardware version supported: \"{0}\"", _hardwareVersionSupported));
	Console->Notice(string::Format(" - Maximum texture's size is {0}", _maxTextureSize));
	Console->Notice(string::Format(" - Vertical sync is {0}", GetSupportedText(_isVerticalSyncSupported)));

#if GRAPHIC_API == OPEN_GL

	Console->Notice(string::Format(" - glDrawElements is {0}", GetSupportedText(_isDrawElementsSupported)));

#elif GRAPHIC_API == DIRECT_3D

#else
#error Graphic API not defined
#endif
}

//===========================================================================//

VideoCardVendor VideoCardCapabilitiesSingleton::GetVendor() const
{
	return _vendor;
}

//===========================================================================//

bool VideoCardCapabilitiesSingleton::IsVerticalSyncSupported() const
{
	return _isVerticalSyncSupported;
}

//===========================================================================//

bool VideoCardCapabilitiesSingleton::IsDrawElementsSupported() const
{
	return _isDrawElementsSupported;
}

//===========================================================================//

integer VideoCardCapabilitiesSingleton::GetMinTextureSize() const
{
	return 2;
}

//===========================================================================//

integer VideoCardCapabilitiesSingleton::GetMaxTextureSize() const
{
	return _maxTextureSize;
}

//===========================================================================//

VideoCardVendor VideoCardCapabilitiesSingleton::GetVendorByName(const string & someVendorName)
{
	//именно с пробелом в конце, чтобы это не было частью какого-нибудь слова
	if (someVendorName.ToUpper().Contains("ATI ") || someVendorName.ToUpper().Contains("AMD "))
	{
		return VideoCardVendor::ATI;
	}

	if (someVendorName.ToUpper().Contains("INTEL "))
	{
		return VideoCardVendor::Intel;
	}

	if (someVendorName.ToUpper().Contains("NVIDIA "))
	{
		return VideoCardVendor::NVIDIA;
	}

	return VideoCardVendor::Unknown;
}

//===========================================================================//

string VideoCardCapabilitiesSingleton::GetSupportedText(bool flag)
{
	return flag ? "supported" : "not supported";
}

//===========================================================================//

} // namespace Bru

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: класс, представляющий геометрию объекта. Содержит данные о вершинах,
//              не связанные с конкретным способом изображения объекта.
//

#pragma once
#ifndef MESH_H
#define MESH_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "Vertex.h"
#include "Enums/VerticesMode.h"

namespace Bru
{

//===========================================================================//

class Mesh
{
public:
	Mesh(const Mesh & other);
	Mesh(VerticesMode verticesMode);
	Mesh(VerticesMode verticesMode, integer reserveVertiesCount, integer reserveIndicesCount);
	virtual ~Mesh();
	
	Mesh & operator =(const Mesh & other);

	void Resize(integer vertiesCount, integer indicesCount);

	void Add(const Mesh & mesh);

	void AddVertex(const Vector3 & position, const Vector2 & textureCoord = Vector2(0.0f, 0.0f),
	               const Color & color = Color::Default);
	void SetVertex(integer vertexIndex, const Vector3 & position, const Vector2 & textureCoord = Vector2(0.0f, 0.0f),
	               const Color & color = Color::Default);
	void ClearVertices();

	void SetVertexPosition(integer vertexIndex, const Vector3 & vertexPosition);
	void TranslateVertexPosition(integer vertexIndex, const Vector3 & deltaVertexPosition);
	const Vector3 & GetVertexPosition(integer vertexIndex) const;
	
	void ScalePosition(float xFactor, float yFactor, float zFactor);

	///Метод возвращает новую вершину, координатами которой являются минимальные координаты вершин
	///Например, для v1 = {0, 22, 13}; v2 = {10, 11, 14} результат будет vr = {0, 11, 13}
	Vector3 GetMinVertexPosition() const;
	///Метод возвращает новую вершину, координатами которой являются максимальные координаты вершин
	///Например, для v1 = {0, 22, 13}; v2 = {10, 11, 14} результат будет vr = {10, 22, 14}
	Vector3 GetMaxVertexPosition() const;
	
	void SetVertexTextureCoord(integer vertexIndex, const Vector2 & vertexTextureCoord);
	const Vector2 & GetVertexTextureCoord(integer vertexIndex) const;	
	
	void SetVertexColor(integer vertexIndex, const Color & color);
	const Color & GetVertexColor(integer vertexIndex) const;

	void SetVerticesColor(const Color & color);
	void SetVerticesColorAlpha(float alpha);
	
	const Array<Vertex>::SimpleType & GetVertices() const;
	const Array<Vector3>::SimpleType & GetWorldVertices() const;
	VerticesMode GetVerticesMode() const;	

	void AddIndex(integer index);
	void SetIndex(integer indexIndex, integer index);
	integer GetMaxIndex() const;
	const Array<uint>::SimpleType & GetIndices() const;

	void CopyVertexPositionFromOtherMesh(const Mesh & other);
	void CopyTextureCoordFromOtherMesh(const Mesh & other);
	void CopyIndicesFromOtherMesh(const Mesh & other);

	void Clear();

	integer GetVertexPerFace() const;

	string ToString() const;

private:
	static integer GetVertexPerFaceByVerticesMode(VerticesMode verticesMode);

	Array<Vertex>::SimpleType _vertices;	
	VerticesMode _verticesMode;

	Array<uint>::SimpleType _indices;
	integer _vertexPerFace;
	integer _maxIndex;

	friend class TestMeshConstructor;
	friend class TestMeshReserveConstructor;
	friend class TestMeshClear;
	friend class TestMeshAddVertex;
	friend class TestMeshSetVertex;
	friend class TestMeshAddFace;
	friend class TestMeshSetFace;
};

//===========================================================================//

} // namespace Bru

#endif // MESH_H

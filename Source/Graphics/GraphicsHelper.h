//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание:
//

#pragma once
#ifndef GRAPHICS_HELPER_H
#define GRAPHICS_HELPER_H

#include "../Common/Common.h"
#include "Enums/PixelFormat.h"

namespace Bru
{

//===========================================================================//

class GraphicsHelper
{
public:
	static integer GetBitsPerPixel(PixelFormat pixelFormat);
	static integer GetBytesPerPixel(PixelFormat pixelFormat);
	
	static PixelFormat GetPixelFormatByBitsPerPixel(integer bitsPerPixel);	
	static PixelFormat GetPixelFormatByBytesPerPixel(integer bytesPerPixel);	

	static integer GetBytesPerPixel(integer bitsPerPixel);

private:
	GraphicsHelper() = delete;
	GraphicsHelper(const GraphicsHelper &) = delete;
	~GraphicsHelper() = delete;
	GraphicsHelper & operator =(const GraphicsHelper &) = delete;
};

//===========================================================================//

} // namespace Bru

#endif // GRAPHICSHELPER_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Класс текстуры
//

#pragma once
#ifndef TEXTURE_H
#define TEXTURE_H

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "Image/Image.h"
#include "Enums/TextureBorderType.h"
#include "Enums/PixelFormat.h"

namespace Bru
{

//===========================================================================//

class Texture
{
	friend class TextureManagerSingleton;

public:
	static const integer DefaultID;
	static const bool DefaultIsMipMap;
	static const bool DefaultIsSmooth;
	static const TextureBorderType DefaultBorderType;

	~Texture();

	string GetName() const;
	integer GetID() const;

	integer GetWidth() const;
	integer GetHeight() const;

	PixelFormat GetPixelFormat() const;

	float GetWidthFactor() const;
	float GetHeightFactor() const;

	bool IsMipMap() const;
	bool IsSmooth() const;
	TextureBorderType GetBorderType() const;

	static string GetBorderTypeTextByBorderType(TextureBorderType borderType);

	string ToString() const;

private:
	static const string CopyedTextureNamePostfix;

	Texture();
	Texture(const string & pathToFile);
	//Внимание! Изображение может изменяться в методе
	Texture(const string & mame, Image & image, bool isMipMap, bool isSmooth, TextureBorderType norderType);
	Texture(const Texture & other);
	Texture & operator =(const Texture & other);

	//Внимание! Изображение может изменяться в методе
	void Initialize(Image & image);
	void InitializeColorBlending();
	void InitializeSmooth();
	void InitializeBorderType();

	void Upload(const Image & image);
	void ATIBuildMipMaps();
	void FreeTexture();

	static void CheckSize(integer width, integer height);

	static TextureBorderType GetBorderTypeByIntegerValue(integer value);

	void DeepCopy(const Texture & other);

	string _name;
	integer _id;

	integer _width;
	integer _height;
	PixelFormat _pixelFormat;

	float _widthFactor;
	float _heightFactor;

	bool _isMipMap;
	bool _isSmooth;
	TextureBorderType _borderType;
};

//===========================================================================//

} // namespace Bru

#endif // TEXTURE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "GraphicsHelper.h"

namespace Bru
{

//===========================================================================//

integer GraphicsHelper::GetBitsPerPixel(PixelFormat pixelFormat)
{
	switch (pixelFormat)
	{
	case PixelFormat::Monochrome: return 8;
	case PixelFormat::MonochromeAlpha: return 16;
	case PixelFormat::RGB: return 24;
	case PixelFormat::RGBA: return 32;
	case PixelFormat::BGR: return 24;
	case PixelFormat::BGRA: return 32;	

	case PixelFormat::Unknown:
	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined pixelFormat");
	}
}

//===========================================================================//

integer GraphicsHelper::GetBytesPerPixel(PixelFormat pixelFormat)
{
	switch (pixelFormat)
	{
	case PixelFormat::Monochrome: return 1;
	case PixelFormat::MonochromeAlpha: return 2;
	case PixelFormat::RGB: return 3;
	case PixelFormat::RGBA: return 4;
	case PixelFormat::BGR: return 3;
	case PixelFormat::BGRA: return 4;

	case PixelFormat::Unknown:
	default:
		INVALID_ARGUMENT_EXCEPTION("Not defined pixelFormat");
	}
}

//===========================================================================//

PixelFormat GraphicsHelper::GetPixelFormatByBitsPerPixel(integer bitsPerPixel)
{
	switch (bitsPerPixel)
	{
	case  8: return PixelFormat::Monochrome;
	case 16: return PixelFormat::MonochromeAlpha;
	case 24: return PixelFormat::RGB;
	case 32: return PixelFormat::RGBA;

	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("bitsPerPixel out of range: {0} ([8, 16, 24, 32])", bitsPerPixel));
	}
}

//===========================================================================//

PixelFormat GraphicsHelper::GetPixelFormatByBytesPerPixel(integer bytesPerPixel)
{
	switch (bytesPerPixel)
	{
	case 1: return PixelFormat::Monochrome;
	case 2: return PixelFormat::MonochromeAlpha;
	case 3: return PixelFormat::RGB;
	case 4: return PixelFormat::RGBA;

	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("bytesPerPixel out of range: {0} ([1, 2, 3, 4])", bytesPerPixel));
	}
}

//===========================================================================//

integer GraphicsHelper::GetBytesPerPixel(integer bitsPerPixel)
{
	switch (bitsPerPixel)
	{
	case 8: return 1;
	case 16: return 2;
	case 24: return 3;
	case 32: return 4;

	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("bitsPerPixel out of range: {0} ([8, 16, 24, 32])", bitsPerPixel));
	}
}

//===========================================================================//

} // namespace Bru

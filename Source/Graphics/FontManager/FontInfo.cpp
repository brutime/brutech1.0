//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FontInfo.h"

namespace Bru
{

//===========================================================================//

FontInfo::FontInfo() :
	_name(),
	_size()
{
}

//===========================================================================//

FontInfo::FontInfo(const string & name, integer size) :
	_name(name),
	_size(size)
{
}

//===========================================================================//

bool FontInfo::operator ==(const FontInfo & other) const
{
	return _name == other._name && _size == other._size;
}

//===========================================================================//

bool FontInfo::operator !=(const FontInfo & other) const
{
	return !(other == *this);
}

//===========================================================================//

bool FontInfo::operator <(const FontInfo & other) const
{
	return _size < other._size || _name < other._name;
}

//===========================================================================//

bool FontInfo::operator >(const FontInfo & other) const
{
	return _size > other._size || _name > other._name;
}

//===========================================================================//

string FontInfo::ToString() const
{
	return string::Format("\"{0}\" {1}", _name, _size);
}

//===========================================================================//

}

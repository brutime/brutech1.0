//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: менеджер шрифтов. Инициализирует/деинициализирует бибилиотеку FreeType.
//              Хранит и возвращается WeakPtr на шрифты. Единолично владеет шрифтами
//              с помощью SharedPtr.
//

#pragma once
#ifndef FONT_MANAGER_H
#define FONT_MANAGER_H

#include <ft2build.h>
#include FT_FREETYPE_H

#include "../../Common/Common.h"
#include "../Font/Font.h"
#include "FontInfo.h"

namespace Bru
{

//===========================================================================//

class FontManagerSingleton
{
public:
	FontManagerSingleton();
	~FontManagerSingleton();

	WeakPtr<Font> Load(const string & pathToFile, integer fontSize, FontType fontType = FontType::AntiAliased);
	WeakPtr<Font> Load(const FontInfo & fontInfo, FontType fontType = FontType::AntiAliased);

	WeakPtr<Font> Get(const string & fontName, integer fontSize);
	WeakPtr<Font> Get(const FontInfo & fontInfo);

	void Remove(const string & fontName, integer fontSize);
	void Remove(const FontInfo & fontInfo);

	WeakPtr<Font> GetDefaultFont();
	FontInfo GetDefaultFontInfo() const;

	///Удаляются все шрифты, кроме шрифта по умолчанию
	void Clear();

	List<string> GetFontsList() const;

private:
	static const string Name;
	static const string SystemAlphabet;
	static const string PathToDefaultFont;

	void LoadDefaultFont();

	string GetAlphabet();

	TreeMap<FontInfo, SharedPtr<Font> > _fonts;
	string _defaultFontPathToFile;
	integer _defaultFontSize;

	FT_Library _library;
	FT_Error _lastError;

	FontManagerSingleton(const FontManagerSingleton &) = delete;
	FontManagerSingleton & operator =(const FontManagerSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<FontManagerSingleton> FontManager;

//===========================================================================//

}// namespace Bru

#endif // FONT_MANAGER_H

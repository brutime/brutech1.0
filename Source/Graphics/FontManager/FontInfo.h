//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: структура для хранения информации о шрифте
//

#pragma once
#ifndef FONT_INFO_H
#define FONT_INFO_H

#include "../../Common/Common.h"

namespace Bru
{

//===========================================================================//

class FontInfo
{
public:
	FontInfo();
	FontInfo(const string & name, integer size);

	bool operator ==(const FontInfo & other) const;
	bool operator !=(const FontInfo & other) const;
	bool operator <(const FontInfo & other) const;
	bool operator >(const FontInfo & other) const;

	const string & GetName() const
	{
		return _name;
	}

	void SetSize(integer newSize)
	{
		_size = newSize;
	}
	integer GetSize() const
	{
		return _size;
	}

	string ToString() const;

private:
	string _name;
	integer _size;
};

//===========================================================================//

}
#endif // FONT_INFO_H

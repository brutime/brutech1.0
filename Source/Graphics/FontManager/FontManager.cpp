//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "FontManager.h"

#include "../../Math/Math.h"
#include "../../Utils/Config/Config.h"
#include "../../Utils/Console/Console.h"
#include "../../Utils/LanguageHelper/LanguageHelper.h"

namespace Bru
{

//===========================================================================//

SharedPtr<FontManagerSingleton> FontManager;

//===========================================================================//

const string FontManagerSingleton::Name = "Font manager";

const string FontManagerSingleton::SystemAlphabet = " `~\"\'#$%^&*@(){}[]<>-+=_\\/|!?:;.,1234567890"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

const string FontManagerSingleton::PathToDefaultFont = "DefaultFont";

//===========================================================================//

FontManagerSingleton::FontManagerSingleton() :
	_fonts(),
	_defaultFontPathToFile(),
	_defaultFontSize(),
	_library(),
	_lastError(0)
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->AddOffset(2);

	_lastError = FT_Init_FreeType(&_library);
	if (_lastError != 0)
	{
		INITIALIZATION_EXCEPTION("Can't initialize FreeType library");
	}
	else
	{
		Console->Notice("FreeType library initialized");
	}

	LanguageHelper = new LanguageHelperSingleton();
	LoadDefaultFont();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

FontManagerSingleton::~FontManagerSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	Clear();
	LanguageHelper = NullPtr;
	_lastError = FT_Done_FreeType(_library);
	if (_lastError != 0)
	{
		SHUT_DOWN_EXCEPTION("Can't shut down FreeType library");
	}
	else
	{
		Console->Notice("FreeType library has shut down");
	}

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

WeakPtr<Font> FontManagerSingleton::Load(const string & pathToFile, integer fontSize, FontType fontType)
{
	FontInfo searchInfo(pathToFile, fontSize);
	if (_fonts.Contains(searchInfo))
	{
		Console->Warning(string::Format("FontManager: Font already loaded: \"{0}\"", searchInfo.ToString()));
		return Get(pathToFile, fontSize);
	}

	SharedPtr<Font> font = new Font(pathToFile, fontSize, fontType, GetAlphabet(), _library);
	_fonts.Add(searchInfo, font);
	return font;
}

//===========================================================================//

WeakPtr<Font> FontManagerSingleton::Load(const FontInfo & fontInfo, FontType fontType)
{
	return Load(fontInfo.GetName(), fontInfo.GetSize(), fontType);
}

//===========================================================================//

WeakPtr<Font> FontManagerSingleton::Get(const string & fontName, integer fontSize)
{
	FontInfo searchInfo(fontName, fontSize);
	if (!_fonts.Contains(searchInfo))
	{
		Console->Warning(
		    string::Format("FontManager: Font not found in manager: \"{0}\", attempt to load", searchInfo.ToString()));
		return Load(fontName, fontSize);
	}

	return _fonts[searchInfo];
}

//===========================================================================//

WeakPtr<Font> FontManagerSingleton::Get(const FontInfo & fontInfo)
{
	return Get(fontInfo.GetName(), fontInfo.GetSize());
}

//===========================================================================//

void FontManagerSingleton::Remove(const string & fontName, integer fontSize)
{
	FontInfo searchInfo(fontName, fontSize);
	if (!_fonts.Contains(searchInfo))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Font not found in manager: \"{0}\"", searchInfo.ToString()));
	}

	_fonts.Remove(searchInfo);
}

//===========================================================================//

void FontManagerSingleton::Remove(const FontInfo & fontInfo)
{
	Remove(fontInfo.GetName(), fontInfo.GetSize());
}

//===========================================================================//

WeakPtr<Font> FontManagerSingleton::GetDefaultFont()
{
	return Get(_defaultFontPathToFile, _defaultFontSize);
}

//===========================================================================//

FontInfo FontManagerSingleton::GetDefaultFontInfo() const
{
	return FontInfo(_defaultFontPathToFile, _defaultFontSize);
}

//===========================================================================//

void FontManagerSingleton::Clear()
{
	_fonts.Clear();
}

//===========================================================================//

List<string> FontManagerSingleton::GetFontsList() const
{
	integer maxNameLength = 0;
	integer maxSizeTextLength = 0;

	for (auto & fontPair : _fonts)
	{
		integer nameLength = fontPair.GetKey().GetName().GetLength();
		if (nameLength > maxNameLength)
		{
			maxNameLength = nameLength;
		}

		integer sizeTextLength = Converter::ToString(fontPair.GetKey().GetSize()).GetLength();
		if (sizeTextLength > maxSizeTextLength)
		{
			maxSizeTextLength = sizeTextLength;
		}
	}

	List<string> fontsList;

	for (auto & fontPair : _fonts)
	{
		//Имя шрифта | Размер | Тип
		string formatString = string("{0:") + Converter::ToString(-maxNameLength) + string("} | ");
		formatString += string("{1:") + Converter::ToString(-maxSizeTextLength) + string("} | ");

		string fontName = fontPair.GetKey().GetName();
		string fontSize = Converter::ToString(fontPair.GetKey().GetSize());

		string fontTextInfo = string::Format(formatString, fontName, fontSize);

		fontsList.Add(fontTextInfo);
	}

	return fontsList;
}

//===========================================================================//

void FontManagerSingleton::LoadDefaultFont()
{
	_defaultFontPathToFile = Config->Get<string>(PathToDefaultFont + ".PathToFont");
	_defaultFontSize = Config->Get<integer>(PathToDefaultFont + ".Size");
	Load(_defaultFontPathToFile, _defaultFontSize);
}

//===========================================================================//

string FontManagerSingleton::GetAlphabet()
{
	return SystemAlphabet + LanguageHelper->GetCurrentAlphabet();
}

//===========================================================================//

} // namespace Bru

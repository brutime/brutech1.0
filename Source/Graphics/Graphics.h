//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: графический API
//

#pragma once
#ifndef GRAPHICS_H
#define GRAPHICS_H

//===========================================================================//

#include "../Common/Common.h"
#include "../Math/Math.h"
#include "../OperatingSystem/OperatingSystem.h"
#include "VideoCardCapabilities.h"
#include "Enums/BlendFunction.h"
#include "Enums/CullMode.h"
#include "Enums/DepthFunction.h"
#include "Enums/FrameBufferType.h"
#include "Enums/PixelFormat.h"
#include "Enums/PolygonMode.h"
#include "Enums/ShadeMode.h"
#include "Enums/VerticesMode.h"
#include "Font/Enums/TextVerticalAlignment.h"
#include "Font/Enums/TextHorizontalAlignment.h"
#include "Font/Enums/WordWrapMode.h"
#include "Font/Font.h"
#include "Font/NoticeBuilder.h"
#include "FontManager/FontInfo.h"
#include "FontManager/FontManager.h"
#include "TextureManager/TextureManager.h"
#include "Mesh.h"
#include "Texture.h"

//===========================================================================//

namespace Bru
{

//===========================================================================//

//#ifdef DEBUGGING
//#define DEBUGGING_GRAPHICS
//#endif

//===========================================================================//

class GraphicsSingleton
{
public:
	static const integer ColorBufferDepth;
	static const integer DepthBufferDepth;

	GraphicsSingleton(const Color & clearColor);
	~GraphicsSingleton();

	void ResizeViewport(integer width, integer height);
	const IntRectangle & GetViewport() const;

	void LoadProjectionMatrix(const Matrix4x4 & newProjectionMatrix);
	void LoadModelMatrix(const Matrix4x4 & newModelMatrix);
	void LoadViewMatrix(const Matrix4x4 & newViewMatrix);

	void SetClearColor(const Color & color);
	const Color & GetClearColor() const;

	void SetClearDepth(float depth);
	float GetClearDepth() const;

	void BeginFrame();
	void EndFrame();
					  
	void DrawElements(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
	                  const Array<uint>::SimpleType & indices);					  

	void DrawElements(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
	                  const Array<uint>::SimpleType & indices, const Matrix4x4 & matrix);
					  
	void DrawElements(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
	                  const Array<Vector3>::SimpleType & worldVertices, const Array<uint>::SimpleType & indices);					  

	void SetShadeMode(ShadeMode shadeMode);
	void SetCullMode(CullMode cullMode);
	void SetDepthFunction(DepthFunction depthFunction);
	void SetBlendFunctions(BlendFunction sourceBlendFunction, BlendFunction destinationBlendFunction);

	void Set2DTexture(integer textureID);
	void SetNo2DTexture();
	integer GetTextureID() const;

	void SetScissorsRectangle(const IntRectangle & scissorsRectangle);
	const IntRectangle & GetScissorsRectangle() const;

	void SetPolygonMode(PolygonMode polygonMode);
	PolygonMode GetPolygonMode() const;

	void SetLineThickness(float lineThickness);
	float GetLineThickness() const;

	const Matrix4x4 & GetProjectionMatrix() const;
	const Matrix4x4 & GetModelMatrix() const;
	const Matrix4x4 & GetViewMatrix() const;

	void EnableDepthTest();
	void DisableDepthTest();
	bool IsDepthTestEnabled() const;

	void EnableLighting();
	void DisableLighting();
	bool IsLightingEnabled() const;

	void EnableBlending();
	void DisableBlending();
	bool IsBlendingEnabled() const;

	void Enable2DTexturing();
	void Disable2DTexturing();
	bool Is2DTexturingEnabled() const;

	void EnableScissorsTest();
	void DisableScissorsTest();
	bool IsScissorsTestEnabled() const;

	void EnableVerticalSync();
	void DisableVerticalSync();
	bool IsVerticalSyncEnabled() const;

#if GRAPHIC_API == OPEN_GL

	static void CheckForErrors(const string & fileName, string methodName, integer fileLine);

#elif GRAPHIC_API == DIRECT_3D

#else
#error Graphic API not defined
#endif

private:
	static const string Name;

	IntRectangle _viewport;

#if GRAPHIC_API == OPEN_GL

	//Устарешная отрисовка вершин по одной, а не массивом
	//Нужна для запуска на старых видеокартах
	void DrawElementsPerVertex(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
	                           const Array<uint>::SimpleType & indices);

	void DrawElementsPerVertex(VerticesMode verticesMode, const Array<Vertex>::SimpleType & vertices,
	                           const Array<Vector3>::SimpleType & worldVertices, const Array<uint>::SimpleType & indices);

	void InitExtensions();

	device_context _deviceContext;
	rendering_context _renderingContext;

	Matrix4x4 _projectionMatrix;
	Matrix4x4 _modelMatrix;
	Matrix4x4 _viewMatrix;

	Color _clearColor;
	float _clearDepth;
	integer _matrixMode;
	DepthFunction _depthFunction;
	BlendFunction _sourceBlendFunction;
	BlendFunction _destinationBlendFunction;
	integer _textureID;
	IntRectangle _scissorsRectangle;
	PolygonMode _polygonMode;
	float _lineThickness;

	//Экономим на вызовах OpenGL для того что можем держать в оперативке
	bool _isDepthTestEnabled;
	bool _isLightingEnabled;
	bool _isBlendingEnabled;
	bool _is2DTexturingEnabled;
	bool _isScissorsTestEnabled;

	bool _isVerticalSyncEnabled;

#elif GRAPHIC_API == DIRECT_3D

#else
#error Graphic API not defined
#endif

	GraphicsSingleton(const GraphicsSingleton &) = delete;
	GraphicsSingleton & operator =(const GraphicsSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<GraphicsSingleton> Graphics;

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#define CHECK_FOR_ERRORS() GraphicsSingleton::CheckForErrors(FILE_NAME, METHOD_NAME, FILE_LINE)

#endif

//===========================================================================//

} // namespace Bru

#endif // GRAPHICS_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "TextureManager.h"

#include "../../Utils/Console/Console.h"
#include "../../Utils/PathHelper/PathHelper.h"

namespace Bru
{

//===========================================================================//

SharedPtr<TextureManagerSingleton> TextureManager;

//===========================================================================//

const string TextureManagerSingleton::Name = "Texture manager";

//===========================================================================//

TextureManagerSingleton::TextureManagerSingleton() :
	_textures()
{
	Console->Notice(string::Format("{0} initializing...", Name));
	Console->Notice(string::Format("{0} initialized", Name));
}

//===========================================================================//

TextureManagerSingleton::~TextureManagerSingleton()
{
	Console->Notice(string::Format("{0} shutting down...", Name));
	Console->AddOffset(2);

	Clear();

	Console->ReduceOffset(2);
	Console->Notice(string::Format("{0} has shut down", Name));
}

//===========================================================================//

WeakPtr<Texture> TextureManagerSingleton::Load(const string & pathToFile)
{
	if (_textures.Contains(pathToFile))
	{
		Console->Warning(string::Format("TextureManager: Texture already loaded: \"{0}\"", pathToFile));
		return Get(pathToFile);
	}

	SharedPtr<Texture> texture = new Texture(pathToFile);
	_textures.Add(texture->GetName(), texture);
	return texture;
}

//===========================================================================//

WeakPtr<Texture> TextureManagerSingleton::Create(const string & name, Image & image, bool mipMap, bool smooth,
        TextureBorderType borderType)
{
	if (_textures.Contains(name))
	{
		Console->Warning(string::Format("TextureManager: Texture already created: \"{0}\"", name));
		return Get(name);
	}

	SharedPtr<Texture> texture = new Texture(name, image, mipMap, smooth, borderType);
	_textures.Add(texture->GetName(), texture);
	return texture;
}

//===========================================================================//

WeakPtr<Texture> TextureManagerSingleton::Get(const string & pathToFile)
{
	if (!_textures.Contains(pathToFile))
	{
		Console->Warning(string::Format("TextureManager: Texture not found in manager: \"{0}\", "
		                                "attempt to load", pathToFile));
		return Load(pathToFile);
	}

	return _textures[pathToFile];
}

//===========================================================================//

void TextureManagerSingleton::Remove(const string & pathToFile)
{
	if (!_textures.Contains(pathToFile))
	{
		INVALID_ARGUMENT_EXCEPTION(string::Format("Texture not found in manager: \"{0}\"", pathToFile));
	}

	return _textures.Remove(pathToFile);
}

//===========================================================================//

void TextureManagerSingleton::Clear()
{
	_textures.Clear();
}

//===========================================================================//

List<string> TextureManagerSingleton::GetTexturesList()
{
	List<string> texturesList;

	for (auto & texturePair : _textures)
	{
		texturesList.Add(texturePair.GetValue()->GetName());
	}

	return texturesList;
}

//===========================================================================//

}// namespace

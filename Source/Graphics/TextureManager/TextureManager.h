//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Менеджер текстур
//

#pragma once
#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include "../../Common/Common.h"
#include "../Texture.h"

namespace Bru
{

//===========================================================================//

class TextureManagerSingleton
{
public:
	TextureManagerSingleton();
	~TextureManagerSingleton();

	WeakPtr<Texture> Load(const string & pathToFile);

	///Внимание! Изображение может изменяться в методе
	WeakPtr<Texture> Create(const string & name, Image & image, bool mipMap = Texture::DefaultIsMipMap, bool smooth =
	                            Texture::DefaultIsSmooth, TextureBorderType borderType = Texture::DefaultBorderType);

	WeakPtr<Texture> Get(const string & pathToFile);

	void Remove(const string & pathToFile);

	void Clear();

	List<string> GetTexturesList();

private:
	static const string Name;

	TreeMap<string, SharedPtr<Texture>> _textures;

	TextureManagerSingleton(const TextureManagerSingleton &) = delete;
	TextureManagerSingleton & operator =(const TextureManagerSingleton &) = delete;
};

//===========================================================================//

extern SharedPtr<TextureManagerSingleton> TextureManager;

//===========================================================================//

}

#endif // TEXTURE_MANAGER_H

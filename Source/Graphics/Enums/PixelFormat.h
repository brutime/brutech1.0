//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тип пиксельного формата
//

#pragma once
#ifndef PIXEL_FORMAT_H
#define PIXEL_FORMAT_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

///Тип пиксельного формата
enum class PixelFormat
{
    Unknown = 0,
    Monochrome = GL_LUMINANCE,
    MonochromeAlpha = GL_LUMINANCE_ALPHA,
    RGB = GL_RGB,
    RGBA = GL_RGBA,
	BGR = GL_BGR,
    BGRA = GL_BGRA
};

//===========================================================================//

} // namespace Bru

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

///Тип пиксельного формата
enum class PixelFormat
{
    //ХЗ что там на самом деле
    Unknown = 0,
    Monochrome,
    MonochromeAlpha,
    RGB,
    RGBA,
};

//===========================================================================//

} // namespace Bru

#else
#error Graphic API not defined
#endif

//===========================================================================//

#endif // PIXEL_FORMAT_H

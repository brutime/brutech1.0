//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тип вершин
//

#pragma once
#ifndef VERTICES_MODE_MODE_H
#define VERTICES_MODE_MODE_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

///Тип вершин
enum class VerticesMode
{
	Points = GL_POINTS,
    Line = GL_LINES,
	LineStrip = GL_LINE_STRIP,
	LinesLoop = GL_LINE_LOOP,
    Triangles = GL_TRIANGLES,
    TriangleStrip = GL_TRIANGLE_STRIP,
	Polygon = GL_POLYGON
};

//===========================================================================//

} // namespace Bru

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

///Тип вершин
#error enum not full
enum class VerticesMode
{
	Points = D3DPT_POINTLIST,
    Line = D3DPT_LINELIST,
	LineStrip,
	LinesLoop,
    Triangles = D3DPT_TRIANGLELIST,
    TriangleStrip = D3DPT_TRIANGLESTRIP
	Polygon
};

//===========================================================================//

} // namespace Bru

#else
#error Graphic API not defined
#endif

//===========================================================================//

#endif // VERTICES_MODE_MODE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ������� ��� ��������� �� �������
//

#pragma once
#ifndef DEPTH_FUNCTION_H
#define DEPTH_FUNCTION_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

enum class DepthFunction
{
    Never = GL_NEVER,
    Less = GL_LESS,
    Equal = GL_EQUAL,
    LessOrEqual = GL_LEQUAL,
    Greater = GL_GREATER,
    NotEqual = GL_NOTEQUAL,
    GreaterOrEqual = GL_GEQUAL,
    Always = GL_ALWAYS
};

//===========================================================================//

}//namespace

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

enum class DepthFunction
{
    Never = D3DCMP_NEVER,
    Less = D3DCMP_LESS,
    Equal = D3DCMP_EQUAL,
    LessOrEqual = D3DCMP_LESSEQUAL,
    Greater = D3DCMP_GREATER,
    NotEqual = D3DCMP_NOTEQUAL,
    GreaterOrEqual = D3DCMP_GREATEREQUAL,
    Always = D3DCMP_ALWAYS
};

//===========================================================================//

} //namespace

#else
#error Graphic API not defined
#endif

//===========================================================================//

#endif // DEPTH_FUNCTION_H

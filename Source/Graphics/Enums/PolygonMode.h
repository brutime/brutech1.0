//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тип оботражения полигонов
//

#pragma once
#ifndef POLYGON_MODE_H
#define POLYGON_MODE_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

///Тип оботражения полигонов
enum class PolygonMode
{
    ///Линия
    Line = GL_LINE,
    ///Заполенный
    Fill = GL_FILL
};

//===========================================================================//

} // namespace Bru

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

///Тип оботражения полигонов
enum class PolygonMode
{
    ///Линия
    Line,
    ///Заполенным
    Fill
};

//===========================================================================//

} // namespace Bru

#else
#error Graphic API not defined
#endif

//===========================================================================//

#endif // POLYGON_MODE_H

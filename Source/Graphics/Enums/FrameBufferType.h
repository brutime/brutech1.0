//
//    BruTime Software (c) 2011. All rights reserved
//    This file is part of BruTech source code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тип буфера
//

#pragma once
#ifndef FRAME_BUFFER_TYPE_H
#define FRAME_BUFFER_TYPE_H

namespace Bru
{

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include <gl/gl.h>

///Тип буфера
enum FrameBufferType
{
    ///Буфер цвета
    FRAME_BUFFER_TYPE_COLOR = GL_COLOR_BUFFER_BIT,
    ///Буфер глубины
    FRAME_BUFFER_TYPE_DEPTH = GL_DEPTH_BUFFER_BIT
};

//===========================================================================//

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

///Тип буфера
enum FrameBufferType
{
	///Буфер цвета
	FRAME_BUFFER_TYPE_COLOR = D3DCLEAR_TARGET,
	///Буфер глубины
	FRAME_BUFFER_TYPE_DEPTH = D3DCLEAR_ZBUFFER
};

//===========================================================================//

#else
#error Graphic API not defined
#endif

//===========================================================================//

}
//namespace

#endif // FRAME_BUFFER_TYPE_H

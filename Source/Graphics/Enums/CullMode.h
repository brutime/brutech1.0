//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ��� ������ ������ ��� ��������
//
//    �� ��������� ����������, ����� �������� (� �� ��� �� ������) ��������� �������,
//    ���� �� �������, ������� � ������, ������������ �� ������ ������ ������� �������.
//

#pragma once
#ifndef CULL_MODE_H
#define CULL_MODE_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

///��� ������ ������ ��� ��������
enum class CullMode
{
    ///��� �������� - ��������� culling
    None,
    ///������ �������
    CounterClockWise = GL_FRONT,
    ///�� �������
    ClockWise = GL_BACK
};

//===========================================================================//

}//namespace

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

enum class CullMode
{
    ///��� ��������
    None = D3DCULL_NONE,
    ///������ �������
    CounterClockWise = D3DCULL_CCW,
    ///�� �������
    ClockWise = D3DCULL_CW
};

//===========================================================================//

} //namespace

#else
#error Graphic API not defined
#endif

//===========================================================================/

#endif // CULL_MODE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Функция блендинга
//

#pragma once
#ifndef BLEND_FUNCTION_H
#define BLEND_FUNCTION_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

enum class BlendFunction
{
    Zero = GL_ZERO,
    One = GL_ONE,
    SourceColor = GL_SRC_COLOR,
    OneMinusSourceColor = GL_ONE_MINUS_SRC_COLOR,
    DestinationColor = GL_DST_COLOR,
    OneMinusDestinationColor = GL_ONE_MINUS_DST_COLOR,
    SourceAlpha = GL_SRC_ALPHA,
    OneMinusSourceAlpha = GL_ONE_MINUS_SRC_ALPHA,
    DestinationAlpha = GL_DST_ALPHA,
    OneMinusDestinationAlpha = GL_ONE_MINUS_DST_ALPHA,
    ConstantColor = GL_CONSTANT_COLOR,
    OneMinusConstantColor = GL_ONE_MINUS_CONSTANT_COLOR,
    ConstantAlpha = GL_CONSTANT_ALPHA,
    OneMinusConstantAlpha = GL_ONE_MINUS_CONSTANT_ALPHA,
    SourceAlphaSaturate = GL_SRC_ALPHA_SATURATE
};

//===========================================================================//

}//namespace Bru

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

enum class BlendFunction
{
    Zero = DX_ZERO,
    One = DX_ONE,
    SourceColor = DX_SRC_COLOR,
    OneMinusSourceColor = DX_ONE_MINUS_SRC_COLOR,
    DestinationColor = DX_DST_COLOR,
    OneMinusDestinationColor = DX_ONE_MINUS_DST_COLOR,
    SourceAlpha = DX_SRC_ALPHA,
    OneMinusSourceAlpha = DX_ONE_MINUS_SRC_ALPHA,
    DestinationAlpha = DX_DST_ALPHA,
    OneMinusDestinationAlpha = DX_ONE_MINUS_DST_ALPHA,
    ConstantColor = DX_CONSTANT_COLOR,
    OneMinusConstantColor = DX_ONE_MINUS_CONSTANT_COLOR,
    ConstantAlpha = DX_CONSTANT_ALPHA,
    OneMinusConstantAlpha = DX_ONE_MINUS_CONSTANT_ALPHA,
    SourceAlphaSaturate = DX_SRC_ALPHA_SATURATE
};

//===========================================================================//

} //namespace

#else
#error Graphic API not defined
#endif

//===========================================================================//

#endif // BLEND_FUNCTION_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    ��������: ��� ���������� (���������� ������) ��������
//

#pragma once
#ifndef SHADE_MODE_H
#define SHADE_MODE_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

///��� ���������� (���������� ������) ��������
enum class ShadeMode
{
    ///������� ���������
    Flat = GL_FLAT,
    ///������� ���������
    Smooth = GL_SMOOTH
};

//===========================================================================//

}//namespace

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

///��� ���������� (���������� ������) ��������
enum class ShadeMode
{
    ///������� ���������
    Flat = D3DSHADE_FLAT,
    ///������� ���������
    Smooth = D3DSHADE_GOURAUD
};

//===========================================================================//

} //namespace

#else
#error Graphic API not defined
#endif

//===========================================================================//

#endif // SHADE_MODE_H

//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//
//    Описание: Тип края текстуры
//

#ifndef TEXTURE_BORDER_TYPE_H
#define TEXTURE_BORDER_TYPE_H

//===========================================================================//

#if GRAPHIC_API == OPEN_GL

#include "../OpenGL/BruGL.h"

namespace Bru
{

//===========================================================================//

///Тип края текстуры
enum class TextureBorderType
{
    Repeat = GL_REPEAT,
    ClampToEdge = GL_CLAMP_TO_EDGE
};

//===========================================================================//

}//namespace

#elif GRAPHIC_API == DIRECT_3D

#include "../../../Libs/Direct3D/d3d9.h"
#include "../../../Libs/Direct3D/d3dx9.h"

namespace Bru
{

//===========================================================================//

///Тип края текстуры
enum class TextureBorderType
{
    //ХЗ что там на самом деле
    Repeat = 0,
    ClampToEdge
};

//===========================================================================//

} //namespace

#else
#error Graphic API not defined
#endif

//===========================================================================//

#endif // TEXTURE_BORDER_TYPE_H

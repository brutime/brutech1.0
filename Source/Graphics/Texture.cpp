//
//    BruTech Source Code
//    Copyright (C) 2010-2013, BruTime
//    This file is part of BruTech Source Code.
//
//    All rights are reserved. Reproduction or transmission
//    in whole or in part, in any form or by any means,
//    electronic, mechanical or otherwise, is prohibited
//    without the prior written consent of the copyright owner.
//

#include "Texture.h"

#include "../Utils/ParametersFile/ParametersFile.h"
#include "../Utils/Console/Console.h"
#include "../Utils/PathHelper/PathHelper.h"
#include "VideoCardCapabilities.h"

namespace Bru
{

//===========================================================================//

const integer Texture::DefaultID = 0;
const bool Texture::DefaultIsMipMap = false;
const bool Texture::DefaultIsSmooth = true;
const TextureBorderType Texture::DefaultBorderType = TextureBorderType::ClampToEdge;

const string Texture::CopyedTextureNamePostfix = "Copy";

//===========================================================================//

Texture::Texture() :
	_name(string::Empty),
	_id(DefaultID),
	_width(0),
	_height(0),
	_pixelFormat(PixelFormat::Unknown),
	_widthFactor(0.0f),
	_heightFactor(0.0f),
	_isMipMap(DefaultIsMipMap),
	_isSmooth(DefaultIsSmooth),
	_borderType(DefaultBorderType)
{
}

//===========================================================================//

Texture::Texture(const string & pathToFile) :
	_name(pathToFile),
	_id(DefaultID),
	_width(0),
	_height(0),
	_pixelFormat(PixelFormat::Unknown),
	_widthFactor(0.0f),
	_heightFactor(0.0f),
	_isMipMap(DefaultIsMipMap),
	_isSmooth(DefaultIsSmooth),
	_borderType(DefaultBorderType)
{
	string fullPathToFile = PathHelper::GetFullPathToTextureFile(pathToFile);
	ParametersFile parametersFile(fullPathToFile);

	_isMipMap = parametersFile.Get<bool>("IsMipMap");
	_isSmooth = parametersFile.Get<bool>("IsSmooth");
	_borderType = GetBorderTypeByIntegerValue(parametersFile.Get<integer>("BorderType"));

	Image image(pathToFile);
	Initialize(image);

	Console->Notice(string::Format("Texture loaded: {0}", ToString()));
}

//===========================================================================//

Texture::Texture(const string & name, Image & image, bool isMipMap, bool isSmooth, TextureBorderType borderType) :
	_name(name),
	_id(DefaultID),
	_width(0),
	_height(0),
	_pixelFormat(PixelFormat::Unknown),
	_widthFactor(0.0f),
	_heightFactor(0.0f),
	_isMipMap(isMipMap),
	_isSmooth(isSmooth),
	_borderType(borderType)
{
	if (_name.IsEmpty())
	{
		INVALID_ARGUMENT_EXCEPTION("name is empty");
	}

	Initialize(image);
	Console->Notice(string::Format("Texture created {0}", ToString()));
}

//===========================================================================//

Texture::Texture(const Texture & other) :
	_name(string::Empty),
	_id(DefaultID),
	_width(0),
	_height(0),
	_pixelFormat(PixelFormat::Unknown),
	_widthFactor(0.0f),
	_heightFactor(0.0f),
	_isMipMap(DefaultIsMipMap),
	_isSmooth(DefaultIsSmooth),
	_borderType(DefaultBorderType)
{
	DeepCopy(other);
	Console->Notice(string::Format("Texture created: {0}", ToString()));
}

//===========================================================================//

Texture & Texture::operator =(const Texture & other)
{
	if (this != &other)
	{
		DeepCopy(other);
	}

	return *this;
}

//===========================================================================//

Texture::~Texture()
{
	FreeTexture();
	Console->Notice(string::Format("Texture destroyed: \"{0}\"", _name));
}

//===========================================================================//

string Texture::GetName() const
{
	return _name;
}

//===========================================================================//

integer Texture::GetID() const
{
	return _id;
}

//===========================================================================//

integer Texture::GetWidth() const
{
	return _width;
}

//===========================================================================//

integer Texture::GetHeight() const
{
	return _height;
}

//===========================================================================//

PixelFormat Texture::GetPixelFormat() const
{
	return _pixelFormat;
}

//===========================================================================//

float Texture::GetWidthFactor() const
{
	return _widthFactor;
}

//===========================================================================//

float Texture::GetHeightFactor() const
{
	return _heightFactor;
}

//===========================================================================//

bool Texture::IsMipMap() const
{
	return _isMipMap;
}

//===========================================================================//

bool Texture::IsSmooth() const
{
	return _isSmooth;
}

//===========================================================================//

TextureBorderType Texture::GetBorderType() const
{
	return _borderType;
}

//===========================================================================//

string Texture::GetBorderTypeTextByBorderType(TextureBorderType borderType)
{
	switch (borderType)
	{
	case TextureBorderType::Repeat:
		return "Repeat";
	case TextureBorderType::ClampToEdge:
		return "Clamp to edge";

	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("borderType {0} not defined", static_cast<integer>(borderType)));
	}
}

//===========================================================================//

TextureBorderType Texture::GetBorderTypeByIntegerValue(integer value)
{
	switch (value)
	{
	case 0:
		return TextureBorderType::Repeat;
	case 1:
		return TextureBorderType::ClampToEdge;

	default:
		INVALID_ARGUMENT_EXCEPTION(string::Format("value {0} not defined", static_cast<integer>(value)));
	}
}

//===========================================================================//

string Texture::ToString() const
{
	return string::Format("\"{0}\" ({1}, {2}x{3}x{4}, {5:0.2}x{6:0.2}, IsMipMap: {7}, IsSmooth: {8}, Border: {9})",
	                      _name, _id, _width, _height, GraphicsHelper::GetBitsPerPixel(_pixelFormat), _widthFactor, _heightFactor,
	                      Converter::ToString(_isMipMap), Converter::ToString(_isSmooth),
	                      Texture::GetBorderTypeTextByBorderType(_borderType));
}

//===========================================================================//

void Texture::CheckSize(integer width, integer height)
{
	const integer minTextureSize = VideoCardCapabilities->GetMinTextureSize();
	const integer maxTextureSize = VideoCardCapabilities->GetMaxTextureSize();

	if (width < minTextureSize || width > maxTextureSize)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("width {0} out of range [{1}..{2}]", width, minTextureSize, maxTextureSize));
	}

	if (height < minTextureSize || height > maxTextureSize)
	{
		INVALID_ARGUMENT_EXCEPTION(
		    string::Format("height {0} out of range [{1}..{2}]", height, minTextureSize, maxTextureSize));
	}
}

//===========================================================================//

}// namespace
